<?php

date_default_timezone_set('Asia/Jakarta');

$system_path = '../protected/system';
$application_folder = '../protected/backend/base';
$view_folder = '../protected/backend/views';

require_once '../protected/db.php';
require_once '../protected/system.php';
