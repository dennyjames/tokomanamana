<?php header('Content-type: application/xml; charset="ISO-8859-1"', true); ?>

<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?php echo site_url(); ?></loc>
        <lastmod><?php echo date('c', strtotime('2017-09-29 18:00:00')); ?></lastmod>
        <changefreq>Weekly</changefreq>
        <priority>0.5</priority>
    </url>
    <url>
        <loc><?php echo site_url('help'); ?></loc>
        <lastmod><?php echo date('c', strtotime('2017-09-29 18:00:00')); ?></lastmod>
        <changefreq>Weekly</changefreq>
        <priority>0.5</priority>
    </url>

    <?php foreach ($urls->result() as $url) { ?>
        <url>
            <loc><?php echo site_url($url->keyword); ?></loc>
            <lastmod><?php echo date('c', strtotime('2017-09-29 18:00:00')); ?></lastmod>
            <changefreq>Weekly</changefreq>
            <priority>0.5</priority>
        </url>
    <?php } ?>

</urlset>