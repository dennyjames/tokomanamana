<!DOCTYPE HTML>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <meta name="description" content="<?php echo (isset($meta_description)) ? $meta_description : settings('meta_description'); ?>"/>
        <meta name="keywords" content="<?php echo (isset($meta_keyword)) ? $meta_keyword : settings('meta_keyword'); ?>"/>
        <title><?php echo "{$title}"; ?></title>
        <?php
        foreach ($css as $file) {
            echo "\n    ";
            echo '<link href="' . $file . '" rel="stylesheet" type="text/css" />';
        } echo "\n";
        ?>
        <link rel="shortcut icon" href="<?php echo site_url('assets/frontend/images/favicon.png'); ?>">
        <style>

        </style>
        <script type="text/javascript">
            var base_url = '<?php echo base_url(); ?>';
            var current_url = '<?php echo current_url(); ?>';
            var decimal_digit = '<?php echo settings('number_of_decimal'); ?>';
            var decimal_separator = '<?php echo settings('separator_decimal'); ?>';
            var thousand_separator = '<?php echo settings('separator_thousand'); ?>';
        </script>
    </head>
    <body style="background:#f4f4f4">
        <div class="wrap">
            <div id="header">
                <div class="header">
                    <div class="top-header">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 hidden-sm hidden-xs">
                                    <div class="account-login">
                                        <a href="#"><i class="fa fa-phone"></i> <?php echo settings('phone'); ?></a>
                                        <a href="#"><i class="fa fa-envelope"></i> <?php echo settings('email'); ?></a>
                                        <a href="<?php echo site_url('merchant'); ?>">Daftar jadi penjual</a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <?php if (!$this->ion_auth->logged_in()) { ?>
                                        <div class="account-login account-login6">
                                            <a href="<?php echo site_url('member/login'); ?>">Login</a>
                                            <a href="<?php echo site_url('member/register'); ?>">Register</a>
                                        </div>
                                    <?php } else { ?>
                                        <div class="currency-language">
                                            <div class="currency-box">
                                                <a href="#" class="currency-current"><span><?php echo $user->fullname; ?></span></a>
                                                <ul class="currency-list list-unstyled">
                                                    <li><a href="<?php echo site_url('member/history'); ?>"><?php echo lang('account_text_order_history'); ?></a></li>
                                                    <li><a href="<?php echo site_url('member/profile'); ?>"><?php echo lang('account_text_profile'); ?></a></li>
                                                    <li><a href="<?php echo site_url('member/logout'); ?>"><?php echo lang('logout'); ?></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Top Header -->
                    <div class="main-header">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-12">
                                    <div class="logo logo4">
                                        <h1 class="hidden"><?php echo settings('company_name'); ?></h1>
                                        <a href="<?php echo site_url(); ?>"><img src="<?php echo site_url('assets/frontend/images/logo.png'); ?>" alt="<?php echo settings('company_name'); ?>" /></a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="smart-search">
                                        <form class="smart-search-form" action="<?php echo site_url('search'); ?>">
                                            <input type="text" name="word" placeholder="Cari nama atau kode SKU produk">
                                            <input type="submit" value="">
                                        </form>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-9">
                                    <div class="check-cart check-cart4">
                                        <div class="mini-cart-box">
                                            <a class="mini-cart-link" href="<?php echo site_url('cart'); ?>">
                                                <span class="mini-cart-icon"><i class="fa fa-shopping-basket" aria-hidden="true"></i></span>
                                                <span class="mini-cart-number"><?php echo ($this->ion_auth->logged_in()) ? $this->cart_model->get_total_cart() : 0; ?></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-3">
                                    <nav class="main-nav">
                                        <ul class="menu-top">
                                            <?php
                                            $categories = $this->category_model->get_categories(0);
                                            if ($categories) {
                                                foreach ($categories->result() as $category) {
                                                    $subcategories = $this->category_model->get_categories($category->id);
                                                    ?>
                                                    <li>
                                                        <a href="<?php echo seo_url('catalog/categories/view/' . $category->id); ?>"><?php echo $category->name; ?></a>
                                                        <?php if ($subcategories) { ?>
                                                            <div class="menu-sub-wrapper" data-sub="notebook">
                                                                <ul class="menu-sub container">
                                                                    <?php foreach ($subcategories->result() as $subcategory) { ?>
                                                                        <li>
                                                                            <a href="<?php echo seo_url('catalog/categories/view/' . $subcategory->id); ?>"><?php echo $subcategory->name; ?></a>
                                                                            <?php
                                                                            $subcategories2 = $this->category_model->get_categories($subcategory->id);
                                                                            if ($subcategories2) {
                                                                                ?>
                                                                                <ul class="menu-sub2">
                                                                                    <?php foreach ($subcategories2->result() as $subcategory2) { ?>
                                                                                        <li><a href="<?php echo seo_url('catalog/categories/view/' . $subcategory2->id); ?>"><?php echo $subcategory2->name; ?></a></li>
                                                                                    <?php } ?>
                                                                                </ul>
                                                                            <?php } ?>
                                                                        </li>
                                                                    <?php } ?>
                                                                </ul>
                                                            </div>
                                                        <?php } ?>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                        <a href="#" class="toggle-mobile-menu"><span></span></a>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-ontop">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="logo">
                                    <a href="<?php echo site_url(); ?>"><img src="<?php echo site_url('assets/frontend/images/logo.png'); ?>" alt="<?php echo settings('company_name'); ?>" /></a>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <nav class="main-nav">
                                    <ul class="menu-top">
                                        <?php
                                        $categories = $this->category_model->get_categories(0);
                                        if ($categories) {
                                            foreach ($categories->result() as $category) {
                                                $subcategories = $this->category_model->get_categories($category->id);
                                                ?>
                                                <li>
                                                    <a href="<?php echo seo_url('catalog/categories/view/' . $category->id); ?>"><?php echo $category->name; ?></a>
                                                    <?php if ($subcategories) { ?>
                                                        <div class="menu-sub-wrapper" data-sub="notebook">
                                                            <ul class="menu-sub container">
                                                                <?php foreach ($subcategories->result() as $subcategory) { ?>
                                                                    <li>
                                                                        <a href="<?php echo seo_url('catalog/categories/view/' . $subcategory->id); ?>"><?php echo $subcategory->name; ?></a>
                                                                        <?php
                                                                        $subcategories2 = $this->category_model->get_categories($subcategory->id);
                                                                        if ($subcategories2) {
                                                                            ?>
                                                                            <ul class="menu-sub2">
                                                                                <?php foreach ($subcategories2->result() as $subcategory2) { ?>
                                                                                    <li><a href="<?php echo seo_url('catalog/categories/view/' . $subcategory2->id); ?>"><?php echo $subcategory2->name; ?></a></li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                        <?php } ?>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        </div>
                                                    <?php } ?>
                                                </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                    <a href="#" class="toggle-mobile-menu"><span></span></a>
                                </nav>
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-12">
                                <div class="check-cart check-cart-ontop">
                                    <div class="search-hover-box">
                                        <a href="#" class="search-hover-link"><i class="fa fa-search" aria-hidden="true"></i></a>
                                        <form class="search-form-hover search" action="<?php echo site_url('search'); ?>">
                                            <input type="text" name="word" placeholder="<?php echo lang('search_product'); ?>">
                                        </form>
                                    </div>
                                    <div class="mini-cart-box">
                                        <a class="mini-cart-link" href="<?php echo site_url('cart'); ?>">
                                            <span class="mini-cart-icon"><i class="fa fa-shopping-basket" aria-hidden="true"></i></span>
                                            <span class="mini-cart-number"><?php echo ($this->ion_auth->logged_in()) ? $this->cart_model->get_total_cart() : 0; ?></span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Header On Top -->
            </div>
            <!-- End Header -->
            <div id="content">
                <?php echo $output; ?>
            </div>
            <!-- End Content -->
            <div id="footer">
                <div class="footer footer4">
                    <div class="container">
                        <div class="footer-list-box">
                            <div class="row">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="newsletter-form footer-box">
                                        <h2 class="title18" style="text-transform: none">Info promo</h2>
                                        <p style="padding-left: 0;">Selalu menjadi yang pertama mendapatkan informasi promo menarik dari KarawangShop.</p>
                                        <form>
                                            <input type="text" onblur="if (this.value == '')
                                                        this.value = this.defaultValue" onfocus="if (this.value == this.defaultValue)
                                                                    this.value = ''" value="Place enter your email">
                                            <input type="submit" value="Submit">
                                        </form>
                                    </div>
                                    <div class="social-footer footer-box">
                                        <h2 class="title14">Stay Connected</h2>
                                        <div class="list-social">
                                            <a href="https://www.facebook.com/krwgshop/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                            <a href="https://www.instagram.com/krwgshop/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="contact-footer-box footer-box">
                                        <h2 class="title18" style="text-transform: none">Kami siap melayani Anda</h2>
                                        <p style="padding-left: 0;">Setiap hari 09.00 - 18.00 WIB | Kecuali hari libur nasional.</p>
                                        <p style="padding-left: 0;"><i class="fa fa-phone"></i> <?php echo settings('phone'); ?> &nbsp;&nbsp;<i class="fa fa-envelope"></i> <a href="mailto:<?php echo settings('email'); ?>"><?php echo settings('email'); ?></a></p>
                                        <p style="padding-left: 0;"></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Footer List Box -->
                    </div>
                    <div class="payment-method text-center">
                        <a href="#" class="wobble-vertical"><img height="20px" src="<?php echo site_url('assets/frontend/images/footer/jne.png'); ?>" alt="" /></a>
                        <a href="#" class="wobble-vertical"><img height="20px" src="<?php echo site_url('assets/frontend/images/footer/bca.png'); ?>" alt="" /></a>
                        <a href="#" class="wobble-vertical"><img height="20px" src="<?php echo site_url('assets/frontend/images/footer/mandiri.png'); ?>" alt="" /></a>
                        <a href="#" class="wobble-vertical"><img height="20px" src="<?php echo site_url('assets/frontend/images/footer/bri.png'); ?>" alt="" /></a>
                        <a href="#" class="wobble-vertical"><img height="20px" src="<?php echo site_url('assets/frontend/images/footer/bni.png'); ?>" alt="" /></a>
                    </div>
                    <div class="footer-copyright">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <p class="copyright">KarawangShop © 2017. All rights reserved.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Footer Copyright -->
                </div>
            </div>
            <!-- End Footer -->
            <a href="#" class="radius scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
        </div>
        <style>.fb-livechat,.fb-widget{display:none}.ctrlq.fb-button,.ctrlq.fb-close{position:fixed;right:24px;cursor:pointer}.ctrlq.fb-button{z-index:1;background:url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/PjwhRE9DVFlQRSBzdmcgIFBVQkxJQyAnLS8vVzNDLy9EVEQgU1ZHIDEuMS8vRU4nICAnaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkJz48c3ZnIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDEyOCAxMjgiIGhlaWdodD0iMTI4cHgiIGlkPSJMYXllcl8xIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCAxMjggMTI4IiB3aWR0aD0iMTI4cHgiIHhtbDpzcGFjZT0icHJlc2VydmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiPjxnPjxyZWN0IGZpbGw9IiMwMDg0RkYiIGhlaWdodD0iMTI4IiB3aWR0aD0iMTI4Ii8+PC9nPjxwYXRoIGQ9Ik02NCwxNy41MzFjLTI1LjQwNSwwLTQ2LDE5LjI1OS00Niw0My4wMTVjMCwxMy41MTUsNi42NjUsMjUuNTc0LDE3LjA4OSwzMy40NnYxNi40NjIgIGwxNS42OTgtOC43MDdjNC4xODYsMS4xNzEsOC42MjEsMS44LDEzLjIxMywxLjhjMjUuNDA1LDAsNDYtMTkuMjU4LDQ2LTQzLjAxNUMxMTAsMzYuNzksODkuNDA1LDE3LjUzMSw2NCwxNy41MzF6IE02OC44NDUsNzUuMjE0ICBMNTYuOTQ3LDYyLjg1NUwzNC4wMzUsNzUuNTI0bDI1LjEyLTI2LjY1N2wxMS44OTgsMTIuMzU5bDIyLjkxLTEyLjY3TDY4Ljg0NSw3NS4yMTR6IiBmaWxsPSIjRkZGRkZGIiBpZD0iQnViYmxlX1NoYXBlIi8+PC9zdmc+) center no-repeat #0084ff;width:60px;height:60px;text-align:center;bottom:24px;border:0;outline:0;border-radius:60px;-webkit-border-radius:60px;-moz-border-radius:60px;-ms-border-radius:60px;-o-border-radius:60px;box-shadow:0 1px 6px rgba(0,0,0,.06),0 2px 32px rgba(0,0,0,.16);-webkit-transition:box-shadow .2s ease;background-size:80%;transition:all .2s ease-in-out}.ctrlq.fb-button:focus,.ctrlq.fb-button:hover{transform:scale(1.1);box-shadow:0 2px 8px rgba(0,0,0,.09),0 4px 40px rgba(0,0,0,.24)}.fb-widget{background:#fff;z-index:2;position:fixed;width:360px;height:435px;overflow:hidden;opacity:0;bottom:0;right:24px;border-radius:6px;-o-border-radius:6px;-webkit-border-radius:6px;box-shadow:0 5px 40px rgba(0,0,0,.16);-webkit-box-shadow:0 5px 40px rgba(0,0,0,.16);-moz-box-shadow:0 5px 40px rgba(0,0,0,.16);-o-box-shadow:0 5px 40px rgba(0,0,0,.16)}.fb-credit{text-align:center;margin-top:8px}.fb-credit a{transition:none;color:#bec2c9;font-family:Helvetica,Arial,sans-serif;font-size:12px;text-decoration:none;border:0;font-weight:400}.ctrlq.fb-overlay{z-index:0;position:fixed;height:100vh;width:100vw;-webkit-transition:opacity .4s,visibility .4s;transition:opacity .4s,visibility .4s;top:0;left:0;background:rgba(0,0,0,.05);display:none}.ctrlq.fb-close{z-index:4;padding:0 6px;background:#365899;font-weight:700;font-size:11px;color:#fff;margin:8px;border-radius:3px}.ctrlq.fb-close::after{content:'x';font-family:sans-serif}</style>

        <div class="fb-livechat">
            <div class="ctrlq fb-overlay"></div>
            <div class="fb-widget">
                <div class="ctrlq fb-close"></div>
                <div class="fb-page" data-href="https://www.facebook.com/krwgshop/" data-tabs="messages" data-width="360" data-height="400" data-small-header="true" data-hide-cover="true" data-show-facepile="false">
                    <blockquote cite="https://www.facebook.com/krwgshop/" class="fb-xfbml-parse-ignore"> </blockquote>
                </div>
                <div class="fb-credit"> 
                    <a href="https://www.labnol.org/software/facebook-messenger-chat-widget/9583/" target="_blank">Facebook Chat Widget by Digital Inspiration</a>
                </div>
                <div id="fb-root"></div>
            </div>
            <a href="https://m.me/digital.inspiration" title="Send us a message on Facebook" class="ctrlq fb-button"></a> 
        </div>
        <?php
        foreach ($js as $file) {
            echo "\n\t\t";
            ?><script src="<?php echo $file; ?>"></script><?php
        } echo "\n\t";
        if (LIVE) {
            ?>
            <!-- Global Site Tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-44988445-1"></script>
            <script>
                                                            window.dataLayer = window.dataLayer || [];
                                                            function gtag() {
                                                                dataLayer.push(arguments)
                                                            }
                                                            gtag('js', new Date());
                                                            gtag('config', 'UA-44988445-1');
            </script>

            <?php
        }
        ?>
        <script type="text/javascript">var cbuser = {name: '', email: '', message: ''};var cburl = 'https://chat.bogasoftware.com/', access_token = '9YNX2HBQ6baWSxL0ZgdD';document.write('<script type="text/javascript" src="' + cburl + 'assets/cmodule-chat/js/chatbull-init.js"></' + 'script>');</script>
    </body>
</html>