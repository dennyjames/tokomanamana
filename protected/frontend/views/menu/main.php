<div class="mega-container visible-lg visible-md">
    <div class="navleft-container">
        <div class="mega-menu-title"><h3><?php echo lang('categories'); ?></h3></div>
        <div class="mega-menu-category" style="display: none;">
            <ul class="nav">
                <?php
                if ($categories) {
                    foreach ($categories->result() as $category) {
                        $subcategories = $this->category_model->get_categories($category->id);
                        ?>
                        <li class="<?php echo (!$subcategories) ? 'nosub' : ''; ?>">
                            <a href="<?php echo seo_url('catalog/categories/view/' . $category->id); ?>"><?php echo $category->name; ?></a>
                            <?php if ($subcategories) { ?>
                                <div class="wrap-popup">
                                    <ul class="menu-sub popup">
                                        <?php foreach ($subcategories->result() as $subcategory) { ?>
                                            <li>
                                                <a href="<?php echo seo_url('catalog/categories/view/' . $subcategory->id); ?>"><?php echo $subcategory->name; ?></a>
                                                <?php
                                                $subcategories2 = $this->category_model->get_categories($subcategory->id);
                                                if ($subcategories2) {
                                                    ?>
                                                    <ul class="menu-sub2">
                                                        <?php foreach ($subcategories2->result() as $subcategory2) { ?>
                                                            <li><a href="<?php echo seo_url('catalog/categories/view/' . $subcategory2->id); ?>"><?php echo $subcategory2->name; ?></a></li>
                                                        <?php } ?>
                                                    </ul>
                                                <?php } ?>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            <?php } ?>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>
        </div>
    </div>
</div>