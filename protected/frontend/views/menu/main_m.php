<nav class="navbar navbar-primary navbar-static-top hidden-lg hidden-md">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <h2 class="navbar-brand visible-xs">Menu</h2>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="<?php echo site_url(); ?>">Home</a></li>
                <?php
                $categories = $this->category_model->get_categories(0);
                if ($categories) {
                    foreach ($categories->result() as $category) {
                        $subcategories = $this->category_model->get_categories($category->id);
                        ?>
                        <li class="<?php echo ($subcategories) ? 'dropdown' : ''; ?>">
                            <a href="<?php echo seo_url('catalog/categories/view/' . $category->id); ?>" <?php echo ($subcategories) ? 'class="dropdown-toggle" data-toggle="dropdown"' : ''; ?>><?php echo $category->name; ?>  <?php echo ($subcategories) ? '<span class="fa fa-angle-down"></span>' : ''; ?></a>
                            <?php if ($subcategories) { ?>
                                <ul class="dropdown-menu">
                                    <?php foreach ($subcategories->result() as $subcategory) {
                                        $subcategories2 = $this->category_model->get_categories($subcategory->id);
                                        ?>
                                        <li class="<?php echo ($subcategories2) ? 'dropdown' : ''; ?>">
                                            <a href="<?php echo seo_url('catalog/categories/view/' . $subcategory->id); ?>" <?php echo ($subcategories2) ? 'class="dropdown-toggle" data-toggle="dropdown"' : ''; ?>><?php echo $subcategory->name; ?></a>
                                            <?php if ($subcategories2) { ?>
                                                <ul class="dropdown-menu">
                                                    <?php foreach ($subcategories2->result() as $subcategory2) { ?>
                                                        <li><a href="<?php echo seo_url('catalog/categories/view/' . $subcategory2->id); ?>"><?php echo $subcategory2->name; ?></a></li>
                                                    <?php } ?>
                                                </ul>
                                            <?php } ?>
                                        </li>
                                    <?php } ?>
                                </ul>
                            <?php } ?>
                        </li>
                        <?php
                    }
                }
                ?>
            </ul>
        </div>
    </div>
</nav>