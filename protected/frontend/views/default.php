<!DOCTYPE html>
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->

<head>
    <title><?php echo "{$title}"; ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="canonical" href="<?php echo current_url(); ?>" />
    <link rel="shortcut icon" href="<?php echo site_url('assets/frontend/images/tokomanamana.ico'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/frontend/css/jssocials-theme-classic.css');?>" />
    <meta property="og:title" content="<?php echo "{$title}"; ?>" />
    <meta property="og:site_name" content="<?php echo settings('store_name'); ?>" />
    <meta property="og:url" content="<?php echo current_url(); ?>" />

    <meta property="og:image" content="<?php echo get_image((isset($meta_image)) ? $meta_image : settings('logo')); ?>" />
    <meta name="twitter:image" content="<?php echo get_image((isset($meta_image)) ? $meta_image : settings('logo')); ?>" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@tokomanamana" />
    <meta name="twitter:creator" content="@tokomanamana" />
    <meta name="twitter:title" content="<?php echo "{$title}"; ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/frontend/css/jssocials.css'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo site_url('assets/frontend/css/jssocials-theme-flat.css'); ?>" />

    <?php if (isset($meta_description)) { ?>
        <meta name="description" content="<?php echo $meta_description; ?>" />
        <meta property="og:description" content="<?php echo $meta_description; ?>" />
        <meta property="twitter:description" content="<?php echo $meta_description; ?>" />
    <?php } ?>
    <?php if (isset($meta_keyword)) { ?>
        <meta name="keywords" content="<?php echo $meta_keyword; ?>" />
    <?php } ?>


    <?php
    foreach ($css as $file) {
        echo "\n    ";
        echo '<link href="' . $file . '" rel="stylesheet" type="text/css" />';
        // echo '<link href="' . $file . '?timestamp='.time().'" rel="stylesheet" type="text/css" />';
    }
    echo "\n";
    ?>
    <script type="text/javascript">
        var site_url = '<?php echo site_url(); ?>';
        var current_url = '<?php echo current_url(); ?>';
        <?php if($this->ion_auth->logged_in()) : ?>
            var userid_chat = '<?php echo 'c-' . $this->data['user']->id ?>';
        <?php endif; ?>
        var decimal_digit = '<?php echo settings('number_of_decimal'); ?>';
        var decimal_separator = '<?php echo settings('number_separator_decimal'); ?>';
        var thousand_separator = '<?php echo settings('number_separator_thousand'); ?>';
        var location_session = <?php echo ($this->session->has_userdata('location')) ? 'true' : 'false'; ?>;
    </script>
    <?php
    foreach ($js as $file) {
        echo "\n\t\t";
    ?>
        <script type="text/javascript" src="<?php echo $file; ?>"></script>
        <!-- <script type="text/javascript" src="<?php echo $file; ?>?timestamp=<?php echo time(); ?>"></script> -->
    <?php
    }
    echo "\n\t";
    ?>
    <script src="<?php echo site_url(); ?>assets/frontend/js/app.js"></script>
    <script src="<?php echo site_url() ?>assets/frontend/js/datepicker.min.js"></script>
    <script src="<?php echo site_url('assets/frontend/js/unveil.js') ?>"></script>
    <!-- <script src="<?php //echo site_url(); ?>assets/frontend/js/app.js?timestamp=<?php //echo time(); ?>"></script> -->
    <!-- <script src="//code.tidio.co/r7bza0srbduhtfbnhflfo3qc3grqffrg.js"></script>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b08a5a6b677991d"></script> -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-128734508-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-128734508-2');
        gtag('config', 'AW-770431596');
    </script>
    <script>
        $(document).ready(function() {
            // $(".lazyload").unveil();
            $('.lazyload', 'div').each(function() {
                // randomly show an image after between 1 and 5 seconds
                var showtimetime = Math.floor(Math.random() * (5000 - 1000 + 1) + 1000);
                var $this = this;
                setTimeout(function() {
                    //console.log($this);
                    $($this).attr('src', $($this).attr('data-src'));
                }, showtimetime);

            });


        });
    </script>
    <noscript>
        <style>
            body {
                overflow: hidden;
            }
        </style>
        <div style="background: #EEE; z-index:1000; position:fixed; top:0; bottom: 0; left: 0; right: 0;font-size: 45px;padding: 150px 15px;overflow:hidden;">
            Not Found 404
        </div>
    </noscript>
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" integrity="sha384-HIipfSYbpCkh5/1V87AWAeR5SUrNiewznrUrtNz1ux4uneLhsAKzv/0FnMbj3m6g" crossorigin="anonymous">
    <!-- <script  defer="defer" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> -->
    <script defer="defer"  src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js" integrity="sha384-222hzbb8Z8ZKe6pzP18nTSltQM3PdcAwxWKzGOKOIF+Y3bROr5n9zdQ8yTRHgQkQ" crossorigin="anonymous"></script>
    <meta name="19e7153fd0121e48ea8183ac012bcfb928ae5c85" content="1b2fe74860a0290c39b326fbcc44a40ab36432da" />

    <style type="text/css">
        .item-profile:hover {
            background: #a7c22a;
        }

        ::-webkit-scrollbar {
            width: 7px;
        }

        ::-webkit-scrollbar-track {
            background: #fff;
        }

        ::-webkit-scrollbar-thumb {
            background: #A7C22A;
            border-radius: 10px;
        }

        .nav-cart .cart-info-group a:hover .badge {
            color: white;
        }

        .nav-logo img {
            max-width: 175px;
            max-height: 60px;
            margin-left: 7px !important;
        }

        .navigation-header {
            height: 47px;
        }

        @media (max-width: 767px) {
            .navigation-header {
                height: 0px !important;
            }

            .footer .container {
                padding: 0 14px !important;
            }

            #lokasi-saya {
                font-size: 13px;
            }

            .tombol-close {
                position: absolute;
                top: 15px;
                right: 20px;
                font-size: 25px;
            }

            .carousel-indicators {
                z-index: 3 !important;
            }

            .navigation-mobile-content {
                position: fixed;
                top: 0;
                bottom: 0;
                right: 0;
                left: 0;
                background-color: white;
                z-index: 10;
                display: none;
            }

            .navigation-mobile-content .mobile-content-top {
                background-color: #a7c22a;
                height: 150px;
            }

            .navigation-mobile-content .is-mobile-login .customer {
                text-align: left;
                font-size: 20px;
                padding-left: 20px;
                padding-top: 50px;
                color: white;
            }

            .mobile-top-navigation a {
                color: white;
            }

            header .navigation-mobile-inner .navigation-mobile-content .leftnavi {
                margin: 15px 15px;
            }

            header .navigation-mobile-inner .navigation-mobile-content {
                visibility: visible;
                overflow: auto;
            }

            header .navigation-mobile-inner .navigation-mobile-content .leftnavi .is-mobile-menu-content .mobile-content-link>ul.navbar-nav {
                margin: 0;
            }

            header .navigation-mobile-inner .navigation-mobile-content .leftnavi .is-mobile-menu-content .mobile-content-link>ul.navbar-nav>li {
                width: 100%;
                border: 1px solid #ebebeb;
                margin-bottom: 2px;
                position: relative;
            }

            header .navigation-mobile-inner .navigation-mobile-content .leftnavi .is-mobile-menu-content .mobile-content-link>ul.navbar-nav>li a {
                height: 38px;
                line-height: 38px;
                display: block;
                padding: 0 20px;
                padding-right: 40px;
            }

            header .navigation-mobile-inner .navigation-mobile-content .leftnavi .is-mobile-menu-content .mobile-content-link>ul.navbar-nav>li a {
                height: 38px;
                line-height: 38px;
                display: block;
                padding: 0 20px;
                padding-right: 40px;
            }

            header .navigation-mobile-inner .navigation-mobile-content .leftnavi .is-mobile-menu-content .mobile-content-link>ul.navbar-nav>li.active>a,
            header .mobile-navigation.active .mobile-navigation-inner .mobile-navigation-content .leftnavi .is-mobile-menu-content .mobile-content-link>ul.navbar-nav>li:hover>a {
                background-color: #97c23d;
                color: #fff !important;
            }

            header .navigation-mobile-inner .navigation-mobile-content .leftnavi .is-mobile-menu-content .mobile-content-link>ul.navbar-nav>li .arrow {
                position: absolute;
                top: 0px;
                right: 0;
                z-index: 2;
                padding: 0;
                width: 38px;
                height: 38px;
                line-height: 38px;
                text-align: center;
                color: #222;
                background-color: #ddd;
                border: 0;
            }

            header .navigation-mobile-inner .navigation-mobile-content .leftnavi .is-mobile-menu-content .mobile-content-link>ul.navbar-nav>li a {
                height: 38px;
                line-height: 38px;
                display: block;
                padding: 0 20px;
                padding-right: 40px;
            }

            #tombolNavigation {
                border: none;
                margin: 0;
                padding: 0;
                width: auto;
                overflow: visible;
                background: transparent;
                color: black;
                font: inherit;
                line-height: normal;
                -webkit-font-smoothing: inherit;
                -moz-osx-font-smoothing: inherit;
                -webkit-appearance: none;
            }

            &::-moz-focus-inner {
                border: 0;
                padding: 0;
            }

            header .mobile-navigation #tombolNavigation {
                position: absolute;
                top: 8px;
                left: 0px;
                width: 40px;
                height: 40px;
                background-color: none;
                z-index: 9;
                border: none;
                color: #a7c22a;
            }

            .nav-search {
                margin-top: 10px;
                width: 90% !important;
                float: left !important;
                margin-left: 8% !important;
            }

            body {
                overflow-x: hidden;
            }

            .fix-sticky {
                margin-top: 78px !important;
            }

            .notification {
                margin-bottom: 20px !important;
            }

            .main-header {
                height: 65px !important;
            }

            .nav-bottom {
                position: fixed;
                left: 0;
                right: 0;
                bottom: -4px;
                height: 58px;
                background-color: #FFF;
                -webkit-box-shadow: 0px 1px 8px rgba(0, 0, 0, 0.5);
                -moz-box-shadow: 0px 1px 8px rgba(0, 0, 0, 0.5);
                -ms-box-shadow: 0px 1px 8px rgba(0, 0, 0, 0.5);
                -o-box-shadow: 0px 1px 8px rgba(0, 0, 0, 0.5);
                box-shadow: 0px 1px 8px rgba(0, 0, 0, 0.5);
                z-index: 9;
            }

            .nav-bottom .row {
                height: 100%;
            }

            .nav-bottom .nav-bottom-inner .nav-bottom-item {
                display: inline-block;
                margin-top: 11px;
            }

            .nav-bottom .nav-bottom-inner .nav-bottom-item .nav-bottom-link {
                font-size: 12px;
                color: #999;
            }

            .nav-bottom .nav-bottom-inner .nav-bottom-item a .nav-bottom-icon {
                font-size: 18px;
                color: #999 !important;
            }

            header .mobile-navigation #cart-button .fa {
                font-size: 18px;
                margin-top: 0px !important;
            }

            .nav-link-active {
                color: #a7c22a !important;
            }

            .nav-icon-active {
                color: #a7c22a !important;
                font-size: 18px;
            }

            header .mobile-navigation #cart-button {
                position: relative !important;
                width: auto !important;
                height: auto !important;
                font-size: 12px;
                color: #999;
            }

            header .mobile-navigation #cart-button .badge-cart {
                right: 5px !important;
                top: -9px !important;
                width: 18px !important;
                height: 18px !important;
                line-height: 19px !important;
            }

            footer {
                margin-bottom: 55px;
            }
        }

        @media (max-width: 540px) {
            .nav-search {
                width: 87% !important;
                float: left !important;
                margin-left: 11% !important;
            }
        }

        @media (max-width: 410px) {
            .nav-search {
                width: 85% !important;
                float: left !important;
                margin-left: 13% !important;
            }
        }

        @media (max-width: 330px) {
            .nav-search {
                width: 83% !important;
                float: left !important;
                margin-left: 15% !important;
            }
        }

        @media (max-width: 1023px) and (min-width: 768px) {
            body {
                overflow-x: hidden;
            }
        }

        .home_proban_wrapper {
            margin-top: -39px;
        }

        .home_blog_layout {
            margin-top: -39px;
        }

        .icon_cart {
            display: none;
        }

        header section.navigation-header .navigation_area .navigation_left .group_navbtn>a {
            padding-left: 7px !important;
        }

        section.home_blog_layout .home_blog_content {
            padding-top: 0px !important;
        }

        .fix-sticky {
            margin-top: 112px;
            display: block;
        }

        #PageContainer {
            padding-top: 1px !important;
        }

        .footer .container {
            padding: 0 21px;
        }

        .headSmallSlider {
            right: -22px;
        }

        .hssEach:nth-child(n+2) {
            display: none;
        }

        .hssEach:nth-child(1) {
            display: block;
        }

        .cart-info .cart-content .action .btn {
            border-radius: 5px;
            -webkit-transition: .5s;
            -moz-transition: .5s;
            -ms-transition: .5s;
            -o-transition: .5s;
            transition: .5s;
        }

        .error {
            color: red;
        }

        input[name=word]::placeholder {
            color: #bbb;
        }

        .search_submit:hover {
            background: #86a83d !important;
        }

        .menu-right-header li a:hover {
            background: #ccc;
        }

        .menu-right-header li a i {
            color: #666 !important;
        }

        .menu-right-header li {
            display: inline-block;
        }

        .menu-right-header .cart_icon:hover .cart-dropdown {
            display: block;
        }

        .menu-right-header .cart_icon:hover #icon_cart {
            background: #ccc;
        }

        .menu-right-header .cart_icon .cart-dropdown {
            display: none;
            position: absolute;
            background: #FFF;
            max-height: 350px;
            width: 350px;
            top: 33px;
            border-radius: 0 0 5px 5px;
            box-shadow: 0 15px 20px rgba(0, 0, 0, .175);
        }

        .carts-items {
            margin-top: 5px;
            margin-right: 15px;
            margin-left: 15px;
        }

        .cart-left .cart-image:hover {
            background: none;
            color: none;
        }

        .cart-right div a:hover {
            background: none;
        }

        .cart-dropdown .action .btn-show-all {
            background: #a7c22a !important;
            color: white !important;
            border: 1px solid #a7c22a;
            transition: .2s;
        }

        .cart-dropdown .action .btn-show-all:hover {
            background: #8ea623 !important;
            border: 1px solid #8ea623;
        }

        .menu-right-header .notification-list:hover #icon_notification {
            background: #ccc;
        }

        .menu-right-header .notification-list .notification-dropdown {
            display: none;
            position: absolute;
            background: #FFF;
            max-height: 350px;
            width: 250px;
            top: 30px;
            border-radius: 0 0 5px 5px;
            box-shadow: 0 15px 20px rgba(0, 0, 0, .175);
        }

        .menu-right-header .notification-list:hover .notification-dropdown {
            display: block;
        }

        .notification-body .row a {
            display: block;
            padding: 5px;
            border-radius: 3px;
            position: relative;
        }

        .notification-body .row a span {
            position: absolute;
            right: 10px;
        }

        .notification-body .row a:hover {
            font-weight: bold;
        }

        .notification-body .action button {
            background: #a7c22a;
            color: white;
            border: 1px solid #a7c22a;
            transition: .2s;
        }

        .notification-body .action button:hover {
            background: #8ea623 !important;
            border: 1px solid #8ea623;
        }

        .menu-right-header .profile-list .profile-dropdown {
            display: none;
            position: absolute;
            background: #FFF;
            max-height: 350px;
            width: 250px;
            top: 33px;
            border-radius: 0 0 5px 5px;
            box-shadow: 0 15px 20px rgba(0, 0, 0, .175);
            right: 0px;
        }

        .menu-right-header .profile-list:hover .profile-dropdown {
            display: block;
        }

        .menu-right-header .profile-list:hover #icon_profile {
            background: #CCC;
        }

        .profile-body .row a {
            display: block;
            padding: 5px;
            border-radius: 3px;
        }

        .profile-body .row a:hover {
            font-weight: bold;
        }

        .profile-body .row a:last-child,
        .profile-body .row a:last-child i {
            color: #d9534f !important;
        }

        .profile-body .row a:last-child:hover {
            background: #d9534f;
        }

        .profile-body .row a:last-child:hover,
        .profile-body .row a:last-child:hover i {
            color: white !important;
        }

        .badge {
            background: #d9534f !important;
        }

        .menu-right-header .btn-register {
            background: white;
            color: #A7C22A;
            border: 1px solid #A7C22A;
            font-weight: bold;
            border-radius: 3px;
            /*transition: .3s;*/
            margin-top: 2px;
            height: 35px;
            line-height: 35px;
        }

        .menu-right-header .btn-register:hover {
            background: #A7C22A;
            color: white;
        }

        .menu-right-header .btn-login {
            background: white;
            color: #A7C22A;
            border: 1px solid #A7C22A;
            font-weight: bold;
            border-radius: 3px;
            /*transition: .3s;*/
            margin-top: 2px;
            height: 35px;
            line-height: 35px;
            margin-right: 10px;
        }

        .menu-right-header .btn-login:hover {
            background: #A7C22A;
            color: white;
        }

        #search_suggest a {
            border-radius: 3px;
        }

        #search_suggest a:hover {
            background: #f5f5f5;
        }

        .carts-items .row:hover .cart-right a {
            font-weight: bold;
        }

        #modal-guest .modal-dialog .modal-content input,
        #modal-guest .modal-dialog .modal-content select {
            border-radius: 3px;
        }

        #modal-guest .modal-dialog .modal-content input:focus,
        #modal-guest .modal-dialog .modal-content select:focus {
            border-color: #A7C22A;
        }

        #modal-guest .modal-dialog .modal-content a {
            transition: .2s;
        }

        #modal-guest .modal-dialog .modal-content a:hover {
            color: #A7C22A;
            font-weight: bold;
        }

        #modal-guest .modal-dialog .modal-content button {
            border-radius: 3px;
        }

        #modal-guest .modal-dialog .modal-content .btn-success {
            border: 1px solid #A7C22A;
            color: #A7C22A !important;
            font-weight: bold;
        }

        #modal-guest .modal-dialog .modal-content .btn-primary {
            background: #A7C22A;
            border: 1px solid #A7C22A;
            font-weight: bold;
        }

        #modal-guest .modal-dialog .modal-content .btn-primary:hover {
            color: white !important;
        }

        @media (max-width: 1199px) {
            <?php if (current_url() == site_url('')) : ?>
                .group-search-cart {
                    margin-left: -15px !important;
                    width: 56% !important;
                }
                .group-search-cart .nav-search form.search {
                    width: 500px !important;
                }
            <?php else : ?>
                .group-search-cart {
                    margin-left: -15px !important;
                    width: 45% !important;
                }
                .group-search-cart .nav-search form.search {
                    width: 410px !important;
                }
            <?php endif; ?>
            .main-header-inner .nav-top .category-navigation {
                margin-top: 7px;
            }
        }

        @media (max-width: 991px) {
            <?php if (!$this->ion_auth->logged_in()) : ?>
                .menu-right-header li a {
                    padding: 0px 15px !important;
                    margin-right: 0px !important;
                }
            <?php endif; ?>
        }

        @media (max-width: 1551px) {

            .menu-right-header .cart_icon .cart-dropdown,
            .menu-right-header .notification-list .notification-dropdown {
                right: 0px !important;
            }
        }

        @media (max-width: 767px) and (min-width: 601px) {
            .nav-top-mobile .nav-top-button {
                padding-left: 5% !important;
            }
        }

        @media (max-width: 600px) and (min-width: 520px) {
            .nav-top-mobile .nav-top-button {
                padding-left: 4% !important;
            }
        }

        @media (max-width: 519px) and (min-width: 426px) {
            .nav-top-mobile .nav-top-button {
                padding-left: 3% !important;
            }
        }

        .cart-dropdown .btn-show-all:focus,
        .notification-dropdown .action .btn:focus {
            outline: none;
        }

        .cart-dropdown .btn-show-all,
        .notification-dropdown .action .btn {
            font-weight: bold;
        }

        #modal-guest .modal-content .modal-footer .btn-login:focus {
            outline: none;
            color: #FFF !important;
        }

        #modal-guest .modal-content .modal-footer .btn-exit:focus {
            outline: none;
            color: #a7c22a;
        }

        /* slick */
        #desktop-slider .slick-slide,
        .slick-slide img {
            border-radius: 7px;
        }

        #modal_register .modal-content {
            border-radius: 5px;
            border: none;
            padding: 12px;
            margin-top: 15%;
        }
        #modal_register .modal-content .modal-header h1 {
            font-weight: bold;
        }
        
        #modal_register input, #modal_guest input, #modal_guest select, #modal_register select {
            background-image:none;
            background-color:transparent;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            border: 1px solid #CCC;
            border-radius: 3px;
        }
        #modal_register input:focus {
            border-color: #A7C22A;
            background-color: none !important;
            background-image: none;
        }
        #modal_register .text_login {
            margin-top: 10px;
            margin-bottom: 10px;
        }
        #modal_register a {
            color: #A7C22A;
        }
        #modal_register a:hover {
            color: #E75A5F;
        }
        #modal_register .action {
            margin-top: 10px;
        }
        #modal_register .action button {
            width: 100%;
        }
        #modal_register .action button:focus, #modal_register .action button:focus i {
            outline: none;
            color: white;
        }
        #modal_register .action button.btn_register {
            border-radius: 3px;
            background: #A7C22A;
            border: 1px solid #A7C22A;
            text-transform: capitalize;
            font-weight: bold;
            font-size: 14px;
        }
        #modal_register .action button.btn_register:hover {
            color: white !important;
            background: #8EA623;
            border: 1px solid #8EA623;
        }
        #modal_register .action button.btn_register_facebook {
            background: #3b5998;
            border: 1px solid #3b5998;
            border-radius: 3px;
            text-transform: capitalize;
            font-weight: bold;
            font-size: 14px;
        }
        #modal_register .action button.btn_register_facebook:hover {
            background: #2c4170 !important;
            border: 1px solid #2c4170 !important;
            color: white;
        }
        #modal_register .action button.btn_register_facebook:hover i {
            color: white !important;
        }
        #modal_register .action button.btn_register_google {
            color: #444;
            background: #CCC;
            border: 1px solid #CCC;
            border-radius: 3px;
            text-transform: capitalize;
            font-weight: bold;
            font-size: 14px;
        }
        #modal_register .action button.btn_register_google:hover {
            background: #B3B3B3;
            border: 1px solid #B3B3B3;
        }
        #modal_register .action button.btn_register_google i {
            color: #444;
        }
        #modal_register .action .col-md-3 {
            padding-left: 0px;
        }
        #modal_register .captcha_register_image img {
            /*width: 100%;*/
            border-radius: 3px;
        }
        #modal_register .modal-header {
            padding-bottom: 0px;
        }
        #modal_register .modal-body {
            padding: 0 15px 15px 15px;
        }
        #modal_register .error_validation span {
            color: #D9534F;
        }
        #modal_register button#button_phone_verification {
            height: 34px;
            width: 100%;
            border: 1px solid #CCC;
            background: #CCC;
            border-radius: 3px;
            font-weight: bold;
            font-size: 12px;
            color: #444;
        }
        #modal_register button#button_phone_verification:focus {
            outline: none;
        }
        #modal_register button#button_phone_verification:hover {
            background: #B3B3B3;
            border: 1px solid #B3B3B3;
        }
        #modal_register .error_register_validation .alert {
            border-radius: 3px;
        }
        #modal_register .send_otp_success .alert {
            border-radius: 3px;
        }
        #modal_register input:active, #modal_register input:focus, #modal_register select:active, #modal_register select:focus {
            border: 1px solid #A7C22A !important;
        }



        #modal_login .modal-content {
            border-radius: 5px;
            border: none;
            padding: 12px;
            margin-top: 20%;
        }
        #modal_login .modal-content .modal-header h1 {
            font-weight: bold;
        }
        
        #modal_login input, #modal_guest input, #modal_guest select, #modal_login select {
            background-image:none;
            background-color:transparent;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            border: 1px solid #CCC;
            border-radius: 3px;
        }
        #modal_login input:focus {
            border-color: #A7C22A;
            background-color: none !important;
            background-image: none;
        }
        #modal_login .text_login {
            margin-top: 10px;
            margin-bottom: 10px;
        }
        #modal_login a {
            color: #A7C22A;
        }
        #modal_login a:hover {
            color: #E75A5F;
        }
        #modal_login .action {
            margin-top: 10px;
        }
        #modal_login .action button {
            width: 100%;
        }
        #modal_login .action button:focus, #modal_login .action button:focus i {
            outline: none;
            color: white;
        }
        #modal_login .action button.btn_register {
            border-radius: 3px;
            background: #A7C22A;
            border: 1px solid #A7C22A;
            text-transform: capitalize;
            font-weight: bold;
            font-size: 14px;
        }
        #modal_login .action button.btn_register:hover {
            color: white !important;
            background: #8EA623;
            border: 1px solid #8EA623;
        }

        #modal_login .action button.btn_register_merchant {
            border-radius: 3px;
            background: #4B473B;
            border: 1px solid #4B473B;
            text-transform: capitalize;
            font-weight: bold;
            font-size: 14px;
        }
        #modal_login .action button.btn_register_merchant:hover {
            color: white !important;
            background: #333027;
            border: 1px solid #333027;
        }
        #modal_login .action button.btn_register_facebook {
            background: #3b5998;
            border: 1px solid #3b5998;
            border-radius: 3px;
            text-transform: capitalize;
            font-weight: bold;
            font-size: 14px;
        }
        #modal_login .action button.btn_register_facebook:hover {
            background: #2c4170 !important;
            border: 1px solid #2c4170 !important;
            color: white;
        }
        #modal_login .action button.btn_register_facebook:hover i {
            color: white !important;
        }
        #modal_login .action button.btn_register_google {
            color: white;
            background: #db4a39;
            border: 1px solid #db4a39;
            border-radius: 3px;
            text-transform: capitalize;
            font-weight: bold;
            font-size: 14px;
        }
        #modal_login .action button.btn_register_google:hover {
            background: #bb3222;
            border: 1px solid #bb3222;
        }
        #modal_login .action button.btn_register_google i {
            color: white;
        }
        #modal_login .action .col-md-2 {
            /*padding-left: 0px;*/
            padding: 0 5px;
        }
        #modal_login .captcha_register_image img {
            /*width: 100%;*/
            border-radius: 3px;
        }
        #modal_login .modal-header {
            padding-bottom: 0px;
        }
        #modal_login .modal-body {
            padding: 15px;
            margin-top: -10px;
        }
        #modal_login .modal-body-merchant {
            box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.3);
            border-radius: 5px;
            margin-right: 15px;
        }
        #modal_login .error_validation span {
            color: #D9534F;
        }
        #modal_login .alert {
            border-radius: 5px;
        }
        #modal_login button#button_login_verification {
            height: 34px;
            width: 100%;
            border: 1px solid #CCC;
            background: #CCC;
            border-radius: 3px;
            font-weight: bold;
            font-size: 12px;
            color: #444;
        }
        #modal_login button#button_login_verification:focus {
            outline: none;
        }
        #modal_login button#button_login_verification:hover {
            background: #B3B3B3;
            border: 1px solid #B3B3B3;
        }
        #modal_login .error_validation_login .alert, #modal_login .send_otp_success .alert {
            border-radius: 3px;
        }
        #modal_login input:active, #modal_login input:focus, #modal_login select:active, #modal_login select:focus {
            border: 1px solid #A7C22A !important;
        }
        .notification_profile {
            display: block;
            width: 100%;
            height: 70px;
            background: white;
            -webkit-box-shadow: rgb(219, 222, 226) 0px 2px 4px 2px;
            -moz-box-shadow: rgb(219, 222, 226) 0px 2px 4px 2px;
            -ms-box-shadow: rgb(219, 222, 226) 0px 2px 4px 2px;
            -o-box-shadow: rgb(219, 222, 226) 0px 2px 4px 2px;
            box-shadow: rgb(219, 222, 226) 0px 2px 4px 2px;
            padding: 25px 80px 25px 70px;
        }
        .notification_profile div {
            display: inline-block;
        }
        .notification_profile .notification_right {
            float: right;
        }
        .notification_profile .notification_left .notification_profile_text {
            font-weight: bold;
            margin-right: 5px;
        }
        .notification_profile .notification_left .notification_progress_bar {
            width: 30%;
            background: #CCC;
            display: inline-block;
            border-radius: 10px;
        }
        .notification_profile .notification_left .notification_progress_bar .progress_bar {
            background: #a7c22a;
            text-align: right;
            padding-top: 1px;
            padding-bottom: 1px;
            color: white;
            border-radius: 10px;
            height: 100%;
            padding-right: 5px;
            font-weight: bold;
        }
        .notification_profile .notification_left {
            float: left;
            width: 80%;
        }
        .notification_profile .notification_right .profile_button {
            background: #A7C22A;
            border: 1px solid #a7c22a;
            color: white;
            padding: 10px 50px;
            margin-top: -10px;
            font-weight: bold;
            border-radius: 5px;
        }
        .notification_profile .notification_right .profile_button:hover {
            background: #8EA623;
            border: 1px solid #8EA623;
        }

        @media (max-width: 1076px) {
            .notification_profile .notification_left {
                width: 70%;
            }
            .notification_profile .notification_left .notification_progress_bar {
                width: 35%;
            }
            .notification_profile .notification_left .notification_profile_text {
                font-size: 11px;
            }
        }

        @media (max-width: 877px) {
            .notification_profile .notification_left .notification_profile_text {
                font-size: 10px;
            }
            .notification_profile .notification_right .profile_button {
                padding: 7px 40px;
            }
        }

        @media(max-width: 812px) {
            .notification_profile .notification_left {
                width: 77%;
            }
        }

        #modal_verification .modal-content {
            border-radius: 5px;
            border: none;
            padding: 12px;
        }
        #modal_verification .modal-content .modal-header h1 {
            font-weight: bold;
        }
        
        #modal_verification input, #modal_guest input, #modal_guest select, #modal_verification select {
            background-image:none;
            background-color:transparent;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            border: 1px solid #CCC;
            border-radius: 3px;
        }
        #modal_verification input:focus {
            border-color: #A7C22A;
            background-color: none !important;
            background-image: none;
        }
        #modal_verification .text_login {
            margin-top: 10px;
            margin-bottom: 10px;
        }
        #modal_verification a {
            color: #A7C22A;
        }
        #modal_verification a:hover {
            color: #E75A5F;
        }
        #modal_verification .action {
            margin-top: 10px;
        }
        #modal_verification .action button {
            width: 100%;
        }
        #modal_verification .action button:focus, #modal_verification .action button:focus i {
            outline: none;
            color: white;
        }
        #modal_verification .action button.btn_register {
            border-radius: 3px;
            background: #A7C22A;
            border: 1px solid #A7C22A;
            text-transform: capitalize;
            font-weight: bold;
            font-size: 14px;
        }
        #modal_verification .action button.btn_register:hover {
            color: white !important;
            background: #8EA623;
            border: 1px solid #8EA623;
        }
        #modal_verification .action button.btn_register_facebook {
            background: #3b5998;
            border: 1px solid #3b5998;
            border-radius: 3px;
            text-transform: capitalize;
            font-weight: bold;
            font-size: 14px;
        }
        #modal_verification .action button.btn_register_facebook:hover {
            background: #2c4170 !important;
            border: 1px solid #2c4170 !important;
            color: white;
        }
        #modal_verification .action button.btn_register_facebook:hover i {
            color: white !important;
        }
        #modal_verification .action button.btn_register_google {
            color: #444;
            background: #CCC;
            border: 1px solid #CCC;
            border-radius: 3px;
            text-transform: capitalize;
            font-weight: bold;
            font-size: 14px;
        }
        #modal_verification .action button.btn_register_google:hover {
            background: #B3B3B3;
            border: 1px solid #B3B3B3;
        }
        #modal_verification .action button.btn_register_google i {
            color: #444;
        }
        #modal_verification .action .col-md-3 {
            padding-left: 0px;
        }
        #modal_verification .captcha_register_image img {
            /*width: 100%;*/
            border-radius: 3px;
        }
        #modal_verification .modal-header {
            padding-bottom: 0px;
        }
        #modal_verification .modal-body {
            padding: 0 15px 15px 15px;
        }
        #modal_verification .error_validation span {
            color: #D9534F;
        }
        #modal_verification button#button_login_verification {
            height: 34px;
            width: 100%;
            border: 1px solid #CCC;
            background: #CCC;
            border-radius: 3px;
            font-weight: bold;
            font-size: 12px;
            color: #444;
        }
        #modal_verification button#button_login_verification:focus {
            outline: none;
        }
        #modal_verification button#button_login_verification:hover {
            background: #B3B3B3;
            border: 1px solid #B3B3B3;
        }
        #modal_verification .error_validation_login .alert, #modal_verification .send_otp_success .alert {
            border-radius: 3px;
        }
        #modal_verification input:active, #modal_verification input:focus, #modal_verification select:active, #modal_verification select:focus {
            border: 1px solid #A7C22A !important;
        }
        #modal_verification .modal-footer .btn {
            border-radius: 3px;
            font-weight: 400;
        }
        #modal_verification .modal-footer .btn:focus {
            outline: 0;
            border: none;
        }
        #modal_verification .modal-footer .btn_next {
            background: #a7c22a;
            border: 1px solid #a7c22a;
            color: #fff
        }
        #modal_verification .modal-footer .btn_next:hover {
            background: #8ea623;
            border: 1px solid #8ea623
        }
        #modal_verification .btn_cancel {
            background: #fff!important;
            border: 1px solid #ccc!important;
            color: #888!important
        }

        #modal_verification .btn_cancel:hover {
            background-color: #eee!important
        }
        #modal_verification .alert {
            border-radius: 3px;
        }
        @media (max-width: 991px) {
            <?php if(current_url() == site_url('')) : ?>
                .main-header-inner .group-search-cart {
                    width: 50% !important;
                }
                header section.main-header .nav-top .nav-search form.search {
                    width: 360px !important;
                }
            <?php else : ?>
                .main-header-inner .group-search-cart {
                    width: 35% !important;
                }
                header section.main-header .nav-top .nav-search form.search {
                    width: 260px !important;
                }
            <?php endif; ?>
            header section.main-header .nav-top .nav-search {
                margin-right: -5px;
            }
            header section.main-header .nav-top .nav-search {
                padding-left: 0px;
            }
            .nav-logo img {
                max-width: 115px !important;
                max-height: 60px;
                margin-left: 0 !important;
            }
            .main-header-inner .nav-top .category-navigation {
                padding: 10px 10px;
            }
        }
        #modal_register .btn_pin_address {
            border-radius: 3px;
            background: #A7C22A;
            border: 1px solid #A7C22A;
            text-transform: capitalize;
            font-weight: bold;
            font-size: 14px;
            display: inline-block;float: right;
        }
        #modal_register .btn_pin_address:focus {
            outline: none;
            color: white;
        }
        #modal_register .btn_pin_address:hover {
            background: #8EA623;
            color: white;
            border: 1px solid #8EA623;
        }
        #modal_register .btn_pin_address:hover i,  #modal_register .btn_pin_address:focus i{
            color: white;
        }

        #modal_set_address .modal-content {
            border-radius: 5px;
            border: none;
            padding: 12px;
        }
        #modal_set_address .modal-content .modal-header h1 {
            font-weight: bold;
        }
        #modal_set_address .modal-footer .btn {
            border-radius: 3px;
            font-weight: 500;
        }
        #modal_set_address .modal-footer .btn:focus {
            outline: 0;
        }
        #modal_set_address .modal-footer .btn_save {
            background: #a7c22a;
            border: 1px solid #a7c22a;
            color: #fff
        }
        #modal_set_address .modal-footer .btn_save:hover {
            background: #8ea623;
            border: 1px solid #8ea623
        }
        #modal_set_address .btn_cancel {
            background: #fff!important;
            border: 1px solid #ccc!important;
            color: #888!important
        }

        #modal_set_address .btn_cancel:hover {
            background-color: #eee!important
        }
        #modal_set_address #modal_set_address_body {
            padding-top: 0px;
            margin-top: -15px;
        }
        #modal_set_address input {
            background-image:none;
            background-color:transparent;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            border: 1px solid #CCC;
            border-radius: 3px;
            margin-bottom: 15px;
        }
        #modal_set_address input:focus {
            border-color: #A7C22A;
            background-color: none !important;
            background-image: none;
        }
        #modal_set_address .alert {
            border-radius: 5px;
        }
        @media (max-width: 386px) {
            #modal_set_address .modal-footer button.btn {
                padding: 0px 10px !important;
                font-size: 11px !important;
            }
        }
        #modal_login #myTab li.active a {
            background: #A7C22A;
            color: #FFF;
            border: 1px solid #A7C22A;
        }
        #modal_login {
            width: 780px;
        }
        @media(max-width: 991px) {
            #modal_login {
                width: 800px;
            }
        }
        @media(max-width: 815px) {
            #modal_login {
                width: 730px;
            }
        }
        #modal_login .modal-body h1 {
            font-weight: bold;
        }

        <?php if(!$this->agent->is_mobile()) : ?>
            #modal_login_merchant {
                width: 450px;
            }
        <?php endif;?>
        #modal_login_merchant .modal-content {
            border-radius: 5px;
            border: none;
            padding: 12px;
            margin-top: 20%;
        }
        #modal_login_merchant .modal-content .modal-header h1 {
            font-weight: bold;
        }
        
        #modal_login_merchant input, #modal_guest input, #modal_guest select, #modal_login_merchant select {
            background-image:none;
            background-color:transparent;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            border: 1px solid #CCC;
            border-radius: 3px;
        }
        #modal_login_merchant input:focus {
            border-color: #A7C22A;
            background-color: none !important;
            background-image: none;
        }
        #modal_login_merchant .text_login {
            margin-top: 10px;
            margin-bottom: 10px;
        }
        #modal_login_merchant a {
            color: #A7C22A;
        }
        #modal_login_merchant a:hover {
            color: #E75A5F;
        }
        #modal_login_merchant .action {
            margin-top: 10px;
        }
        #modal_login_merchant .action button {
            width: 100%;
        }
        #modal_login_merchant .action button:focus, #modal_login_merchant .action button:focus i {
            outline: none;
            color: white;
        }
        #modal_login_merchant .action button.btn_register {
            border-radius: 3px;
            background: #A7C22A;
            border: 1px solid #A7C22A;
            text-transform: capitalize;
            font-weight: bold;
            font-size: 14px;
        }
        #modal_login_merchant .action button.btn_register:hover {
            color: white !important;
            background: #8EA623;
            border: 1px solid #8EA623;
        }

        #modal_login_merchant .action button.btn_register_merchant {
            border-radius: 3px;
            background: #4B473B;
            border: 1px solid #4B473B;
            text-transform: capitalize;
            font-weight: bold;
            font-size: 14px;
        }
        #modal_login_merchant .action button.btn_register_merchant:hover {
            color: white !important;
            background: #333027;
            border: 1px solid #333027;
        }
        #modal_login_merchant .action button.btn_register_facebook {
            background: #3b5998;
            border: 1px solid #3b5998;
            border-radius: 3px;
            text-transform: capitalize;
            font-weight: bold;
            font-size: 14px;
        }
        #modal_login_merchant .action button.btn_register_facebook:hover {
            background: #2c4170 !important;
            border: 1px solid #2c4170 !important;
            color: white;
        }
        #modal_login_merchant .action button.btn_register_facebook:hover i {
            color: white !important;
        }
        #modal_login_merchant .action button.btn_register_google {
            color: white;
            background: #db4a39;
            border: 1px solid #db4a39;
            border-radius: 3px;
            text-transform: capitalize;
            font-weight: bold;
            font-size: 14px;
        }
        #modal_login_merchant .action button.btn_register_google:hover {
            background: #bb3222;
            border: 1px solid #bb3222;
        }
        #modal_login_merchant .action button.btn_register_google i {
            color: white;
        }
        #modal_login_merchant .action .col-md-2 {
            /*padding-left: 0px;*/
            padding: 0 5px;
        }
        #modal_login_merchant .captcha_register_image img {
            /*width: 100%;*/
            border-radius: 3px;
        }
        #modal_login_merchant .modal-header {
            padding-bottom: 0px;
        }
        #modal_login_merchant .modal-body {
            padding: 15px;
            margin-top: -10px;
        }
        #modal_login_merchant .modal-body-merchant {
            -webkit-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.3);
            -moz-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.3);
            -ms-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.3);
            -o-box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.3);
            box-shadow: 0px 0px 8px 0px rgba(0,0,0,0.3);
            border-radius: 5px;
            margin-right: 15px;
        }
        #modal_login_merchant .error_validation span {
            color: #D9534F;
        }
        #modal_login_merchant .alert {
            border-radius: 5px;
        }
        #modal_login_merchant button#button_login_verification {
            height: 34px;
            width: 100%;
            border: 1px solid #CCC;
            background: #CCC;
            border-radius: 3px;
            font-weight: bold;
            font-size: 12px;
            color: #444;
        }
        #modal_login_merchant button#button_login_verification:focus {
            outline: none;
        }
        #modal_login_merchant button#button_login_verification:hover {
            background: #B3B3B3;
            border: 1px solid #B3B3B3;
        }
        #modal_login_merchant .error_validation_login .alert, #modal_login_merchant .send_otp_success .alert {
            border-radius: 3px;
        }
        #modal_login_merchant input:active, #modal_login_merchant input:focus, #modal_login_merchant select:active, #modal_login_merchant select:focus {
            border: 1px solid #A7C22A !important;
        }
        .main-header-inner .nav-top .category-navigation {
            color: #333;
            margin-top: 4px;
            margin-right: 8px;
            margin-left: 8px;
            font-weight: normal;
            position: relative;
            padding: 10px 15px;
            cursor: pointer;
            /*transition: .3s;*/
        }
        .main-header-inner .nav-top .category-navigation #category_icon {
            width: 100%;
            height: 100%;
            /*padding: 10px 20px;*/
            position: relative;
            font-size: 13px;
        }
        .main-header-inner .nav-top .category-navigation:hover {
            /*font-weight: bold;*/
            color: black;
            background: #CCC;
            border-radius: 5px;
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section {
            display: none;
            position: absolute;
            background: white;
            width: 250%;
            top: 39px;
            -webkit-box-shadow: 0 15px 20px rgba(0, 0, 0, .175);
            -moz-box-shadow: 0 15px 20px rgba(0, 0, 0, .175);
            -o-box-shadow: 0 15px 20px rgba(0, 0, 0, .175);
            -ms-box-shadow: 0 15px 20px rgba(0, 0, 0, .175);
            box-shadow: 0 15px 20px rgba(0, 0, 0, .175);
            border-radius: 0 0 5px 5px;
            left: 0;
            padding: 20px 15px;
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-list {
            width: 100%;
            display: inline-block;
            /*padding: 7px 10px;*/
            /*border-bottom: 1px solid #EEE;*/
            color: #333;
            font-weight: normal;
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-list .header-link-category-list {
            width: 100%;
            display: inline-block;
            padding: 7px 10px;
            /*border-bottom: 1px solid #EEE;*/
            color: #333;
            font-weight: normal;
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-list .header-link-category-list i {
            color: #CCC;
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-list:first-child {
            /*border-top: 1px solid #EEE;*/
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-list:hover {
            background: #CCC;
            color: black;
            font-weight: bold;
            border-radius: 5px;
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-list:hover .header-link-category-list i {
            color: black;
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-list:hover .header-link-category-list {
            font-weight: bold;
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-list img {
            width: 15px;
            margin-right: 10px;
        }
        .main-header-inner .nav-top .category-navigation:hover .category-navigation-section {
            display: block;
        }
        .main-header-inner .nav-top .category-navigation:hover #category_icon i {
            transform: rotate(180deg);
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-child-section {
            position: absolute;
            top: 0%;
            z-index: 3;
            padding: 20px 15px;
            background: white;
            width: 70%;
            -moz-box-shadow: 8px 0 12px 0px rgba(0,0,0,0.1);
            -ms-box-shadow: 8px 0 12px 0px rgba(0,0,0,0.1);
            -o-box-shadow: 8px 0 12px 0px rgba(0,0,0,0.1);
            -webkit-box-shadow: 8px 0 12px 0px rgba(0,0,0,0.1);
            box-shadow: 8px 0 12px 0px rgba(0,0,0,0.1);
            border-radius: 5px;
            display: none;
            height: 264px;
            overflow: hidden;
            width: 100%;
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-child-section .header-category-child-inner-list {
            width: 100%;
            height: 100%;
            display: inline-block;
            overflow: auto;
            padding-right: 0px;
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-child-section .header-category-child-inner-list::-webkit-scrollbar {
            width: 4px;
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-list:hover .header-category-child-section {
            -webkit-display: inline-block;
            -moz-display: inline-block;
            -ms-display: inline-block;
            -o-display: inline-block;
            display: inline-block;
            cursor: normal;
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-child-section .header-category-child-list {
            color: #333;
            border: none;
            font-weight: normal;
            border-radius: 5px;
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-child-section .header-category-child-list-see-all {
            color: #333;
            border: none;
            font-weight: normal;
            border-radius: 5px;
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-child-section .header-link-category-child-list-see-all {
            padding: 7px 10px;
            border: none;
            font-weight: normal;
            display: inline-block;
            width: 100%;
            text-align: center;
            background: #a7c22a;
            margin-top: 10px;
            border-radius: 5px;
            color: white;
            font-weight: bold;
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-child-section .header-link-category-child-list-see-all:hover {
            background: #8EA623;
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-child-section .header-link-category-child-list {
            padding: 6px 10px;
            border: none;
            font-weight: normal;
            display: inline-block;
            width: 100%;
        }
        .main-header-inner .nav-top .category-navigation .category-navigation-section .header-category-child-section .header-category-child-list .header-link-category-child-list:hover {
            background: #CCC;
            font-weight: bold;
            /*color: #E75A5F;*/
            border-radius: 5px;
        }
        @media (max-height: 772px) {
            #modal_login .modal-content {
                margin-top: 10% !important;
            }
        }
    </style>

</head>

<body class="index-template sarahmarket_1" style="background-color: #fff;">
    <header id="top" class="header clearfix">
        <div id="shopify-section-theme-header" class="shopify-section" style="position: fixed !important;top: 0 !important;right: 0 !important;left: 0 !important;background: #fff !important;-webkit-box-shadow: 0px -4px 9px rgba(0,0,0,0.5);-ms-box-shadow: 0px -4px 9px rgba(0,0,0,0.5);-moz-box-shadow: 0px -4px 9px rgba(0,0,0,0.5);-o-box-shadow: 0px -4px 9px rgba(0,0,0,0.5);box-shadow: 0px -4px 9px rgba(0,0,0,0.5);">
            <div data-section-id="theme-header" data-section-type="header-section">
                <section class="top-header">
                    <div class="top-header-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="top-header-inner">
                                    <ul class="unstyled top-menu">
                                        <li class="nav-item">
                                            <a href="<?php echo seo_url('pages/view/13'); ?>">
                                                <span>Punya Toko ?</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="<?php echo seo_url('pages/view/90'); ?>">
                                                <span>Punya Merek ?</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="<?php echo seo_url('pages/view/4'); ?>">
                                                <span>Menjadi Penjual</span>
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="<?php echo seo_url('pages/view/5'); ?>">
                                                <span>Bantuan</span>
                                            </a>
                                        </li>
                                        <!-- <?php //if ($this->ion_auth->logged_in()) { 
                                                ?>
                                                <li class="toolbar-customer login-account">
                                                    <span id="loginButton" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="sub-dropdown1 visible-sm visible-md visible-lg"></i>
                                                        <i class="sub-dropdown visible-sm visible-md visible-lg"></i>
                                                        <a href="#"><?php //echo $user->fullname; 
                                                                    ?> <?php //echo ($total_notif) ? '<span class="badge badge-notification" style="margin-left: 5px;">' . $total_notif . '</span>' : ''; 
                                                                        ?></a>
                                                    </span>
                                                    <div class="dropdown-menu text-left" style="display: none;min-width: 180px;">
                                                        <ul class="control-container customer-accounts list-unstyled">
                                                            <li class="clearfix item-profile">
                                                                <a href="<?php //echo site_url('member/profile'); 
                                                                            ?>"><?php echo lang('account_profile'); ?></a>
                                                            </li>
                                                            <li class="clearfix item-profile">
                                                                <a href="<?php //echo site_url('member/history'); 
                                                                            ?>"><?php //echo lang('account_order_history'); 
                                                                                ?> <?php //echo ($total_active_order) ? '<span class="badge badge-notification">' . $total_active_order . '</span>' : ''; 
                                                                                    ?></a>
                                                            </li>
                                                            <?php
                                                            // $total_point = 0;

                                                            // $this->load->model('Model');

                                                            // $q_point = $this->Model->get_data('point', 'point', null, array('id_customer' => $this->session->userdata('user_id')));

                                                            // if($q_point->num_rows() > 0){
                                                            //     $total_point = $q_point->row()->point;
                                                            // }

                                                            ?>
                                                            <li class="clearfix item-profile">
                                                                <a href="<?php //echo site_url('member/point'); 
                                                                            ?>">Poin Saya <?php //echo ($total_point > 0) ? '<span class="badge badge-notification">'.$total_point.'</span>' : ''; 
                                                                                            ?></a>
                                                            </li>
                                                            <li class="clearfix item-profile">
                                                                <a href="<?php //echo site_url('member/wishlist'); 
                                                                            ?>"><?php //echo lang('account_wishlist'); 
                                                                                ?></a>
                                                            </li>
                                                            <li class="clearfix">
                                                                <a href="<?php //echo site_url('member/complain'); 
                                                                            ?>">Komplain <?php //echo ($complain_chat) ? '<span class="badge badge-notification badge-complain">' . $complain_chat . '</span>' : ''; 
                                                                                            ?></a>
                                                            </li> -->
                                        <!-- <li class="clearfix item-profile">
                                                                <a href="<?php //echo site_url('member/logout'); 
                                                                            ?>"><?php //echo lang('logout'); 
                                                                                ?></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            <?php //} else { 
                                            ?>
                                                <li class="toolbar-customer"><a href="<?php //echo site_url('member/login'); 
                                                                                        ?>">Masuk</a> <span>/</span> <a href="<?php //echo site_url('member/register'); 
                                                                                                                                ?>">Daftar</a></li>
                                            <?php //} 
                                            ?> -->

                                        <!-- <li>
                                                <div class="nav-cart " id="cart-target">
                                                    <div class="cart-info-group">
                                                        <?php //if($this->ion_auth->logged_in()) : 
                                                        ?>
                                                        <a href="<?php //echo site_url('cart'); 
                                                                    ?>" class="cart dropdown-toggle dropdown-link" data-toggle="dropdown">
                                                            <i class="sub-dropdown1 visible-sm visible-md visible-lg"></i>

                                                            <i class="sub-dropdown visible-sm visible-md visible-lg"></i>
                                                            
                                                            <div class="num-items-in-cart" style="margin-top: 2px;">
                                                                <div class="items-cart-left">
                                                                    <img class="cart_img" src="<?php //echo site_url('assets/frontend/images/bg-cart-2.png'); 
                                                                                                ?>">
                                                                    <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 20px;margin-top: 2px;"></i>
                                                                </div>

                                                                <?php
                                                                // $id_customer = $this->data['user']->id;
                                                                // $ip_address = $this->input->ip_address();
                                                                // $carts = $this->cart_model->get_all_cart($id_customer, $ip_address)->result();
                                                                // $quantity = 0;
                                                                // $grand_total = 0;
                                                                // foreach($carts as $cart) {
                                                                //     $quantity += $cart->quantity;
                                                                //     $grand_total += $cart->grand_total;
                                                                //     // var_dump($cart); die;
                                                                // }
                                                                ?>
                                                                <div class="items-cart-right" style="margin-top: -15px; margin-left: -12px;z-index: 1;">
                                                                    <span class="badge badge-cart"><?php //echo $this->cart->total_items(); 
                                                                                                    ?></span>
                                                                    <span class="badge"><?php //echo $this->cart->total_items(); 
                                                                                        ?></span>
                                                                    <span class="badge"><? //= $quantity 
                                                                                        ?></span>
                                                                </div>
                                                            </div>
                                                        </a>

                                                        <div class="dropdown-menu cart-info" style="display: none; border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; max-height: 350px;">
                                                            <div class="cart-content">
                                                                <?php //if ($this->cart->contents()) { 
                                                                ?>
                                                                    <div class="items control-container" style="max-height: 150px; overflow: auto">
                                                                        <?php
                                                                        //foreach ($this->cart->contents() as $item) {
                                                                        // SEPARATED ID INTO 2 VARIABLE, PRODUCT ID AND STORE ID
                                                                        //list($product_id, $store_id) = explode('-', $item['id']);

                                                                        //$item['store'] = $store_id;
                                                                        ?>
                                                                            <div class="row" style="padding: 10px 10px;">
                                                                                <div class="cart-left">
                                                                                    <a class="cart-image" href="<?php //echo seo_url('catalog/products/view/' . $product_id . '/' . $item['store']); 
                                                                                                                ?>">
                                                                                        <img style="width: 50px;" src="<?php //echo get_image($item['image']); 
                                                                                                                        ?>" alt="" title="">
                                                                                    </a>
                                                                                </div>
                                                                                <div class="cart-right">
                                                                                    <div class="cart-title">
                                                                                        <a href="<?php //echo seo_url('catalog/products/view/' . $product_id . '/' . $item['store']); 
                                                                                                    ?>" style="color:#000;"><?php //echo $item['name']; 
                                                                                                                            ?></a>
                                                                                    </div>
                                                                                    <div class="cart-price">
                                                                                        <span class="money"><?php //echo rupiah($item['price']); 
                                                                                                            ?></span><span class="x"> x <?php //echo number($item['qty']); 
                                                                                                                                        ?></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        <?php //} 
                                                                        ?>
                                                                    </div>
                                                                    <div class="subtotal" style="border-top: 1px solid #EBEBEB">
                                                                        <span>Subtotal:</span><span class="cart-total-right money"><?php //echo rupiah($this->cart->total()); 
                                                                                                                                    ?></span>
                                                                    </div>
                                                                    <div class="action">
                                                                        <button class="btn" style="font-size:10px; width: 100%;" onclick="window.location = '<?php //echo site_url('cart'); 
                                                                                                                                                                ?>'">Lihat Semua</button>
                                                                    </div>
                                                                <?php //} else //{ 
                                                                ?>
                                                                   <div class="items control-container">
                                                                        <center><p style="font-size: 13px; font-weight: bold; padding: 15px;">Keranjang belanja Anda kosong. Yuk, belanja sekarang!</p>
                                                                        </center>
                                                                    </div>
                                                                <?php //} 
                                                                ?>
                                                                <?php //if($carts) : 
                                                                ?>
                                                                    <div class="items control-container" style="max-height: 150px; overflow: auto">
                                                                    <?php //foreach($carts as $cart) : 
                                                                    ?>
                                                                        <?php
                                                                        // $product = $this->main->get('products', ['id' => $cart->product_id]);
                                                                        // if($product->store_type == 'merchant') {
                                                                        //     $product_quantity = $product->quantity;
                                                                        // } else {
                                                                        //     $get_product_principal = $this->main->get('products_principal_stock', ['product_id' => $product->id, 'branch_id' => $cart->merchant_id]);
                                                                        //     $product_quantity = $get_product_principal->quantity;
                                                                        // }
                                                                        ?>
                                                                        <div class="row" id="cart-header-<? //= $cart->id 
                                                                                                            ?>" style="padding: 10px 10px;">
                                                                            <a class="cart-image" href="<?php //echo seo_url('catalog/products/view/' . $cart->product_id . '/' . $cart->merchant_id); 
                                                                                                        ?>">
                                                                                <img style="width: 50px;" src="<?php //echo get_image($cart->image); 
                                                                                                                ?>" alt="" title="">
                                                                            </a>
                                                                            <div class="cart-right">
                                                                                <div class="">
                                                                                    <a href="<?php //echo seo_url('catalog/products/view/' . $cart->product_id . '/' . $cart->merchant_id); 
                                                                                                ?>" style="color:#000;">
                                                                                        <? //= $cart->name 
                                                                                        ?>
                                                                                    </a>
                                                                                </div>
                                                                                <div class="cart-price">
                                                                                    <span class="money">
                                                                                        <?php //echo rupiah($cart->price); 
                                                                                        ?>
                                                                                    </span>
                                                                                    <span class="x"> 
                                                                                        x <?php //echo number($cart->quantity); 
                                                                                            ?>
                                                                                    </span>
                                                                                </div>
                                                                                <?php //if($product_quantity == 0) : 
                                                                                ?>
                                                                                    <div style="float:right">
                                                                                        <span style="color:red;">Stok produk habis!</span>
                                                                                    </div>
                                                                                <?php //endif; 
                                                                                ?>
                                                                            </div>
                                                                        </div>
                                                                    <?php //endforeach; 
                                                                    ?>
                                                                    </div>
                                                                    <div class="subtotal" style="border-top: 1px solid #EBEBEB">
                                                                        <span>Subtotal:</span><span class="cart-total-right money"><?php //echo rupiah($grand_total); 
                                                                                                                                    ?></span>
                                                                    </div>
                                                                    <div class="action">
                                                                        <button class="btn" style="font-size:10px; width: 100%;" onclick="window.location = '<?php //echo site_url('cart'); 
                                                                                                                                                                ?>'">Lihat Semua</button>
                                                                    </div>
                                                                <?php //else : 
                                                                ?>
                                                                    <div class="items control-container">
                                                                        <center><p style="font-size: 13px; font-weight: bold; padding: 15px;">Keranjang belanja Anda kosong. Yuk, belanja sekarang!</p>
                                                                        </center>
                                                                    </div>
                                                                <?php //endif; 
                                                                ?>
                                                            </div>
                                                        </div>
                                                    <?php //else : 
                                                    ?>
                                                        <a href="javascript:void(0)" class="cart dropdown-toggle dropdown-link" data-toggle="dropdown">
                                                            <i class="sub-dropdown1 visible-sm visible-md visible-lg"></i>

                                                            <i class="sub-dropdown visible-sm visible-md visible-lg"></i>
                                                            
                                                            <div class="num-items-in-cart" style="margin-top: 2px;">
                                                                <div class="items-cart-left">
                                                                    <img class="cart_img" src="<?php //echo site_url('assets/frontend/images/bg-cart-2.png'); 
                                                                                                ?>">
                                                                    <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 20px;margin-top: 2px;"></i>
                                                                </div>

                                                                <div class="items-cart-right" style="margin-top: -15px; margin-left: -12px;z-index: 1;">
                                                                    <span class="badge badge-cart"><?php //echo $this->cart->total_items(); 
                                                                                                    ?></span>
                                                                    <span class="badge"><?php //echo $this->cart->total_items(); 
                                                                                        ?></span>
                                                                    <span class="badge">0</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div class="dropdown-menu cart-info" style="display: none; border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; max-height: 350px;">
                                                            <div class="cart-content">
                                                                <div class="items control-container">
                                                                    <center><p style="font-size: 13px; font-weight: bold; padding: 15px;">Keranjang belanja Anda kosong. Yuk, belanja sekarang!</p>
                                                                    </center>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php //endif; 
                                                    ?>
                                                    </div>
                                                </div>

                                            </li> -->
                                        <!-- <li class="nav-item active" style="margin-right: 7px !important;">
                                                <a href="javascript:my_location()">
                                                    <span><i class="fa fa-icon fa-map-marker"></i> Lokasi saya</span>
                                                </a>
                                            </li> -->
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="main-header" style="height: 65px;">
                    <div class="main-header-wrapper">
                        <div class="container clearfix">
                            <div class="row">
                                <div class="main-header-inner">
                                    <div class="nav-top" style="margin-top:-4px;">
                                        <div class="nav-logo">
                                            <a href="<?php echo site_url(); ?>"><img src="<?php echo get_image(settings('logo')); ?>" alt="Logo <?php echo settings('store_name'); ?>" title="<?php echo settings('store_name'); ?>"></a>
                                            <h1 style="display:none"><a href="<?php echo site_url(); ?>"><?php echo settings('store_name'); ?></a></h1>
                                        </div>
                                        <?php if(current_url() != site_url('')) : ?>
                                            <div class="category-navigation">
                                                <a href="javascript:void(0)" id="category_icon">
                                                    Kategori &nbsp; <i class="fa fa-caret-down" aria-hidden="true" style="font-size: 15px"></i>
                                                </a>
                                               <div class="category-navigation-section">
                                                    <ul class="header-category-list-inner">
                                                        <?php foreach($categories->result() as $category) : ?>
                                                            <?php if($category->active == 1 && $category->name != 'Promo' && $category->home_show == 1) : ?>
                                                                <?php $subs = $this->main->gets('categories', ['parent' => $category->id, 'active' => 1]) ?>
                                                                <?php //$subs = $this->main->gets_paging('categories', 0, 6, ['parent' => $category->id, 'active' => 1]) ?>
                                                                <?php if($subs) : ?>
                                                                    <li class="header-category-list">
                                                                        <a href="<?php echo seo_url('catalog/categories/view/' . $category->id) ?>" class="header-link-category-list">
                                                                            <img src="<?= get_image($category->icon) ?>">
                                                                            <?= $category->name ?>
                                                                            <i class="fa fa-caret-right" aria-hidden="true" style="float: right;padding-top: 3px;"></i>
                                                                        </a>
                                                                        <ul class="header-category-child-section">
                                                                            <div class="header-category-child-inner-list">
                                                                                <?php foreach($subs->result() as $key => $sub) : ?>
                                                                                    <li class="header-category-child-list">
                                                                                        <a href="<?php echo seo_url('catalog/categories/view/' . $sub->id) ?>" class="header-link-category-child-list">
                                                                                            <?php echo $sub->name;  ?>
                                                                                        </a>
                                                                                    </li>
                                                                                <?php endforeach; ?>
                                                                                <!-- <?php if($subs->num_rows() == 6) : ?>
                                                                                    <li class="header-category-child-list-see-all">
                                                                                        <a href="<?php echo seo_url('catalog/categories/view/' . $category->id) ?>" class="header-link-category-child-list-see-all">
                                                                                            Lihat Semua
                                                                                        </a>
                                                                                    </li>
                                                                                <?php endif; ?> -->
                                                                            </div>
                                                                        </ul>
                                                                    </li>
                                                                <?php endif; ?>
                                                            <?php endif; ?>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                </div> 
                                            </div>
                                        <?php endif; ?>
                                        <div class="group-search-cart" style="<?php echo (current_url() == site_url('')) ? 'width: 60%;margin-top:3px;margin-left:-15px;' : 'width: 54%;margin-top:4px;margin-left:-15px;' ; ?>">
                                            <?php if(!$this->agent->is_mobile()) : ?>
                                                <div class="nav-search" style="border-left:none;border:1px solid #A7C22A;border-radius:3px;">
                                                    <form class="search" action="<?php echo site_url('search'); ?>" style="background: none;<?php echo (current_url() == site_url('')) ? 'width: 650px;' : 'width: 605px;' ; ?>">
                                                        <input type="text" name="word" id="search_box" class="search_box" placeholder="Cari produk ..." style="height: auto;" autocomplete="off">
                                                        <button class="search_submit" type="submit" style="border-radius: 0 3px 3px 0;right:-2px;width:50px;color:#FFF;background:#A7C22A;transition:.3s;">
                                                            <i class="fa fa-search"></i>
                                                        </button>
                                                        <div id="search_suggest" style="display: none;left:0px;border:none;border-radius: 0 0 5px 5px;box-shadow:0 15px 20px rgba(0,0,0,.2);"></div>
                                                    </form>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="menu-right-header" style="margin-top: 2px;">
                                            <?php if ($this->ion_auth->logged_in()) : ?>
                                                <?php
                                                $id_customer = $this->data['user']->id;
                                                $ip_address = $this->input->ip_address();
                                                $customer = $this->main->get('customers', ['id' => $id_customer]);
                                                $carts = $this->cart_model->get_all_cart($id_customer, $ip_address)->result();
                                                $quantity = 0;
                                                $grand_total = 0;
                                                foreach ($carts as $cart) {
                                                    $quantity += $cart->quantity;
                                                    $grand_total += $cart->grand_total;
                                                }
                                                ?>
                                                <li class="cart_icon" style="position:relative;">
                                                    <a href="javascript:void(0)" id="icon_cart" style="border-radius: 5px;padding:12px 12px;position:relative;">
                                                        <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 17px;margin-top: 1px;margin-right:1px;"></i>
                                                        <span class="badge" style="position: absolute;top:0px;right:0px;font-size:11px;padding-top:3px;min-width:16px;height:16px;"><?= $quantity ?></span>
                                                    </a>
                                                    <div class="cart-dropdown">
                                                        <?php if ($carts) : ?>
                                                            <div style="max-height: 200px; overflow: auto">
                                                                <div class="carts-items">
                                                                    <?php foreach ($carts as $cart) : ?>
                                                                        <?php if($cart->option != '') : ?>
                                                                            <?php $products_option = json_decode($cart->option); ?>
                                                                            <?php foreach ($products_option as $key => $product): ?>
                                                                                <div class="row" id="cart-header-option-<?= encode($cart->id * $product->id) ?>" style="padding: 10px 10px;border-bottom:1px solid #EEE;">
                                                                                    <div class="cart-left col-md-4" style="display: inline-block;">
                                                                                        <a class="cart-image" href="<?php echo seo_url('catalog/products/view/' . $cart->product_id . '/' . $cart->merchant_id); ?>">
                                                                                            <img style="width: 50px;" src="<?php echo get_image($product->image); ?>" alt="" title="">
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">
                                                                                        <div class="">
                                                                                            <a href="<?php echo seo_url('catalog/products/view/' . $cart->product_id . '/' . $cart->merchant_id); ?>" style="color:#000;">
                                                                                                <?= $cart->name ?>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="cart-price">
                                                                                            <span class="money" style="color: #A7C22A;">
                                                                                                <?php echo rupiah($product->price); ?>
                                                                                            </span>
                                                                                            <span class="x" style="color: #A7C22A;">
                                                                                                x <?php echo number($product->quantity); ?>
                                                                                            </span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            <?php endforeach ?>
                                                                        <?php else : ?>
                                                                            <div class="row" id="cart-header-<?= encode($cart->id) ?>" style="padding: 10px 10px;border-bottom:1px solid #EEE;">
                                                                                <div class="cart-left col-md-4" style="display: inline-block;">
                                                                                    <a class="cart-image" href="<?php echo seo_url('catalog/products/view/' . $cart->product_id . '/' . $cart->merchant_id); ?>">
                                                                                        <img style="width: 50px;" src="<?php echo get_image($cart->image); ?>" alt="" title="">
                                                                                    </a>
                                                                                </div>
                                                                                <div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">
                                                                                    <div class="">
                                                                                        <a href="<?php echo seo_url('catalog/products/view/' . $cart->product_id . '/' . $cart->merchant_id); ?>" style="color:#000;">
                                                                                            <?= $cart->name ?>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="cart-price">
                                                                                        <span class="money" style="color: #A7C22A;">
                                                                                            <?php echo rupiah($cart->price); ?>
                                                                                        </span>
                                                                                        <span class="x" style="color: #A7C22A;">
                                                                                            x <?php echo number($cart->quantity); ?>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        <?php endif; ?>
                                                                    <?php endforeach; ?>
                                                                </div>
                                                            </div>
                                                            <div class="subtotal" style="color: black;padding: 15px 20px;font-size:14px;border-top:1px solid #EEE;">
                                                                <span>Subtotal:</span><span class="cart-total-right money" style="float: right;"><?php echo rupiah($grand_total); ?></span>
                                                            </div>
                                                            <div class="action" style="padding: 15px 20px;margin-top:-10px;">
                                                                <button class="btn btn-show-all" style="font-size:10px; width: 100%;border-radius:3px;" onclick="window.location = '<?php echo site_url('cart'); ?>'">Lihat Semua</button>
                                                            </div>
                                                        <?php else : ?>
                                                            <p style="font-size: 13px; font-weight: bold; padding: 15px;text-align:center;">Keranjang belanja Anda kosong. Yuk, belanja sekarang!</p>
                                                        <?php endif; ?>
                                                    </div>
                                                </li>
                                                <?php
                                                $total_orders = $this->db->query("SELECT * FROM order_invoice WHERE customer = $id_customer AND order_status <= 7")->result();
                                                $waiting_for_payment = $this->db->query("SELECT * FROM order_invoice WHERE customer = $id_customer AND order_status = 1")->num_rows();
                                                $waiting_for_confirm = $this->db->query("SELECT * FROM order_invoice WHERE customer = $id_customer AND order_status = 2")->num_rows();
                                                $order_processed = $this->db->query("SELECT * FROM order_invoice WHERE customer = $id_customer AND (order_status = 3 OR order_status = 4)")->result();
                                                $order_shipped = $this->db->query("SELECT * FROM order_invoice WHERE customer = $id_customer AND order_status = 5")->num_rows();
                                                $order_finish = $this->db->query("SELECT * FROM order_invoice WHERE customer = $id_customer AND order_status = 7")->result();

                                                $total_all_orders = 0;
                                                $total_order_finish = 0;
                                                $total_order_processed = 0;
                                                foreach ($total_orders as $total) {
                                                    $today = date('Y-m-d');
                                                    $today = date('Y-m-d', strtotime($today));
                                                    if ($total->order_status == 7) {
                                                        $date_end = date('Y-m-d', strtotime($total->due_date_finish_order));
                                                        if ($today <= $date_end) {
                                                            $total_all_orders++;
                                                        }
                                                    } elseif ($total->order_status == 3) {
                                                        if ($total->due_date != null) {
                                                            $total_all_orders++;
                                                        }
                                                    } else {
                                                        $total_all_orders++;
                                                    }
                                                }
                                                foreach ($order_finish as $total) {
                                                    $today = date('Y-m-d');
                                                    $today = date('Y-m-d', strtotime($today));
                                                    $date_end = date('Y-m-d', strtotime($total->due_date_finish_order));
                                                    if ($today <= $date_end) {
                                                        $total_order_finish++;
                                                    }
                                                }
                                                foreach ($order_processed as $total) {
                                                    if ($total->due_date != null) {
                                                        $total_order_processed++;
                                                    }
                                                }
                                                ?>
                                                <li class="notification-list" style="position: relative">
                                                    <a href="<?php echo site_url('member/history') ?>" id="icon_notification" style="border-radius: 5px;padding:12px 12px;position:relative;">
                                                        <i class="fa fa-bell" aria-hidden="true" style="font-size: 15px;margin-top: 1px;margin-right:1px;"></i>
                                                        <?php if ($total_orders) : ?>
                                                            <span class="badge" style="position: absolute;top:0px;right:0px;font-size:11px;padding-top:3px;min-width:16px;height:16px;"><?= $total_all_orders ?></span>
                                                        <?php else : ?>
                                                            <span class="badge" style="position: absolute;top:0px;right:0px;font-size:11px;padding-top:3px;min-width:16px;height:16px;">0</span>
                                                        <?php endif; ?>
                                                    </a>
                                                    <!-- <div class="notification-dropdown">
                                                        <div class="notification-header" style="padding: 5px 15px;border-bottom:1px solid #CCC;">
                                                            <h4 style="color: black;font-weight:bold;margin-bottom: 5px;margin-top: 10px;">Notifikasi</h4>
                                                        </div>
                                                        <div class="notification-body" style="color:black; padding:5px 15px;">
                                                            <div class="row" style="padding: 5px 15px;">
                                                                <a href="#"><?= lang('notification_waiting_payment') ?> <?= ($waiting_for_payment) ? '<span class="badge">' . $waiting_for_payment . '</span>' : ''; ?></a>
                                                                <a href="#"><?= lang('notification_waiting_confirm') ?> <?= ($waiting_for_confirm) ? '<span class="badge">' . $waiting_for_confirm . '</span>' : ''; ?></a></a>
                                                                <a href="#"><?= lang('notification_order_processed') ?> <?= ($total_order_processed) ? '<span class="badge">' . $total_order_processed . '</span>' : ''; ?></a></a>
                                                                <a href="#"><?= lang('notification_order_shipped') ?> <?= ($order_shipped) ? '<span class="badge">' . $order_shipped . '</span>' : ''; ?></a></a>
                                                                <a href="#"><?= lang('notification_order_finish') ?> <?= ($total_order_finish) ? '<span class="badge">' . $total_order_finish . '</span>' : ''  ?></a>
                                                            </div>
                                                            <div class="action" style="padding: 5px 0px">
                                                                <button class="btn" style="font-size:10px; width: 100%;border-radius:3px;" onclick="window.location = '<?php echo site_url('member/history'); ?>'">Lihat Riwayat Pesanan</button>
                                                            </div>
                                                        </div>
                                                    </div> -->
                                                </li>
                                                <li class="messages-list" style="position: relative">
                                                    <a href="javascript:void(0)" id="icon_message" style="border-radius: 5px;padding:12px 12px;position:relative;">
                                                        <i class="fa fa-envelope" aria-hidden="true" style="font-size: 15px;margin-top: 1px;margin-right:1px;"></i>
                                                        <span class="badge" style="position: absolute;top:0px;right:0px;font-size:11px;padding-top:3px;min-width:16px;height:16px;"></span>
                                                    </a>
                                                </li>
                                                <?php
                                                $get_wishlist = $this->main->gets('wishlist', ['customer' => $id_customer]);
                                                $get_review = $this->main->gets('order_invoice', ['customer' => $id_customer, 'order_status' => 7]);
                                                $total_reviews = 0;
                                                if ($get_review) {
                                                    foreach ($get_review->result() as $review) {
                                                        $check_review = $this->main->get('product_review', ['order_id' => $review->order, 'customer' => $review->customer, 'merchant' => $review->merchant]);
                                                        $today = date('Y-m-d');
                                                        $today = date('Y-m-d', strtotime($today));
                                                        $date_end = date('Y-m-d', strtotime($review->due_date_finish_order));
                                                        if (!$check_review) {
                                                            if ($today <= $date_end) {
                                                                $total_reviews++;
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    $total_reviews = 0;
                                                }
                                                ?>
                                                <?php 
                                                $name = explode(' ', $customer->fullname); 
                                                if(strlen($name[0]) >= 8) {
                                                    $name_top = substr($name[0], 0, 8) . '...';
                                                } else {
                                                    $name_top = $name[0];
                                                }
                                                ?>
                                                <li class="profile-list" style="position:relative">
                                                    <a href="javascript:void(0)" id="icon_profile" style="border-radius: 5px;padding:12px 12px;position:relative;font-size:13px;">
                                                        <i class="fa fa-user" aria-hidden="true" style="font-size: 17px;margin-top: 1px;margin-right:1px;"></i>&nbsp; <?= $name_top ?>
                                                    </a>
                                                    <div class="profile-dropdown">
                                                        <div class="profile-header" style="padding: 5px 15px;border-bottom:1px solid #CCC;">
                                                            <h4 style="color: black;font-weight:bold;margin-bottom: 5px;margin-top: 10px;font-size:16px;"><?= (count($name) > 1) ? $name[0] . ' ' . $name[1] : $name[0]; ?></h4>
                                                        </div>
                                                        <div class="profile-body" style="color:black; padding:5px 15px;">
                                                            <div class="row" style="padding: 5px 15px;position:relative;">
                                                                <?php if($customer->type == 'm') : ?>
                                                                    <a href="<?= site_url('ma') ?>" id="see_merchant_">&nbsp;<i class="fa fa-briefcase" aria-hidden="true"></i> &nbsp;Lihat Toko</a>
                                                                <?php endif; ?>
                                                                <a href="<?= site_url('member/profile') ?>">&nbsp;<i class="fa fa-user" aria-hidden="true"></i> &nbsp;<?php echo lang('account_profile'); ?></a>
                                                                <a href="<?= site_url('member/balance') ?>">&nbsp;<i class="fa fa-money" aria-hidden="true"></i> &nbsp;Saldo</a>
                                                                <a href="<?= site_url('member/wishlist') ?>" class="wishlist-link">&nbsp;<i class="fa fa-heart" aria-hidden="true"></i> &nbsp;<?php echo lang('account_wishlist'); ?>
                                                                    <?php if ($get_wishlist) : ?>
                                                                        <span class="badge" style="position: absolute;right:25px;"><?= $get_wishlist->num_rows(); ?></span></a>
                                                            <?php else : ?>
                                                                <span class="badge" style="position: absolute;right:25px;display:none;">0</span></a>
                                                            <?php endif; ?>
                                                            <a href="<?= site_url('member/review') ?>">&nbsp;<i class="fa fa-star" aria-hidden="true"></i> &nbsp;Ulasan
                                                                <span class="badge" style="position: absolute;right:25px;<?php echo $total_reviews == 0 ? 'display:none;' : '' ?>"><?= $total_reviews; ?></span>
                                                            </a>
                                                            <?php if($customer->type == 'm') : ?>
                                                                <a href="<?= site_url('member/logout') ?>" id="logout_merchant">&nbsp;<i class="fa fa-sign-out" aria-hidden="true"></i> &nbsp;Keluar</a>
                                                            <?php else : ?>
                                                                <a href="<?= site_url('member/logout') ?>">&nbsp;<i class="fa fa-sign-out" aria-hidden="true"></i> &nbsp;Keluar</a>
                                                            <?php endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php else : ?>
                                                <li>
                                                    <a href="#" class="btn btn-register" id="button_login">
                                                        Masuk
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn btn-register" id="button_register">
                                                        Daftar
                                                    </a>
                                                </li>
                                            <?php endif; ?>
                                        </div>

                                        <?php //if ($banner_payments = $this->main->gets('banners', array('type' => 'slider_payment', 'status' => 1))) { 
                                        ?>
                                        <!-- <div class="headSmallSlider">
                                                    <?php //foreach ($banner_payments->result() as $banner) { 
                                                    ?>
                                                        <div class="hssEach" style="height: 63px">
                                                            <img class="cart_img" src="<?php //echo get_image($banner->image); 
                                                                                        ?>"><br/>
                                                            <?php //echo $banner->name; 
                                                            ?>
                                                        </div>
                                                    <?php //} 
                                                    ?>
                                                    <div class="hssNav">
                                                        <a href="javascript:plusDivs(-1)">&#8592;</a> <span id="slide-id">1</span>/<?php //echo $banner_payments->num_rows(); 
                                                                                                                                    ?>
                                                        <a href="javascript:plusDivs(1)">&#8594;</a>
                                                    </div>

                                                </div> -->
                                        <?php //} 
                                        ?>
                                    </div>

                                    <div class="mobile-navigation" style="padding-bottom: 20px;" style="display: none;">
                                        <!-- <button id="showLeftPush" class="visible-xs"><i class="fa fa-bars fa-2x"></i></button>
                                            <div class="nav-logo visible-xs">
                                                <div><a href="#"><img src="<?php //echo site_url('assets/frontend/images/logo.png'); 
                                                                            ?>" alt="<?php //echo settings('store_name'); 
                                                                                        ?>" title="<?php //echo settings('store_name'); 
                                                                                                    ?>"></a></div>
                                            </div>
                                            <a id="cart-button" href="<?php //echo site_url('cart'); 
                                                                        ?>" class="visible-xs">
                                                <i class="fa fa-icon fa-shopping-cart"></i>
                                                <span class="badge badge-cart"><?php //echo $this->cart->total_items(); 
                                                                                ?></span>
                                            </a> -->
                                        <!-- <button id="tombolNavigation" class="visible-xs"><i class="fa fa-bars fa-2x"></i></button> -->
                                        <!-- <div class="nav-search visible-xs">
                                                <form class="search" action="<?php //echo site_url('search'); 
                                                                                ?>">
                                                    <input type="text" name="word" class="search_box" placeholder="Cari produk ..." required>
                                                    <button class="search_submit" type="submit">
                                                        <svg aria-hidden="true" role="presentation" class="icon icon-search" viewBox="0 0 37 40"><path d="M35.6 36l-9.8-9.8c4.1-5.4 3.6-13.2-1.3-18.1-5.4-5.4-14.2-5.4-19.7 0-5.4 5.4-5.4 14.2 0 19.7 2.6 2.6 6.1 4.1 9.8 4.1 3 0 5.9-1 8.3-2.8l9.8 9.8c.4.4.9.6 1.4.6s1-.2 1.4-.6c.9-.9.9-2.1.1-2.9zm-20.9-8.2c-2.6 0-5.1-1-7-2.9-3.9-3.9-3.9-10.1 0-14C9.6 9 12.2 8 14.7 8s5.1 1 7 2.9c3.9 3.9 3.9 10.1 0 14-1.9 1.9-4.4 2.9-7 2.9z"></path></svg>
                                                    </button>
                                                </form>
                                            </div> -->
                                        <!-- <div class="nav-search" style="border-left:none;border:1px solid #A7C22A;border-radius:3px;">
                                                <form class="search" action="<?php //echo site_url('search'); 
                                                                                ?>" style="background: none;">
                                                    <input type="text" name="word" id="search_box" class="search_box" placeholder="Cari produk ..." style="height: 30px;" autocomplete="off">
                                                    <button class="search_submit" type="submit" style="border-radius: 0 3px 3px 0;right:-2px;width:50px;color:#FFF;background:#A7C22A;transition:.3s;">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                    <div id="search_suggest" style="display: none;left:0px;border:none;border-radius: 0 0 5px 5px;box-shadow:0 15px 20px rgba(0,0,0,.2);"></div>
                                                </form>
                                            </div>
                                            <div class="notification-mobile">
                                                <i class="fa fa-bell" aria-hidden="true" style="font-size: 15px;margin-top: 1px;margin-right:1px;"></i>
                                            </div> -->
                                        <?php if($this->agent->is_mobile()) : ?>
                                            <div class="nav-top-mobile visible-xs" style="margin-top: -6px;">
                                                <div class="row" style="position:relative;">
                                                    <div class="nav-top-button" style="width: 10%;display: inline-block;position: absolute;top: 9px;">
                                                        <button id="tombolNavigation" class="visible-xs" style="outline:none;position:static;"><i class="fa fa-bars fa-2x"></i></button>
                                                    </div>
                                                    <div class="nav-top-search" style="width: 75%;display: inline-block;position: absolute;top: 0px;left: 13%;">
                                                        <div class="nav-search" style="border-left:none;border:1px solid #A7C22A;border-radius:3px;margin-left:0px !important;width:100% !important;">
                                                            <form class="search" action="<?php echo site_url('search'); ?>" style="background: none;">
                                                                <input type="text" name="word" id="search_box" class="search_box" placeholder="Cari produk ..." style="height: 30px;" autocomplete="off">
                                                                <button class="search_submit" type="submit" style="border-radius: 0 3px 3px 0;right:-2px;width:50px;color:#FFF;background:#A7C22A;transition:.3s;">
                                                                    <i class="fa fa-search"></i>
                                                                </button>
                                                                <div id="search_suggest" style="display: none;left:0px;border:none;border-radius: 0 0 5px 5px;box-shadow:0 15px 20px rgba(0,0,0,.2);"></div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                    <div class="nav-top-notification" style="width: 10%;display: inline-block;position: absolute;top: 16px;right: -2%;">
                                                        <?php if ($this->ion_auth->logged_in()) : ?>
                                                            <a href="javascript:void(0)">
                                                                <i class="fa fa-bell" aria-hidden="true" style="font-size: 1.6em;color: #a7c22a;position:relative;margin-top: 5px;"><span class="badge" style="position: absolute;top: -7px;right: -7px;"><?= $total_all_orders ?></span></i>
                                                            </a>
                                                        <?php else : ?>
                                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-guest">
                                                                <i class="fa fa-bell" aria-hidden="true" style="font-size: 1.6em;color: #a7c22a;position:relative;margin-top: 5px;"><span class="badge" style="position: absolute;top: -7px;right: -7px;"></span></i>
                                                            </a>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>


                                        <?php if ($this->ion_auth->logged_in()) : ?>
                                            <div class="nav-bottom visible-xs">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <div class="nav-bottom-inner text-center">
                                                                <div class="nav-bottom-item">
                                                                    <a href="<?= site_url() ?>" class="nav-bottom-link <?= site_url() == current_url() ? 'nav-link-active' : '' ?>">
                                                                        <i class="fa fa-home <?= site_url() == current_url() ? 'nav-icon-active' : 'nav-bottom-icon' ?>" aria-hidden="true"></i> <br>
                                                                        Beranda
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="nav-bottom-inner text-center">
                                                                <div class="nav-bottom-item">
                                                                    <a id="cart-button" href="<?= site_url('cart') ?>" class="nav-bottom-link <?= site_url('cart') == current_url() ? 'nav-link-active' : '' ?>">
                                                                        <i class="fa fa-shopping-cart <?= site_url('cart') == current_url() ? 'nav-icon-active' : 'nav-bottom-icon' ?>" aria-hidden="true"></i>
                                                                        <span class="badge badge-cart"><?php echo $quantity; ?></span>
                                                                        <br>
                                                                        Keranjang
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="nav-bottom-inner text-center">
                                                                <div class="nav-bottom-item">
                                                                    <a href="<?= site_url('member/profile') ?>" class="nav-bottom-link <?= site_url('member/profile') == current_url() ? 'nav-link-active' : '' ?>">

                                                                        <i class="fa fa-user <?= site_url('member/profile') == current_url() ? 'nav-icon-active' : 'nav-bottom-icon' ?>" aria-hidden="true"></i> <br>
                                                                        Akun
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="nav-bottom-inner text-center">
                                                                <div class="nav-bottom-item">
                                                                    <a href="<?= site_url('member/logout') ?>" class="nav-bottom-link">
                                                                        <i class="fa fa-sign-out nav-bottom-icon" aria-hidden="true"></i> <br>
                                                                        Keluar
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php else : ?>
                                            <div class="nav-bottom visible-xs">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <div class="nav-bottom-inner text-center">
                                                                <div class="nav-bottom-item">
                                                                    <a href="<?= site_url() ?>" class="nav-bottom-link <?= site_url() == current_url() ? 'nav-link-active' : '' ?>">
                                                                        <i class="fa fa-home <?= site_url() == current_url() ? 'nav-icon-active' : 'nav-bottom-icon' ?>" aria-hidden="true"></i> <br>
                                                                        Beranda
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="nav-bottom-inner text-center">
                                                                <div class="nav-bottom-item">
                                                                    <a id="cart-button" href="<?= site_url('cart') ?>" class="nav-bottom-link <?= site_url('cart') == current_url() ? 'nav-link-active' : '' ?>">
                                                                        <i class="fa fa-shopping-cart <?= site_url('cart') == current_url() ? 'nav-icon-active' : 'nav-bottom-icon' ?>" aria-hidden="true"></i>
                                                                        <?php if ($this->cart->total_items() > 0) : ?>
                                                                            <span class="badge badge-cart"><?php echo $this->cart->total_items(); ?></span>
                                                                        <?php else : ?>
                                                                            <span class="badge badge-cart" style="display: none;"><?php echo $this->cart->total_items(); ?></span>
                                                                        <?php endif; ?>
                                                                        <br>
                                                                        Keranjang
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="nav-bottom-inner text-center">
                                                                <div class="nav-bottom-item">
                                                                    <a href="<?= site_url('member/login') ?>" class="nav-bottom-link <?= site_url('member/login') == current_url() ? 'nav-link-active' : '' ?>">
                                                                        <i class="fa fa-sign-in <?= site_url('member/login') == current_url() ? 'nav-icon-active' : 'nav-bottom-icon' ?>" aria-hidden="true"></i> <br>
                                                                        Masuk
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <div class="nav-bottom-inner text-center">
                                                                <div class="nav-bottom-item">
                                                                    <a href="<?= site_url('member/register') ?>" class="nav-bottom-link <?= site_url('member/register') == current_url() ? 'nav-link-active' : '' ?>">
                                                                        <i class="fa fa-user-plus <?= site_url('member/register') == current_url() ? 'nav-icon-active' : 'nav-bottom-icon' ?>" aria-hidden="true"></i> <br>
                                                                        Daftar
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <div class="navigation-mobile-inner">
                                            <div class="navigation-mobile-content">
                                                <div class="mobile-top-navigation visible-xs">
                                                    <div class="mobile-content-top">
                                                        <div class="tombol-close">
                                                            <i class="fa fa-times"></i>
                                                        </div>
                                                        <div class="mobile-top-account">
                                                            <div class="is-mobile-login">
                                                                <ul class="customer">
                                                                    <?php if ($this->ion_auth->logged_in()) { ?>
                                                                        <!--  <li>
                                                                                <a href="<?php echo site_url('member/profile'); ?>"><i class="fa fa-user" aria-hidden="true"></i>
                                                                                    <span><?php echo $user->fullname; ?></span>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="<?php echo site_url('member/logout'); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i>
                                                                                    <span>Keluar</span>
                                                                                </a>
                                                                            </li> -->
                                                                            <?php 
                                                                            if($customer->image) {
                                                                                $image_picture = 'customers_image/' . $customer->image;
                                                                            } else {
                                                                                $image_picture = 'customers_image/default_image_profile.jpg';
                                                                            }
                                                                            ?>
                                                                            <span id="profile_picture_mobile">
                                                                                <img src="<?php echo site_url('files/images/' . $image_picture) ?>" style="display: inline-block;margin-top: -30px;width: 25%;
                                                                            border-radius:15px;height: all;">
                                                                            </span>
                                                                            <span style="display: inline-block;margin-bottom: -30px;margin-left: 5px;font-size: 15px;">
                                                                                <b style="font-size: 22px;"><?= $user->fullname ?></b>
                                                                                <br>
                                                                                <a href="<?php echo site_url('member/profile') ?>">Lihat Profil</a>
                                                                            </span>
                                                                    <?php } else { ?>
                                                                        <!--  <li>
                                                                                <a href="<?php echo site_url('member/login'); ?>"><i class="fa fa-sign-in" aria-hidden="true"></i>
                                                                                    <span>Masuk</span>
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="<?php echo site_url('member/register'); ?>"><i class="fa fa-user-plus" aria-hidden="true"></i>
                                                                                    <span>Daftar</span>
                                                                                </a>
                                                                            </li> -->
                                                                        <a href="<?= site_url('member/login') ?>">
                                                                            Masuk
                                                                        </a>
                                                                        /
                                                                        <a href="<?= site_url('member/register') ?>">
                                                                            Daftar
                                                                        </a>
                                                                    <?php } ?>
                                                                    <!--  <li>
                                                                            <a href="javascript:my_location();"><i class="fa fa-icon fa-map-marker" aria-hidden="true"></i>
                                                                                <span>Lokasi Saya</span>
                                                                            </a>
                                                                        </li> -->
                                                                    <br>
                                                                    <!-- <a href="javascript:my_location();" id="lokasi-saya"><i class="fa fa-icon fa-map-marker" aria-hidden="true"></i>
                                                                            <span>Set Lokasi</span>
                                                                        </a> -->
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="nav-menu visible-xs leftnavi" id="is-mobile-nav-menu">
                                                    <div class="is-mobile-menu-content">
                                                        <div class="mobile-content-link">
                                                            <ul class="nav navbar-nav hoverMenuWrapper">
                                                                <!-- <li class="nav-item">
                                                                        <a href="<?php echo site_url(); ?>">Beranda</a>
                                                                    </li> -->
                                                                <li class="nav-item  navigation_mobile">
                                                                    <a href="javascript:void(0)" class="menu-mobile-link">Lihat Kategori</a>
                                                                    <a href="javascript:void(0)" class="arrow">
                                                                        <i class="fa fa-angle-down"></i>
                                                                    </a>
                                                                    <div class="menu-mobile-container" style="display: none;">
                                                                        <ul class="sub-mega-menu">
                                                                            <?php if ($categories) foreach ($categories->result() as $category) { ?>
                                                                                <?php if ($category->name == 'Promo' || $category->name == 'promo') : ?>
                                                                                <?php else : ?>
                                                                                    <li class=" li-sub-mega">
                                                                                        <a href="<?php echo seo_url('catalog/categories/view/' . $category->id); ?>"><?php echo $category->name; ?></a>
                                                                                    </li>
                                                                                <?php endif; ?>
                                                                            <?php } ?>
                                                                        </ul>
                                                                    </div>
                                                                </li>
                                                                <!-- <?php if ($this->ion_auth->logged_in()) { ?>
                                                                        <li class="nav-item  navigation_mobile">
                                                                            <a href="javascript:void(0)" class="menu-mobile-link">Akun Saya</a>
                                                                            <a href="javascript:void(0)" class="arrow">
                                                                                <i class="fa fa-angle-down"></i>
                                                                            </a>
                                                                            <div class="menu-mobile-container" style="display: none;">
                                                                                <ul class="sub-mega-menu">
                                                                                    <li class="li-sub-mega"><a href="<?php echo site_url('member/history'); ?>"><?php echo lang('account_order_history'); ?></a></li>
                                                                                    <li class="li-sub-mega"><a href="<?php echo site_url('member/point'); ?>">Poin Saya</a></li>
                                                                                    <li class="li-sub-mega"><a href="<?php echo site_url('member/profile'); ?>"><?php echo lang('account_profile'); ?></a></li>
                                                                                    <li class="li-sub-mega"><a href="<?php echo site_url('member/wishlist'); ?>"><?php echo lang('account_wishlist'); ?></a></li>
                                                                                    <li class="li-sub-mega"><a href="<?php echo site_url('member/review'); ?>"><?php echo lang('account_review'); ?></a></li>
                                                                                    <li class="li-sub-mega"><a href="<?php echo site_url('member/address'); ?>"><?php echo lang('account_address'); ?></a></li>
                                                                                    <li class="li-sub-mega"><a href="<?php echo site_url('member/security'); ?>"><?php echo lang('account_security'); ?></a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </li>
                                                                    <?php } ?> -->
                                                                <li class="nav-item">
                                                                    <a href="<?php echo seo_url('pages/view/4'); ?>">Menjadi Penjual</a>
                                                                </li>
                                                                <li class="nav-item">
                                                                    <a href="<?php echo seo_url('pages/view/5'); ?>">Bantuan</a>
                                                                </li>
                                                                <li class="nav-item">
                                                                    <a href="<?php echo site_url('blogs'); ?>">Artikel</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- <section class="navigation-header">
                        <div class="navigation-header-wrapper">
                            <div class="container clearfix">
                                <div class="row">
                                    <div class="main-navigation-inner">
                                        <div class="navigation_area">
                                            <span>Dikirim ke</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section> -->
                <!-- <section class="navigation-header">
                        <div class="navigation-header-wrapper">
                            <div class="container clearfix">
                                <div class="row">
                                    <div class="main-navigation-inner">
                                        <div class="navigation_area">
                                            <div class="navigation_left">
                                                <div class="group_navbtn">
                                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                                        <span class="dropdown-toggle">
                                                            Kategori
                                                        </span>
                                                        <i class="fa fa-bars" aria-hidden="true"></i>
                                                    </a>
                                                    <ul class="navigation_links_left dropdown-menu" style="display: none;">
                                                        <?php //if ($categories) foreach ($categories->result() as $category) { 
                                                        ?>
                                                            <?php //if($category->name == 'Promo' || $category->name == 'promo') : 
                                                            ?>
                                                            <?php //else : 
                                                            ?>
                                                                <li class="nav-item _icon">
                                                                    <a href="<?php //echo seo_url('catalog/categories/view/' . $category->id); 
                                                                                ?>" class="parCat">
                                                                        <img src="<?php //echo get_image($category->icon); 
                                                                                    ?>">
                                                                        <span><?php //echo $category->name; 
                                                                                ?></span>
                                                                    </a>
                                                                    <?php //if ($subs = $this->main->gets_paging('categories', 0, 6, array('parent' => $category->id, 'active' => 1), 'sort_order asc')) { 
                                                                    ?>
                                                                        <div class="navSubCat">
                                                                            <?php //foreach ($subs->result() as $sub) { 
                                                                            ?>
                                                                                <a href="<?php //echo seo_url('catalog/categories/view/' . $sub->id); 
                                                                                            ?>"><?php //echo $sub->name; 
                                                                                                ?></a>
                                                                            <?php //} 
                                                                            ?>
                                                                            <?php //if ($subs->num_rows() == 6) { 
                                                                            ?>
                                                                                <a href="<?php //echo seo_url('catalog/categories/view/' . $category->id); 
                                                                                            ?>">Lihat semua</a>
                                                                            <?php //} 
                                                                            ?>
                                                                        </div>
                                                                    <?php //} 
                                                                    ?>
                                                                </li>
                                                            <?php //endif; 
                                                            ?>
                                                        <?php //} 
                                                        ?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="navigation_right">
                                                <ul class="navigation_links">
                                                    <li class="nav-item">
                                                        <a href="<?php //echo site_url(); 
                                                                    ?>">
                                                            <span>Beranda</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="<?php //echo site_url('blogs'); 
                                                                    ?>">
                                                            <span>Artikel</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="<?php //echo seo_url('pages/view/6'); 
                                                                    ?>">
                                                            <span>Cara Pembelian</span>
                                                        </a>
                                                    </li>
                                                    <li class="nav-item">
                                                        <a href="<?php //echo seo_url('pages/view/88'); 
                                                                    ?>">
                                                            <span>Karir</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="navigation_icon" id="cart-target-mobile">
                                                <div class="navigation_icon_group">
                                                    <div class="icon_cart">
                                                        <div class="cart-info-group">
                                                            <a href="<?php //echo site_url('cart'); 
                                                                        ?>" class="cart dropdown-toggle dropdown-link" data-toggle="dropdown">
                                                                <i class="sub-dropdown1 visible-sm visible-md visible-lg"></i>
                                                                <i class="sub-dropdown visible-sm visible-md visible-lg"></i>
                                                                <div class="num-items-in-cart">
                                                                    <div class="items-cart-left">
                                                                        <img class="cart_img" src="<?php //echo site_url('assets/frontend/images/bg-cart.png'); 
                                                                                                    ?>">
                                                                        <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 22px;margin-right: -5px;"></i>
                                                                       <?php //if ($this->cart->contents()) : 
                                                                        ?>
                                                                            <span class="cart_text icon badge number" style="margin-top: -35px;font-size: 13px;line-height: 16px;width: auto; height: 20px;"><?php //echo $this->cart->total_items(); 
                                                                                                                                                                                                                ?></span>
                                                                        <?php //else : 
                                                                        ?>
                                                                            <span class="cart_text icon badge number" style="margin-top: -35px;font-size: 11px;line-height: 13px;width: auto;">0</span>
                                                                        <?php //endif; 
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <div class="dropdown-menu cart-info" style="display: none; border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; max-height: 350px;">
                                                                <div class="cart-content">
                                                                    <?php //if ($this->cart->contents()) { 
                                                                    ?>
                                                                        <div class="items control-container" style="max-height: 150px; overflow: auto">
                                                                        <?php
                                                                        // foreach ($this->cart->contents() as $item) {
                                                                        //     // SEPARATED ID INTO 2 VARIABLE, PRODUCT ID AND STORE ID
                                                                        //     list($product_id, $store_id) = explode('-', $item['id']);

                                                                        //     $item['store'] = $store_id;
                                                                        ?>
                                                                                <div class="row" style="padding: 10px 10px;">
                                                                                    <div class="cart-left">
                                                                                        <a class="cart-image" href="<?php //echo seo_url('catalog/products/view/' . $product_id . '/' . $item['store']); 
                                                                                                                    ?>">
                                                                                            <img style="width: 50px;" src="<?php //echo get_image($item['image']); 
                                                                                                                            ?>" alt="" title="">
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="cart-right">
                                                                                        <div class="cart-title"><a href="<?php //echo seo_url('catalog/products/view/' . $product_id . '/' . $item['store']); 
                                                                                                                            ?>" style="color:#000;"><?php //echo $item['name']; 
                                                                                                                                                    ?></a></div>
                                                                                        <div class="cart-price"><span class="money"><?php //echo rupiah($item['price']); 
                                                                                                                                    ?></span><span class="x"> x <?php //echo number($item['qty']); 
                                                                                                                                                                ?></span></div>
                                                                                    </div>
                                                                                </div>
                                                                            <?php //} 
                                                                            ?>
                                                                        </div>
                                                                        <div class="subtotal" style="border-top: 1px solid #EBEBEB"><span>Subtotal:</span><span class="cart-total-right money"><?php //echo rupiah($this->cart->total()); 
                                                                                                                                                                                                ?></span></div>
                                                                        <div class="action"><button class="btn" style="font-size:10px; width: 100%;" onclick="window.location = '<?php //echo site_url('cart'); 
                                                                                                                                                                                    ?>'">Lihat Semua</button></div>
                                                                    <?php //} else { 
                                                                    ?>
                                                                        <div class="items control-container">
                                                                            <center><p style="font-size: 13px; font-weight: bold; padding: 15px;">Keranjang belanja Anda kosong. Yuk, belanja sekarang!</p>
                                                                            </center>
                                                                        </div>
                                                                    <?php //} 
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section> -->
            </div>
            <script>
                // function addaffix(scr) {
                //     if ($(window).innerWidth() >= 992) {
                //         if (scr > 170) {
                //             if (!$('#top').hasClass('affix')) {
                //                 $('#top').addClass('affix').addClass('fadeInDown animated');
                //             }
                //         } else {
                //             if ($('#top').hasClass('affix')) {
                //                 $('#top').prev().remove();
                //                 $('#top').removeClass('affix').removeClass('fadeInDown animated');
                //             }
                //         }
                //     } else
                //         $('#top').removeClass('affix');
                // }
                // $(window).scroll(function () {
                //     var scrollTop = $(this).scrollTop();
                //     addaffix(scrollTop);
                // });
                // $(window).resize(function () {
                //     var scrollTop = $(this).scrollTop();
                //     addaffix(scrollTop);
                // });
            </script>
        </div>
    </header>
    <?php if($this->session->flashdata('not_login_redirect')) : ?>
        <?php echo $this->session->flashdata('not_login_redirect'); ?>
    <?php endif; ?>
    <?php if($this->session->flashdata('login_email_not_phone')) : ?>
        <?php echo $this->session->flashdata('login_email_not_phone'); ?>
    <?php endif; ?>
    <div class="fix-sticky"></div>
    <div class="notification" style="display: none;">
        <?php
        if ($this->ion_auth->logged_in()) {
            if (!$user->verification_phone && $this->uri->segment(2) != 'verification_phone') {
        ?>
                <div class="alert alert-danger text-center no-margin">Segera lakukan <b>Verifikasi No. Handphone</b> Anda <a class="link" href="<?php echo site_url('member/verification_phone'); ?>">disini</a></div>
                <?php
            }
            if ($orders_pending) {
                if ($orders_pending->num_rows() > 1) {
                ?>
                    <div class="alert alert-warning text-center no-margin"><b>Segera lakukan pembayaran</b> untuk <b><?php echo $orders_pending->num_rows(); ?></b> tagihan sebelum kadaluarsa. Selengkapnya lihat <a class="link" href="<?php echo site_url('member/history'); ?>">disini</a></div>
                <?php
                } else {
                    $op = $orders_pending->row();
                ?>
                    <div class="alert alert-warning text-center no-margin">Segera lakukan pembayaran</b> sebesar <b><?php echo rupiah($op->total + $op->unique_code); ?></b> sebelum <b><?php echo get_date_indo_full($op->due_date); ?></b>. Selengkapnya lihat <a class="link" href="<?php echo site_url('member/order_detail/' . encode($op->id)); ?>">disini</a></div>
        <?php
                }
            }
            if ($this->router->fetch_class() == 'member') {
                if ($notification_invoice) {
                    foreach ($notification_invoice->result() as $invoice) {
                        $invoice_code = explode(',', $invoice->invoice);
                        $code = '';
                        foreach ($invoice_code as $i => $ic) {
                            $ic = explode('-', $ic);
                            $code .= '<a class="link" href="' . site_url('member/order_detail/' . encode($ic[1])) . '"><b>' . $ic[0] . '</b></a>' . ($i + 1 < count($invoice_code) ? ', ' : '');
                        }
                        echo '<div class="alert alert-success text-center no-margin">Invoice ' . $code;
                        if ($invoice->status == settings('order_process_status')) {
                            echo ' sedang <b>Diproses</b> oleh penjual.';
                        } elseif ($invoice->status == settings('order_shipped_status')) {
                            echo ' sudah <b>Dikirim</b>.';
                        } elseif ($invoice->status == settings('order_ready_pickup_status')) {
                            echo ' sudah bisa <b>Diambil</b> di alamat penjual.';
                        } elseif ($invoice->status == settings('order_delivered_status')) {
                            echo ' sudah <b>Diterima</b>. Segera konfirmasi penerimaan.';
                        } elseif ($invoice->status == settings('order_complete_pickup_status')) {
                            echo ' sudah <b>Diambil</b>. Segera konfirmasi penerimaan.';
                        }
                        echo '</div>';
                    }
                }
            }
        }
        ?>
    </div>
    <?php if($this->ion_auth->logged_in()) : ?>
        <?php 
            $percent_profile = 0;
            $customer = $this->main->get('customers', ['id' => $this->data['user']->id]);
            if($customer->phone || $customer->username) {
                $percent_profile += 30;
            }
            if($customer->email && $customer->email_verification == 1) {
                $percent_profile += 30;
            }
            if($customer->gender) {
                $percent_profile += 5;
            }
            if($customer->birthday) {
                $percent_profile += 5;
            }
            $check_address = $this->main->gets('customer_address', ['customer' => $this->data['user']->id]);
            if($check_address) {
                $percent_profile += 30;
            }
            if($customer->fullname) {
                $percent_profile += 10;
            }
        ?>
        <?php if(!$this->agent->is_mobile()) : ?>
            <div class="notification_profile_container">
                <div id="notification_profile">
                <?php if($percent_profile < 100) : ?>
                    <div class="notification_profile">
                        <div class="notification_left">
                            <div class="notification_profile_text">
                                Segera lengkapi profil anda agar akun terverifikasi!.
                            </div>
                            <div class="notification_progress_bar">
                                <div class="progress_bar" style="width: <?php echo $percent_profile ?>%"><?php echo $percent_profile ?> %</div>
                            </div>
                        </div>
                        <div class="notification_right">
                            <div class="notification_button">
                                <a href="<?php echo site_url('member/profile') ?>" class="profile_button">Lengkapi Profil</a>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
    <?php endif; ?>
    <div class="page-container" id="PageContainer" style="<?php echo ($this->agent->is_mobile()) ? 'margin-bottom: 50px;' : ''  ?>">
        <main class="main-content" id="MainContent" role="main">
            <?php echo $output; ?>
        </main>
    </div>
    <hr style="border-top: 2px solid #eee;">
    <?php if(!$this->agent->is_mobile()) : ?>
        <footer class="footer" style="margin-top: -20px;background: none;">
            <div id="shopify-section-theme-footer" class="shopify-section">
                <!-- <section class="footer_newsoc_block">
                    <div class="footer_newsoc_wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="footer_newsoc_inner">
                                    <div class="footer_newsoc_content">
                                        <div class="footer_newsletter_content">
                                            <span class="newsletter-title">
                                                Dapatkan promo &amp; info terbaru
                                            </span>
                                            <div class="newsletter_content">
                                                <form>
                                                    <input type="email" class="form-control" id="email-subscribe" required="" name="email" placeholder="Masukkan alamat email anda">
                                                    <button type="button" id="btn-subscribe" class="btn_newsletter_send btn">Kirim</button>
                                                </form>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section> -->
                <section class="footer_linklist_block">
                    <div class="footer_linklist_wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="footer_linklist_inner">
                                    <div class="footer_linklist_content">
                                        <?php
                                        $footer = json_decode(settings('footer'), TRUE);
                                        $n = 0;
                                        foreach ($footer as $foot) {
                                        ?>
                                            <div class="col-sm-3 linklist_item">
                                                <span class="footer-title"><?php echo $foot['title']; ?></span>
                                                <div class="linklist_content">
                                                    <?php if ($foot['type'] == 'list') { ?>
                                                        <ul class="linklist_menu">
                                                            <?php foreach ($foot['content'] as $link) { ?>
                                                                <li><a href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a></li>
                                                            <?php } ?>
                                                        </ul>
                                                    <?php } else { ?>
                                                        <div class="linklist_content">
                                                            <p style="color: #333; margin:0px; margin-bottom: 5px;"><?php echo nl2br($foot['content']); ?></p>
                                                            <?php $n++;
                                                            if ($n == 1) { ?>
                                                                <a target="_blank" href="https://wa.me/628118332018"><img src="<?php echo base_url('assets/frontend/images/bank/whatsapp.png'); ?>"></a>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="copy-right">
                    <div class="copy-right-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="copy-right-inner">
                                    <div class="copy-right-group">
                                        <div class="col-md-1" style="padding-left:0px;padding-right: 0px;">
                                            <div class="social_content" style="text-align:left;">
                                                <?php
                                                $social_media_link = json_decode(settings('social_media_link'), true);
                                                if ($social_media_link) {
                                                    foreach ($social_media_link as $name => $link) {
                                                        echo '<a href="' . $link . '" style="margin-right:10px;" class="icon-social ' . $name . '"><i class="fa fa-' . $name . '"></i></a>';
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-md-11" style="padding-left:0px;">
                                            <div class="" style="text-align:left;padding-left:0px;">
                                                <a target="_blank" href="https://play.google.com/store/apps/details?id=com.tokomanamana.tokomanamana"><img style="height:30px;" src="<?php echo base_url('assets/frontend/images/dl-googleplay.png'); ?>"></a>
                                            </div>
                                        </div>
                                        <div class="col-md-7" style="padding-left:0px;padding-right: 0px;">
                                            <div style="text-align:left">
                                                <p style="font-weight:bold;font-size:14px;text-align:left;margin:10px auto;">Metode Pembayaran</p>
                                                <img src="<?php echo base_url('assets/frontend/images/bank/bca.png'); ?>">
                                                <img src="<?php echo base_url('assets/frontend/images/bank/mandiri.webp'); ?>">
                                                <img src="<?php echo base_url('assets/frontend/images/bank/bni.webp'); ?>">


                                                <img src="<?php echo base_url('assets/frontend/images/bank/bri.webp'); ?>">
                                                <img src="<?php echo base_url('assets/frontend/images/bank/klikpay.webp'); ?>">
                                                <img src="<?php echo base_url('assets/frontend/images/bank/sakuku.jpeg'); ?>" style="width:20px;">
                                                <img src="<?php echo base_url('assets/frontend/images/bank/kredivo.jpeg'); ?>" style="width:40px;">
                                                <img src="<?php echo base_url('assets/frontend/images/bank/visa.png'); ?>">
                                                <img src="<?php echo base_url('assets/frontend/images/bank/mastercard.webp'); ?>">
                                                <img src="<?php echo base_url('assets/frontend/images/bank/alfamart.png'); ?>" style="width:60px;">
                                                <img src="<?php echo base_url('assets/frontend/images/bank/ovo.png'); ?>" style="width:60px;">
                                            </div>
                                        </div>
                                        <div class="col-md-3" style="padding-left:0px;padding-right: 0px;">
                                            <div style="text-align:left">
                                                <p style="font-weight:bold;font-size:14px;text-align:left;margin:15px auto;">Jasa Pengiriman</p>
                                                <img src="<?php echo base_url('assets/frontend/images/bank/jne.jpg'); ?>">
                                                <img src="<?php echo base_url('assets/frontend/images/bank/tiki.jpg'); ?>">
                                                <img src="<?php echo base_url('assets/frontend/images/bank/pos.jpg'); ?>">
                                                <img src="<?php echo base_url('assets/frontend/images/bank/grab.png'); ?>" style="width:60px;">
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-left:0px;padding-right: 0px;">
                                            <div style="text-align:left">
                                                <p style="font-weight:bold;font-size:14px;text-align:left;margin:15px auto;">Keamanan Belanja</p>
                                                <img src="<?php echo base_url('assets/frontend/images/bank/vbv.jpeg'); ?>" style="width:50px;">
                                                <img src="<?php echo base_url('assets/frontend/images/bank/mastercard.jpeg'); ?>" style="width:50px;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </section>
            </div>
        </footer>
    <?php endif; ?>
    <!-- <div id="modal-location" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;">
        <div class="modal-dialog fadeIn animated">
            <div class="modal-content">
                <div class="modal-header">
                    <h1>Set Lokasi Anda</h1>
                </div>
                <div class="modal-body"></div>
                <input type="hidden" id="temp-lat">
                <input type="hidden" id="temp-lng">
                <div class="modal-footer">
                    <button type="button" id="my-location" class="btn btn-success" style="background: #fff; color: #000; float: left;">Lokasi Saya</button>
                    <button type="button" id="btn-set-location" class="btn btn-primary">Set Lokasi</button>
                </div>
            </div>
        </div>
    </div>
    <div id="modal-location-pin" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;">
        <div class="modal-dialog fadeIn animated">
            <div class="modal-content">
                <div class="modal-header">
                    <h1>Set Lokasi Anda</h1>
                </div>
                <div class="modal-body"></div>

                <div class="modal-footer">
                    <button type="button" id="my-location-pin" class="btn btn-success" style="background: #fff; color: #000; float: left;">Batal</button>
                    <button type="button" id="btn-set-location-pin" class="btn btn-primary">Set Lokasi</button>
                </div>
            </div>
        </div>
    </div> -->
    <!-- Modal -->
    <div id="modal-guest" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;">
        <div class="modal-dialog fadeIn animated">
            <div class="modal-content" style="border-radius: 3px; border:none;">
                <div class="modal-header">
                    <h1><b>Masuk Sebagai Tamu</b></h1>
                </div>
                <form action="<?= base_url('auth/guest') ?>" method="post" id="modal-form-guest">
                    <div class="modal-body">
                        <?php
                        $provincies = $this->main->gets('provincies', array(), 'name ASC');
                        ?>
                        <label class="">Nama Lengkap</label>
                        <input type="text" class="text required" name="name" style="width: 100%;">
                        <label class="">Email</label>
                        <input type="email" class="text required" name="email" style="width: 100%;">
                        <label class="">No. Handphone</label>
                        <input type="text" class="text required" name="phone" style="width: 100%;">
                        <label class="">Password</label>
                        <input type="password" class="text required" name="password" style="width: 100%;">
                        <label class="">Alamat</label>
                        <input type="text" class="text required" name="address" style="width: 100%;">
                        <label class="">Provinsi</label>
                        <select name="province" id="province-modal" class="form-control required" style="margin-bottom:10px">
                            <option value="">Pilih provinsi</option>
                            <?php if ($provincies) { ?>
                                <?php foreach ($provincies->result() as $province) { ?>
                                    <option value="<?php echo $province->id; ?>"><?php echo $province->name; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                        <label class="">Kota</label>
                        <select name="city" id="city-modal" class="form-control required" style="margin-bottom:10px">
                            <option value="">Pilih kota</option>
                        </select>
                        <label class="">Kecamatan</label>
                        <select name="district" id="district-modal" class="form-control required" style="margin-bottom:10px">
                            <option value="">Pilih kecamatan</option>
                        </select>
                        <label class="">Kode Pos</label>
                        <input type="hidden" name="back" id="back-modal" value="">
                        <input name="postcode" type="text" class="form-control required" onkeypress="return isNumberKeyModal(event)" style="margin-bottom:10px">

                        <p>Sudah jadi member? <a href="javascript:void(0)" class="color" onclick="$('#modal_login').modal('show');">Masuk</a></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success btn-exit" style="background: #fff; color: #000; float: left;" data-dismiss="modal">Tutup</button>
                        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                        <button type="submit" class="btn btn-primary btn-login">Masuk Sebagai Tamu</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php if(!$this->ion_auth->logged_in()) : ?>
        <!-- Modal Register -->
        <div id="modal_register" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;">
            <div class="modal-dialog fadeIn animated modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1>Daftar Akun Tokomanamana</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -37px;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="<?php echo site_url('auth/save_register') ?>" method="POST" id="form_register">
                        <div class="modal-body">
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="error_register_validation col-md-12">
                                        </div>
                                        <div class="send_otp_success col-md-12">
                                        </div>
                                        <div class="save_address_success col-md-12"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5 col-sm-5 form-group">
                                            <label for="no_handphone">No Handphone*</label>
                                            <input type="text" class="form-control" placeholder="No Handphone" name="phone_register" id="phone_register">
                                            <div id="error_phone_register" class="error_validation"></div>
                                        </div>
                                        <div class="col-md-7 form-group col-sm-7">
                                            <label for="full_name">Nama Lengkap*</label>
                                            <input type="text" class="form-control" placeholder="Nama Lengkap" name="fullname" id="fullname">
                                            <div id="error_fullname" class="error_validation"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-7 form-group col-sm-7">
                                            <label for="verification_phone_register">Kode OTP*</label>
                                            <input type="text" class="form-control" placeholder="Kode OTP" name="verification_phone_register" id="verification_phone_register">
                                            <div id="error_verification_phone_register" class="error_validation"></div>
                                        </div>
                                        <div class="col-md-5 col-sm-5 form-group">
                                            <label for="button_phone_verification" style="color: white;">T</label>
                                            <button type="button" id="button_phone_verification" class="button_phone_verification" data-otp="true">Kirim OTP</button>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label for="password_register">Kata Sandi*</label>
                                            <input type="password" class="form-control" placeholder="Kata Sandi" name="password_register" id="password_register">
                                            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"
                                            style="float: right;
                                            margin-top:-23px;
                                            margin-right:10px;
                                            position: relative;
                                            z-index: 2;
                                            cursor: pointer;" onclick="password_field_register(this)"></span>
                                            <div id="error_password_register" class="error_validation"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-7 col-sm-7 form-group birtday">
                                            <div class="">
                                                <label for="birthdate_register">Tanggal Lahir</label>
                                            </div>
                                            <input type="date" class="form-control" placeholder="Tanggal Lahir" id="birthdate_register" name="birthdate_register">
                                        </div>
                                        <div class="col-md-5 col-sm-5 form-group">
                                            <label for="gender_register">Jenis Kelamin</label>
                                            <select name="gender_register" id="gender_register" class="form-control">
                                                <option disabled="" selected="" value="">Jenis Kelamin</option>
                                                <option value="L">Laki - Laki</option>
                                                <option value="P">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-7 col-sm-7 form-group">
                                            <label for="captcha_register">Kode Captcha*</label>
                                            <input type="text" class="form-control" name="captcha_register" id="captcha_register" placeholder="Kode Captcha">
                                            <div id="error_captcha_register" class="error_validation"></div>
                                        </div>
                                        <div class="col-md-5 col-sm-5 form-group">
                                            <label for="" style="text-align: right;margin-right: 10px;"><a href="#" id="change_captcha">Ganti Captcha</a></label>
                                            <div class="captcha_register_image"></div>
                                            <!-- <a href="#" id="change_captcha">Ganti Captcha</a> -->
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 form-group">
                                            <label for="">Pin alamat untuk memudahkan anda dalam pengiriman Grab Express</label>
                                            <div class="square" style="width: 100%; border: 1px solid #EEE;vertical-align: middle;clear: both;float: left;padding: 20px;border-radius: 5px;">
                                                <div style="display: inline-block;width: 50%;float: left;line-height: 36px">Pin Alamat (Opsional)</div>
                                                <button type="button" class="btn btn_pin_address" onclick="add_other_address();"><i class="fa fa-map-marker"></i> &nbsp;Pin Alamat</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-7 col-sm-7">
                                            <div class="text_other">
                                                Dengan mendaftar, Anda menyetujui <a href="javascript:void(0)" data-toggle="modal" data-target="#privacy">Syarat, Ketentuan dan Kebijakan dari Tokomanamana</a>
                                            </div>
                                            <div class="text_login">
                                                Sudah punya akun Tokomanamana ? <a href="javascript:void(0)">Masuk Disini</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row action">
                                <div class="col-md-6 col-sm-6">
                                    <button class="btn btn_register" type="submit">Daftar Sekarang</button>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <button class="btn btn_register_facebook" type="button"><i class="fa fa-facebook" aria-hidden="true"></i> &nbsp;Facebook</button>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <button class="btn btn_register_google" type="button"><i class="fa fa-google" aria-hidden="true"></i> &nbsp;Google</button>
                                </div>
                            </div>
                        </div>

                        <!-- <div class="modal-footer">
                            <button type="button" class="btn btn-success btn-exit" data-dismiss="modal" style="border: 1px solid #A7C22A !important;color: #A7C22A !important;">Tutup</button>
                                <button type="submit" class="btn btn-primary btn-register" style="background: #A7C22A;border: 1px solid #A7C22A;font-weight: bold;">Daftar</button>
                        </div> -->
                        <input type="hidden" name="back" value="<?php echo current_url() ?>">
                        <input type="hidden" id="lat_register" name="lat">
                        <input type="hidden" id="lng_register" name="lng">
                    </form>
                </div>
            </div>
        </div>
        <div id="privacy" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 550px;margin: auto;vertical-align: middle;margin-top: 80px;border-radius: 5px;">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: 20px;margin-right: 20px;position: relative;z-index: 1;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div class="modal-body">
                        <?php echo settings('term_customer_register'); ?>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal Login -->
        <!-- <div id="modal_login" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;width: 400px;margin: auto;">
            <div class="modal-dialog fadeIn animated modal-dialog-centered" style="width: auto;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1><center>Masuk Dengan No Handphone / Email</center></h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -37px;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <hr>
                    <form action="<?php echo site_url('auth/new_login') ?>" method="POST" id="form_login">
                        <?php 
                        $remember = get_cookie('remember_me');
                        if($remember) {
                            $account = $this->main->get('customers', ['remember_code' => $remember]);
                            if($account && $account->email) {
                                $identity_login = $account->email;
                            } else if ($account) {
                                $identity_login = $account->phone;
                            } else {
                                $identity_login = false;
                            }
                        }  else {
                            $identity_login = false;
                        }
                        ?>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="error_validation_login col-md-12">
                                        </div>
                                        <div class="send_otp_success col-md-12">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 form-group">
                                            <label for="identity_login">No Handphone / Email</label>
                                            <input type="text" class="form-control" placeholder="Masukkan No Handphone / Email" name="identity_login" id="identity_login" value="<?php echo ($identity_login) ? $identity_login : '' ?>">
                                            <div id="error_identity_login" class="error_validation"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <label for="password_login">Kata Sandi</label>
                                            <input type="password" class="form-control" id="password_login" name="password_login" placeholder="Masukkan Kata Sandi">
                                            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"
                                            style="float: right;
                                            margin-top:-23px;
                                            margin-right:10px;
                                            position: relative;
                                            z-index: 2;
                                            cursor: pointer;" onclick="password_field_login(this)"></span>
                                            <div id="error_password_login" class="error_validation"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <input type="checkbox" class="form-horizontal" id="remember_me" name="remember_me" <?php echo ($remember) ? 'checked' : ''; ?>>
                                            <label for="remember_me">Ingat Saya</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="text_other" style="text-align: right;">
                                                <a href="<?php echo site_url('member/forgot_password') ?>">Lupa Kata Sandi?</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="text_register">
                                                Belum punya akun? <a href="javascript:void(0)">Daftar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="action">
                                <div class="row">
                                    <div class="col-md-12 col-sm-6">
                                        <button class="btn btn_register" type="submit">Masuk</button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row" style="margin-top: 15px;">
                                            <div class="col-md-6 col-sm-6">
                                                <button class="btn btn_register_facebook" type="button"><i class="fa fa-facebook" aria-hidden="true"></i> &nbsp;Facebook</button>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <button class="btn btn_register_google" type="button"><i class="fa fa-google" aria-hidden="true"></i> &nbsp;Google</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="back" value="<?php echo current_url() ?>">
                    </form>
                </div>
            </div>
        </div> -->
        <div id="modal_login" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;margin: auto;">
            <div class="modal-dialog fadeIn animated modal-dialog-centered" style="width: auto;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1><center>Selamat datang di Tokomanamana</center></h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -37px;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <hr style="margin-left: 15px;margin-right: 15px;">
                    <div class="row">
                        <div class="col-md-6 col-sm-6" style="margin-bottom: 10px;margin-top: 10px;">
                            <form action="<?php echo site_url('auth/new_login') ?>" method="POST" id="form_login">
                                <?php 
                                $remember = get_cookie('remember_me');
                                if($remember) {
                                    $account = $this->main->get('customers', ['remember_code' => $remember]);
                                    if($account && $account->email) {
                                        $identity_login = $account->email;
                                    } else if ($account) {
                                        $identity_login = $account->phone;
                                    } else {
                                        $identity_login = false;
                                    }
                                }  else {
                                    $identity_login = false;
                                }
                                ?>
                                <div class="modal-body">
                                    <h1>Masuk sebagai customer</h1>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="error_validation_login col-md-12">
                                                </div>
                                                <div class="send_otp_success col-md-12">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 form-group">
                                                    <label for="identity_login">No Handphone / Email</label>
                                                    <input type="text" class="form-control" placeholder="Masukkan No Handphone / Email" name="identity_login" id="identity_login" value="">
                                                    <div id="error_identity_login" class="error_validation"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label for="password_login">Kata Sandi</label>
                                                    <input type="password" class="form-control" id="password_login" name="password_login" placeholder="Masukkan Kata Sandi">
                                                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"
                                                    style="float: right;
                                                    margin-top:-23px;
                                                    margin-right:10px;
                                                    position: relative;
                                                    z-index: 2;
                                                    cursor: pointer;" onclick="password_field_login(this)"></span>
                                                    <div id="error_password_login" class="error_validation"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <!-- <div class="col-md-6 form-group">
                                                    <input type="checkbox" class="form-horizontal" id="remember_me" name="remember_me" <?php echo ($remember) ? 'checked' : ''; ?>>
                                                    <label for="remember_me">Ingat Saya</label>
                                                </div> -->
                                                <div class="col-md-12">
                                                    <div class="text_other" style="text-align: right;">
                                                        <a href="<?php echo site_url('member/forgot_password') ?>">Lupa Kata Sandi?</a>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-md-12">
                                                    <div class="text_register">
                                                        Belum punya akun? <a href="javascript:void(0)">Daftar</a>
                                                    </div>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="action">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <button class="btn btn_register" type="submit">Masuk</button>
                                            </div>
                                        </div>
                                        <!-- <div class="row">
                                            <div class="col-md-12">
                                                <div class="row" style="margin-top: 15px;">
                                                    <div class="col-md-4 col-sm-4"></div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <button class="btn btn_register_facebook" type="button"><i class="fa fa-facebook" aria-hidden="true"></i></button>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <button class="btn btn_register_google" type="button"><i class="fa fa-google" aria-hidden="true"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-md-12">
                                                <div class="text_register" style="text-align: center;">
                                                    Belum punya akun? <a href="javascript:void(0)">Daftar Sekarang</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="back" value="<?php echo current_url() ?>">
                            </form>  
                        </div>
                        <div class="col-md-6 col-sm-6" style="padding-left: 0px;margin-bottom: 10px;margin-top: 10px;">
                            <form action="<?php echo site_url('auth/new_login_merchant') ?>" method="POST" id="form_login_merchant">
                                <div class="modal-body modal-body-merchant">
                                    <h1>Masuk sebagai merchant</h1>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="error_validation_login_merchant col-md-12">
                                                </div>
                                                <div class="send_otp_success col-md-12">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 form-group">
                                                    <label for="identity_login_merchant">No Handphone / Email</label>
                                                    <input type="text" class="form-control" placeholder="Masukkan No Handphone / Email" name="identity_login_merchant" id="identity_login_merchant" value="">
                                                    <div id="error_identity_login_merchant" class="error_validation"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label for="password_login_merchant">Kata Sandi</label>
                                                    <input type="password" class="form-control" id="password_login_merchant" name="password_login_merchant" placeholder="Masukkan Kata Sandi">
                                                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"
                                                    style="float: right;
                                                    margin-top:-23px;
                                                    margin-right:10px;
                                                    position: relative;
                                                    z-index: 2;
                                                    cursor: pointer;" onclick="password_field_login_merchant(this)"></span>
                                                    <div id="error_password_login_merchant" class="error_validation"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <!-- <div class="col-md-6 form-group">
                                                    <input type="checkbox" class="form-horizontal" id="remember_me_merchant" name="remember_me_merchant">
                                                    <label for="remember_me_merchant">Ingat Saya</label>
                                                </div> -->
                                                <div class="col-md-12">
                                                    <div class="text_other" style="text-align: right;">
                                                        <a href="<?php echo site_url('ma/auth/forgot_password') ?>">Lupa Kata Sandi?</a>
                                                    </div>
                                                </div>
                                                <!-- <div class="col-md-12">
                                                    <div class="text_register_merchant">
                                                        Belum punya akun? <a href="<?php echo site_url('ma/register') ?>">Daftar Merchant</a>
                                                    </div>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="action">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <button class="btn btn_register btn_register_merchant" type="submit">Masuk</button>
                                            </div>
                                        </div>
                                        <!-- <div class="row">
                                            <div class="col-md-12">
                                                <div class="row" style="margin-top: 15px;">
                                                    <div class="col-md-4 col-sm-4"></div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <button class="btn btn_register_facebook" type="button"><i class="fa fa-facebook" aria-hidden="true"></i></button>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2">
                                                        <button class="btn btn_register_google" type="button"><i class="fa fa-google" aria-hidden="true"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-md-12">
                                                <div class="text_register_merchant" style="text-align: center;">
                                                    Belum punya akun? <a href="<?php echo site_url('ma/register') ?>">Daftar Merchant</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="back_merchant" value="<?php echo current_url() ?>">
                            </form>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="modal_set_address" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;">
            <div class="modal-dialog fadeIn animated modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1>Set Alamat</h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -37px;" onclick="$('#modal_register').modal('show')">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <hr>
                    </div>
                    <div id="modal_set_address_body" class="modal-body">
                    </div>
                    <div class="modal-footer">
                        <input type="hidden" id="temp_lat_register" name="lat">
                        <input type="hidden" id="temp_lng_register" name="lng">
                        <button type="button" id="my-location" class="btn btn_save" style="float: left;">Lokasi Saya</button>
                        <button type="button" class="btn btn_cancel" data-dismiss="modal" onclick="$('#modal_register').modal('show')">Tutup</button>
                        <button type="button" class="btn btn_save" onclick="save_location_new();">Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if($this->ion_auth->logged_in()) : ?>
        <div id="modal_login_merchant" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;margin: auto;">
            <div class="modal-dialog fadeIn animated modal-dialog-centered" style="width: auto;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1><center>Masuk sebagai merchant</center></h1>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -37px;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <hr style="margin-left: 15px;margin-right: 15px;">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <form action="<?php echo site_url('auth/new_login_merchant') ?>" method="POST" id="form_login_merchant">
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="error_validation_login_merchant col-md-12">
                                                </div>
                                                <div class="send_otp_success col-md-12">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 form-group">
                                                    <label for="identity_login_merchant">No Handphone / Email</label>
                                                    <input type="text" class="form-control" placeholder="Masukkan No Handphone / Email" name="identity_login_merchant" id="identity_login_merchant" value="">
                                                    <div id="error_identity_login_merchant" class="error_validation"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label for="password_login_merchant">Kata Sandi</label>
                                                    <input type="password" class="form-control" id="password_login_merchant" name="password_login_merchant" placeholder="Masukkan Kata Sandi">
                                                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"
                                                    style="float: right;
                                                    margin-top:-23px;
                                                    margin-right:10px;
                                                    position: relative;
                                                    z-index: 2;
                                                    cursor: pointer;" onclick="password_field_login_merchant(this)"></span>
                                                    <div id="error_password_login_merchant" class="error_validation"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <!-- <div class="col-md-6 form-group">
                                                    <input type="checkbox" class="form-horizontal" id="remember_me_merchant" name="remember_me_merchant">
                                                    <label for="remember_me_merchant">Ingat Saya</label>
                                                </div> -->
                                                <div class="col-md-12">
                                                    <div class="text_other" style="text-align: right;">
                                                        <a href="<?php echo site_url('ma/auth/forgot_password') ?>">Lupa Kata Sandi?</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="action">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12" style="<?php echo ($this->agent->is_mobile()) ? 'padding-right: 30px;padding-left: 30px;' : '' ; ?>">
                                                <button class="btn btn_register btn_register_merchant" type="submit">Masuk</button>
                                            </div>
                                        </div>
                                        <!-- <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="row" style="margin-top: 15px;">
                                                    <div class="col-md-4 col-sm-4 col-xs-3"></div>
                                                    <div class="col-md-2 col-sm-2 col-xs-3">
                                                        <button class="btn btn_register_facebook" type="button"><i class="fa fa-facebook" aria-hidden="true"></i></button>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2 col-xs-3">
                                                        <button class="btn btn_register_google" type="button"><i class="fa fa-google" aria-hidden="true"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-md-12">
                                                <div class="text_register_merchant" style="text-align: center;">
                                                    Belum punya akun? <a href="<?php echo site_url('ma/register') ?>">Daftar Merchant</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="back_merchant" value="<?php echo current_url() ?>">
                            </form>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div id="modal_verification" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;margin: auto;<?php echo ($this->agent->is_mobile()) ? '' : 'width: 450px;' ; ?>">
        <div class="modal-dialog fadeIn animated modal-dialog-centered" style="width: auto;">
            <div class="modal-content">
                <div class="modal-header">
                    <?php if($this->agent->is_mobile()) : ?>
                        <h1><center>Masukkan kode verifikasi</center></h1>
                    <?php else : ?>
                        <h1>Masukkan kode verifikasi</h1>
                    <?php endif; ?>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -37px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div id="modal_verification_body">
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="float-right-icon">
            <ul>
                <li>
                    <div id="scroll-to-top" data-toggle="" data-placement="left" title="Scroll to Top" class="off">
                        <i class="fa fa-angle-up"></i>
                    </div>
                </li>
            </ul>
        </div> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.js"></script>
    <script>
       function getCitiesModal(i){$("#district-modal option").remove(),$("#district-modal").append('<option value="">Pilih kecamatan</option>'),""==i||0==i?($("#city-modal option").remove(),$("#city-modal").append('<option value="">Pilih kota</option>'),$("#select-shipping").children().remove(),$("#select-shipping").html("<p>Pilih provinsi terlebih dahulu.</p>")):$.get(site_url+"auth/getCities/"+i,function(i){$("#city-modal").html(i),$("#select-shipping").children().remove(),$("#select-shipping").html("<p>Pilih kota terlebih dahulu.</p>")})}function getDistrictsModal(i){""==i||0==i?($("#district-modal option").remove(),$("#district-modal").append('<option value="">Pilih kecamatan</option>'),$("#select-shipping").children().remove(),$("#select-shipping").html("<p>Pilih kota terlebih dahulu.</p>")):$.get(site_url+"auth/getDistricts/"+i,function(i){$("#district-modal").html(i),$("#select-shipping").children().remove(),$("#select-shipping").html("<p>Pilih kecamatan terlebih dahulu.</p>")})}function isNumberKeyModal(i){var e=i.which?i.which:event.keyCode;return!(e>31&&(e<48||e>57))}$(function(){$.validator.addMethod("regex",function(i,e,t){return this.optional(e)||t.test(i)}),$("#modal-form-guest").validate({rules:{email:{remote:{url:site_url+"auth/check_email_guest",type:"post"},regex:/^\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i}},messages:{name:{required:"Nama Harus Diisi!"},email:{required:"Email Harus Diisi!",email:"Harus Menggunakan Email Yang Valid!",remote:"Email sudah terdaftar!",regex:"Harus Menggunakan Email Yang Valid!"},phone:{required:"No Handphone Harus Diisi!"},password:{required:"Password Harus Diisi!"},address:{required:"Alamat Harus Diisi!"},province:{required:"Provinsi Harus Diisi!"},city:{required:"Kota Harus Diisi!"},district:{required:"Kecamatan Harus Diisi!"},postcode:{required:"Kode Pos Harus Diisi!"}}}),$("#tombolNavigation").on("click",function(){$("#tombolNavigation").hide(),$(".navigation-mobile-content").show("slow")}),$(".tombol-close").on("click",function(){$(".navigation-mobile-content").hide("slow"),$("#tombolNavigation").show(),$("#tombolNavigation").html('<i class="fa fa-bars fa-2x"></i>')}),$(".navigation_mobile .menu-mobile-link").on("click",function(){"menu-mobile-link class_test"==$(this).attr("class")?($(".navigation_mobile .menu-mobile-link").removeClass("class_test"),$(".navigation_mobile").removeClass("active"),$(".navigation_mobile").find(".menu-mobile-container").hide("slow")):($(".navigation_mobile .menu-mobile-link").removeClass("class_test"),$(this).addClass("class_test"),$(".navigation_mobile").each(function(){"menu-mobile-link class_test"==$(this).find(".menu-mobile-link").attr("class")?($(this).find(".menu-mobile-container").show("slow"),$(this).addClass("active")):($(this).find(".menu-mobile-container").hide("slow"),$(this).removeClass("active"))}))}),$(".tombol-cart").on("click",function(){$("#cart-button .badge-cart").css("display","block")}),$(".purchase a").on("click",function(){$("#cart-button .badge-cart").css("display","block")}),$("#province-modal").change(function(i){i.preventDefault();var e=$(this).val();console.log(e),getCitiesModal(e)}),$("#city-modal").change(function(i){i.preventDefault(),getDistrictsModal($(this).val())})});
    </script>
    <script src="<?= base_url('assets/frontend/js/sweetalert2.all.min.js') ?>"></script>
</body>

</html>