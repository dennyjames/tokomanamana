<div bgcolor="#FFFFFF" style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;width:100%!important;height:100%;font-size:14px;color:#404040;margin:0;padding:0">
    <table style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0" bgcolor="transparent">
        <tbody>
            <tr style="margin:0;padding:0">
                <td style="margin:0;padding:0"></td>
                <td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0" bgcolor="#FFFFFF">
                    <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;border:1px solid #e7e7e7">
                        <table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:20px;" bgcolor="transparent">
                            <tbody>
                                <tr style="margin:0;padding:0">
                                    <td>
                                        <img src="<?php echo site_url('/assets/frontend/images/logo.png'); ?>" alt="" title="" style="margin:10px 0;height: 35px;">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="<?php echo site_url('/assets/frontend/images/waitingpayment.png'); ?>" alt="" title="" style="margin-bottom: 16px;
                                            max-height: 128px;
                                            margin-left: auto;
                                            margin-right: auto;
                                            display: block;">
                                    </td>
                                </tr>

                                <tr style="margin:0;padding:0">
                                    <td style="margin:0;padding:0">
                                        <h5 style="line-height:32px;color:#666;font-weight:700;font-size:24px;margin:0 0 20px;padding:0">Segera selesaikan pembayaran Anda</h5>
                                        <p style="font-weight:normal;color:#999;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0">
                                            Hai <?php echo $customer->fullname; ?>,<br>
                                            Terimakasih atas kepercayaanmu berbelanja di <?php echo settings('store_name'); ?> pada tanggal <?php echo get_date_indo_full($order->date_added); ?>. Berikut adalah detail tagihanmu:
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table style="width:100%;margin-bottom:24px;padding:0 20px;">
                            <thead>
                                <tr><td style="width:50%">
                                        <span style="font-size:12px;color:#666;font-weight:600">Total Pembayaran</span>
                                    </td>
                                    <td style="width:50%">
                                        <span style="font-size:12px;color:#666;font-weight:600">Batas Waktu Pembayaran</span>
                                    </td>
                                </tr></thead>
                            <tbody>
                                <tr>
                                    <td style="vertical-align:top;">

                                        <p style="font-size:16px;color:#999;margin:0 0 5px 0"><b><?php echo ($order->payment_method == 'transfer') ?rupiah($order->total_plus_kode) : rupiah($order->total);  ?></b></p>

                                    </td>
                                    <td style="vertical-align:top;">
                                        <p style="font-size:16px;color:#999;margin:0 0 5px 0"><?php echo get_date_indo_full($order->date_added); ?></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table style="width:100%;margin-bottom:24px;padding:0 20px; ">
                            <thead>
                                <tr><td style="width:50% ">
                                        <span style="font-size:12px;color:#666;font-weight:600 ">Metode Pembayaran</span>
                                    </td>
                                    <td style="width:50% ">
                                        <span style="font-size:12px;color:#666;font-weight:600 ">Referensi Pembayaran</span>
                                    </td>
                                </tr></thead>
                            <tbody>
                                <tr>
                                    <td style="vertical-align:top">
                                        <?php if($payment_method->title == 'Credit Card'):
                                            $credit_image = explode(" ",$payment_method->image); ?>

                                            <img src="<?php echo site_url('/files/images/'. $credit_image[0]); ?>" alt="" title="" style="max-height: 50px;display: block;">
                                            <img src="<?php echo site_url('/files/images/'. $credit_image[1]); ?>" alt="" title="" style="max-height: 50px;display: block;">
                                        <?php else: ?>

                                        <img src="<?php echo site_url('/files/images/'. $payment_method->image); ?>" alt="" title="" style=" max-height: 50px;display: block;">
                                        <?php endif; ?>

                                        <p style="font-size:16px;color:#999;margin:5px 0"><?php echo ($payment_method->name == 'transfer') ? 'Transfer ' . strtoupper($payment_method->title) : strtoupper($payment_method->title) ?></p>
                                    </td>
                                    <td style="vertical-align:top">
                                        <p style="font-size:16px;color:#999;margin:5px 0"><?php echo $order->code; ?></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table style="width:100%;margin-bottom:24px;padding:0 20px;">
                            <thead>
                                <tr><td style="width:50%">
                                        <span style="font-size:12px;color:#666;font-weight:600">Status Pembayaran</span>
                                    </td>
                                </tr></thead>
                            <tbody>
                                <tr>
                                    <td style="vertical-align:top">
                                        <span style="font-size:16px;color:#999;">Menunggu Pembayaran</span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <?php 
                            if($order->payment_method == 'transfer' || $order->payment_method == 'bca_va' || $order->payment_method == 'bni_va' || $order->payment_method == 'bri_va' || $order->payment_method == 'alfamart'){ 
                                    $payment_details = json_decode($order->payment_to); ?>
    
                        <table style="width:80%;margin-bottom:24px;padding:0 20px;margin-left: auto;margin-right: auto;">
                            <tbody>
                                <tr>
                                    <td>
                                        <div style="border: 1px solid #A7C22A;border-radius: 5px;padding: 10px 20px;text-align: center;">
                                            Kode Pembayaran:
                                            <?php foreach ($payment_details AS $payment_detail) { 
                                                    if($order->payment_method == 'transfer' && $payment_method->title == 'BCA'){
                                                    echo
                                                    '<b style="font-weight: bold;font-size: large;">' . $payment_detail->account_number . '</b>
                                                         <br>' .
                                                        $payment_detail->account_name .
                                                        '<br>' .
                                                        $payment_detail->branch .
                                                        '<br>
                                                        Berita: <b>' .$order->transfer_code . '</b>';
                                                    }elseif($order->payment_method == 'transfer'){
                                                      echo
                                                    '<b style="font-weight: bold;font-size: large;">' . $payment_detail->account_number . '</b>
                                                         <br>' .
                                                        $payment_detail->account_name .
                                                        '<br>' .
                                                        $payment_detail->branch;  
                                                    } 
                                                }
                                                    
                                            if($order->payment_method != 'transfer') { ?>
                                                <b style="font-weight: bold;font-size: large;"><?= $payment_details->account_number?></b>  
                                            <?php } ?>

                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <?php } ?>
                        <table style="width:100%;margin-bottom:24px;padding:0 20px;">
                            <tbody>
                                <tr>
                                    <td>
                                        <div style="border: 1px solid #e7e7e7;border-radius: 5px;padding: 0px 20px;">
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <h2 style="font-size:14px;font-weight:600;color:#666;margin:50px 0 16px 0;padding:0 20px;">Detail Pemesanan</h2>
                        <span style="width:30px;border:2px solid #FF5722;display:inline-block;margin-left:20px;"></span>
                        <table style="width:100%;margin-top:16px;margin-bottom:18px;border-bottom:1px solid #E0E0E0;padding: 0 20px;">
                            <tbody>
                                <?php foreach ($invoices->result() as $invoice) { ?>
                                    <tr>
                                        <td style="padding-top: 16px;" colspan="2">
                                            <p style="color:#97C23C;text-decoration:none;"><?php echo $invoice->code; ?></p>
                                        </td>
                                    </tr>
                                    <?php foreach ($this->main->gets('order_product', array('invoice' => $invoice->id))->result() as $product) { ?>
                                        <?php if($product->options){ 
                                                    $products_option = json_decode($product->options);
                                                    if(count($products_option) > 1 || !isset($products_option[0]->type)){
                                                        foreach($products_option as $product_option) {  ?>
                                                            <tr>
                                                                <td style="vertical-align:top">
                                                                        
                                                                    <p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;"><?php echo $product->name; ?></p>
                                                                    <p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;">
                                                                    <?php $options = json_decode($product_option->data_option);
                                                                        $count = false;
                                                                        foreach($options as $option) { ?>
                                                                            <?php if($count == true) echo ', '; ?>
                                                                            <?php echo $option->type . ' ' . $option->value; ?>
                                                                            <?php $count = true; ?>
                                                                        <?php } ?>
                                                                    </p>

                                                                    <p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:8px;"><?php echo $product_option->quantity; ?> X <?php echo rupiah($product_option->price); ?></p>
                                                                </td>
                                                                <td style="vertical-align:top;text-align:right;width:120px;">
                                                                    <span style="float:left;font-size:14px;color:#999;margin-top:5px;">Rp</span>
                                                                    <p style="float:right;font-size:14px;color:#999;margin-top:5px;"><?php echo number($product_option->quantity * $product_option->price); ?></p>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                        <?php }else{ ?>
                                                <tr>
                                                    <td style="vertical-align:top">
                                                        <p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;"><?php echo $product->name; ?></p>
                                                        <p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:8px;"><?php echo $product->quantity; ?> X <?php echo rupiah($product->price); ?></p>
                                                    </td>
                                                    <td style="vertical-align:top;text-align:right;width:120px;">
                                                        <span style="float:left;font-size:14px;color:#999;margin-top:5px;">Rp</span>
                                                        <p style="float:right;font-size:14px;color:#999;margin-top:5px;"><?php echo number($product->total); ?></p>
                                                    </td>
                                                </tr>
                                        <?php } ?>
                                    <?php } ?>
                                    <tr>
                                        <td style="vertical-align:top">
                                            <p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;">
                                                <?php
                                                if ($invoice->shipping_courier != 'pickup') {
                                                    $invoice->shipping_courier = explode('-', $invoice->shipping_courier);
                                                    $invoice->shipping_courier = strtoupper($invoice->shipping_courier[0]) . ' ' . $invoice->shipping_courier[1];
                                                } else {
                                                    $invoice->shipping_courier = 'Ambil Sendiri';
                                                }
                                                echo $invoice->shipping_courier;
                                                ?>
                                            </p>
                                        </td>
                                        <td style="vertical-align:top;text-align:right;width:120px;">
                                            <span style="float:left;font-size:14px;color:#999;margin-top:5px;">Rp</span>
                                            <p style="float:right;font-size:14px;color:#999;margin-top:5px;"><?php echo number($invoice->shipping_cost); ?></p>
                                        </td>
                                    </tr>
                                    <?php if($order->payment_method == 'transfer'){ ?>
                                         <tr>
                                            <td style="vertical-align:top">
                                                <p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;">
                                                    Kode Unik
                                                </p>
                                            </td>
                                            <td style="vertical-align:top;text-align:right;width:120px;">
                                                <span style="float:left;font-size:14px;color:#999;margin-top:5px;">Rp</span>
                                                <p style="float:right;font-size:14px;color:#999;margin-top:5px;"><?php echo number($order->unique_code); ?></p>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                        <table style="width:100%;margin-bottom:20px;padding:0 20px;">
                            <tbody>
                                <tr style="margin-bottom:16px;">
                                    <td style="vertical-align:top">
                                        <p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;font-weight:bold;">Total Pembayaran</p>
                                    </td>
                                    <td style="vertical-align:top;text-align:right;width:120px;">
                                        <span style="float:left;font-size:14px;color:#999;margin-top:5px;font-weight:bold;">Rp</span>
                                        <?php if($order->payment_method == 'transfer'){ ?>
                                            <p style="float:right;font-size:14px;color:#999;margin-top:5px;font-weight:bold;"><?php echo number($order->total_plus_kode); ?></p>
                                        <?php }else{ ?>
                                            <p style="float:right;font-size:14px;color:#999;margin-top:5px;font-weight:bold;"><?php echo number($order->total); ?></p>
                                        <?php }?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="padding:0 20px">
                            <p style="font-size:14px;color:#999;padding:24px 0 10px;margin:0;border-top: 1px solid #E0E0E0;">
                                Email dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.
                            </p>
                        </div>
                    </div>
                </td>
                <td style="margin:0;padding:0"></td>
            </tr>
        </tbody>
    </table>
</div>
