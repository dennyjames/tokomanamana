<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; ">
            <title>Menunggu Pembayaran Invoice <?php echo $order->code; ?></title>

    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;margin: 0;padding: 0;background-color: #DEE0E2;height: 100% !important;width: 100% !important;">
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 0;background-color: #DEE0E2;border-collapse: collapse !important;height: 100% !important;width: 100% !important;">
                <tr>
                    <td align="center" valign="top" id="bodyCell" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;margin: 0;padding: 0px;height: 100% !important;width: 100% !important;">
                        <!-- BEGIN TEMPLATE // -->
                        <table border="0" cellpadding="0" cellspacing="0" id="templateContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;border-collapse: collapse !important;">
                            <tr>
                                <td align="center" valign="top" class="templateHeaderContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background: #FFFFFF;">
                                    <!-- BEGIN HEADER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #FFFFFF;border-bottom: 1px solid #DDDDDD;border-collapse: collapse !important;">
                                        <tr>
                                            <td valign="top" class="headerContent" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #565656;font-family: Helvetica;font-size: 20px;font-weight: bold;line-height: 100%;padding-top: 30px;padding-right: 30px;padding-bottom: 30px;padding-left: 30px;text-align: left;vertical-align: middle;width: 100%;">
                                                <img src="<?php echo site_url(setting('logo')); ?>" style="max-width: 600px;-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;" id="headerImage" mc:label="header_image" mc:edit="header_image" mc:allowdesigner mc:allowtext alt="<?php echo setting('shop_title'); ?>" />
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END HEADER -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                    <!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody" class="templateBody" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #FFFFFF;border-collapse: collapse !important;">
                                        <tr>
                                            <td valign="top" class="bodyContent" mc:edit="body_content00" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #565656;font-family: Helvetica;font-size: 13px;line-height: 150%;padding-top: 25px;padding-right: 30px;padding-bottom: 5px;padding-left: 30px;text-align: left;background: #FFFFFF;">
                                                <h1 style="color: #0099FF;display: block;font-family: Helvetica;font-size: 17px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;">Invoice Online</h1>
                                                Terimakasih Bapak/Ibu <?php echo $customer->name; ?> telah berbelanja di <?php echo setting('company_name'); ?> pada tanggal <strong style="color: #444;"><?php echo get_date_indo_full($order->date_added); ?></strong>.
                                                <br/>
                                                <br/>
                                                Berikut detail pesanan Anda : 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="pad30" mc:edit="body_content00" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;padding: 0 30px;color: #565656;font-family: Helvetica;font-size: 13px;line-height: 150%;text-align: left;background: #FFFFFF;">
                                                <!-- BEGIN COLUMNS // -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateColumns" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                    <tr mc:repeatable>
                                                        <td align="center" valign="top" class="templateColumnContainer importantBlock" style="padding-top: 20px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border: 1px solid #ddd;width: 600px;max-width: 100%;">
                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                <tr>
                                                                    <td valign="top" class="columnContent leftColumnContent" mc:edit="left_column_content" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #565656;font-family: Helvetica;font-size: 14px;line-height: 150%;padding-top: 0;padding-right: 20px;padding-bottom: 20px;padding-left: 20px;text-align: center;">
                                                                        <h4 style="display: block;font-family: Helvetica;font-size: 12px;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 3px;margin-left: 0;color: #808080 !important;">Kode Transaksi</h4>
                                                                        <strong class="mono" style="color: #444;font-family: 'courier new',courier,monospace;font-size: 14px;"><?php echo $order->code; ?></strong>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td align="center" valign="top" class="templateColumnContainer importantBlock" style="padding-top: 20px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border: 1px solid #ddd;width: 600px;max-width: 100%;">
                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                <tr>
                                                                    <td valign="top" class="columnContent centerColumnContent" mc:edit="center_column_content" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #565656;font-family: Helvetica;font-size: 14px;line-height: 150%;padding-top: 0;padding-right: 20px;padding-bottom: 20px;padding-left: 20px;text-align: center;">
                                                                        <h4 style="display: block;font-family: Helvetica;font-size: 12px;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 3px;margin-left: 0;color: #808080 !important;">Total</h4>
                                                                        <strong style="color: #0099FF;">
                                                                            <?php echo rp($order->total); ?> </strong>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td align="center" valign="top" class="templateColumnContainer importantBlock" style="padding-top: 20px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border: 1px solid #ddd;width: 600px;max-width: 100%;">
                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                <tr>
                                                                    <td valign="top" class="columnContent rightColumnContent" mc:edit="right_column_content" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #565656;font-family: Helvetica;font-size: 14px;line-height: 150%;padding-top: 0;padding-right: 20px;padding-bottom: 20px;padding-left: 20px;text-align: center;">
                                                                        <h4 style="display: block;font-family: Helvetica;font-size: 12px;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 3px;margin-left: 0;color: #808080 !important;">Status</h4>
                                                                        <strong style="color: #444;">
                                                                            <span style="color:#ff1c2d"><?php echo $order->order_status_name; ?></span> </strong>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- // END COLUMNS -->
                                            </td>
                                        </tr> </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                    <!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody" class="templateBody" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #FFFFFF;border-collapse: collapse !important;">
                                        <tr>
                                            <td valign="top" class="bodyContent" mc:edit="body_content00" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #565656;font-family: Helvetica;font-size: 13px;line-height: 150%;padding-top: 25px;padding-right: 30px;padding-bottom: 25px;padding-left: 30px;text-align: left;background: #FFFFFF;">
                                                <h2 style="display: block;font-family: Helvetica;font-size: 15px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;color: #0099FF !important;">Pesanan dikirim ke</h2>
                                                <!-- BEGIN COLUMNS // -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                    <tr mc:repeatable>
                                                        <td valign="top" class="templateColumnContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;max-width: 100%;">
                                                            <strong style="color: #444;"><?php echo $order->shipping_name; ?></strong><br/>
                                                            <strong style="color: #444;"><?php echo $order->shipping_phone; ?></strong><br/>
                                                        </td>
                                                        <td valign="top" class="templateColumnContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;max-width: 100%;">
                                                            <?php echo nl2br($order->shipping_address); ?><br/>
                                                            <?php echo $order->shipping_province_name . ' - ' . $order->shipping_city_name; ?> <br/>
                                                            <?php echo $order->shipping_postcode; ?> 
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- // END COLUMNS -->
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                    <!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody" class="templateBody" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #FFFFFF;border-collapse: collapse !important;">
                                        <tr>
                                            <td valign="top" class="bodyContent" mc:edit="body_content00" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #565656;font-family: Helvetica;font-size: 13px;line-height: 150%;padding-top: 25px;padding-right: 30px;padding-bottom: 25px;padding-left: 30px;text-align: left;background: #FFFFFF;">
                                                <h2 style="display: block;font-family: Helvetica;font-size: 15px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;color: #0099FF !important;">Cara pembayaran</h2>
                                                Pesanan Anda akan segera diproses setelah melakukan pelunasan dengan cara pembayaran transfer bank ke nomor rekening resmi kami yang tertera di bawah ini.
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="bodyContent pad30" mc:edit="body_content00" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #565656;font-family: Helvetica;font-size: 13px;line-height: 150%;padding-top: 25px;padding-right: 30px;padding-bottom: 25px;padding-left: 30px;text-align: left;background: #FFFFFF;padding: 0 30px;">
                                                <!-- BEGIN COLUMNS // -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateColumns" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                    <tr mc:repeatable>
                                                        <td align="center" valign="top" class="templateColumnContainer importantBlock" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border: 1px solid #ddd;width: 600px;max-width: 100%;">
                                                            <table border="0" cellpadding="10" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                <tr>
                                                                    <td valign="top" class="columnContent" mc:edit="left_column_content" style="text-align: center;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                        <img src="<?php echo site_url('themes/' . setting('shop_theme') . '/images/bca.png'); ?>" alt="BCA" style="-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;display: inline;max-width: 100px;"> <br/>
                                                                            <strong style="color: #444;">1091 44 3151</strong>
                                                                            <h4 style="display: block;font-family: Helvetica;font-size: 12px;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 3px;margin-left: 0;color: #808080 !important;">
                                                                                a/n Rifky Syaripudin <br/>
                                                                            </h4>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td align="center" valign="top" class="templateColumnContainer importantBlock" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border: 1px solid #ddd;width: 600px;max-width: 100%;">
                                                            <table border="0" cellpadding="10" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                <tr>
                                                                    <td valign="top" class="columnContent" mc:edit="left_column_content" style="text-align: center;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                        <img src="<?php echo site_url('themes/' . setting('shop_theme') . '/images/mandiri.png'); ?>" alt="BCA" style="-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;display: inline;max-width: 100px;"> <br/>
                                                                            <strong style="color: #444;">173 00000 289 52</strong>
                                                                            <h4 style="display: block;font-family: Helvetica;font-size: 12px;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 3px;margin-left: 0;color: #808080 !important;">
                                                                                a/n Rifky Syaripudin <br/>
                                                                            </h4>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td align="center" valign="top" class="templateColumnContainer importantBlock" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border: 1px solid #ddd;width: 600px;max-width: 100%;">
                                                            <table border="0" cellpadding="10" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                <tr>
                                                                    <td valign="top" class="columnContent" mc:edit="left_column_content" style="text-align: center;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                        <img src="<?php echo site_url('themes/' . setting('shop_theme') . '/images/bri.png'); ?>" alt="BCA" style="-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;display: inline;max-width: 100px;"> <br/>
                                                                            <strong style="color: #444;">79070 1000 912 537</strong>
                                                                            <h4 style="display: block;font-family: Helvetica;font-size: 12px;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 3px;margin-left: 0;color: #808080 !important;">
                                                                                a/n Rifky Syaripudin <br/>
                                                                            </h4>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- // END COLUMNS -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="bodyContent" mc:edit="body_content00" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #565656;font-family: Helvetica;font-size: 13px;line-height: 150%;padding-top: 5px;padding-right: 30px;padding-bottom: 25px;padding-left: 30px;text-align: left;background: #FFFFFF;">
                                                Catatan pembayaran yang perlu diketahui untuk pemrosesan pesanan Anda : 
                                                <ul>
                                                    <li>Lakukan pembayaran sesegera mungkin untuk menghindari kehabisan stok</li>
                                                    <li>Invoice ini bukan sebagai bukti booking produk pesanan, booking produk pesanan jika sudah menjadi Invoice</li>
                                                    <li>Senin - Jumat : Pembayaran melebihi pukul 17:00, pesanan akan diproses keesokan harinya</li>
                                                    <li>Sabtu : Pembayaran melebihi pukul 15:00, pesanan akan diproses mulai Senin</li>
                                                    <li>Pembayaran pada hari Minggu / Hari Besar, pesanan akan diproses keesokan harinya pada hari kerja</li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                    <!-- BEGIN BODY // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody" class="templateBody" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;background-color: #FFFFFF;border-collapse: collapse !important;">
                                        <tr>
                                            <td valign="top" class="bodyContent" mc:edit="body_content00" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #565656;font-family: Helvetica;font-size: 13px;line-height: 150%;padding-top: 25px;padding-right: 30px;padding-bottom: 25px;padding-left: 30px;text-align: left;background: #FFFFFF;">
                                                <h2 style="display: block;font-family: Helvetica;font-size: 15px;font-style: normal;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;color: #0099FF !important;">Barang yang dipesan</h2>
                                                <!-- BEGIN COLUMNS // -->
                                                <!-- BEGIN COLUMNS // -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateColumns" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                    <?php foreach ($products->result() as $product) { ?>
                                                        <tr mc:repeatable>
                                                            <td align="center" valign="top" class="templateColumnContainer importantBlock" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border: 1px solid #ddd;width: 600px;max-width: 100%;">
                                                                <table border="0" cellpadding="20" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                    <tr>
                                                                        <td valign="top" class="productColumnContent" mc:edit="left_column_content" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;padding: 15px;padding-bottom: 10px;">
                                                                            <a href="<?php echo seo_url('catalog/products/view/' . $product->product); ?>" target="_BLANK" style="text-decoration:none;">
                                                                                <h3 style="display: block;font-family: Helvetica;font-size: 12px;font-weight: bold;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 10px;margin-left: 0;text-align: left;color: #002166 !important; line-height:16px">
                                                                                    <?php echo $product->name; ?> </h3>
                                                                            </a>
                                                                            <!-- BEGIN COLUMNS // -->
                                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateColumns" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                                <tr mc:repeatable>
                                                                                    <td align="center" valign="middle" class="templateColumnContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;max-width: 100%;">
                                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                                            <tr>
                                                                                                <td valign="top" mc:edit="right_column_content" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                                                    <a href="<?php echo seo_url('catalog/products/view/' . $product->product); ?>" target="_BLANK" style="text-decoration:none;">
                                                                                                        <img src="<?php echo get_server_name($product->image); ?>" alt="" style="-ms-interpolation-mode: bicubic;border: 0;height: auto;line-height: 100%;outline: none;text-decoration: none;display: inline;max-width: 60px;">
                                                                                                    </a>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td align="center" valign="middle" class="templateColumnContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;max-width: 100%;">
                                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                                            <tr>
                                                                                                <td valign="top" class="productDetail" mc:edit="left_column_content" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                                                    <h4 style="display: block;font-family: Helvetica;font-size: 11px;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 3px;margin-left: 0;color: #808080 !important;">SKU</h4>
                                                                                                    <strong style="color: #444;font-size: 11px;"><?php echo $product->sku; ?></strong>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td align="center" valign="middle" class="templateColumnContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;max-width: 100%;">
                                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                                            <tr>
                                                                                                <td valign="top" class="productDetail" mc:edit="center_column_content" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                                                    <h4 style="display: block;font-family: Helvetica;font-size: 11px;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 3px;margin-left: 0;color: #808080 !important;">Harga</h4>
                                                                                                    <strong style="color: #444;font-size: 11px;"><?php echo number($product->price) ?></strong>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td align="center" valign="middle" class="templateColumnContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;max-width: 100%;">
                                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                                            <tr>
                                                                                                <td valign="top" class="productDetail" mc:edit="right_column_content" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                                                    <h4 style="display: block;font-family: Helvetica;font-size: 11px;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 3px;margin-left: 0;color: #808080 !important;">Qty</h4>
                                                                                                    <strong style="color: #444;font-size: 11px;"><?php echo $product->quantity; ?></strong>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td align="center" valign="middle" class="templateColumnContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;max-width: 100%;">
                                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                                            <tr>
                                                                                                <td valign="top" class="productDetail" mc:edit="right_column_content" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                                                    <h4 style="display: block;font-family: Helvetica;font-size: 11px;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 3px;margin-left: 0;color: #808080 !important;">Berat</h4>
                                                                                                    <strong style="color: #444;font-size: 11px;"><?php echo $product->weight; ?> kg</strong>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                    <td align="center" valign="middle" class="templateColumnContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;max-width: 100%;">
                                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                                            <tr>
                                                                                                <td valign="top" class="productDetail" mc:edit="right_column_content" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                                                    <h4 style="display: block;font-family: Helvetica;font-size: 11px;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 3px;margin-left: 0;color: #808080 !important;">Total</h4>
                                                                                                    <strong style="color: #444;font-size: 11px;"><?php echo number($product->total); ?></strong>
                                                                                                </td>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <!-- // END COLUMNS -->
                                                                            <h4 style="display: block;font-family: Helvetica;font-size: 12px;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 5px;margin-right: 0;margin-bottom: 3px;margin-left: 0;color: #808080 !important;">Garansi : <strong style="color:#333"><?php echo ($product->warranty) ? $product->warranty : '-'; ?></strong></h4>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </table>
                                                <!-- // END COLUMNS --><br/><!-- BEGIN COLUMNS // -->
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateColumns" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                    <tr mc:repeatable>
                                                        <td align="center" valign="top" class="templateColumnContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;max-width: 100%;">
                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                <tr>
                                                                    <td valign="top" class="productColumnContent" mc:edit="left_column_content" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;padding: 15px;padding-bottom: 10px;padding-top:0px;">
                                                                        <!-- BEGIN COLUMNS // -->
                                                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateColumns" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                            <tr mc:repeatable>
                                                                                <td align="right" valign="middle" class="templateColumnContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 2200px;max-width: 100%;">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                                        <tr>
                                                                                            <td valign="top" mc:edit="right_column_content" align="right" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                                                <h4 style="display: block;font-family: Helvetica;font-size: 12px;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 3px;margin-left: 0;color: #808080 !important;">Total Belanjaan</h4>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                <td align="right" valign="middle" class="templateColumnContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;max-width: 100%;">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;margin-bottom:3px;">
                                                                                        <tr>
                                                                                            <td valign="top" mc:edit="right_column_content" align="right" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                                                <strong style="color: #444;"><?php echo number($order->total - $order->shipping_cost); ?></strong>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>

                                                                            <!-- SHIPPING COST -->
                                                                            <tr mc:repeatable>
                                                                                <td align="right" valign="middle" class="templateColumnContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;max-width: 100%;">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                                        <tr>
                                                                                            <td valign="top" mc:edit="right_column_content" align="right" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                                                <h4 style="display: block;font-family: Helvetica;font-size: 12px;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 3px;margin-left: 0;color: #808080 !important;">
                                                                                                    Biaya Pengiriman <?php echo $order->shipping_courier_name; ?> (<?php echo number(pembulatan_berat($order->shipping_weight, 0.3)); ?> kg x <?php echo number($order->shipping_cost / pembulatan_berat($order->shipping_weight, 0.3)); ?>) </h4>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                <td align="right" valign="middle" class="templateColumnContainer" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;max-width: 100%;">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                                        <tr>
                                                                                            <td valign="top" mc:edit="right_column_content" align="right" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                                                <strong style="color: #444;">
                                                                                                    <?php echo number($order->shipping_cost); ?> </strong>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                            <!-- KODE UNIK -->
                                                                            <tr mc:repeatable>
                                                                                <td align="right" valign="middle" class="templateColumnContainer" style="border-top: 1px solid #ddd;padding-top: 5px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;max-width: 100%;">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                                        <tr>
                                                                                            <td valign="top" mc:edit="right_column_content" align="right" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                                                <h4 style="display: block;font-family: Helvetica;font-size: 12px;font-weight: normal;line-height: 100%;letter-spacing: normal;margin-top: 0;margin-right: 0;margin-bottom: 3px;margin-left: 0;color: #808080 !important;">Grand Total</h4>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                                <td align="right" valign="middle" class="templateColumnContainer" style="border-top: 1px solid #ddd;padding-top: 5px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;width: 600px;max-width: 100%;">
                                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                                                                        <tr>
                                                                                            <td valign="top" mc:edit="right_column_content" align="right" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                                                                                <strong style="color: #0099FF;"><?php echo rp($order->total); ?></strong>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- // END COLUMNS -->
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table> <!-- // END COLUMNS -->
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END BODY -->
                                </td>
                            </tr> 
                            <tr>
                                <td align="center" valign="top" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                                    <!-- BEGIN FOOTER // -->
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;border-collapse: collapse !important;">
                                        <tr>
                                            <td valign="top" class="footerContent" mc:edit="footer_content01" style="padding-bottom: 15px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #808080;font-family: Helvetica;font-size: 12px;line-height: 150%;padding-top: 20px;padding-right: 20px;padding-left: 20px;text-align: center;">
                                                Apabila ada pertanyaan, silahkan hubungi kami di <br/>
                                                <strong style="color: #444;"><?php echo setting('phone'); ?></strong> atau <strong style="color: #444;"><?php echo setting('email'); ?></strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" class="footerContent" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;mso-table-lspace: 0pt;mso-table-rspace: 0pt;color: #808080;font-family: Helvetica;font-size: 12px;line-height: 150%;padding-right: 20px;padding-bottom: 20px;padding-left: 20px;text-align: center;" mc:edit="footer_content02">
                                                Email ini dikirimkan karena Anda melakukan interaksi dengan servis di <?php echo setting('company_name'); ?>
                                                <br/>
                                                <a href="<?php echo site_url(); ?>" target="_blank" class="link" style="-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;color: #606060;font-weight: normal;text-decoration: underline;">www.tokokarawang.com</a>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // END FOOTER -->
                                </td>
                            </tr>
                        </table>
                        <!-- // END TEMPLATE -->
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>