<div bgcolor="#FFFFFF" style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;width:100%!important;height:100%;font-size:14px;color:#404040;margin:0;padding:0">
    <table style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0" bgcolor="transparent">
        <tbody>
            <tr style="margin:0;padding:0">
                <td style="margin:0;padding:0"></td>
                <td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0" bgcolor="#FFFFFF">
                    <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;border:1px solid #e7e7e7">
                        <table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:20px;" bgcolor="transparent">
                            <tbody>
                                <tr style="margin:0;padding:0">
                                    <td>
                                        <img src="<?php echo site_url('assets/frontend/images/logo.png'); ?>" alt="" style="margin:10px 0">
                                    </td>
                                </tr>

                                <tr style="margin:0;padding:0">
                                    <td style="margin:0;padding:0">
                                        <h5 style="line-height:32px;color:#666;font-weight:700;font-size:24px;margin:0 0 20px;padding:0">Permintaan Reset Password</h5>
                                        <p style="font-weight:normal;color:#999;font-size:16px;line-height:1.6;margin:0 0 20px;padding:0">
                                            Halo, <?php echo $user->fullname; ?>.<br>
                                            Kami telah menerima permintaan Anda untuk mereset password akun Anda di TokoManaMana.com<br>
                                            Selanjutnya silahkan klik link dibawah ini dan ikuti instruksi selanjutnya untuk mereset password Anda.
                                        </p>
                                        <a href="<?php echo site_url('auth/reset_password/'.$password_code); ?>" style="font-weight: bold; line-height: 100%; text-align: center; text-decoration: none; color: #FFF; display: block; background-color: #97c43c; padding: 20px">Reset Password</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </td>
                <td style="margin:0;padding:0"></td>
            </tr>
        </tbody>
    </table>
</div>