<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Model extends CI_Model
{

    function get_from_query($query){
        return $this->db->query($query);
    }

    function get_data($select='*', $tabel, $join=null, $where=null, $groupby='', $orderby='', $limit=0, $offset=0, $where1=null, $like=null){
        $database_ = $this->db;
        $select = $select == '' ? '*' : $select;
        $database_->select($select);
        $database_->from($tabel);
        if($join != null){
            for($a=0; $a<count($join); $a++){
                $exp = explode('-', $join[$a]);
                $database_->join($exp[0], $exp[1], 'left');
            }
        }
        if($where != null){
            $database_->where($where);
        }
        if($where1 != null){
            for($a=0; $a<count($where1); $a++){
              if($where1[$a] != ''){
                $database_->where("(".$where1[$a].")");
              }
            }
        }
        if($groupby != ''){
            $database_->group_by($groupby);
        }
        if($orderby != ''){
            $database_->order_by($orderby);
        }
        if($limit != 0){
            $database_->limit($limit, $offset);
        }
        if($like != null){
            $database_->like($like['key'], $like['value']);
        }
        return $database_->get();
    }

    public function get_id($val=''){
        $y = date('Y');
        $m = date('m');
        if($val == 'peh'){
            $params = 'ORDER/REWARD/';
            $q_peh = $this->get_data('kode', 'point_exchange_history', null, null, '', 'kode desc');
            if($q_peh->num_rows() > 0){
                if($q_peh->row()->kode != ''){
                    $e = explode('/', $q_peh->row()->kode);
                    if($e[2] == $y.$m){
                        $e0 = intval($e[3]) + 1;
                        if($e0 < 10){
                            $kode = $params.$y.$m.'/000'.$e0;
                        }else if($e0 < 100){
                            $kode = $params.$y.$m.'/00'.$e0;
                        }else if($e0 < 1000){
                            $kode = $params.$y.$m.'/0'.$e0;
                        }else{
                            $kode = $params.$y.$m.'/'.$e0;
                        }
                    }else{
                        $kode = $params.$y.$m.'/0001';
                    }
                }else{
                    $kode = $params.$y.$m.'/0001';
                }                
            }else{
                $kode = $params.$y.$m.'/0001';
            }

            return $kode;
        }
    }

    function get_data_all($tabel, $order='', $by='', $limit='', $p_count=0, $start='')
    {
        if($p_count == 0){
            $this->db->select('*');
            $this->db->from($tabel);
            if($order!=''){
                $this->db->order_by($order, $by);
            }
            if($limit!=''){
                $this->db->limit($limit, $start);
            }

            $query = $this->db->get();
        }else{
            $this->db->from($tabel);
            $query = $this->db->count_all_results();
        }

        return $query;
    }

    function get_data_where1($select, $tabel, $join='', $where_value='', $group_by='', $order_by=''){
        if($group_by != ''){
            $group_by = 'group by '.$group_by;
        }
        if($order_by != ''){
            $order_by = 'order by '.$order_by;
        }
        return $this->db->query('
            select '.$select.'
            from '.$tabel.' '.$join.' '.$where_value.' '.$group_by.' '.$order_by);
    }

    function get_data_where($tabel, $where=array(), $value=array(), $params=null, $order='', $by='', $p_count=0, $limit='', $start='')
    {
        if($p_count == 0){
            $this->db->select('*');
            $this->db->from($tabel);
            if($params==null){
                for ($i=0; $i < count($where); $i++) {
                    $this->db->where($where[$i], $value[$i]);
                }
            }else{
                for ($i=0; $i < count($where); $i++) {
                    if($params[$i] == 'and'){
                        $this->db->where($where[$i], $value[$i]);
                    }else if($params[$i] == 'or'){
                        $this->db->or_where($where[$i], $value[$i]);
                    }else if($params[$i] == 'nand'){
                        $this->db->where($where[$i].' != ', $value[$i]);
                    }else if($params[$i] == 'nor'){
                        $this->db->or_where($where[$i]. ' != ', $value[$i]);
                    }
                }
            }
            if($order!=''){
                $this->db->order_by($order, $by);
            }
            if($limit!=''){
                $this->db->limit($limit, $start);
            }
            $query = $this->db->get();
        }else{
            $this->db->from($tabel);
            if($params==null){
                for ($i=0; $i < count($where); $i++) {
                    $this->db->where($where[$i], $value[$i]);
                }
            }else{
                for ($i=0; $i < count($where); $i++) {
                    if($params == 'and'){
                        $this->db->where($where[$i], $value[$i]);
                    }else if($params == 'or'){
                        $this->db->or_where($where[$i], $value[$i]);
                    }else if($params == 'nand'){
                        $this->db->where($where[$i].' != ', $value[$i]);
                    }else if($params == 'nor'){
                        $this->db->or_where($where[$i]. ' != ', $value[$i]);
                    }
                }
            }
            $query = $this->db->count_all_results();
        }

        return $query;
    }

    function insert_data($tabel, $data)
    {
        $query = $this->db->insert($tabel, $data);

        if($query)
            return true;
        else
            return false;
    }

    function update_data($tabel, $data, $id)
    {
         $query = $this->db->update($tabel, $data, $id);

         if($query)
            return true;
         else
            return false;
    }

    function delete_data($tabel, $where, $value)
    {
        $this->db->where($where, $value);

        $query = $this->db->delete($tabel);

        if($query)
            return true;
        else
            return false;
    }

    function delete_data1($tabel, $where)
    {
        $this->db->where($where);

        $query = $this->db->delete($tabel);

        if($query)
            return true;
        else
            return false;
    }

    function get_data_like($tabel, $like, $value, $order, $by)
    {
        $this->db->select('*');
        $this->db->from($tabel);
        $this->db->like($like, $value);
        $this->db->order_by($order, $by);
        $query = $this->db->get();
        return $query;
    }

    function get_data_where_like($tabel, $where1, $value1, $where2, $value2, $order, $by)
    {
        $this->db->select('*');
        $this->db->from($tabel);
        $this->db->where($where1, $value1);
        $this->db->like($where2, $value2);
        $this->db->order_by($order, $by);
        $query = $this->db->get();
        return $query;
    }

    public function cek_cart_detail($email){
        $join = array(
            0 => 'point_reward_cart pcr-pcr.id=pcrd.id_cart',
            1 => 'customers c-c.id=pcr.id_customer'
        );

        $where = array(
            'c.email' => $email
        );
        $q = $this->get_data('pcrd.status, pcrd.id', 'point_reward_cart_detail pcrd', $join, $where);

        if($q->num_rows() > 0){
            if(!$this->cek_periode_tukar_point()){
                $this->delete_data1('point_reward_cart_detail', array('id' => $q->row()->id));
                return '';
            }

            if($q->row()->status != '' && $q->row()->status != null){
                return $q->row()->status;
            }else{
                return '';
            }
        }else{
            return '';
        }
    }

    public function cek_periode_tukar_point(){
        $q = $this->get_data('', 'settings', null, array('key' => 'periode_tukar_point'));
        if($q->num_rows() > 0){
            $value = $q->row()->value;
            $exp = explode(';', $value);

            $date_start = date('Y/m/d', strtotime($exp[0].'/'.$exp[1].'/'.$exp[2]));
            $date_end = date('Y/m/d', strtotime($exp[3].'/'.$exp[4].'/'.$exp[5]));
            if(date('Y/m/d') >= $date_start && date('Y/m/d') <= $date_end){
                return true;
            }else if(date('Y/m/d') > $date_end){
                $this->delete_data1('settings', array('key' => 'periode_tukar_point'));
                $this->update_data('point', array('point' => 0), array('id >' => 0));
                $this->update_data('reward_claim_temp', array('status_periode_aktif' => '0'), array('status_periode_aktif' => '1'));
            }
        }

        return false;
    }

    public function cek_point_customer($email='', $id_customer='', $id_reward='', $poin_reward=0){
        $point = 0;
        if($email != ''){
            $q_point = $this->get_data('p.point', 'point p', array(0 => 'customers c-c.id=p.id_customer'), array('c.email' => $email));
            if($q_point->num_rows() > 0){
                $point = $q_point->row()->point;
            }
        }else if($id_customer != ''){
            $q_point = $this->get_data('point', 'point', null, array('id_customer' => $id_customer));
            if($q_point->num_rows() > 0){
                $point = $q_point->row()->point;
            }
        }
        if($id_reward == ''){
            return $point >= $poin_reward;
        }

        $q_reward = $this->get_data('point', 'reward', null, array('id' => $id_reward));

        if($q_reward->num_rows() > 0){
            return $point >= $q_reward->row()->point;
        }
        return false;
    }

    public function cek_stok_reward($id_reward=''){
        if($id_reward != ''){
            $q = $this->get_data('qty', 'reward', null, array('id' => $id_reward));
            if($q->num_rows() > 0){
                return $q->row()->qty > 0;
            }
        }
        return false;
    }
}
