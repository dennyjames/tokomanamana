<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Order_meta extends MY_Model {

    public $table = 'order_meta';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
    }
}
