<?php

(defined('BASEPATH')) or exit('No direct script access allowed');

class Menu {

    public function __construct() {
        $this->ci = & get_instance();
    }

    function main_menu($mobile = false) {
        $this->ci->data['categories'] = $this->ci->category_model->get_categories(0);
        $this->ci->cache->delete('main_menu');
        $this->ci->cache->delete('main_menu_m');
        if (!$mobile) {
            if ($this->ci->cache->get('main_menu')) {
                $menu = $this->ci->cache->get('main_menu');
            } else {
                $menu = $this->ci->load->view('menu/main', $this->ci->data, true);
                $this->ci->cache->save('main_menu', $menu, 86400);
            }
        } else {
            if ($this->ci->cache->get('main_menu_m')) {
                $menu = $this->ci->cache->get('main_menu_m');
            } else {
                $menu = $this->ci->load->view('menu/main_m', $this->ci->data, true);
                $this->ci->cache->save('main_menu_m', $menu, 86400);
            }
        }
        return $menu;
    }

}
