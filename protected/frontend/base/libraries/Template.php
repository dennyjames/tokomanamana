<?php

(defined('BASEPATH')) or exit('No direct script access allowed');

class Template {

    public function __construct() {
        $this->ci = & get_instance();
    }

    public function _init($default = 'default') {
        $this->ci->output->set_template($default);

        $this->ci->load->css('https://fonts.googleapis.com/css?family=Nunito:300,400,500,600,700');
        $this->ci->load->css('https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700');
        $this->ci->load->css('https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i');
        $this->ci->load->css('assets/frontend/css/bootstrap.min.css');
        $this->ci->load->css('assets/frontend/css/fonts.googleapis.css');
        $this->ci->load->css('assets/frontend/css/font-awesome.min.css');
        $this->ci->load->css('assets/frontend/css/icon-font.min.css');
        // $this->ci->load->css('assets/frontend/css/social-buttons.css');
        $this->ci->load->css('assets/frontend/css/cs-3.styles.css');
        $this->ci->load->css('assets/frontend/css/owl.carousel.min.css');
        $this->ci->load->css('assets/frontend/css/spr.css');
        $this->ci->load->css('assets/frontend/css/slideshow-fade.css');
        $this->ci->load->css('assets/frontend/css/cs.animate.css');
        $this->ci->load->css('assets/frontend/css/custom.css');

        $this->ci->load->js('assets/frontend/js/jquery.min.js');
        // $this->ci->load->js('assets/frontend/js/classie.js');
        // $this->ci->load->js('assets/frontend/js/cs.optionSelect.js');
        $this->ci->load->js('assets/frontend/js/cs.script.js');
       // $this->ci->load->js('assets/frontend/js/jquery.currencies.min.js');
        // $this->ci->load->js('assets/frontend/js/jquery.zoom.min.js');
        // $this->ci->load->js('assets/frontend/js/linkOptionSelectors.js');
        $this->ci->load->js('assets/frontend/js/owl.carousel.min.js');
        // $this->ci->load->js('assets/frontend/js/scripts.js');
        // $this->ci->load->js('assets/frontend/js/social-buttons.js');
        $this->ci->load->js('assets/frontend/js/bootstrap.min.js');
        $this->ci->load->js('assets/frontend/js/jquery.touchSwipe.min.js');
        $this->ci->load->js('assets/frontend/js/blockui.min.js');
        $this->ci->load->js('assets/frontend/js/jquery.form.min.js');
    }

    public function alert($type, $message){
        return '<div class="alert alert-'.$type.' fade in alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> '.$message.' </div>';
    }

}
