<?php

$lang['cart_text'] = 'Keranjang Belanja';
$lang['cart_text_empty'] = 'Keranjang Belanja Kosong';
$lang['cart_text_summary'] = 'Rekap Belanjaan Anda';

$lang['cart_field_product'] = 'Produk';
$lang['cart_field_price'] = 'Harga';
$lang['cart_field_quantity'] = 'Kuantitas';
$lang['cart_field_total'] = 'Total';

$lang['checkout_address_text'] = 'Alamat Pengiriman';
$lang['checkout_shipping_text'] = 'Pilih Jasa Pengiriman';
$lang['checkout_payment_text'] = 'Cara Pembayaran';
$lang['checkout_finish_text'] = 'Selesai';
$lang['checkout_finish_say_thanks_text'] = 'Terimakasih telah belanja di %s';

$lang['checkout_shipping_address_text'] = 'Alamat Pengiriman';
$lang['checkout_shipping_courrier_text'] = 'Jasa Pengiriman';
$lang['checkout_shipping_field_name'] = 'Nama Penerima';
$lang['checkout_shipping_field_phone'] = 'No. Handphone';
$lang['checkout_shipping_field_address'] = 'Alamat';
$lang['checkout_shipping_field_province'] = 'Provinsi';
$lang['checkout_shipping_field_city'] = 'Kota/Kabupaten';
$lang['checkout_shipping_field_district'] = 'Kecamatan';
$lang['checkout_shipping_field_postcode'] = 'Kode Pos';

//$lang['payment_Paid'] = 'Sudah Dibayar';
//$lang['payment_Failed'] = 'Gagal';
//$lang['payment_Paid'] = 'Sudah Dibayar';