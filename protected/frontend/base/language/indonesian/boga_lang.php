<?php

$lang['categories'] = 'Semua Kategori';
$lang['search_product'] = 'Cari produk ...';
$lang['contact_us'] = 'Kontak Kami';
$lang['my_account'] = 'Akun Saya';
$lang['login_register'] = 'Login/Register';
$lang['register'] = 'Register';
$lang['login'] = 'Login';
$lang['logout'] = 'Logout';

$lang['help'] = 'Bantuan';
$lang['forgot_password'] = 'Lupa Password';
$lang['service_center'] = 'Pusat Servis';
$lang['warranty'] = 'Garansi';
$lang['how_to_buy'] = 'Cara Belanja';
$lang['term_condition'] = 'Syarat & Ketentuan';
$lang['privacy'] = 'Kebijakan Privasi';
$lang['about_us'] = 'Tentang Kami';
$lang['store_location'] = 'Lokasi Toko';

$lang['button_buy'] = 'Beli';

$lang['account_order_history'] = 'Riwayat Pesanan';
$lang['account_profile'] = 'Profil Saya';
$lang['account_security'] = 'Ubah Password';
$lang['account_wishlist'] = 'Wishlist';
$lang['favourite_shop'] = 'Toko Favorit';
$lang['account_review'] = 'Ulasan';
$lang['account_address'] = 'Alamat Pengiriman';
$lang['account_payment_confirmation'] = 'Konfirmasi Pembayaran';
$lang['account_balance'] = 'Saldo';

$lang['January'] = 'Januari';
$lang['February'] = 'Februari';
$lang['March'] = 'Maret';
$lang['April'] = 'April';
$lang['May'] = 'Mei';
$lang['June'] = 'Juni';
$lang['July'] = 'Juli';
$lang['August'] = 'Agustus';
$lang['September'] = 'September';
$lang['October'] = 'Oktober';
$lang['November'] = 'Nopember';
$lang['December'] = 'Desember';

$lang['notification_waiting_payment'] = 'Menunggu Pembayaran';
$lang['notification_waiting_confirm'] = 'Menunggu Konfirmasi';
$lang['notification_order_processed'] = 'Pesanan Diproses';
$lang['notification_order_shipped'] = 'Sedang Dikirim';
$lang['notification_order_finish'] = 'Sampai Tujuan';