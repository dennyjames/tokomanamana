<?php

$lang['name'] = 'Nama Lengkap';
$lang['email'] = 'Email';
$lang['phone'] = 'No. Handphone';
$lang['address'] = 'Alamat';
$lang['province'] = 'Provinsi';
$lang['city'] = 'Kota';
$lang['district'] = 'Kecamatan';
$lang['postcode'] = 'Kode Pos';
$lang['old_password'] = 'Password Lama';
$lang['new_password'] = 'Password Baru';
$lang['re_password'] = 'Tulis Ulang Password Baru';

$lang['code'] = 'Kode Pesanan';
$lang['note'] = 'Catatan';
$lang['to'] = 'Ke Rekening';
$lang['total'] = 'Jumlah Bayar';
$lang['date'] = 'Tanggal Bayar';
$lang['Pending'] = '<span class="label label-warning">Menunggu Pembayaran</span>';
$lang['Expired'] = '<span class="label label-danger">Kadaluarsa</span>';
$lang['Cancel'] = '<span class="label label-danger">Dibatalkan</span>';
$lang['Paid'] = '<span class="label label-success">Dibayar</span>';
// $lang['Confirmed'] = '<span class="label label-primary">Dikonfirmasi</span>';
$lang['Confirmed'] = '<span class="label label-primary">Menunggu Konfirmasi</span>';
$lang['Failed'] = '<span class="label label-danger">Gagal</span>';
//$lang['Pending_label_class'] = 'Menunggu Pembayaran';
//$lang['Expired'] = 'Kadaluarsa';
//$lang['Cancel'] = 'Dibatalkan';
//$lang['Paid'] = 'Dibayar';

$lang['button_save'] = 'Simpan';
$lang['button_confirmation_save'] = 'Konfirmasi';

$lang['message_success_update_profile'] = 'Profil berhasil diperbaharui.';
$lang['message_error_update_password'] = 'Password gagal diperbaharui. Isian password lama salah.';
$lang['message_success_update_password'] = 'Password berhasil diperbaharui.';
$lang['message_success_payment_confirmation'] = 'Konfirmasi Pembayaran berhasil disimpan. Kami akan memverifikasi pembayaran Anda dan segera memprosesnya. Terimakasih';

$lang['last_seen_products'] = 'Produk Terakhir Dilihat';
