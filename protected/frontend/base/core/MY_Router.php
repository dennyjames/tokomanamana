<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Router class */
require APPPATH . "third_party/MX/Router.php";

class MY_Router extends MX_Router {

    function _set_routing() {
        // Are query strings enabled in the config file?  Normally CI doesn't utilize query strings
        // since URI segments are more search-engine friendly, but they can optionally be used.
        // If this feature is enabled, we will gather the directory/class/method a little differently


        $segments = array();
        if ($this->config->item('enable_query_strings') === TRUE AND isset($_GET[$this->config->item('controller_trigger')])) {
            if (isset($_GET[$this->config->item('directory_trigger')])) {
                $this->set_directory(trim($this->uri->_filter_uri($_GET[$this->config->item('directory_trigger')])));
                $segments[] = $this->fetch_directory();
            }

            if (isset($_GET[$this->config->item('controller_trigger')])) {
                $this->set_class(trim($this->uri->_filter_uri($_GET[$this->config->item('controller_trigger')])));
                $segments[] = $this->fetch_class();
            }

            if (isset($_GET[$this->config->item('function_trigger')])) {
                $this->set_method(trim($this->uri->_filter_uri($_GET[$this->config->item('function_trigger')])));
                $segments[] = $this->fetch_method();
            }
        }

        // Load the routes.php file.
        if (defined('ENVIRONMENT') AND is_file(APPPATH . 'config/' . ENVIRONMENT . '/routes.php')) {
            include(APPPATH . 'config/' . ENVIRONMENT . '/routes.php');
        } elseif (is_file(APPPATH . 'config/routes.php')) {
            include(APPPATH . 'config/routes.php');
        }



        $this->routes = (!isset($route) OR ! is_array($route)) ? array() : $route;
        unset($route);


        /* warning .... hack here */
        $this->routes = $this->_db_routes($this->routes);


        // Set the default controller so we can display it in the event
        // the URI doesn't correlated to a valid controller.
        $this->default_controller = (!isset($this->routes['default_controller']) OR $this->routes['default_controller'] == '') ? FALSE : strtolower($this->routes['default_controller']);

        // Were there any query string segments?  If so, we'll validate them and bail out since we're done.
        if (count($segments) > 0) {
            return $this->_validate_request($segments);
        }

        // Fetch the complete URI string
//        $this->uri->_fetch_uri_string();

        // Is there a URI string? If not, the default controller specified in the "routes" file will be shown.
        if ($this->uri->uri_string == '') {
            return $this->_set_default_controller();
        }

        // Do we need to remove the URL suffix?
//        $this->uri->_remove_url_suffix();

        // Compile the segments into an array
//        $this->uri->_explode_segments();

        // Parse any custom routing that may exist
        $this->_parse_routes();

        // Re-index the segment array so that it starts with 1 rather than 0
//        $this->uri->_reindex_segments();
    }

    function _db_routes($routes) {

        /* ============= HACK to get dynamic route from db ============================== */
        require_once(APPPATH . 'config/database' . EXT);
        require_once(BASEPATH . "database/DB.php");

        $d_b = & DB($db['default'], true);

        $tmp = $d_b->query('SELECT * FROM seo_url')->result();

        // cars routes
        foreach ($tmp as $key => $val) {
            if ($val->keyword != '') {
                $routes[$val->keyword] = $val->query;
                /*
                  $routes[$val->slug.'/archive'] = $val->route.'/archive';
                  $routes[$val->slug.'/archive/(:num)/(:num)/page(/:num)?'] = $val->route.'/archive/$1/$2';

                  $routes[$val->slug.'/page(/:num)?'] = $val->route.'/index$1';
                  $routes[$val->slug.'/(:any)'] = $val->route.'/view/$1';
                 */
            }
        }
        /* 	   
          $routes['activity/type/(:any)'] = 'activity/activity_type/$1';
          $routes['company/workfield/(:any)'] = 'company/by_workfield_name/$1';

          $routes['gallery/photo/(:num)/(:any)']	= 'gallery/photo/view/$1';
          $routes['gallery/video/(:num)/(:any)']	= 'gallery/video/view/$1';

          $routes['clinic/ask'] = 'clinic/ask';
          $routes['clinic/thanks'] = 'clinic/thanks';
         */
        /* ================= end HACK ==================================================== */

        //var_dump($routes); exit;


        return $routes;
    }

}
