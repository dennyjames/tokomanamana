<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Xenditapp extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('xendit');
    }

    public function index() {
//        $virtual = $this->xendit->getVirtualAccountBanks();
//        print_r($virtual);
//        $balance = $this->xendit->getBalance();
//        print_r($balance);
//        echo '<br><br>';
        $order = $this->main->get('orders', array('id' => 6));
        $invoice = $this->xendit->createInvoice($order->code, $order->total, 'rifkysyaripudin@gmail.com', $order->code);
//        print_r($invoice);
//        $this->main->update('orders', array('payment_status' => 'Paid', 'payment_method' => 'POOL', 'payment_to' => 'BCA'), array('id' => 6));
    }

    public function email() {
        $data['order'] = $this->main->get('orders', array('id' => 6));
        $data['customer'] = $this->main->get('customers', array('id' => $data['order']->customer));
        $data['invoices'] = $this->main->gets('order_invoice', array('order' => $data['order']->id));
        $this->load->view('email/invoice', $data);
    }

    public function accept_payment() {
        $data = file_get_contents("php://input");
        $data = json_decode($data);
        log_message('debug', $data->status);
        if (NULL === $data) {
            echo 'Error. Data return NULL';
            exit;
        }

        if ('COMPLETED' === strtoupper($data->status)) {
            $this->main->update('orders', array('payment_status' => 'Paid', 'payment_method' => $data->payment_method, 'payment_to' => $data->bank_code), array('id' => 6));
//            $invoice = $data->external_id;
//            $transaction = $this->transaction_model->get(['invoice' => $invoice]);
//            if ($transaction) {
//                $this->transaction_meta_model->insert(['transaction_id' => $transaction->id, 'meta_name' => 'xendit_response', 'meta_value' => json_encode($data)]);
//                $this->transaction_model->update([
//                    'status' => 'Success',
//                    'payment_status' => 'Paid',
//                    'payment_time' => date('Y-m-d H:i:s'),
//                        ], ['id' => $transaction->id]);
//
//                $this->deduct_kuota($transaction);
//                $this->send_email($transaction);

            echo 'success';
        } else {
            echo 'failed, no transaction found';
        }
//        }
    }

}
