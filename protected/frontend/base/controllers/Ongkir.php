<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ongkir extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('rajaongkir');
    }

    public function province() {
        $provinces = $this->rajaongkir->province();
        $provinces = json_decode($provinces);
        $data = array();
        foreach ($provinces->rajaongkir->results as $province) {
            array_push($data, array(
                'id' => $province->province_id,
                'name' => $province->province
            ));
        }
        $this->db->insert_batch('provincies', $data);
    }

    public function city() {
        $provincies = $this->main->gets('provincies');
        foreach ($provincies->result() as $province) {
            $cities = $this->rajaongkir->city($province->id);
            $cities = json_decode($cities);
            $data = array();
            foreach ($cities->rajaongkir->results as $city) {
                array_push($data, array(
                    'id' => $city->city_id,
                    'province' => $city->province_id,
                    'type' => $city->type,
                    'name' => $city->city_name,
                    'postcode' => $city->postal_code,
                ));
            }
            $this->db->insert_batch('cities', $data);
        }
    }
    public function subdistrict() {
        $cities = $this->main->gets('cities');
        foreach ($cities->result() as $city) {
            $subdistricts = $this->rajaongkir->subdistrict($city->id);
            $subdistricts = json_decode($subdistricts);
            $data = array();
            foreach ($subdistricts->rajaongkir->results as $subdistrict) {
                array_push($data, array(
                    'id' => $subdistrict->subdistrict_id,
                    'city' => $subdistrict->city_id,
                    'name' => $subdistrict->subdistrict_name,
                ));
            }
            $this->db->insert_batch('subdistricts', $data);
        }
    }

}
