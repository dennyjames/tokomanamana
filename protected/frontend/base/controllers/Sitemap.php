<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sitemap extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        
        $data['urls'] = $this->main->gets('seo_url');

        $this->load->view('sitemap',$data);
    }

    function show_error ($code) {
        if ($code == '404') {
            $heading = '404 - Not Found';
            $message = 'Halaman yang anda tuju tidak dapat ditemukan';
            $image = '404-notfound.jpeg';
        } elseif ($code == '403') {
            $heading = '403 - Forbidden';
            $message = 'Anda tidak memiliki akses ke halaman yang anda tuju';
            $image = 'unauthorized.jpeg';
        } elseif ($code == '401') {
            $heading = '401 - Unauthorized';
            $message = 'Anda tidak memiliki akses ke halaman yang anda tuju';
            $image = 'unauthorized.jpeg';
        } elseif ($code == '408') {
            $heading = '408 - Timeout';
            $message = 'Koneksi anda terputus';
            $image = 'connection-error.jpeg';
        } elseif ($code = '410') {
            $heading = '410 - Under Maintenance';
            $message = 'Konten tidak tersedia';
            $image = 'under-maintenance.jpeg';
        }

        $data = array(
            'code' => $code, 
            'heading' => $heading,
            'message' => $message,
            'image' => get_image($image)
        );

        $this->load->view('errors/html/errors', $data);
    }

}
