<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Crons extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('date');
    }

//    public function due_date_invoice() {
//        $orders = $this->db->where('due_date <= NOW()')->where('order_status', settings('order_payment_received_status'))->get('order_invoice');
//        if ($orders->num_rows() > 0) {
//            foreach ($orders->result() as $order) {
//                $this->main->update('orders', array('payment_status' => 'Expired'), array('id' => $order->id));
//                $this->main->update('order_invoice', array('order_status' => settings('order_cancel_status')), array('order' => $order->id));
//            }
//        }
//    }

    public function manual_finish_order() {
        $order_code = $this->input->get('order_code');
        $orders = $this->main->gets('orders', ['code' => $order_code]);
        if($orders){
            $this->load->model('orders');
            $this->load->helper('text');
            $status = settings('order_finish_status');
            $res_orders = $orders->result();
            $this->data['invoice'] = $this->main->gets('order_invoice', ['order' => $res_orders[0]->id]);
            //var_dump($this->data['invoice']->result());exit();
            foreach($this->data['invoice']->result() as $invoice) {
                $this->main->update('order_invoice', array('order_status' => $status), array('id' => $invoice->id));
                $this->main->insert('order_history', array('order' => $invoice->order, 'invoice' => $invoice->id, 'order_status' => $status));

                $this->data['invoice'] = $invoice;
                $this->data['order'] = $this->orders->get($this->data['invoice']->order);
                $this->data['merchant'] = $this->main->get('merchants', array('id' => $this->data['invoice']->merchant));
                $this->data['products'] = $this->main->gets('order_product', array('invoice' => $this->data['invoice']->id));

                //update balance merchant
                $balance = $this->data['merchant']->cash_balance + $invoice->total;
                $this->main->insert('merchant_balances', array('merchant' => $invoice->merchant, 'type' => 'in', 'invoice' => $invoice->id, 'amount' => $invoice->total, 'balance' => $balance));
                $this->main->update('merchants', array('cash_balance' => $balance), array('id' => $this->data['merchant']->id));

                $customer = $this->main->get('customers', array('id' => $this->data['order']->customer));
                if($customer->email){
                    $message = $this->load->view('email/transaction/order_finish', $this->data, true);
                    $cronjob = array(
                        'from' => settings('send_email_from'),
                        'from_name' => settings('store_name'),
                        'to' => $customer->email,
                        'subject' => 'Pesanan Selesai: ' . $this->data['order']->code,
                        'message' => $message
                    );
                    $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                }
                
                if ($customer->verification_phone) {
                    $sms = json_decode(settings('sprint_sms'), true);
                    $sms['m'] = 'Pesanan ' . $this->data['order']->code . ' telah selesai. Terimakasih telah berbelanja di ' . settings('store_name');
                    $sms['d'] = $customer->phone;
                    $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                    $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $customer->phone));
                }
            }
            //var_dump($this->data['invoice']->result());
        }

    }

    public function finish_order() {
        $invoices = $this->db->where('due_date <= NOW()')->where_in('order_status', [settings('order_complete_pickup_status'), settings('order_delivered_status')])->get('order_invoice');
        
        if ($invoices->num_rows() > 0) {
            $this->load->model('orders');
            $this->load->helper('text');
            $status = settings('order_finish_status');
            foreach ($invoices->result() as $invoice) {
                $this->main->update('order_invoice', array('order_status' => $status), array('id' => $invoice->id));
                $this->main->insert('order_history', array('order' => $invoice->order, 'invoice' => $invoice->id, 'order_status' => $status));

                $this->data['invoice'] = $invoice;
                $this->data['order'] = $this->orders->get($this->data['invoice']->order);
                $this->data['merchant'] = $this->main->get('merchants', array('id' => $this->data['invoice']->merchant));
                $this->data['products'] = $this->main->gets('order_product', array('invoice' => $this->data['invoice']->id));

                //update balance merchant
                $balance = $this->data['merchant']->cash_balance + $invoice->total;
                $this->main->insert('merchant_balances', array('merchant' => $invoice->merchant, 'type' => 'in', 'invoice' => $invoice->id, 'amount' => $invoice->total, 'balance' => $balance));
                $this->main->update('merchants', array('cash_balance' => $balance), array('id' => $this->data['merchant']->id));

                $customer = $this->main->get('customers', array('id' => $this->data['order']->customer));
                if($customer->email){
                    $message = $this->load->view('email/transaction/order_finish', $this->data, true);
                    $cronjob = array(
                        'from' => settings('send_email_from'),
                        'from_name' => settings('store_name'),
                        'to' => $customer->email,
                        'subject' => 'Pesanan Selesai: ' . $this->data['order']->code,
                        'message' => $message
                    );
                    $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                }
                if ($customer->verification_phone) {
                    $sms = json_decode(settings('sprint_sms'), true);
                    $sms['m'] = 'Pesanan ' . $this->data['order']->code . ' telah selesai. Terimakasih telah berbelanja di ' . settings('store_name');
                    $sms['d'] = $customer->phone;
                    $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                    $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $customer->phone));
                }
            }
        }
    }

    public function cancel_order() {
        $orders = $this->db->where('due_date <= NOW()')->where('payment_status', 'Pending')->get('orders');
        if ($orders->num_rows() > 0) {
            $status = settings('order_cancel_status');
            foreach ($orders->result() as $order) {
                $this->main->update('orders', array('payment_status' => 'Expired'), array('id' => $order->id));
                $this->main->update('order_invoice', array('order_status' => $status), array('order' => $order->id));
                $invoices = $this->main->gets('order_invoice', array('order' => $order->id));
                if ($invoices) {
                    foreach ($invoices->result() as $invoice) {
                        $this->main->insert('order_history', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => $status));
                    }
                }
            }
        }
    }

    public function no_respon_order() {
        $this->load->model('orders');
        $invoices = $this->db->select('oi.*,m.name AS merchant_name,m.auth AS auth')->where('due_date <= NOW()')->where_in('order_status', [settings('order_payment_received_status'), settings('order_process_status')])->join('merchants m', 'm.id = oi.merchant', 'left')->get('order_invoice oi');
        if ($invoices->num_rows() > 0) {
           $status = settings('order_cancel_status');
            foreach ($invoices->result() as $invoice) {
                $this->main->update('order_invoice', array('order_status' => $status), array('id' => $invoice->id));
                $this->main->insert('order_history', array('order' => $invoice->order, 'invoice' => $invoice->id, 'order_status' => $status));
                $this->main->update('orders',array('payment_status' => 'Cancel'),array('id' => $invoice->order));

                $merchant_user = $this->main->get('merchant_users', array('id' => $invoice->auth));
                $this->data['invoice'] = $invoice;

                $message = $this->load->view('email/merchant/cancelled_order', $this->data, true);
                $cronjob = array(
                    'from' => settings('send_email_from'),
                    'from_name' => settings('store_name'),
                    'to' => $merchant_user->email,
                    'subject' => 'Pembatalan pesanan '.$invoice->code. ' karena melewati batas waktu',
                    'message' => $message
                );
                $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));

                
            }
        }
    }

    public function send_email() {
        $emails = $this->db->where('type', 'email')->get('cron_job');
        if ($emails->num_rows() > 0) {
            foreach ($emails->result() as $email) {
                $id = $email->id;
                $email = json_decode($email->content);
                if (send_mail($email->from, $email->from_name, $email->to, $email->subject, $email->message)) {
                    $this->main->delete('cron_job', array('id' => $id));
                }
            }
        }
    }

    public function send_sms() {
        $sms = $this->db->where('type', 'sms')->get('cron_job');
        if ($sms->num_rows() > 0) {
            $this->load->library('sprint');
            foreach ($sms->result() as $s) {
                $id = $s->id;
                $s = json_decode($s->content, TRUE);
                $url = $s['url'];
                $to = $s['d'];
                unset($s['url']);
                $result = $this->sprint->sms($url, $s);
                $result = explode('_', $result);
                if ($result[0] == 0) {
                    $this->main->insert('log_sms', array('content' => $to));
                    $this->main->delete('cron_job', array('id' => $id));
                }
            }
        }
    }

    public function check_duplicate_balance() {
        $q = $this->db->query('SELECT id,merchant,invoice,amount,MAX(balance) as balance,COUNT(*) c FROM merchant_balances where type="in" GROUP BY invoice,merchant HAVING c > 0');
        if ($q->num_rows() > 0) {
            foreach ($q->result() as $balance) {
                $this->data['merchant'] = $this->main->get('merchants', array('id' => $balance->merchant));
                $min_saldo = ($balance->c - 1) * $balance->amount;
                $balance_min = $this->data['merchant']->cash_balance - $min_saldo;
                $balance_min2 = $balance->balance - $min_saldo;
                $this->db->where('invoice',$balance->invoice);
                $this->db->where('merchant',$balance->merchant);
                $this->db->where('id !=',$balance->id);
                $list = $this->db->get('merchant_balances');
                if ($list->num_rows() > 0) {
                    $this->main->update('merchant_balances', array('balance' => $balance_min2), array('id' => $balance->id));
                    foreach ($list->result() as $row) {
                        $this->main->delete('merchant_balances', array('id' => $row->id)); 
                        $this->main->update('merchants', array('cash_balance' => $balance_min), array('id' => $balance->merchant));
                    }
                }
            }
        }
    }

    public function update_order_dummy() {
        if(isset($_GET['id'])){
            $invoice_no = $_GET['id'];
            $status = 8;
            $this->load->model('orders');
            $invoice = $this->main->get('order_invoice', array('code' => $invoice_no));
            $this->main->update('order_invoice', array('order_status' => $status), array('code' => $invoice_no));
            $this->main->update('order_history', array('order_status' => $status), array('invoice' => $invoice->id));
            //var_dump($inv);exit();
            $this->data['invoice'] = $invoice;
            $this->data['order'] = $this->orders->get($this->data['invoice']->order);
            $this->data['merchant'] = $this->main->get('merchants', array('id' => $this->data['invoice']->merchant));
            $this->data['products'] = $this->main->gets('order_product', array('invoice' => $this->data['invoice']->id));
            //var_dump($this->data['order']);exit();
            //update balance merchant
            $balance = $this->data['merchant']->cash_balance - $invoice->total;
            $this->main->insert('merchant_balances', array('merchant' => $invoice->merchant, 'type' => 'out', 'invoice' => $invoice->id, 'amount' => $invoice->total, 'balance' => $balance));
            $this->main->update('merchants', array('cash_balance' => $balance), array('id' => $this->data['merchant']->id));
        } else {
            echo "Invoice ID needed!";
        }
        

    }

    public function cancel_withdrawal($w_id,$amount) {
        $id = decode($w_id);
        $amount = $amount;
        $data = $this->main->get('merchant_balance_withdrawal', array('id' => $id));
        if ($data) {
            $this->main->update('merchant_balance_withdrawal', array('status' => 'Cancel'), array('id' => $id));

            $merchant = $this->main->get('merchants', array('id' => $data->merchant));
            $balance = $merchant->cash_balance + $data->amount;
            $this->main->insert('merchant_balances', array('merchant' => $data->merchant, 'type' => 'in', 'amount' => $data->amount, 'balance' => $balance,'note'=>'Pembatalan Penarikan Dana'));
            $this->main->update('merchants', array('cash_balance' => $balance), array('id' => $merchant->id));
            echo "Withdraw berhasil dicancel";
        }
        

    }

    public function check_tracking_old() {
        $this->load->model('orders');
        $this->load->helper('text');
        $orders = $this->db->where('order_status', settings('order_shipped_status'))
                ->get('order_invoice');
        $this->load->library('rajaongkir');
        $status = settings('order_finish_status');
        if ($orders->num_rows() > 0) {
            foreach ($orders->result() as $order) {
                $awb = $order->tracking_number;
                $courier = explode('-', $order->shipping_courier);
                $tracking = $this->rajaongkir->waybill($awb, $courier[0]);
                $tracking_res = json_decode($tracking)->rajaongkir;
                $this->data['invoice'] = $order;
                $this->data['order'] = $this->orders->get($this->data['invoice']->order);
                $this->data['merchant'] = $this->main->get('merchants', array('id' => $this->data['invoice']->merchant));
                $this->data['products'] = $this->main->gets('order_product', array('invoice' => $this->data['invoice']->id));
                $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['invoice']->customer));
                if($tracking_res->status->code == 200){
                    if($tracking_res->result->delivered) {
                        $pod_date = $tracking_res->result->delivery_status->pod_date;
                        $pod_time = $tracking_res->result->delivery_status->pod_time.':00';
                        $this->data['date_received'] = $pod_date.' '.$pod_time;
                        $this->data['expired_time'] = date('Y-m-d',strtotime($pod_date. ' + 2 days')).' '.$pod_time;
                        $upd_data = array('date_received' => $this->data['date_received'],
                                            'due_date_confirmation' => $this->data['expired_time'],
                                            'order_status' => settings('order_delivered_status'));
                        $this->main->update('order_invoice', $upd_data , array('id' => $order->id));
                        $message = $this->load->view('email/transaction/confirm_order', $this->data, true);
                        $cronjob = array(
                            'from' => settings('send_email_from'),
                            'from_name' => settings('store_name'),
                            'to' => $this->data['customer']->email,
                            'subject' => 'Konfirmasi Pesanan: ' . $order->code,
                            'message' => $message
                        );
                        $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                    }
                }
                //var_dump($tracking_res);
            }
        }
    }

    public function check_tracking_() {
        $this->load->model('orders');
        $this->load->helper('text');
        $orders = $this->db->where('order_status', settings('order_shipped_status'))
                ->get('order_invoice');
        $this->load->library('rajaongkir');
        $status = settings('order_finish_status');
        if ($orders->num_rows() > 0) {
            foreach ($orders->result() as $order) {
                $awb = $order->tracking_number;
                $courier = explode('-', $order->shipping_courier);
                $tracking = $this->rajaongkir->waybill($awb, strtolower($courier[0]));
                $this->main->insert('log_raja_ongkir', array('order_id' => $order->id, 'source' => 'Crons Customer'));
                $tracking_res = json_decode($tracking)->rajaongkir;
                $this->data['invoice'] = $order;
                $this->data['order'] = $this->orders->get($this->data['invoice']->order);
                $this->data['merchant'] = $this->main->get('merchants', array('id' => $this->data['invoice']->merchant));
                $this->data['products'] = $this->main->gets('order_product', array('invoice' => $this->data['invoice']->id));
                $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['invoice']->customer));
                if($tracking_res->status->code == 200){
                    if($tracking_res->result->delivered) {
                        $pod_date = $tracking_res->result->delivery_status->pod_date;
                        $pod_time = $tracking_res->result->delivery_status->pod_time.':00';
                        $this->data['date_received'] = $pod_date.' '.$pod_time;
                        $this->data['expired_time'] = date('Y-m-d',strtotime($pod_date. ' + 2 days')).' '.$pod_time;
                        $upd_data = array('date_received' => $this->data['date_received'],
                                            'due_date_confirmation' => $this->data['expired_time'],
                                            'order_status' => settings('order_delivered_status'));
                        $this->main->update('order_invoice', $upd_data, array('id' => $order->id));
                        if($this->data['customer']->email){
                            $message = $this->load->view('email/transaction/confirm_order', $this->data, true);
                            $cronjob = array(
                                'from' => settings('send_email_from'),
                                'from_name' => settings('store_name'),
                                'to' => $this->data['customer']->email,
                                'subject' => 'Konfirmasi Pesanan: ' . $order->code,
                                'message' => $message
                            );
                            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                        }
                        else{
                            if($this->data['customer']->verification_phone){
                                $sms = json_decode(settings('sprint_sms'), true);
                                $sms['m'] = 'Harap segara mengkonfirmasi penerimaan barang ' . $this->data['order']->code . '. Terimakasih telah berbelanja di ' . settings('store_name');
                                $sms['d'] = $customer->phone;
                                $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                                $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $customer->phone));
                            }

                        }
                        $order_ = $this->db->query(
                            "select * from orders where id=".$this->data['invoice']->order
                        )->row();

                        $post_ = array(
                            'data' => array(
                                'title' => 'Pesanan Sudah Sampai Ditujuan !',
                                'body' => 'Pesanan Anda dengan kode: "'.$order_->code.'" telah ditolak oleh merchant!',
                                'sound' => 'default',
                                'priority' => 'high',
                                'show_in_foreground' => true,
                                'targetScreen' => 'RiwayatDetail_'.$order_->id,
                                'vibrate' => 1,
                                'sound' => 1,
                                'large_icon' => 'http://157.230.36.113/site/files/images/notification/menunggu_konfirmasi_pembeli_2.png'
                            ),
                            'to' => $this->data['customer']->fcm_token,
                            'priority' => 10
                        );
                        //$this->response($post_, 200);
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                        //curl_setopt($ch, CURLOPT_URL, 'http://157.230.36.113/site/api/index.php/v1/checkout/sembarang');
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_));
                        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                            'Content-Type: application/json',
                            'Authorization: key=AAAAiDkugXM:APA91bEIcTPtXm8vhR85_-Dhs7sA3uzA-wuP7GJGuOUjva92qNb1fTuTOakYtP60xozgi956GJcw_FEQFDMDMuEcphxUETLWMSGhelNrU2ksJJb6hFezvghjMjKlZGpKE0lkAVhTXt5K'
                        ));
                        $output = curl_exec($ch);
                    }
                }
                //var_dump($tracking_res);
            }
        }
    }

    public function set_point(){
        $this->load->model('Model');

        $m = $this->Model;

        $join = array(
            0 => 'order_product op-op.order=oi.order and op.invoice=oi.id',
            1 => 'products p-p.id=op.product',
            2 => 'merchants m-m.id=oi.merchant'
        );
        $where0 = array('op.param_point' => '0', 'order_status' => 7);
        $q = $m->get_data('oi.*, op.product, p.category, op.id as op_id, m.parent as m_parent, op.quantity', 'order_invoice oi', $join, $where0, '', '', 50);

        foreach ($q->result() as $key) {
            $point = 0;

            //setting point for self merchant
            $where_item = array('id_general' => $key->product, 'tipe_item' => 'i', 'tipe_merchant' => 'c');
            $q_point_item = $m->get_data('', 'setting_point', null, $where_item);

            if($q_point_item->num_rows() > 0){
                $point = $q_point_item->row()->point;
            }else{
                $where_category = array('id_general' => $key->category, 'tipe_merchant' => 'c');
                $where_category0 = array(0 => "tipe_item='k' or tipe_item='sk'");
                $q_point_category = $m->get_data('', 'setting_point', null, $where_category, '', 'tipe_item asc', 0, 0, $where_category0);

                if($q_point_category->num_rows() > 0){
                    $point = $q_point_category->row()->point;
                }else{
                    $where_global = array('tipe_item' => 'g', 'id_general' => 0, 'tipe_merchant' => 'c');
                    $q_point_global = $m->get_data('', 'setting_point', null, $where_global);

                    if($q_point_global->num_rows() > 0){
                        $point = $q_point_global->row()->point;
                    }
                }
            }

            $data_insert_child = array(
                'tgl' => date('d'),
                'bulan' => date('m'),
                'tahun' => date('Y'),
                'id_order_invoice' => $key->id,
                'code' => $key->code,
                'merchant' => $key->merchant,
                'customer' => $key->customer,
                'product' => $key->product,
                'qty' => $key->quantity,
                'point' => $point * $key->quantity,
                'merchant_downline_customer' => $key->m_parent > 0 ? 1 : 0
            );

            $m->insert_data('order_point_history', $data_insert_child);

            //insert/update ke tabel point
            $cek_merchant = $m->get_data('', 'point', null, array('id_merchant' => $key->merchant));
            if($cek_merchant->num_rows() > 0){

                $data_merchant0 = array(
                    'point' => $cek_merchant->row()->point + ($point * $key->quantity),
                    'last_update' => date('Y/m/d H:i:s')
                );

                $where_merchant0['id'] = $cek_merchant->row()->id;

                $m->update_data('point', $data_merchant0, $where_merchant0);
            }else{
                $data_merchant0 = array(
                    'id_merchant' => $key->merchant,
                    'id_customer' => 0,
                    'point' => $point * $key->quantity,
                    'last_update' => date('Y/m/d H:i:s')
                );

                $m->insert_data('point', $data_merchant0);
            }

            //cek self merchant has parent

            if($key->m_parent > 0){
                $point = 0;

                //setting point for parent merchant
                $where_item1 = array('id_general' => $key->product, 'tipe_item' => 'i', 'tipe_merchant' => 'p');
                $q_point_item = $m->get_data('', 'setting_point', null, $where_item1);

                if($q_point_item->num_rows() > 0){
                    $point = $q_point_item->row()->point;
                }else{
                    $where_category1 = array('id_general' => $key->category, 'tipe_merchant' => 'p');
                    $where_category01 = array(0 => "tipe_item='k' or tipe_item='sk'");
                    $q_point_category = $m->get_data('', 'setting_point', null, $where_category1, '', 'tipe_item asc', 0, 0, $where_category01);

                    if($q_point_category->num_rows() > 0){
                        $point = $q_point_category->row()->point;
                    }else{
                        $where_global1 = array('tipe_item' => 'g', 'id_general' => 0, 'tipe_merchant' => 'p');
                        $q_point_global = $m->get_data('', 'setting_point', null, $where_global1);

                        if($q_point_global->num_rows() > 0){
                            $point = $q_point_global->row()->point;
                        }
                    }
                }

                $data_insert_child1 = array(
                    'tgl' => date('d'),
                    'bulan' => date('m'),
                    'tahun' => date('Y'),
                    'id_order_invoice' => $key->id,
                    'code' => $key->code,
                    'merchant' => $key->m_parent,
                    'customer' => $key->customer,
                    'product' => $key->product,
                    'qty' => $key->quantity,
                    'point' => $point * $key->quantity,
                    'merchant_downline_customer' => 0
                );

                $m->insert_data('order_point_history', $data_insert_child1);

                //insert/update ke parent merchant
                $cek_merchant = $m->get_data('', 'point', null, array('id_merchant' => $key->m_parent));
                if($cek_merchant->num_rows() > 0){

                    $data_merchant1 = array(
                        'point' => $cek_merchant->row()->point + ($point * $key->quantity),
                        'last_update' => date('Y/m/d H:i:s')
                    );

                    $where_merchant1['id'] = $cek_merchant->row()->id;

                    $m->update_data('point', $data_merchant1, $where_merchant1);
                }else{
                    $data_merchant1 = array(
                        'id_merchant' => $key->m_parent,
                        'id_customer' => 0,
                        'point' => $point * $key->quantity,
                        'last_update' => date('Y/m/d H:i:s')
                    );

                    $m->insert_data('point', $data_merchant1);
                }
            }

            $point = 0;

            //setting point for customer
            $where_item10 = array('id_general' => $key->product, 'tipe_item' => 'i', 'tipe_merchant' => 'cust');
            $q_point_item = $m->get_data('', 'setting_point', null, $where_item10);

            if($q_point_item->num_rows() > 0){
                $point = $q_point_item->row()->point;
            }else{
                $where_category10 = array('id_general' => $key->category, 'tipe_merchant' => 'cust');
                $where_category010 = array(0 => "tipe_item='k' or tipe_item='sk'");
                $q_point_category = $m->get_data('', 'setting_point', null, $where_category10, '', 'tipe_item asc', 0, 0, $where_category010);

                if($q_point_category->num_rows() > 0){
                    $point = $q_point_category->row()->point;
                }else{
                    $where_global10 = array('tipe_item' => 'g', 'id_general' => 0, 'tipe_merchant' => 'cust');
                    $q_point_global = $m->get_data('', 'setting_point', null, $where_global10);

                    if($q_point_global->num_rows() > 0){
                        $point = $q_point_global->row()->point;
                    }
                }
            }

            $data_insert_child1 = array(
                'tgl' => date('d'),
                'bulan' => date('m'),
                'tahun' => date('Y'),
                'id_order_invoice' => $key->id,
                'code' => $key->code,
                'merchant' => $key->m_parent,
                'customer' => $key->customer,
                'product' => $key->product,
                'qty' => $key->quantity,
                'point' => $point * $key->quantity,
                'merchant_downline_customer' => 2
            );

            $m->insert_data('order_point_history', $data_insert_child1);

            //insert/update ke customer
            $cek_customer = $m->get_data('', 'point', null, array('id_customer' => $key->customer));
            if($cek_customer->num_rows() > 0){

                $data_customer = array(
                    'point' => $cek_customer->row()->point + ($point * $key->quantity),
                    'last_update' => date('Y/m/d H:i:s')
                );

                $where_customer['id'] = $cek_customer->row()->id;

                $m->update_data('point', $data_customer, $where_customer);
            }else{
                $data_customer = array(
                    'id_merchant' => 0,
                    'id_customer' => $key->customer,
                    'point' => $point * $key->quantity,
                    'last_update' => date('Y/m/d H:i:s')
                );

                $m->insert_data('point', $data_customer);
            }

            //update param_point di order_invoice

            $data_update_param_point['param_point'] = '1';
            $where_param_point['id'] = $key->op_id;

            $m->update_data('order_product', $data_update_param_point, $where_param_point);
        }
    }

    public function finishing_order() {
        $this->db->where('due_date_confirmation <= NOW()');
        $orders =$this->db->where('order_status', settings('order_delivered_status'))
                ->get('order_invoice');
        $this->load->model('orders');
        $this->load->helper('text');
        $status = settings('order_finish_status');
        if ($orders->num_rows() > 0) {
            foreach ($orders->result() as $order) {
                $this->main->update('order_invoice', array('order_status' => $status), array('id' => $order->id));
                $this->main->insert('order_history', array('order' => $order->order, 'invoice' => $order->id, 'order_status' => $status));

                $this->data['invoice'] = $order;
                $this->data['order'] = $this->orders->get($this->data['invoice']->order);
                $this->data['merchant'] = $this->main->get('merchants', array('id' => $this->data['invoice']->merchant));
                $this->data['products'] = $this->main->gets('order_product', array('invoice' => $this->data['invoice']->id));

                //update balance merchant
                $balance = $this->data['merchant']->cash_balance + $order->total;
                $this->main->insert('merchant_balances', array('merchant' => $order->merchant, 'type' => 'in', 'invoice' => $order->id, 'amount' => $order->total, 'balance' => $balance));
                $this->main->update('merchants', array('cash_balance' => $balance), array('id' => $this->data['merchant']->id));
                // var_dump($this->data['merchant']->cash_balance);
                // var_dump($balance);
                $customer = $this->main->get('customers', array('id' => $this->data['order']->customer));
                if($customer->email){
                    $message = $this->load->view('email/transaction/order_finish', $this->data, true);
                    $cronjob = array(
                        'from' => settings('send_email_from'),
                        'from_name' => settings('store_name'),
                        'to' => $customer->email,
                        'subject' => 'Pesanan Selesai: ' . $this->data['order']->code,
                        'message' => $message
                    );
                    $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                }

                if ($customer->verification_phone) {
                    $sms = json_decode(settings('sprint_sms'), true);
                    $sms['m'] = 'Pesanan ' . $this->data['order']->code . ' telah selesai. Terimakasih telah berbelanja di ' . settings('store_name');
                    $sms['d'] = $customer->phone;
                    $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                    $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $customer->phone));
                }
            }
        }
    }

    public function delete_session() {
        $this->db->truncate('sessions');
    }

    public function manual_receive_pickup() {
        $order_code = $this->input->get('order_code');
        $orders = $this->main->gets('orders', ['code' => $order_code]);
        if($orders){
            $status = settings('order_complete_pickup_status');
            $res_orders = $orders->result();
            $this->data['invoice'] = $this->main->gets('order_invoice', ['order' => $res_orders[0]->id]);
            //var_dump($this->data['invoice']->result());exit();
            foreach($this->data['invoice']->result() as $inv) {
                if($inv->shipping_courier =='pickup') {
                    $this->main->insert('order_history', array('order' => $inv->order, 'invoice' => $inv->id, 'order_status' => $status));
                    $this->main->update('order_invoice', array('order_status' => $status,'date_received' => date('Y-m-d H:i:s'), 'due_date' => date('Y-m-d H:i:s', strtotime(' +2 day'))), array('id' => $inv->id));
                }
            }
            //var_dump($this->data['invoice']->result());
        }

    }

    public function reminder_payment_client() {
        $orders = $this->db->where('DATE_FORMAT(due_date, "%Y-%m-%d %H:%i") = (SELECT DATE_FORMAT((NOW() - INTERVAL 3 HOUR), "%Y-%m-%d %H:%i") )')->where('payment_status', 'Pending')->get('orders');
        if ($orders->num_rows() > 0) {
            foreach ($orders->result() as $order) {
                $this->data['order'] = $order;
                $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['order']->customer));
                $this->data['invoices'] = $this->main->gets('order_invoice', array('order' => $this->data['order']->id));
                if($this->data['customer']->email){
                    $message = $this->load->view('email/transaction/invoice', $this->data, true);
                    $cronjob = array(
                        'from' => settings('send_email_from'),
                        'from_name' => settings('store_name'),
                        'to' => $this->data['customer']->email,
                        'subject' => 'Reminder Pesanan: ' . $order->code,
                        'message' => $message
                    );
                    $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                }
                else{
                     if ($this->data['customer']->verification_phone) {
                        $sms = json_decode(settings('sprint_sms'), true);
                        $sms['m'] = 'REMINDER: Segera lakukan pembayaran melalui ' . strtoupper($this->data['order']->payment_method) . ' dengan nominal ' . rupiah($this->data['order']->total) . ' sebelum ' . get_date_indo_full($this->data['order']->due_date) . ' WIB';
                        $sms['d'] = $this->data['customer']->phone;
                        $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                        $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $this->data['customer']->phone));
                    }
                }
            }
        }
    }

    public function reminder_confirm_order_client() {
        $this->load->model('orders');
        $this->load->helper('text');
        $this->db->where('DATE_FORMAT(due_date_confirmation, "%Y-%m-%d %H:%i") = (SELECT DATE_FORMAT((NOW() - INTERVAL 6 HOUR), "%Y-%m-%d %H:%i") )');
        $invoices =$this->db->where('order_status', settings('order_delivered_status'))
                ->get('order_invoice');
        if ($invoices->num_rows() > 0) {
            foreach ($invoices->result() as $invoice) {
                $this->data['invoice'] = $invoice;
                $this->data['order'] = $this->orders->get($this->data['invoice']->order);
                $this->data['merchant'] = $this->main->get('merchants', array('id' => $this->data['invoice']->merchant));
                $this->data['products'] = $this->main->gets('order_product', array('invoice' => $this->data['invoice']->id));
                $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['invoice']->customer));
                if($this->data['customer']->email){
                    $message = $this->load->view('email/transaction/reminder_confirm_order', $this->data, true);
                    $cronjob = array(
                        'from' => settings('send_email_from'),
                        'from_name' => settings('store_name'),
                        'to' => $this->data['customer']->email,
                        'subject' => 'Reminder Konfirmasi Pesanan: ' . $order->code,
                        'message' => $message
                    );
                    $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                }
                else{
                    if ($this->data['customer']->verification_phone) {
                        $sms = json_decode(settings('sprint_sms'), true);
                       $sms['m'] = 'REMINDER: Harap segara mengkonfirmasi penerimaan barang ' . $this->data['order']->code . '. Terimakasih telah berbelanja di ' . settings('store_name');
                        $sms['d'] = $this->data['customer']->phone;
                        $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                        $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $this->data['customer']->phone));
                    }
                }
            }
        }
    }

    // public function testing(){
    //     $histories =$this->db->get('order_history');
    //     foreach ($histories->result() as $history) {
    //         //var_dump($history);exit();
    //         $this->db->where('id',$history->order);
    //         $orders =$this->db->get('orders');
    //         foreach ($orders->result() as $order) {
    //             //var_dump($order);exit();
    //             $payment_status = $order->payment_status;
    //             //var_dump($payment_status);exit();
    //             $this->db->where('order',$order->id);
    //             $invoices =$this->db->get('order_invoice');
    //             foreach ($invoices->result() as $invoice) {
    //                 $inv_status = $invoice->order_status;
    //             }
    //         }
    //         var_dump($orders->result());
    //         exit();
            
    //     }
    // }

}
