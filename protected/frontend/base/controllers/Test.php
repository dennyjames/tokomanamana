<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function email_register() {
        $this->load->view('email/register');
    }

    public function email_invoice() {
        $order = 35;
        $this->data['order'] = $this->main->get('orders', array('id' => $order));
        $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['order']->customer));
        $this->data['invoices'] = $this->main->gets('order_invoice', array('order' => $this->data['order']->id));
        $this->load->view('email/transaction/invoice', $this->data);
    }
    
    public function email_payment_success() {
        $order = 35;
        $this->data['order'] = $this->main->get('orders', array('id' => $order));
        $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['order']->customer));
        $this->data['invoices'] = $this->main->gets('order_invoice', array('order' => $this->data['order']->id));
        $message = $this->load->view('email/transaction/payment_success', $this->data,true);
        send_mail('noreply@tokomanamana.com', $this->data['customer']->email, 'Pembayaran Sukses #' . $this->data['order']->code, $message);
    }

}
