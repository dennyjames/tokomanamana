<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

if (!function_exists('asset_url')) {

    function asset_url($file) {
        return site_url('assets/frontend/' . $file);
    }

}

function order_code() {
    $CI = get_instance();
    $CI->load->database();
    $query = $CI->db->select('MAX(SUBSTR(code, 14)) code')
            ->where('YEAR(date_added)', date('Y'))
            ->where('MONTH(date_added)', date('m'))
            ->get('orders');
    if ($query->num_rows() > 0) {
        $increment = $query->row()->code;
    } else {
        $increment = 0;
    }
    $increment = ($increment == 0) ? 1 : $increment + 1;
    if ($increment < 10) {
        $increment = '000' . $increment;
    } elseif ($increment < 100) {
        $increment = '00' . $increment;
    } elseif ($increment < 1000) {
        $increment = '0' . $increment;
    }
    $code = 'ORDER/' . date('Ym') . '/' . $increment;
    return $code;
}

function invoice_code() {
    $CI = get_instance();
    $CI->load->database();
    $query = $CI->db->select('MAX(SUBSTR(code, 12)) code')
            ->where('YEAR(date_added)', date('Y'))
            ->where('MONTH(date_added)', date('m'))
            ->get('order_invoice');
    if ($query->num_rows() > 0) {
        $increment = $query->row()->code;
    } else {
        $increment = 0;
    }
    $increment = ($increment == 0) ? 1 : $increment + 1;
    if ($increment < 10) {
        $increment = '000' . $increment;
    } elseif ($increment < 100) {
        $increment = '00' . $increment;
    } elseif ($increment < 1000) {
        $increment = '0' . $increment;
    }
    $code = 'INV/' . date('Ym') . '/' . $increment;
    return $code;
}

function payment_code() {
    $CI = get_instance();
    $CI->load->database();
    $query = $CI->db->select('MAX(SUBSTR(code, 12)) code')
            ->where('YEAR(date_added)', date('Y'))
            ->where('MONTH(date_added)', date('m'))
            ->get('order_payment');
    if ($query->num_rows() > 0) {
        $increment = $query->row()->code;
    } else {
        $increment = 0;
    }
    $increment = ($increment == 0) ? 1 : $increment + 1;
    if ($increment < 10) {
        $increment = '000' . $increment;
    } elseif ($increment < 100) {
        $increment = '00' . $increment;
    } elseif ($increment < 1000) {
        $increment = '0' . $increment;
    }
    $code = 'PAY/' . date('Ym') . '/' . $increment;
    return $code;
}

function seo_url($url) {
    $CI = get_instance();
    $CI->load->database();
    $CI->load->model('main');
    $query = $CI->main->get('seo_url', array('query' => $url));
    $url = ($query) ? $query->keyword : $url;
    return site_url($url);
}

function get_thumbnail($image) {
//    $image = explode('/', $image);
//    $variable = substr(end($image), 0, strpos(end($image), "."));
//    $image = site_url('files/image_thumbs/' . $variable . '.png');
    $image = site_url('files/image_thumbs/' . $image);
    $file_headers = @get_headers($image);
    if ($file_headers[0] == 'HTTP/1.0 404 Not Found') {
        return false;
    } else if ($file_headers[0] == 'HTTP/1.0 302 Found' && $file_headers[7] == 'HTTP/1.0 404 Not Found') {
        return false;
    } else {
        return $image;
    }
}

function get_image($image) {
    $image = base_url('files/images/' . $image);
    $file_headers = @get_headers($image);
    if ($file_headers[0] == 'HTTP/1.0 404 Not Found') {
        return false;
    } else if ($file_headers[0] == 'HTTP/1.0 302 Found' && $file_headers[7] == 'HTTP/1.0 404 Not Found') {
        return false;
    } else {
        return $image;
    }
}

function price_discount($price, $discount) {
    return $price - ($price * $discount / 100);
}

function weight_rounding($weight, $max = 0.2) {
    if ($weight > 1) {
        if (strpos($weight, '.')) {
            $number = explode('.', $weight);
            $decimal = '0.' . $number[1];
            if ($decimal > $max) {
                $decimal = 1;
            } else {
                $decimal = 0;
            }
            $weight = $number[0] + $decimal;
        } else {
            $weight = $weight;
        }
    } else
        $weight = 1;
    return $weight;
}

function generate_xor_key($length)
{
    $result = array_fill(0, $length, 0);

    for ($i = 0, $bit = 1; $i < $length; $i++) {
        for ($j = 0; $j < 3; $j++, $bit++) {
            $result[$i] |= ($bit % 2) << $j;
        }
    }

    return implode('', array_map('chr', $result));
}

function encode_id($id, $encodedLength = 9, $rawBits = 16, $key = null)
{
    $maxRawBits = $encodedLength * 3;
    if ($rawBits > $maxRawBits) {
        trigger_error('encode_id(): $rawBits must be no more than 3 times greater than $encodedLength');
        return false;
    }

    if ($key === null) {
        $key = generate_xor_key($encodedLength);
    }

    $result = array_fill(0, $encodedLength, 0x30);

    for ($position = 0; $position < $rawBits; $position++) {
        $bit = (($id >> $position) & 0x01) << floor($position / $encodedLength);
        $index = $position % $encodedLength;
        $result[$index] |= $bit;
    }

    do {
        $index = $position % $encodedLength;
        $bit = ($position % 2) << floor($position / $encodedLength);
        $result[$index] |= $bit;
    } while (++$position < $maxRawBits);

    return implode('', array_map('chr', $result)) ^ $key;
}

function decode_id($id, $encodedLength = 9, $rawBits = 16, $key = null)
{
    if ($key === null) {
        $key = generate_xor_key($encodedLength);
    }

    $bytes = array_map(
        'ord',
        str_split(
            str_pad($id, $encodedLength, '0', STR_PAD_LEFT) ^ $key,
            1
        )
    );

    $result = 0;

    for ($position = 0; $position < $rawBits; $position++) {
        $index = $position % $encodedLength;
        $bit = (($bytes[$index] >> floor($position / $encodedLength)) & 0x01) << $position;
        $result |= $bit;
    }

    return $result;
}
