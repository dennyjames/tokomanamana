<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Favourite_model extends CI_Model {
    function get_favourite_shop($customer, $limit = 20, $start = 0){
        $this->db->select('fs.id,m.name,m.id as merchant_id,m.telephone as phone,m.address,p.name as province_name,c.name as city_name,d.name as district_name,m.postcode')
                ->join('merchants m','fs.merchant_id = m.id','left')
                ->join('provincies p', 'p.id = m.province')
                ->join('cities c', 'c.id = m.city')
                ->join('districts d', 'd.id = m.district')
                ->where('fs.customer_id', $customer)
                ->order_by('fs.created_at desc')
                ->limit($limit, $start);
        return $this->db->get('favourite_shop fs');
    }

}
