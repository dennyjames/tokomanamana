<style>
    .collection-title {
      font-size: 18px !important;
      color: #a7c22a !important;
      font-weight: bold !important;
    }
    .alert {
        border-radius: 5px;
        font-size: 15px;
    }
</style>
<div class="my-account" id="address">
    <section class="collection-heading heading-content ">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-sm-push-3" style="padding-left: 0px; padding-right: 0px;">
                    <div class="collection-wrapper block" style="padding: 10px 15px;background: none;">
                        <h1 class="collection-title"><span><?php echo lang('favourite_shop'); ?></span></h1>
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="collection-content">
        <div class="collection-wrapper">
            <div class="container" style="margin-bottom: 30px;">
                <div class="row">
                    <?php echo $this->load->view('auth/member/navigation'); ?>
                    <div class="collection-mainarea col-md-9 col-sm-8 col-xs-12">
                        <?php
                             if ($this->session->flashdata('error')) {
                                echo $this->template->alert('danger', $this->session->flashdata('error'));
                            }
                            if ($this->session->flashdata('success')) {
                                echo $this->template->alert('success', $this->session->flashdata('success'));
                            }
                            if ($this->session->flashdata('warning')) {
                                echo $this->template->alert('warning', $this->session->flashdata('warning'));
                            }
                        ?>
                        <?php if ($merchants->num_rows() > 0) foreach($merchants->result() as $merchant){
                            ?>
                            <div class="col-sm-6 col-xs-12" id="merchant-<?php echo $merchant->id; ?>">
                                <div class="panel">
                                <div class="panel-heading">
                                    <span class="header-label">Informasi Toko</span>
                                    <span><?php echo $merchant->name.' - '.$merchant->phone; ?></span>
                                </div>
                                <div class="panel-body">
                                    <?php echo nl2br($merchant->address); ?>
                                    <?php echo $merchant->province_name.', '.$merchant->city_name.', '.$merchant->district_name.' '.$merchant->postcode; ?>
                                </div>
                                <div class="panel-footer">
                                    <a href="<?php echo base_url('member/favourite_shop/delete/'.encode_id($merchant->merchant_id))?>">Delete</a>
                                </div>
                            </div>
                            </div>
                        <?php } else {
                            ?>
                                <div class="alert alert-warning">
                                    <center><b>Tidak ada toko favorit.</b></center>
                                </div>
                            <?php
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

