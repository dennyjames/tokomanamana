<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Favourite_shop extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $customer = $this->session->userdata('user');
        //print_r($customer);
        $this->load->model('Model');


        $this->load->library('form_validation');
        $this->load->model('auth/member_model', 'member');
        $this->load->model('favourite_model', 'favourite');
        $this->load->model('orders');
        if (!$this->input->is_ajax_request()) {
            if (!$this->ion_auth->logged_in()) {
                redirect('member/login?back=' . uri_string());
            }
            $this->load->library('breadcrumb');
            $this->load->library('pagination');

            $this->data['notification_invoice'] = $this->member->get_invoice_for_notification($customer->id);
        }


        $this->load->language('member', settings('language'));

//        $this->data['notification_invoice'] = array();
    }

    public function index(){
        $config['base_url'] = site_url('member/favourite_shop/');
        $config['total_rows'] = $this->main->count('favourite_shop', array('customer_id' => $this->data['user']->id));
        $config['per_page'] = 20;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('favourite_shop'), site_url('member/favourite_shop'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $merchants = $this->session->userdata('list_merchant');
        $merchant_group = $merchants[0]['group'];
        $this->data['merchants'] = $this->favourite->get_favourite_shop($this->data['user']->id, $config['per_page'], $start);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'member/favourite_shop';

        $this->output->set_title(lang('favourite_shop') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('list', $this->data);
    }

    public function add($merchant_id){
        $merchant_id = decode_id($merchant_id);
        $check_id = $this->main->get('merchants', array('id' => $merchant_id));
        if(!$check_id){ 
            $this->session->set_flashdata('error', 'Toko tidak ditemukan');
            redirect('member/favourite_shop');
        }
        $data = $this->main->get('favourite_shop', array('customer_id' => $this->data['user']->id, 'merchant_id' => $merchant_id));
        if ($data) {
            $this->session->set_flashdata('error', 'Toko ini sudah pernah ditambahkan ke toko favorit.');
            redirect('member/favourite_shop');
        } else {
            $this->session->set_flashdata('success', 'Berhasil menambahkan toko favorit.');
            $this->main->insert('favourite_shop', array('customer_id' => $this->data['user']->id, 'merchant_id' => $merchant_id));
            redirect('member/favourite_shop');
        }
    }

    public function delete($merchant_id){
        $merchant_id = decode_id($merchant_id);
        $check_id = $this->main->get('merchants', array('id' => $merchant_id));
        if(!$check_id){ 
            $this->session->set_flashdata('error', 'Toko tidak ditemukan');
            redirect('member/favourite_shop');
        }
        $data = $this->main->get('favourite_shop', array('customer_id' => $this->data['user']->id, 'merchant_id' => $merchant_id));
        if ($data) {
            $this->session->set_flashdata('success', 'Toko berhasil dihapus dari toko favorit.');
            $this->main->delete('favourite_shop', array('customer_id' => $this->data['user']->id, 'merchant_id' => $merchant_id));
            redirect('member/favourite_shop');
        } else {
            $this->session->set_flashdata('error', 'Toko tidak ada di dalam list favourite');
            redirect('member/favourite_shop');
        }
    }

    

}
