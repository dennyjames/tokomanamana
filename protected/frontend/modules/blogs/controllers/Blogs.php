<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blogs extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('breadcrumb');
        $this->load->library('pagination');
        $this->load->helper('text');
    }

    public function index() {
        $this->template->_init();

        $config['base_url'] = current_url();
        $config['total_rows'] = $this->main->gets('blogs', array('status' => 1))->num_rows();
        $config['per_page'] = ($this->input->get('show')) ? $this->input->get('show') : 8;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;
        $this->data['pagination'] = $this->pagination->create_links();

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Blogs', site_url('blogs'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();

        $this->data['posts'] = $this->main->gets_paging('blogs', $start, $config['per_page'], array('status' => 1), 'date_added DESC');
        $this->data['blog_categories'] = $this->main->gets('blog_categories',array(),'name asc');
        $this->data['meta_description'] = "Blogs Tokomanamana, Update seputar barang tokomanamana.";

        $this->output->set_title('Blogs - ' . settings('meta_title'));
        $this->load->view('blogs', $this->data);
    }

}
