<section class="collection-heading heading-content ">
    <div class="container">
        <div class="row">
            <div class="collection-wrapper">
                <h1 class="collection-title"><span>Blogs</span></h1>
                <div class="breadcrumb-group">
                    <div class="breadcrumb clearfix">
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="blog-content">
    <div class="blog-wrapper">
        <div class="container">
            <div class="row">
                <div id="shopify-section-blog-template" class="shopify-section">
                    <div class="blog-inner">
                        <div id="blog">
                            <div class="col-sm-3 sidebar">
                                <div class="sidebar-block blog-category">
                                    <h3 class="sidebar-title"><span>Category</span></h3>
                                    <div class="sidebar-content">
                                        <ul class="category">
                                            <?php if ($categories) foreach ($categories->result() as $category) { ?>
                                                    <li class="nav-item active">
                                                        <a href="<?php echo seo_url('blogs/categories/view/' . $category->id); ?>"><?php echo $category->name; ?></a>
                                                    </li>
                                                <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-9 articles">
                                 <h1 class="article-name"><?php echo $data->name; ?></h1>
                                <div class="blog-items">
                                    <?php if ($posts) foreach ($posts->result() as $post) { ?>
                                            <div class="article clearfix col-sm-6 blog_Grid">
                                                <div class="article-image">
                                                    <a href="<?php echo seo_url('blogs/posts/view/' . $post->id); ?>"><img src="<?php echo get_image($post->image); ?>" alt="<?php echo $post->title; ?>"></a>
                                                </div>
                                                <div class="articleinfo_group">
                                                    <div class="article-title">
                                                        <h2 class="article-name"><a href="<?php echo seo_url('blogs/posts/view/' . $post->id); ?>"><?php echo $post->title; ?></a></h2>
                                                    </div>
                                                    <ul class="article-info list-inline">
                                                        <li class="article-date"><?php echo get_date($post->date_added); ?></li>
                                                    </ul>
                                                    <div class="article-content"><p><?php echo character_limiter(stripHTMLtags($post->content), 100, '...'); ?></p></div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                </div>
                                <?php if ($pagination) { ?>
                                    <div class="collection-bottom-toolbar">
                                        <div class="product-pagination">
                                            <div class="pagination_group">
                                                <?php echo $pagination; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
