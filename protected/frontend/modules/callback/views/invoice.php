<!DOCTYPE html>
<html>
<head>
    <title>Invoice - Tanaka Marketplace</title>

        
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" type="text/css" />
    <link href="https://tokomanamana.com/assets/frontend/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="https://tokomanamana.com/assets/frontend/css/fonts.googleapis.css" rel="stylesheet" type="text/css" />
    <link href="https://tokomanamana.com/assets/frontend/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://tokomanamana.com/assets/frontend/css/icon-font.min.css" rel="stylesheet" type="text/css" />
    <link href="https://tokomanamana.com/assets/frontend/css/social-buttons.css" rel="stylesheet" type="text/css" />
    <link href="https://tokomanamana.com/assets/frontend/css/cs-3.styles.css" rel="stylesheet" type="text/css" />
    <link href="https://tokomanamana.com/assets/frontend/css/owl.carousel.min.css" rel="stylesheet" type="text/css" />
    <link href="https://tokomanamana.com/assets/frontend/css/spr.css" rel="stylesheet" type="text/css" />
    <link href="https://tokomanamana.com/assets/frontend/css/slideshow-fade.css" rel="stylesheet" type="text/css" />
    <link href="https://tokomanamana.com/assets/frontend/css/cs.animate.css" rel="stylesheet" type="text/css" />
    <link href="https://tokomanamana.com/assets/frontend/css/custom.css" rel="stylesheet" type="text/css" />
</head>
<body class="index-template sarahmarket_1">
<?php $products = $this->member->getOrderProducts($order->id); ?>
<div class="panel panel-default">
    <div id="order<?php echo $order->id; ?>" class="panel-collapse collapsed" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body order-detail">
            <table>
                <tr>
                    <td>
                        <b>Alamat Pengiriman:</b><br>
                        <b><?php echo $order->shipping_name; ?></b><br>
                        <?php echo $order->shipping_phone; ?><br>
                        <?php echo nl2br($order->shipping_address); ?>,
                        <?php echo $order->shipping_district_name; ?><br><?php echo $order->shipping_city_name; ?> - <?php echo $order->shipping_postcode; ?>
                    </td>
                    <td>
                        <b>Ringkasan Pembayaran:</b><br>
                        <ul>
                            <li>
                                <span class="leftside">Total Belanja</span>
                                <span class="rightside"><?php echo rupiah($this->member->get_subtotal($order->id)); ?></span>
                            </li>
                            <li>
                                <span class="leftside">Diskon</span>
                                <span class="rightside">Rp -0</span>
                            </li>
                            <li>
                                <span class="leftside">Biaya Pengiriman</span>
                                <span class="rightside"><?php echo rupiah($this->member->get_total_shipping_cost($order->id)); ?></span>
                            </li>
                            <li>
                                <span class="leftside">Total Pembayaran</span>
                                <span class="rightside"><?php echo rupiah($order->total); ?></span>
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
            <?php $invoices = $this->member->get_order_invoice($order->id); ?>
            <?php if ($invoices->num_rows() > 0) foreach ($invoices->result() as $invoice) { ?>
                <table class="order-detail-table">
                    <tr>
                        <td colspan="2"><b><i class="fa fa-list-alt"></i> Belanja dari <?php echo $invoice->merchant_name; ?>:</b></td>
                    </tr>
                    <?php $products = $this->member->get_order_products($invoice->id); ?>
                    <?php if ($products->num_rows() > 0) foreach ($products->result() as $product) { ?>
                            <tr>
                                <td colspan="2">
                                    <div class="col-sm-8">
                                        <a class="product-image" href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>" target="_blank">
                                            <img src="<?php echo get_image($product->image); ?>">
                                        </a>
                                        <div style="float: left; width: 80%;">
                                            <a href="<?php echo seo_url('catalog/products/view/' . $product->product); ?>" target="_blank"><?php echo $product->name; ?></a>
                                            <span style="font-size: 11px;margin-top: 3px;">
                                                <div><?php echo $product->code; ?></div>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <?php echo number($product->quantity) . ' x ' . rupiah($product->price); ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <?php echo '<b>' . rupiah($product->total) . '</b>'; ?>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    <tr>
                        <td style="width:50%"><b>Pengiriman</b></td>
                        <td style="width:50%"><?php
                            if ($invoice->shipping_courier != 'pickup') {
                                $invoice->shipping_courier = explode('-', $invoice->shipping_courier);
                                $invoice->shipping_courier = strtoupper($invoice->shipping_courier[0]) . ' ' . $invoice->shipping_courier[1];
                            } else {
                                $invoice->shipping_courier = 'Ambil Sendiri';
                            }
                            echo $invoice->shipping_courier;
                            ?></td>
                    </tr>
                    <tr>
                        <td style="width:50%"><b>Total Belanja</b></td>
                        <td style="width:50%"><?php echo rupiah($invoice->subtotal); ?></td>
                    </tr>
                    <tr>
                        <td style="width:50%"><b>Biaya Pengiriman</b></td>
                        <td style="width:50%"><?php echo rupiah($invoice->shipping_cost); ?></td>
                    </tr>
                    <tr>
                        <td style="width:50%"><b>Total</b></td>
                        <td style="width:50%"><?php echo rupiah($invoice->total); ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><b><i class="fa fa-clock-o"></i> Histori Transaksi</b></td>
                    </tr>
                    <?php $histories = $this->member->get_order_histories($invoice->id); ?>
                    <tr>
                        <td colspan="2">
                            <table class="order-history-table">
                                <?php foreach ($histories->result() as $history) { ?>
                                    <tr>
                                        <td style="width: 20%">
                                            <span class="label label-<?php echo $history->color; ?>"><?php echo $history->name; ?></span><br>
                                            <small><?php echo get_date($history->date_added, 'full'); ?></small>
                                        </td>
                                        <td>
                                            <?php echo nl2br($history->description); ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </td>
                    </tr>
                </table>
            <?php } ?>
        </div>
    </div>
</div>
</body>
</html>