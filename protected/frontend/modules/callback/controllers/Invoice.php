<?php
ini_set('display_errors', 1);
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        if (!$this->ion_auth->logged_in()) {
            redirect('member/login?back=' . uri_string());
        }

        $this->load->language('member', settings('language'));
        $this->load->model('auth/member_model', 'member');
        $this->load->model('orders');
    }

    public function download($id) {
        $this->load->library('pdf');

        $this->data['order'] = $this->orders->get($id);

        $this->load->view('invoice', $this->data);
        // $content = $this->load->view('invoice', $this->data, true);

        // $this->pdf->generate($content, 'invoice');
    }
}
