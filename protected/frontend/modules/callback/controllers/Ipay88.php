<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ipay88 extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function response() {
        $this->load->library('ipaycallback');
        $resp = $this->ipaycallback->verifySignature()->getData();
        $order = $this->main->get('orders', ['code' => $resp['RefNo']]);
        $this->main->insert('order_meta', array('order_id' => $order->id, 'meta_name' => 'ipay88_response', 'meta_value' => json_encode($resp)));

        if($resp['Status'] == "1")
        {
            
            //please write a code for

            //redirect to your success page / thank you page
            redirect('checkout/finish?id=' . $order->code);
        
        }

        elseif($resp['Status'] == "6")

        {
            //update your database, transaction is pending
            $this->main->update('orders', array('payment_status' => 'Pending'), array('id' => $order->id));
            //redirect to your pending page
            redirect('checkout/finish?id=' . $order->code);
        }

        else

        {
            //please write a code for
            if($resp['PaymentId'] == 63){
                $this->main->update('orders', array('payment_status' => 'Pending'), array('id' => $order->id));
            } else {
                //update your database, transaction is failed
                $this->main->update('orders', array('payment_status' => 'Failed'), array('id' => $order->id));
                $this->main->update('order_invoice', array('order_status' => settings('order_cancel_status')), array('order' => $order->id));
                $invoices = $this->main->gets('order_invoice', array('order' => $order->id));
                if ($invoices) {
                    foreach ($invoices->result() as $invoice) {
                        $this->main->insert('order_history', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => settings('order_cancel_status')));
                    }
                }
            }
            //redirect to your failed page
            redirect('checkout/finish?id=' . $order->code);
           
        }
    }

    public function backend() {
        $this->load->library('ipaycallback');
        $resp = $this->ipaycallback->verifySignature()->getData();
        $order = $this->main->get('orders', ['code' => $resp['RefNo']]);
        $this->main->insert('order_meta', array('order_id' => $order->id, 'meta_name' => 'ipay88_response_flag', 'meta_value' => json_encode($resp)));
        if($resp['Status'] == "1")
        {
            //please write a code for
            //update your database, transaction is success
            // Approved Transaction
            $this->main->update('orders', array('payment_status' => 'Paid', 'payment_date' => date('Y-m-d H:i:s')), array('id' => $order->id));
            //$this->main->update('order_invoice', array('order_status' => settings('order_payment_received_status')), array('order' => $order->id));
            $this->data['order'] = $order;
            $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['order']->customer));
            $this->data['invoices'] = $this->main->gets('order_invoice', array('order' => $this->data['order']->id));
            if ($this->data['invoices'])
                foreach ($this->data['invoices']->result() as $invoice) {
                    if($invoice->shipping_courier == 'pickup') {
                        $due_date_inv = date('Y-m-d H:i:s', strtotime('+1 days'));
                    } else {
                        $due_date_inv = date('Y-m-d H:i:s', strtotime('+2 days'));
                    }
                    $this->main->update('order_invoice', array('order_status' => settings('order_payment_received_status'), 'due_date' => $due_date_inv), array('id' => $invoice->id));
                    $this->main->insert('order_history', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => settings('order_payment_received_status')));
                    $this->data['invoice'] = $invoice;
                    $this->data['merchant'] = $this->main->get('merchants', array('id' => $invoice->merchant));
                    
                    //update stock merchant
                    if ($this->data['merchant']->type == 'merchant' || $this->data['merchant']->type == 'branch') {
                        $products = $this->main->gets('order_product', array('invoice' => $invoice->id));
                        if ($products) {
                            foreach ($products->result() as $product) {
                                $product_detail = $this->db->query("select type, package_items from products where id = $product->product ");
                                $row = $product_detail->row();
                                if($row->type == 'p') {
                                    $package_items = json_decode($row->package_items, true);
                                    foreach ($package_items as $package) {
                                        $this->db->query("UPDATE product_merchant SET quantity = (quantity - $product->quantity) WHERE product = ".$package['product_id']." AND merchant = " . $this->data['merchant']->id); 
                                    }
                                } else {
                                    $this->db->query("UPDATE product_merchant SET quantity = (quantity - $product->quantity) WHERE product = $product->product AND merchant = " . $this->data['merchant']->id);
                                }
                            }
                        }
                    }
                    
                    $merchant_user = $this->main->get('merchant_users', array('id' => $this->data['merchant']->auth));
                    $message = $this->load->view('email/merchant/new_order', $this->data, true);
                    $cronjob = array(
                        'from' => settings('send_email_from'),
                        'from_name' => settings('store_name'),
                        'to' => $merchant_user->email,
                        'subject' => 'Pesanan baru dari ' . $this->data['customer']->fullname,
                        'message' => $message
                    );
                    $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));

                    if ($merchant_user->verification_phone) {
                        $sms = json_decode(settings('sprint_sms'), true);
                        $sms['m'] = 'Pesanan baru dari ' . $this->data['customer']->fullname . '. Segera proses sebelum ' . get_date_indo_full($invoice->due_date) . ' WIB.';
                        $sms['d'] = $merchant_user->phone;
                        $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                    }
                }

            $message = $this->load->view('email/transaction/payment_success', $this->data, true);
            $cronjob = array(
                'from' => settings('send_email_from'),
                'from_name' => settings('store_name'),
                'to' => $this->data['customer']->email,
                'subject' => 'Pembayaran Sukses #' . $this->data['order']->code,
                'message' => $message
            );
            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
            if ($this->data['customer']->verification_phone) {
                $sms = json_decode(settings('sprint_sms'), true);
                $sms['m'] = 'Pembayaran sebesar ' . rupiah($this->data['order']->total) . ' telah berhasil pada ' . get_date_indo_full(date('Y-m-d H:i:s')) . ' WIB. Pesanan Anda akan diteruskan ke penjual.';
                $sms['d'] = $this->data['customer']->phone;
                $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
            }
            print_r("RECEIVEOK");exit;
        }
        else
        {
            print_r("BACKEND FAILED");exit;
        } 
        //$resp = $$his->ipaycallback->verifySignature()->requeryStatus()->getData(); 
        //var_dump($resp);
    }

}
