<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Brands extends CI_Controller {
    
    function __construct()
    {
        parent::__construct();
        $this->lang->load('catalog', settings('language'));
        $this->load->model('brand_model', 'brand');
        $this->load->model('product_model', 'products');
        $this->load->library('pagination');
        $this->load->library('breadcrumb');
    }

    public function view($id) {
        $brand = $this->main->get('brands', array('id' => $id, 'status' => 1));

        $config['base_url'] = seo_url('catalog/brands/view/' . $id);
        $config['total_rows'] = $this->brand->get_products_brand ($id, '', '', '')->num_rows();
        $config['per_page'] = ($this->input->get('show')) ? ($this->input->get('show')) : 20;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add($brand->name, site_url());
        
        $this->data['brand'] = $brand;
        $this->data['products'] = $this->brand->get_products_brand ($id, (($this->input->get('sort')) ? $this->input->get('sort') : 'popular'), $config['per_page'], $start);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['meta_description'] = 'Produk-produk ' . $brand->name . ' - ' . settings('meta_description');
        $this->data['meta_keyword'] = settings('meta_keyword');
        $this->data['total_products'] = $config['total_rows'];
        $this->data['start'] = $start + 1;
        $this->data['to'] = ($this->data['total_products'] < ($this->data['start'] * $config['per_page'])) ? $this->data['total_products'] : ($this->data['start'] * $config['per_page']);
        
        $this->output->set_title($brand->name . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('brands', $this->data);
    }
}
