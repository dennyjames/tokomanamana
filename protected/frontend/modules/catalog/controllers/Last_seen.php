<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Last_seen extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('catalog', settings('language'));
        $this->load->model('last_seen_model', 'last_seen');
        $this->load->model('products');
        $this->load->library('pagination');
        $this->load->library('breadcrumb');
    }

    public function view() {
        $config['base_url'] = site_url('last_seen');

        if ($this->ion_auth->logged_in()) {
            $user_id = $this->data['user']->id;
            /* getting all data from product_last_review table */
            $config['total_rows'] = $this->main->gets('product_last_review', array('user_id' => $user_id))->num_rows();
        }
        else {
            $ip_address = $this->input->ip_address();
            $config['total_rows'] = $this->main->gets('product_last_review', array('ip_address' => $ip_address))->num_rows();
        }
        
        $config['per_page'] = ($this->input->get('show')) ? $this->input->get('show') : 18;
        $this->pagination->initialize($config);
        $start = $this->input->get('page') ? ($this->input->get('page') - 1) * $config['per_page'] : 0;
        //var_dump($start);exit();

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Terakhir dilihat', site_url('last_seen'));

        if ($this->ion_auth->logged_in()) {
            $user_id = $this->data['user']->id;
            $products = $this->last_seen->get_last_seen_product($user_id, FALSE, $config['per_page'], $start);
        } else {
            $ip_address = $this->input->ip_address();
            $products = $this->last_seen->get_last_seen_product(FALSE, $ip_address, $config['per_page'], $start);
        }
        
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['meta_description'] = 'Produk terakhir dilihat.';
        $this->data['products'] = $products;
        $this->data['total_products'] = $config['total_rows'];
        //var_dump($this->data['total_products']);exit();
        $this->data['start'] = $start + 1;
        $this->data['to'] = ($this->data['total_products'] < ($this->data['start'] * $config['per_page'])) ? $this->data['total_products'] : ($this->data['start'] * $config['per_page']);
        $this->output->set_title('Terakhir dilihat ' . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('last_seen', $this->data);
    }
}
