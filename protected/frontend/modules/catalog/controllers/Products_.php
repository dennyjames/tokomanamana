<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('catalog', settings('language'));
        $this->load->model('catalog/category_model', 'category');
        $this->load->model('product_model', 'product');
        $this->load->library('breadcrumb');
    }

    public function view($id) {
        $product = $this->product->get_product($id);
        if (!$product)
            show_404();

        $this->product->add_viewed($product->id);

        //breadcrumb
        $categoryPaths = $this->category->get_category_path($product->category);
        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        if ($categoryPaths)
            foreach ($categoryPaths->result() as $categoryPath)
                $this->breadcrumb->add($categoryPath->name, seo_url('catalog/categories/view/' . $categoryPath->id));
        $this->breadcrumb->add($product->name, seo_url('catalog/products/view/' . $product->id));

        $this->data['data'] = $product;
        if($product->merchant == 0){
            $merchant = $this->session->userdata('list_merchant')[0];
//            print_r($merchant);
//            $price = $this->main->get('product_price', array('product' => $product->id, 'merchant_group' => $merchant->group));
            $this->data['price'] = $this->main->get('product_price', array('product' => $product->id, 'merchant_group' => $merchant['group']))->price;
        }else{
            $this->data['price'] = $product->price;
        }
        $this->data['data']->rating = round($this->data['data']->rating);
        $this->data['images'] = $this->product->get_images($id);
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['also_boughts'] = $this->product->get_also_boughts($categoryPaths->row(0)->id, $product->id, 6);
        $this->data['features'] = $this->product->get_product_features($product->id);
        $this->data['reviews'] = $this->product->get_reviews($product->id);
        $this->data['product_relations'] = $this->product->get_relations($product->category, $product->id, 12, $categoryPaths->row(0)->id);
        $this->data['meta_description'] = ($product->meta_description) ? $product->meta_description : $product->short_description;
        $this->data['meta_keyword'] = ($product->meta_keyword) ? $product->meta_keyword : settings('meta_keyword');
        $this->data['page'] = 'product';
        $this->data['merchants'] = $this->product->get_merchants($product->id);

        $this->output->set_title((($product->meta_title) ? $product->meta_title : $product->name) . ' - ' . settings('meta_title'));
        $this->template->_init();

//        $this->load->css('assets/frontend/css/libs/owl.carousel.css');
//        $this->load->css('assets/frontend/css/owl.carousel.css');
//        $this->load->css('assets/frontend/css/jquery.bxslider.css');
//        $this->load->css('assets/frontend/css/cloud-zoom.css');
//        $this->load->css('assets/frontend/css/star-rating.css');
//        $this->load->js('assets/frontend/js/jquery.jcarousellite.js');
//        $this->load->js('assets/frontend/js/jquery.elevatezoom.js');
//        $this->load->js('assets/frontend/js/jquery.bxslider.js');
//        $this->load->js('assets/frontend/js/cloud-zoom.js');
//        $this->load->js('assets/frontend/js/star-rating.js');
        $this->load->js('assets/frontend/js/modules/product.js');

        $this->load->view('products/view', $this->data);
    }

    public function submit_review() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('rating', 'Rating', 'trim|required');
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required|min_length[10]');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            do {
                $this->main->insert('product_review', array('product' => $data['product'], 'customer' => $this->data['user']->id, 'title' => $data['title'], 'rating' => $data['rating'], 'description' => $data['description']));
                $return = array('message' => $this->template->alert('success', 'Review telah diterima dan akan di review oleh Administrator.'), 'status' => 'success');
            } while (0);
        } else {
            $return = array('message' => $this->template->alert('danger', validation_errors()), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function submitLikeDislike() {
        if ($this->ion_auth->logged_in()) {
            $id = $this->input->post('id', TRUE);
            $type = $this->input->post('type', TRUE);
            if ($likeDislike = $this->main->get('product_review_likedislike', array('product_review' => $id, 'customer' => $this->data['user']->id))) {
                $type = $likeDislike->likedislike;
            } else {
                $this->main->insert('product_review_likedislike', array('product_review' => $id, 'customer' => $this->data['user']->id, 'likedislike' => $type));
                $typeName = ($type == 1) ? "like" : "dislike";
                $this->db->query("UPDATE product_review SET `$typeName`=`$typeName`+1 WHERE id=$id");
            }
            echo ($type == 1) ? lang('product_text_review_like_this') : lang('product_text_review_dislike_this');
        }
    }

}
