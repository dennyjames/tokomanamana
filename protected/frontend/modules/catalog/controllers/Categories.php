<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('catalog', settings('language'));
        $this->load->model('category_model', 'category');
        $this->load->library('pagination');
        $this->load->library('breadcrumb');
    }

//     public function view($id) {
//         $category = $this->category->getCategory($id);
//         if (!$category)
//             show_404();

//         $merchants = $this->session->userdata('list_merchant');
//         $merchant_group = $merchants[0]['group'];
//         foreach($merchants as $m){
//             $list[] = $m['id'];
//         }
// //        print_r($merchants);

//         //pagination
//         $config['base_url'] = current_url();
//         $config['total_rows'] = $this->category->getProducts($id, $list, $merchant_group, '', '', '')->num_rows();
//         $config['per_page'] = ($this->input->get('show')) ? $this->input->get('show') : 20;
//         $this->pagination->initialize($config);
//         $page_num = ($this->input->get('page')) ? ($this->input->get('page')) : 1;
//         $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

//         //breadcrumb
//         $categoryPaths = $this->category->get_category_path($id);
//         $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
//         foreach ($categoryPaths->result() as $categoryPath) {
//             $this->breadcrumb->add($categoryPath->name, seo_url('catalog/categories/view/' . $categoryPath->id));
//         }
//         $this->data['data'] = $category;
//         $this->data['products'] = $this->category->getProducts($id, $list, $merchant_group, ($this->input->get('sort')) ? $this->input->get('sort') : 'populr', $config['per_page'], $start);
//         var_dump($this->db->last_query()); die;
//         $this->data['pagination'] = $this->pagination->create_links();
//         $this->data['breadcrumb'] = $this->breadcrumb->output();
//         $this->data['meta_description'] = ($category->meta_description) ? $category->meta_description : $category->name . ', ' . settings('meta_title');
//         $this->data['meta_keyword'] = ($category->meta_keyword) ? $category->meta_keyword : settings('meta_keyword');
//         $this->data['total_products'] = $config['total_rows'];
//         $this->data['start'] = $start + 1;
//         $this->data['to'] = ($this->data['total_products'] < ($page_num *  $config['per_page'])) ? $this->data['total_products'] : ($page_num * $config['per_page']);

//         $this->output->set_title((($category->meta_title) ? $category->meta_title : $category->name) . ' - ' . settings('meta_title'));
//         $this->template->_init();
//         $this->load->js('assets/frontend/js/modules/categories.js');
//         $this->load->view('categories/view', $this->data);
//     }

public function view($id) {
    $category = $this->category->getCategory($id);
    if (!$category)
        show_404();

    $merchants = $this->session->userdata('list_merchant');
    if($merchants) {
        $merchant_group = $merchants[0]['group'];
        foreach($merchants as $m){
            $list[] = $m['id'];
        }
    }
//        print_r($merchants);

    //pagination
    $config['base_url'] = current_url();
    $config['total_rows'] = $this->category->get_products($id, '', '', '')->num_rows();
    $config['per_page'] = ($this->input->get('show')) ? $this->input->get('show') : 20;
    $this->pagination->initialize($config);
    $page_num = ($this->input->get('page')) ? ($this->input->get('page')) : 1;
    $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

    //breadcrumb
    $categoryPaths = $this->category->get_category_path($id);
    $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
    foreach ($categoryPaths->result() as $categoryPath) {
        $this->breadcrumb->add($categoryPath->name, seo_url('catalog/categories/view/' . $categoryPath->id));
    }
    $this->data['data'] = $category;
    $this->data['products'] = $this->category->get_products($id, ($this->input->get('sort')) ? $this->input->get('sort') : 'popular', $config['per_page'], $start);
    $this->data['pagination'] = $this->pagination->create_links();
    $this->data['breadcrumb'] = $this->breadcrumb->output();
    $this->data['meta_description'] = ($category->meta_description) ? $category->meta_description : $category->name . ', ' . settings('meta_title');
    $this->data['meta_keyword'] = ($category->meta_keyword) ? $category->meta_keyword : settings('meta_keyword');
    $this->data['total_products'] = $config['total_rows'];
    $this->data['start'] = $start + 1;
    $this->data['to'] = ($this->data['total_products'] < ($page_num *  $config['per_page'])) ? $this->data['total_products'] : ($page_num * $config['per_page']);

    $this->output->set_title((($category->meta_title) ? $category->meta_title : $category->name) . ' - ' . settings('meta_title'));
    $this->template->_init();
    $this->load->js('assets/frontend/js/modules/categories.js');
    $this->load->view('categories/view', $this->data);
}

}
