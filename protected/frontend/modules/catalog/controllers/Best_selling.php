<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Best_selling extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('catalog', settings('language'));
        $this->load->model('bestselling_model', 'bestselling');
        $this->load->library('pagination');
        $this->load->library('breadcrumb');
        $this->load->library('user_agent');
    }

    // NEW METHOD
    public function index() {
        $config['base_url'] = site_url('best_selling');

        $products = $this->bestselling->get_all_products(12);

        $config['total_rows'] = $this->bestselling->get_all_products()->num_rows();

        $config['per_page'] = ($this->input->get('show')) ? $this->input->get('show') : 12;
        $this->pagination->initialize($config);
        $start = $this->input->get('page') ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Produk Terlaris', site_url('Produk Terlaris'));

        //pagination
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['meta_description'] = 'Produk Terlaris dari Tokomanamana.com';
        $this->data['products'] = $products;
        $this->data['total_products'] = $config['total_rows'];
        $this->data['start'] = $start + 1;
        $this->data['to'] = ($this->data['total_products'] < ($this->data['start'] * $config['per_page'])) ? $this->data['total_products'] : ($this->data['start'] * $config['per_page']);
        $this->output->set_title('Produk Terlaris - ' . settings('meta_title'));
        $this->template->_init();

        $this->load->view('best_selling', $this->data);
    }

    public function load_more_products() {
        $last_view = $this->input->post('viewed');
        $last_key = $this->input->post('last_key');
        $per_page = 12;

        $conditions = [];

        $conditions['where'] = "products.viewed < $last_view";
        $conditions['limit'] = $per_page;

        $conditions['return_type'] = 'count';
        $products_num = $this->bestselling->get_more_all_products($conditions);
        $conditions['return_type'] = '';
        $products = $this->bestselling->get_more_all_products($conditions);


        $this->data['products'] = $products;
        $this->data['products_num'] = $products_num;
        $this->data['products_limit'] = $per_page;
        $this->data['key'] = $last_key;

        $this->load->view('more_bestselling', $this->data, false);
    }
}
