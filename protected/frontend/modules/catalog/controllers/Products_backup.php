<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Products extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->lang->load('catalog', settings('language'));
        $this->load->model('catalog/category_model', 'category');
        $this->load->model('product_model', 'product');
        $this->load->library('breadcrumb');
        $this->load->library('rajaongkir');
        $this->load->model('cart_model');
    }

    public function view($product_id, $store_id)
    {
        $this->load->library('user_agent');
        $product = $this->product->get_detail_products($product_id, $store_id);

        if (!$product)
            redirect('error_404');

        $now = date('Y-m-d H:i:s');
        $this->product->add_viewed($product->product_id);


        /* setting for last seen product section */
        if ($this->ion_auth->logged_in()) {
            $user_id = $this->data['user']->id;

            /* checking if the product is already in the product_last_review table
            ** if the product viewed is already in the product_last_review table
            ** the data will be updating, but if isn't it will inserting a new data into database */
            $cek_exist_product = $this->main->get('product_last_review', array('product_id' => $product->stock_id, 'user_id' => $user_id));

            if ($cek_exist_product) {
                $this->main->update('product_last_review', array('modified_time' => $now), array('product_id' => $product->stock_id, 'user_id' => $user_id));
            } else {
                $this->main->insert('product_last_review', array('product_id' => $product->stock_id, 'user_id' => $user_id, 'modified_time' => $now));
            }

            /* get_all_last_seen_product() is used from getting all data from database
            ** please note :    product_id column in product_last_review table
            **                  is stored the id column from products_principal_stock table */
            // $last_seen_product = $this->product->product_last_view(array('product_id' => $product->stock_id, 'user_id' => $user_id), 'modified_time', 'DESC', 6);
            $last_seen_product_detail = $this->product->get_all_last_seen_product($user_id, FALSE, 6, $product->stock_id);

            $count = $this->product->get_all_last_seen_product($user_id, FALSE, FALSE, FALSE);

            if ($count->num_rows() > 24) {
                $this->product->delete_product_last_view($count->last_row()->id);
            }

            /* getting the wishlist */
            $this->data['mywish'] = $this->main->get('wishlist', array('customer' => $user_id, 'product' => $product_id, 'branch_id' => $store_id));
        } else {
            $ip_address = $this->input->ip_address();

            $cek_exist_product = $this->main->get('product_last_review', array('product_id' => $product->stock_id, 'ip_address' => $ip_address));

            if ($cek_exist_product) {
                $this->main->update('product_last_review', array('modified_time' => $now), array('product_id' => $product->stock_id, 'ip_address' => $ip_address));
            } else {
                $this->main->insert('product_last_review', array('product_id' => $product->stock_id, 'ip_address' => $ip_address, 'modified_time' => $now));
            }

            // $last_seen_product = $this->product->product_last_view(array('product_id' => $product->stock_id, 'ip_address' => $ip_address), 'modified_time', 'DESC', 6);
            $last_seen_product_detail = $this->product->get_all_last_seen_product(FALSE, $this->input->ip_address(), 6, $product->stock_id);

            $count = $this->product->get_all_last_seen_product(FALSE, $ip_address, FALSE, FALSE);

            if ($count->num_rows() > 24) {
                $this->product->delete_product_last_view($count->last_row()->id);
            }

            $this->data['mywish'] = $this->main->get('wishlist', array('customer' => 0, 'product' => $product_id, 'branch_id' => $store_id));
        }


        /* if the last seen product is less than 6 data, it will be added with product related */
        if ($last_seen_product_detail) {
            if ($last_seen_product_detail->num_rows() < 6) {

                if ($this->ion_auth->logged_in()) {
                    $this->data['last_seen_product'] = $this->product->last_seen_with_additional($this->data['user']->id, FALSE, 6);
                } else {
                    $this->data['last_seen_product'] = $this->product->last_seen_with_additional(FALSE, $this->input->ip_address(), 6);
                }
            } else {
                $this->data['last_seen_product'] = $last_seen_product_detail;
            }
        }


        if ($product->promo == 1) {
            $promo_data = json_decode($product->promo_data, true);

            if ($promo_data['type'] == 'P') {
                $disc_price = round(($product->price * $promo_data['discount']) / 100);
                $label_disc = $promo_data['discount'] . '% off';
            } else {
                $disc_price = $promo_data['discount'];
                $label_disc = 'SALE';
            }

            $end_price = intval($product->price) - intval($disc_price);
            $today = date('Y-m-d');
            $today = date('Y-m-d', strtotime($today));
            $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
            $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

            if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                $this->data['price'] = $end_price;
                $this->data['inPromo'] = true;
                $this->data['label_disc'] =  $label_disc;
            } else {
                $this->data['price'] = $product->price;
                $this->data['inPromo'] = false;
                $this->data['label_disc'] =  '';
            }
        } else {
            $this->data['price'] = $product->price;
            $this->data['inPromo'] = false;
            $this->data['label_disc'] =  '';
        }

        if ($product->merchant == 0) {
            $merchant = $this->session->userdata('list_merchant')[0];
            $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
            if ($price_group) {
                $this->data['old_price'] = intval($price_group->price);
                if ($product->promo == 1) {
                    if ($promo_data['type'] == 'P') {
                        $promo_data['discount'] = intval($promo_data['discount']);
                        $disc_price_group = round((intval($price_group->price) * $promo_data['discount']) / 100);
                    } else {
                        $disc_price_group = intval($promo_data['discount']);
                    }
                    $end_price_group = intval($price_group->price) - $disc_price_group;

                    if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                        $this->data['price'] = intval($end_price_group);
                    } else {
                        $this->data['price'] = intval($price_group->price);
                    }
                } else {
                    $this->data['price'] = intval($price_group->price);
                }
            }
            // $this->db->limit(2);
            // $this->data['price_level'] = $this->product->price_level_group($product->product_id, $merchant['group']);
            $this->data['price_level'] = $this->product->product_price_level($product->product_id);
        }

        $this->data['images'] = $this->product->get_images($product_id);

        if ($this->data['images']) {
            $this->data['meta_image'] = $this->data['images']->row(0)->image;
        }

        $this->data['is_option'] = false;

        if ($this->main->gets('product_option', array('product' => $product->product_id))) {
            $this->data['is_option'] = true;
            $options = $this->option($product->product_id);
            $this->data['options'] = $options['options'];
            $this->data['images'] = $options['images'];
            $this->data['options_setting'] = $options;
            // $this->data['images_carousel'] = $options['images_carousel'];
        }

        // $this->data['reseller'] = $this->main->gets('product_price_level', array('product' => $product_id, 'type' => 'reseller'));
        $this->data['reseller'] = $this->main->gets('product_price_reseller', ['product' => $product_id]);
        $this->data['spesifikasi'] = json_decode($product->table_description);
        $this->data['features'] = $this->product->get_product_features($product->product_id);
        $this->data['rating'] = $this->product->get_rating($product->product_id, $product->branch_id);
        $this->data['rating']->rating = round($this->data['rating']->rating);
        $this->data['reviews'] = $this->product->get_reviews($product->product_id, $product->branch_id);
        $this->data['category'] = $this->product->get_product_by_category($product->category, $product->product_id, $product->price);

        //breadcrumb
        $categoryPaths = $this->category->get_category_path($product->category);
        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());

        if ($categoryPaths)
            foreach ($categoryPaths->result() as $categoryPath)
                $this->breadcrumb->add($categoryPath->name, seo_url('catalog/categories/view/' . $categoryPath->id));

        $this->breadcrumb->add($product->name, seo_url('catalog/products/view/' . $product->product_id . '/' . $store_id));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['meta_description'] = ($product->meta_description) ? $product->meta_description : $product->short_description;
        $this->data['meta_keyword'] = ($product->meta_keyword) ? $product->meta_keyword : settings('meta_keyword');
        $this->data['page'] = 'product';
        $this->data['merchants'] = $this->product->get_merchants($product->product_id);
        $this->data['also_boughts'] = $this->product->get_also_boughts($categoryPaths->row(0)->id, $product->product_id, 6);
        $this->data['product_relations'] = $this->product->get_related_product($product->category, $product->product_id, 6, $categoryPaths->row(0)->id);
        $this->data['products_merchant'] = $this->product->get_products_merchant($product->product_id, $product->store_type, $product->branch_id);

        $this->data['data'] = $product;
        $this->data['data']->store_id = $store_id;
        $this->data['old_price'] = intval($product->price);
        $this->data['show_last_seen_product'] =  TRUE;
        if($this->ion_auth->logged_in()) {
            $this->data['customer'] = $this->main->get('customers', ['id' => $this->data['user']->id]);
        } else {
            $this->data['customer'] = false;
        }

        $this->output->set_title((($product->meta_title) ? $product->meta_title : $product->name) . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->css('assets/frontend/css/minified/product_min.css');
        $this->load->css('assets/frontend/css/jssocials-theme-flat.css');
        $this->load->css('assets/frontend/css/jquery.fancybox.min.css');
        $this->load->css('https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.2/flexslider.min.css');
        $this->load->js('assets/frontend/js/jssocials.js');
        $this->load->js('assets/frontend/js/jquery.fancybox.min.js');
        $this->load->js('https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.2/jquery.flexslider-min.js');
        // $this->load->js(base_url() . 'assets/frontend/js/minified/product.min.js');
        $this->load->js(base_url() . 'assets/frontend/js/modules/product.js');
        $this->load->view('products/views', $this->data);
    }

    public function get_grochire () {
        $this->input->is_ajax_request() or exit(redirect('error_403'));

        $id = $this->input->post('id');
        $qty = $this->input->post('qty');
        $type = $this->input->post('type');
        if($this->input->post('option')) {
            $option = decode($this->input->post('option', true));
        } else {
            $option = false;
        }

        $product_option = false;
        if($option != false) {
            $product_option_ = $this->main->get('product_option', ['id' => $option]);
            if($product_option_->price != 0) {
                $product_option = $product_option_;
            } else {
                $product_option = false;
            }
        } else {
            $product_option = false;
        }

        $return = array();

        $product = $this->main->get('products', array('id' => decode($id)));
        if ($qty > 1) {
            if($type == 'grochire') {
                if($product_option) {
                    $data = $this->main->get('product_price_level', array('product' => decode($id), 'min_qty' => $qty));
                    if ($data && $product) {
                        $range_price = $product->price - $data->price;
                        $fixed_price = $product_option->price - $range_price;
                        if ($product->promo_data && $product->promo) {
                            $promo_data = json_decode($product->promo_data, true);
            
                            if ($promo_data['type'] == 'P') {
                                $end_price = $fixed_price - (round($fixed_price * $promo_data['discount']) / 100);
                            } else {
                                if ($promo_data['discount'] >= $fixed_price) {
                                    $end_price = $promo_data['discount'];
                                } else {
                                    $end_price = $fixed_price - $promo_data['discount'];
                                }
                            }
            
                            $today = date('Y-m-d');
                            $today = date('Y-m-d', strtotime($today));
                            $date_begin = date('Y-m-d', strtotime($promo_data['date_start']));
                            $date_end = date('Y-m-d', strtotime($promo_data['date_end']));
            
                            if (($today >= $date_begin) && ($today <= $date_end)){
                                $price = $end_price;
                            } else {
                                $price = $fixed_price;
                            }
                        } else {
                            $price = $fixed_price;
                        }
            
                        $return = array(
                            'status' => 'success',
                            'price' => $price
                        );
                    }
                } else {
                    $data = $this->main->get('product_price_level', array('product' => decode($id), 'min_qty' => $qty));
                    if ($data && $product) {
                        if ($product->promo_data && $product->promo) {
                            $promo_data = json_decode($product->promo_data, true);
            
                            if ($promo_data['type'] == 'P') {
                                $end_price = $data->price - (round($data->price * $promo_data['discount']) / 100);
                            } else {
                                if ($promo_data['discount'] >= $data->price) {
                                    $end_price = $promo_data['discount'];
                                } else {
                                    $end_price = $data->price - $promo_data['discount'];
                                }
                            }
            
                            $today = date('Y-m-d');
                            $today = date('Y-m-d', strtotime($today));
                            $date_begin = date('Y-m-d', strtotime($promo_data['date_start']));
                            $date_end = date('Y-m-d', strtotime($promo_data['date_end']));
            
                            if (($today >= $date_begin) && ($today <= $date_end)){
                                $price = $end_price;
                            } else {
                                $price = $data->price;
                            }
                        } else {
                            $price = $data->price;
                        }
            
                        $return = array(
                            'status' => 'success',
                            'price' => $price
                        );
                    }
                }
            } else {
                if($product_option) {
                    $data = $this->main->get('product_price_reseller', array('product' => decode($id), 'min_qty' => $qty));
                    if ($data && $product) {
                        $range_price = $product->price - $data->price;
                        $fixed_price = $product_option->price - $range_price;
                        if ($product->promo_data && $product->promo) {
                            $promo_data = json_decode($product->promo_data, true);
            
                            if ($promo_data['type'] == 'P') {
                                $end_price = $fixed_price - (round($fixed_price * $promo_data['discount']) / 100);
                            } else {
                                if ($promo_data['discount'] >= $fixed_price) {
                                    $end_price = $promo_data['discount'];
                                } else {
                                    $end_price = $fixed_price - $promo_data['discount'];
                                }
                            }
            
                            $today = date('Y-m-d');
                            $today = date('Y-m-d', strtotime($today));
                            $date_begin = date('Y-m-d', strtotime($promo_data['date_start']));
                            $date_end = date('Y-m-d', strtotime($promo_data['date_end']));
            
                            if (($today >= $date_begin) && ($today <= $date_end)){
                                $price = $end_price;
                            } else {
                                $price = $fixed_price;
                            }
                        } else {
                            $price = $fixed_price;
                        }
            
                        $return = array(
                            'status' => 'success',
                            'price' => $price
                        );
                    }
                } else {
                    $data = $this->main->get('product_price_reseller', array('product' => decode($id), 'min_qty' => $qty));
                    if ($data && $product) {
                        if ($product->promo_data && $product->promo) {
                            $promo_data = json_decode($product->promo_data, true);
            
                            if ($promo_data['type'] == 'P') {
                                $end_price = $data->price - (round($data->price * $promo_data['discount']) / 100);
                            } else {
                                if ($promo_data['discount'] >= $data->price) {
                                    $end_price = $promo_data['discount'];
                                } else {
                                    $end_price = $data->price - $promo_data['discount'];
                                }
                            }
            
                            $today = date('Y-m-d');
                            $today = date('Y-m-d', strtotime($today));
                            $date_begin = date('Y-m-d', strtotime($promo_data['date_start']));
                            $date_end = date('Y-m-d', strtotime($promo_data['date_end']));
            
                            if (($today >= $date_begin) && ($today <= $date_end)){
                                $price = $end_price;
                            } else {
                                $price = $data->price;
                            }
                        } else {
                            $price = $data->price;
                        }
            
                        $return = array(
                            'status' => 'success',
                            'price' => $price
                        );
                    }
                }
            }
        } elseif ($qty == 1) {
            if($product_option) {
                if ($product->promo_data && $product->promo) {
                    $promo_data = json_decode($product->promo_data, true);

                    if ($promo_data['type'] == 'P') {
                        $end_price = $product_option->price - (round($product_option->price * $promo_data['discount']) / 100);
                    } else {
                        if ($promo_data['discount'] >= $product_option->price) {
                            $end_price = $promo_data['discount'];
                        } else {
                            $end_price = $product_option->price - $promo_data['discount'];
                        }
                    }

                    $today = date('Y-m-d');
                    $today = date('Y-m-d', strtotime($today));
                    $date_begin = date('Y-m-d', strtotime($promo_data['date_start']));
                    $date_end = date('Y-m-d', strtotime($promo_data['date_end']));

                    if (($today >= $date_begin) && ($today <= $date_end)){
                        $price = $end_price;
                    } else {
                        $price = $product_option->price;
                    }
                } else {
                    $price = $product_option->price;
                }
            } else {
                if ($product->promo_data && $product->promo) {
                    $promo_data = json_decode($product->promo_data, true);

                    if ($promo_data['type'] == 'P') {
                        $end_price = $product->price - (round($product->price * $promo_data['discount']) / 100);
                    } else {
                        if ($promo_data['discount'] >= $product->price) {
                            $end_price = $promo_data['discount'];
                        } else {
                            $end_price = $product->price - $promo_data['discount'];
                        }
                    }

                    $today = date('Y-m-d');
                    $today = date('Y-m-d', strtotime($today));
                    $date_begin = date('Y-m-d', strtotime($promo_data['date_start']));
                    $date_end = date('Y-m-d', strtotime($promo_data['date_end']));

                    if (($today >= $date_begin) && ($today <= $date_end)){
                        $price = $end_price;
                    } else {
                        $price = $product->price;
                    }
                } else {
                    $price = $product->price;
                }
            }
            $return = array(
                'status' => 'success',
                'price' => $price
            );
        }

        echo json_encode($return);
    }

    // private function option($product, $selected = array())
    // {
    //     $res = array();
    //     $options = $this->product->get_option_groups($product);
    //     $groups = array();
    //     foreach ($options->result() as $option) {
    //         if ($option->type == 'c' && $option->color) {
    //             $colors[$option->option]['value'] = $option->color;
    //             $colors[$option->option]['name'] = $option->option_name;
    //         }
    //         if (!isset($groups[$option->group])) {
    //             $groups[$option->group] = array(
    //                 'name' => $option->group_name,
    //                 'type' => $option->type,
    //                 'default' => -1,
    //                 'price' => $option->price
    //             );
    //         }
    //         $groups[$option->group]['options'][$option->option] = array(
    //             'name' => $option->option_name,
    //             'color' => $option->color,
    //             'selected' => (isset($selected[$option->group]) && $selected[$option->group] == $option->option) ? true : false,
    //             'price' => $option->price
    //         );
    //         if (!$selected) {
    //             if ($option->default && $groups[$option->group]['default'] == -1) {
    //                 $groups[$option->group]['default'] = $option->option;
    //                 $current_product_option = $option->product_option;
    //             }
    //         }
    //     }
    //     if (!$selected) {
    //         foreach ($groups as $key => $group) {
    //             foreach ($group['options'] as $id_option => $option) {
    //                 if ($group['default'] == $id_option) {
    //                     $groups[$key]['options'][$id_option]['selected'] = true;
    //                     continue;
    //                 }
    //             }
    //         }
    //     } else {
    //         $current_product_option = $this->product->get_product_option_id($product, $selected);
    //     }
    //     $data['option_groups'] = $groups;
    //     $res['options'] = $this->load->view('products/option', $data, true);
        
    //     if ($this->main->gets('product_option_image', array('product_option' => $current_product_option))) {
    //         $images = $this->product->get_product_option_images($current_product_option);
    //     } else {
    //         $images = $this->product->get_images($product);
    //     }

    //     $res['images'] = array();
    //     $res['images_small'] = array();
    //     $list_images = array();
    //     $img_primary = array();

    //     if ($images) {
    //         foreach ($images->result_array() as $image) {
    //             $list_image[] = $image;
    //         }
    //         $newlist_img = array_merge($img_primary, $list_images);
    //         foreach ($newlist_img as $image) {
    //             if ($this->agent->is_mobile()) {
    //                 $res['images'][] =  '<li style="width: 347px; margin-right: 0px; float: left; display: block;">' .
    //                                         '<a href="' . get_image($image['image']) . '" data-fancybox="gallery" data-options=`{"loop":true,"smallBtn":false,"toolbar":false}`>' .
    //                                             '<img src="' . get_image($image['image']) . '" data-item="' . $image['id'] . '">' .
    //                                         '</a>' .
    //                                     '</li>';
    //             } else {
    //                 $res['images'][] =  '<li style="width: 465px; margin-right: 0px; float: left; display: block;">' .
    //                                         '<a href="' . get_image($image['image']) . '" data-fancybox="gallery" data-options=`{"loop":true,"smallBtn":false,"toolbar":false}`>' .
    //                                             '<img src="' . get_image($image['image']) . '" data-item="' . $image['id'] . '">' .
    //                                         '</a>' .
    //                                     '</li>';
    //             }
    //             $res['images_small'][] =    '<li style="width: 100px; margin-right: 5px; float: left; display: block;">' .
    //                                             '<img src="' . get_image($image['image']) . '" data-item="' . $image['id'] . '" draggable="false">' .
    //                                         '</li>';
    //         }
    //     }

    //     $get_current_option = $this->main->get('product_option', array('id' => $current_product_option));
    //     if ($this->main->gets('product_option', array('product' => $product))) {
    //         $res['price'] = $this->_get_harga($product) + $get_current_option->price;
    //     }

    //     $get_price_level = $this->main->gets('product_price_level', array('product' => $product));
    //     if ($get_price_level) {
    //         $get_product = $this->main->get('products', array('id' => $product));
    //         if ($get_product->promo == 1) {
    //             $promo_data = json_decode($get_product->promo_data, true);
    //             $today = date('Y-m-d');
    //             $today = date('Y-m-d', strtotime($today));
    //             $date_begin = date('Y-m-d', strtotime($promo_data->date_start));
    //             $date_end = date('Y-m-d', strtotime($promo_data->date_end));

    //             if (($today >= $date_begin) && ($today <= $date_end)) {
    //                 if ($promo_data->type == 'P') {
    //                     $discount_percent = $promo_data->discount;
    //                     $discount_ammount = round((($get_product->price + $get_current_option->price) * $promo_data->discount) / 100);
    //                 } else {
    //                     $discount_percent = NULL;
    //                     $discount_ammount = $promo_data->discount;
    //                 }

    //                 $discount = $discount_ammount;
    //                 $inPromo = TRUE;
    //             } else {
    //                 $discount = 0;
    //                 $inPromo = FALSE;
    //             }
    //         } else {
    //             $discount = 0;
    //             $inPromo = FALSE;
    //         }
    //         $first_price_level = $get_price_level->row(0);
    //         $count_price_level = $get_price_level->num_rows();
    //     }
    //     $res['grochire'] = '';
        
    //     if($get_price_level) {
    //         foreach ($get_price_level->result() as $row => $level) {
            
    //         }
    //     }

    //     return $res;
    // }

    private function option($product, $selected = array())
    {
        $res = array();
        $options = $this->product->get_option_groups($product);
        //        $current_product_option = 0;
        //        if ($options->num_rows() > 0) {
        //            $this->data['is_option'] = true;
        //            $colors = array();
        $groups = array();
        //            $combinations = array();
        //            $combination_prices = array();
        //            $combination_images = $this->product->get_combination_images($product);
        foreach ($options->result() as $option) {
            //                print_r($option);
            if ($option->type == 'c' && $option->color) {
                $colors[$option->option]['value'] = $option->color;
                $colors[$option->option]['name'] = $option->option_name;
            }
            if (!isset($groups[$option->group])) {
                $groups[$option->group] = array(
                    'name' => $option->group_name,
                    'type' => $option->type,
                    'default' => -1,
                    'price' => $option->price
                );
            }
            $groups[$option->group]['options'][$option->option] = array(
                'name' => $option->option_name,
                'color' => $option->color,
                'selected' => (isset($selected[$option->group]) && $selected[$option->group] == $option->option) ? true : false,
                'price' => $option->price
            );
            if (!$selected) {
                if ($option->default && $groups[$option->group]['default'] == -1) {
                    $groups[$option->group]['default'] = $option->option;
                    $current_product_option = $option->product_option;
                }
            }
        }
        if (!$selected) {
            foreach ($groups as $key => $group) {
                foreach ($group['options'] as $id_option => $option) {
                    if ($group['default'] == $id_option) {
                        $groups[$key]['options'][$id_option]['selected'] = true;
                        continue;
                    }
                }
            }
        } else {
            $current_product_option = $this->product->get_product_option_id($product, $selected);
        }
        $data['option_groups'] = $groups;
        $res['options'] = $this->load->view('products/option', $data, true);
        //            $this->data['option_groups'] = $groups;
        //get images group by product option
        //            $image_groups = $this->db->select("po.id, GROUP_CONCAT(o.id ORDER BY og.sort_order ASC, o.sort_order ASC, og.name ASC SEPARATOR '-') path, po.default", false)
        //                            ->join('product_option_combination poc', 'poc.product_option = po.id', 'left')
        //                            ->join('options o', 'poc.option = o.id', 'left')
        //                            ->join('option_group og', 'o.group = og.id', 'left')
        //                            ->where('po.product', $product)
        //                            ->group_by('po.id')
        //                            ->get('product_option po')->result_array();
        //            foreach ($image_groups as $key => $image_group) {
        //                //get image by product option
        //                $images = $this->db->select('pi.id, pi.image')
        //                        ->join('product_image pi', 'pi.id = poi.product_image', 'left')
        //                        ->where('poi.product_option', $image_group['id'])
        //                        ->get('product_option_image poi');
        //                if ($images->num_rows() > 0) {
        //                    $image_groups[$key]['images'] = $images->result_array();
        //                } else {
        //                    $image_groups[$key]['images'] = $this->data['images']->result_array();
        //                }
        //            }
        if ($this->main->gets('product_option_image', array('product_option' => $current_product_option))) {
            $images = $this->product->get_product_option_images($current_product_option);
        } else {
            $images = $this->product->get_images($product);
        }
        $data_product = $this->product->get_product($product);
        $list_image = [];
        $img_primary = [];
        $res['images'] = [];
        $res['images_small'] = [];
        if ($images) {
            foreach ($images->result_array() as $image) {
                $list_image[] = $image;
            }
            $newlist_img = array_merge($list_image, $img_primary);
            foreach ($newlist_img as $image) {
                if ($this->agent->is_mobile()) {
                    $res['images'][] =  '<li style="width: 347px; margin-right: 0px; float: left; display: block;">' .
                        '<a href="' . get_image($image['image']) . '" data-fancybox="gallery" data-options=`{"loop":true,"smallBtn":false,"toolbar":false}`>' .
                        '<img src="' . get_image($image['image']) . '" data-item="' . $image['id'] . '">' .
                        '</a>' .
                        '</li>';
                } else {
                    $res['images'][] =  '<li style="width: 367px; margin-right: 0px; float: left; display: block;">' .
                        '<a href="' . get_image($image['image']) . '" data-fancybox="gallery" data-options=`{"loop":true,"smallBtn":false,"toolbar":false}`>' .
                        '<img src="' . get_image($image['image']) . '" data-item="' . $image['id'] . '">' .
                        '</a>' .
                        '</li>';
                }
                $res['images_small'][] =    '<li style="margin-right: 5px; float: left; display: block;width:auto;width:100px;">' .
                    '<img src="' . get_image($image['image']) . '" data-item="' . $image['id'] . '" draggable="false">' .
                    '</li>';
            }
        }

        // if ($images) {
        //     $res['images'] = '';
        //     $res['images'] .= '<ul class="slides">';
        //     foreach ($images->result() as $image) {
        //         // $res['images'] .= '<div class="image-item">' .
        //         //         '<a href="' . get_image($image->image) . '" class="thumbnail fancybox" data-fancybox="gallery">' .
        //         //         '<img src="' . get_image($image->image) . '" data-item="' . $image->id . '" />' .
        //         //         '</a>' .
        //         //         '</div>';
        //         // $res['images'] .= '<li style="margin-right: 0px; float: left; display: block; width: 367px;">';
        //         $res['images'] .= '<li>';
        //         $res['images'] .= '<a href="' . get_image($image->image) . '" data-fancybox="gallery" data-options=\'{"loop":true,"smallBtn":false,"toolbar":false}\'>';
        //         $res['images'] .= '<img src="' . get_image($image->image) . '" alt="' . $data_product->name . '" data-item="' . $image->id . '" >';
        //         $res['images'] .= '</a>';
        //         $res['images'] .= '</li>';
        //     }
        //     $res['images'] .= '</ul>';

        //     $res['images_carousel'] = '';
        //     $res['images_carousel'] .= '<ul class="slides" style="width: 200%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">';
        //     foreach($images->result() as $image) {
        //         // $res['images_carousel'] .= '<li style="margin-right: 5px; float: left; display: block; width: 100px;">';
        //         $res['images_carousel'] .= '<li>';
        //         $res['images_carousel'] .= '<a href="' . get_image($image->image) . '" data-fancybox="gallery" data-options=\'{"loop":true,"smallBtn":false,"toolbar":false}\'>';
        //         $res['images_carousel'] .= '<img src="' . get_image($image->image) . '" alt="' . $data_product->name . '" data-item="' . $image->id . '" >';
        //         $res['images_carousel'] .= '</a>';
        //         $res['images_carousel'] .= '</li>';
        //     }
        //     $res['images_carousel'] .= '</ul>';
        // }

        $res['price'] = '';
        $res['old_price'] = '';
        $data_option = $current_product_option;
        $data_optionnn = $this->product->get_option_groups($product, $data_option);
        // var_dump($data_product);die;
        // var_dump($data_optionnn->row()); die;
        if ($data_optionnn->row()->price == 0) {
            if ($data_product->promo == 1) {
                $promo_data = json_decode($data_product->promo_data);
                if ($promo_data->type == 'P') {
                    $disc_price = round(($data_product->price * $promo_data->discount) / 100);
                } else {
                    $disc_price = $promo_data->discount;
                }
                // $end_price= $data_product->price - $disc_price;
                $today = date('Y-m-d');
                $today = date('Y-m-d', strtotime($today));
                $DateBegin = date('Y-m-d', strtotime($promo_data->date_start));
                $DateEnd = date('Y-m-d', strtotime($promo_data->date_end));

                if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                    $res['price'] = $data_product->price - $disc_price;
                    $res['old_price'] = $data_product->price;
                } else {
                    $res['price'] = $data_product->price;
                    $res['old_price'] = $data_product->price;
                }
                // $res['price'] = $data_product->price - $disc_price;
                // $res['old_price'] = $data_product->price;
            } else {
                $res['price'] = $data_product->price;
                $res['old_price'] = $data_product->price;
            }
        } else {
            if ($data_product->promo == 1) {
                $promo_data = json_decode($data_product->promo_data);
                if ($promo_data->type == 'P') {
                    $disc_price = round(($data_optionnn->row()->price * $promo_data->discount) / 100);
                } else {
                    $disc_price = $promo_data->discount;
                }
                $res['price'] = $data_optionnn->row()->price - $disc_price;
                $res['old_price'] = $data_optionnn->row()->price;
            } else {
                $res['price'] = $data_optionnn->row()->price;
                $res['old_price'] = $data_optionnn->row()->price;
            }
        }
        $price_level = $this->main->gets('product_price_level', ['product' => $product]);
        $price_level_price = [];
        if($price_level && $price_level->num_rows() > 0) {
            if($data_optionnn->row()->price != 0) {
                $price_option = $data_optionnn->row()->price;
                $product_price = $data_product->price;
                foreach($price_level->result() as $key => $price_l) {
                    $range_price = $product_price - $price_l->price;
                    $fixed_price_level = $price_option - $range_price;
                    array_push($price_level_price, [
                        'min_qty' => $price_l->min_qty,
                        'price' => $fixed_price_level,
                        'product_id' => encode($price_l->product)
                    ]);
                }
            } else {
                foreach($price_level->result() as $key => $price_l) {
                    array_push($price_level_price, [
                        'min_qty' => $price_l->min_qty,
                        'price' => $price_l->price,
                        'product_id' => encode($price_l->product)
                    ]);
                }
            }
        }
        $reseller = $this->main->gets('product_price_reseller', ['product' => $product]);
        if($reseller && $reseller->num_rows() > 0) {
            if($data_optionnn->row()->price != 0) {
                $price_option = $data_optionnn->row()->price;
                $product_price = $data_product->price;
                foreach($reseller->result() as $key => $reseller_p) {
                    $range_price = $product_price - $reseller_p->price;
                    $fixed_reseller_price = $price_option - $range_price;
                    array_push($price_level_price, [
                        'min_qty' => $reseller_p->min_qty,
                        'price' => $fixed_reseller_price,
                        'product_id' => encode($reseller_p->product)
                    ]);
                }
            } else {
                foreach($reseller->result() as $key => $reseller_p) {
                    array_push($price_level_price, [
                        'min_qty' => $reseller_p->min_qty,
                        'price' => $reseller_p->price,
                        'product_id' => encode($reseller_p->product)
                    ]);
                }
            }
        }
        $res['price_level_option'] = $price_level_price;

        $res['in_promo'] = $data_product->promo;
        $res['product_option'] = encode($data_optionnn->row()->product_option);


        //        $this->data['images'] = $image_groups;
        //        }
        return $res;
    }

    private function _get_harga($id) {
        $product = $this->product->get_product($id);
        $this->data['data'] = $product;
        $this->data['old_price'] = $product->price;

        if ($product->promo == '1') {
            $promo_data = json_decode($product->promo_data, true);
            if ($promo_data['type'] == 'P') {
                $disc_price = round(($product->price * $promo_data['discount']) / 100);
            } else {
                $disc_price = $promo_data['discount'];
            }
            $end_price = $product->price - $disc_price;
            $today = date('Y-m-d');
            $today = date('Y-m-d', strtotime($today));
            $date_begin = date('Y-m-d', strtotime($promo_data['date_start']));
            $date_end = date('Y-m-d', strtotime($promo_data['date_end']));

            if (($today >= $date_begin) && ($today <= $date_end)) {
                $this->data['price'] = $end_price;
                $this->data['inPromo'] = true;
            } else {
                $this->data['price'] = $product->price;
                $this->data['inPromo'] = false;
            }
        } else {
            $this->data['price'] = $product->price;
            $this->data['inPromo'] = false;
        }

        if ($product->merchant == 0) {
            $merchant = $this->session->userdata('list_merchant')[0];
            $price_group = $this->main->get('product_price', array('product' => $product->id, 'merchant_group' => $merchant['group']));
            if ($price_group) {
                $this->data['old_price'] = $price_group->price;
                if ($product->promo == '1') {
                    if ($promo_data['type'] == 'P') {
                        $disc_price_group = round(($price_group->price * $promo_data['discount']) / 100);
                    } else {
                        $disc_price_group = $promo_data['discount'];
                    }
                    $end_price_group = $price_group->price - $disc_price_group;

                    if (($today >= $date_begin) && ($today <= $date_end)) {
                        $this->data['price'] = $end_price_group;
                    } else {
                        $this->data['price'] = $price_group->price;
                    }
                } else {
                    $this->data['price'] = $price_group->price;
                }
            }

            $this->db->limit(2);
            $this->data['price_level'] = $this->product->price_level_group($product->id, $merchant['group']);
        }

        return $this->data['price'];
    }

    public function change_option()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $selected = $this->input->post('option');
        $product = decode($this->input->post('product'));
        $options = $this->option($product, $selected);
        echo json_encode($options);
    }

    public function add_to_cart_ajax()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $data = $this->input->post(null, true);
        $id = decode($data['product']);
        $store = $data['store'];
        $qty = $data['qty'];

        $output = array('status' => 'error', 'message' => 'Qty minimum 1', 'items' => array());

        if ($qty > 0) {
            $product = $this->product->get_detail_products($id, $store);

            if ($product) {
                do {
                    $product_option = 0;

                    if (isset($data['option'])) {
                        $product_option = $this->product->get_product_option_id($product->product_id, $data['option']);

                        if (!$product_option) {
                            $output['message'] = 'Kesalahan memilih variasi produk.';
                            break;
                        }

                        //$product_option = $this->main->get('product_option', array('id' => $current_product_option));
                        //$product_option = $product_options->row();
                        $options = $this->product->get_option_groups($product->product_id, $product_option);
                        $option = $options->row();
                        $image = $this->product->get_product_option_images($product_option);

                        if ($image->num_rows() > 0) {
                            $product->image = $image->row()->image;
                        }
                    }
                    if ($product->length && $product->height && $product->width) {
                        $weight_volume = ($product->length * $product->height * $product->width) / 6;

                        if ($weight_volume > $product->weight)
                            $product->weight = round($weight_volume);
                    }

                    $old_price = $product->price;

                    if ($product->promo == 1) {
                        $promo_data = json_decode($product->promo_data, true);

                        if ($promo_data['type'] == 'P') {
                            $disc_value = $promo_data['discount'];
                            $disc_price = round(($product->price * $promo_data['discount']) / 100);
                        } else {
                            $disc_value = NULL;
                            $disc_price = $promo_data['discount'];
                        }

                        $end_price = $product->price - $disc_price;
                        $today = date('Y-m-d');
                        $today = date('Y-m-d', strtotime($today));
                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                        if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                            $price = $end_price;
                            $inPromo = true;
                        } else {
                            $price = $product->price;
                            $inPromo = false;
                        }
                    } else {
                        $price = $product->price;
                        $inPromo = false;
                    }

                    $rowid = md5($id . ($product_option ? $product_option : 0));
                    $item = $this->cart->get_item($rowid);
                    $merchant = $this->session->userdata('list_merchant')[0];

                    //jika produk tanaka
                    if ($product->merchant == 0) {
                        $price_level = $this->product->price_in_level($product->product_id, $merchant['group'], ($item ? $item['qty'] + $qty : $qty));

                        if ($price_level) { //jika price level ada, maka produk tsb masuk ke harga grosir
                            $price = $price_level;
                            $price_level = 1;
                        } else {
                            $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));

                            if ($price_group) {
                                $old_price = $price_group->price;

                                if ($product->promo == 1) {
                                    if ($promo_data['type'] == 'P') {
                                        $disc_price_group = round(($price_group->price * $promo_data['discount']) / 100);
                                    } else {
                                        $disc_price_group = $promo_data['discount'];
                                    }
                                    $end_price_group = $price_group->price - $disc_price_group;

                                    if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                        $price = $end_price_group;
                                    } else {
                                        $price = $price_group->price;
                                    }
                                } else {
                                    $price = $price_group->price;
                                }
                            }
                        }
                    } else {
                        $price_level = 0;
                    }
                    $price += ($product_option) ? $option->price : 0;
                    $weight = $product->weight + ($product_option ? $option->weight : 0);
                    if ($item) {
                        $item['qty'] += $qty;
                        $item['price'] = $price;
                        $item['price_level'] = $price_level;
                        $item['weight'] = $weight * $item['qty'];

                        $this->cart->update($item);
                    } else {
                        $item = array(
                            'id' => $id . '-' . $store,
                            'name' => $product->name,
                            'type' => $product->type,
                            'image' => $product->image,
                            'code' => $product->code,
                            'category' => $product->category,
                            'description' => $product->short_description,
                            'qty' => $qty,
                            'price' => $price,
                            'preorder' => $product->preorder,
                            'price_level' => $price_level,
                            'merchant' => $product->merchant,
                            'merchant_name' => $product->merchant_name,
                            'merchant_group' => ($product->merchant) ? 0 : $merchant['group'],
                            'weight' => $weight * $qty,
                            'shipping' => '',
                            'shipping_merchant' => $product->merchant,
                            'shipping_cost' => 0,
                            'options' => ($product_option) ? $options->result_array() : array()
                        );
                        if ($product->type == 'p') {
                            $item['package_items'] = json_decode($product->package_items, true);
                        } else {
                            $item['package_items'] = array();
                        }
                        if ($inPromo) {
                            $item['inPromo'] = true;
                            $item['promo_data'] = $promo_data;
                            $item['disc_value'] = $disc_value;
                            $item['disc_price'] = $disc_price;
                            $item['old_price'] = $old_price;
                        } else {
                            $item['inPromo'] = false;
                            $item['promo_data'] = array();
                            $item['disc_value'] = NULL;
                            $item['disc_price'] = NULL;
                            $item['old_price'] = $price;
                        }
                        $this->cart->insert($item);
                    }
                    $output['status'] = 'success';
                } while (0);
            }
            $output['items'] = $this->cart->contents() ? $this->cart->contents() : array();
        }
        echo json_encode($output);
    }

    public function add_to_cart_ajax_new()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $data = $this->input->post(null, true);
        $id = intval(decode($data['product']));
        $store = intval(decode($data['store']));
        $qty = intval($data['qty']);
        $ip_address = $this->input->ip_address();
        $id_customer = $this->data['user']->id;
        if(isset($data['type_merchant'])) {
            $data['type_merchant'] = $data['type_merchant'];
        } else {
            $data['type_merchant'] = false;
        }

        $output = array('status' => 'error', 'message' => 'Qty minimum 1', 'items' => array());

        if ($qty > 0) {
            if (is_int($store) && is_int($id)) {
                $check_merchant = $this->main->get('merchants', ['id' => $store]);
                $product = $this->product->get_detail_products($id, $store);
                if ($product && $check_merchant) {
                    do {
                        $old_price = intval($product->price);
                        $data_option = null;
                        $option = null;
                        if (isset($data['option'])) {
                            $product_option = $this->product->get_product_option_id($product->product_id, $data['option']);

                            if (!$product_option) {
                                $output['status'] = 'error';
                                $output['message'] = 'Kesalahan memilih variasi produk.';
                                exit();
                            }

                            //$product_option = $this->main->get('product_option', array('id' => $current_product_option));
                            $options = $this->product->get_option_groups($product->id, $product_option);
                            $option = $options->row();

                            //print_r($option);
                            //$product_option = $product_options->row();
                            $image = $this->product->get_product_option_images($product_option);
                            if ($image) {
                                $product->image = $image->row()->image;
                            }
                            $data_option = json_encode($options->result());
                            $id_option = $options->row()->product_option;
                            $item = $this->cart_model->check_cart($id, $store, $id_customer, $ip_address, $id_option);

                            if ($option->price == 0) {
                                $old_price = intval($product->price);
                                $price = $old_price;
                            } else {
                                $old_price = intval($option->price);
                            }
                        } else {
                            $id_option = null;
                            $item = $this->cart_model->check_cart($id, $store, $id_customer, $ip_address);
                            $old_price = intval($product->price);
                            $price = $old_price;
                        }
                        if ($product->length && $product->height && $product->width) {
                            $weight_volume = ($product->length * $product->height * $product->width) / 6;
                            if ($weight_volume > $product->weight) {
                                $product->weight = round($weight_volume);
                                $total_weight = $qty * $product->weight;
                            } else {
                                $total_weight = $qty * $product->weight;
                            }
                        } else {
                            $total_weight = $qty * $product->weight;
                        }
                        if ($product->promo == 1) {
                            $promo_data = json_decode($product->promo_data, true);
                            if ($promo_data['type'] == 'P') {
                                $disc_value = $promo_data['discount'];
                                $disc_price = round(($old_price * $promo_data['discount']) / 100);
                            } else {
                                $disc_value = NULL;
                                $disc_price = $promo_data['discount'];
                            }
                            $disc_price = intval($disc_price);
                            $end_price = intval($old_price) - $disc_price;
                            $today = date('Y-m-d');
                            $today = date('Y-m-d', strtotime($today));
                            $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                            $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                            if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                $price = $end_price;
                                $inPromo = true;
                                $promo_type = $promo_data['type'];
                                $data_promo_data = $product->promo_data;
                            } else {
                                $price = $old_price;
                                $inPromo = false;
                                $promo_type = false;
                                $data_promo_data = null;
                            }
                        } else {
                            $price = $old_price;
                            $inPromo = false;
                            $promo_type = false;
                            $disc_value = null;
                            $disc_price = null;
                            $data_promo_data = null;
                        }
                        // $merchant = $this->session->userdata('list_merchant')[0];
                        $price_level = $this->product->product_price_in_level($product->product_id, ($item ? $item['quantity'] + $qty : $qty));
                        if ($price_level) { //jika price level ada, maka produk tsb masuk ke harga grosir
                            $old_price = $price_level;
                            if(isset($data['option'])) {
                                $product_option_price_level = $this->product->get_product_option_id($product->product_id, $data['option']);
                                $options_price_level = $this->product->get_option_groups($product->id, $product_option_price_level);
                                $option_price_level = $options_price_level->row();
                                if($option_price_level->price != 0) {
                                    $range_price_level = intval($product->price) - $price_level;
                                    $fixed_price_level = $option_price_level->price - $range_price_level;
                                    if ($product->promo == 1) {
                                        $promo_data = json_decode($product->promo_data, true);
                                        if ($promo_data['type'] == 'P') {
                                            $disc_value = intval($promo_data['discount']);
                                            $disc_price = round(($fixed_price_level * $disc_value) / 100);
                                            $end_price = intval($fixed_price_level) - $disc_price;
                                        } else {
                                            $disc_price = intval($promo_data['discount']);
                                            $end_price = intval($fixed_price_level) - $disc_price;
                                            $data_promo_data = $product->promo_data;
                                        }
                                        $today = date('Y-m-d');
                                        $today = date('Y-m-d', strtotime($today));
                                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                        if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                            $price = intval($end_price);
                                            $inPromo = true;
                                            $promo_type = $promo_data['type'];
                                            $data_promo_data = $product->promo_data;
                                        } else {
                                            $price = $old_price;
                                            $inPromo = false;
                                            $promo_type = false;
                                            $data_promo_data = null;
                                        }
                                    } else {
                                        $price = intval($fixed_price_level);
                                        $inPromo = false;
                                        $promo_type = false;
                                        $disc_value = null;
                                        $disc_price = null;
                                        $data_promo_data = null;
                                    }
                                } else {
                                    if ($product->promo == 1) {
                                        $promo_data = json_decode($product->promo_data, true);
                                        if ($promo_data['type'] == 'P') {
                                            $disc_value = intval($promo_data['discount']);
                                            $disc_price = round(($price_level * $disc_value) / 100);
                                            $end_price = intval($price_level) - $disc_price;
                                        } else {
                                            $disc_price = intval($promo_data['discount']);
                                            $end_price = intval($price_level) - $disc_price;
                                            $data_promo_data = $product->promo_data;
                                        }
                                        $today = date('Y-m-d');
                                        $today = date('Y-m-d', strtotime($today));
                                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                        if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                            $price = intval($end_price);
                                            $inPromo = true;
                                            $promo_type = $promo_data['type'];
                                            $data_promo_data = $product->promo_data;
                                        } else {
                                            $price = $old_price;
                                            $inPromo = false;
                                            $promo_type = false;
                                            $data_promo_data = null;
                                        }
                                    } else {
                                        $price = intval($price_level);
                                        $inPromo = false;
                                        $promo_type = false;
                                        $disc_value = null;
                                        $disc_price = null;
                                        $data_promo_data = null;
                                    }
                                }
                            } else {
                                if ($product->promo == 1) {
                                    $promo_data = json_decode($product->promo_data, true);
                                    if ($promo_data['type'] == 'P') {
                                        $disc_value = intval($promo_data['discount']);
                                        $disc_price = round(($price_level * $disc_value) / 100);
                                        $end_price = intval($price_level) - $disc_price;
                                    } else {
                                        $disc_price = intval($promo_data['discount']);
                                        $end_price = intval($price_level) - $disc_price;
                                        $data_promo_data = $product->promo_data;
                                    }
                                    $today = date('Y-m-d');
                                    $today = date('Y-m-d', strtotime($today));
                                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                    if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                        $price = intval($end_price);
                                        $inPromo = true;
                                        $promo_type = $promo_data['type'];
                                        $data_promo_data = $product->promo_data;
                                    } else {
                                        $price = $old_price;
                                        $inPromo = false;
                                        $promo_type = false;
                                        $data_promo_data = null;
                                    }
                                } else {
                                    $price = intval($price_level);
                                    $inPromo = false;
                                    $promo_type = false;
                                    $disc_value = null;
                                    $disc_price = null;
                                    $data_promo_data = null;
                                }
                            }
                            $price_level = 1;
                        }
                        $reseller_price = null;
                        if($data['type_merchant'] == 'true') {
                            $reseller_price = $this->product->get_product_reseller($product->product_id, ($item ? $item['quantity'] + $qty : $qty));
                            if($reseller_price) {
                                $status_reseller = 1;
                                $old_price = $reseller_price;
                                if(isset($data['option'])) {
                                    $product_option_reseller = $this->product->get_product_option_id($product->product_id, $data['option']);
                                    $options_reseller = $this->product->get_option_groups($product->id, $product_option_reseller);
                                    $option_price_reseller = $options_reseller->row();
                                    if($option_price_reseller->price != 0) {
                                        $range_reseller_price = intval($product->price) - $reseller_price;
                                        $fixed_reseller_price = $option_price_level->price - $range_reseller_price;
                                        if ($product->promo == 1) {
                                            $promo_data = json_decode($product->promo_data, true);
                                            if ($promo_data['type'] == 'P') {
                                                $disc_value = intval($promo_data['discount']);
                                                $disc_price = round(($fixed_reseller_price * $disc_value) / 100);
                                                $end_price = intval($fixed_reseller_price) - $disc_price;
                                            } else {
                                                $disc_price = intval($promo_data['discount']);
                                                $end_price = intval($fixed_reseller_price) - $disc_price;
                                                $data_promo_data = $product->promo_data;
                                            }
                                            $today = date('Y-m-d');
                                            $today = date('Y-m-d', strtotime($today));
                                            $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                            $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                            if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                                $price = intval($end_price);
                                                $inPromo = true;
                                                $promo_type = $promo_data['type'];
                                                $data_promo_data = $product->promo_data;
                                            } else {
                                                $price = $old_price;
                                                $inPromo = false;
                                                $promo_type = false;
                                                $data_promo_data = null;
                                            }
                                        } else {
                                            $price = intval($fixed_reseller_price);
                                            $inPromo = false;
                                            $promo_type = false;
                                            $disc_value = null;
                                            $disc_price = null;
                                            $data_promo_data = null;
                                        }
                                    } else {
                                        if ($product->promo == 1) {
                                            $promo_data = json_decode($product->promo_data, true);
                                            if ($promo_data['type'] == 'P') {
                                                $disc_value = intval($promo_data['discount']);
                                                $disc_price = round(($reseller_price * $disc_value) / 100);
                                                $end_price = intval($reseller_price) - $disc_price;
                                            } else {
                                                $disc_price = intval($promo_data['discount']);
                                                $end_price = intval($reseller_price) - $disc_price;
                                                $data_promo_data = $product->promo_data;
                                            }
                                            $today = date('Y-m-d');
                                            $today = date('Y-m-d', strtotime($today));
                                            $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                            $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                            if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                                $price = intval($end_price);
                                                $inPromo = true;
                                                $promo_type = $promo_data['type'];
                                                $data_promo_data = $product->promo_data;
                                            } else {
                                                $price = $old_price;
                                                $inPromo = false;
                                                $promo_type = false;
                                                $data_promo_data = null;
                                            }
                                        } else {
                                            $price = intval($reseller_price);
                                            $inPromo = false;
                                            $promo_type = false;
                                            $disc_value = null;
                                            $disc_price = null;
                                            $data_promo_data = null;
                                        }
                                    }
                                } else {
                                    if ($product->promo == 1) {
                                        $promo_data = json_decode($product->promo_data, true);
                                        if ($promo_data['type'] == 'P') {
                                            $disc_value = intval($promo_data['discount']);
                                            $disc_price = round(($reseller_price * $disc_value) / 100);
                                            $end_price = intval($reseller_price) - $disc_price;
                                        } else {
                                            $disc_price = intval($promo_data['discount']);
                                            $end_price = intval($reseller_price) - $disc_price;
                                            $data_promo_data = $product->promo_data;
                                        }
                                        $today = date('Y-m-d');
                                        $today = date('Y-m-d', strtotime($today));
                                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                        if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                            $price = intval($end_price);
                                            $inPromo = true;
                                            $promo_type = $promo_data['type'];
                                            $data_promo_data = $product->promo_data;
                                        } else {
                                            $price = $old_price;
                                            $inPromo = false;
                                            $promo_type = false;
                                            $data_promo_data = null;
                                        }
                                    } else {
                                        $price = intval($reseller_price);
                                        $inPromo = false;
                                        $promo_type = false;
                                        $disc_value = null;
                                        $disc_price = null;
                                        $data_promo_data = null;
                                    }
                                }
                            } else {
                                $status_reseller = 0;
                            }
                        } else {
                            $status_reseller = 0;
                        }
                        // if ($product->merchant == 0) {
                        //     // $price_level = $this->product->price_in_level($product->product_id, $merchant['group'], ($item ? $item['quantity'] + $qty : $qty));
                        //     $price_level = $this->product->product_price_in_level($product->product_id, ($item ? 0 + $qty : $qty));
                        //     $promo_data = json_decode($product->promo_data, true);
                        //     if ($price_level) { //jika price level ada, maka produk tsb masuk ke harga grosir
                        //         $old_price = $price_level;
                        //         if ($product->promo == 1) {
                        //             if ($promo_data['type'] == 'P') {
                        //                 $disc_value = intval($promo_data['discount']);
                        //                 $disc_price = round(($price_level * $disc_value) / 100);
                        //                 $end_price = intval($price_level) - $disc_price;
                        //             } else {
                        //                 $disc_price = intval($promo_data['discount']);
                        //                 $end_price = intval($price_level) - $disc_price;
                        //                 $data_promo_data = $product->promo_data;
                        //             }
                        //             $today = date('Y-m-d');
                        //             $today = date('Y-m-d', strtotime($today));
                        //             $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                        //             $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                        //             if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                        //                 $price = intval($end_price);
                        //                 $inPromo = true;
                        //                 $promo_type = $promo_data['type'];
                        //                 $data_promo_data = $product->promo_data;
                        //             } else {
                        //                 $price = $old_price;
                        //                 $inPromo = false;
                        //                 $promo_type = false;
                        //                 $data_promo_data = null;
                        //             }
                        //         } else {
                        //             $price = intval($price_level);
                        //             $inPromo = false;
                        //             $promo_type = false;
                        //             $disc_value = null;
                        //             $disc_price = null;
                        //             $data_promo_data = null;
                        //         }
                        //         $price_level = 1;
                        //     } else {
                        //         $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
                        //         if ($price_group) {
                        //             if (isset($data['option'])) {
                        //                 $product_option = $this->product->get_product_option_id($product->product_id, $data['option']);

                        //                 $options = $this->product->get_option_groups($product->id, $product_option);
                        //                 $option = $options->row();

                        //                 if ($product->promo == 1) {
                        //                     if ($promo_data['type'] == 'P') {
                        //                         $disc_price = round(($old_price * intval($promo_data['discount'])) / 100);
                        //                     } else {
                        //                         $disc_price = $promo_data['discount'];
                        //                     }
                        //                     $end_price = intval($old_price) - intval($disc_price);
                        //                     $today = date('Y-m-d');
                        //                     $today = date('Y-m-d', strtotime($today));
                        //                     $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                        //                     $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                        //                     if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                        //                         $price = intval($end_price);
                        //                         $data_promo_data = $product->promo_data;
                        //                     } else {
                        //                         if ($option->price == 0) {
                        //                             $old_price = intval($product->price);
                        //                             $price = intval($old_price);
                        //                         } else {
                        //                             $old_price = intval($option->price);
                        //                             $price = intval($old_price);
                        //                         }
                        //                         $data_promo_data = null;
                        //                     }
                        //                 } else {
                        //                     if ($option->price == 0) {
                        //                         $old_price = intval($product->price);
                        //                         $price = intval($old_price);
                        //                     } else {
                        //                         $old_price = intval($option->price);
                        //                         $price = intval($old_price);
                        //                     }
                        //                     $inPromo = false;
                        //                     $promo_type = false;
                        //                     $disc_value = null;
                        //                     $disc_price = null;
                        //                     $data_promo_data = null;
                        //                 }
                        //             } else {
                        //                 $old_price = intval($price_group->price);
                        //                 if ($product->promo == 1) {
                        //                     if ($promo_data['type'] == 'P') {
                        //                         $disc_price_group = round(($price_group->price * intval($promo_data['discount'])) / 100);
                        //                     } else {
                        //                         $disc_price_group = intval($promo_data['discount']);
                        //                     }
                        //                     $end_price_group = intval($price_group->price) - $disc_price_group;

                        //                     if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                        //                         $price = intval($end_price_group);
                        //                         $data_promo_data = $product->promo_data;
                        //                     } else {
                        //                         $price = intval($price_group->price);
                        //                         $data_promo_data = null;
                        //                     }
                        //                 } else {
                        //                     $price = intval($price_group->price);
                        //                     $inPromo = false;
                        //                     $promo_type = false;
                        //                     $disc_value = null;
                        //                     $disc_price = null;
                        //                     $data_promo_data = null;
                        //                 }
                        //             }
                        //         }
                        //     }
                        // } else {
                        //     $price_level = 0;
                        // }
                        if ($product->type == 'p') {
                            $package_items = json_encode($product->package_items);
                        } else {
                            $package_items = null;
                        }
                        $total_weight = intval($total_weight);
                        $price = intval($price);
                        if ($item) {

                            $item['quantity'] += $qty;
                            $item['price'] = $price;
                            $item['total_weight'] += $total_weight;
                            $item['grand_total'] = $price * intval($item['quantity']);
                            $item['price_level'] = $price_level;
                            $item['in_promo'] = $inPromo;
                            $item['promo_type'] = $promo_type;
                            $item['disc_value'] = $disc_value;
                            $item['disc_price'] = $disc_price;
                            $item['old_price'] = $old_price;
                            $item['reseller'] = $status_reseller;

                            $this->db->where('id', $item['id']);
                            $this->db->update('cart', $item);
                        } else {
                            $cart = [
                                'id_customer' => $id_customer,
                                'ip_address' => $ip_address,
                                'status' => 0,
                                'product' => $id,
                                'quantity' => $qty,
                                'merchant' => $store,
                                'total_weight' => $total_weight,
                                'price' => $price,
                                'grand_total' => $qty * $price,
                                'in_promo' => $inPromo,
                                'price_level' => $price_level,
                                'promo_type' => $promo_type,
                                'disc_value' => $disc_value,
                                'disc_price' => $disc_price,
                                'old_price' => $old_price,
                                'image' => $product->image,
                                'options' => $data_option,
                                'package_items' => $package_items,
                                'promo_data' => $data_promo_data,
                                'id_option' => $id_option,
                                'reseller' => $status_reseller
                            ];
                            $this->db->insert('cart', $cart);
                        }

                        $cart_merchant = $this->cart_model->cart_merchant($id_customer, $store)->row_array();
                        if ($cart_merchant['merchant'] == $store) {
                            $cart_merchant['total_weight'] += $total_weight;
                            $this->db->where('merchant', $store);
                            $this->db->where('id_customer', $id_customer);
                            $this->db->update('cart_merchant', $cart_merchant);
                        } else {
                            $data_cart_merchant = [
                                'merchant' => $store,
                                'total_weight' => $total_weight,
                                'id_customer' => $id_customer
                            ];
                            $this->db->insert('cart_merchant', $data_cart_merchant);
                        }
                    } while (0);
                    $output['status'] = 'success';
                    $output['message'] = 'Keranjang Berhasil Ditambah';
                    $output['items'] = $this->cart_model->get_all_cart($id_customer, $ip_address)->result();

                    foreach ($output['items'] as $key => $cart) {
                        $product = $this->main->get('products', ['id' => $cart->product_id]);
                        if ($product->store_type == 'merchant') {
                            $output['items'][$key]->product_quantity = $product->quantity;
                            $output['items'][$key]->id_encrypt = encode($cart->id);
                        } else {
                            $get_product_principal = $this->main->get('products_principal_stock', ['product_id' => $product->id, 'branch_id' => $cart->merchant_id]);
                            $output['items'][$key]->product_quantity = $get_product_principal->quantity;
                            $output['items'][$key]->id_encrypt = encode($cart->id);
                        }
                    }
                } else {
                    $output['status'] = 'error';
                    $output['message'] = 'Produk tidak tersedia!';
                    $output['items'] = [];
                }
            } else {
                $output = [
                    'status' => 'error',
                    'message' => 'Terjadi suatu kesalahan!',
                    'items' => []
                ];
            }
        }
        echo json_encode($output);
    }

    public function update_cart_ajax()
    {
        $qty = $this->input->post('qty', true);
        // $id_customer = $this->input->post('id_customer', true);
        $id_customer = $this->data['user']->id;
        $product_id = decode($this->input->post('product', true));
        $merchant_id = decode($this->input->post('store', true));
        $id_cart = decode($this->input->post('id_cart', true));

        $qty = intval($qty);

        $item = $this->cart_model->check_cart_update($id_cart);
        $check_product = $this->main->get('products', ['id' => $product_id]);
        $check_merchant = $this->main->get('merchants', ['id' => $merchant_id]);

        if ($check_product && $check_merchant) {
            if ($item) {
                $product = $this->product->get_detail_products($product_id, $merchant_id);
                if ($product) {
                    $old_price = intval($item['old_price']);
                    if ($product->length && $product->height && $product->width) {
                        $weight_volume = ($product->length * $product->height * $product->width) / 6;
                        if ($weight_volume > $product->weight) {
                            $product->weight = round($weight_volume);
                            $total_weight = $qty * $product->weight;
                        } else {
                            $total_weight = $qty * $product->weight;
                        }
                    } else {
                        $total_weight = $qty * $product->weight;
                    }
                    if ($item['id_option'] != null) {
                        $options = $this->product->get_option_groups($product_id, $item['id_option']);
                        $option = $options->row();
                        if ($option->price == 0) {
                            $old_price = intval($product->price);
                        } else {
                            $old_price = intval($option->price);
                        }
                    }
                    if ($item['in_promo'] == 1) {
                        $promo_data = json_decode($item['promo_data'], true);
                        if ($promo_data['type'] == 'P') {
                            $disc_value = intval($promo_data['discount']);
                            $disc_price = round(($old_price * $disc_value) / 100);
                        } else {
                            $disc_value = NULL;
                            $disc_price = intval($promo_data['discount']);
                        }
                        $end_price = intval($old_price) - intval($disc_price);
                        $today = date('Y-m-d');
                        $today = date('Y-m-d', strtotime($today));
                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                        if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                            $price = $end_price;
                            $inPromo = true;
                            $promo_type = $promo_data['type'];
                        } else {
                            $price = intval($old_price);
                            $inPromo = false;
                            $promo_type = false;
                        }
                    } else {
                        $price = intval($old_price);
                        $inPromo = false;
                        $promo_type = false;
                        $disc_value = null;
                        $disc_price = null;
                    }
                    $merchant = $this->session->userdata('list_merchant')[0];
                    $price_level = false;
                    $reseller_price = false;
                    $status_reseller = 0;
                    if($item['reseller']) {
                        $reseller_price = $this->product->get_product_reseller($product->product_id, ($item ? 0 + $qty : $qty));
                        if($reseller_price) {
                            if($item['id_option'] != null) {
                                $options = $this->product->get_option_groups($product_id, $item['id_option']);
                                $option = $options->row();
                                $status_reseller = 1;
                                if ($option->price != 0) {
                                    $range_price = $product->price - $reseller_price;
                                    $fixed_reseller_price = $option->price - $range_price;
                                    $old_price = intval($fixed_reseller_price);
                                    if ($product->promo == 1) {
                                        $promo_data = json_decode($item['promo_data'], true);
                                        if ($promo_data['type'] == 'P') {
                                            $disc_value = intval($promo_data['discount']);
                                            $disc_price = round((intval($fixed_reseller_price) * $disc_value) / 100);
                                            $end_price = intval($fixed_reseller_price) - $disc_price;
                                        } else {
                                            $disc_price = intval($promo_data['discount']);
                                            $end_price = intval($fixed_reseller_price) - $disc_price;
                                        }
                                        $today = date('Y-m-d');
                                        $today = date('Y-m-d', strtotime($today));
                                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                        if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                            $price = $end_price;
                                            $inPromo = true;
                                            $promo_type = $promo_data['type'];
                                            $data_promo_data = $product->promo_data;
                                        } else {
                                            $price = $old_price;
                                            $inPromo = false;
                                            $promo_type = false;
                                            $data_promo_data = null;
                                        }
                                    } else {
                                        $price = $old_price;
                                    }
                                } else {
                                    $old_price = intval($reseller_price);
                                    if ($product->promo == 1) {
                                        $promo_data = json_decode($item['promo_data'], true);
                                        if ($promo_data['type'] == 'P') {
                                            $disc_value = intval($promo_data['discount']);
                                            $disc_price = round((intval($reseller_price) * $disc_value) / 100);
                                            $end_price = intval($reseller_price) - $disc_price;
                                        } else {
                                            $disc_price = intval($promo_data['discount']);
                                            $end_price = intval($reseller_price) - $disc_price;
                                        }
                                        $today = date('Y-m-d');
                                        $today = date('Y-m-d', strtotime($today));
                                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                        if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                            $price = $end_price;
                                            $inPromo = true;
                                            $promo_type = $promo_data['type'];
                                            $data_promo_data = $product->promo_data;
                                        } else {
                                            $price = $old_price;
                                            $inPromo = false;
                                            $promo_type = false;
                                            $data_promo_data = null;
                                        }
                                    } else {
                                        $price = $old_price;
                                    }
                                }
                            } else {
                                $old_price = intval($reseller_price);
                                $status_reseller = 1;
                                if ($product->promo == 1) {
                                    $promo_data = json_decode($item['promo_data'], true);
                                    if ($promo_data['type'] == 'P') {
                                        $disc_value = intval($promo_data['discount']);
                                        $disc_price = round((intval($reseller_price) * $disc_value) / 100);
                                        $end_price = intval($reseller_price) - $disc_price;
                                    } else {
                                        $disc_price = intval($promo_data['discount']);
                                        $end_price = intval($reseller_price) - $disc_price;
                                    }
                                    $today = date('Y-m-d');
                                    $today = date('Y-m-d', strtotime($today));
                                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                    if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                        $price = $end_price;
                                        $inPromo = true;
                                        $promo_type = $promo_data['type'];
                                        $data_promo_data = $product->promo_data;
                                    } else {
                                        $price = $old_price;
                                        $inPromo = false;
                                        $promo_type = false;
                                        $data_promo_data = null;
                                    }
                                } else {
                                    $price = $old_price;
                                }
                            }
                        } else {
                            $price_level = $this->product->product_price_in_level($product->product_id, ($item ? 0 + $qty : $qty));
                            if ($price_level) {
                                $old_price = intval($price_level);
                                if ($product->promo == 1) {
                                    $promo_data = json_decode($item['promo_data'], true);
                                    if ($promo_data['type'] == 'P') {
                                        $disc_value = intval($promo_data['discount']);
                                        $disc_price = round((intval($price_level) * $disc_value) / 100);
                                        $end_price = intval($price_level) - $disc_price;
                                    } else {
                                        $disc_price = intval($promo_data['discount']);
                                        $end_price = intval($price_level) - $disc_price;
                                    }
                                    $today = date('Y-m-d');
                                    $today = date('Y-m-d', strtotime($today));
                                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                    if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                        $price = $end_price;
                                        $inPromo = true;
                                        $promo_type = $promo_data['type'];
                                        $data_promo_data = $product->promo_data;
                                    } else {
                                        $price = $old_price;
                                        $inPromo = false;
                                        $promo_type = false;
                                        $data_promo_data = null;
                                    }
                                } else {
                                    $price = $old_price;
                                }
                                $price_level = 1;
                            } else {
                                $old_price = $product->price;
                                if ($item['in_promo'] == 1) {
                                    $promo_data = json_decode($item['promo_data'], true);
                                    if ($promo_data['type'] == 'P') {
                                        $disc_value = intval($promo_data['discount']);
                                        $disc_price = round(($old_price * $disc_value) / 100);
                                    } else {
                                        $disc_value = NULL;
                                        $disc_price = intval($promo_data['discount']);
                                    }
                                    $end_price = intval($old_price) - intval($disc_price);
                                    $today = date('Y-m-d');
                                    $today = date('Y-m-d', strtotime($today));
                                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                    if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                        $price = $end_price;
                                        $inPromo = true;
                                        $promo_type = $promo_data['type'];
                                    } else {
                                        $price = intval($old_price);
                                        $inPromo = false;
                                        $promo_type = false;
                                    }
                                } else {
                                    $price = intval($old_price);
                                    $inPromo = false;
                                    $promo_type = false;
                                    $disc_value = null;
                                    $disc_price = null;
                                }
                            }
                        }
                    } else {
                        $price_level = $this->product->product_price_in_level($product->product_id, ($item ? 0 + $qty : $qty));
                        if ($price_level) {
                            if ($item['id_option'] != null) {
                                $options = $this->product->get_option_groups($product_id, $item['id_option']);
                                $option = $options->row();
                                if ($option->price != 0) {
                                    $range_price = $product->price - $price_level;
                                    $fixed_price_level = $option->price - $range_price;
                                    $old_price = intval($fixed_price_level);
                                    if ($product->promo == 1) {
                                        $promo_data = json_decode($item['promo_data'], true);
                                        if ($promo_data['type'] == 'P') {
                                            $disc_value = intval($promo_data['discount']);
                                            $disc_price = round((intval($fixed_price_level) * $disc_value) / 100);
                                            $end_price = intval($fixed_price_level) - $disc_price;
                                        } else {
                                            $disc_price = intval($promo_data['discount']);
                                            $end_price = intval($fixed_price_level) - $disc_price;
                                        }
                                        $today = date('Y-m-d');
                                        $today = date('Y-m-d', strtotime($today));
                                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                        if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                            $price = $end_price;
                                            $inPromo = true;
                                            $promo_type = $promo_data['type'];
                                            $data_promo_data = $product->promo_data;
                                        } else {
                                            $price = $old_price;
                                            $inPromo = false;
                                            $promo_type = false;
                                            $data_promo_data = null;
                                        }
                                    } else {
                                        $price = $old_price;
                                    }
                                } else {
                                    $old_price = intval($price_level);
                                    if ($product->promo == 1) {
                                        $promo_data = json_decode($item['promo_data'], true);
                                        if ($promo_data['type'] == 'P') {
                                            $disc_value = intval($promo_data['discount']);
                                            $disc_price = round((intval($price_level) * $disc_value) / 100);
                                            $end_price = intval($price_level) - $disc_price;
                                        } else {
                                            $disc_price = intval($promo_data['discount']);
                                            $end_price = intval($price_level) - $disc_price;
                                        }
                                        $today = date('Y-m-d');
                                        $today = date('Y-m-d', strtotime($today));
                                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                        if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                            $price = $end_price;
                                            $inPromo = true;
                                            $promo_type = $promo_data['type'];
                                            $data_promo_data = $product->promo_data;
                                        } else {
                                            $price = $old_price;
                                            $inPromo = false;
                                            $promo_type = false;
                                            $data_promo_data = null;
                                        }
                                    } else {
                                        $price = $old_price;
                                    }
                                }
                            } else {
                                $old_price = intval($price_level);
                                if ($product->promo == 1) {
                                    $promo_data = json_decode($item['promo_data'], true);
                                    if ($promo_data['type'] == 'P') {
                                        $disc_value = intval($promo_data['discount']);
                                        $disc_price = round((intval($price_level) * $disc_value) / 100);
                                        $end_price = intval($price_level) - $disc_price;
                                    } else {
                                        $disc_price = intval($promo_data['discount']);
                                        $end_price = intval($price_level) - $disc_price;
                                    }
                                    $today = date('Y-m-d');
                                    $today = date('Y-m-d', strtotime($today));
                                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                    if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                        $price = $end_price;
                                        $inPromo = true;
                                        $promo_type = $promo_data['type'];
                                        $data_promo_data = $product->promo_data;
                                    } else {
                                        $price = $old_price;
                                        $inPromo = false;
                                        $promo_type = false;
                                        $data_promo_data = null;
                                    }
                                } else {
                                    $price = $old_price;
                                }
                            }
                            $price_level = 1;
                        } else {
                            if($item['id_option']) {
                                $options = $this->product->get_option_groups($product_id, $item['id_option']);
                                $option = $options->row();
                                if ($option->price != 0) {
                                    $old_price = $option->price;
                                    if ($item['in_promo'] == 1) {
                                        $promo_data = json_decode($item['promo_data'], true);
                                        if ($promo_data['type'] == 'P') {
                                            $disc_value = intval($promo_data['discount']);
                                            $disc_price = round(($old_price * $disc_value) / 100);
                                        } else {
                                            $disc_value = NULL;
                                            $disc_price = intval($promo_data['discount']);
                                        }
                                        $end_price = intval($old_price) - intval($disc_price);
                                        $today = date('Y-m-d');
                                        $today = date('Y-m-d', strtotime($today));
                                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                        if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                            $price = $end_price;
                                            $inPromo = true;
                                            $promo_type = $promo_data['type'];
                                        } else {
                                            $price = intval($old_price);
                                            $inPromo = false;
                                            $promo_type = false;
                                        }
                                    } else {
                                        $price = intval($old_price);
                                        $inPromo = false;
                                        $promo_type = false;
                                        $disc_value = null;
                                        $disc_price = null;
                                    }
                                } else {
                                    $old_price = $product->price;
                                    if ($item['in_promo'] == 1) {
                                        $promo_data = json_decode($item['promo_data'], true);
                                        if ($promo_data['type'] == 'P') {
                                            $disc_value = intval($promo_data['discount']);
                                            $disc_price = round(($old_price * $disc_value) / 100);
                                        } else {
                                            $disc_value = NULL;
                                            $disc_price = intval($promo_data['discount']);
                                        }
                                        $end_price = intval($old_price) - intval($disc_price);
                                        $today = date('Y-m-d');
                                        $today = date('Y-m-d', strtotime($today));
                                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                        if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                            $price = $end_price;
                                            $inPromo = true;
                                            $promo_type = $promo_data['type'];
                                        } else {
                                            $price = intval($old_price);
                                            $inPromo = false;
                                            $promo_type = false;
                                        }
                                    } else {
                                        $price = intval($old_price);
                                        $inPromo = false;
                                        $promo_type = false;
                                        $disc_value = null;
                                        $disc_price = null;
                                    }
                                }
                            } else {
                                $old_price = $product->price;
                                if ($item['in_promo'] == 1) {
                                    $promo_data = json_decode($item['promo_data'], true);
                                    if ($promo_data['type'] == 'P') {
                                        $disc_value = intval($promo_data['discount']);
                                        $disc_price = round(($old_price * $disc_value) / 100);
                                    } else {
                                        $disc_value = NULL;
                                        $disc_price = intval($promo_data['discount']);
                                    }
                                    $end_price = intval($old_price) - intval($disc_price);
                                    $today = date('Y-m-d');
                                    $today = date('Y-m-d', strtotime($today));
                                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                    if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                        $price = $end_price;
                                        $inPromo = true;
                                        $promo_type = $promo_data['type'];
                                    } else {
                                        $price = intval($old_price);
                                        $inPromo = false;
                                        $promo_type = false;
                                    }
                                } else {
                                    $price = intval($old_price);
                                    $inPromo = false;
                                    $promo_type = false;
                                    $disc_value = null;
                                    $disc_price = null;
                                }
                            }
                        }
                    }
                    // $price_level = $this->product->price_in_level($product->product_id, $merchant['group'], ($item ? 0 + $qty : $qty));
                    // else {
                    //     $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
                    //     if ($price_group) {
                    //         if ($item['id_option'] != null) {
                    //             $options = $this->product->get_option_groups($product_id, $item['id_option']);
                    //             $option = $options->row();
                    //             if ($option->price == 0) {
                    //                 $old_price = intval($product->price);
                    //             } else {
                    //                 $old_price = intval($option->price);
                    //             }
                    //         } else {
                    //             $old_price = intval($price_group->price);
                    //             if ($item['in_promo'] == 1) {
                    //                 $promo_data = json_decode($item['promo_data'], true);
                    //                 if ($promo_data['type'] == 'P') {
                    //                     $disc_price_group = round((intval($price_group->price) * intval($promo_data['discount'])) / 100);
                    //                 } else {
                    //                     $disc_price_group = intval($promo_data['discount']);
                    //                 }
                    //                 $end_price_group = intval($price_group->price) - $disc_price_group;

                    //                 if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                    //                     $price = $end_price_group;
                    //                 } else {
                    //                     $price = intval($price_group->price);
                    //                 }
                    //             } else {
                    //                 $price = intval($price_group->price);
                    //             }
                    //         }
                    //     } else {
                    //         $price_level = 0;
                    //     }
                    // }
                    $item['quantity'] = $qty;
                    $item['price'] = $price;
                    $item['total_weight'] = $total_weight;
                    $item['grand_total'] = $price * $item['quantity'];
                    $item['price_level'] = $price_level;
                    $item['in_promo'] = $inPromo;
                    $item['promo_type'] = $promo_type;
                    $item['disc_value'] = $disc_value;
                    $item['disc_price'] = $disc_price;
                    $item['old_price'] = $old_price;
                    // $item['reseller'] = $status_reseller;

                    $this->db->where('id', $item['id']);
                    $this->db->update('cart', $item);

                    $output['items'] = $this->cart_model->get_cart($id_cart);
                    $output['items']->id = encode($id_cart);
                    $output['status'] = 'success';
                    $output['message'] = 'Keranjang Berhasil Diupdate!';

                    $all_cart = $this->cart_model->get_all_cart($id_customer, $this->input->ip_address())->result();

                    $grand_total = 0;
                    $total_qty = 0;
                    foreach ($all_cart as $cart) {
                        $grand_total += $cart->grand_total;
                        $total_qty += $cart->quantity;
                    }

                    $cart_merchant = $this->cart_model->cart_merchant($id_customer, $item['merchant'])->row_array();
                    $cart_by_merchant = $this->cart_model->get_cart_by_merchant($id_customer, $item['merchant'])->result();
                    if ($cart_merchant) {
                        $total_weight_cart_merchant = 0;
                        foreach ($cart_by_merchant as $cart_m) {
                            $total_weight_cart_merchant += $cart_m->total_weight;
                        }
                        $this->db->set('total_weight', $total_weight_cart_merchant);
                        $this->db->where('id_customer', $id_customer);
                        $this->db->where('merchant', $item['merchant']);
                        $this->db->update('cart_merchant');
                    }

                    $output['grand_total'] = $grand_total;
                    $output['total_qty'] = $total_qty;
                } else {
                    $output['status'] = 'error';
                    $output['message'] = 'Barang Kosong!';
                    $output['grand_total'] = null;
                    $output['items'] = null;
                }
            } else {
                $output['status'] = 'error';
                $output['message'] = 'Terjadi Kesalahan!';
                $output['grand_total'] = null;
                $output['items'] = null;
            }
        } else {
            $output = [
                'status' => 'error',
                'message' => 'Terjadi suatu kesalahan!',
                'items' => null,
                'grand_total' => null
            ];
        }
        echo json_encode($output);
    }

    public function delete_cart()
    {
        $id = decode($this->input->post('id', true));
        $id_customer = $this->data['user']->id;
        $ip_address = $this->input->ip_address();
        $id_merchant = decode($this->input->post('id_merchant', true));

        $this->db->where('id', $id);
        $this->db->delete('cart');

        $cart_by_merchant = $this->cart_model->get_cart_by_merchant($id_customer, $id_merchant);

        if ($cart_by_merchant->num_rows() > 0) {
            $all_cart_merchant = $cart_by_merchant->result();
            $total_weight_cart_merchant = 0;
            foreach ($all_cart_merchant as $cart_merchant) {
                $total_weight_cart_merchant += $cart_merchant->total_weight;
            }
            $this->db->set('total_weight', $total_weight_cart_merchant);
            $this->db->where('id_customer', $id_customer);
            $this->db->where('merchant', $id_merchant);
            $this->db->update('cart_merchant');
        } else {
            $this->db->where('merchant', $id_merchant);
            $this->db->where('id_customer', $id_customer);
            $this->db->delete('cart_merchant');
        }

        $all_cart = $this->cart_model->get_all_cart($id_customer, $ip_address)->result();

        $grand_total = 0;
        $total_qty = 0;
        $output['views'] = [];

        foreach ($all_cart as $key => $cart) {
            $product = $this->main->get('products', ['id' => $cart->product_id]);
            $stock_empty = 0;
            if ($product->store_type == 'principal') {
                $product_principal_stock = $this->main->get('products_principal_stock', ['product_id' => $product->id, 'branch_id' => $cart->merchant_id]);
                if ($product_principal_stock->quantity < $cart->quantity) {
                    $stock_empty = 1;
                } else {
                    $stock_empty = 0;
                }
                $product_quantity = $product_principal_stock->quantity;
            } else {
                if ($product->quantity < $cart->quantity) {
                    $stock_empty = 1;
                } else {
                    $stock_empty = 0;
                }
                $product_quantity = $product->quantity;
            }
            $grand_total += $cart->grand_total;
            $total_qty += $cart->quantity;
            $cart->stock_empty = $stock_empty;
            $cart->product_quantity = $product_quantity;
            $output['views'][$key] = $cart;
            $output['views'][$key]->id = encode($cart->id);
            $output['views'][$key]->id_customer = encode($output['views'][$key]->id_customer);
            $output['views'][$key]->input_merchant_id = encode($cart->merchant_id);
            $output['views'][$key]->input_product_id = encode($cart->product_id);
        }

        $output['grand_total'] = $grand_total;
        $output['total_qty'] = $total_qty;
        $output['message'] = 'Keranjang Berhasil Dihapus';

        echo json_encode($output);
    }

    public function add_wishlist($id = '')
    {
        if (!$this->ion_auth->logged_in())
            redirect('member/login?back=' . uri_string());
        $id = decode($id);
        if ($product = $this->main->get('products', array('id' => $id))) {
            $this->main->insert('wishlist', array('customer' => $this->data['user']->id, 'product' => $product->id));
            redirect('member/wishlist');
        }
        show_404();
    }

    public function add_wishlist_ajax()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($this->input->post('id'));
        $store = decode($this->input->post('store'));
        $response =   array();

        if (!$this->ion_auth->logged_in()) {
            $response['message'] = 'Belum Login';
            $response['status'] = 'failed';
            $response['status_code'] = 'login_first';
        } else {
            $check_wishlist = $this->main->get('wishlist', array('customer' => $this->data['user']->id, 'product' => $id, 'branch_id' => $store));
            if ($check_wishlist) {
                $this->main->delete('wishlist', array('customer' => $this->data['user']->id, 'product' => $id, 'branch_id' => $store));
                $response['message'] = 'Berhasil menghapus produk favorit!';
                $response['status'] = 'success';
                $response['status_code'] = 'remove_wishlist_success';
            } else {
                $this->main->insert('wishlist', array('customer' => $this->data['user']->id, 'product' => $id, 'branch_id' => $store));
                $response['message'] = 'Berhasil menambahkan produk favorit!';
                $response['status'] = 'success';
                $response['status_code'] = 'add_wishlist_success';
            }
            // if ($product = $this->main->get('products', array('id' => $id))) {
            //     $this->main->insert('wishlist', array('customer' => $this->data['user']->id, 'product' => $product->id));
            // }
        }

        echo json_encode($response);
    }

    public function submit_review()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('rating', 'Rating', 'trim|required');
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required|min_length[10]');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            do {
                $this->main->insert('product_review', array('product' => $data['product'], 'customer' => $this->data['user']->id, 'title' => $data['title'], 'rating' => $data['rating'], 'description' => $data['description']));
                $return = array('message' => $this->template->alert('success', 'Review telah diterima dan akan di review oleh Administrator.'), 'status' => 'success');
            } while (0);
        } else {
            $return = array('message' => $this->template->alert('danger', validation_errors()), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function submitLikeDislike()
    {
        if ($this->ion_auth->logged_in()) {
            $id = $this->input->post('id', TRUE);
            $type = $this->input->post('type', TRUE);
            if ($likeDislike = $this->main->get('product_review_likedislike', array('product_review' => $id, 'customer' => $this->data['user']->id))) {
                $type = $likeDislike->likedislike;
            } else {
                $this->main->insert('product_review_likedislike', array('product_review' => $id, 'customer' => $this->data['user']->id, 'likedislike' => $type));
                $typeName = ($type == 1) ? "like" : "dislike";
                $this->db->query("UPDATE product_review SET `$typeName`=`$typeName`+1 WHERE id=$id");
            }
            echo ($type == 1) ? lang('product_text_review_like_this') : lang('product_text_review_dislike_this');
        }
    }

    public function get_districts($city = 1, $district_selected = '')
    {
        $key = $this->input->get("q");
        if ($key) {
            $districts = $this->product->get_districts($key);
        }
        echo json_encode($districts);
    }

    public function cek_estimasi_harga()
    {
        $id = $this->input->post('id');
        $qty = $this->input->post('qty');
        $weight = $this->input->post('weight');
        $merchant_ = $this->session->userdata('list_merchant')[0];
        $merchant = $this->main->get('merchants', array('id' => $merchant_['id']));
        $couriers = json_decode($merchant->shipping);
        $address = $this->main->get('customer_address', array('id' => $this->session->userdata('checkout_address'), 'customer' => $this->data['user']->id));
        $results = array();
        $serv = array();
        $harga = array();

        foreach ($this->cart->contents() as $item) {
            if ($item['merchant']) {
                $merchant = $this->main->get('merchants', array('id' => $item['merchant']));
            }
        }
        if ($couriers) {
            foreach ($couriers as $courier) {
                $services = $this->rajaongkir->cost($merchant->district, 'subdistrict', $id, 'subdistrict', ($qty * $weight), $courier);
                if ($services) {
                    $services = json_decode($services);
                    if ($services->rajaongkir->status->code == 200) {
                        foreach ($services->rajaongkir->results as $service) {
                            foreach ($service->costs as $cost) {
                                /*?>
                                <option value="<?php echo $service->code . '-' . $cost->service; ?>" data-id="<?php echo $item['rowid']; ?>" data-value="<?php echo $cost->cost[0]->value; ?>"><?php echo strtoupper($service->code) . ' ' . $cost->service; ?> <span>(<?php echo rupiah($cost->cost[0]->value); ?>)</option>
                                    <?php*/
                                $results[] = $cost;
                                $serv[] = $service;
                                $harga[] = $cost->cost[0]->value;
                            }
                        }
                    }
                }
            }
        }
        //var_dump($results);
        /*$array = array('result' => $results, 'service' => $serv);
        $out = array_values($array);*/
        $termurah = rupiah(min($harga));
        $termahal = rupiah(max($harga));
        $array = array('result' => $results, 'termurah' => $termurah, 'termahal' => $termahal, 'kurir' => $couriers);
        $out = array_values($array);
        echo json_encode($out);
    }

    public function add_wishlist_new()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $data = $this->input->post(null, true);
        $product = decode($data['product']);
        $store = decode($data['store']);
        $customer = decode($data['customer']);

        $data = [
            'product' => $product,
            'branch_id' => $store,
            'customer' => $customer
        ];

        $check = $this->main->get('wishlist', ['product' => $product, 'branch_id' => $store, 'customer' => $customer]);
        if ($check) {
            $this->main->delete('wishlist', $data);
            $total = $this->main->gets('wishlist', ['customer' => $customer]);
            if ($total) {
                $total = $total->num_rows();
            } else {
                $total = 0;
            }
            $result = [
                'status' => 'remove',
                'total' => $total
            ];
        } else {
            $this->main->insert('wishlist', $data);
            $total = $this->main->gets('wishlist', ['customer' => $customer]);
            if ($total) {
                $total = $total->num_rows();
            } else {
                $total = 0;
            }
            $result = [
                'status' => 'add',
                'total' => $total
            ];
        }
        echo json_encode($result);
    }

    public function check_user_merchant() {
        $this->input->is_ajax_request() or exit(redirect('error_403'));
        $return = [];
        if($this->ion_auth->logged_in()) {
            $customer = $this->main->get('customers', ['id' => $this->data['user']->id, 'type' => 'm']);
            if($customer) {
                $merchant = $this->main->get('merchant_users', ['phone' => $customer->phone]);
                $return['status'] = 'login';
                $return['callback'] = encode($merchant->id);
            } else {
                $return['status'] = 'not_login';
            }
        } else {
            $return['status'] = 'not_login';
        }
        echo json_encode($return);
    }
}
