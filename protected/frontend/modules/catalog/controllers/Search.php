<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Search extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->lang->load('catalog', settings('language'));
        $this->load->model('search_model', 'search');
        $this->load->model('products');
        $this->load->library('pagination');
        $this->load->library('breadcrumb');
    }

    // public function view() {
    //     $word = $this->input->get('word');

    //     if ($this->session->has_userdata('list_merchant')) {
    //         $merchants = $this->session->userdata('list_merchant');
    //         $this->data['merchant_group'] = $merchants[0]['group'];
    //         foreach ($merchants as $m) {
    //            $list[] = $m['id'];
    //         }
    //     } else {
    //         $merchants = $this->db->where('group',13)->where('type','merchant')->get('merchants')->result();
    //         $this->data['merchant_group'] = 13;
    //         foreach ($merchants as $m) {
    //            $list[] = $m->id;
    //         }
    //     }
    //     $merchant_group =  $this->data['merchant_group'];

    //     //pagination
    //     $config['base_url'] = site_url('search');
    //     $config['total_rows'] = $this->search->getProducts($word, $list, $merchant_group, '', '', '')->num_rows();
    //     $config['per_page'] = ($this->input->get('show')) ? $this->input->get('show') : 20;
    //     $this->pagination->initialize($config);
    //     $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

    //     $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
    //     $this->breadcrumb->add('Cari "' . $word . '"', site_url('search'));
    //     $this->data['products'] = $this->search->getProducts($word, $list, $merchant_group, (($this->input->get('sort')) ? $this->input->get('sort') : 'popular'), $config['per_page'], $start);
    //     $this->data['pagination'] = $this->pagination->create_links();
    //     $this->data['breadcrumb'] = $this->breadcrumb->output();
    //     $this->data['meta_description'] = 'Cari produk ' . $word . ' harga termurah.';
    //     $this->data['total_products'] = $config['total_rows'];
    //     $this->data['start'] = $start + 1;
    //     $this->data['to'] = ($this->data['total_products'] < ($this->data['start'] * $config['per_page'])) ? $this->data['total_products'] : ($this->data['start'] * $config['per_page']);
    //     $this->output->set_title('Pencarian produk ' . $word . ' - ' . settings('meta_title'));
    //     $this->template->_init();
    //     $this->load->view('search', $this->data);
    // }

    public function view()
    {
        $word = $this->input->get('word', true);

        $word = htmlspecialchars($word);

        //pagination
        $config['base_url'] = site_url('search');
        $config['total_rows'] = $this->search->get_products($word, '', '')->num_rows();
        $config['per_page'] = ($this->input->get('show')) ? $this->input->get('show') : 20;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Cari "' . $word . '"', site_url('search'));
        $this->data['products'] = $this->search->get_products($word, (($this->input->get('sort')) ? $this->input->get('sort') : 'distance'), $config['per_page'], $start);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['meta_description'] = 'Cari produk ' . $word . ' harga termurah.';
        $this->data['total_products'] = $config['total_rows'];
        $this->data['start'] = $start + 1;
        $this->data['to'] = ($this->data['total_products'] < ($this->data['start'] * $config['per_page'])) ? $this->data['total_products'] : ($this->data['start'] * $config['per_page']);
        $this->output->set_title('Pencarian produk ' . $word . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->js(site_url('assets/frontend/js/modules/search.js'));
        $this->load->view('search', $this->data);
    }

    // public function suggest()
    // {
    //     $keyword = $this->input->get('keyword');

    //     $this->db->select('products.name,
    //                         products_principal_stock.product_id,
    //                         products_principal_stock.branch_id AS store_id,');
    //     $this->db->where("products.status", 1);
    //     $this->db->where("products_principal_stock.quantity > ", 0);
    //         $this->db->group_start();
    //         $this->db->where("products.name LIKE '%$keyword%'");
    //         $this->db->or_where("products.meta_keyword REGEXP '$keyword'");
    //         $this->db->or_where("categories.name LIKE '%$keyword%'");
    //         $this->db->group_end();
    //     $this->db->limit(5);
    //     $this->db->join('products', 'products_principal_stock.product_id = products.id', 'LEFT');
    //     $this->db->join('categories', 'categories.id = products.category', 'LEFT');
    //     $this->db->order_by('products.viewed', 'DESC');

    //     $search = $this->db->get('products_principal_stock')->result();

    // $arr = [];
    // foreach ($search as $key => $value) {
    //     $arr[] = [
    //         'url' => seo_url('catalog/products/view/' . $value->product_id . '/' . $value->store_id),
    //         'name' => $value->name
    //     ];
    // }

    // echo json_encode($arr);
    // }

    // NEW METHOD
    public function suggest()
    {
        $keyword = $this->input->get('keyword');

        $search = $this->search->get_suggest($keyword);

        $arr = [];
        // foreach ($search->result() as $key => $value) {
        //     $arr[] = [
        //         'url' => seo_url('catalog/products/view/' . $value->product_id . '/' . $value->store_id),
        //         'name' => $value->name
        //     ];
        // }
        foreach ($search->result() as $key => $value) {
            $arr[] = [
                'url' => seo_url('search?word=' . $value->name),
                'name' => $value->name
            ];
        }

        if ($search->num_rows() > 0) {
            $result = $arr;
        } else {
            $result = [
                [
                    'url' => '#',
                    'name' => 'Produk tidak ada'
                ]
            ];
        }

        echo json_encode($result);
    }
}
