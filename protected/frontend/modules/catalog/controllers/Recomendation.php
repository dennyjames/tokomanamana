<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recomendation extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('catalog', settings('language'));
        $this->load->model('recomendation_model', 'recomend');
        $this->load->library('pagination');
        $this->load->library('breadcrumb');
        $this->load->library('user_agent');
    }

    // public function index() {
    //     $config['base_url'] = site_url('recomendation');

    //     /* setting for last seen product section */
    //     if($this->ion_auth->logged_in()) {
    //         $auth_id = $this->data['user']->id;
    //         $keyword = $this->recomend->get_recomended_keyword($auth_id, FALSE);

    //         foreach ($keyword->result() as $k) {
    //             $array[] = $k->keyword;
    //         }

    //         $config['total_rows'] = $this->recomend->get_recomendations(implode('|', $array))->num_rows();
    //     }
    //     else {
    //         $auth_id = $this->input->ip_address();
    //         $keyword = $this->recomend->get_recomended_keyword(FALSE, $auth_id);
    //         $config['total_rows'] = $this->recomend->get_all_products()->num_rows();
    //     }

    //     $config['per_page'] = ($this->input->get('show')) ? $this->input->get('show') : 12;
    //     $this->pagination->initialize($config);
    //     $start = $this->input->get('page') ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

    //     $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
    //     $this->breadcrumb->add('Rekomendasi untuk Anda', site_url('recomendation'));

    //     if($keyword) {
    //         if ($keyword->num_rows() > 0) {
    //             foreach ($keyword->result() as $k) {
    //                 $array[] = $k->keyword;
    //             }
    
    //             $products = $this->recomend->get_recomendations(implode('|', $array), (($this->input->get('sort')) ? ($this->input->get('sort')) : NULL), $config['per_page'], $start);
    //         }
    //         else {
    //             $products = $this->recomend->get_all_products((($this->input->get('sort')) ? $this->input->get('sort') : NULL), $config['per_page'], $start);
    //         }
    //     } else {
    //         $products = $this->recomend->get_all_products((($this->input->get('sort')) ? $this->input->get('sort') : NULL), $config['per_page'], $start);
    //     }

    //     // var_dump($this->db->last_query()); die;


    //     //pagination
    //     $this->data['pagination'] = $this->pagination->create_links();
    //     $this->data['breadcrumb'] = $this->breadcrumb->output();
    //     $this->data['meta_description'] = 'Produk Rekomendasi dari Tokomanamana.com';
    //     $this->data['products'] = $products;
    //     $this->data['total_products'] = $config['total_rows'];
    //     $this->data['start'] = $start + 1;
    //     $this->data['to'] = ($this->data['total_products'] < ($this->data['start'] * $config['per_page'])) ? $this->data['total_products'] : ($this->data['start'] * $config['per_page']);
    //     $this->output->set_title('Produk Rekomendasi - ' . settings('meta_title'));
    //     $this->template->_init();

    //     $this->load->view('recomendation', $this->data);
    // }

    // NEW METHOD
    public function index() {
        $config['base_url'] = site_url('recomendation');

        /* setting for last seen product section */
        if($this->ion_auth->logged_in()) {
            $auth_id = $this->data['user']->id;
            $keyword = $this->recomend->get_recomended_keyword($auth_id, FALSE);

            if($keyword) {
                foreach ($keyword->result() as $k) {
                    $array[] = $k->keyword;
                }
    
                $recomend_rows = $this->recomend->get_recomendations(implode('|', $array));
                if($recomend_rows) {
                    if($recomend_rows->num_rows() < 18) {
                        $config['total_rows'] = $this->recomend->get_all_products()->num_rows();
                    } else {
                        $config['total_rows'] = $recomend_rows->num_rows();
                    }
                } else {
                    $config['total_rows'] = $this->recomend->get_all_products()->num_rows();
                }
            } else {
                $config['total_rows'] = $this->recomend->get_all_products()->num_rows();
            }
        }
        else {
            $auth_id = $this->input->ip_address();
            $keyword = $this->recomend->get_recomended_keyword(FALSE, $auth_id);
            $config['total_rows'] = $this->recomend->get_all_products()->num_rows();
        }

        $config['per_page'] = ($this->input->get('show')) ? $this->input->get('show') : 12;
        $this->pagination->initialize($config);
        $start = $this->input->get('page') ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Rekomendasi untuk Anda', site_url('recomendation'));

        if($keyword) {
            if ($keyword->num_rows() > 0) {
                foreach ($keyword->result() as $k) {
                    $array[] = $k->keyword;
                }
                $products_recommendations = $this->recomend->get_recomendations(implode('|', $array));
                if($products_recommendations) {
                    if($products_recommendations->num_rows() < 18) {
                        $products = $this->recomend->get_all_products((($this->input->get('sort')) ? $this->input->get('sort') : NULL), $config['per_page'], $start);
                    } else {
                        $products = $this->recomend->get_recomendations(implode('|', $array), (($this->input->get('sort')) ? ($this->input->get('sort')) : NULL), $config['per_page'], $start);
                    }
                } else {
                    $products = $this->recomend->get_all_products((($this->input->get('sort')) ? $this->input->get('sort') : NULL), $config['per_page'], $start);
                }
            }
            else {
                $products = $this->recomend->get_all_products((($this->input->get('sort')) ? $this->input->get('sort') : NULL), $config['per_page'], $start);
            }
        } else {
            $products = $this->recomend->get_all_products((($this->input->get('sort')) ? $this->input->get('sort') : NULL), $config['per_page'], $start);
        }

        // var_dump($this->db->last_query()); die;


        //pagination
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['meta_description'] = 'Produk Rekomendasi dari Tokomanamana.com';
        $this->data['products'] = $products;
        $this->data['total_products'] = $config['total_rows'];
        $this->data['start'] = $start + 1;
        $this->data['to'] = ($this->data['total_products'] < ($this->data['start'] * $config['per_page'])) ? $this->data['total_products'] : ($this->data['start'] * $config['per_page']);
        $this->output->set_title('Produk Rekomendasi - ' . settings('meta_title'));
        $this->template->_init();

        $this->load->view('recomendation', $this->data);
    }

    public function load_more_recomendation_product() {
        $last_view = $this->input->post('viewed');
        $last_key = $this->input->post('last_key');
        $per_page = 12;

        $conditions = [];

        $conditions['where'] = "products.date_modified < '$last_view'";
        $conditions['limit'] = $per_page;

        if($this->ion_auth->logged_in()) {
            $auth_id = $this->data['user']->id;
            $keyword = $this->recomend->get_recomended_keyword($auth_id, FALSE);

            if($keyword) {
                foreach ($keyword->result() as $k) {
                    $array[] = $k->keyword;
                }
            }
        }
        else {
            $auth_id = $this->input->ip_address();
            $keyword = $this->recomend->get_recomended_keyword(FALSE, $auth_id);
        }

        if($keyword) {
            if ($keyword->num_rows() > 0) {
                foreach ($keyword->result() as $k) {
                    $array[] = $k->keyword;
                }
                $products_recommendations = $this->recomend->get_recomendations(implode('|', $array));
                if($products_recommendations) {
                    if($products_recommendations->num_rows() < 18) {
                        $conditions['return_type'] = 'count';
                        $products_num = $this->recomend->get_more_all_product($conditions);
        
                        $conditions['return_type'] = '';
                        $products = $this->recomend->get_more_all_product($conditions);
                    } else {
                        $conditions['return_type'] = 'count';
                        $products_num = $this->recomend->get_more_recomendations(implode('|', $array), $conditions);
        
                        $conditions['return_type'] = '';
                        $products = $this->recomend->get_more_recomendations(implode('|', $array), $conditions);
                    }
                } else {
                    $conditions['return_type'] = 'count';
                    $products_num = $this->recomend->get_more_all_product($conditions);
    
                    $conditions['return_type'] = '';
                    $products = $this->recomend->get_more_all_product($conditions);
                }
            }
            else {
                $conditions['return_type'] = 'count';
                $products_num = $this->recomend->get_more_all_product($conditions);

                $conditions['return_type'] = '';
                $products = $this->recomend->get_more_all_product($conditions);
            }
        } else {
            $conditions['return_type'] = 'count';
            $products_num = $this->recomend->get_more_all_product($conditions);

            $conditions['return_type'] = '';
            $products = $this->recomend->get_more_all_product($conditions);
        }


        $this->data['products'] = $products;
        $this->data['products_num'] = $products_num;
        $this->data['products_limit'] = $per_page;
        $this->data['key'] = $last_key;

        $this->load->view('more_recommendations', $this->data, false);
    }
}
