<?php if($this->agent->is_mobile()) {
    $width_lds = '46%';
} else {
    $width_lds = '48%';
} ?>
<style>
.lds-ring{left:<?php echo $width_lds ?>;display:inline-block;position:relative;width:64px;height:64px;display:none;margin-bottom:25px}.lds-ring div{box-sizing:border-box;display:block;position:absolute;width:51px;height:51px;margin:6px;border-radius:50%;animation:lds-ring .8s cubic-bezier(.5,0,.5,1) infinite;border-color:#a7c22a transparent transparent transparent!important}.lds-ring div:nth-child(1){animation-delay:0s}.lds-ring div:nth-child(2){animation-delay:-80ms}.lds-ring div:nth-child(3){animation-delay:-.1s}@keyframes lds-ring{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}.slick-next:before,.slick-prev:before{color:#97c23c}.loader{border:6px solid #f3f3f3;border-radius:50%;border-top:6px solid #a6dd69;width:50px;height:50px;margin:20px;-webkit-animation:spin 2s linear infinite;animation:spin 2s linear infinite}@-webkit-keyframes spin{0%{-webkit-transform:rotate(0)}100%{-webkit-transform:rotate(360deg)}}@keyframes spin{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}.spinner{margin:100px auto 0;width:70px;text-align:center;padding-bottom:20px}.spinner>div{width:18px;height:18px;background-color:#a6dd69;border-radius:100%;display:inline-block;-webkit-animation:sk-bouncedelay 1.4s infinite ease-in-out both;animation:sk-bouncedelay 1.4s infinite ease-in-out both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes sk-bouncedelay{0%,100%,80%{-webkit-transform:scale(0)}40%{-webkit-transform:scale(1)}}@keyframes sk-bouncedelay{0%,100%,80%{-webkit-transform:scale(0);transform:scale(0)}40%{-webkit-transform:scale(1);transform:scale(1)}}.enter{margin-top:25px;margin-bottom:25px;overflow:auto;width:100%}.title-segment{font-size:18px;color:#a7c22a;font-weight:700;margin-bottom:20px;padding:0 7px}.box-brand{border:1px solid #e8e8e8;box-shadow:0 0 7px -3px rgba(0,0,0,.3);margin-right:30px;padding:0 10px}.box-product{box-shadow:0 1px 6px 0 rgba(49,53,59,.12);border:1px solid #a7c22a;border-radius:3px;height:270px;margin-top:25px}.btn-default{background:#a7c22a;border-radius:4px;color:#fff;border:none;text-transform:capitalize;box-shadow:0 1px 6px 0 rgba(49,53,59,.12)}.btn-default:hover{background:#81961e}.btn-white{background:#fff;border-radius:4px;color:#999;border:#ccc;text-transform:capitalize;box-shadow:0 1px 6px 0 rgba(49,53,59,.12)}.btn-white:hover{background:#ccc}body{background-color:#fff}.product-discount-label{position:absolute;top:8px;right:10px;z-index:1;background-color:#d9534f;color:#fff;padding:5px 10px;border-radius:5px}.lds-ring{display:inline-block;position:relative;height:100%;padding:9px 2px}.lds-ring div{box-sizing:border-box;display:inline-block;position:absolute;width:20px;height:20px;border:3px solid #000;border-radius:50%;animation:lds-ring 1.2s cubic-bezier(.5,0,.5,1) infinite;border-color:#000 transparent transparent transparent}.lds-ring div:nth-child(1){animation-delay:-.45s}.lds-ring div:nth-child(2){animation-delay:-.3s}.lds-ring div:nth-child(3){animation-delay:-.15s}@keyframes lds-ring{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}@media (max-width:991px) and (min-width:585px){.collection-items .products .product-item{width:33.333333%!important}}@media (max-width:320px){.img-responsive{width:70%;margin:0 auto}.product-item{width:100%}}@media (max-width:375px){.title-segment{margin-left:-30px}.product{min-height:355px!important}.tombol-cart{top:48%!important}.title-segment{font-size:15px!important}.lihat-semua-object a{font-size:13px!important}}@media (max-width:425px) and (min-width:376px){.title-segment{margin-left:-30px}.tombol-cart{top:48%!important}.lihat-semua-object{margin-right:-25px!important}}@media (max-width:425px){.group-home-slideshow{margin-top:-45px!important}.lihat-semua-object{margin-right:-20px}}@media (max-width:1024px) and (min-width:426px){.home-slideshow-inner{width:100%!important}}@media (max-width:767px) and (min-width:426px){.tombol-cart{top:46%!important}.product{min-height:369px!important}}@media (max-width:767px) and (min-width:601px){section.home_blog_layout .home_blog_content>div{padding-left:5px!important;padding-right:5px!important}}@media (max-width:600px){section.home_blog_layout .home_blog_content>div{padding-left:0!important;padding-right:0!important}}@media (max-width:1024px) and (min-width:768px){.tombol-cart{top:50%!important}.product{min-height:369px!important}}@media (max-width:1199px) and (min-width:1025px){.product{min-height:369px!important}}.garis-tengah{width:100%;height:1px;background-color:#e8e8e8;display:block;margin-top:10px}.tombol-cart{z-index:1;background-color:#fff;position:absolute;top:48%;right:5%;padding:0;display:inline-block;border:1px solid rgba(167,194,42,.3);height:40px;border-radius:50%;width:40px;box-shadow:0 0 7px 1px rgba(167,194,42,.3);-webkit-transition:all .5s;-moz-transition:all .5s;-ms-transition:all .5s;-o-transition:all .5s;transition:all .5s}.tombol-cart button{background:0 0;border:none;line-height:0;padding:0 7px;outline:0}.tombol-cart:hover .icon-cart i{color:#fff}.tombol-cart button-cart:active,.tombol-cart button-cart:hover,.tombol-cart button:active,.tombol-cart button:hover{border:none;background:0 0;outline:0}.tombol-cart a{padding:12px 7px;line-height:37px}.tombol-cart:hover{background-color:#a7c22a;box-shadow:0 0 7px 1px rgba(167,194,42,.3)}.product-item{border:none}.product{margin-left:7px;margin-right:7px;margin-bottom:15px!important;border:1px solid #e8e8e8;box-shadow:0 0 7px -3px rgba(0,0,0,.3);min-height:355px;border-radius:5px}.pre-order-label{color:#666;bottom:0;display:inline-block;margin-top:3px;position:relative;z-index:1;padding:2px 10px;background-color:#eee;font-weight:700;border-radius:2px}.lihat-semua-object{padding:2px 5px 2px 5px;border-radius:10px;transition:all .5s ease-in-out;-webkit-transition:all .5s ease-in-out;-moz-transition:all .5s ease-in-out;-o-transition:all .5 ease-in-out}.hover-lihat-semua a{transition:all .3s;-webkit-transition:all .3s;-moz-transition:all .3s;-o-transition:all .3s}.hover-lihat-semua:hover a{color:#a7c22a}.slick-dots{bottom:1px}.slick-next,.slick-prev{transition:all .4s ease-in-out;box-shadow:0 1px 12px 0 rgba(0,0,0,.12);border-radius:100%;z-index:3}.responsive-desktop .slick-prev{left:12px}.responsive-desktop .slick-next{right:12px}.responsive-brand .slick-prev{left:-46px}.responsive-brand .slick-next{right:-46px}.slick-next:before,.slick-prev:before{color:#a6a6a6}.slick-next:hover,.slick-prev:hover{transform:scale(2);transform-origin:bottom}.slick-slide{position:relative}#mobile-slider .slick-slide{margin:0 27px}#mobile-slider .slick-list{margin:0 -27px}#desktop-slider .slick-slide{margin:0 27px}#desktop-slider .slick-list{margin:0 -27px}.responsive-brand .slick-slide{margin:0 27px}.responsive-brand .slick-list{margin:0 -27px}.slick-dots li.slick-active button:before{color:#a7c22a!important;font-size:10px;line-height:10px}.slick-dots li button:before{font-size:10px;line-height:10px}.responsive-mobile{margin-top:30px;margin-bottom:-10px}.responsive-desktop{margin:10px 7px}.responsive-brand{margin:0 50px}#banner-home-web{margin-top:50px}.slick-dotted.slick-slider{margin-bottom:0!important}.main-slideshow{margin-top:30px}@media(max-width:425px){.responsive-brand .slick-list{margin:0 -50px!important}}@media (min-width:480px) and (max-width:649px){.responsive-brand .slick-list{margin:0 -70px!important}.responsive-brand .slick-slide{margin:0 15px!important}}@media (max-width:767px) and (min-width:426px){.lihat-semua-object{margin-right:-25px!important}.title-segment{margin-left:-30px}}@media (max-width:360px) and (min-width:349px){.lihat-semua-object{margin-right:-24px!important}}@media (max-width:348px) and (min-width:331px){.lihat-semua-object{margin-right:-30px!important}}@media (max-width:330px){.lihat-semua-object{margin-right:-34px!important}}.swal2-icon.swal2-info{color:#a7c22a;border-color:#a7c22a}.big-title-segment{font-size:18px;color:#a7c22a;font-weight:700;margin-bottom:20px}.free-ongkir-label{color:#fff;bottom:0;display:inline-block;margin-top:3px;margin-left:1px;margin-right:1px;position:relative;z-index:1;padding:2px 5px;background-color:#a7c22a;font-weight:700;border-radius:2px}.btn-add-to-wishlist{position:absolute;z-index:1;top:6px;left:10px;font-size:18px;transition:.5s}.btn-add-to-wishlist:hover{color:#d9534f!important}.money{font-weight:600}@media (max-width:382px) and (min-width:321px){.pre-order-label{font-size:10px;padding:2px 5px}.free-ongkir-label{font-size:10px;padding:2px 3px}}
</style>
<section class="collection-heading heading-content ">
    <div class="container">
        <div class="row" style="padding-left: 10px; padding-right: 10px;">
            <div class="collection-wrapper" style="margin-top: 20px;">
                <h1 class="collection-title">
                    <span class="big-title-segment">Semua Produk Rekomendasi</span>
                </h1>
                <?php echo $breadcrumb; ?>
            </div>
        </div>
    </div>
</section>

<section class="collection-content" style="margin-bottom:<?php echo ($this->agent->is_mobile()) ? '70px;' : '0px;' ; ?>">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div id="shopify-section-collection-template" class="shopify-section">
                    <div class="collection-inner">
                        <div id="tags-load" style="display:none;">
                            <i class="fa fa-spinner fa-pulse fa-2x"></i>
                        </div>
                        <div id="collection">
                            <div class="collection_inner">
                                <div class="collection-mainarea col-sm-12 clearfix">
                                    <?php if ($products->num_rows() > 0) { ?>
                                        <div class="collection_toolbar">
                                            <!-- <div class="toolbar_left">
                                                Item <?php //echo number($start); ?> s/d <?php //echo number($to); ?> dari <?php //echo number($total_products); ?> Total Item
                                            </div> -->

                                            <!-- <div class="toolbar_right">
                                                <div class="group_toolbar">
                                                    <form id="form-filter">
                                                        <div class="sortBy">
                                                            <span class="toolbar_title">Urut Berdasarkan:</span>
                                                            <div class="control-container">
                                                                <select name="sort" class="select-bootstrap" tabindex="-1" onchange="$('#form-filter').submit();">
                                                                    <option value="random" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'random') ? 'selected' : ''; ?>>Acak</option>
                                                                    <option value="popular" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'popular') ? 'selected' : ''; ?>>Terpopuler</option>
                                                                    <option value="nameAZ" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'nameAZ') ? 'selected' : ''; ?>>Nama (A-Z)</option>
                                                                    <option value="nameZA" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'nameZA') ? 'selected' : ''; ?>>Nama (Z-A)</option>
                                                                    <option value="lowprice" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'lowprice') ? 'selected' : ''; ?>>Harga Termurah</option>
                                                                    <option value="highprice" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'highprice') ? 'selected' : ''; ?>>Harga Termahal</option>
                                                                    <option value="newest" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'newest') ? 'selected' : ''; ?>>Terbaru</option>
                                                                </select>
                                                            </div>

                                                            <span class="toolbar_title">Tampilkan:</span>
                                                            <div class="control-container">
                                                                <select name="show" tabindex="-1" onchange="$('#form-filter').submit();">
                                                                    <option value="20" <?php //echo ($this->input->get('show') && $this->input->get('show') == '20') ? 'selected' : ''; ?>>20</option>
                                                                    <option value="40" <?php //echo ($this->input->get('show') && $this->input->get('show') == '40') ? 'selected' : ''; ?>>40</option>
                                                                    <option value="80" <?php //echo ($this->input->get('show') && $this->input->get('show') == '80') ? 'selected' : ''; ?>>80</option>
                                                                </select>
                                                            </div>
                                                            <span class="toolbar_title">Item Per Halaman</span>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div> -->
                                        </div>

                                        <div class="collection-items clearfix" style="border:none;">
                                            <div class="products" id="product_recommended" style="border:none;">
                                                <?php
                                                    foreach ($products->result() as $key => $product) {

                                                        if ($product->merchant == 0) {
                                                            $merchant = $this->session->userdata('list_merchant')[0];
                                                            $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
                                                            if ($price_group) {
                                                                $product->price = $price_group->price;
                                                            }
                                                        }

                                                        if ($this->ion_auth->logged_in()) {
                                                            $id_customer = $this->data['user']->id;
                                                            $get_wishlist = $this->main->get('wishlist', ['product' => $product->product_id, 'branch_id' => $product->store_id, 'customer' => $id_customer]);
                                                            if ($get_wishlist) {
                                                                $product_wishlist = true;
                                                            } else {
                                                                $product_wishlist = false;
                                                            }
                                                        } else {
                                                            $product_wishlist = false;
                                                        }

                                                        if($product->variation) {
                                                            $product_variation = $this->main->get('product_option', ['product' => $product->product_id, 'default' => 1]);
                                                            if($product->price_grosir) {
                                                                if($product_variation) {
                                                                    $product->price = $product->price;
                                                                    $product->quantity = $product_variation->quantity;
                                                                } else {
                                                                    $product->price = $product->price;
                                                                    $product->quantity = $product->quantity;
                                                                }
                                                            } else {
                                                                if($product_variation) {
                                                                    $product->price = $product_variation->price;
                                                                    $product->quantity = $product_variation->quantity;
                                                                } else {
                                                                    $product->price = $product->price;
                                                                    $product->quantity = $product->quantity;
                                                                }
                                                            }
                                                        }

                                                        $sale_on = false;

                                                        if($product->promo) {
                                                            $promo_data = json_decode($product->promo_data,true);
                                                        
                                                            if($promo_data['type'] == 'P') {
                                                                $disc_price = round(($product->price * $promo_data['discount']) / 100);
                                                                $label_disc = $promo_data['discount'].'% off';
                                                            } else {
                                                                $disc_price = $promo_data['discount'];
                                                                $label_disc = 'SALE';
                                                            }
                                                            
                                                            $end_price = intval($product->price) - intval($disc_price);
                                                            $today = date('Y-m-d');
                                                            $today=date('Y-m-d', strtotime($today));
                                                            $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                            $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                                            if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                                                $sale_on = true;
                                                            } else {
                                                                $sale_on = false;
                                                            }
                                                        }

                                                        
                                                ?>
                                                        <div class="product-item col-sm-2" style="padding: 0px; border:none;">
                                                            <div class="product" onmouseover="change_store(<?= $key ?>, 'over')" onmouseout="change_store(<?= $key ?>, 'out')">

                                                                <?php if ($sale_on){ ?>
                                                                    <span class="product-discount-label"><?php echo $label_disc;?></span>
                                                                <?php
                                                                    }
                                                                ?>

                                                                <?php if ($this->ion_auth->logged_in()) : ?>
                                                                    <a href="#" class="btn-add-to-wishlist" style="color:<?= ($product_wishlist) ? '#d9534f;' : '#BBB'  ?>" onclick="add_wishlist(this)" data-product="<?= encode($product->product_id) ?>" data-store="<?= encode($product->store_id) ?>" data-customer="<?= encode($id_customer) ?>">
                                                                        <i class="fa fa-heart" aria-hidden="true"></i>
                                                                    </a>
                                                                <?php else : ?>
                                                                    <a href="#" class="btn-add-to-wishlist" style="color:#BBB" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                                                        <i class="fa fa-heart" aria-hidden="true"></i>
                                                                    </a>
                                                                <?php endif; ?>

                                                                <div class="row-left">
                                                                    <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="hoverBorder container_item" style="min-height: 180px;">
                                                                        <img src="<?php echo ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" class="img-responsive" alt="<?php echo $product->name; ?>">
                                                                    </a>
                                                                </div>

                                                                <div class="garis-tengah"></div>
                                                                <div class="tombol-cart">
                                                                <?php if($this->ion_auth->logged_in()) : ?>
                                                                <?php if ($product->variation) : ?>
                                                                    <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="icon-cart">
                                                                    <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                    </a>
                                                                <?php else : ?>
                                                                    <button data-id="<?php echo encode($product->product_id); ?>" data-storeid="<?= encode($product->store_id) ?>" onclick="addtoCart($(this).data('id'), $(this).data('storeid'))" class="icon-cart">
                                                                        <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                    </button>
                                                                <?php endif; ?>
                                                                <?php else : ?>
                                                                    <button type="button" class="icon-cart btn-cart-not-login" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                                                        <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                    </button>
                                                                    <!-- <a href="<?php echo site_url('/?back=' . $this->input->get('back')) ?>" class="icon-cart" id="btn-cart-not-login" data-toggle="modal" data-target="#modal-guest">
                                                                        <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                    </a> -->
                                                                <?php endif; ?>
                                                                </div>

                                                                <div class="row-right animMix">
                                                                    <div class="grid-mode">
                                                                        <div class="product-title">
                                                                            <a class="title-5" style="font-weight: bold; width: 75%;transition:.3s;" href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>"><?php echo $product->name; ?></a>
                                                                        </div>

                                                                        <div class="product-price">
                                                                            <span class="price_sale">

                                                                                <?php if ($sale_on) { ?>
                                                                                    <span class="money" style="text-decoration: line-through;font-size:11px;color:#8b8f8b"><?php echo rupiah($product->price); ?></span>
                                                                                    <span class="money"><?php echo rupiah($end_price); ?></span>
                                                                                <?php
                                                                                    }
                                                                                    else {
                                                                                ?>
                                                                                        <span class="money"><?php echo rupiah($product->price); ?></span>
                                                                                <?php } ?>

                                                                            </span>
                                                                        </div>

                                                                        <?php
                                                                        $merchant = $this->main->get('merchants', ['id' => $product->store_id]);
                                                                        $city = $this->main->get('cities', ['id' => $merchant->city]);
                                                                        ?>
                                                                        <div style="height: 15px;overflow: hidden;">
                                                                            <span id="text-ellipsis-city-<?= $key ?>" style="display: block;"><?= ($city->type == 'Kabupaten') ? 'Kab. ' . $city->name : $city->name ?></span>
                                                                            <span id="text-ellipsis-store-<?= $key ?>" style="display: block;"><?= $merchant->name ?></span>
                                                                        </div>

                                                                        <div class="rating-star">
                                                                            <span class="spr-badge" data-rating="0.0">
                                                                                <span class="spr-starrating spr-badge-starrating">
                                                                                    <?php
                                                                                        $rating = $this->recomend->get_rating ($product->product_id, $product->store_id);
                                                                                        
                                                                                        if (round($rating->rating) > 0) {
                                                                                            
                                                                                            for ($i = 1; $i <= round($rating->rating); $i++) {
                                                                                    ?>
                                                                                                <i class="spr-icon spr-icon-star"></i>
                                                                                    <?php
                                                                                            }
                                                                                            for ($n = (($rating->rating) + 1); $n <= 5; $n++) {
                                                                                    ?>
                                                                                                <i class="spr-icon spr-icon-star-empty"></i>
                                                                                    <?php
                                                                                            }
                                                                                        }
                                                                                    ?>
                                                                                </span>
                                                                            </span>
                                                                        </div>

                                                                        <div class="product-footer">
                                                                            &nbsp;
                                                                            <i class="fa fa-eye" aria-hidden="true"></i> <?= $product->viewed; ?>
                                                                        </div>
                                                                       <?php
                                                                        $preorder = $product->preorder;
                                                                        $free_ongkir = $product->free_ongkir;

                                                                        if ($preorder == 1){
                                                                            echo'<div class="pre-order-label">Preorder</div>';
                                                                        }
                                                                        if($free_ongkir == 1) {
                                                                            echo'<div class="free-ongkir-label">Free Ongkir</div>';
                                                                        } ?>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                <?php
                                                    }
                                                ?>
                                                <div class="load-more" lastView="<?php echo $product->date_modified; ?>" last_key="<?= $key + 1 ?>" style="display: none;"> </div>
                                            </div>
                                        </div>
                                        <?php if($products->num_rows() > 11) : ?>
                                            <div class="lds-ring"><div></div><div></div><div></div><div></div></div> 
                                            <div id="bottom_check"></div>
                                        <?php endif; ?>
                                        <?php
                                            //if ($pagination) {
                                        ?>
                                                <!-- <div class="collection-bottom-toolbar">
                                                    <div class="product-pagination">
                                                        <div class="pagination_group">
                                                            <?php //echo $pagination; ?>
                                                        </div>
                                                    </div>
                                                </div> -->
                                    <?php  //}
                                        } else {
                                    ?>
                                            <p>Tidak ada produk yang dapat ditampilkan</p>
                                    <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
function addtoCart(t,a){$.ajax({url:site_url+"catalog/products/add_to_cart_ajax_new",type:"post",data:{product:t,store:a,qty:1},beforeSend:function(){$("#tags-load").css("display","block")},complete:function(){$("#tags-load").css("display","none")},success:function(t){if("error"==(t=JSON.parse(t)).status)Swal.fire({type:"error",title:"Terjadi Kesalahan",text:t.message});else{var a=Object.keys(t.items).length,e=0,i=0;if(a>0){var s='<div style="max-height: 200px; overflow: auto">';s+='<div class="carts-items">',$.each(t.items,function(t,a){if(a.option){let t=JSON.parse(a.option);$.each(t,function(t,o){e+=Number(o.price*o.quantity),i+=Number(o.quantity),s+='<div class="row" id="cart-header-option-'+a.id_encrypt+'" style="padding: 10px 10px;border-bottom:1px solid #EEE;">',s+='<div class="cart-left col-md-4" style="display: inline-block;">',s+='<a class="cart-image" href="'+site_url+"catalog/products/view/"+a.product_id+"/"+a.merchant_id+'">',s+='<img style="width: 50px;" src="'+site_url+"files/images/"+o.image+'" alt="" title="">',s+="</a>",s+="</div>",s+='<div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">',s+='<div class="">',s+='<a href="'+site_url+"catalog/products/view/"+a.product_id+"/"+a.merchant_id+'" style="color:#000;">',s+=a.name,s+="</a>",s+="</div>",s+='<div class="cart-price">',s+='<span class="money" style="color: #97C23C;">'+formatRupiah(o.price,".")+"</span>",s+='<span class="x" style="color: #97C23C;">x '+o.quantity+"</span>",s+="</div>",s+="</div>",s+="</div>"})}else e+=Number(a.price*a.quantity),i+=Number(a.quantity),s+='<div class="row" id="cart-header-'+a.id_encrypt+'" style="padding: 10px 10px;border-bottom:1px solid #EEE;">',s+='<div class="cart-left col-md-4" style="display: inline-block;">',s+='<a class="cart-image" href="'+site_url+"catalog/products/view/"+a.product_id+"/"+a.merchant_id+'">',s+='<img style="width: 50px;" src="'+site_url+"files/images/"+a.image+'" alt="" title="">',s+="</a>",s+="</div>",s+='<div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">',s+='<div class="">',s+='<a href="'+site_url+"catalog/products/view/"+a.product_id+"/"+a.merchant_id+'" style="color:#000;">',s+=a.name,s+="</a>",s+="</div>",s+='<div class="cart-price">',s+='<span class="money" style="color: #97C23C;">'+formatRupiah(a.price,".")+"</span>",s+='<span class="x" style="color: #97C23C;">x '+a.quantity+"</span>",s+="</div>",s+="</div>",s+="</div>"}),s+="</div>",s+="</div>",s+='<div class="subtotal" style="color: black;padding: 15px 20px;font-size:14px;border-top:1px solid #EEE;">',s+='<span>Subtotal:</span><span class="cart-total-right money" style="float: right;">'+formatRupiah(e,".")+"</span>",s+="</div>",s+='<div class="action" style="padding: 15px 20px;margin-top:-10px;">',s+='<a href="'+site_url+'cart" class="btn btn-show-all" style="font-size:10px; width: 100%;border-radius:3px;">Lihat Semua</a>',s+="</div>"}else s='<p style="font-size: 13px; font-weight: bold; padding: 15px;text-align:center;">Keranjang belanja Anda kosong. Yuk, belanja sekarang!</p>';$("#cart-target .badge").html(i),$("#cart-button .badge").html(i),$(".icon-cart .cart_text .number").html(i),$("#cart-target-mobile .number").html(i),$("#cart-target .cart-info .cart-content").html(s),$("#cart-target-mobile .cart-info .cart-content").html(s),$(".cart_icon #icon_cart .badge").html(i),$(".cart_icon .cart-dropdown").html(s),Swal.fire({type:"success",title:"Produk Berhasil Ditambah!",timer:1e3,showConfirmButton:!1,customClass:"swal-class"})}}})}function formatRupiah(t,a){var e=(t=t.toString()).replace(/[^,\d]/g,"").toString().split(","),i=e[0].length%3,s=e[0].substr(0,i),o=e[0].substr(i).match(/\d{3}/gi);return o&&(separator=i?".":"",s+=separator+o.join(".")),s=null!=e[1]?s+","+e[1]:s,null==a?s:s?"Rp "+s:""}function loaddata(t,a){$.ajax({type:"POST",url:site_url+"catalog/recomendation/load_more_recomendation_product",data:{viewed:t,last_key:a},beforeSend:function(){$(".load-more").show(),$(".lds-ring").show()},success:function(t){$(".load-more").remove(),$(".lds-ring").remove(),$("#product_recommended").append(t)}})}function change_store(t,a){"over"==a?$("#text-ellipsis-city-"+t).attr("style","margin-top:-18px;display:block;transition:.3s;"):$("#text-ellipsis-city-"+t).attr("style","margin-top:0px;display:block;transition:.3s;")}function add_wishlist(t){let a=$(t).data("product"),e=$(t).data("store"),i=$(t).data("customer");$.ajax({url:site_url+"catalog/products/add_wishlist_new",data:{product:a,store:e,customer:i},type:"POST",success:function(a){"add"==(a=JSON.parse(a)).status?($(t).attr("style","color:#d9534f;"),Swal.fire({type:"success",title:"Wishlist Berhasil Ditambah",timer:1e3,showConfirmButton:!1}),$(".profile-list .profile-dropdown .profile-body .wishlist-link .badge").html(a.total),$(".profile-list .profile-dropdown .profile-body .wishlist-link .badge").attr("style","position: absolute;right:25px;")):($(t).attr("style","color:#BBB;"),Swal.fire({type:"success",title:"Wishlist Berhasil Dihapus",timer:1e3,showConfirmButton:!1}),$(".profile-list .profile-dropdown .profile-body .wishlist-link .badge").html(a.total),$(".profile-list .profile-dropdown .profile-body .wishlist-link .badge").attr("style","position: absolute;right:25px;"))}})}$(document).ready(function(){var t;$(window).scroll(function(){t&&clearTimeout(t);var a=$(".load-more").attr("lastView"),e=$(".load-more").attr("last_key"),i=$("#bottom_check").offset().top-($(window).scrollTop()+$(window).height());t=setTimeout(function(){i<300&&0!=a&&loaddata(a,e)},600)}),$(".btn-add-to-wishlist").on("click",function(t){t.preventDefault()})});
</script>