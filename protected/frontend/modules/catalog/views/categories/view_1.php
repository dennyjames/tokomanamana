<?php echo $breadcrumb; ?>
<div class="content-page">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="sidebar sidebar-left">
                    <div class="widget widget-product-cat">
                        <div class="widget-content">
                            <ul>
                                <?php
                                $pathCategories = explode(',', $data->path);
                                $categories = $this->category_model->get_categories(0);
                                foreach ($categories->result() as $category) {
                                    ?>
                                    <li class="<?php echo (in_array($category->id, $pathCategories)) ? 'active' : ''; ?>">
                                        <a href="<?php echo seo_url('catalog/categories/view/' . $category->id); ?>"><?php echo $category->name; ?></a>
                                        <?php if (in_array($category->id, $pathCategories) && $subcategories = $this->category_model->get_categories($category->id)) { ?> 
                                            <ul>
                                                <?php foreach ($subcategories->result() as $subcategory) { ?>
                                                    <li class="<?php echo (in_array($subcategory->id, $pathCategories)) ? 'active' : ''; ?>">
                                                        <a href="<?php echo seo_url('catalog/categories/view/' . $subcategory->id); ?>"><?php echo $subcategory->name; ?></a>
                                                        <?php if (in_array($subcategory->id, $pathCategories) && $subcategories2 = $this->category_model->get_categories($subcategory->id)) { ?>
                                                            <ul>
                                                                <?php foreach ($subcategories2->result() as $subcategory2) { ?>
                                                                    <li class="<?php echo (in_array($subcategory2->id, $pathCategories)) ? 'active' : ''; ?>"><a href="<?php echo seo_url('catalog/categories/view/' . $subcategory2->id); ?>"><?php echo $subcategory2->name; ?></a></li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php } ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        <?php } ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="title-single"><?php echo $data->name; ?></h2>
                    </div>
                </div>
                <div class="content-grid-boxed">
                    <?php if ($products->num_rows() == 0) { ?>
                        <div class="grid-pro-color">
                            <p>Belum ada produk di kategori ini.</p>
                        </div>
                    <?php } else { ?>
                        <div class="sort-pagi-bar clearfix">
                            <div class="sort-paginav pull-right">
                                <form id="form-filter">
                                    <div class="select-box">
                                        <label>Sort By:</label>
                                        <select name="sort" id="sort" tabindex="-1">
                                            <option value="popular" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'popular') ? 'selected' : ''; ?>>Terpopuler</option>
                                            <option value="nameAZ" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'nameAZ') ? 'selected' : ''; ?>>Nama (A-Z)</option>
                                            <option value="nameZA" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'nameZA') ? 'selected' : ''; ?>>Nama (Z-A)</option>
                                            <option value="lowprice" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'lowprice') ? 'selected' : ''; ?>>Termurah</option>
                                            <option value="highprice" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'highprice') ? 'selected' : ''; ?>>Termahal</option>
                                            <option value="newest" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'newest') ? 'selected' : ''; ?>>Terbaru</option>
                                        </select>
                                    </div>
                                    <div class="show-bar select-box">
                                        <label>Show:</label>
                                        <select name="show" class="" tabindex="-1">
                                            <option value="20" <?php echo ($this->input->get('show') && $this->input->get('show') == '20') ? 'selected' : ''; ?>>20</option>
                                            <option value="40" <?php echo ($this->input->get('show') && $this->input->get('show') == '40') ? 'selected' : ''; ?>>40</option>
                                            <option value="80" <?php echo ($this->input->get('show') && $this->input->get('show') == '80') ? 'selected' : ''; ?>>80</option>
                                        </select>
                                    </div>
                                </form>
                                <div class="pagi-bar">
                                    <?php echo $pagination; ?>
                                </div>
                            </div>
                        </div>
                        <div class="grid-pro-color">
                            <div class="row">
                                <?php foreach ($products->result() as $product) { ?>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                        <div class="item-pro-color">
                                            <div class="product-thumb">
                                                <a href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>">
                                                    <img src="<?php echo ($product->image) ? ($image = get_thumbnail($product->image)) ? $image : site_url($product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" alt="<?php echo $product->name; ?>">
                                                </a>
                                            </div>
                                            <div class="product-info">
                                                <h3 class="product-title"><a href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>"><?php echo $product->name; ?></a></h3>
                                                <div class="product-price">
                                                    <?php if ($product->discount > 0) { ?>
                                                        <del><span><?php echo rupiah($product->price_old); ?></span></del>
                                                    <?php } ?>
                                                    <ins><span><?php echo rupiah($product->price); ?></span><?php echo ($product->discount > 0) ? '<span class="sale">-' . number($product->discount) . '%</span>' : ''; ?></ins>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="pagi-bar bottom">
                                <?php echo $pagination; ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
