<style>
    .collection-title{margin-top:25px;font-size:18px!important;color:#a7c22a!important;font-weight:700!important}.breadcrumb a,i.fa.fa-home{color:#97c23c!important}.breadcrumb a:hover,.breadcrumb a:hover i.fa.fa-home{color:#969eaa!important}.product-item{border:none}.product{margin-left:7px;margin-right:7px;margin-bottom:15px!important;border:1px solid #e8e8e8;box-shadow:0 0 7px -3px rgba(0,0,0,.3);min-height:355px;border-radius:5px}#collection .collection-mainarea .collection-items .product-item{border:none}.product-footer{margin-top:0}.garis-tengah{width:100%;height:1px;background-color:#e8e8e8;display:block;margin-top:10px}.tombol-cart{z-index:1;background-color:#fff;position:absolute;top:48%;right:5%;padding:0;display:inline-block;border:1px solid rgba(167,194,42,.3);height:40px;border-radius:50%;width:40px;box-shadow:0 0 7px 1px rgba(167,194,42,.3);-webkit-transition:all .5s;-moz-transition:all .5s;-ms-transition:all .5s;-o-transition:all .5s;transition:all .5s}.tombol-cart button{background:0 0;border:none;line-height:0;padding:0 7px;outline:0}.tombol-cart button-cart:active,.tombol-cart button-cart:hover,.tombol-cart button:active,.tombol-cart button:hover{border:none;background:0 0;outline:0}.tombol-cart a{padding:12px 7px;line-height:37px}.tombol-cart:hover{background-color:#a7c22a;box-shadow:0 0 7px 1px rgba(167,194,42,.3)}.tombol-cart:hover .icon-cart i{color:#fff}.pagination_group{padding:10px 0!important;text-align:right}.pagination_group .pagination li a{border-top:1px solid #e0e0e0;border-bottom:1px solid #e0e0e0;border-left:1px solid #e0e0e0;background-color:#fff;color:#97c23c;height:30px;vertical-align:middle;padding:0 13px;text-transform:capitalize;font-size:13px;border-radius:0;font-weight:500;-webkit-transition:all .5s;-moz-transition:all .5s;-ms-transition:all .5s;-o-transition:all .5s;transition:all .5s}.pagination_group .pagination li.active a{background-color:#f1f1f1;color:#ccc;border:1px solid #e0e0e0}.pagination_group .pagination li:hover a{background-color:#f1f1f1;color:#ccc}.pre-order-label{color:#666;bottom:0;display:inline-block;margin-top:3px;position:relative;z-index:1;padding:2px 10px;background-color:#eee;font-weight:700;border-radius:2px}.collection-leftsidebar{border:1px solid #e8e8e8;box-shadow:0 0 7px -3px rgba(0,0,0,.3)}.sidebar.collection-leftsidebar{margin-top:-65px}.collection-wrapper{margin-top:20px}@media (max-width:320px){.pre-order-label{position:absolute!important;margin-bottom:10px;bottom:0}.product{min-height:355px!important}.tombol-cart{top:48%}.hoverBorder{width:70%!important}.img-responsive{margin-left:23%}}@media (max-width:375px){.pre-order-label{position:relative!important;margin-top:5px}}@media (max-width:425px) and (min-width:376px){.tombol-cart{top:48%}}@media (max-width:375px) and (min-width:321px){.tombol-cart{top:48%}.product{min-height:357px!important}}@media (max-width:767px) and (min-width:426px){.tombol-cart{top:48%}}@media (max-width:767px){.pre-order-label{position:relative!important;margin-bottom:5px!important}.product{min-height:360px}}@media (max-width:1024px) and (min-width:768px){.tombol-cart{top:48%}}@media (min-width:1024px){.product{min-height:355px}}@media (max-width:1199px) and (min-width:992px){#collection .products .product-item{width:25%!important}}@media (max-width:991px) and (min-width:585px){#collection .products .product-item{width:33.333333%!important}}.free-ongkir-label{color:#fff;bottom:0;display:inline-block;margin-top:3px;margin-left:1px;margin-right:1px;position:relative;z-index:1;padding:2px 5px;background-color:#a7c22a;font-weight:700;border-radius:2px}.btn-add-to-wishlist{position:absolute;z-index:1;top:6px;left:10px;font-size:18px;transition:.5s}.btn-add-to-wishlist:hover{color:#d9534f!important}.money{font-weight:600}.product-discount-label{position:absolute;top:8px;right:10px;z-index:1;background-color:#d9534f;color:#fff;padding:5px 10px;border-radius:5px}@media (max-width:382px) and (min-width:321px){.pre-order-label{font-size:10px;padding:2px 5px}.free-ongkir-label{font-size:10px;padding:2px 3px}}
</style>
<?php $word = htmlspecialchars($this->input->get('word')); ?>
<section class="collection-heading heading-content" style="<?= ($this->agent->is_mobile()) ? 'margin-top:-35px;' : 'margin-top:-20px;' ?>">
    <div class="container">
        <div class="row">
            <div class="collection-wrapper" style="padding-right: 5px;padding-left: 5px;">
                <h1 class="collection-title">
                    <span><?php echo 'Pencarian produk "' . $word . '"'; ?></span>
                </h1>
                <?php echo $breadcrumb; ?>
            </div>
        </div>
    </div>
</section>

<section class="collection-content">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div id="shopify-section-collection-template" class="shopify-section">
                    <div class="collection-inner">
                        <div id="tags-load" style="display:none;">
                            <i class="fa fa-spinner fa-pulse fa-2x"></i>
                        </div>

                        <div id="collection">
                            <div class="collection_inner">
                                <div class="collection-mainarea col-sm-12 clearfix">
                                    <?php if ($products->num_rows() > 0) { ?>
                                        <div class="collection_toolbar" style="padding-left: 7px; padding-right: 7px;">
                                            <div class="toolbar_left">
                                                Item <?php echo number($start); ?> s/d <?php echo number($to); ?> dari <?php echo number($total_products); ?> Total Item
                                            </div>

                                            <div class="toolbar_right">
                                                <div class="group_toolbar">
                                                    <form id="form-filter">
                                                        <input type="hidden" name="word" value="<?php echo $this->input->get('word'); ?>">
                                                        <input type="hidden" name="cat" value="<?php echo $this->input->get('cat'); ?>">
                                                        <div class="sortBy">
                                                            <span class="toolbar_title">Urut Berdasarkan:</span>
                                                            <div class="control-container">
                                                                <select name="sort" tabindex="-1" onchange="$('#form-filter').submit();">
                                                                    <!-- <option value="distance" <?= ($this->input->get('sort') && $this->input->get('sort' == 'distance')) ? 'selected' : '' ?>>Terdekat</option> -->
                                                                    <option value="popular" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'popular') ? 'selected' : ''; ?>>Terpopuler</option>
                                                                    <option value="nameAZ" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'nameAZ') ? 'selected' : ''; ?>>Nama (A-Z)</option>
                                                                    <option value="nameZA" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'nameZA') ? 'selected' : ''; ?>>Nama (Z-A)</option>
                                                                    <option value="lowprice" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'lowprice') ? 'selected' : ''; ?>>Harga Termurah</option>
                                                                    <option value="highprice" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'highprice') ? 'selected' : ''; ?>>Harga Termahal</option>
                                                                    <option value="newest" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'newest') ? 'selected' : ''; ?>>Terbaru</option>
                                                                </select>
                                                            </div>

                                                            <span class="toolbar_title">Tampilkan:</span>
                                                            <div class="control-container">
                                                                <select name="show" tabindex="-1" onchange="$('#form-filter').submit();">
                                                                    <option value="20" <?php echo ($this->input->get('show') && $this->input->get('show') == '20') ? 'selected' : ''; ?>>20</option>
                                                                    <option value="40" <?php echo ($this->input->get('show') && $this->input->get('show') == '40') ? 'selected' : ''; ?>>40</option>
                                                                    <option value="80" <?php echo ($this->input->get('show') && $this->input->get('show') == '80') ? 'selected' : ''; ?>>80</option>
                                                                </select>
                                                            </div>
                                                            <span class="toolbar_title">Item Per Halaman</span>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="collection-items clearfix" style="border:none;">
                                            <div class="products" style="border:none;">
                                                <?php
                                                foreach ($products->result() as $key => $product) {

                                                    // if ($product->merchant == 0) {
                                                    //   $merchant = $this->session->userdata('list_merchant')[0];
                                                    //   $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
                                                    //   if ($price_group) {
                                                    //     $product->price = $price_group->price;
                                                    //   }
                                                    // }

                                                    if ($this->ion_auth->logged_in()) {
                                                        $id_customer = $this->data['user']->id;
                                                        $get_wishlist = $this->main->get('wishlist', ['product' => $product->product_id, 'branch_id' => $product->store_id, 'customer' => $id_customer]);
                                                        if ($get_wishlist) {
                                                            $product_wishlist = true;
                                                        } else {
                                                            $product_wishlist = false;
                                                        }
                                                    } else {
                                                        $product_wishlist = false;
                                                    }

                                                    $sale_on = false;

                                                    if($product->variation) {
                                                        $product_variation = $this->main->get('product_option', ['product' => $product->product_id, 'default' => 1]);
                                                        if($product->price_grosir) {
                                                            if($product_variation) {
                                                                $product->price = $product->price;
                                                                $quantity_variations = $product_variation->quantity;
                                                            } else {
                                                                $product->price = 0;
                                                                $quantity_variations = 0;
                                                            }
                                                        } else {
                                                            if($product_variation) {
                                                                $product->price = $product_variation->price;
                                                                $quantity_variations = $product_variation->quantity;
                                                            } else {
                                                                $product->price = 0;
                                                                $quantity_variations = 0;
                                                            }
                                                        }
                                                    }

                                                    if($product->promo) {
                                                        $promo_data = json_decode($product->promo_data, true);

                                                        if ($promo_data['type'] == 'P') {
                                                            $disc_price = round(($product->price * $promo_data['discount']) / 100);
                                                            $label_disc = $promo_data['discount'] . '% off';
                                                        } else {
                                                            $disc_price = $promo_data['discount'];
                                                            $label_disc = 'SALE';
                                                        }

                                                        $end_price = intval($product->price) - intval($disc_price);
                                                        $today = date('Y-m-d');
                                                        $today = date('Y-m-d', strtotime($today));
                                                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                                        if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                                            $sale_on = true;
                                                        } else {
                                                            $sale_on = false;
                                                        }
                                                    }
                                                ?>
                                                    <div class="product-item col-sm-2">
                                                        <div class="product" onmouseover="change_store(<?= $key ?>, 'over')" onmouseout="change_store(<?= $key ?>, 'out')">

                                                            <?php if ($sale_on) : ?>
                                                                <span class="product-discount-label"><?php echo $label_disc; ?></span>
                                                            <?php endif; ?>

                                                            <?php if ($this->ion_auth->logged_in()) : ?>
                                                                <a href="#" class="btn-add-to-wishlist" style="<?= ($product_wishlist) ? 'color:#d9534f;' : 'color:#BBB'  ?>" onclick="add_wishlist(this)" data-product="<?= encode($product->product_id) ?>" data-store="<?= encode($product->store_id) ?>" data-customer="<?= encode($id_customer) ?>">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                                                </a>
                                                            <?php else : ?>
                                                                <a href="#" class="btn-add-to-wishlist" style="color:#BBB" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                                                </a>
                                                            <?php endif; ?>

                                                            <div class="row-left">
                                                                <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="hoverBorder container_item" style="min-height: 180px;">
                                                                    <!-- <img src="<?php //echo ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" class="img-responsive" alt="<?php //echo $product->name; ?>"> -->
                                                                    <img src="<?php echo base_url('assets/frontend/images/loading.gif') ?>" alt="<?= $product->name; ?>" class="img-responsive lazyload" data-src="<?php echo ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>">
                                                                </a>

                                                            </div>

                                                            <div class="garis-tengah"></div>
                                                            <div class="tombol-cart">
                                                                <?php if ($this->ion_auth->logged_in()) : ?>
                                                                    <?php if ($product->variation) : ?>
                                                                        <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="icon-cart">
                                                                            <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                        </a>
                                                                    <?php else : ?>
                                                                        <button data-id="<?php echo encode($product->product_id); ?>" data-storeid="<?= encode($product->store_id) ?>" onclick="addtoCart($(this).data('id'), $(this).data('storeid'))" class="icon-cart">
                                                                            <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                        </button>
                                                                    <?php endif; ?>
                                                                <?php else : ?>
                                                                    <button type="button" class="icon-cart" data-toggle="modal" data-target="#modal-guest">
                                                                        <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                    </button>
                                                                <?php endif; ?>
                                                            </div>

                                                            <div class="row-right animMix">
                                                                <div class="grid-mode">
                                                                    <div class="product-title">
                                                                        <a class="title-5" style="font-weight: bold; width: 75%;transition:.3s;" href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>"><?php echo $product->name; ?></a>
                                                                    </div>

                                                                    <div class="product-price">
                                                                        <span class="price_sale">
                                                                            <?php if ($sale_on) { ?>
                                                                                <span class="money" style="text-decoration: line-through;font-size:9px;color:#8b8f8b"><?php echo rupiah($product->price); ?></span>
                                                                                <span class="money"><?php echo rupiah($end_price); ?></span>
                                                                            <?php
                                                                            } else {
                                                                            ?>
                                                                                <span class="money"><?php echo rupiah($product->price); ?></span>
                                                                            <?php } ?>
                                                                        </span>
                                                                    </div>

                                                                    <?php
                                                                    $merchant = $this->main->get('merchants', ['id' => $product->store_id]);
                                                                    $city = $this->main->get('cities', ['id' => $merchant->city]);
                                                                    ?>
                                                                    <div style="height: 15px;overflow: hidden;">
                                                                        <span id="text-ellipsis-city-<?= $key ?>" style="display: block;"><?= ($city->type == 'Kabupaten') ? 'Kab. ' . $city->name : $city->name ?></span>
                                                                        <span id="text-ellipsis-store-<?= $key ?>" style="display: block;"><?= $merchant->name ?></span>
                                                                    </div>

                                                                    <div class="rating-star">
                                                                        <span class="spr-badge" data-rating="0.0">
                                                                            <span class="spr-starrating spr-badge-starrating">
                                                                                <?php
                                                                                $rating = $this->search->get_rating($product->product_id, $product->store_id);

                                                                                if (round($rating->rating) > 0) {

                                                                                    for ($i = 1; $i <= round($rating->rating); $i++) {
                                                                                ?>
                                                                                        <i class="spr-icon spr-icon-star"></i>
                                                                                    <?php
                                                                                    }
                                                                                    for ($n = (($rating->rating) + 1); $n <= 5; $n++) {
                                                                                    ?>
                                                                                        <i class="spr-icon spr-icon-star-empty"></i>
                                                                                <?php
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </span>
                                                                        </span>
                                                                    </div>

                                                                    <div class="product-footer">
                                                                        &nbsp;
                                                                        <i class="fa fa-eye" aria-hidden="true"></i> <?= $product->viewed; ?>
                                                                    </div>
                                                                    <?php
                                                                    $preorder = $product->preorder;
                                                                    $free_ongkir = $product->free_ongkir;

                                                                    if ($preorder == 1) {
                                                                        echo '<div class="pre-order-label">Preorder</div>';
                                                                    }
                                                                    if ($free_ongkir == 1) {
                                                                        echo '<div class="free-ongkir-label">Free Ongkir</div>';
                                                                    } ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>

                                        <?php if ($pagination) { ?>
                                            <div class="collection-bottom-toolbar">
                                                <div class="product-pagination">
                                                    <div class="pagination_group">
                                                        <?php echo $pagination; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php   }
                                    } else { ?>
                                        <p>Tidak ada produk dengan kata kunci yang Anda masukkan</p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>