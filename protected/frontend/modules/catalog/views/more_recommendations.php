<?php if($this->agent->is_mobile()) {
    $width_lds = '46%';
} else {
    $width_lds = '48%';
} ?>
<style>
.lds-ring{left:<?php echo $width_lds ?>;display:inline-block;position:relative;width:64px;height:64px;display:none;margin-bottom:25px}.lds-ring div{box-sizing:border-box;display:block;position:absolute;width:51px;height:51px;margin:6px;border-radius:50%;animation:lds-ring .8s cubic-bezier(.5,0,.5,1) infinite;border-color:#a7c22a transparent transparent transparent!important}.lds-ring div:nth-child(1){animation-delay:0s}.lds-ring div:nth-child(2){animation-delay:-80ms}.lds-ring div:nth-child(3){animation-delay:-.1s}@keyframes lds-ring{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}.slick-next:before,.slick-prev:before{color:#97c23c}.loader{border:6px solid #f3f3f3;border-radius:50%;border-top:6px solid #a6dd69;width:50px;height:50px;margin:20px;-webkit-animation:spin 2s linear infinite;animation:spin 2s linear infinite}@-webkit-keyframes spin{0%{-webkit-transform:rotate(0)}100%{-webkit-transform:rotate(360deg)}}@keyframes spin{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}.spinner{margin:100px auto 0;width:70px;text-align:center;padding-bottom:20px}.spinner>div{width:18px;height:18px;background-color:#a6dd69;border-radius:100%;display:inline-block;-webkit-animation:sk-bouncedelay 1.4s infinite ease-in-out both;animation:sk-bouncedelay 1.4s infinite ease-in-out both}.spinner .bounce1{-webkit-animation-delay:-.32s;animation-delay:-.32s}.spinner .bounce2{-webkit-animation-delay:-.16s;animation-delay:-.16s}@-webkit-keyframes sk-bouncedelay{0%,100%,80%{-webkit-transform:scale(0)}40%{-webkit-transform:scale(1)}}@keyframes sk-bouncedelay{0%,100%,80%{-webkit-transform:scale(0);transform:scale(0)}40%{-webkit-transform:scale(1);transform:scale(1)}}.enter{margin-top:25px;margin-bottom:25px;overflow:auto;width:100%}.title-segment{font-size:18px;color:#a7c22a;font-weight:700;margin-bottom:20px;padding:0 7px}.box-brand{border:1px solid #e8e8e8;box-shadow:0 0 7px -3px rgba(0,0,0,.3);margin-right:30px;padding:0 10px}.box-product{box-shadow:0 1px 6px 0 rgba(49,53,59,.12);border:1px solid #a7c22a;border-radius:3px;height:270px;margin-top:25px}.btn-default{background:#a7c22a;border-radius:4px;color:#fff;border:none;text-transform:capitalize;box-shadow:0 1px 6px 0 rgba(49,53,59,.12)}.btn-default:hover{background:#81961e}.btn-white{background:#fff;border-radius:4px;color:#999;border:#ccc;text-transform:capitalize;box-shadow:0 1px 6px 0 rgba(49,53,59,.12)}.btn-white:hover{background:#ccc}body{background-color:#fff}.product-discount-label{position:absolute;top:10px;right:10px;z-index:1;background-color:#d9534f;color:#fff;padding:5px 10px}.lds-ring{display:inline-block;position:relative;height:100%;padding:9px 2px}.lds-ring div{box-sizing:border-box;display:inline-block;position:absolute;width:20px;height:20px;border:3px solid #000;border-radius:50%;animation:lds-ring 1.2s cubic-bezier(.5,0,.5,1) infinite;border-color:#000 transparent transparent transparent}.lds-ring div:nth-child(1){animation-delay:-.45s}.lds-ring div:nth-child(2){animation-delay:-.3s}.lds-ring div:nth-child(3){animation-delay:-.15s}@keyframes lds-ring{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}@media (max-width:991px) and (min-width:585px){.collection-items .products .product-item{width:33.333333%!important}}@media (max-width:320px){.img-responsive{width:70%;margin:0 auto}.product-item{width:100%}}@media (max-width:375px){.title-segment{margin-left:-30px}.product{min-height:355px!important}.tombol-cart{top:48%!important}.title-segment{font-size:15px!important}.lihat-semua-object a{font-size:13px!important}}@media (max-width:425px) and (min-width:376px){.title-segment{margin-left:-30px}.tombol-cart{top:48%!important}.lihat-semua-object{margin-right:-25px!important}}@media (max-width:425px){.group-home-slideshow{margin-top:-45px!important}.lihat-semua-object{margin-right:-20px}}@media (max-width:1024px) and (min-width:426px){.home-slideshow-inner{width:100%!important}}@media (max-width:767px) and (min-width:426px){.tombol-cart{top:46%!important}.product{min-height:369px!important}}@media (max-width:767px) and (min-width:601px){section.home_blog_layout .home_blog_content>div{padding-left:5px!important;padding-right:5px!important}}@media (max-width:600px){section.home_blog_layout .home_blog_content>div{padding-left:0!important;padding-right:0!important}}@media (max-width:1024px) and (min-width:768px){.tombol-cart{top:50%!important}.product{min-height:369px!important}}@media (max-width:1199px) and (min-width:1025px){.product{min-height:369px!important}}.garis-tengah{width:100%;height:1px;background-color:#e8e8e8;display:block;margin-top:10px}.tombol-cart{z-index:1;background-color:#fff;position:absolute;top:48%;right:5%;padding:0;display:inline-block;border:1px solid rgba(167,194,42,.3);height:40px;border-radius:50%;width:40px;box-shadow:0 0 7px 1px rgba(167,194,42,.3);-webkit-transition:all .5s;-moz-transition:all .5s;-ms-transition:all .5s;-o-transition:all .5s;transition:all .5s}.tombol-cart button{background:0 0;border:none;line-height:0;padding:0 7px;outline:0}.tombol-cart:hover .icon-cart i{color:#fff}.tombol-cart button-cart:active,.tombol-cart button-cart:hover,.tombol-cart button:active,.tombol-cart button:hover{border:none;background:0 0;outline:0}.tombol-cart a{padding:12px 7px;line-height:37px}.tombol-cart:hover{background-color:#a7c22a;box-shadow:0 0 7px 1px rgba(167,194,42,.3)}.product-item{border:none}.product{margin-left:7px;margin-right:7px;margin-bottom:15px!important;border:1px solid #e8e8e8;box-shadow:0 0 7px -3px rgba(0,0,0,.3);min-height:355px;border-radius:5px}.pre-order-label{color:#666;bottom:0;display:inline-block;margin-top:3px;position:relative;z-index:1;padding:2px 10px;background-color:#eee;font-weight:700;border-radius:2px}.lihat-semua-object{padding:2px 5px 2px 5px;border-radius:10px;transition:all .5s ease-in-out;-webkit-transition:all .5s ease-in-out;-moz-transition:all .5s ease-in-out;-o-transition:all .5 ease-in-out}.hover-lihat-semua a{transition:all .3s;-webkit-transition:all .3s;-moz-transition:all .3s;-o-transition:all .3s}.hover-lihat-semua:hover a{color:#a7c22a}.slick-dots{bottom:1px}.slick-next,.slick-prev{transition:all .4s ease-in-out;box-shadow:0 1px 12px 0 rgba(0,0,0,.12);border-radius:100%;z-index:3}.responsive-desktop .slick-prev{left:12px}.responsive-desktop .slick-next{right:12px}.responsive-brand .slick-prev{left:-46px}.responsive-brand .slick-next{right:-46px}.slick-next:before,.slick-prev:before{color:#a6a6a6}.slick-next:hover,.slick-prev:hover{transform:scale(2);transform-origin:bottom}.slick-slide{position:relative}#mobile-slider .slick-slide{margin:0 27px}#mobile-slider .slick-list{margin:0 -27px}#desktop-slider .slick-slide{margin:0 27px}#desktop-slider .slick-list{margin:0 -27px}.responsive-brand .slick-slide{margin:0 27px}.responsive-brand .slick-list{margin:0 -27px}.slick-dots li.slick-active button:before{color:#a7c22a!important;font-size:10px;line-height:10px}.slick-dots li button:before{font-size:10px;line-height:10px}.responsive-mobile{margin-top:30px;margin-bottom:-10px}.responsive-desktop{margin:10px 7px}.responsive-brand{margin:0 50px}#banner-home-web{margin-top:50px}.slick-dotted.slick-slider{margin-bottom:0!important}.main-slideshow{margin-top:30px}@media(max-width:425px){.responsive-brand .slick-list{margin:0 -50px!important}}@media (min-width:480px) and (max-width:649px){.responsive-brand .slick-list{margin:0 -70px!important}.responsive-brand .slick-slide{margin:0 15px!important}}@media (max-width:767px) and (min-width:426px){.lihat-semua-object{margin-right:-25px!important}.title-segment{margin-left:-30px}}@media (max-width:360px) and (min-width:349px){.lihat-semua-object{margin-right:-24px!important}}@media (max-width:348px) and (min-width:331px){.lihat-semua-object{margin-right:-30px!important}}@media (max-width:330px){.lihat-semua-object{margin-right:-34px!important}}.swal2-icon.swal2-info{color:#a7c22a;border-color:#a7c22a}.big-title-segment{font-size:18px;color:#a7c22a;font-weight:700;margin-bottom:20px}.free-ongkir-label{color:#fff;bottom:0;display:inline-block;margin-top:3px;margin-left:1px;margin-right:1px;position:relative;z-index:1;padding:2px 5px;background-color:#a7c22a;font-weight:700;border-radius:2px}.btn-add-to-wishlist{position:absolute;z-index:1;top:6px;left:10px;font-size:18px;transition:.5s}.btn-add-to-wishlist:hover{color:#d9534f!important}.money{font-weight:600}@media (max-width:382px) and (min-width:321px){.pre-order-label{font-size:10px;padding:2px 5px}.free-ongkir-label{font-size:10px;padding:2px 3px}}
</style>


        <?php
            foreach ($products as $product) {

                if ($product->merchant == 0) {
                    $merchant = $this->session->userdata('list_merchant')[0];
                    $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
                    if ($price_group) {
                        $product->price = $price_group->price;
                    }
                }

                if ($this->ion_auth->logged_in()) {
                    $id_customer = $this->data['user']->id;
                    $get_wishlist = $this->main->get('wishlist', ['product' => $product->product_id, 'branch_id' => $product->store_id, 'customer' => $id_customer]);
                    if ($get_wishlist) {
                        $product_wishlist = true;
                    } else {
                        $product_wishlist = false;
                    }
                } else {
                    $product_wishlist = false;
                }

                if($product->variation) {
                    $product_variation = $this->main->get('product_option', ['product' => $product->product_id, 'default' => 1]);
                    if($product->price_grosir) {
                        if($product_variation) {
                            $product->price = $product->price;
                            $product->quantity = $product_variation->quantity;
                        } else {
                            $product->price = $product->price;
                            $product->quantity = $product->quantity;
                        }
                    } else {
                        if($product_variation) {
                            $product->price = $product_variation->price;
                            $product->quantity = $product_variation->quantity;
                        } else {
                            $product->price = $product->price;
                            $product->quantity = $product->quantity;
                        }
                    }
                }

                $sale_on = false;

                if($product->promo) {
                    $promo_data = json_decode($product->promo_data,true);
                
                    if($promo_data['type'] == 'P') {
                        $disc_price = round(($product->price * $promo_data['discount']) / 100);
                        $label_disc = $promo_data['discount'].'% off';
                    } else {
                        $disc_price = $promo_data['discount'];
                        $label_disc = 'SALE';
                    }
                    
                    $end_price = intval($product->price) - intval($disc_price);
                    $today = date('Y-m-d');
                    $today=date('Y-m-d', strtotime($today));
                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                    if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                        $sale_on = true;
                    } else {
                        $sale_on = false;
                    }
                }
        ?>
                <div class="product-item col-sm-2" style="padding: 0px; border:none;">
                    <div class="product" onmouseover="change_store(<?= $key ?>, 'over')" onmouseout="change_store(<?= $key ?>, 'out')">

                        <?php if ($sale_on){ ?>
                            <span class="product-discount-label"><?php echo $label_disc;?></span>
                        <?php
                            }
                        ?>

                        <?php if ($this->ion_auth->logged_in()) : ?>
                            <a href="#" class="btn-add-to-wishlist" style="color:<?= ($product_wishlist) ? '#d9534f;' : '#BBB'  ?>" onclick="add_wishlist(this)" data-product="<?= encode($product->product_id) ?>" data-store="<?= encode($product->store_id) ?>" data-customer="<?= encode($id_customer) ?>">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        <?php else : ?>
                            <a href="#" class="btn-add-to-wishlist" style="color:#BBB" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        <?php endif; ?>

                        <div class="row-left">
                            <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="hoverBorder container_item" style="min-height: 180px;">
                                <img src="<?php echo ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" class="img-responsive" alt="<?php echo $product->name; ?>">
                            </a>
                        </div>

                        <div class="garis-tengah"></div>
                        <div class="tombol-cart">
                        <?php if($this->ion_auth->logged_in()) : ?>
                        <?php if ($product->variation) : ?>
                            <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="icon-cart">
                            <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                            </a>
                        <?php else : ?>
                            <button data-id="<?php echo encode($product->product_id); ?>" data-storeid="<?= encode($product->store_id) ?>" onclick="addtoCart($(this).data('id'), $(this).data('storeid'))" class="icon-cart">
                                <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                            </button>
                        <?php endif; ?>
                        <?php else : ?>
                            <button type="button" class="icon-cart btn-cart-not-login" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                            </button>
                            <!-- <a href="<?php echo site_url('/?back=' . $this->input->get('back')) ?>" class="icon-cart" id="btn-cart-not-login" data-toggle="modal" data-target="#modal-guest">
                                <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                            </a> -->
                        <?php endif; ?>
                        </div>

                        <div class="row-right animMix">
                            <div class="grid-mode">
                                <div class="product-title">
                                    <a class="title-5" style="font-weight: bold; width: 75%;transition:.3s;" href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>"><?php echo $product->name; ?></a>
                                </div>

                                <div class="product-price">
                                    <span class="price_sale">

                                        <?php if ($sale_on) { ?>
                                            <span class="money" style="text-decoration: line-through;font-size:11px;color:#8b8f8b"><?php echo rupiah($product->price); ?></span>
                                            <span class="money"><?php echo rupiah($end_price); ?></span>
                                        <?php
                                            }
                                            else {
                                        ?>
                                                <span class="money"><?php echo rupiah($product->price); ?></span>
                                        <?php } ?>

                                    </span>
                                </div>

                                <?php
                                $merchant = $this->main->get('merchants', ['id' => $product->store_id]);
                                $city = $this->main->get('cities', ['id' => $merchant->city]);
                                ?>
                                <div style="height: 15px;overflow: hidden;">
                                    <span id="text-ellipsis-city-<?= $key ?>" style="display: block;"><?= ($city->type == 'Kabupaten') ? 'Kab. ' . $city->name : $city->name ?></span>
                                    <span id="text-ellipsis-store-<?= $key ?>" style="display: block;"><?= $merchant->name ?></span>
                                </div>

                                <div class="rating-star">
                                    <span class="spr-badge" data-rating="0.0">
                                        <span class="spr-starrating spr-badge-starrating">
                                            <?php
                                                $rating = $this->recomend->get_rating ($product->product_id, $product->store_id);
                                                
                                                if (round($rating->rating) > 0) {
                                                    
                                                    for ($i = 1; $i <= round($rating->rating); $i++) {
                                            ?>
                                                        <i class="spr-icon spr-icon-star"></i>
                                            <?php
                                                    }
                                                    for ($n = (($rating->rating) + 1); $n <= 5; $n++) {
                                            ?>
                                                        <i class="spr-icon spr-icon-star-empty"></i>
                                            <?php
                                                    }
                                                }
                                            ?>
                                        </span>
                                    </span>
                                </div>

                                <div class="product-footer">
                                    &nbsp;
                                    <i class="fa fa-eye" aria-hidden="true"></i> <?= $product->viewed; ?>
                                </div>
                                <?php
                                $preorder = $product->preorder;
                                $free_ongkir = $product->free_ongkir;

                                if ($preorder == 1){
                                    echo'<div class="pre-order-label">Preorder</div>';
                                }
                                if($free_ongkir == 1) {
                                    echo'<div class="free-ongkir-label">Free Ongkir</div>';
                                } ?>

                            </div>
                        </div>
                    </div>
                </div>
        <?php
        $key++;
            }
        ?>
        <?php if($products_num > $products_limit) : ?>
            <div class="load-more" lastView="<?php echo $product->date_modified; ?>" last_key="<?= $key + 1 ?>" style="display: none;"> </div>
        <?php endif; ?>

<?php if($products_num > $products_limit) : ?>
<div class="lds-ring"><div></div><div></div><div></div><div></div></div> 
<?php endif; ?>

<script>
$(function() {
    $('.btn-add-to-wishlist').on('click', function(e) {
        e.preventDefault();
    })
})
</script>