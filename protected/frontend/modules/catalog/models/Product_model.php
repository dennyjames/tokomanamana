<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product_model extends MY_Model
{

    public $table = 'products';
    public $primary_key = 'id';

    /* ---------- NEW METHOD ---------- */
    function get_detail_products($product_id, $store_id)
    {
        $query = "(SELECT `products_principal_stock`.`id` AS `stock_id`,
                `products_principal_stock`.`product_id`, 
                `products_principal_stock`.`branch_id`, 
                `products_principal_stock`.`quantity` AS `product_quantity`, 
                `products`.*, 
                `merchants`.`name` AS `merchant_name`, 
                `merchants`.`merchant_profile_image` AS `merchant_image`,
                `brands`.`name` AS `brand_name`, 
                `product_image`.`image` 
                FROM `products_principal_stock` 
                LEFT JOIN `products` ON `products_principal_stock`.`product_id` = `products`.`id` 
                LEFT JOIN `merchants` ON `products_principal_stock`.`branch_id` = `merchants`.`id` 
                LEFT JOIN `brands` ON `products`.`brand` = `brands`.`id` 
                LEFT JOIN `product_image` ON `product_image`.`product` = `products`.`id` AND `product_image`.`primary` = 1 
                WHERE `products`.`status` = 1 
                AND `products_principal_stock`.`product_id` = $product_id
                AND `products_principal_stock`.`branch_id` = $store_id
                AND `products`.`store_type` = 'principal')

                UNION

                (SELECT `products`.`id` AS `stock_id`,
                `products`.`id` AS `product_id`, 
                `products`.`store_id` AS `branch_id`, 
                `products`.`quantity` AS `product_quantity`, 
                `products`.*, 
                `merchants`.`name` AS `merchant_name`, 
                `merchants`.`merchant_profile_image` AS `merchant_image`,
                `brands`.`name` AS `brand_name`, 
                `product_image`.`image` 
                FROM `products` 
                LEFT JOIN `merchants` ON `merchants`.`id`= `products`.`store_id` 
                LEFT JOIN `brands` ON `products`.`brand` = `brands`.`id` 
                LEFT JOIN `product_image` ON `product_image`.`product` = `products`.`id` AND `product_image`.`primary` = 1 
                WHERE `products`.`status` = 1 
                AND `products`.`id` = $product_id 
                AND `products`.`store_id` = $store_id 
                AND `products`.`store_type` = 'merchant')

                ";
        $return = $this->db->query($query);

        return ($return->num_rows() > 0) ? $return->row() : FALSE;
    }

    // function get_detail_products($product_id, $store_id) {
    //     $this->db->select('products_principal_stock.id AS stock_id,
    //                         products_principal_stock.product_id,
    //                         products_principal_stock.branch_id,
    //                         products_principal_stock.quantity AS product_quantity,
    //                         products.*,
    //                         merchants.name AS merchant_name,
    //                         brands.name AS brand_name,
    //                         product_image.image')
    //             ->join('products', 'products_principal_stock.product_id = products.id', 'LEFT')
    //             ->join('merchants', 'products_principal_stock.branch_id = merchants.id', 'LEFT')
    //             ->join('brands', 'products.brand = brands.id', 'LEFT')
    //             ->join('product_image', 'product_image.product = products.id AND product_image.primary = 1', 'LEFT')
    //             ->where('products.status', 1)
    //             ->where('products_principal_stock.product_id', $product_id)
    //             ->where('products_principal_stock.branch_id', $store_id);

    //     $query = $this->db->get('products_principal_stock');

    //     return ($query->num_rows() > 0) ? $query->row() : FALSE;
    //     // $this->db->select("pps.id AS stock_id,
    //     //                     pps.branch_id,
    //     //                     pps.product_id,
    //     //                     pps.quantity AS product_quantity,
    //     //                     p.*,
    //     //                     merchants.name AS merchant_name,
    //     //                     brands.name AS brand_name,
    //     //                     pi.image", FALSE)
    //     //             ->join('products p', 'pps.product_id = p.id', 'left')
    //     //             ->join('product_image pi', 'pi.product = p.id AND pi.primary = 1', 'left')
    //     //             ->join('merchants', 'pps.branch_id = merchants.id', 'left')
    //     //             ->join('brands', 'p.brand = brands.id', 'left')
    //     //             ->where('p.status', 1)
    //     //             ->where('pps.quantity > ', 0)
    //     //             ->where('pps.product_id', $product_id)
    //     //             ->where('pps.branch_id', $store_id);
    //     // $query = $this->db->get('products_principal_stock pps');
    //     // return ($query->num_rows() > 0) ? $query->row() : false;
    // }

    // function get_product_by_category($id_category) {
    //     return $this->db->query("SELECT
    //                         products_principal_stock.product_id AS product_id,
    //                         products_principal_stock.branch_id AS store_id,
    //                         products_principal_stock.quantity,
    //                         products.merchant,
    //                         products.name,
    //                         products.category,
    //                         products.brand,
    //                         products.price,
    //                         products.table_description,
    //                         merchants.name AS merchant_name,
    //                         product_image.image
    //                     FROM products_principal_stock
    //                     LEFT JOIN products ON products_principal_stock.product_id = products.id
    //                     LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
    //                     LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
    //                     WHERE products.merchant = 0
    //                     AND products.status = 1
    //                     AND products.category = $id_category
    //                     AND (products_principal_stock.quantity > 0 OR products.preorder = 1)
    //                     ORDER BY RAND()
    //                     LIMIT 3
    //         ");
    // }

    // NEW METHOD
    function get_product_by_category($id_category, $id_product = null, $price = null)
    {
        if ($id_product) {
            $query_where = "AND products.id != $id_product";
        } else {
            $query_where = null;
        }
        if ($price) {
            $where_price = "AND products.price >= $price";
        } else {
            $where_price = NULL;
        }
        return $this->db->query("SELECT
                            products_principal_stock.product_id AS product_id,
                            products_principal_stock.branch_id AS store_id,
                            products_principal_stock.quantity,
                            products.merchant,
                            products.name,
                            products.category,
                            products.brand,
                            products.price,
                            products.table_description,
                            merchants.name AS merchant_name,
                            product_image.image,
                            products.variation,
                            products.price_grosir
                        FROM products_principal_stock
                        LEFT JOIN products ON products_principal_stock.product_id = products.id
                        LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                        LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
                        WHERE products.status = 1
                        AND products.category = $id_category
                        AND products.store_type = 'principal'
                        AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
                        AND (products.table_description != NULL OR products.table_description != '')
                        $query_where
                        $where_price
                        GROUP BY products_principal_stock.branch_id, products_principal_stock.product_id

                        UNION 

                        SELECT products.id AS product_id,
                            products.store_id AS store_id,
                            products.quantity,
                            products.merchant,
                            products.name,
                            products.category,
                            products.brand,
                            products.price,
                            products.table_description,
                            merchants.name AS merchant_name,
                            product_image.image,
                            products.variation,
                            products.price_grosir
                        FROM products
                        LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                        LEFT JOIN merchants ON merchants.id = products.store_id
                        WHERE products.status = 1
                        AND products.category = $id_category
                        AND products.store_type = 'merchant'
                        AND (products.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
                        AND (products.table_description != NULL OR products.table_description != '')
                        $query_where
                        $where_price

                        ORDER BY RAND()
                        LIMIT 2
            ");
    }

    function get_product($id)
    {
        $this->db->select("p.*, pi.image, b.name brand_name, m.name merchant_name, p.quantity", FALSE)
            ->join('product_image pi', 'p.id = pi.product AND primary = 1', 'left')
            ->join('brands b', 'b.id = p.brand', 'left')
            ->join('merchants m', 'm.id = p.merchant', 'left')
            ->where('p.status', 1)
            ->where('p.id', $id);
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_rating($product, $store)
    {
        $this->db->select('IFNULL((SUM(rating_speed) + SUM(rating_service) + SUM(rating_accuracy)) / (COUNT(id) * 3), 0) AS rating,
                            IFNULL(COUNT(id), 0) AS review')
            ->where('product', $product)
            ->where('merchant', $store)
            ->where('status', 1);

        $query = $this->db->get('product_review');
        return ($query->result() > 0) ? $query->row() : FALSE;
    }

    // function get_rating($product) {
    //     $this->db->select('IFNULL((SUM(pr.rating_speed)+SUM(pr.rating_service)+SUM(pr.rating_accuracy))/(COUNT(pr.id)*3),0) rating, IFNULL(COUNT(pr.id),0) review')
    //             ->where('pr.product', $product)
    //             ->where('pr.status', 1);
    //     return $this->db->get('product_review pr')->row();
    // }

    function get_images($id)
    {
        $this->db->where('product', $id);
        //                ->where('primary', 0);
        $query = $this->db->get('product_image');
        return ($query->num_rows() > 0) ? $query : false;
    }

    // function get_option_groups($product, $product_option = 0)
    // {
    //     $this->db->select('og.id group, og.type, og.name group_name, o.id option, o.value option_name, o.color, po.id product_option, po.price, po.weight, po.default')
    //         ->join('product_option_combination poc', 'po.id = poc.product_option', 'left')
    //         ->join('options o', 'poc.option = o.id', 'left')
    //         ->join('option_group og', 'o.group = og.id', 'left')
    //         ->where('po.product', $product)
    //         ->group_by('o.group, po.id')
    //         ->order_by('og.sort_order ASC, o.sort_order ASC, og.name ASC');
    //     if ($product_option) {
    //         $this->db->where('po.id', $product_option);
    //     }
    //     return $this->db->get('product_option po');
    // }

    function get_option_groups($product, $product_option = 0)
    {
        $this->db->select('o.*, po.*, o.id option, o.value option_name, o.type group_name, po.id product_option')
            ->join('product_option_combination poc', 'po.id = poc.product_option', 'left')
            ->join('options o', 'poc.option = o.id', 'left')
            ->where('po.product', $product)
            // ->order_by('o.sort_order ASC, o.sort_order ASC');
            ->order_by('o.type DESC');
        if ($product_option) {
            $this->db->where('po.id', $product_option);
        }
        return $this->db->get('product_option po');
    }

    function get_option_groups_branch($product, $store) {
        $this->db->select('o.*, po.*, o.id option, o.value option_name, o.type group_name, po.id product_option')
            ->join('product_option_combination poc', 'po.id = poc.product_option', 'left')
            ->join('options o', 'poc.option = o.id', 'left')
            ->join('products_principal_stock pps', 'pps.id_option = po.id')
            ->where('po.product', $product)
            ->where('pps.branch_id', $store)
            ->order_by('o.type DESC');
        return $this->db->get('product_option po');
    }

    // function get_related_product($cat_id, $id, $limit = 6, $categoryParent = 0) {
    //     $this->db->select('products.name,
    //                         products.id,
    //                         products.price,
    //                         products.preorder,
    //                         products.promo,
    //                         products.promo_data,
    //                         products.merchant,
    //                         products.viewed,
    //                         products.free_ongkir,
    //                         product_image.image,
    //                         merchants.name AS store_name,
    //                         merchants.id AS store_id')
    //             ->join('products', 'products_principal_stock.product_id = products.id', 'left')
    //             ->join('product_image', 'products.id = product_image.product AND product_image.primary = 1', 'left')
    //             ->join('merchants', 'products_principal_stock.branch_id = merchants.id', 'left')
    //             ->where('products.status !=', 0)
    //             ->where('products.category', $cat_id)
    //             ->where('products.id !=', $id)
    //             ->where('(products_principal_stock.quantity > 0 OR products.preorder = 1)')
    //             ->order_by('RAND()')
    //             ->limit($limit);
    //     $query = $this->db->get('products_principal_stock');

    //     return ($query->num_rows() > 0) ? $query : $this->get_also_boughts($categoryParent, $id, $limit);
    // }

    // NEW METHOD
    function get_related_product($cat_id, $id, $limit = 6, $categoryParent = 0)
    {
        $query = $this->db->query("(SELECT products.name,
                                    products.id,
                                    products.price,
                                    products.preorder,
                                    products.promo,
                                    products.promo_data,
                                    products.merchant,
                                    products.viewed,
                                    products.free_ongkir,
                                    product_image.image,
                                    merchants.name AS store_name,
                                    products_principal_stock.branch_id AS store_id,
                                    products.variation,
                                    products.price_grosir
                                    FROM products_principal_stock
                                    LEFT JOIN products ON products.id = products_principal_stock.product_id
                                    LEFT JOIN merchants ON merchants.id = products_principal_stock.branch_id
                                    LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                    WHERE products.status != 0
                                    AND products.category = $cat_id
                                    AND products.id != $id
                                    AND products.store_type = 'principal'
                                    AND products.store_id != 0
                                    AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
                                    GROUP BY products_principal_stock.branch_id, products_principal_stock.product_id)
                                    
                                    UNION

                                    (SELECT products.name,
                                    products.id,
                                    products.price,
                                    products.preorder,
                                    products.promo,
                                    products.promo_data,
                                    products.merchant,
                                    products.viewed,
                                    products.free_ongkir,
                                    product_image.image,
                                    merchants.name AS store_name,
                                    products.store_id AS store_id,
                                    products.variation,
                                    products.price_grosir
                                    FROM products
                                    LEFT JOIN merchants ON merchants.id = products.store_id
                                    LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                    WHERE products.status != 0
                                    AND products.category = $cat_id
                                    AND products.id != $id
                                    AND products.store_type = 'merchant'
                                    AND products.store_id != 0
                                    AND (products.quantity > 0 OR products.preorder = 1 OR products.variation = 1))

                                    ORDER BY RAND()

                                    LIMIT $limit
        
        ");

        return ($query->num_rows() > 0) ? $query : $this->get_also_boughts($categoryParent, $id, $limit);
    }

    // function get_all_last_seen_product ($user_id = NULL, $ip_address = NULL, $limit = NULL, $stock_id = NULL) {
    //     if ($user_id) {
    //         $this->db->where('product_last_review.user_id', $user_id);
    //     }
    //     else {
    //         $this->db->where('product_last_review.ip_address', $ip_address);
    //     }

    //     if ($stock_id) {
    //         $this->db->where('product_last_review.product_id != ', $stock_id);
    //     }

    //     $this->db->select('product_last_review.id,
    //                         product_last_review.user_id,
    //                         product_last_review.ip_address,
    //                         product_last_review.modified_time,
    //                         products_principal_stock.id AS stock_id,
    //                         products_principal_stock.branch_id AS store_id,
    //                         products_principal_stock.product_id AS product_id,
    //                         merchants.name AS merchant_name,
    //                         products.name,
    //                         products.price,
    //                         products.preorder,
    //                         products.promo,
    //                         products.promo_data,
    //                         products.merchant,
    //                         products.viewed,
    //                         products.free_ongkir,
    //                         product_image.image')
    //             ->join('products_principal_stock', 'product_last_review.product_id = products_principal_stock.id', 'LEFT')
    //             ->join('products', 'products_principal_stock.product_id = products.id', 'LEFT')
    //             ->join('merchants', 'products_principal_stock.branch_id = merchants.id', 'LEFT')
    //             ->join('product_image', 'product_image.product = products.id AND product_image.primary = 1', 'LEFT')
    //             ->where('products.status != ', 0)
    //             ->order_by('product_last_review.modified_time', 'DESC');

    //     if ($limit) {
    //         $this->db->limit($limit);
    //     }

    //     $query = $this->db->get('product_last_review');

    //     return ($query->num_rows() > 0) ? $query : FALSE;
    // }

    /* NEW METHODS */
    function get_all_last_seen_product($user_id = NULL, $ip_address = NULL, $limit = NULL, $stock_id = NULL)
    {

        if ($user_id) {
            $auth = 'product_last_review.user_id = "' . $user_id . '"';
        } else {
            $auth = 'product_last_review.ip_address = "' . $ip_address . '"';
        }

        if ($stock_id) {
            $stock_id = 'product_last_review.product_id != "' . $stock_id . '"';
        } else {
            $stock_id = 'product_last_review.product_id != \'NULL\'';
        }

        $query_sql = "(SELECT product_last_review.id,
                    product_last_review.user_id,
                    product_last_review.ip_address,
                    product_last_review.modified_time,
                    products_principal_stock.id AS stock_id,
                    products_principal_stock.branch_id AS store_id,
                    products_principal_stock.product_id AS product_id,
                    products_principal_stock.quantity AS quantity,
                    merchants.name AS merchant_name,
                    products.name,
                    products.price,
                    products.preorder,
                    products.promo,
                    products.promo_data,
                    products.merchant,
                    products.viewed,
                    products.free_ongkir,
                    product_image.image
                    FROM product_last_review
                    LEFT JOIN products_principal_stock ON products_principal_stock.id = product_last_review.product_id
                    LEFT JOIN products ON products.id = products_principal_stock.product_id
                    LEFT JOIN merchants ON merchants.id = products_principal_stock.branch_id
                    LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                    WHERE products.status != 0
                    AND products.store_type = 'principal'
                    AND $auth
                    AND $stock_id
                    GROUP BY products_principal_stock.branch_id, products_principal_stock.product_id)

                    UNION

                    (SELECT product_last_review.id,
                    product_last_review.user_id,
                    product_last_review.ip_address,
                    product_last_review.modified_time,
                    products.id AS stock_id,
                    products.store_id AS store_id,
                    products.id AS product_id,
                    products.quantity AS quantity,
                    merchants.name AS merchant_name,
                    products.name,
                    products.price,
                    products.preorder,
                    products.promo,
                    products.promo_data,
                    products.merchant,
                    products.viewed,
                    products.free_ongkir,
                    product_image.image
                    FROM product_last_review
                    LEFT JOIN products ON products.id = product_last_review.product_id
                    LEFT JOIN merchants ON merchants.id = products.store_id
                    LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                    WHERE products.status != 0
                    AND products.store_type = 'merchant'
                    AND $auth
                    AND $stock_id)

                    ORDER BY modified_time DESC
                ";

        if ($limit) {
            $query_sql .= " LIMIT $limit";
        }

        $query = $this->db->query($query_sql);

        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    // function last_seen_with_additional($user_id, $ip_address, $limit) {
    //     if ($user_id) {
    //         $auth = 'product_last_review.user_id = "' . $user_id . '"';
    //     }
    //     else {
    //         $auth = 'product_last_review.ip_address = "' . $ip_address . '"';
    //     }
    //     return $this->db->query("SELECT products_principal_stock.branch_id AS store_id,
    //                                     products_principal_stock.product_id AS product_id,
    //                                     merchants.name AS merchant_name,
    //                                     products.name,
    //                                     products.price,
    //                                     products.preorder,
    //                                     products.promo,
    //                                     products.promo_data,
    //                                     products.merchant,
    //                                     products.viewed,
    //                                     products.free_ongkir,
    //                                     product_image.image
    //                             FROM product_last_review LEFT JOIN products_principal_stock
    //                             ON product_last_review.product_id = products_principal_stock.id
    //                             LEFT JOIN products ON products_principal_stock.product_id = products.id
    //                             LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
    //                             LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
    //                             WHERE products.status != 0
    //                             AND $auth

    //                             UNION

    //                             SELECT products_principal_stock.branch_id AS store_id,
    //                                     products_principal_stock.product_id AS product_id,
    //                                     merchants.name AS merchant_name,
    //                                     products.name,
    //                                     products.price,
    //                                     products.preorder,
    //                                     products.promo,
    //                                     products.promo_data,
    //                                     products.merchant,
    //                                     products.viewed,
    //                                     products.free_ongkir,
    //                                     product_image.image
    //                             FROM products_principal_stock LEFT JOIN products ON products_principal_stock.product_id = products.id
    //                             LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
    //                             LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
    //                             WHERE products.status != 0

    //                             LIMIT $limit");
    // }

    // NEW METHOD
    function last_seen_with_additional($user_id, $ip_address, $limit)
    {
        if ($user_id) {
            $auth = 'product_last_review.user_id = "' . $user_id . '"';
        } else {
            $auth = 'product_last_review.ip_address = "' . $ip_address . '"';
        }
        return $this->db->query("SELECT products_principal_stock.branch_id AS store_id,
                                        products_principal_stock.product_id AS product_id,
                                        merchants.name AS merchant_name,
                                        products.name,
                                        products.price,
                                        products.preorder,
                                        products.promo,
                                        products.promo_data,
                                        products.merchant,
                                        products.viewed,
                                        products.free_ongkir,
                                        product_image.image
                                FROM product_last_review LEFT JOIN products_principal_stock
                                ON product_last_review.product_id = products_principal_stock.id
                                LEFT JOIN products ON products_principal_stock.product_id = products.id
                                LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                WHERE products.status != 0
                                AND products.store_type = 'principal'
                                AND $auth
                                GROUP BY products_principal_stock.branch_id, products_principal_stock.product_id
                                
                                UNION

                                SELECT products_principal_stock.branch_id AS store_id,
                                        products_principal_stock.product_id AS product_id,
                                        merchants.name AS merchant_name,
                                        products.name,
                                        products.price,
                                        products.preorder,
                                        products.promo,
                                        products.promo_data,
                                        products.merchant,
                                        products.viewed,
                                        products.free_ongkir,
                                        product_image.image
                                FROM products_principal_stock LEFT JOIN products ON products_principal_stock.product_id = products.id
                                LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                WHERE products.status != 0
                                AND products.store_type = 'principal'
                                GROUP BY products_principal_stock.branch_id, products_principal_stock.product_id

                                UNION

                                SELECT products.store_id AS store_id,
                                        products.id AS product_id,
                                        merchants.name AS merchant_name,
                                        products.name,
                                        products.price,
                                        products.preorder,
                                        products.promo,
                                        products.promo_data,
                                        products.merchant,
                                        products.viewed,
                                        products.free_ongkir,
                                        product_image.image
                                FROM product_last_review LEFT JOIN products ON product_last_review.product_id = products.id
                                LEFT JOIN merchants ON merchants.id = products.store_id
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                WHERE products.status != 0
                                AND products.store_type = 'merchant'
                                AND $auth

                                UNION

                                SELECT products.store_id AS store_id,
                                        products.id AS product_id,
                                        merchants.name AS merchant_name,
                                        products.name,
                                        products.price,
                                        products.preorder,
                                        products.promo,
                                        products.promo_data,
                                        products.merchant,
                                        products.viewed,
                                        products.free_ongkir,
                                        product_image.image
                                FROM products 
                                LEFT JOIN merchants ON merchants.id = products.store_id
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                WHERE products.status != 0
                                AND products.store_type = 'merchant'

                                LIMIT $limit");
    }

    function delete_product_last_view($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('product_last_review');
    }

    //function get_combination_images($product) {
    //$images = array();
    //$this->db->select("poi.product_image, poi.product_option, pi.image, pi.primary")
    //          ->join('product_image pi', 'pi.id = poi.product_image', 'left')
    //          ->where_in('poi.product_option', 'SELECT id FROM product_option	WHERE product = ' . $product);
    //$query = $this->db->get('product_option_image poi');
    //if ($query->num_rows() > 0) {
    //   foreach ($query->result_array() as $image) {
    //        $images[$image['product_option']][] = $image;
    //   }
    //}
    //return $images;
    //}

    function get_product_option_id($product, $options)
    {
        $this->db->select('poc.product_option')
            ->join('product_option po', 'po.id = poc.product_option', 'left')
            ->where('po.product', $product)
            ->where('po.status', 1)
            ->where_in('option', $options)
            ->group_by('poc.product_option')
            ->having('COUNT(poc.product_option)', count($options));
        $query = $this->db->get('product_option_combination poc');
        return ($query->num_rows() > 0) ? $query->row()->product_option : false;
    }

    function get_product_option_images($product_option)
    {
        $this->db->select("pi.id, pi.image")
            ->join('product_image pi', 'pi.id = poi.product_image', 'left')
            ->where('poi.product_option', $product_option)
            ->order_by('pi.sort_order asc, pi.primary desc');
        $query = $this->db->get('product_option_image poi');
        return ($query->num_rows() > 0) ? $query : false;
    }

    function get_relations($category, $product, $limit = 10, $categoryParent = 0)
    {
        $this->db->select("p.id, pi.image, price, price_old, discount, name", FALSE)
            ->join('product_image pi', 'p.id = pi.product AND primary = 1', 'left')
            ->where("category", $category)
            ->where('p.id !=', $product)
            ->where('p.status', 1)
            ->order_by('RAND()')
            ->limit($limit);
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query : $this->get_also_boughts($categoryParent, $product, $limit);
    }

    // function get_also_boughts($category, $product, $limit = 6) {
    //     $this->db->select('products.name,
    //                         products.id,
    //                         products.price,
    //                         products.preorder,
    //                         products.discount,
    //                         products.promo,
    //                         products.promo_data,
    //                         products.merchant,
    //                         products.viewed,
    //                         products.free_ongkir,
    //                         product_image.image,
    //                         merchants.name AS store_name,
    //                         merchants.id AS store_id')
    //             ->join('products', 'products_principal_stock.product_id = products.id', 'LEFT')
    //             ->join('product_image', 'product_image.product = products.id AND product_image.primary = 1', 'LEFT')
    //             ->join('merchants', 'products_principal_stock.branch_id = merchants.id', 'FALSE')
    //             ->where('products.category IN (SELECT category FROM category_path WHERE path = ' . $category . ')')
    //             ->where('products_principal_stock.product_id !=', $product)
    //             ->where('products.status', 1)
    //             ->where('(products_principal_stock.quantity > 0 OR products.preorder = 1)')
    //             ->order_by('RAND()')
    //             ->limit($limit);
    //     $query = $this->db->get('products_principal_stock');

    //     return ($query->num_rows() > 0) ? $query : FALSE;

    //     // $this->db->select("p.id, pi.image, price_old, price, discount, name", FALSE)
    //     //         ->join('product_image pi', 'p.id = pi.product AND primary = 1', 'left')
    //     //         ->where("category IN (SELECT category FROM category_path WHERE path = $category)")
    //     //         ->where('p.id !=', $product)
    //     //         ->where('p.status =', 1)
    //     //         ->order_by('RAND()')
    //     //         ->limit($limit);
    // }

    // NEW METHOD
    function get_also_boughts($category, $product, $limit = 6)
    {
        $query = "
        (SELECT products.name,
                products.id,
                products.price,
                products.preorder,
                products.promo,
                products.promo_data,
                products.merchant,
                products.viewed,
                products.free_ongkir,
                product_image.image,
                merchants.name AS store_name,
                products_principal_stock.branch_id AS store_id,
                products.variation,
                products.price_grosir
                FROM products_principal_stock
                LEFT JOIN products ON products.id = products_principal_stock.product_id
                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                LEFT JOIN merchants ON merchants.id = products_principal_stock.branch_id
                WHERE products.category IN (SELECT category FROM category_path WHERE path = $category)
                AND products_principal_stock.product_id != $product
                AND products.status = 1
                AND products.store_type = 'principal'
                AND products.store_id != 0
                AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
                GROUP BY products_principal_stock.branch_id, products_principal_stock.product_id)
        UNION
        (SELECT products.name,
                products.id,
                products.price,
                products.preorder,
                products.promo,
                products.promo_data,
                products.merchant,
                products.viewed,
                products.free_ongkir,
                product_image.image,
                merchants.name AS store_name,
                merchants.id AS store_id,
                products.variation,
                products.price_grosir
                FROM products
                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                LEFT JOIN merchants ON merchants.id = products.store_id
                WHERE products.category IN (SELECT category FROM category_path WHERE path = $category)
                AND products.id != $product
                AND products.status = 1
                AND products.store_type = 'merchant'
                AND products.store_id != 0
                AND (products.quantity > 0 OR products.preorder = 1 OR products.variation = 1))
        ORDER BY RAND()
        LIMIT $limit
        ";
        $query = $this->db->query($query);
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function get_products_by_id($products = array())
    {
        $this->db->select("p.id, pi.image, price_old, price, discount, name", FALSE)
            ->join('product_image pi', 'p.id = pi.product AND primary = 1', 'left')
            ->where_in('p.id', $products)
            ->where('p.status', 1);
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query : false;
    }

    function get_product_features($product)
    {
        $this->db->select('pf.product, f.id, type, name, pf.value')
            ->join('features f', 'pf.feature = f.id', 'left')
            ->group_by('pf.feature')
            ->order_by('f.sort_order ASC')
            ->where('pf.product', $product);
        $query = $this->db->get('product_feature pf');
        return ($query->num_rows() > 0) ? $query : false;
    }

    function get_product_feature_variants($product, $feature)
    {
        $this->db->select('pfv.*')
            ->join('product_feature_variant pfv', 'ptf.feature = pfv.feature AND ptf.variant = pfv.id', 'left')
            ->where('ptf.product', $product)
            ->where('ptf.feature', $feature);
        $query = $this->db->get('product_to_feature ptf');
        return ($query->num_rows() > 0) ? $query : false;
    }

    //    function get_product_options($product) {
    //        $this->db->select('po.*, o.name, o.type')
    //                ->join('options o', 'po.option = o.id')
    //                ->where('po.product', $product);
    //        $query = $this->db->get('product_option po');
    //        return($query->num_rows() > 0) ? $query : false;
    //    }
    //    function get_product_option_variants($product,$option) {
    //        $this->db->select('pov.*, ov.value, ov.color')
    //                ->join('option_variant ov', 'pov.option_variant = ov.id')
    //                ->where('pov.product', $product)
    //                ->where('pov.product_option', $option);
    //        $query = $this->db->get('product_option_variant pov');
    //        return($query->num_rows() > 0) ? $query : false;
    //    }
    //    
    //    function get_product_option_variant_full($option, $variant){
    //        $this->db->select('pov.id, pov.product_option, pov.option_variant, pov.price, pov.weight, po.option, o.type, o.name, ov.value, ov.color')
    //                ->join('product_option po','po.id = pov.product_option','left')
    //                ->join('options o','o.id = po.option','left')
    //                ->join('option_variant ov','ov.id = pov.option_variant','left')
    //                ->where('pov.product_option',$option)
    //                ->where('pov.option_variant',$variant);
    //        $query = $this->db->get('product_option_variant pov');
    //        return($query->num_rows() > 0) ? $query->row_array() : false;
    //    }

    function get_reviews($product, $merchant)
    {
        $customer = ($this->ion_auth->logged_in()) ? ' AND prl.customer = ' . $this->data['user']->id . ' ' : '';
        $this->db->select('pr.id, pr.rating, pr.title, pr.description, pr.like, pr.dislike, pr.date_added, c.fullname name, prl.likedislike')
            ->join('customers c', 'pr.customer = c.id', 'left')
            ->join('product_review_likedislike prl', 'pr.id = prl.product_review' . $customer, 'left')
            ->where('pr.product', $product)
            ->where('pr.merchant', $merchant)
            ->where('pr.status', 1)
            ->order_by('pr.date_added DESC');
        $query = $this->db->get('product_review pr');
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function get_review_headers($product)
    {
        $query = $this->db->query("SELECT 5 rating, COUNT(id) total FROM product_review WHERE rating = 5 AND product = $product AND status = 1 UNION
SELECT 4 rating, COUNT(id) total FROM product_review WHERE rating = 4 AND product = $product AND status = 1 UNION
SELECT 3 rating, COUNT(id) total FROM product_review WHERE rating = 3 AND product = $product AND status = 1 UNION
SELECT 2 rating, COUNT(id) total FROM product_review WHERE rating = 2 AND product = $product AND status = 1 UNION
SELECT 1 rating, COUNT(id) total FROM product_review WHERE rating = 1 AND product = $product AND status = 1");
        return $query;
    }

    function add_viewed($product)
    {
        $this->db->set('viewed', 'viewed + ' . 1, FALSE);
        $this->db->where('id', $product);
        $this->db->update('products');
        return true;
    }

    function get_merchants($product)
    {
        $this->db->select('m.id, pm.quantity, pm.price_old, pm.price, pm.discount, m.name name, m.district districts, c.name city')
            ->join('merchants m', 'm.id = pm.merchant AND m.status = 1', 'left')
            ->join('cities c', 'c.id = m.city', 'left')
            ->where('pm.product', $product)
            ->where('pm.status', 1);
        return $this->db->get('product_merchant pm');
    }

    function price_in_level($product, $group, $qty)
    {
        $this->db->select('pplg.price')
            ->join('product_price_level ppl', 'ppl.id = pplg.product_price_level', 'left')
            ->where('merchant_group', $group)
            ->where('product', $product)
            ->where('min_qty <=', $qty)
            ->order_by('min_qty', 'asc')
            ->limit(2);
        $query = $this->db->get('product_price_level_groups pplg');
        return ($query->num_rows() > 0) ? $query->last_row()->price : false;
    }

    function product_price_in_level ($product, $qty) {
        $this->db->select('price')
                 ->where('product', $product)
                 ->where('min_qty <=', $qty)
                 ->order_by('min_qty asc');
                 // ->limit(2);
        $query = $this->db->get('product_price_level');
        return ($query->num_rows() > 0) ? $query->last_row()->price : false;
    }

    function get_product_reseller($product, $qty) {
        $this->db->select('price')
                 ->where('product', $product)
                 ->where('min_qty <=', $qty)
                 ->order_by('min_qty asc');
        $query = $this->db->get('product_price_reseller');
        return ($query->num_rows() > 0) ? $query->last_row()->price : false;
    }

    function price_level_group($product, $group = 0)
    {
        $this->db->select('pclg.*, pcl.min_qty, pcl.product')
            ->join('product_price_level pcl', 'pcl.id = pclg.product_price_level', 'left')
            ->where_in('product_price_level', '(SELECT id FROM product_price_level WHERE product = ' . $product . ')', false)
            ->where('merchant_group', $group)
            ->order_by('pcl.min_qty asc');
        return $this->db->get('product_price_level_groups pclg');
    }

    function product_price_level($product) {
        $this->db->select('*')
                 ->where('product', $product)
                 ->order_by('min_qty asc');
        return $this->db->get('product_price_level');
    }

    function get_districts($key)
    {
        $this->db->like('name', $key);
        $query = $this->db->select('id, name as text')
            ->get("districts");
        $json = $query->result();
        return ($json);
    }

    function get_merchants_($product, $quantity, $pricing_level = 1)
    {
        $location = $this->session->userdata('location');
        return $this->db->query("SELECT *, (6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) AS distance FROM merchants WHERE status = 1 AND (type = 'merchant' OR type = 'distributor') AND id IN (SELECT merchant FROM product_merchant WHERE quantity >= $quantity AND product = $product " . ($pricing_level ? "AND pricing_level = 1" : "") . ") HAVING distance < (CASE
        WHEN type='merchant' THEN 40
        ELSE 200
        END ) ORDER BY distance LIMIT 5");
        //        return $this->db->get('orders o');
    }

    function get_products_merchant($product, $type, $merchant)
    {
        if ($type == 'merchant') {
            $query = "SELECT products.store_id AS store_id,
                                        products.id AS product_id,
                                        merchants.name AS merchant_name,
                                        products.name,
                                        products.price,
                                        products.preorder,
                                        products.promo,
                                        products.promo_data,
                                        products.merchant,
                                        products.viewed,
                                        products.free_ongkir,
                                        product_image.image,
                                        products.quantity AS quantity,
                                        products.variation,
                                        products.price_grosir
                FROM products
                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                LEFT JOIN merchants ON merchants.id = products.store_id
                WHERE products.store_id = $merchant
                AND products.id != $product
                AND products.status = 1
                AND products.store_type = 'merchant'
                AND products.store_id != 0
                AND (products.quantity > 0 OR products.preorder = 1 OR products.variation = 1) ORDER BY RAND() LIMIT 6";
        } else {
            $query = "SELECT products_principal_stock.branch_id AS store_id,
                                        products_principal_stock.product_id AS product_id,
                                        merchants.name AS merchant_name,
                                        products.name,
                                        products.price,
                                        products.preorder,
                                        products.promo,
                                        products.promo_data,
                                        products.merchant,
                                        products.viewed,
                                        products.free_ongkir,
                                        product_image.image,
                                        products_principal_stock.quantity AS quantity,
                                        products.variation,
                                        products.price_grosir
                FROM products_principal_stock
                LEFT JOIN products ON products.id = products_principal_stock.product_id
                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                LEFT JOIN merchants ON merchants.id = products_principal_stock.branch_id
                WHERE products_principal_stock.branch_id = $merchant
                AND products_principal_stock.product_id != $product
                AND products.status = 1
                AND products.store_type = 'principal'
                AND products.store_id != 0
                AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
                GROUP BY products_principal_stock.branch_id, products_principal_stock.product_id
                ORDER BY RAND() LIMIT 6";
        }
        return $this->db->query($query);
    }

    function check_option($option, $product_option) {
        $this->db->select('poc.*')
                ->join('product_option po', 'poc.product_option = po.id', 'left')
                ->where('po.status', 1)
                ->where('poc.product_option', $product_option)
                ->where('poc.option', $option);
        $query =  $this->db->get('product_option_combination poc');
        return ($query) ? TRUE : FALSE; 
    }
}
