<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Search_model extends CI_Model {

    // function getProducts($word, $merchant, $merchant_group, $sort = 'popular', $limit = 20, $start = 0) {
    //     $query_order = 'ORDER BY ';
    //     switch ($sort) {
    //         default:
    //         case 'popular':
    //             $query_order .= 'products.viewed DESC';
    //             break;
    //         case 'newest' :
    //             $query_order .= 'products.date_added DESC';
    //             break;
    //         case 'lowprice' :
    //             $query_order .= 'price ASC';
    //             break;
    //         case 'highprice' :
    //             $query_order .= 'price DESC';
    //             break;
    //         case 'nameAZ' :
    //             $query_order .= 'products.name ASC';
    //             break;
    //         case 'nameZA' :
    //             $query_order .= 'products.name DESC';
    //             break;
    //     }
    //     $query_limit = '';
    //     if ($limit) {
    //         $query_limit = 'LIMIT ' . $start . ',' . $limit;
    //     }
    //     $merchant = implode(',', $merchant);
    //     $where = '';

    //     $location = $this->session->userdata('location');


    //     return $this->db->query("SELECT * FROM (
    //                                     SELECT products.name,
    //                                             products_principal_stock.product_id,
    //                                             products_principal_stock.branch_id AS store_id,
    //                                             products.price,
    //                                             products.discount,
    //                                             products.preorder,
    //                                             products.promo,
    //                                             products.promo_data,
    //                                             products.short_description,
    //                                             products.code,
    //                                             products.category,
    //                                             products.brand,
    //                                             product_image.image,
    //                                             products.viewed,
    //                                             products.date_added,
    //                                             products.meta_keyword,
    //                                             products.merchant,
    //                                             merchants.name AS merchant_name,
    //                                             merchants.type AS merchant_type,
    //                                             categories.name AS cat_name,
    //                                             (6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) AS distance
    //                                     FROM products_principal_stock
    //                                     LEFT JOIN products ON products_principal_stock.product_id = products.id
    //                                     LEFT JOIN product_price ON product_price.product = products.id AND product_price.merchant_group = $merchant_group
    //                                     LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
    //                                     LEFT JOIN categories ON categories.id = products.category
    //                                     LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
    //                                     WHERE products.merchant = 0
    //                                     AND products.status = 1
    //                                     AND (products_principal_stock.quantity > 0 OR products.preorder = 1)
                                        
    //                                     UNION
                                        
    //                                     SELECT products.name,
    //                                             products_principal_stock.product_id,
    //                                             products_principal_stock.branch_id AS store_id,
    //                                             products.price,
    //                                             products.discount,
    //                                             products.preorder,
    //                                             products.promo,
    //                                             products.promo_data,
    //                                             products.short_description,
    //                                             products.code,
    //                                             products.category,
    //                                             products.brand,
    //                                             product_image.image,
    //                                             products.viewed,
    //                                             products.date_added,
    //                                             products.meta_keyword,
    //                                             products.merchant,
    //                                             merchants.name AS merchant_name,
    //                                             merchants.type AS merchant_type,
    //                                             categories.name AS cat_name,
    //                                             (6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) AS distance
    //                                     FROM products_principal_stock
    //                                     LEFT JOIN products ON products_principal_stock.product_id = products.id
    //                                     LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
    //                                     LEFT JOIN categories ON categories.id = products.category
    //                                     LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
    //                                     WHERE merchants.id IN (SELECT id FROM merchants WHERE id IN ($merchant) AND status = 1)
    //                                     AND products.status = 1) products
                                        
    //                                     WHERE (products.code LIKE '%$word%' OR products.name LIKE '%$word%' OR products.meta_keyword REGEXP '$word' OR cat_name LIKE '%$word%')
    //                                     AND (products.merchant_type = 'merchant' OR products.merchant_type = 'branch' OR products.merchant_type = 'distributor')
    //                                     HAVING products.distance < (CASE WHEN products.merchant_type = 'merchant' THEN 40 ELSE 200 END)
    //                                     $where $query_order $query_limit");
    // }

    function get_products($word, $sort = '', $limit = 20, $start = 0) {
        $query_order = 'ORDER BY ';
        switch ($sort) {
            default:
                $query_order .= 'distance ASC';
                break;
            case 'popular':
                $query_order .= 'viewed DESC';
                break;
            case 'newest' :
                $query_order .= 'date_added DESC';
                break;
            case 'lowprice' :
                $query_order .= 'price ASC';
                break;
            case 'highprice' :
                $query_order .= 'price DESC';
                break;
            case 'nameAZ' :
                $query_order .= 'name ASC';
                break;
            case 'nameZA' :
                $query_order .= 'name DESC';
                break;
            case 'distance' :
                $query_order .= 'distance ASC';
                break;  
        }
        $query_limit = '';
        if ($limit) {
            $query_limit = 'LIMIT ' . $start . ',' . $limit;
        }
        $where = '';
        $location = $this->session->userdata('location');
        if($location) {
            return $this->db->query("SELECT * FROM 
                                (SELECT products.name AS name,
                                        products.id AS product_id,
                                        products.store_id AS store_id,
                                        products.price,
                                        products.discount,
                                        products.preorder,
                                        products.promo,
                                        products.promo_data,
                                        products.short_description,
                                        products.code,
                                        products.category,
                                        products.brand,
                                        product_image.image,
                                        products.viewed AS viewed,
                                        products.date_added AS date_added,
                                        products.meta_keyword,
                                        products.merchant,
                                        products.free_ongkir,
                                        merchants.name AS merchant_name,
                                        merchants.type AS merchant_type,
                                        categories.name AS cat_name,
                                        (6971 * acos(cos(radians($location[lat])) * cos(radians(merchants.lat)) * cos(radians(merchants.lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(merchants.lat)))) AS distance,
                                        products.variation,
                                        products.price_grosir
                                FROM products
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                LEFT JOIN merchants ON merchants.id = products.store_id
                                LEFT JOIN categories ON categories.id = products.category
                                WHERE products.status = 1
                                AND products.store_type = 'merchant'
                                AND (products.quantity > 0 OR products.preorder = 1 OR products.variation = 1)

                                UNION

                                SELECT products.name AS name,
                                        products.id AS product_id,
                                        products_principal_stock.branch_id AS store_id,
                                        products.price,
                                        products.discount,
                                        products.preorder,
                                        products.promo,
                                        products.promo_data,
                                        products.short_description,
                                        products.code,
                                        products.category,
                                        products.brand,
                                        product_image.image,
                                        products.viewed AS viewed,
                                        products.date_added AS date_added,
                                        products.meta_keyword,
                                        products.merchant,
                                        products.free_ongkir,
                                        merchants.name AS merchant_name,
                                        merchants.type AS merchant_type,
                                        categories.name AS cat_name,
                                        (6971 * acos(cos(radians($location[lat])) * cos(radians(merchants.lat)) * cos(radians(merchants.lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(merchants.lat)))) AS distance,
                                        products.variation,
                                        products.price_grosir
                                FROM products_principal_stock
                                LEFT JOIN products ON products.id = products_principal_stock.product_id
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                LEFT JOIN merchants ON merchants.id = products_principal_stock.branch_id
                                LEFT JOIN categories ON categories.id = products.category
                                LEFT JOIN product_option ON CASE WHEN products_principal_stock.id_option THEN product_option.id = products_principal_stock.id_option ELSE 0 END
                                WHERE products.status = 1
                                AND products.store_type = 'principal'
                                AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
                                AND CASE WHEN products_principal_stock.id_option != '' AND products.variation = 1 THEN product_option.default = 1 ELSE 1 END) products

                                WHERE (products.code LIKE '%$word%' OR products.name LIKE '%$word%' OR products.meta_keyword REGEXP '$word' OR cat_name LIKE '%$word%' OR merchant_name LIKE '%$word%')
                                $query_order
                                $query_limit

                                        ");
        } else {
            return $this->get_also_products($word, $sort , $limit, $start);
        }
    }

    function get_rating ($product, $store) {
        $this->db->select('IFNULL((SUM(rating_speed) + SUM(rating_service) + SUM(rating_accuracy)) / (COUNT(id) * 3), 0) AS rating,
                            IFNULL(COUNT(id), 0) AS review')
                ->where('product', $product)
                ->where('merchant', $store)
                ->where('status', 1);

        $query = $this->db->get('product_review');
        return ($query->result() > 0) ? $query->row() : FALSE;
    }

    function get_suggest($keyword) {
        $query = $this->db->query("
                SELECT products.name,
                        products.id AS product_id,
                        products_principal_stock.branch_id AS store_id,
                        products.viewed AS viewed
                FROM products_principal_stock
                LEFT JOIN products ON products.id = products_principal_stock.product_id
                LEFT JOIN categories ON categories.id = products.category
                WHERE products.status = 1
                AND products_principal_stock.quantity > 0
                AND (products.name LIKE '%$keyword%' OR products.meta_keyword REGEXP '$keyword' OR categories.name LIKE '%$keyword%')
                AND products.store_type = 'principal'
                
                UNION 

                SELECT products.name,
                        products.id AS product_id,
                        products.store_id AS store_id,
                        products.viewed AS viewed
                FROM products
                LEFT JOIN categories ON categories.id = products.category
                WHERE products.status = 1
                AND products.quantity > 0
                AND (products.name LIKE '%$keyword%' OR products.meta_keyword REGEXP '$keyword' OR categories.name LIKE '%$keyword%')
                AND products.store_type = 'merchant'

                ORDER BY viewed DESC
                LIMIT 5
        ");

        return $query;
    }

    // function get_suggest($keyword) {
    //     return $this->db->query("SELECT * FROM products WHERE status = 1 AND name LIKE '%$keyword%' ORDER BY viewed DESC LIMIT 5");
    // }

    private function get_also_products($word, $sort = '', $limit = 20, $start = 0) {
        $query_order = 'ORDER BY ';
        switch ($sort) {
            default:
                $query_order .= 'viewed ASC';
                break;
            case 'popular':
                $query_order .= 'viewed DESC';
                break;
            case 'newest' :
                $query_order .= 'date_added DESC';
                break;
            case 'lowprice' :
                $query_order .= 'price ASC';
                break;
            case 'highprice' :
                $query_order .= 'price DESC';
                break;
            case 'nameAZ' :
                $query_order .= 'name ASC';
                break;
            case 'nameZA' :
                $query_order .= 'name DESC';
                break;
        }
        $query_limit = '';
        if ($limit) {
            $query_limit = 'LIMIT ' . $start . ',' . $limit;
        }
        return $this->db->query("(SELECT products.name AS name,
                                        products.id AS product_id,
                                        products.store_id AS store_id,
                                        products.price,
                                        products.discount,
                                        products.preorder,
                                        products.promo,
                                        products.promo_data,
                                        products.short_description,
                                        products.code AS code,
                                        products.category,
                                        products.brand,
                                        product_image.image,
                                        products.viewed AS viewed,
                                        products.date_added AS date_added,
                                        products.meta_keyword AS meta_keyword,
                                        products.merchant,
                                        products.free_ongkir,
                                        merchants.name AS merchant_name,
                                        merchants.type AS merchant_type,
                                        categories.name AS cat_name,
                                        products.variation,
                                        products.price_grosir
                                        FROM products
                                        LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                        LEFT JOIN merchants ON merchants.id = products.store_id
                                        LEFT JOIN categories ON categories.id = products.category
                                        WHERE products.status = 1
                                        AND products.store_type = 'merchant'
                                        AND (products.quantity > 0 OR products.preorder = 1 OR products.variation)
                                        AND (products.code LIKE '%$word%' OR products.name LIKE '%$word%' OR products.meta_keyword REGEXP '$word' OR categories.name LIKE '%$word%' OR merchants.name LIKE '%$word%'))
                                        
                                        UNION
                                        
                                        (SELECT products.name AS name,
                                                products.id AS product_id,
                                                products_principal_stock.branch_id AS store_id,
                                                products.price,
                                                products.discount,
                                                products.preorder,
                                                products.promo,
                                                products.promo_data,
                                                products.short_description,
                                                products.code AS code,
                                                products.category,
                                                products.brand,
                                                product_image.image,
                                                products.viewed AS viewed,
                                                products.date_added AS date_added,
                                                products.meta_keyword AS meta_keyword,
                                                products.merchant,
                                                products.free_ongkir,
                                                merchants.name AS merchant_name,
                                                merchants.type AS merchant_type,
                                                categories.name AS cat_name,
                                                products.variation,
                                                products.price_grosir
                                        FROM products_principal_stock
                                        LEFT JOIN products ON products.id = products_principal_stock.product_id
                                        LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                        LEFT JOIN merchants ON merchants.id = products_principal_stock.branch_id
                                        LEFT JOIN categories ON categories.id = products.category
                                        LEFT JOIN product_option ON CASE WHEN products_principal_stock.id_option THEN product_option.id = products_principal_stock.id_option ELSE 0 END
                                        WHERE products.status = 1
                                        AND products.store_type = 'principal'
                                        AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation)
                                        AND CASE WHEN products_principal_stock.id_option != '' AND products.variation = 1 THEN product_option.default = 1 ELSE 1 END
                                        AND (products.code LIKE '%$word%' OR products.name LIKE '%$word%' OR products.meta_keyword REGEXP '$word' OR categories.name LIKE '%$word%' OR merchants.name LIKE '%$word%'))

                                        $query_order
                                        $query_limit
                                        ");
    }

}
