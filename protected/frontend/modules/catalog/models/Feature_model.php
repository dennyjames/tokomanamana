<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Feature_model extends CI_Model {

    function getAllFeatures($start = 0, $limit, $sort, $order, $name) {
        $this->db->select("pf.id, pfd.name", FALSE)
                ->join('product_feature_description pfd', 'pf.id = pfd.id', 'left');

        if ($start && $limit)
            $this->db->limit($limit, $start);
        if ($name)
            $this->db->where("(pfd.name LIKE '%$name%')");
        if ($sort && $order)
            $this->db->order_by($sort, $order);

        $query = $this->db->get('product_features pf');
        return($query->num_rows() > 0) ? $query : false;
    }

    function getFeature($id) {
        $this->db->select("pf.id, pf.type, pfd.name", FALSE)
                ->join('product_feature_description pfd', 'pf.id = pfd.id', 'left')
                ->where('pf.id',$id);
        $query = $this->db->get('product_features pf');
        return($query->num_rows() > 0) ? $query->row() : false;
    }

    function count($name) {
        if ($name)
            $this->db->like('name', $name);
        $this->db->join('product_feature_description pfd', 'pfd.id = pf.id', 'left');
        $query = $this->db->count_all_results('product_features pf');
        return $query;
    }

}
