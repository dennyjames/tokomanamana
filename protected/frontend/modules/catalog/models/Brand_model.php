<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Brand_model extends CI_Model {

    function getAllBrands($start = 0, $limit, $sort, $order, $name) {
        $this->db->select("pb.id, pbd.name", FALSE)
                ->join('product_brand_description pbd', 'pb.id = pbd.id', 'left');

        if ($start && $limit)
            $this->db->limit($limit, $start);
        if ($name)
            $this->db->where("(pbd.name LIKE '%$name%')");
        if ($sort && $order)
            $this->db->order_by($sort, $order);

        $query = $this->db->get('product_brands pb');
        return($query->num_rows() > 0) ? $query : false;
    }

    function getBrand($id) {
        $this->db->select("pb.id, pb.image, pbd.name, pbd.description", FALSE)
                ->join('product_brand_description pbd', 'pb.id = pbd.id', 'left')
                ->where('pb.id',$id);
        $query = $this->db->get('product_brands pb');
        return($query->num_rows() > 0) ? $query->row() : false;
    }

    function count($name) {
        if ($name)
            $this->db->like('name', $name);
        $this->db->join('product_brand_description pbd', 'pbd.id = pb.id', 'left');
        $query = $this->db->count_all_results('product_brands pb');
        return $query;
    }

    function get_products_brand ($id, $sort = 'popular', $limit = 20, $start = 0) {
        $query_order = 'ORDER BY ';
        switch ($sort) {
            default:
            case 'popular':
                $query_order .= 'products.viewed DESC';
                break;
            case 'newest' :
                $query_order .= 'products.date_added DESC';
                break;
            case 'lowprice' :
                $query_order .= 'price ASC';
                break;
            case 'highprice' :
                $query_order .= 'price DESC';
                break;
            case 'nameAZ' :
                $query_order .= 'products.name ASC';
                break;
            case 'nameZA' :
                $query_order .= 'products.name DESC';
                break;
        }
        $query_limit = '';
        if ($limit) {
            $query_limit = 'LIMIT ' . $start . ',' . $limit;
        }

        return $this->db->query("SELECT products.name,
                                            products_principal_stock.product_id,
                                            products_principal_stock.branch_id AS store_id,
                                            products.price,
                                            products.discount,
                                            products.preorder,
                                            products.promo,
                                            products.promo_data,
                                            products.short_description,
                                            products.code,
                                            products.category,
                                            products.brand,
                                            product_image.image,
                                            products.viewed,
                                            products.date_added,
                                            products.meta_keyword,
                                            products.merchant,
                                            merchants.name AS merchant_name,
                                            merchants.type AS merchant_type
                                    FROM products_principal_stock
                                    LEFT JOIN products ON products_principal_stock.product_id = products.id
                                    LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                    LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
                                    WHERE products.status != 0
                                    AND (products_principal_stock.quantity > 0 OR products.preorder = 1)
                                    AND products.brand = $id
                                    $query_order
                                    $query_limit");
        // return ($query->num_rows() > 0) ? $query : FALSE;
    }

}
