<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Last_seen_model extends CI_Model {

    function get_last_seen_product ($user_id = NULL, $ip_address = NULL, $limit = NULL, $start = NULL) {
        if ($user_id) {
            $this->db->where('product_last_review.user_id', $user_id);
        }
        else {
            $this->db->where('product_last_review.ip_address', $ip_address);
        }

        $this->db->select('product_last_review.id,
                            product_last_review.user_id,
                            product_last_review.ip_address,
                            product_last_review.modified_time,
                            products_principal_stock.id AS stock_id,
                            products_principal_stock.branch_id AS store_id,
                            products_principal_stock.product_id AS product_id,
                            merchants.name AS merchant_name,
                            products.name,
                            products.price,
                            products.preorder,
                            products.promo,
                            products.promo_data,
                            products.merchant,
                            product_image.image')
                ->join('products_principal_stock', 'product_last_review.product_id = products_principal_stock.id', 'LEFT')
                ->join('products', 'products_principal_stock.product_id = products.id', 'LEFT')
                ->join('merchants', 'products_principal_stock.branch_id = merchants.id', 'LEFT')
                ->join('product_image', 'product_image.product = products.id AND product_image.primary = 1', 'LEFT')
                ->where('products.status != ', 0);
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        
        $this->db->order_by('product_last_review.modified_time', 'DESC');

        $query = $this->db->get('product_last_review');

        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function get_rating ($product, $store) {
        $this->db->select('IFNULL((SUM(rating_speed) + SUM(rating_service) + SUM(rating_accuracy)) / (COUNT(id) * 3), 0) AS rating,
                            IFNULL(COUNT(id), 0) AS review')
                ->where('product', $product)
                ->where('merchant', $store)
                ->where('status', 1);

        $query = $this->db->get('product_review');
        return ($query->result() > 0) ? $query->row() : FALSE;
    }
}