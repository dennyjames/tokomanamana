<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category_model extends CI_Model {

    function get_categories($parent = 0) {
        $this->db->select('id, name, image, icon')
                ->where('parent', $parent)
                ->order_by('sort_order')
                ->where('active', 1);
        $query = $this->db->get('categories');
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function getCategory($id) {
        $this->db->select("c.*, GROUP_CONCAT(cp.path ORDER BY cp.level SEPARATOR ',') path", FALSE)
                ->join('category_path cp', 'cp.category = c.id', 'left')
                ->where('active', 1)
                ->where('c.id', $id);
        $query = $this->db->get('categories c');
        return ($query->num_rows() > 0) ? $query->row() : FALSE;
    }

    function getProducts($category, $merchant, $merchant_group, $sort = 'popular', $limit = 20, $start = 0) {
        $query_order = 'ORDER BY ';
        switch ($sort) {
            default:
            case 'popular':
                $query_order .= 'viewed DESC';
                break;
            case 'newest' :
                $query_order .= 'date_added DESC';
                break;
            case 'lowprice' :
                $query_order .= 'price ASC';
                break;
            case 'highprice' :
                $query_order .= 'price DESC';
                break;
            case 'nameAZ' :
                $query_order .= 'name ASC';
                break;
            case 'nameZA' :
                $query_order .= 'name DESC';
                break;
        }
        $query_limit = '';
        if ($limit) {
            $query_limit = 'LIMIT ' . $start . ',' . $limit;
        }
        $merchant = implode(',', $merchant);

        return $this->db->query("SELECT products_principal_stock.product_id AS product_id,
                                        products_principal_stock.branch_id AS store_id,
                                        products_principal_stock.quantity,
                                        products.merchant,
                                        products.name,
                                        products.short_description,
                                        products.code,
                                        products.category,
                                        products.brand,
                                        IFNULL (product_price.price, products.price) AS price,
                                        products.discount,
                                        products.promo,
                                        products.preorder,
                                        products.promo_data,
                                        products.viewed,
                                        products.date_added,
                                        merchants.name AS merchant_name,
                                        product_image.image,
                                        products.variation
                                FROM products_principal_stock
                                LEFT JOIN products ON products_principal_stock.product_id = products.id
                                LEFT JOIN product_price ON product_price.product = products.id AND product_price.merchant_group = $merchant_group
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
                                WHERE products.merchant = 0
                                AND products.status = 1
                                AND products.category IN (SELECT category FROM category_path WHERE path = $category)
                                AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
                                
                                UNION

                                SELECT products_principal_stock.product_id AS product_id,
                                        products_principal_stock.branch_id AS store_id,
                                        products_principal_stock.quantity,
                                        products.merchant,
                                        products.name,
                                        products.short_description,
                                        products.code,
                                        products.category,
                                        products.brand,
                                        products.price AS price,
                                        products.discount,
                                        products.promo,
                                        products.preorder,
                                        products.promo_data,
                                        products.viewed,
                                        products.date_added,
                                        merchants.name AS merchant_name,
                                        product_image.image,
                                        products.variation
                                FROM products_principal_stock
                                LEFT JOIN products ON products_principal_stock.product_id = products.id
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
                                WHERE products_principal_stock.branch_id IN (SELECT id FROM merchants WHERE id IN ($merchant) AND status = 1)
                                AND products.status = 1
                                AND products.category IN (SELECT category FROM category_path WHERE path = $category)
                                AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
                                
                                $query_order
                                $query_limit");


        // return $this->db->query("SELECT * FROM (
        //             SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, IFNULL(pp.price, p.price) price, pi.image, pp.id ppid, p.viewed, p.date_added, p.quantity,p.promo,p.promo_data
        //             FROM products p
        //             LEFT JOIN product_price pp ON pp.product = p.id AND pp.merchant_group = $merchant_group
        //             LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
        //             WHERE p.merchant = 0 AND p.status = 1
        //             UNION
        //             SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, p.price, pi.image, 0, p.viewed, p.date_added, p.quantity,p.promo,p.promo_data
        //             FROM products p
        //             LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
        //             WHERE merchant IN (SELECT id FROM merchants WHERE id IN ($merchant) AND status = 1) AND p.status = 1
        //             ) p
        //          WHERE p.category IN (SELECT category FROM category_path WHERE path = $category) $query_order $query_limit");
    }

    // NEW METHOD
    function get_products($category, $sort = 'popular', $limit = 20, $start = 0) {
        $query_order = 'ORDER BY ';
        switch ($sort) {
            default:
            case 'popular':
                $query_order .= 'viewed DESC';
                break;
            case 'newest' :
                $query_order .= 'date_added DESC';
                break;
            case 'lowprice' :
                $query_order .= 'price ASC';
                break;
            case 'highprice' :
                $query_order .= 'price DESC';
                break;
            case 'nameAZ' :
                $query_order .= 'name ASC';
                break;
            case 'nameZA' :
                $query_order .= 'name DESC';
                break;
        }

        $query_limit = '';
        if ($limit) {
            $query_limit = 'LIMIT ' . $start . ',' . $limit;
        }

        return $this->db->query("SELECT products_principal_stock.product_id AS product_id,
                                        products_principal_stock.branch_id AS store_id,
                                        products_principal_stock.quantity,
                                        products.name,
                                        products.short_description,
                                        products.code,
                                        products.category,
                                        products.brand,
                                        products.price AS price,
                                        products.discount,
                                        products.promo,
                                        products.preorder,
                                        products.promo_data,
                                        products.viewed,
                                        products.date_added,
                                        products.free_ongkir,
                                        merchants.name AS merchant_name,
                                        merchants.type AS store_type,
                                        product_image.image,
                                        products.variation,
                                        products.price_grosir
                                FROM products_principal_stock
                                LEFT JOIN products ON products_principal_stock.product_id = products.id
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
                                WHERE products.status = 1
                                AND products.category IN (SELECT category FROM category_path WHERE path = $category)
                                AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
                                AND products.store_type = 'principal'
                                GROUP BY products_principal_stock.branch_id, products_principal_stock.product_id

                                UNION

                                SELECT  products.id AS product_id,
                                        products.store_id AS store_id,
                                        products.quantity,
                                        products.name,
                                        products.short_description,
                                        products.code,
                                        products.category,
                                        products.brand,
                                        products.price AS price,
                                        products.discount,
                                        products.promo,
                                        products.preorder,
                                        products.promo_data,
                                        products.viewed,
                                        products.date_added,
                                        products.free_ongkir,
                                        merchants.name AS merchant_name,
                                        merchants.type AS store_type,
                                        product_image.image,
                                        products.variation,
                                        products.price_grosir
                                FROM products
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                LEFT JOIN merchants ON merchants.id = products.store_id
                                WHERE products.status = 1
                                AND products.category IN (SELECT category FROM category_path WHERE path = $category)
                                AND (products.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
                                AND products.store_type = 'merchant'
                                
                                $query_order
                                $query_limit");

    }

    function get_category_path($category) {
        $this->db->select("c.id, c.name", FALSE)
                ->join('categories c', 'pc.path = c.id', 'left')
                ->where('category', $category)
                ->order_by('level', 'ASC');
        $query = $this->db->get('category_path pc');
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function get_rating ($product, $store) {
        $this->db->select('IFNULL((SUM(rating_speed) + SUM(rating_service) + SUM(rating_accuracy)) / (COUNT(id) * 3), 0) AS rating,
                            IFNULL(COUNT(id), 0) AS review')
                ->where('product', $product)
                ->where('merchant', $store)
                ->where('status', 1);

        $query = $this->db->get('product_review');
        return ($query->result() > 0) ? $query->row() : FALSE;
    }
}
