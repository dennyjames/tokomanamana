<style type="text/css">
    .center{margin-top:30px;padding:10px;text-align:right}input[type=number]::-webkit-inner-spin-button{opacity:1}.cart-footer .total span:last-child{color:#d9534f}.qty{border:none;border-bottom:1px solid #ccc;margin-right:10px}.item-image{display:inline-block}.item-desc{display:inline-block}.cart-footer-mobile{display:none}.tombol-belanja{text-transform:capitalize;background-color:#a7c22a;border:1px solid #a7c22a;padding:0 30px;transition:.3s;font-weight:700!important;font-size:13px!important}.tombol-belanja:hover{background-color:#8ea623;color:#fff;border:1px solid #8ea623}.cart-title{display:none}.cart-footer .action .link-back:hover{color:#a7c22a}@media (max-width:767px){.cart-title{display:block}.cart-footer-mobile{display:block;border-top:3px solid #ddd;margin-top:10px;padding-top:30px}.cart-footer{display:none}.cart-footer-mobile .row .total span{width:100%;display:block}.cart-footer-mobile .row .total span:first-child{font-size:14px}.cart-footer-mobile .row .total span:last-child{font-size:18px;color:#d9534f;font-weight:500}.cart-footer-mobile .row .total{margin-top:5px}.cart-footer-mobile .row .action{justify-content:flex-end;float:right}.cart-footer-mobile .row .action a{border-radius:2px}.tombol-belanja{border-radius:3px}}@media (max-width:369px){.qty{margin-right:0!important;font-size:14px}.center div a i{font-size:20px!important}.cart-item .item-image img{width:100%!important}.center{margin-top:50px!important}}@media (max-width:557px){.item-image{display:block}.item-desc{display:block}.cart-line{margin-bottom:-10px}.center{margin-top:40px}}@media (max-width:520px){.total span:first-child{font-size:14px!important}.total span:last-child{font-size:18px!important}.action a:last-child{width:100%!important;height:45px;line-height:45px;margin-bottom:-30px;border-radius:5px}}input[type=number]::-webkit-inner-spin-button,input[type=number]::-webkit-outer-spin-button{-webkit-appearance:none;margin:0}.btn-delete-cart i{color:#ccc;-webkit-transition:.5s;-moz-transition:.5s;-ms-transition:.5s;-o-transition:.5s;transition:.5s}.btn-delete-cart:hover i{color:#d9534f}button:disabled,button[disabled]{background-color:#ddd;color:#666}.buy-now-btn{border-radius:3px;background:#a7c22a;color:#fff;border:1px solid #a7c22a;font-weight:700;transition:.3s}.buy-now-btn:hover{background:#859a20;border:1px solid #859a20;color:#fff!important}.buy-now-btn:hover i{color:#fff!important}.buy-now-btn:focus{outline:0;background:#859a20;border:1px solid #859a20;color:#fff!important}.buy-now-btn:focus i{color:#fff!important}.buy-now-btn:visited{background:#a7c22a;color:#fff;border:1px solid #a7c22a;font-weight:700}.cart-line .merchant a{transition:.3s}.cart-line .merchant a:hover{color:#a7c22a}.btn-buy-mobile{background:#a7c22a;border:1px solid #a7c22a;color:#fff;font-weight:700}.btn-buy-mobile:active,.btn-buy-mobile:focus{color:#fff;background:#859a20;border:1px solid #859a20}.cart-product-mobile-container{display:none}@media (max-width:767px){.cart-product-mobile-container{display:block}.cart-footer,.cart-product{display:none}}.cart-product-mobile-container .cart-item .cart-product-mobile{display:block}.cart-product-mobile-container .cart-item .cart-product-mobile .item-image-mobile{display:inline-block;width:25%}.cart-product-mobile-container .cart-item .cart-product-mobile .item-image-mobile a img{margin-top:-70px}.cart-product-mobile-container .cart-item .cart-product-mobile .item-desc-mobile{display:inline-block;width:70%;margin-left:4%}.cart-product-mobile-container .cart-item .cart-quantity-mobile{display:block;text-align:right;margin-top:5%}@media (max-width:425px){.cart-product-mobile-container .cart-item .cart-quantity-mobile{margin-top:9%}}@media (max-width:353px){.cart-footer-mobile .btn-buy-mobile{font-size:10px}}@media (max-width:322px){.cart-footer-mobile .action{width:130px}.cart-product-mobile .item-image-mobile{width:23%!important}}
</style>
<div id="tags-load" style="display:none;"><i class="fa fa-spinner fa-pulse fa-2x"></i></div>
<section class="collection-heading heading-content">
    <div class="container">
        <div class="cart-step">
            <ul style="font-size: 20px">
                <li style="width:80%" class="active"><i class="fa fa-shopping-cart"></i> Cart</li>
                <li style="width:5%"> <i style="color:#a7c22a" class="fa fa-arrow-right"></i> </li>
                <li style="width:80%" class=""><i class="fa fa-credit-card"></i> Checkout</li>
                <li style="width:5%"> <i class="fa fa-arrow-right"></i> </li>
                <li style="width:80%" class=""><i class="fa fa-money"></i> Payment</li>
                <li style="width:5%"> <i class="fa fa-arrow-right"></i> </li>
                <li style="width:80%"><i class="fa fa-check"></i> Finish</li>
            </ul>
        </div>
        <?php if ($this->ion_auth->logged_in()) : ?>
            <?php if (!$user->verification_phone || $orders_pending) : ?>
                <style>
                    .cart-title {
                        margin-top: -10px;
                    }
                </style>
            <?php else : ?>
                <style>
                    .cart-title {
                        margin-top: 10px;
                    }
                </style>
            <?php endif; ?>
        <?php endif; ?>
        <div class="cart-title" style="font-size: 18px;font-weight:bold;margin-bottom:30px;"><i class="fa fa-shopping-cart"></i> Keranjang</div>
    </div>
</section>

<section class="cart-content checkout" id="cart-section">
    <div id="cart-page">
        <div class="container">
            <?php if ($cart_item) : ?>
                <?php if($this->agent->is_mobile()) : ?>
                    <div class="cart-product-mobile-container">
                        <?php $grand_total = 0; ?>
                        <?php foreach ($cart_item as $item) : ?>
                            <?php
                            $product = $this->main->get('products', ['id' => $item->product_id]);
                            $stock_empty = false;
                            if ($product->store_type == 'principal') {
                                $stock_product = $this->main->get('products_principal_stock', ['product_id' => $item->product_id, 'branch_id' => $item->merchant_id]);
                                if ($stock_product->quantity < $item->quantity) {
                                    $stock_empty = true;
                                } else {
                                    $stock_empty = false;
                                }
                                $product->quantity = $stock_product->quantity;
                            } else {
                                if ($product->quantity < $item->quantity) {
                                    $stock_empty = true;
                                } else {
                                    $stock_empty = false;
                                }
                            }
                            ?>
                            <?php if($item->option) : ?>
                                <?php $cart_product_option_mobile = json_decode($item->option); ?>
                                <?php foreach($cart_product_option_mobile as $product_option_mobile) : ?>
                                    <?php 
                                    if($product->store_type == 'principal') {
                                        $product_option_branch_mobile = $this->main->get('products_principal_stock', ['product_id' => $item->product_id, 'branch_id' => $item->merchant_id, 'id_option' => $product_option_mobile->id]);
                                        if($product_option_branch_mobile) {
                                            $product_option_quantity_mobile = $product_option_branch_mobile->quantity;
                                        } else {
                                            $product_option_quantity_mobile = $product->quantity;
                                        }
                                        $stock_empty_option_mobile = false;
                                        if($product_option_quantity_mobile < $product_option_mobile->quantity) {
                                            $stock_empty_option_mobile = true;
                                        } else {
                                            $stock_empty_option_mobile = false;
                                        }
                                    } else {
                                        $product_option_quantity_mobile = $this->main->get('product_option', ['id' => $product_option_mobile->id])->quantity;
                                        if($product_option_quantity_mobile) {
                                            $product_option_quantity_mobile = $product_option_quantity_mobile;
                                        } else {
                                            $product_option_quantity_mobile = $product->quantity;
                                        }
                                        $stock_empty_option_mobile = false;
                                        if($product_option_quantity_mobile < $product_option_mobile->quantity) {
                                            $stock_empty_option_mobile = true;
                                        } else {
                                            $stock_empty_option_mobile = false;
                                        }
                                    }
                                    ?>
                                    <div class="cart-item" id="cart-mobile-option-<?php echo encode($item->id * $product_option_mobile->id); ?>" style="<?= ($stock_empty_option_mobile) ? 'border-color:red;border-bottom:1px solid red;' : '' ?>">
                                        <div class="row" style="width: 100%;" id="cart-content-mobile-<?= encode($item->id * $product_option_mobile->id) ?>">
                                            <!-- <div class="cart-item-left col-sm-6"> -->
                                            <div class="cart-product-mobile">
                                                <div class="item-image-mobile">
                                                    <a href="<?php echo seo_url('catalog/products/view/' . $item->product_id . '/' . $item->merchant_id); ?>">
                                                        <img src="<?php echo get_image($product_option_mobile->image); ?>" style="margin-top:-70px;">
                                                    </a>
                                                </div>
                                                <div class="item-desc-mobile" style="position: relative;">
                                                    <div class="item-name">
                                                        <a href="<?php echo seo_url('catalog/products/view/' . $item->product_id . '/' . $item->merchant_id); ?>">
                                                            <strong><?php echo $item->name; ?></strong>
                                                        </a>
                                                    </div>
                                                    <div class="cart-line">
                                                        <?php if ($product_option_mobile->data_option) : ?>
                                                            <?php $options = json_decode($product_option_mobile->data_option); ?>
                                                            <?php foreach ($options as $option) : ?>
                                                                <div class="option" style="height: 20px;"><?= $option->value ?></div>
                                                                <div class="divider"></div>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                        <div class="info"><?php echo $item->code; ?></div>
                                                        <div class="info weight"><?php echo $product_option_mobile->total_weight; ?> gram</div>
                                                    </div>
                                                    <br>
                                                    <span class="price" style="font-size: 16px; color: #a7c22a;"><?php echo rupiah($product_option_mobile->price); ?></span>
                                                    <?php if ($item->merchant_id) { ?>
                                                        <div class="cart-line">
                                                            <span class="merchant">Penjual: <a href="<?= seo_url('merchant_home/view/' . $item->merchant_id) ?>"><b><?php echo $item->merchant_name; ?></b></a></span>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="alertEmptyStock" id="alert-mobile-option-<?php echo encode($item->id * $product_option_mobile->id); ?>" style="color:#d9534f;position:absolute;top:115%;"><?= ($stock_empty_option_mobile) ? 'Maaf, Stok Produk tidak mencukupi!' : '' ?></div>
                                                </div>
                                            </div>
                                            <div class="cart-quantity-mobile">
                                                <div class="">
                                                    <div class="quantity-field" style="margin-right: 15px;margin-top:-11px; ">
                                                        <button id="btn_down_qty-mobile-option-<?= encode($item->id * $product_option_mobile->id) ?>" onclick="down_qty('<?= encode($item->id * $product_option_mobile->id) ?>', true);update_cart($('#qty-mobile-option-<?= encode($item->id * $product_option_mobile->id) ?>').val(), $('#qty-mobile-option-<?= encode($item->id * $product_option_mobile->id) ?>').data('store'), $('#qty-mobile-option-<?= encode($item->id * $product_option_mobile->id) ?>').data('product'), $('#qty-mobile-option-<?= encode($item->id * $product_option_mobile->id) ?>').data('idcart'), '<?php echo encode($product_option_mobile->id) ?>')" <?= ($product_option_mobile->quantity <= 1 || ($product_option_quantity_mobile == 1 && $product_option_mobile->quantity <= 1)) ? 'disabled' : '' ?>>
                                                            <i class="fa fa-minus"></i>
                                                        </button>

                                                        <input type="number" id="qty-mobile-option-<?= encode($item->id * $product_option_mobile->id) ?>" name="qty_mobile" value="<?= $product_option_mobile->quantity ?>" min="1" maxlength="3" class="item-quantity" data-store="<?= encode($item->merchant_id) ?>" data-product="<?= encode($item->product_id) ?>" data-idcart="<?= encode($item->id) ?>" data-qty="<?= $product_option_quantity_mobile ?>" onkeyup="update_cart($(this).val(), $(this).data('store'), $(this).data('product'), $(this).data('idcart'), '<?php echo encode($product_option_mobile->id) ?>');qty_form('<?php echo encode($product_option_mobile->id * $item->id) ?>', true)">

                                                        <button id="btn_up_qty-mobile-option-<?= encode($item->id * $product_option_mobile->id) ?>" onclick="up_qty('<?= encode($item->id * $product_option_mobile->id) ?>', true);update_cart($('#qty-mobile-option-<?= encode($item->id * $product_option_mobile->id) ?>').val(), $('#qty-mobile-option-<?= encode($item->id * $product_option_mobile->id) ?>').data('store'), $('#qty-mobile-option-<?= encode($item->id * $product_option_mobile->id) ?>').data('product'), $('#qty-mobile-option-<?= encode($item->id * $product_option_mobile->id) ?>').data('idcart'), '<?php echo encode($product_option_mobile->id) ?>')" class="plus" <?= ($product_option_mobile->quantity >= $product_option_quantity_mobile) ? 'disabled' : '' ?>>
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>
                                                    <a href="javascript:void(0)" class="btn-delete-cart" onclick="delete_cart(this, $(this).data('id'), $(this).data('merchant'), '<?php echo encode($product_option_mobile->id) ?>')" data-id="<?= encode($item->id) ?>" data-merchant="<?= encode($item->merchant_id) ?>"><i class="fa fa-trash" style="font-size:24px;"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <div class="cart-item" id="cart-mobile-<?php echo encode($item->id); ?>" style="<?= ($stock_empty) ? 'border-color:red;border-bottom:1px solid red;' : '' ?>">
                                    <div class="row" style="width: 100%;" id="cart-content-mobile-<?= encode($item->id) ?>">
                                        <!-- <div class="cart-item-left col-sm-6"> -->
                                        <div class="cart-product-mobile">
                                            <div class="item-image-mobile">
                                                <a href="<?php echo seo_url('catalog/products/view/' . $item->product_id . '/' . $item->merchant_id); ?>">
                                                    <img src="<?php echo get_image($item->image); ?>" style="margin-top:-70px;">
                                                </a>
                                            </div>
                                            <div class="item-desc-mobile" style="position: relative;">
                                                <div class="item-name">
                                                    <a href="<?php echo seo_url('catalog/products/view/' . $item->product_id . '/' . $item->merchant_id); ?>">
                                                        <strong><?php echo $item->name; ?></strong>
                                                    </a>
                                                </div>
                                                <div class="cart-line">
                                                    <?php if ($item->options) : ?>
                                                        <?php $options = json_decode($item->options); ?>
                                                        <?php foreach ($options as $option) : ?>
                                                            <div class="option" style="height: 20px;"><?= $option->option_name ?></div>
                                                            <div class="divider"></div>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                    <div class="info"><?php echo $item->code; ?></div>
                                                    <div class="info weight"><?php echo $item->weight; ?> gram</div>
                                                </div>
                                                <br>
                                                <span class="price" style="font-size: 16px; color: #a7c22a;"><?php echo rupiah($item->price); ?></span>
                                                <?php if ($item->merchant_id) { ?>
                                                    <div class="cart-line">
                                                        <span class="merchant">Penjual: <a href="<?= seo_url('merchant_home/view/' . $item->merchant_id) ?>"><b><?php echo $item->merchant_name; ?></b></a></span>
                                                    </div>
                                                <?php } ?>
                                                <div class="alertEmptyStock" id="alert-mobile-<?php echo encode($item->id); ?>" style="color:#d9534f;position:absolute;top:115%;"><?= ($stock_empty) ? 'Maaf, Stok Produk tidak mencukupi!' : '' ?></div>
                                            </div>
                                        </div>
                                        <div class="cart-quantity-mobile">
                                            <div class="">
                                                <div class="quantity-field" style="margin-right: 15px;margin-top:-11px; ">
                                                    <button id="btn_down_qty-mobile-<?= encode($item->id) ?>" onclick="down_qty('<?= encode($item->id) ?>');update_cart($('#qty-mobile-<?= encode($item->id) ?>').val(), $('#qty-mobile-<?= encode($item->id) ?>').data('store'), $('#qty-mobile-<?= encode($item->id) ?>').data('product'), $('#qty-mobile-<?= encode($item->id) ?>').data('idcart'))" <?= ($item->quantity <= 1 || ($product->quantity == 1 && $item->quantity <= 1)) ? 'disabled' : '' ?>>
                                                        <i class="fa fa-minus"></i>
                                                    </button>

                                                    <input type="number" id="qty-mobile-<?= encode($item->id) ?>" name="qty_mobile" value="<?= $item->quantity ?>" min="1" maxlength="3" class="item-quantity" data-store="<?= encode($item->merchant_id) ?>" data-product="<?= encode($item->product_id) ?>" data-idcart="<?= encode($item->id) ?>" data-qty="<?= $product->quantity ?>" onkeyup="update_cart($(this).val(), $(this).data('store'), $(this).data('product'), $(this).data('idcart'));qty_form($(this).data('idcart'))">

                                                    <button id="btn_up_qty-mobile-<?= encode($item->id) ?>" onclick="up_qty('<?= encode($item->id) ?>');update_cart($('#qty-mobile-<?= encode($item->id) ?>').val(), $('#qty-mobile-<?= encode($item->id) ?>').data('store'), $('#qty-mobile-<?= encode($item->id) ?>').data('product'), $('#qty-mobile-<?= encode($item->id) ?>').data('idcart'))" class="plus" <?= ($item->quantity >= $product->quantity) ? 'disabled' : '' ?>>
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                                <a href="javascript:void(0)" class="btn-delete-cart" onclick="delete_cart(this, $(this).data('id'), $(this).data('merchant'))" data-id="<?= encode($item->id) ?>" data-merchant="<?= encode($item->merchant_id) ?>"><i class="fa fa-trash" style="font-size:24px;"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php $grand_total += $item->grand_total; ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="cart-footer-mobile">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="total">
                                    <span>Total Harga</span>
                                    <span class="grand-total"><?= rupiah($grand_total); ?></span>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="action">
                                    <a href="javascript:void(0)" onclick="check_verifikasi()" class="btn btn-buy-mobile">Beli Sekarang</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php else : ?>
                    <div class="cart-product">
                        <?php $grand_total = 0; ?>
                        <?php foreach ($cart_item as $item) : ?>
                            <?php
                            $product = $this->main->get('products', ['id' => $item->product_id]);
                            $stock_empty = false;
                            if ($product->store_type == 'principal') {
                                $stock_product = $this->main->get('products_principal_stock', ['product_id' => $item->product_id, 'branch_id' => $item->merchant_id]);
                                if ($stock_product->quantity < $item->quantity) {
                                    $stock_empty = true;
                                } else {
                                    $stock_empty = false;
                                }
                                $product->quantity = $stock_product->quantity;
                            } else {
                                if ($product->quantity < $item->quantity) {
                                    $stock_empty = true;
                                } else {
                                    $stock_empty = false;
                                }
                            }
                            ?>
                            <?php if($item->option) : ?>
                                <?php $cart_product_option = json_decode($item->option); ?>
                                <?php foreach($cart_product_option as $product_option) : ?>
                                    <?php 
                                    if($product->store_type == 'principal') {
                                        $product_option_branch = $this->main->get('products_principal_stock', ['branch_id' => $item->merchant_id, 'product_id' => $item->product_id, 'id_option' => $product_option->id]);
                                        if($product_option_branch) {
                                            $product_option_quantity = $product_option_branch->quantity;
                                        } else {
                                            $product_option_quantity = $product->quantity;
                                        }
                                        $stock_empty_option = false;
                                        if($product_option_quantity < $product_option->quantity) {
                                            $stock_empty_option = true;
                                        } else {
                                            $stock_empty_option = false;
                                        }
                                    } else {
                                        $product_option_quantity = $this->main->get('product_option', ['id' => $product_option->id])->quantity;
                                        if($product_option_quantity) {
                                            $product_option_quantity = $product_option_quantity;
                                        } else {
                                            $product_option_quantity = $product->quantity;
                                        }
                                        $stock_empty_option = false;
                                        if($product_option_quantity < $product_option->quantity) {
                                            $stock_empty_option = true;
                                        } else {
                                            $stock_empty_option = false;
                                        }
                                    }
                                    ?>
                                    <div class="cart-item" id="cart-option-<?php echo encode($item->id * $product_option->id); ?>" style="<?= ($stock_empty_option) ? 'border-color:red;border-bottom:1px solid red;' : '' ?>">
                                        <div class="row" style="width: 100%;" id="cart-content-<?= encode($item->id * $product_option->id) ?>">
                                            <!-- <div class="cart-item-left col-sm-6"> -->
                                            <div class="col-sm-8 col-xs-8">
                                                <div class="item-image">
                                                    <a href="<?php echo seo_url('catalog/products/view/' . $item->product_id . '/' . $item->merchant_id); ?>">
                                                        <img src="<?php echo get_image($product_option->image); ?>">
                                                    </a>
                                                </div>
                                                <div class="item-desc">
                                                    <div class="item-name">
                                                        <a href="<?php echo seo_url('catalog/products/view/' . $item->product_id . '/' . $item->merchant_id); ?>">
                                                            <strong><?php echo $item->name; ?></strong>
                                                        </a>
                                                    </div>
                                                    <div class="cart-line">
                                                        <?php $options = json_decode($product_option->data_option); ?>
                                                        <?php foreach ($options as $option) : ?>
                                                            <div class="option" style="height: 20px;"><?= $option->value ?></div>
                                                            <div class="divider"></div>
                                                        <?php endforeach; ?>
                                                    </div>
                                                    <br>
                                                    <span class="price" style="font-size: 16px; color: #a7c22a;"><?php echo rupiah($product_option->price); ?></span>
                                                    <?php if ($item->merchant_id) { ?>
                                                        <div class="cart-line">
                                                            <span class="merchant">Penjual: <a href="<?= seo_url('merchant_home/view/' . $item->merchant_id) ?>"><b><?php echo $item->merchant_name; ?></b></a></span>
                                                        </div>
                                                    <?php } ?>
                                                    <div class="alertEmptyStock" id="alert-option-<?php echo encode($item->id * $product_option->id); ?>" style="color:#d9534f;"><?= ($stock_empty_option) ? 'Maaf, Stok Produk tidak mencukupi!' : '' ?></div>
                                                </div>
                                            </div>
                                            <div class="center col-sm-4 col-xs-4">
                                                <div class="">
                                                    <div class="quantity-field" style="margin-right: 15px;margin-top:-11px; ">
                                                        <button id="btn_down_qty-option-<?= encode($item->id * $product_option->id) ?>" onclick="down_qty('<?= encode($item->id * $product_option->id) ?>', true);update_cart($('#qty-option-<?= encode($item->id * $product_option->id) ?>').val(), $('#qty-option-<?= encode($item->id * $product_option->id) ?>').data('store'), $('#qty-option-<?= encode($item->id * $product_option->id) ?>').data('product'), $('#qty-option-<?= encode($item->id * $product_option->id) ?>').data('idcart'), '<?= encode($product_option->id) ?>')" <?= ($product_option->quantity <= 1 || ($product_option->quantity == 1 && $product_option_quantity <= 1)) ? 'disabled' : '' ?>>
                                                            <i class="fa fa-minus"></i>
                                                        </button>

                                                        <input type="number" id="qty-option-<?= encode($item->id * $product_option->id) ?>" name="qty" value="<?= $product_option->quantity ?>" min="1" maxlength="3" class="item-quantity" data-store="<?= encode($item->merchant_id) ?>" data-product="<?= encode($item->product_id) ?>" data-idcart="<?= encode($item->id) ?>" data-qty="<?= $product_option_quantity ?>" onkeyup="update_cart($(this).val(), $(this).data('store'), $(this).data('product'), $(this).data('idcart'), '<?= encode($product_option->id) ?>');qty_form('<?= encode($item->id * $product_option->id)?>', true)">

                                                        <button id="btn_up_qty-option-<?= encode($item->id * $product_option->id) ?>" onclick="up_qty('<?= encode($item->id * $product_option->id) ?>', true);update_cart($('#qty-option-<?= encode($item->id * $product_option->id) ?>').val(), $('#qty-option-<?= encode($item->id * $product_option->id) ?>').data('store'), $('#qty-option-<?= encode($item->id * $product_option->id) ?>').data('product'), $('#qty-option-<?= encode($item->id * $product_option->id) ?>').data('idcart'), '<?= encode($product_option->id) ?>')" class="plus" <?= ($product_option->quantity >= $product_option_quantity) ? 'disabled' : '' ?>>
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>
                                                    <a href="javascript:void(0)" class="btn-delete-cart" onclick="delete_cart(this, $(this).data('id'), $(this).data('merchant'), '<?php echo encode($product_option->id) ?>')" data-id="<?= encode($item->id) ?>" data-merchant="<?= encode($item->merchant_id) ?>"><i class="fa fa-trash" style="font-size:24px;"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <div class="cart-item" id="cart-<?php echo encode($item->id); ?>" style="<?= ($stock_empty) ? 'border-color:red;border-bottom:1px solid red;' : '' ?>">
                                    <div class="row" style="width: 100%;" id="cart-content-<?= encode($item->id) ?>">
                                        <!-- <div class="cart-item-left col-sm-6"> -->
                                        <div class="col-sm-8 col-xs-8">
                                            <div class="item-image">
                                                <a href="<?php echo seo_url('catalog/products/view/' . $item->product_id . '/' . $item->merchant_id); ?>">
                                                    <img src="<?php echo get_image($item->image); ?>">
                                                </a>
                                            </div>
                                            <div class="item-desc">
                                                <div class="item-name">
                                                    <a href="<?php echo seo_url('catalog/products/view/' . $item->product_id . '/' . $item->merchant_id); ?>">
                                                        <strong><?php echo $item->name; ?></strong>
                                                    </a>
                                                </div>
                                                <div class="cart-line">
                                                    <div class="info"><?php echo $item->code; ?></div>
                                                    <div class="info weight"><?php echo $item->weight; ?> gram</div>
                                                </div>
                                                <br>
                                                <span class="price" style="font-size: 16px; color: #a7c22a;"><?php echo rupiah($item->price); ?></span>
                                                <?php if ($item->merchant_id) { ?>
                                                    <div class="cart-line">
                                                        <span class="merchant">Penjual: <a href="<?= seo_url('merchant_home/view/' . $item->merchant_id) ?>"><b><?php echo $item->merchant_name; ?></b></a></span>
                                                    </div>
                                                <?php } ?>
                                                <div class="alertEmptyStock" id="alert-<?php echo encode($item->id); ?>" style="color:#d9534f;"><?= ($stock_empty) ? 'Maaf, Stok Produk tidak mencukupi!' : '' ?></div>
                                            </div>
                                        </div>
                                        <div class="center col-sm-4 col-xs-4">
                                            <div class="">
                                                <div class="quantity-field" style="margin-right: 15px;margin-top:-11px; ">
                                                    <button id="btn_down_qty-<?= encode($item->id) ?>" onclick="down_qty('<?= encode($item->id) ?>');update_cart($('#qty-<?= encode($item->id) ?>').val(), $('#qty-<?= encode($item->id) ?>').data('store'), $('#qty-<?= encode($item->id) ?>').data('product'), $('#qty-<?= encode($item->id) ?>').data('idcart'))" <?= ($item->quantity <= 1 || ($product->quantity == 1 && $item->quantity <= 1)) ? 'disabled' : '' ?>>
                                                        <i class="fa fa-minus"></i>
                                                    </button>

                                                    <input type="number" id="qty-<?= encode($item->id) ?>" name="qty" value="<?= $item->quantity ?>" min="1" maxlength="3" class="item-quantity" data-store="<?= encode($item->merchant_id) ?>" data-product="<?= encode($item->product_id) ?>" data-idcart="<?= encode($item->id) ?>" data-qty="<?= $product->quantity ?>" onkeyup="update_cart($(this).val(), $(this).data('store'), $(this).data('product'), $(this).data('idcart'));qty_form($(this).data('idcart'))">

                                                    <button id="btn_up_qty-<?= encode($item->id) ?>" onclick="up_qty('<?= encode($item->id) ?>');update_cart($('#qty-<?= encode($item->id) ?>').val(), $('#qty-<?= encode($item->id) ?>').data('store'), $('#qty-<?= encode($item->id) ?>').data('product'), $('#qty-<?= encode($item->id) ?>').data('idcart'))" class="plus" <?= ($item->quantity >= $product->quantity) ? 'disabled' : '' ?>>
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                                <a href="javascript:void(0)" class="btn-delete-cart" onclick="delete_cart(this, $(this).data('id'), $(this).data('merchant'))" data-id="<?= encode($item->id) ?>" data-merchant="<?= encode($item->merchant_id) ?>"><i class="fa fa-trash" style="font-size:24px;"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php $grand_total += $item->grand_total; ?>
                        <?php endforeach; ?>
                    </div>
                    <div class="cart-footer">
                        <div class="total">
                            <span>Total Harga</span>
                            <span class="grand-total"><?php echo rupiah($grand_total); ?></span>
                        </div>
                        <div class="action">
                            <a href="<?php echo site_url(); ?>" class="link-back"><i class="fa fa-arrow-left"></i> Lanjut Belanja</a>
                            <a href=""></a>
                            <a href="javascript:void(0)" class="btn buy-now-btn" style="border-radius: 5px;" onclick="check_verifikasi()">Beli Sekarang &nbsp; <i class="fa fa-arrow-right"></i></a>
                        </div>
                    </div>
                <?php endif; ?>
            <?php else : ?>
                <div class="text-center">
                    <img src="<?= base_url('files/images/keranjang-kosong.jpeg') ?>" alt="Keranjang Kosong" style="width: 40%; margin-top: -20px;">
                    <div style="font-size: 20px;font-weight: bold;margin-bottom: 5px;">Keranjang belanja anda kosong.</div>
                    <div style="font-size: 18px;margin-bottom: 20px;">Yuk belanja sekarang!</div>
                    <a href="<?= site_url() ?>" class="btn btn-block tombol-belanja" style="border-radius: 5px;">Mulai Belanja</a>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>