<?php

    $m = $this->Model;
    $join = array(
        0 => 'point_reward_cart prc-prc.id=prcd.id_cart',
        1 => 'reward r-r.id=prcd.id_reward'
    );

    $where = array(
        'prc.id_customer' => $customer->id,
        'prcd.status' =>  2
    );
    $q_cart = $m->get_data('prcd.*, r.nama, r.id_product, r.id_product_paket, r.id_coupon, r.qty_product, r.qty_paket, r.qty_coupon', 'point_reward_cart_detail prcd', $join, $where);
    $product = $q_cart->row();

    $q_rr = $m->get_data('', 'reward_claim_temp', null, array('id_reward' => $product->id_reward, 'status_periode_aktif' => 1), '', '', 1, 0, array(0 => "id_customer LIKE '%".$customer->id."%'"))->row();

    if($q_cart->row()->id_product != 0 || $q_cart->row()->id_product_paket != 0){
      $cart_step = '
        <li class="active">2. Alamat</li>
        <li class="active">3. Selesai</li>
      ';
    }else{
      $cart_step = '<li class="active">2. Selesai</li>';
    }
    
?>
<section class="collection-heading heading-content ">
    <div class="container">
        <div class="cart-step">
            <ul>
                <li class="active">1. Tukar Poin Saya</li>
                <?=$cart_step?>
            </ul>
        </div>
    </div>
</section>
<section class="cart-content checkout">
    <div class="container">
        <div class="finish">
            <div class="finish-header">
                <h2>Hai <?php echo $customer->fullname; ?>, terimakasih telah berbelanja di TokoManaMana.com</h2>
                
            </div>
            <div class="finish-detail">
                <div class="text-center text-bold"><h3><?=$product->nama?></h3></div>
                <div class="text-center text-bold">Rincian Reward Anda</div>
                <div class="items">
                    <?php
                        if($product->id_product != 0){
                            $q_product = $m->get_data('', 'products', null, array('id' => $product->id_product));
                            if($q_product->num_rows() > 0){
                                $r_product = $q_product->row();
                    ?>
                    <div class="item">
                        <div class="item-left"><?php echo $r_product->name; ?></div>
                        <div class="item-right"> x <?php echo $product->qty_product; ?></div>
                    </div>
                    <?php
                            }
                        }

                        if($product->id_product_paket != 0){
                            $q_paket = $m->get_data('', 'products', null, array('id' => $product->id_product_paket));
                            if($q_paket->num_rows() > 0){
                                $r_paket = $q_paket->row();
                    ?>
                    <div class="item">
                        <div class="item-left"><?php echo $r_paket->name; ?></div>
                        <div class="item-right"> x <?php echo $product->qty_paket; ?></div>
                    </div>
                    <?php
                            }
                        }

                        if($product->id_coupon != 0){
                            $q_coupon = $m->get_data('', 'coupons', null, array('id' => $product->id_coupon));
                            if($q_coupon->num_rows() > 0){
                                $r_coupon = $q_coupon->row();
                    ?>
                    <div class="item">
                        <div class="item-left"><?php echo $r_coupon->name; ?></div>
                        <div class="item-right"> x <?php echo $product->qty_coupon; ?></div>
                    </div>
                    <?php
                            }
                        }
                    ?>
                    
                    <!-- <div class="item">
                        <div class="item-left">Ongkos Kirim</div>
                        <div class="item-right"><?php echo rupiah(0); ?></div>
                    </div> -->
                </div>
                <!-- <div class="item">
                    <div class="item-left">Total Transaksi</div>
                    <div class="item-right"><?php echo rupiah(0); ?></div>
                </div> -->
            </div>
            <div class="finish-footer">
                <a class="btn btn-primary" href="<?php echo site_url('member/order_reward_detail/' . encode($q_rr->id)); ?>">Lihat Detail</a>
            </div>
        </div>
    </div>
</section>