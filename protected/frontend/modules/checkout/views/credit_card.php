<section class="collection-heading heading-content ">
    <div class="container">
        <div class="cart-step">
            <ul>
                <li class="active">1. Belanjaan Saya</li>
                <li class="active">2. Alamat</li>
                <li class="active">3. Pengiriman</li>
                <li class="active">4. Pembayaran</li>
                <li>5. Selesai</li>
            </ul>
        </div>
    </div>
</section>
<section class="cart-content checkout">
    <div class="container">
        <div class="row">
            <div class="cart-payment">
                <?php if (isset($message)) { ?>
                    <div class="alert alert-danger fade in alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                        <?php echo $message; ?>
                    </div>
                <?php } ?>
                <div class="col-md-6 col-md-offset-3">
                    <div id="success" style="display:none">
                        <div class="alert alert-success"></div>
                    </div>
                    <div id="error" style="display:none">
                        <div class="alert alert-danger"></div>
                    </div>
                    <form action="javascript:void(0);" id="payment-form" method="post">
                        <input type="hidden" id="token">
                        <h1 class="text-center">kartu Kredit</h1>
                        <div id="creditcard">
                            <div class="row">
                                <div class="col-sm-12 mb-3 form-group">
                                    <label for="card-number">No. Kartu kredit</label>
                                    <input type="text" class="form-control" id="card-number" name="card-number" value="" placeholder="Masukan No kartu kredit anda" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 mb-3 form-group">
                                    <label for="card-exp-month">Exp. Month (MM)</label>
                                    <input type="text" class="form-control" id="card-exp-month" minlength="2" maxlength="2" value="" placeholder="" required>
                                </div>
                                <div class="col-sm-4 mb-3 form-group">
                                    <label for="card-exp-year">Exp. Year (YYYY)</label>
                                    <input type="text" class="form-control" id="card-exp-year" minlength="4" maxlength="4" value="" placeholder="" required>
                                </div>
                                <div class="col-sm-4 mb-3 form-group">
                                    <label for="card-cvn">CVV/CVN</label>
                                    <input type="text" class="form-control" id="card-cvn" minlength="3" maxlength="3" value="" placeholder="" required>
                                </div>
                            </div>
                        </div>
                        <hr class="mb-4">
                        <button class="btn btn-primary btn-lg btn-block" type="submit">Bayar Sekarang</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="overlay" style="display: none;"></div>
<div id="three-ds-container" style="display: none;">
    <iframe height="450" width="550" id="sample-inline-frame" name="sample-inline-frame"> </iframe>
</div>
<div class="loading"></div>
<style type="text/css">
.loading {
    background: rgba(255,255,255,0.5) url(../images/loading.gif) no-repeat center center;
    background-size: 36px;
    display: none;
    position: fixed;
    height: 100%;
    width: 100%;
    left: 0;
    top: 0;
    z-index: 999;
}
.overlay {
    background: rgba(255,255,255,0.5);
    display: none;
    position: fixed;
    height: 100%;
    width: 100%;
    left: 0;
    top: 0;
    z-index: 999;
}
#three-ds-container {
    background: #fff;
    border: none;
    box-shadow: 0 0 10px #999;
    display: none;
    position: fixed;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    z-index: 1000;
}
#three-ds-container iframe {
    border: none;
}
</style>
<script src="https://js.xendit.co/v1/xendit.min.js"></script>
<script>Xendit.setPublishableKey('<?php
$this->config->load('xendit');
if ($this->config->item('xendit_is_production')) {
    echo $this->config->item('xendit_public_api_key_production');
} else {
    echo $this->config->item('xendit_public_api_key_development');
}
?>');</script>
<?php /*<script>Xendit.setPublishableKey('xnd_public_production_PIyJfuV10bH9kcFtL7UYHDOXNN7394J9lnXl+Bhi+WHW+ryjDwY=');</script>*/ ?>
<script>
$(function () {
    var $form = $('#payment-form');

    $form.submit(function (event) {
        hideResults();

        $form.find('.submit').prop('disabled', true);
        $( ".loading" ).show();

        var tokenData = getTokenData();
        var fraudData = getFraudData();

        Xendit.card.validateCardNumber(tokenData.card_number);
        Xendit.card.validateExpiry(tokenData.card_exp_month, tokenData.card_exp_year);
        Xendit.card.validateCvn(tokenData.card_cvn);

        Xendit.card.createToken(tokenData, xenditTokenResponseHandler);

        return false;
    });

    function xenditTokenResponseHandler (err, creditCardCharge) {
        $form.find('.submit').prop('disabled', false);

        if (err) {
            return displayError(err);
        }

        if (creditCardCharge.status === 'APPROVED' || creditCardCharge.status === 'VERIFIED') {
            handleTokenSuccess(creditCardCharge);
        } else if (creditCardCharge.status === 'IN_REVIEW') {
            window.open(creditCardCharge.payer_authentication_url, 'sample-inline-frame');
            $('.overlay').show();
            $('#three-ds-container').show();
        } else if (creditCardCharge.status === 'FRAUD') {
            displayError(creditCardCharge);
        } else if (creditCardCharge.status === 'FAILED') {
            displayError(creditCardCharge);
        }
    }

    function xenditResponseHandler (err, creditCardCharge) {
        $form.find('.submit').prop('disabled', false);

        if (err) {
            return displayError(err);
        }

        if (creditCardCharge.status === 'APPROVED' || creditCardCharge.status === 'VERIFIED') {
            chargeCard(creditCardCharge);
        } else if (creditCardCharge.status === 'IN_REVIEW') {
            window.open(creditCardCharge.payer_authentication_url, 'sample-inline-frame');
            $('.overlay').show();
            $('#three-ds-container').show();
        } else if (creditCardCharge.status === 'FRAUD') {
            displayError(creditCardCharge);
        } else if (creditCardCharge.status === 'FAILED') {
            displayError(creditCardCharge);
        }
    }

    function handleTokenSuccess (creditCardCharge) {
        $( ".loading" ).hide();
        console.log(creditCardCharge);
        $('#token').val(creditCardCharge.id);
        var authenticationData = { token_id: creditCardCharge.id, amount: <?php echo $order->total; ?> };
        Xendit.card.createAuthentication(authenticationData, xenditResponseHandler);
    }

    function chargeCard (creditCardCharge) {
        $('.overlay').hide();
        $('#three-ds-container').hide();
        $.ajax({
            method: "POST",
            url: '<?php echo site_url('checkout/creditcard/charge'); ?>',
            data: { token_id: $('#token').val(), authentication_id: creditCardCharge.id, data: getTokenData(), id: '<?php echo $order->id; ?>' }
        })
        .done(function(res) {
            window.location = '<?php echo site_url('checkout'); ?>/' + res;
        });
    }

    function displayError (err) {
        $('#three-ds-container').hide();
        $('.overlay').hide();
        $('#error .result').text(JSON.stringify(err, null, 4));
        $('#error .alert').text(err.message);
        $('#error').show();

        var requestData = {};
        $.extend(requestData, getTokenData(), getFraudData());
        $('#error .request-data').text(JSON.stringify(requestData, null, 4));

        $( ".loading" ).hide();
    };

    function displaySuccess (creditCardCharge) {
        $('#three-ds-container').hide();
        $('.overlay').hide();
        $('#success .result').text(JSON.stringify(creditCardCharge, null, 4));
        $('#success .alert').text(creditCardCharge.message);
        $('#success').show();

        var requestData = {};
        $.extend(requestData, getTokenData(), getFraudData());
        $('#success .request-data').text(JSON.stringify(requestData, null, 4));

    }

    function getTokenData () {
        return {
            amount: '<?php echo $order->total; ?>',
            card_number: $form.find('#card-number').val(),
            card_exp_month: $form.find('#card-exp-month').val(),
            card_exp_year: $form.find('#card-exp-year').val(),
            card_cvn: $form.find('#card-cvn').val(),
            is_multiple_use: true,
            should_authenticate: true
        };
    }

    function getFraudData () {
        <?php /*return JSON.parse($form.find('#meta-json').val());*/ ?>
    }

    function hideResults() {
        $('#success').hide();
        $('#error').hide();
    }

    $( document ).ajaxStart(function() {
        $( ".loading" ).show();
    });
    $( document ).ajaxStop(function() {
        $( ".loading" ).hide();
    });
    $( document ).ajaxComplete(function() {
        $( ".loading" ).hide();
    });
});
</script>
