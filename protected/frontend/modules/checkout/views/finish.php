<script>
  fbq('track', 'Purchase');
</script>
<link rel="stylesheet" href="<?php echo site_url('assets/frontend/css/minified/finish_min.css') ?>">
<section class="collection-heading heading-content ">
    <div class="container">
        <div class="cart-step">
            <ul style="font-size: 20px;">
                <li style="width:80%" class="active"><i class="fa fa-shopping-cart"></i> Cart</li>
                <li style="width:5%"> <i style="color:#a7c22a" class="fa fa-arrow-right"></i> </li>
                <li style="width:80%" class="active"><i class="fa fa-credit-card"></i> Checkout</li>
                <li style="width:5%"> <i style="color:#a7c22a" class="fa fa-arrow-right"></i> </li>
                <li style="width:80%" class="active"><i class="fa fa-money"></i> Payment</li>
                <li style="width:5%"> <i style="color:#a7c22a" class="fa fa-arrow-right"></i> </li>
                <li style="width:80%" class="active"><i class="fa fa-check"></i> Finish</li>
            </ul>
        </div>
    </div>
</section>
<section class="cart-content checkout">
    <div class="container" style="<?php echo ($this->agent->is_mobile) ? 'padding: 0px 15px;' : '' ; ?>">
        <?php if($this->agent->is_mobile()) : ?>
            <div class="row">
                <div class="finish_mobile">
                    <div class="finish-header">
                        <h2>Hai <?php echo $customer->fullname; ?>,<br> Terimakasih telah berbelanja di TokoManaMana.com</h2>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="order-info-label">NO. TAGIHAN</div>
                                <div class="order-info-content order-info-content-code"><a href="<?php echo site_url('member/order_detail/' . encode($order->id)); ?>" style="color: #a7c22a;"><?php echo $order->code; ?></a></div>
                            </div>
                            <div class="col-xs-6 text-right">
                                <div class="order-info-label">STATUS</div>
                                <div class="order-info-content" style="color: #666">
                                    <?php
                                    if ($order->payment_status == 'Paid') {
                                        echo 'SUDAH DIBAYAR';
                                    } elseif (in_array($order->payment_status,['Failed','Cancel'])) {
                                        echo 'GAGAL';
                                    } elseif ($order->payment_status=='Expired') {
                                        echo 'KADALUARSA';
                                    } else {
                                        echo'BELUM DIBAYAR';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php // if ($order->payment_method != 'credit_card') {  ?>
                    <!-- <hr> -->
                    <!-- <div class="finish-subheader" >
                        <?php
                        $payment_method = $this->main->get('payment_methods', array('name' => $order->payment_method));
                        $payment = json_decode($order->payment_to);

                        if ($order->payment_status == 'Pending') {
                            if($order->payment_method == 'transfer'){
                                $total_akhir = $order->total+$order->unique_code;
                            } else {
                                $total_akhir = $order->total;
                            }
                            
                            if (in_array($order->payment_method, ['transfer', 'bca_va', 'mandiri_va', 'bni_va', 'bri_va'])) { ?>
                                <div class="col-xs-12">
                                    <div class="col-xs-6">
                                        <div class="text-center" style="font-size: 20px;">Jumlah Bayar</div>
                                        <div class="finish-amount"><?= rupiah($total_akhir) ?></div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="text-center" style="font-size: 20px;">Berita Transfer</div>
                                        <div class="finish-amount" style="color: #a7c22a"><?= $order->transfer_code ?></div>
                                    </div>
                                </div>
                                <?php    if($order->payment_method == 'transfer'){ ?>
                                            <div class=""><h4>Pembayaran Anda otomatis terkonfirmasi apabila mentransfer sesuai <span style="font-weight: bold; color: #000">Jumlah</span> dan <span style="font-weight: bold; color: #000">Berita Transfer</span> diatas.</h4></div> 
                                <?php    }
                                echo '<div class="text-center">ke rekening ' . ($order->payment_method != 'transfer' ? 'virtual' : '') . ' berikut ini:</div>';
                                if ($order->payment_method != 'transfer') {
                                    echo '<div class="detail-bank">
                                    <img src="' . base_url('files/images/' . $payment_method->image) . '">
                                    <div>' . $payment->account_name . '</div>
                                    <strong>' . $payment->account_number . '</strong>
                                    <div>' . $payment->account_holder_name . '</div>
                                    </div>';
                                } else {
                                    echo '<div class="list-bank">';
                                    foreach ($payment as $pym) {
                                        echo '<div class="detail-bank">
                                        <img src="' . base_url('files/images/' . $pym->image) . '">
                                        <div><strong>' . $pym->account_number . '</strong></div>
                                        <div>' . $pym->account_name . '</div>
                                        <div>' . $pym->name . '</div>
                                        <div>' . $pym->branch . '</div>
                                        </div>';
                                    }
                                    echo '</div>';
                                    echo '<div class="text-center" style="margin-top: 15px;"><button type="button" id="payment-confirmation" class="btn btn-primary">Konfirmasi Pembayaran</button></div>';
                                }
                            } elseif (in_array($order->payment_method, ['alfamart', 'kredivo'])) {
                                echo '<div class="text-center">Silahkan lakukan pembayaran sebesar:</div>
                                    <div class="finish-amount">' . rupiah($order->total) . '</div>';
                                if ($order->payment_method == 'alfamart') {
                                    echo '<div class="text-center">di Alfamart/Alfamidi terdekat</div>';
                                    echo '<div class="detail-bank">
                                    <img src="' . base_url('files/images/' . $payment_method->image) . '">
                                    <div>Kode Pembayaran:</div>
                                    <strong>' . $payment->account_number . '</strong>
                                    </div>';
                                } elseif ($order->payment_method == 'kredivo') {
                                    echo '<div class="text-center">melalui Kredivo</div>';
                                    echo '<div class="detail-bank">
                                    <div class="text-center"><a class="btn" href="' . $payment->payment_url . '">BAYAR SEKARANG</a></div>
                                    </div>';
                                }
                            }
                        }
                        
                        if ($order->payment_status == 'Pending') {
                            echo '<div class="text-center">Transaksi otomatis batal jika pembayaran tidak dilakukan sampai <b>' . get_date_time($order->due_date) . '</b></div>';
                        } elseif ($order->payment_status == 'Paid') {
                            echo '<div class="text-center"><div class="alert alert-success">Terima Kasih transaksi anda sudah berhasil</div></div>';
                        } elseif ($order->payment_status == 'Confirmed') {
                            echo '<div class="text-center"><div class="alert alert-success">Terima Kasih, Pesanan Anda Akan Dikonfirmasi</div></div>';
                        } else {
                            echo '<div class="text-center"><div class="alert alert-danger">Mohon maaf transaksi anda gagal</div></div>';
                        }
                        ?>
                    </div> -->
                    <?php // }   ?>
                    <div class="finish-detail">
                        <div class="text-center text-bold finish-detail-title">Rincian Belanja Anda</div>
                        <div class="items">
                            <?php
                            $products = $this->main->gets('order_product', array('order' => $order->id));
                            $total_price_product = 0;
                            foreach ($products->result() as $product) {
                                $total_price_product += $product->price;
                                ?>
                                <div class="item">
                                    <div class="item-left"><?php echo $product->name . ' ' . rupiah($product->price) . ' x ' . $product->quantity; ?></div>
                                    <div class="item-right"><?php echo rupiah($product->total); ?></div>
                                </div>
                            <?php } ?>
                            <hr>
                            <div class="item">
                                <div class="item-left" style="font-weight: bold;">Total Harga Barang</div>
                                <div class="item-right" style="font-weight: bold;"><?php echo rupiah($total_price_product) ?></div>
                            </div>
                            <div class="item">
                                <div class="item-left">Ongkos Kirim</div>
                                <div class="item-right"><?php echo rupiah($order->shipping_cost); ?></div>
                            </div>
                            <?php if ($order->payment_method == 'transfer'){ ?>
                                <div class="item">
                                    <div class="item-left">Kode Unik</div>
                                    <div class="item-right"><?php echo rupiah($order->unique_code); ?></div>
                                </div>
                            <?php } ?>
                            <?php if ($order->payment_method == 'coupon'){ ?>
                                <div class="item">
                                    <div class="item-left">Kupon ( <?php echo $payment->CouponCode.' = '.rupiah($payment->CouponValue) ;?> )</div>
                                    <div class="item-right">- <?php echo rupiah($order->disc_value); ?></div>
                                </div>
                            <?php } ?>
                        </div>
                        <hr>    
                        <div class="item">
                            <div class="item-left" style="font-weight: bold;">Total Transaksi</div>
                            <div class="item-right" style="font-weight: bold;color: #D9534F"><?php if ($order->payment_method == 'transfer'){ echo rupiah($order->total+$order->unique_code); } else { echo rupiah($order->total); } ?></div>
                        </div>
                    </div>
                    <!-- <div class="finish-footer">
                        <a class="btn btn-primary" href="<?php echo site_url('member/order_detail/' . encode($order->id)); ?>">Lihat Tagihan</a>
                    </div> -->
                    <div class="finish-middle">
                        <div class="total_paid">
                            <div class="total_paid_head">Jumlah Transaksi</div>
                            <div class="total_paid_price"><?php if ($order->payment_method == 'transfer'){ echo rupiah($order->total+$order->unique_code); } else { echo rupiah($order->total); } ?>
                                <?php if($order->payment_method == 'transfer') : ?>
                                    <span><a href="javascript:void(0)" onclick="copy_text('<?php echo $order->total+$order->unique_code ?>', this)" data-toggle="tooltip" data-placement="top" title="Salin"><u>Salin</u></a></span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php if($order->payment_method == 'transfer' && $order->payment_status == 'Pending') : ?>
                            <?php foreach($payment as $pym) : ?>
                                <?php if($pym->code == 'BCA' || $pym->code == 'bca' || $pym->code == 'Bca') : ?>
                                    <div class="transfer_news">
                                        <div class="transfer_news_title">Berita Transfer</div>
                                        <div class="transfer_news_code"><?php echo $order->transfer_code; ?> <span><a href="javascript:void(0)" onclick="copy_text('<?php echo $order->transfer_code; ?>', this)" data-toggle="tooltip" data-placement="top" title="Salin"><u>Salin</u></a></span></div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <?php if($order->payment_method == 'transfer' && $order->payment_status == 'Pending') : ?>
                        <?php foreach($payment as $pym) : ?>
                            <?php if($pym->code == 'BCA' || $pym->code == 'bca' || $pym->code == 'Bca') : ?>
                                <div class="message_transfer">
                                    <div class="message_transfer_title">
                                        HARAP PERHATIKAN!
                                    </div>
                                    <span>PEMBAYARAN <b>WAJIB</b> MEMASUKKAN <b>NOMINAL</b> DAN <b>BERITA TRANSFER</b> SESUAI DENGAN DIATAS</span>
                                    <br>
                                    <span>APABILA PEMBAYARAN TIDAK DISERTAI BERITA TRANSFER, <b>PEMBELIAN TIDAK DAPAT DIPROSES</b></span>
                                </div>
                                <!-- <div class="message_transfer">
                                    <div class="message_transfer_title">
                                        HARAP PERHATIKAN!
                                    </div>
                                    <ul>
                                        <li>Pembayaran <b>Wajib</b> Memasukkan Nominal Sesuai Dengan Di Atas, apabila nominal tidak sesuai, pesanan tidak dapat diproses</li>
                                        <li><b>wajib</b> melakukan konfirmasi pembayaran beserta bukti transfer</li>
                                        <li>verifikasi pembayaran manual membutuhkan waktu 1 x 24 jam</li>
                                    </ul>
                                </div> -->
                            <?php else : ?>
                                <div class="message_transfer">
                                    <div class="message_transfer_title">
                                        HARAP PERHATIKAN!
                                    </div>
                                    <ul>
                                        <li>Pembayaran <b>Wajib</b> Memasukkan Nominal Sesuai Dengan Di Atas, apabila nominal tidak sesuai, pesanan tidak dapat diproses</li>
                                        <li><b>wajib</b> melakukan konfirmasi pembayaran beserta bukti transfer</li>
                                        <li>verifikasi pembayaran manual membutuhkan waktu 1 x 24 jam</li>
                                    </ul>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php if($order->payment_status == 'Pending') : ?>
                        <?php if (in_array($order->payment_method, ['transfer', 'bca_va', 'mandiri_va', 'bni_va', 'bri_va'])) : ?>
                            <div class="transfer_finish_detail">
                                <div>Lakukan pembayaran ke <?php echo ($order->payment_method == 'transfer') ? 'nomor rekening' : 'rekening virtual' ?> berikut ini.</div>
                                <?php if ($order->payment_method == 'transfer') : ?>
                                    <?php foreach($payment as $pym) : ?>
                                        <div class="payment_finish_bottom">
                                            <img src="<?php echo base_url('files/images/' . $pym->image) ?>" alt="">
                                            <div class="payment_finish_bottom_keterangan">
                                                <span><b><?php echo $pym->account_number ?></b> &nbsp; <a href="javascript:void(0)" onclick="copy_text('<?php echo $pym->account_number ?>')" data-toggle="tooltip" data-placement="top" title="Salin"><u>Salin</u></a></span>
                                                <span><?php echo $pym->account_name ?></span>
                                                <span>Cabang <?php echo $pym->branch ?></span>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php else : ?>
                                    <div class="payment_finish_bottom">
                                        <img src="<?php echo base_url('files/images/' . $payment_method->image) ?>" alt="">
                                        <div class="payment_finish_bottom_keterangan">
                                            <span><b><?php echo $payment->account_number ?></b> &nbsp; <a href="javascript:void(0)" onclick="copy_text('<?php echo $payment->account_number ?>')" data-toggle="tooltip" data-placement="top" title="Salin"><u>Salin</u></a></span>
                                            <span><?php echo $payment->account_name ?></span>
                                            <span><?php echo $payment->account_holder_name ?></span>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php elseif(in_array($order->payment_method, ['alfamart', 'kredivo'])) : ?>
                            <div class="transfer_finish_detail">
                                <div>Lakukan pembayaran <?php echo ($order->payment_method == 'alfamart') ? 'ke Alfamart/Alfamidi terdekat.' : 'melalui Kredivo.' ?></div>
                                <?php if ($order->payment_method == 'alfamart') : ?>
                                    <div class="payment_finish_bottom" style="margin-top: -10px;">
                                        <img src="<?php echo base_url('files/images/' . $payment_method->image) ?>" alt="" style="margin-top: 20px;">
                                        <div class="payment_finish_bottom_keterangan">
                                            <span>Kode Pembayaran : <br> <b><?php echo $payment->account_number ?></b> &nbsp; <a href="javascript:void(0)" onclick="copy_text('<?php echo $payment->account_number ?>')" data-toggle="tooltip" data-placement="top" title="Salin"><u>Salin</u></a></span>
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <div class="payment_finish_bottom">
                                        <!-- <img src="<?php echo base_url('files/images/' . $payment_method->image) ?>" alt="">
                                        <div class="payment_finish_bottom_keterangan">
                                            <span><b><?php echo $payment->account_number ?></b> &nbsp; Salin</span>
                                            <span><?php echo $payment->account_name ?></span>
                                            <span>Cabang <?php echo $payment->branch ?></span>
                                        </div> -->
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if($order->payment_status == 'Pending') : ?>
                        <div class="payment_status_class"><div class="alert alert-success">Transaksi otomatis batal jika pembayaran tidak dilakukan sampai <b><?php echo get_date_time($order->due_date); ?></b></div></div>
                    <?php elseif ($order->payment_status == 'Paid') : ?>
                        <div class="payment_status_class"><div class="alert alert-success">Terima Kasih transaksi anda sudah berhasil</div></div>
                    <?php elseif ($order->payment_status == 'Confirmed') : ?>
                        <div class="payment_status_class"><div class="alert alert-success">Terima Kasih, Pesanan Anda Akan Dikonfirmasi</div></div>
                    <?php else : ?>
                        <div class="payment_status_class"><div class="alert alert-danger">Mohon maaf transaksi anda gagal</div></div>
                    <?php endif; ?>
                </div>
                <div class="buttons_payment_mobile">
                    <?php if($order->payment_method == 'transfer' && $order->payment_status == 'Pending') : ?>
                        <a href="javascript:void(0)" class="btn btn_confirm_transfer" id="payment-confirmation">Konfirmasi Pembayaran</a>
                    <?php endif; ?>
                    <!-- <a href="javascript:void(0)" class="btn">Panduan Transfer</a> -->
                    <a href="<?php echo site_url('member/order_detail/' . encode($order->id)); ?>" class="btn">Riwayat Pembelian</a>
                    <a class="btn" id="payment-instruction">Petunjuk Pembayaran</a>
                </div>
            </div>
        <?php else : ?>
            <div class="row">
                <div class="finish col-sm-9">
                    <div class="finish-header">
                        <h2>Hai <?php echo $customer->fullname; ?>,<br> Terimakasih telah berbelanja di TokoManaMana.com</h2>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="order-info-label">NO. TAGIHAN</div>
                                <div class="order-info-content order-info-content-code"><a href="<?php echo site_url('member/order_detail/' . encode($order->id)); ?>" style="color: #a7c22a;"><?php echo $order->code; ?></a></div>
                            </div>
                            <div class="col-xs-6 text-right">
                                <div class="order-info-label">STATUS</div>
                                <div class="order-info-content" style="color: #666">
                                    <?php
                                    if ($order->payment_status == 'Paid') {
                                        echo 'SUDAH DIBAYAR';
                                    } elseif (in_array($order->payment_status,['Failed','Cancel'])) {
                                        echo 'GAGAL';
                                    } elseif ($order->payment_status=='Expired') {
                                        echo 'KADALUARSA';
                                    } else {
                                        echo'BELUM DIBAYAR';
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php // if ($order->payment_method != 'credit_card') {  ?>
                    <!-- <hr> -->
                    <!-- <div class="finish-subheader" >
                        <?php
                        $payment_method = $this->main->get('payment_methods', array('name' => $order->payment_method));
                        $payment = json_decode($order->payment_to);

                        if ($order->payment_status == 'Pending') {
                            if($order->payment_method == 'transfer'){
                                $total_akhir = $order->total+$order->unique_code;
                            } else {
                                $total_akhir = $order->total;
                            }
                            
                            if (in_array($order->payment_method, ['transfer', 'bca_va', 'mandiri_va', 'bni_va', 'bri_va'])) { ?>
                                <div class="col-xs-12">
                                    <div class="col-xs-6">
                                        <div class="text-center" style="font-size: 20px;">Jumlah Bayar</div>
                                        <div class="finish-amount"><?= rupiah($total_akhir) ?></div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="text-center" style="font-size: 20px;">Berita Transfer</div>
                                        <div class="finish-amount" style="color: #a7c22a"><?= $order->transfer_code ?></div>
                                    </div>
                                </div>
                                <?php    if($order->payment_method == 'transfer'){ ?>
                                            <div class=""><h4>Pembayaran Anda otomatis terkonfirmasi apabila mentransfer sesuai <span style="font-weight: bold; color: #000">Jumlah</span> dan <span style="font-weight: bold; color: #000">Berita Transfer</span> diatas.</h4></div> 
                                <?php    }
                                echo '<div class="text-center">ke rekening ' . ($order->payment_method != 'transfer' ? 'virtual' : '') . ' berikut ini:</div>';
                                if ($order->payment_method != 'transfer') {
                                    echo '<div class="detail-bank">
                                    <img src="' . base_url('files/images/' . $payment_method->image) . '">
                                    <div>' . $payment->account_name . '</div>
                                    <strong>' . $payment->account_number . '</strong>
                                    <div>' . $payment->account_holder_name . '</div>
                                    </div>';
                                } else {
                                    echo '<div class="list-bank">';
                                    foreach ($payment as $pym) {
                                        echo '<div class="detail-bank">
                                        <img src="' . base_url('files/images/' . $pym->image) . '">
                                        <div><strong>' . $pym->account_number . '</strong></div>
                                        <div>' . $pym->account_name . '</div>
                                        <div>' . $pym->name . '</div>
                                        <div>' . $pym->branch . '</div>
                                        </div>';
                                    }
                                    echo '</div>';
                                    echo '<div class="text-center" style="margin-top: 15px;"><button type="button" id="payment-confirmation" class="btn btn-primary">Konfirmasi Pembayaran</button></div>';
                                }
                            } elseif (in_array($order->payment_method, ['alfamart', 'kredivo'])) {
                                echo '<div class="text-center">Silahkan lakukan pembayaran sebesar:</div>
                                    <div class="finish-amount">' . rupiah($order->total) . '</div>';
                                if ($order->payment_method == 'alfamart') {
                                    echo '<div class="text-center">di Alfamart/Alfamidi terdekat</div>';
                                    echo '<div class="detail-bank">
                                    <img src="' . base_url('files/images/' . $payment_method->image) . '">
                                    <div>Kode Pembayaran:</div>
                                    <strong>' . $payment->account_number . '</strong>
                                    </div>';
                                } elseif ($order->payment_method == 'kredivo') {
                                    echo '<div class="text-center">melalui Kredivo</div>';
                                    echo '<div class="detail-bank">
                                    <div class="text-center"><a class="btn" href="' . $payment->payment_url . '">BAYAR SEKARANG</a></div>
                                    </div>';
                                }
                            }
                        }
                        
                        if ($order->payment_status == 'Pending') {
                            echo '<div class="text-center">Transaksi otomatis batal jika pembayaran tidak dilakukan sampai <b>' . get_date_time($order->due_date) . '</b></div>';
                        } elseif ($order->payment_status == 'Paid') {
                            echo '<div class="text-center"><div class="alert alert-success">Terima Kasih transaksi anda sudah berhasil</div></div>';
                        } elseif ($order->payment_status == 'Confirmed') {
                            echo '<div class="text-center"><div class="alert alert-success">Terima Kasih, Pesanan Anda Akan Dikonfirmasi</div></div>';
                        } else {
                            echo '<div class="text-center"><div class="alert alert-danger">Mohon maaf transaksi anda gagal</div></div>';
                        }
                        ?>
                    </div> -->
                    <?php // }   ?>
                    <div class="finish-detail">
                        <div class="text-center text-bold finish-detail-title">Rincian Belanja Anda</div>
                        <div class="items">
                            <?php
                            $products = $this->main->gets('order_product', array('order' => $order->id));
                            $total_price_product = 0;
                            foreach ($products->result() as $product) {
                                $total_price_product += $product->price;
                                ?>
                                <div class="item">
                                    <div class="item-left"><?php echo $product->name . ' ' . rupiah($product->price) . ' x ' . $product->quantity; ?></div>
                                    <div class="item-right"><?php echo rupiah($product->total); ?></div>
                                </div>
                            <?php } ?>
                            <hr>
                            <div class="item">
                                <div class="item-left" style="font-weight: bold;">Total Harga Barang</div>
                                <div class="item-right" style="font-weight: bold;"><?php echo rupiah($total_price_product) ?></div>
                            </div>
                            <div class="item">
                                <div class="item-left">Ongkos Kirim</div>
                                <div class="item-right"><?php echo rupiah($order->shipping_cost); ?></div>
                            </div>
                            <?php if ($order->payment_method == 'transfer'){ ?>
                                <div class="item">
                                    <div class="item-left">Kode Unik</div>
                                    <div class="item-right"><?php echo rupiah($order->unique_code); ?></div>
                                </div>
                            <?php } ?>
                            <?php if ($order->payment_method == 'coupon'){ ?>
                                <div class="item">
                                    <div class="item-left">Kupon ( <?php echo $payment->CouponCode.' = '.rupiah($payment->CouponValue) ;?> )</div>
                                    <div class="item-right">- <?php echo rupiah($order->disc_value); ?></div>
                                </div>
                            <?php } ?>
                        </div>
                        <hr>    
                        <div class="item">
                            <div class="item-left" style="font-weight: bold;">Total Transaksi</div>
                            <div class="item-right" style="font-weight: bold;color: #D9534F"><?php if ($order->payment_method == 'transfer'){ echo rupiah($order->total+$order->unique_code); } else { echo rupiah($order->total); } ?></div>
                        </div>
                    </div>
                    <!-- <div class="finish-footer">
                        <a class="btn btn-primary" href="<?php //echo site_url('member/order_detail/' . encode($order->id)); ?>">Lihat Tagihan</a>
                    </div> -->
                    <div class="finish-middle">
                        <div class="total_paid">
                            <div class="total_paid_head">Jumlah Transaksi</div>
                            <div class="total_paid_price"><?php if ($order->payment_method == 'transfer'){ echo rupiah($order->total+$order->unique_code); } else { echo rupiah($order->total); } ?>
                                <?php if($order->payment_method == 'transfer') : ?>
                                    <span><a href="javascript:void(0)" onclick="copy_text('<?php echo $order->total+$order->unique_code ?>', this)" data-toggle="tooltip" data-placement="top" title="Salin"><u>Salin</u></a></span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <?php if($order->payment_method == 'transfer' && $order->payment_status == 'Pending') : ?>
                            <?php foreach($payment as $pym) : ?>
                                <?php if($pym->code == 'BCA' || $pym->code == 'bca' || $pym->code == 'Bca') : ?>
                                    <div class="transfer_news">
                                        <div class="transfer_news_title">Berita Transfer</div>
                                        <div class="transfer_news_code"><?php echo $order->transfer_code; ?> <span><a href="javascript:void(0)" onclick="copy_text('<?php echo $order->transfer_code; ?>', this)" data-toggle="tooltip" data-placement="top" title="Salin"><u>Salin</u></a></span></div>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <?php if($order->payment_method == 'transfer' && $order->payment_status == 'Pending') : ?>
                        <?php foreach($payment as $pym) : ?>
                            <?php if($pym->code == 'BCA' || $pym->code == 'bca' || $pym->code == 'Bca') : ?>
                                <div class="message_transfer">
                                    <div class="message_transfer_title">
                                        HARAP PERHATIKAN!
                                    </div>
                                    <span>PEMBAYARAN <b>WAJIB</b> MEMASUKKAN <b>NOMINAL</b> DAN <b>BERITA TRANSFER</b> SESUAI DENGAN DIATAS</span>
                                    <br>
                                    <span>APABILA PEMBAYARAN TIDAK DISERTAI BERITA TRANSFER, <b>PEMBELIAN TIDAK DAPAT DIPROSES</b></span>
                                </div>
                                <!-- <div class="message_transfer">
                                    <div class="message_transfer_title">
                                        HARAP PERHATIKAN!
                                    </div>
                                    <ul>
                                        <li>Pembayaran <b>Wajib</b> Memasukkan Nominal Sesuai Dengan Di Atas, apabila nominal tidak sesuai, pesanan tidak dapat diproses</li>
                                        <li><b>wajib</b> melakukan konfirmasi pembayaran beserta bukti transfer</li>
                                        <li>verifikasi pembayaran manual membutuhkan waktu 1 x 24 jam</li>
                                    </ul>
                                </div> -->
                            <?php else : ?>
                                <div class="message_transfer">
                                    <div class="message_transfer_title">
                                        HARAP PERHATIKAN!
                                    </div>
                                    <ul>
                                        <li>Pembayaran <b>Wajib</b> Memasukkan Nominal Sesuai Dengan Di Atas, apabila nominal tidak sesuai, pesanan tidak dapat diproses</li>
                                        <li><b>wajib</b> melakukan konfirmasi pembayaran beserta bukti transfer</li>
                                        <li>verifikasi pembayaran manual membutuhkan waktu 1 x 24 jam</li>
                                    </ul>
                                </div>
                            <?php endif; ?>
                        <?php endforeach;?>
                    <?php endif; ?>
                    <?php if($order->payment_status == 'Pending') : ?>
                        <?php if (in_array($order->payment_method, ['transfer', 'bca_va', 'mandiri_va', 'bni_va', 'bri_va'])) : ?>
                            <div class="transfer_finish_detail">
                                <div>Lakukan pembayaran ke <?php echo ($order->payment_method == 'transfer') ? 'nomor rekening' : 'rekening virtual' ?> berikut ini.</div>
                                <?php if ($order->payment_method == 'transfer') : ?>
                                    <?php foreach($payment as $pym) : ?>
                                        <div class="payment_finish_bottom">
                                            <img src="<?php echo base_url('files/images/' . $pym->image) ?>" alt="">
                                            <div class="payment_finish_bottom_keterangan">
                                                <span><b><?php echo $pym->account_number ?></b> &nbsp; <a href="javascript:void(0)" onclick="copy_text('<?php echo $pym->account_number ?>')" data-toggle="tooltip" data-placement="top" title="Salin"><u>Salin</u></a></span>
                                                <span><?php echo $pym->account_name ?></span>
                                                <span>Cabang <?php echo $pym->branch ?></span>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php else : ?>
                                    <div class="payment_finish_bottom">
                                        <img src="<?php echo base_url('files/images/' . $payment_method->image) ?>" alt="">
                                        <div class="payment_finish_bottom_keterangan">
                                            <span><b><?php echo $payment->account_number ?></b> &nbsp; <a href="javascript:void(0)" onclick="copy_text('<?php echo $payment->account_number ?>')" data-toggle="tooltip" data-placement="top" title="Salin"><u>Salin</u></a></span>
                                            <span><?php echo $payment->account_name ?></span>
                                            <span><?php echo $payment->account_holder_name ?></span>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php elseif(in_array($order->payment_method, ['alfamart', 'kredivo'])) : ?>
                            <div class="transfer_finish_detail">
                                <div>Lakukan pembayaran <?php echo ($order->payment_method == 'alfamart') ? 'ke Alfamart/Alfamidi terdekat.' : 'melalui Kredivo.' ?></div>
                                <?php if ($order->payment_method == 'alfamart') : ?>
                                    <div class="payment_finish_bottom" style="margin-top: -10px;">
                                        <img src="<?php echo base_url('files/images/' . $payment_method->image) ?>" alt="" style="margin-top: 20px;">
                                        <div class="payment_finish_bottom_keterangan">
                                            <span>Kode Pembayaran : <br> <b><?php echo $payment->account_number ?></b> &nbsp; <a href="javascript:void(0)" onclick="copy_text('<?php echo $payment->account_number ?>')" data-toggle="tooltip" data-placement="top" title="Salin"><u>Salin</u></a></span>
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <div class="payment_finish_bottom">
                                        <!-- <img src="<?php echo base_url('files/images/' . $payment_method->image) ?>" alt="">
                                        <div class="payment_finish_bottom_keterangan">
                                            <span><b><?php echo $payment->account_number ?></b> &nbsp; Salin</span>
                                            <span><?php echo $payment->account_name ?></span>
                                            <span>Cabang <?php echo $payment->branch ?></span>
                                        </div> -->
                                    </div>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>

                    <?php if($order->payment_status == 'Pending') : ?>
                        <div class="payment_status_class"><div class="alert alert-success">Transaksi otomatis batal jika pembayaran tidak dilakukan sampai <b><?php echo get_date_time($order->due_date); ?></b></div></div>
                    <?php elseif ($order->payment_status == 'Paid') : ?>
                        <div class="payment_status_class"><div class="alert alert-success">Terima Kasih transaksi anda sudah berhasil</div></div>
                    <?php elseif ($order->payment_status == 'Confirmed') : ?>
                        <div class="payment_status_class"><div class="alert alert-success">Terima Kasih, Pesanan Anda Akan Dikonfirmasi</div></div>
                    <?php else : ?>
                        <div class="payment_status_class"><div class="alert alert-danger">Mohon maaf transaksi anda gagal</div></div>
                    <?php endif; ?>
                </div>
                <div class="buttons_payment col-sm-3">
                    <?php if($order->payment_method == 'transfer' && $order->payment_status == 'Pending') : ?>
                        <a href="javascript:void(0)" class="btn btn_confirm_transfer" id="payment-confirmation">Konfirmasi Pembayaran</a>
                    <?php endif; ?>
                    <!-- <a href="javascript:void(0)" class="btn">Panduan Transfer</a> -->
                    <a href="<?php echo site_url('member/order_detail/' . encode($order->id)); ?>" class="btn">Riwayat Pembelian</a>
                    <a href="javascript:void(0)" class="btn" id="payment-instruction">Petunjuk Pembayaran</a>
                </div>
            </div>
        <?php endif; ?>
    </div>
</section>
<?php if ($order->payment_method == 'transfer' && $order->payment_status == 'Pending') { ?>
    <div id="modal-payment" class="modal" role="dialog" aria-hidden="true" tabindex="-1" style="display: none;">
        <div class="modal-dialog fadeIn animated">
            <div class="modal-content">
                <!-- <form action="<?php echo site_url('auth/member/do_upload') ?>" class="form-horizontal" enctype="multipart/form-data" id="form_upload_image" method="POST"> -->
                    <?php echo form_open_multipart('auth/member/do_upload');?>
                    <input type="hidden" name="id" value="<?php echo encode_id($order->id); ?>">
                    <input type="hidden" name="code_order" value="<?php echo $order->code; ?>">
                    <div class="modal-header">
                        <h1 class="modal-title">Konfirmasi Pembayaran<br><small>Tagihan <?php echo $order->code; ?></small></h1>
                    </div>
                    <div class="modal-body">
                        <div id="message"></div>
                        <div class="form-group row">
                            <label class="control-label col-sm-3">Transfer ke</label>
                            <div class="col-sm-9">
                                <select class="form-control" name="to" required="">
                                    <?php foreach ($payment as $id_bank => $pym) { ?>
                                        <option value="<?php echo $id_bank; ?>"><?php echo $pym->name . ' (' . $pym->account_number . ')'; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <!-- <div class="form-group row">
                            <label class="control-label col-sm-3">Sebesar</label>
                            <div class="col-sm-9">
                                <input name="amount" type="text" class="form-control" required="" value="<?php echo $order->total+$order->unique_code; ?>" placeholder="Total transfer, input hanya angka">
                                Total Transaksi: <b><?php echo rupiah($order->total+$order->unique_code); ?></b>
                            </div>
                        </div> -->
                        <!-- <div class="form-group row">
                            <label class="control-label col-sm-3">Dari Rekening Atas Nama</label>
                            <div class="col-sm-9">
                                <input name="from" type="text" class="form-control" required="" placeholder="Nama yang tertera di rekening pengirim">
                            </div>
                        </div> -->
                        <div class="form-group row">
                            <label class="control-label col-sm-3">Upload Bukti Transaksi</label>
                            <!-- <div class="col-md-9" id="add-image">
                                <button type="button">Pilih Gambar</button> -->
                                <!-- <input name="image" type="" id="add-image-value" required=""> -->
                            <div class="col-sm-9">
                                <input type="file" style="width: 100%" name="image" required="">
                                <span>* Ukuran maksimal 300kb</span>
                            </div>
                            <!-- </div> -->
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-sm-3">Catatan</label>
                            <div class="col-sm-9">
                                <textarea name="note" type="text" class="form-control" placeholder="Catatan khusus yang perlu kami perhatikan" required=""></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn" data-dismiss="modal" style="background-color: #FFF; color: #000;">Batal</button>
                        <input type="submit" name="upload" class="btn">
                        <!-- <button type="button" onclick="submit_payment();" class="btn">Konfirmasi</button> -->
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="filemanager" class="modal">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">File Manager</h5>
                </div>

                <div class="modal-body">
                    <iframe  width="100%" height="550" frameborder="0" src="<?php echo site_url('assets/frontend/filemanager/dialog.php?type=1&editor=false&field_id=add-image-value&relative_url=1'); ?>"></iframe>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<div id="payment-instruction-modal" class="modal">
        <div class="modal-dialog modal-full">
            <div class="modal-content">
                <?php 
                    if($order->payment_method == 'transfer'){
                        foreach ($payment as $py) {
                            if(isset($py->code)) {
                                if($py->code == 'BCA'){
                ?>
                <!-- TRANSFER ATM BCA -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1 class="modal-title" style="color:#a7c22a">Cara Bayar Menggunakan Transfer BCA</h1><br>
                    <p>Rekening atas nama PT Tokomanamana Nasional Indonesia. Verifikasi pembayaran otomatis apabila nilai yang ditransfer sesuai  (termasuk kode unik 3 digit terakhir tagihan transaksi) dan memasukan berita transfer pada kolom berita saat transfer.</p><p> Kode unik adalah nominal yang ditambahkan dalam total pembayaran jika pembeli memilih metode pembayaran transfer. Kode unik berfungsi sebagai pembeda antara pembayaran pembeli dengan pembeli lain, sehingga memudahkan proses verifikasi.</p><p> Setiap transaksi di Tokomanamana akan dikenakan biaya operasional dalam bentuk kode unik pembayaran yang ditanggung oleh pembeli. Perbedaan jumlah dana yang ditransfer (tidak sesuai tagihan atau dibulatkan) akan menghambat proses verifikasi pembayaran.</p><p> Berikut ini adalah langkah-langkah yang dapat pembeli lakukan jika ingin membayar transaksi dengan cara transfer.</p>
                    <strong>PENTING!</strong><strong>Pembeli wajib mengisi berita transfer pada saat pembayaran demi kelancaran proses verifikasi. Apabila tidak ada berita transfer, proses verifikasi tidak dapat di proses.</strong>
                    <ul class="nav nav-pills">
                      <li class="nav-item active">
                        <a data-toggle="tab" class="nav-link" href="#atm_bca">Melalui ATM</a>
                      </li>
                      <li class="nav-item">
                        <a data-toggle="tab" class="nav-link" href="#mobile_bca">Melalui Mobile BCA</a>
                      </li>
                      <li class="nav-item">
                        <a data-toggle="tab" class="nav-link" href="#klik_bca">Melalui KlikBCA</a>
                      </li>
                    </ul>
                    <br>
                    <div class="tab-content">
                        <div id="atm_bca" class="tab-pane fade in active">
                            <div>1. <strong>Masukkan kartu BCA, Pilih bahasa pengantar</strong>, Agar lebih mudah, sebaiknya pilih “Indonesia”.</div>
                            <div>2. <b>Masukkan 6 digit PIN</b> Anda.</div>
                            <div>3. Setelah Anda memasukkan PIN, Pilih <b>“menu lain”</b>.</div>
                            <div>4. Setelah itu silahkan pilih <b>“transfer”</b>.</div>
                            <div>5. Setelah memilih “transfer”, Silahkan pilih <b>“dari rekening tabungan”</b>.</div>
                            <div>6. Untuk melakukan transfer ke sesama BCA, pilih  <b>“ke rekening BCA”</b>.</div>
                            <div>7. Masukan no <b>rekening tujuan</b>.</div>
                            <div>8. <b>Masukkan nominal uang</b> yang akan Anda transfer.</div>
                            <div>9. Anda akan dimintai <b>konfirmasi ulang</b> tentang kebenaran data yang telah dimasukkan. Pilih <b>“ya”</b> jika semua data sudah sesuai,</div> 
                            <div>10. <b>Transaksi selesai</b>, simpan bukti transfer sebagai bukti transaksi.</div>
                        </div>
                        <div id="mobile_bca" class="tab-pane fade">
                            <div>1. Pilih menu “m-BCA” di ponsel Anda dan tekan OK/YES</div>
                            <div>2. Pilih menu “m-Transfer” dan tekan OK/YES</div>
                            <div>3. Pilih “Antar Rek” dan tekan OK/YES</div>
                            <div>4. Pilih “Mata Uang (Rp/USD/SGD)” dan tekan OK/YES</div>
                            <div>5. Masukkan Jumlah Uang yang akan ditransfer dan tekan OK/YES</div>
                            <div>6. Masukkan No. RekeningTujuan yang akan ditransfer dan tekan OK/YES</div>
                            <div>7. Masukkan PIN m-BCA Anda dan tekan OK/YES</div>
                            <div>8. Muncul keterangan "berita" yang dapat Anda isi/kosongkan</div>
                            <div>9. Jika Anda memiliki lebih dari 1 (satu) rekening yang terhubung dengan m-BCA, pilih nomor rekening yang akan digunakan, kemudian tekan OK/YES</div>
                            <div>10. <b>Pastikan data transaksi yang muncul di layar konfirmasi telah benar</b>. Jika benar, tekan OK/YES dan lanjutkan ke no. 11. Jika tidak, diamkan selama beberapa saat sampai message tersebut hilang dan otomatis transaksi tersebut akan dibatalkan</div>
                            <div>11. Masukkan PIN m-BCA Anda dan tekan OK/YES</div>
                            <div>12. Setelah beberapa saat, Anda akan menerima message yang berisi informasi transaksi transfer Anda Berhasil atau Gagal.</div>
                        </div>
                        <div id="klik_bca" class="tab-pane fade">
                            <div>1. Buka klikbca.com lalu login</div>
                            <div>2. Pilih menu “Transfer Dana”, pilih “Transfer Ke Rek BCA”</div>
                            <div>3. Pilih rekening tujuan, masukkan jumlah dan berita transfer sesuai aplikasi</div>
                            <div>4. Scroll kebawah, pilih “transfer sekarang”, klik tombol <b>lanjutkan</b></div>
                            <div>5. <b>Pastikan data transaksi yang muncul di layar konfirmasi telah benar</b> dan lanjutkan </div>
                            <div>6. Transfer uang telah selesai, simpan bukti transfer sebagai bukti transaksi</div>
                        </div>
                    </div>
                </div> 
            <?php } else if($py->code == 'Mandiri') {
            ?>
                <!-- TRANSFER ATM MANDIRI -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1 class="modal-title" style="color:#a7c22a">Cara Bayar Menggunakan Transfer Bank Mandiri</h1><br>
                    <p>Rekening atas nama PT Tokomanamana Nasional Indonesia. Verifikasi pembayaran otomatis apabila nilai yang ditransfer sesuai  (termasuk kode unik 3 digit terakhir tagihan transaksi) dan memasukan berita transfer pada kolom berita saat transfer. Kode unik adalah nominal yang ditambahkan dalam total pembayaran jika pembeli memilih metode pembayaran transfer. Kode unik berfungsi sebagai pembeda antara pembayaran pembeli dengan pembeli lain, sehingga memudahkan proses verifikasi. Setiap transaksi di Tokomanamana akan dikenakan biaya operasional dalam bentuk kode unik pembayaran yang ditanggung oleh pembeli. Perbedaan jumlah dana yang ditransfer (tidak sesuai tagihan atau dibulatkan) akan menghambat proses verifikasi pembayaran.</p><p>Berikut ini adalah langkah-langkah yang dapat pembeli lakukan jika ingin membayar transaksi dengan cara transfer.</p>
                    <ul class="nav nav-pills">
                      <li class="nav-item active">
                        <a data-toggle="tab" class="nav-link" href="#atm_mandiri">Melalui ATM</a>
                      </li>
                      <li class="nav-item">
                        <a data-toggle="tab" class="nav-link" href="#mandiri_online">Melalui Mandiri Online</a>
                      </li>
                    </ul>
                    <br>
                    <div class="tab-content">
                        <div id="atm_mandiri" class="tab-pane fade in active">
                            <div>1. Masukan kartu ATM Mandiri</div>
                            <div>2. Pilih Menu Bahasa Indonesia</div>
                            <div>3. Masukan 6 digit Pin anda dengan benar</div>
                            <div>4. Pilih menu “transaksi lainnya”</div>
                            <div>5. Pilih menu “Transfer”</div>
                            <div>6. Pilih menu “ke Rekening Mandiri”</div>
                            <div>7. Masukan nomor rekening tujuan (pilih Benar)</div>
                            <div>8. masukan jumlah uang yang akan ditransfer (pilih Benar)</div>
                            <div>9. Perhatikan konfirmasi transfer, jika benar pilih Benar</div>
                            <div>10. Transaksi anda telah selesai, pilih Keluar</div>
                        </div>
                        <div id="mandiri_online" class="tab-pane fade">
                            <div>1. Login di aplikasi Mandiri Online</div>
                            <div>2. Pilih "Transfer"</div>
                            <div>3. Pilih "Ke Rekening Mandiri"</div>
                            <div>4. Tentukan "Rekening Sumber"</div>
                            <div>5. Isi "Rekening Tujuan" dan "Jumlah" Transfer</div>
                            <div>6. "Konfirmasi" dan Masukkan "MPIN"</div>
                        </div>
                    </div>
                </div>
                <!-- END Transfer Mandiri -->
                <?php
            } else if($py->code == 'BRI'){
                ?>

                <!-- TRANSFER ATM BRI -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1 class="modal-title" style="color:#a7c22a">Cara Bayar Menggunakan Transfer Bank BRI</h1><br>
                    <p>Rekening atas nama PT Tokomanamana Nasional Indonesia. Verifikasi pembayaran otomatis apabila nilai yang ditransfer sesuai  (termasuk kode unik 3 digit terakhir tagihan transaksi) dan memasukan berita transfer pada kolom berita saat transfer.</p><p>
                    Kode unik adalah nominal yang ditambahkan dalam total pembayaran jika pembeli memilih metode pembayaran transfer. Kode unik berfungsi sebagai pembeda antara pembayaran pembeli dengan pembeli lain, sehingga memudahkan proses verifikasi.</p><p>
                    Setiap transaksi di Tokomanamana akan dikenakan biaya operasional dalam bentuk kode unik pembayaran yang ditanggung oleh pembeli. Perbedaan jumlah dana yang ditransfer (tidak sesuai tagihan atau dibulatkan) akan menghambat proses verifikasi pembayaran.</p><p>
                    Berikut ini adalah langkah-langkah yang dapat pembeli lakukan jika ingin membayar transaksi dengan cara transfer.</p>
                    <ul class="nav nav-pills">
                      <li class="nav-item active">
                        <a data-toggle="tab" class="nav-link" href="#atm_bri">Melalui ATM</a>
                      </li>
                      <li class="nav-item">
                        <a data-toggle="tab" class="nav-link" href="#internet_banking_bri">Melalui Internet Banking BRI</a>
                      </li>
                    </ul>
                    <br>
                    <div class="tab-content">
                        <div id="atm_bri" class="tab-pane fade in active">
                            <div>1. Masukan Kartu ATM</div>
                            <div>2. Pilih “Bahasa Indonesia”</div>
                            <div>3. Pilih Lanjutkan</div>
                            <div>4. Masukan PIN ATM</div>
                            <div>5. Pilih Transaksi Lain</div>
                            <div>6. Pilih Transfer</div>
                            <div>7. Selanjutnya Pilih Bank BRI</div>
                            <div>8. Masukan Nomor Rekening Tujuan -> BENAR</div>
                            <div>9. Masukan Jumlah Transfer -> BENAR</div>
                            <div>10. Konfirmasi Transfer -> YA</div>
                            <div>11. Transaksi Selesai</div>
                        </div>
                        <div id="internet_banking_bri" class="tab-pane fade">
                            <div>1. Login ke BRI Mobile</div>
                            <div>2. Tekan menu <b>“Transfer”</b></div>
                            <div>3. Kemudian pilih menu <b>“Transfer dalam Bank”</b></div>
                            <div>4. Masukkan nomor Rekening Tujuan dan jumlah pembayaran.</div>
                            <div>5. Tekan <b>“Kirim”</b> dan masukan password untuk melanjutkan transaksi</div>
                            <div>6. Transaksi berhasil</div>
                            <div>7. Setelah beberapa saat, Anda akan menerima SMS konfirmasi</div>
                        </div>
                    </div>
                </div>
                <!-- END Transfer BRI -->
                <?php
            } } } } else if($order->payment_method == 'bca_va'){
                ?>
                <!-- BCA Virtual Account -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1 class="modal-title" style="color:#a7c22a">Cara Bayar Menggunakan BCA Virtual Account</h1><br>
                    <p>Berikut adalah syarat dan ketentuan pembayaran transaksi menggunakan BCA Virtual Account:</p>
                    <div>1.  Bila pembeli masuk sebagai tamu atau pembeli dengan nomor telepon yang belum terverifikasi di Tokomanamana, maka harus melakukan konfirmasi agar dapat melanjutkan pembelian.
                    Virtual account yang dimasukkan adalah kode perusahaan + nomor yang akan dibuatkan secara otomatis oleh sistem.</div>
                    <div>Contoh: 39229 - 081340XXXXXX.</div>
                    <div>2.  Rekening BCA Virtual Account tersebut atas nama PT. Tokomanamana Nasional Indonesia. Pembayaran transaksi menggunakan BCA Virtual Account tanpa dikenakan biaya pelayanan.</div>
                    <div>3.  Satu nomor virtual account hanya berlaku untuk satu akun (tidak berubah-ubah).</div>
                    <div>4.  Pembayaran dengan virtual account hanya berlaku untuk satu tagihan terbaru.</div>
                    <div>5.  Pembeli tidak dapat mengganti nomor telepon yang sudah terdaftar di Tokomanamana sebelum tagihan terbaru virtual account dibayar.</div><br>
                    <p>Dibawah ini adalah langkah-langkah yang dapat pembeli lakukan ketika ingin membayar transaksi mengggunakan BCA Virtual Account:</p>
                    <ul class="nav nav-pills">
                      <li class="nav-item active">
                        <a data-toggle="tab" class="nav-link" href="#atm_va_bca">Melalui ATM</a>
                      </li>
                      <li class="nav-item">
                        <a data-toggle="tab" class="nav-link" href="#va_bca_mobile">BCA Mobile</a>
                      </li>
                      <li class="nav-item">
                        <a data-toggle="tab" class="nav-link" href="#va_klikbca">KlikBCA Pribadi</a>
                      </li>
                      <li class="nav-item">
                        <a data-toggle="tab" class="nav-link" href="#va_klikbca_bisnis">KlikBCA Bisnis</a>
                      </li>
                    </ul>
                    <br>
                    <div class="tab-content">
                        <div id="atm_va_bca" class="tab-pane fade in active">
                            <div>1. Masukkan kartu ke mesin ATM</div>
                            <div>2. Masukkan 6 digit PIN Anda</div>
                            <div>3. Pilih <b>“Transaksi Lainnya”</b></div>
                            <div>4. Pilih <b>“Transfer”</b></div>
                            <div>5. Pilih ke <b>“ke Rekening BCA Virtual Account”</b></div>
                            <div>6. Masukkan nomor BCA Virtual Account Anda, kemudian tekan <b>“Benar”</b></div>
                            <div>7. Periksa jumlah yang akan dibayarkan, selanjutnya tekan <b>“Benar”</b></div>
                            <div>8. Validasi pembayaran Anda. Pastikan semua detail transaksi yang ditampilkan sudah benar, kemudian pilih <b>“Ya”</b></div>
                            <div>9. Pembayaran Anda telah selesai. Tekan <b>“Tidak”</b> untuk menyelesaikan transaksi, atau tekan <b>“Ya”</b> untuk melakukan transaksi lainnya</div>
                        </div>
                        <div id="va_bca_mobile" class="tab-pane fade">
                            <div>1. Silahkan login pada aplikasi BCA Mobile</div>
                            <div>2. Pilih <b>“m-BCA”</b>, lalu masukkan kode akses m-BCA</div>
                            <div>3. Pilih <b>“m-Transfer”</b></div>
                            <div>4. Pilih <b>“BCA Virtual Account”</b></div>
                            <div>5. Masukkan nomor BCA Virtual Account Anda, atau pilih dari Daftar Transfer</div>
                            <div>6. Periksa jumlah yang akan dibayarkan</div>
                            <div>7. Masukkan PIN m-BCA Anda</div>
                            <div>8. Transaksi telah berhasil</div>
                        </div>
                        <div id="va_klikbca" class="tab-pane fade">
                            <div>1. Silahkan login pada aplikasi KlikBCA Individual</div>
                            <div>2. Masukkan User ID dan PIN Anda</div>
                            <div>3. Pilih <b>“Transfer Dana”</b> </div>
                            <div>4. Pilih <b>“Transfer ke BCA Virtual Account”</b></div>
                            <div>5. Masukkan nomor BCA Virtual Account Anda atau pilih dari Daftar Transfer</div>
                            <div>6. Periksa jumlah yang akan dibayarkan</div>
                            <div>7. Validasi pembayaran. Pastikan semua datanya sudah benar, lalu masukkan kode yang diperoleh dari KEYBCA APPLI 1, kemudian klik <b>“Kirim”</b></div>
                            <div>8. Pembayaran telah selesai dilakukan</div>
                        </div>
                        <div id="va_klikbca_bisnis" class="tab-pane fade">
                            <div>1. Silahkan melakukan login di KlikBCA Bisnis</div>
                            <div>2. Pilih <b>“Transfer Dana”</b> > <b>“Daftar Transfer”</b> > <b>“Tambah”</b></div>
                            <div>3. Masukkan nomor BCA Virtual Account, lalu “Kirim”</div>
                            <div>4. Pilih <b>“Transfer Dana”</b></div>
                            <div>5. Pilih <b>“Ke BCA Virtual Account”</b></div>
                            <div>6. Pilih rekening sumber dana dan BCA Virtual Account tujuan</div>
                            <div>7. Periksa jumlah yang akan dibayarkan, lalu pilih <b>“Kirim”</b></div>
                            <div>8. Validasi Pembayaran. Sampai tahap ini berarti data berhasil di input. Kemudian pilih <b>“simpan”</b></div>
                            <div>9. Pilih <b>“Transfer Dana”</b> > <b>“Otorisasi Transaksi”</b>, lalu pilih transaksi yang akan diotorisasi</div>
                            <div>10. Pembayaran telah selesai dilakukan</div>
                        </div>
                    </div>
                </div> 
                <!-- END BCA Virtual Account -->
                <?php 
            } else if($order->payment_method == 'bri_va'){
                ?>
                <!-- BRI Virtual Account -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1 class="modal-title" style="color:#a7c22a">Cara Bayar Menggunakan BRI Virtual Account</h1><br>
                    <p>Berikut adalah syarat dan ketentuan pembayaran transaksi menggunakan BRI Virtual Account:</p>
                    <div>1.  Bila pembeli masuk sebagai tamu atau pembeli dengan nomor telepon yang belum terverifikasi di Tokomanamana, maka harus melakukan konfirmasi agar dapat melanjutkan pembelian.</div>
                    <div>2.  Rekening BRI Virtual Account tersebut atas nama PT. Tokomanamana Nasional Indonesia. Pembayaran transaksi menggunakan BRI Virtual Account tanpa dikenakan biaya pelayanan.</div>
                    <div>3.  Satu nomor virtual account hanya berlaku untuk satu akun (tidak berubah-ubah).</div>
                    <div>4.  Pembayaran dengan virtual account hanya berlaku untuk satu tagihan terbaru.</div>
                    <div>5.  Pembeli tidak dapat mengganti nomor telepon yang sudah terdaftar di Tokomanamana sebelum tagihan terbaru virtual account dibayar.</div><br>
                    <p>Dibawah ini adalah langkah-langkah yang dapat pembeli lakukan ketika ingin membayar transaksi mengggunakan BRI Virtual Account:</p>
                    <ul class="nav nav-pills">
                      <li class="nav-item active">
                        <a data-toggle="tab" class="nav-link" href="#atm_va_bri">Melalui ATM</a>
                      </li>
                      <li class="nav-item">
                        <a data-toggle="tab" class="nav-link" href="#va_bri_mobile">Melalui Mobile Banking</a>
                      </li>
                      <li class="nav-item">
                        <a data-toggle="tab" class="nav-link" href="#va_internet_banking_bri">Melalui Internet Banking</a>
                      </li>
                    </ul>
                    <br>
                    <div class="tab-content">
                        <div id="atm_va_bri" class="tab-pane fade in active">
                            <div>1. Pilih menu <b>“Transaksi Lain”</b></div>
                            <div>2. Selanjutnya pilih menu <b>“Lainnya”</b></div>
                            <div>3. Kemudian pilih menu <b>“Pembayaran”</b></div>
                            <div>4. Tekan pada pilihan <b>“BRIVA”</b></div>
                            <div>5. Masukkan Nomor BRI Virtual Account (Contoh 26215-xxxxxxxxxxxx) lalu tekan <b>“Benar”</b></div>
                            <div>6. Selanjutnya muncul konfirmasi pembayaran, tekan <b>“Ya”</b> jika semua data sudah sesuai</div>
                            <div>7. Transaksi berhasil dilakukan</div>
                        </div>
                        <div id="va_bri_mobile" class="tab-pane fade">
                            <div>1. Login ke mobile banking</div>
                            <div>2. Tekan menu <b>“Pembayaran”</b></div>
                            <div>3. Kemudian pilih menu <b>“BRIVA”</b></div>
                            <div>4. Masukkan nomor BRI Virtual Account Anda beserta jumlah pembayaran</div>
                            <div>5. Masukkan nomor PIN Anda</div>
                            <div>6. Tekan <b>“OK”</b> untuk melanjutkan transaksi</div>
                            <div>7. Transaksi berhasil</div>
                            <div>8. Setelah beberapa saat, Anda akan menerima SMS konfirmasi</div>
                        </div>
                        <div id="va_internet_banking_bri" class="tab-pane fade">
                            <div>1. Buka website internet banking BRI</div>
                            <div>2. Login dengan memasukkan <b>User ID</b> dan <b>Password</b></div>
                            <div>3. Pilih pada menu <b>“Pembayaran”</b></div>
                            <div>4. Selanjutnya pilih menu <b>“BRIVA”</b></div>
                            <div>5. Kemudian pilih rekening <b>Pembayar</b></div>
                            <div>6. Masukkan Nomor Virtual Account BRI Anda (Contoh 26215-xxxxxxxxxxxx) </div>
                            <div>7. Masukkan nominal yang akan dibayarkan</div>
                            <div>8. Masukkan password dan Mtoken Anda</div>
                            <div>9. Transaksi berhasil dilakukan</div>
                        </div>
                    </div>
                </div>
                <!-- END BRI Virtual Account -->
                <?php
            } else if($order->payment_method == 'mandiri_va'){
                ?>
                <!-- MANDIRI Virtual Account -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1 class="modal-title" style="color:#a7c22a">Cara Bayar Menggunakan Mandiri Virtual Account</h1><br>
                    <p>Berikut adalah syarat dan ketentuan pembayaran transaksi menggunakan MANDIRI Virtual Account:</p>
                    <div>1.  Bila pembeli masuk sebagai tamu atau pembeli dengan nomor telepon yang belum terverifikasi di Tokomanamana, maka harus melakukan konfirmasi agar dapat melanjutkan pembelian.</div>
                    <div>2.  Rekening MANDIRI Virtual Account tersebut atas nama PT. Tokomanamana Nasional Indonesia. Pembayaran transaksi menggunakan MANDIRI Virtual Account tanpa dikenakan biaya pelayanan.</div>
                    <div>3.  Satu nomor virtual account hanya berlaku untuk satu akun (tidak berubah-ubah).</div>
                    <div>4.  Pembayaran dengan virtual account hanya berlaku untuk satu tagihan terbaru.</div>
                    <div>5.  Pembeli tidak dapat mengganti nomor telepon yang sudah terdaftar di Tokomanamana sebelum tagihan terbaru virtual account dibayar.</div>
                    <p>Dibawah ini adalah langkah-langkah yang dapat pembeli lakukan ketika ingin membayar transaksi mengggunakan MANDIRI Virtual Account:</p>
                    <ul class="nav nav-pills">
                      <li class="nav-item active">
                        <a data-toggle="tab" class="nav-link" href="#atm_va_mandiri">Melalui ATM</a>
                      </li>
                      <li class="nav-item">
                        <a data-toggle="tab" class="nav-link" href="#va_bri_mobile">Mobile Banking</a>
                      </li>
                      <li class="nav-item">
                        <a data-toggle="tab" class="nav-link" href="#va_internet_banking_bri">Melalui Internet Banking</a>
                      </li>
                    </ul>
                    <br>
                    <div class="tab-content">
                        <div id="atm_va_mandiri" class="tab-pane fade in active">
                            <div>1. Silahkan pilih menu “Bayar/Beli”</div>
                            <div>2. Selanjutnya akan muncul tipe pembayaran seperti Multi Payment dan Bill Payment (PLN, PDAM, dll), untuk pembayaran VA pilih “Multi Payment”</div>
                            <div>3. Silahkan masukkan kode perusahaan, contoh: “88908” (Xendit), lalu pilih “Benar”</div>
                            <div>4. Masukkan nomor Virtual Account Anda (contoh: 8890802001287578) dan klik “Benar”</div>
                            <div>5. Isikan nominal yang akan dibayarkan, kemudian tekan “Benar”</div>
                            <div>6. Jangan lupa untuk memeriksa informasi yang tertera pada layar. Pastikan semua informasi dan total tagihan yang ditampilkan sudah benar. Jika benar, tekan angka 1 dan pilih “Ya”</div>
                            <div>7. Periksa layar konfirmasi dan pilih “Ya” untuk melakukan pembayaran</div>
                            <div>8. Bukti pembayaran dalam bentuk struk agar disimpan sebagai bukti pembayaran yang sah dari Bank Mandiri</div>
                            <div>9. Transaksi Anda telah selesai</div>
                        </div>
                        <div id="va_mandiri_mobile" class="tab-pane fade">
                            <div>1. Silahkan login ke mobile banking Anda.</div>
                            <div>2. Klik <b>“Icon Menu”</b> di sebelah kiri atas.</div>
                            <div>3. Pilih menu <b>“Pembayaran”</b></div>
                            <div>4. Pilih buat <b>“Pembayaran Baru”</b></div>
                            <div>5. Pilih <b>“Multi Payment”</b></div>
                            <div>6. Klik <b>“Penyedia Jasa”</b> atau <b>“Service Provider”</b>,  kemudian pilih kode perusahaan, contoh:  “Xendit 88908”</div>
                            <div>7. Pilih <b>“No. Virtual”</b></div>
                            <div>8. Masukkan nomor Virtual Account Anda dengan kode perusahaan (contoh 8890802001287578), kemudian pilih <b>“Tambah Sebagai Nomor Baru”</b></div>
                            <div>9. Masukkan nominal lalu pilih <b>“Konfirmasi”</b> dan <b>“lanjut”</b></div>
                            <div>10. Selanjutnya akan muncul tampilan konfirmasi pembayaran. Pastikan semua informasi dan total tagihan sudah benar. Jika sudah benar, lalu scroll ke bawah  dan pilih <b>“Konfirmasi”</b></div>
                            <div>11. Masukkan PIN Anda dan pilih <b>“OK”</b></div>
                            <div>12. Sampai pada tahap ini, berarti transaksi dengan menggunakan VA telah berhasil dilakukan.</div>
                        </div>
                        <div id="va_internet_banking_mandiri" class="tab-pane fade">
                            <div>1. Login ke website Mandiri Online dengan memasukkan <b>user ID dan PIN</b></div>
                            <div>2. Pilih menu <b>“Pembayaran”</b></div>
                            <div>3. Pilih menu <b>“Multi Payment”</b></div>
                            <div>4. Silahkan pilih <b>“No Rekening Anda”</b></div>
                            <div>5. Pilih <b>“Penyedia Jasa”</b>, contoh: <b>“Xendit 88908”</b></div>
                            <div>6. Pilih <b>“No Virtual Account”</b></div>
                            <div>7. Masukkan <b>“Nomor Virtual Account”</b></div>
                            <div>8. Masuk ke halaman konfirmasi 1</div>
                            <div>9. Apabila sudah sesuai, klik <b>“Tagihan Total”</b>, kemudian <b>“Lanjutkan”</b></div>
                            <div>10. Masuk ke halaman konfirmasi 2</div>
                            <div>11. Masukkan Challenge Code yang dikirimkan ke Token Internet Banking Anda, kemudian <b>“Kirim”</b></div>
                            <div>12. Anda akan masuk ke halaman konfirmasi jika pembayaran telah selesai</div>
                        </div>
                    </div>
                </div>
                <!-- END MANDIRI Virtual Account -->
                <?php
            } else if($order->payment_method == 'alfamart'){
                ?>
                <!-- Retail Outlet -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1 class="modal-title" style="color:#a7c22a">Cara Bayar Menggunakan Retail Outlet</h1><br>
                    <p>Lakukan pembayaran di retail outlet seperti Alfamart (termasuk Alfamidi, DAN-DAN dan Lawson) dengan menunjukkan Kode Pembayaran yang tercantum pada rincian tagihan ke petugas kasir.</p>
                    <p>Nilai maksimum per transaksi yang diperbolehkan adalah Rp5.000.000. Pembeli akan dikenakan biaya tambahan per transaksi sebesar Rp2.500 (di luar total belanja) yang dibayarkan langsung saat pembayaran melalui gerai Alfamart (termasuk Alfamidi, DAN-DAN dan Lawson). Ini merupakan ketentuan sepenuhnya dari Alfamart dan dapat berubah sewaktu-waktu tanpa pemberitahuan sebelumnya. Metode pembayaran ini bergantung kepada jam operasional cabang gerai Alfamart pilihan pembeli. Hal tersebut sepenuhnya berada di luar kuasa dan tanggung jawab Tokomanamana.</p>
                    <p>Berikut ini adalah langkah-langkah yang dapat pembeli lakukan jika ingin membayar transaksi menggunakan Aflamart.</p>
                    <ul class="nav nav-pills">
                      <li class="nav-item active">
                        <a data-toggle="tab" class="nav-link" href="#alfamart">Alfamart</a>
                      </li>
                    </ul>
                    <br>
                    <div class="tab-content">
                        <div id="alfamart" class="tab-pane fade in active">
                            <div>1. Tunjukkan <b>Kode Pembayaran</b> ke petugas kasir Alfamart.</div>
                            <!-- GAMBAR ALFAMART -->
                            <div>2. Simpan bukti pembayaran yang diberikan oleh petugas kasir Alfamart sebagai bukti transaksi apabila sewaktu-waktu dibutuhkan.</div>
                        </div>
                    </div>
                </div>
                <!-- END Retail Outlet -->
                <?php
            } else if($order->payment_method == 'bca_klikpay'){
                ?>
                <!-- BCA KlikPay -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1 class="modal-title" style="color:#a7c22a">Cara Bayar Menggunakan BCA KlikPay</h1><br>
                    <p>BCA KlikPay merupakan fitur pembayaran dari Bank BCA yang dapat digunakan untuk bertransaksi di Tokomanamana. Syarat utama untuk menggunakan fasilitas ini adalah sudah terdaftar sebagai pengguna KlikBCA individu atau BCA Card.</p>
                    <p>Berikut ini adalah langkah-langkah yang dapat pembeli lakukan jika ingin membayar transaksi menggunakan BCA KlikPay.</p>
                    <div class="tab-content">
                        <div id="bca_klikpay" class="tab-pane fade in active">
                            <div>1. Login menggunakan account kamu di halaman BCA KlikPay. Setelah kamu berhasil login, detail transaksi akan muncul secara otomatis.</div>
                            <div>2. Periksa kembali detail transaksi kamu, pilih pembayaran KlikBCA atau BCA Kartu Kredit, bila semuanya telah sesuai klik “Kirim“.</div>
                            <div>3. Bila semuanya telah sesuai klik KIRIM OTP (One Time Password)</div>
                            <div>4. Melalui SMS ke nomor handphone kamu, BCA KlikPay akan mengirimkan kode OTP (One Time Password) dan segera masukkan kode OTP (One Time Password) tersebut pada website BCA KlikPay.</div>
                            <div>5. Transaksi Berhasil.</div>
                        </div>
                    </div>
                </div>
                <!-- END BCA KlikPay -->
                <?php

            } else if($order->payment_method == 'ovo'){
                ?>
                <!-- OVO -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1 class="modal-title" style="color:#a7c22a">Cara Bayar Menggunakan OVO</h1><br>
                    <p>Pembeli dapat melakukan pembayaran melalui OVO dengan mudah dan cepat. Wajib memiliki akun OVO sebelum memilih menggunakan metode ini. Pastikan saldo OVO pembeli tersedia dan cukup dengan nilai jumlah transaksi di Tokomanamana.</p>
                    <p>Berikut ini penjelasan cara bayar menggunakan ovo:</p>
                    <div class="tab-content">
                        <div id="bca_klikpay" class="tab-pane fade in active">
                            <div>1. Pilih menu “Pembayaran”</div>
                            <div>2.  Pilih menu “OVO”</div>
                            <div>3.  Pembeli akan diarahkan ke laman OVO</div>
                            <div>4.  Masukkan nomor yang telah terdaftar di aplikasi OVO pembeli</div>
                            <div>5.  Pastikan saldo mencukupi dengan nilai jumlah transaksi</div>
                            <div>6.  Masukkan PIN OVO pembeli</div>
                            <div>7.  Proses pembayaran selesai</div>
                        </div>
                    </div>
                </div>
                <!-- END OVO -->
                <?php

            } else if($order->payment_method == 'kredivo'){
                ?>
                <!-- OVO -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1 class="modal-title" style="color:#a7c22a">Cara Bayar Menggunakan Kredivo</h1><br>
                    <p>Dengan menggunakan kredivo, pembeli dapat membeli produk yang ada di tokomanamana dengan kredit limit tertentu tanpa uang muka dan pembayaran dapat dilakukan dalam 30 hari sejak pembelian. Dapat juga dicicil dengan tenor 3 bulan, 6 bulan atau 12 bulan. Pembeli wajib melakukan registrasi di aplikasi Kredivo sebelum melakukan pembelian.</p>
                    <p>Berikut ini penjelasan cara bayar menggunakan kredivo:</p>
                    <div class="tab-content">
                        <div id="bca_klikpay" class="tab-pane fade in active">
                            <div>1. Pilih menu “Pembayaran”</div>
                            <div>2.  Pilih menu “Kredivo”</div>
                            <div>3.  Pembeli akan diarahkan ke laman Kredivo</div>
                            <div>4.  Register atau Login</div>
                            <div>5.  Ikuti langkah-langkah selanjutnya</div>
                            <div>6.  Pilih tenor pembayaran. (Pembayaran penuh atau cicilan)</div>
                            <div>7.  Masukkan OTP yang dikirimkan.</div>
                            <div>8.  Apabila pembayaran berhasil, pembeli dapat mengecek pada menu transaksi di aplikasi kredivo dan dapat melihat jadwal pembayaran pada halaman transaksi.</div>
                        </div>
                    </div>
                </div>
                <!-- END OVO -->
                <?php

            // } else if($payment_method == 'credit_card' || $payment_method == 'creditcard' || $payment_method == 'creditcard01' || $payment_method == 'creditcard03' || $payment_method == 'creditcard06' || $payment_method == 'creditcard12'){
            } else {
                ?>

                <!-- Kartu Kredit -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h1 class="modal-title" style="color:#a7c22a">Cara Bayar Menggunakan Kartu Kredit</h1><br>
                    <p>Tokomanamana menerima pembayaran transaksi via kartu kredit berlogo Visa, Mastercard yang dikeluarkan oleh Bank di Indonesia. Berikut keuntungan pembayaran transaksi via kartu kredit:</p>
                    <div>•   Pembayaran akan langsung terverifikasi secara otomatis sehingga pesanan dapat langsung diteruskan ke penjual.</div><br>
                    <div>•   Telah didukung dengan teknologi 3D Secure yang menjamin keamanan transaksi kartu kredit pembeli.</div><br>

                    <p>Hal yang Dapat Dilakukan Jika Transaksi Kartu Kredit Tidak Berhasil
                    Jika transaksi Kartu kredit pembeli gagal, pastikan :</p>
                    <div>1.  Kartu kredit pembeli termasuk dalam daftar bank yang tersedia di Indonesia.</div>
                    <div>2.  Limit kartu kredit pembeli mencukupi.</div>
                    <div>3.  Kartu kredit pembeli berlogo visa/mastercard dan sudah terdaftar layanan 3D secure.</div>
                    <div>4.  Jika kartu kredit tetap tidak bisa digunakan, silakan hubungi bank penerbit kartu kredit pembeli.</div><br>

                    <p>Berikut ini adalah langkah-langkah yang dapat pembeli lakukan jika ingin membayar transaksi menggunakan Kartu Kredit.</p>
                    <div class="tab-content">
                        <div id="kartu_kredit" class="tab-pane fade in active">
                            <div>1.Isi dan lengkapi data Card Number, Card Brand, Card Expiry Date, dan Security Code (3 digit terakhir yang terdapat pada kolom tanda tangan kartu kredit kamu), dan Nama pada Kartu. Klik tombol “Pay Now“.</div>

                            <p><b>Jangan tutup halaman sampai seluruh proses transaksi selesai.</b></p>

                            <div>2.Jika Kartu Kredit kamu memerlukan <b>Authentication (3D Secure)</b>, maka halaman OTP akan muncul. Kamu perlu meng-input kode dari Bank yang dikirimkan ke Handphone kamu (sesuai dengan nomor Handphone yang kamu daftarkan dengan pihak Bank). Jika kartu kredit kamu tidak memerlukan Authentication (3D Secure), maka proses pembayaran <b>telah selesai</b> dan kamu akan langsung diarahkan ke laman Terima Kasih.</div>
                        </div>
                    </div>
                </div>
                <!-- END BCA KlikPay -->
                <?php
            }
                ?>
            </div>
        </div>
</div>