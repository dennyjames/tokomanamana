<?php
  $m = $this->Model;
  $email = $this->session->userdata('email');

  $join0 = array(
      0 => 'customers c-c.id=prc.id_customer',
      1 => 'point_reward_cart_detail prcd-prcd.id_cart=prc.id',
      2 => 'reward r-r.id=prcd.id_reward'
  );
  $q0 = $m->get_data('prcd.*, r.*', 'point_reward_cart prc', $join0, array('c.email' => $email));

  $cart_step = '';

  if($q0->num_rows() > 0){
    if($q0->row()->id_product != 0 || $q0->row()->id_product_paket != 0){
      $cart_step = '
        <li class="">2. Alamat</li>
        <li class="">3. Selesai</li>
      ';
    }else{
      $cart_step = '<li class="">2. Selesai</li>';
    }
  }
?>
<section class="collection-heading heading-content">
    <div class="container">
        <div class="cart-step">
            <ul>
                <li class="active">1. Tukar Poin Saya</li>
                <?=$cart_step?>
            </ul>
        </div>
        <div class="cart-title">1. Tukar Poin Saya</div>
    </div>
</section>
<section class="cart-content checkout">
    <div class="container">
        <?php

            if($q0->num_rows() > 0){
                foreach ($q0->result() as $key) {
                    $gambar = $key->gambar != '' ? $key->gambar : 'http://localhost/tmm/files/images/logo.png';
        ?>
        <div class="col-sm-6 col-md-5 col-lg-4" style="text-align: left;">
            <img src="<?=$gambar?>" class="thumbnail" style="width: 100%; margin-top: 12px">
        </div>
        <div class="col-sm-6 col-md-7 col-lg-8">
            <h3 class="align-middle" style="margin-top: 12px; text-align: left"><?=$key->nama?></h3><br>
            <p class="align-middle"><?=$key->deskripsi?></p>
            <h4 style="margin-top: 12px; color: red; text-align: left"><?=$key->point?> Point</h4>
        </div>
        <div class="col-lg-12">
            <hr>
        </div>
        <div class="cart-product">
        <?php
                    $exp = explode('-', $key->tipe);

                    $nama = '';
                    $gambar1 = '';
                    $qty = 0;
                    if($exp[0] != ''){
                        $q_product = $m->get_data('', 'products', null, array('id' => $key->id_product));

                        if($q_product->num_rows() > 0){

                            $q_product_image = $m->get_data('', 'product_image', null, array('product' => $q_product->row()->id), '', 'sort_order asc', 1);

                            if($q_product_image->num_rows() > 0){
                                $gambar1 = $q_product_image->row()->image;
                            }

                            $nama = $q_product->row()->name;
                            $qty = $key->qty_product;
                        }

                        $gambar1 = $gambar1 == '' ? 'http://localhost/tmm/files/images/logo.png' : $gambar1;
                        ?>
            <div class="cart-item" id="cart-<?=$key->id?>">
                <div class="cart-item-left">
                    <div class="item-image">
                        <a href="#">
                            <img src="<?=$gambar1?>">
                        </a>
                    </div>
                    <div class="item-desc">
                        <div class="item-name">
                            <a href="#" style="font-size: 16px"><?=$nama?></a>
                            <p style="margin-top: 8px">QTY <span style="font-size: 16px"><b><?=$qty?></b></span></p>
                        </div>
                        <div class="cart-line">
                        </div>
                        <div class="alertEmptyStock" id="alert-<?=$key->id?>" style="color:red;"></div>
                    </div>
                </div>
            </div>
                        <?php
                    }

                    $nama = '';
                    $gambar1 = '';
                    $qty = 0;
                    if($exp[1] != ''){
                        $q_paket = $m->get_data('', 'products', null, array('id' => $key->id_product_paket));

                        if($q_paket->num_rows() > 0){

                            $q_paket_image = $m->get_data('', 'product_image', null, array('product' => $q_paket->row()->id), '', 'sort_order asc', 1);

                            if($q_paket_image->num_rows() > 0){
                                $gambar1 = $q_paket_image->row()->image;
                            }

                            $nama = $q_paket->row()->name;
                            $qty = $key->qty_paket;
                        }

                        $gambar1 = $gambar1 == '' ? 'http://localhost/tmm/files/images/logo.png' : $gambar1;

                        ?>
            <div class="cart-item" id="cart-<?=$key->id?>">
                <div class="cart-item-left">
                    <div class="item-image">
                        <a href="#">
                            <img src="<?=$gambar1?>">
                        </a>
                    </div>
                    <div class="item-desc">
                        <div class="item-name">
                            <a href="#" style="font-size: 16px"><?=$nama?></a>
                            <p style="margin-top: 8px">QTY <span style="font-size: 16px"><b><?=$qty?></b></span></p>
                        </div>
                        <div class="cart-line">
                        </div>
                        <div class="alertEmptyStock" id="alert-<?=$key->id?>" style="color:red;"></div>
                    </div>
                </div>
            </div>
                        <?php
                    }

                    $nama = '';
                    $gambar1 = 'http://localhost/tmm/files/images/logo.png';
                    $qty = 0;
                    $desc = '';
                    if($exp[2] != ''){
                        $q_coupon = $m->get_data('', 'coupons', null, array('id' => $key->id_coupon));

                        if($q_coupon->num_rows() > 0){
                            $nama = $q_coupon->row()->name;
                            $qty = $key->qty_coupon;

                            $categories = '';
                            $exp_cat = explode(',', $q_coupon->row()->catIds);

                            for($a=0; $a<count($exp_cat); $a++){
                                $q_category = $m->get_data('name', 'categories', null, array('id' => $exp_cat[$a]));
                                if($q_category->num_rows() > 0){
                                    $categories .= $categories == '' ? $q_category->row()->name : ', '.$q_category->row()->name;
                                }
                            }
                            $desc = $q_coupon->row()->deskripsi;

                            $gambar1 = $q_coupon->row()->gambar == '' ? $gambar1 : $q_coupon->row()->gambar;
                        }

                        

                        ?>
            <div class="cart-item" id="cart-<?=$key->id?>">
                <div class="cart-item-left">
                    <div class="item-image">
                        <a href="#">
                            <img src="<?=$gambar1?>">
                        </a>
                    </div>
                    <div class="item-desc">
                        <div class="item-name">
                            <a href="#" style="font-size: 16px"><?=$nama?></a>
                            <!-- <p style="margin-top: 8px">QTY <span style="font-size: 16px"><b><?=$qty?></b></span></p> -->
                            <p style=""><?=$desc?></p>
                        </div>
                        <div class="cart-line">
                        </div>
                        <div class="alertEmptyStock" id="alert-<?=$key->id?>" style="color:red;"></div>
                    </div>
                </div>
            </div>
                        <?php
                    } 
                }
                ?>
        </div>


        <div class="cart-footer">
            <!-- <div class="total">
                <span>Total Belanja</span>
                <span><?=number_format($key->qty * $key->point)?></span>
            </div> -->
            <div class="action">

                <a href="#" onclick="back()"><i class="fa fa-arrow-left"></i> Kembali</a>
                <a href="javascript:void(0)" onclick="check_stok()" class="btn">Tukar Sekarang <i class="fa fa-arrow-right"></i></a>
            </div>
        </div>
                <?php
            }else{
                echo '<h3>Keranjang Tukar Point kosong.</h3>';
            }
        ?>
        
    </div>
</section>

<script type="text/javascript">
  let current_url1 = '<?=base_url('checkout')?>';
  function check_stok() {
    $.ajax({
      type: 'POST',
      url: current_url1 + "/cart_point/check_stock",
      data: {
      },
      dataType: "json",
      success: function (data) {
        if(data.success == '1'){
          window.location.replace(current_url1 + data.link);
        }else{
          let al = confirm('Ooops.. ' + data.msg);
          if(al){
            window.location.replace('<?=base_url('member/point')?>');
          }else{
            window.location.replace('<?=base_url('member/point')?>');
          }
        }
      }
    });
  }

    function back() {
        $.ajax({
          type: 'POST',
          url: current_url1 + "/cart_point/back",
          data: {
          },
          dataType: "json",
          success: function (data) {
            if(data.success == 1){
              window.location.replace('<?=base_url('member/point')?>');
            }else{
              let al = confirm('Ooops.. ' + data.msg);
              if(al){
                window.location.replace('<?=base_url('member/point')?>');
              }else{
                window.location.replace('<?=base_url('member/point')?>');
              }
            }
          }
        });
    }
</script>