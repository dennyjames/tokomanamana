<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('checkout', settings('language'));
        $this->load->model('catalog/product_model', 'product');
        $this->load->model('cart_model');
    }

    public function index() {
        $this->output->set_title(lang('cart_text') . ' - ' . settings('shop_title'));
        $this->template->_init();
        $this->load->js('assets/frontend/js/modules/cart.js');
        $this->data['page'] = 'cart';

        $this->load->view('cart', $this->data);
    }

//    public function add($id1, $id2, $id3) {
    public function add($id1) {
        $id1 = decode($id1);
//        if (!$this->ion_auth->logged_in()) {
//            redirect('member/login?back=' . seo_url('catalog/products/view/' . $id1));
//        }
//        $id2 = decode($id2);
//        $id3 = decode($id3);
//        $product = $this->cart_model->get_product($id1, $id2, $id3);
        $product = $this->product->get_product($id1);
        if ($product) {
            if ($product->merchant == 0) {
                $merchant = $this->session->userdata('list_merchant')[0];
//            print_r($merchant);
//            $price = $this->main->get('product_price', array('product' => $product->id, 'merchant_group' => $merchant->group));
                $price = $this->main->get('product_price', array('product' => $product->id, 'merchant_group' => $merchant['group']))->price;
            } else {
                $price = $product->price;
            }
            $item = array(
//                'id' => $id1 . '-' . $id2 . '-' . $id3, //product id, merchant id, product merchant id
                'id' => $id1, //product id, merchant id, product merchant id
                'name' => $product->name,
                'image' => $product->image,
                'code' => $product->code,
                'description' => $product->short_description,
                'qty' => $this->input->post('qty'),
//                'price_old' => $product->price_old,
//                'discount' => $product->discount,
                'price' => $price,
                'merchant' => $product->merchant,
                'merchant_name' => $product->merchant_name,
                'merchant_group' => ($product->merchant) ? 0 : $merchant['group'],
                'weight' => $product->weight,
                'shipping' => '',
                'shipping_merchant' => $product->merchant,
                'shipping_cost' => 0,
            );
            $this->cart->insert($item);
            redirect('cart');
        } else {
            show_404();
        }
    }

    public function update($rowid, $qty) {
        $this->cart->update(array('rowid' => $rowid, 'qty' => $qty));
        redirect('cart');
    }

    public function delete($rowid) {
        $this->cart->remove($rowid);
        redirect('cart');
    }

    public function destroy() {
        $this->cart->destroy();
        redirect('cart');
    }

}
