<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CheckoutMobile extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Model', 'm');
    }

    public function testing_(){
        $q = $this->m->get_data('', 'orders', null, array('customer' => '156'));
        die(json_encode(array('success' => 1, 'd' => $q->result())));
    }
    public function mobile_api_xendit($grand_total, $id_cust, $order, $payment_method){

        $code = $this->m->get_data('code', 'orders', null, array('id' => $order))->row()->code;
        $email = $this->m->get_data('email', 'customers', null, array('id' => $id_cust))->row()->email;

        $payment_method = $this->m->get_data('', 'payment_methods', null, array('name' => $payment_method))->row();
        $this->load->library('xendit');
        $xendit_response = $this->xendit->createInvoice($code, $grand_total, $email, 'Order #' . $order);
        $payment = $xendit_response;
        //die(json_encode(array('c' => $payment_method->name)));
        if (in_array($payment_method->name, array('mandiri_va', 'bni_va', 'bri_va'))) {
            switch ($payment_method->name) {
                case 'mandiri_va':
                    $va_name = 'Mandiri Virtual Account';
                    $va_code = 'MANDIRI';
                    break;
                case 'bni_va':
                    $va_name = 'BNI Virtual Account';
                    $va_code = 'BNI';
                    break;
                case 'bri_va':
                    $va_name = 'BRI Virtual Account';
                    $va_code = 'BRI';
                    break;
            }
            foreach ($payment['available_banks'] as $account) {
                if ($account['bank_code'] == $va_code) {
                    $payment_detail = array(
                        'vendor' => 'xendit',
                        'account_code' => $account['bank_code'],
                        'account_name' => $va_name,
                        'account_holder_name' => $account['account_holder_name'],
                        'account_branch' => $account['bank_branch'],
                        'account_number' => $account['bank_account_number'],
                        'amount' => $account['transfer_amount'],
                    );
                    break;
                }
            }
        } elseif ($payment_method->name == 'alfamart') {
            foreach ($payment['available_retail_outlets'] as $account) {
                if ($account['retail_outlet_name'] == 'ALFAMART') {
                    $payment_detail = array(
                        'vendor' => 'xendit',
                        'account_code' => $account['retail_outlet_name'],
                        'account_name' => $account['retail_outlet_name'],
                        'account_holder_name' => 'XENDIT',
                        'account_branch' => '',
                        'account_number' => $account['payment_code'],
                        'amount' => $account['transfer_amount'],
                    );
                    break;
                }
            }
        }

        $this->m->insert_data('order_meta', array('order_id' => $order, 'meta_name' => 'xendit_response', 'meta_value' => json_encode($payment)));
        
        $this->m->update_data('orders', array('payment_to' => json_encode($payment_detail)), array('id' => $order));

        die(json_encode(array('success' => 1, 'data' => $payment_detail)));
    }

    public function mobile_api_sprint($order, $payment_method){
        $this->load->library('sprint');

        $transaction = $this->m->get_data(
            'o.*, cst.fullname customer_name, cst.email customer_email, cst.phone customer_phone, p.name shipping_province_name, c.name shipping_city_name, d.name shipping_district_name',
            'orders o',
            array(
                0 => 'customers cst-cst.id=o.customer',
                1 => 'provincies p-p.id=o.shipping_province',
                2 => 'cities c-c.id=o.shipping_city',
                3 => 'districts d-d.id=o.shipping_district'
            ),
            array('o.id' => $order)
        )->row();

        $payment_method = $this->m->get_data('', 'payment_methods', null, array('name' => $payment_method))->row();
        $data = $transaction;

        $response = $this->sprint->process($transaction);
        
        $this->m->insert_data('order_meta', array('order_id' => $order, 'meta_name' => 'sprint_request', 'meta_value' => json_encode($response['request'])));
        $payment_detail = array(
            'vendor' => 'sprint',
            'account_code' => $response['request']['channelId'],
            'account_name' => $payment_method->title,
            'account_holder_name' => isset($payment_method->merchant_name) ? $payment_method->merchant_name : '',
            'account_branch' => '',
            'account_number' => ($data->payment_method == 'bca_va') ? $response['request']['customerAccount'] : '',
            'amount' => $response['request']['transactionAmount'],
        );
        if (in_array($data->payment_method, ['kredivo', 'bca_sakuku', 'creditcard','creditcard01','creditcard03','creditcard06','creditcard12'])) {
            $payment_detail['payment_url'] = $response['response']['redirectURL'];
            $this->m->update_data('orders', array('payment_to' => json_encode($payment_detail)), array('id' => $order));
            die(json_encode(array('success' => 1, 'payment_detail' => $payment_detail)));
        } elseif ($data->payment_method == 'bca_klikpay') {
            $payment_detail['payment_url'] = $response['response']['redirectURL'];
            $payment_detail['payment_data'] = $response['response']['redirectData'];
            $this->m->update_data('orders', array('payment_to' => json_encode($payment_detail)), array('id' => $order));
            die(json_encode(array('success' => 2, 'payment_detail' => $payment_detail)));
        }
        
        if (in_array($data->payment_method, ['kredivo', 'bca_sakuku', 'creditcard','creditcard01','creditcard03','creditcard06','creditcard12'])) {
            redirect($response['response']['redirectURL']);
        } elseif ($data->payment_method == 'bca_klikpay') {
            $this->sprint->bca_klikpay($response['response']['redirectURL'], $response['response']['redirectData']);
        } else if (in_array($data->payment_method, ['ovo'])) {
            echo $responseIpay;
        } else {
            $this->m->update_data('orders', array('payment_to' => json_encode($payment_detail)), array('id' => $order));
            die(json_encode(array('success' => '3', 'code' => $transaction->code, 'data' => $payment_detail)));
        }
    }

    public function ddd(){
        echo 'asd';
    }
}
