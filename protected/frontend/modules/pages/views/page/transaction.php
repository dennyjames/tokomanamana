<?php echo $breadcrumb; ?>
<div class="main">
    <div class="container" style="margin-bottom: 30px;">
        <div class="row">
            <?php $this->load->view('navigation'); ?>
            <div class="col-sm-9 col-right">
                <div class="page-title">
                    <h1><?php echo lang('help_text_transaction'); ?></h1>
                </div>
                <div class="panel-group" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" data-toggle="collapse" href="#store" aria-expanded="true" aria-controls="collapseOne" style="cursor: pointer; display: block;">
                            Pembelian Langsung ke Toko
                        </div>

                        <div id="store" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body" style="padding-left: 30px; padding-right: 30px;">
                                <p>Pembelian secara langsung dapat dilakukan dengan toko kami di alamat:<br>Jalan Raya Syekh Quro Telagasari<br>Desa Talagsari RT 02 RW 01, Kec. Telagasari<br>Karawang - Jawa Barat<br>41381</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" data-toggle="collapse" href="#cod" aria-expanded="true" aria-controls="collapseOne" style="cursor: pointer; display: block;">
                            Pembelian COD (Cash On Delviery)
                        </div>
                        <div id="cod" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body" style="padding-left: 30px; padding-right: 30px;">
                                <p>Saat ini COD hanya dapat dilakukan untuk Anda yang berdomisili di Karawang. Pesanan Anda akan diantarkan oleh kurir kami.</p>
                                <p>Prosedur Layanan COD:</p>
                                <ul>
                                    <li>Produk yang dapat dibeli hanya produk yang tersedia Ready di Toko (dapat dilihat di halaman detail produk)</li>
                                    <li>Atau pelanggan dapat memesan langsung dengan menghubungi sales kami atau hubungi kami di <?php echo settings('phone'); ?></li>
                                    <li>Kami akan mengatur jadwal pengiriman barang pesanan pelanggan</li>
                                    <li>Barang akan diantar ke tempat pelanggan pada jadwal yang telah disepakati</li>
                                    <li>Pelanggan melakukan pembayaran setelah dilakukan pengecekkan barang pesanan</li>
                                </ul>
                                <p>Syarat & Ketentuan COD:</p>
                                <ul>
                                    <li>Untuk pembelian diatas Rp 1.000.000 Pelanggan diwajibkan untuk melunasi pembayaran terlebih dahulu sebelum diatur jadwal pengiriman</li>
                                    <li>Kerusakkan barang setelah pengecekkan barang oleh pelanggan selesai bukan menjadi tanggungjawab kami</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" data-toggle="collapse" href="#online" aria-expanded="true" aria-controls="collapseOne" style="cursor: pointer; display: block;">
                            Pembelian Online
                        </div>
                        <div id="online" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body" style="padding-left: 30px; padding-right: 30px;">
                                <p>Pembelian online dapat langsung dilakukan dengan mengikuti prosedur yang tersedia di halaman transaksi kami.</p>
                                <ul>
                                    <li>Pilih produk-produk yang akan dibeli untuk dimasukkan kedalam Keranjang Belanja</li>
                                    <li>Lanjutkan proses pembelian dengan klik <b>Lanjut ke Pembayaran</b> pada halaman <b>Keranjang Belanja</b></li>
                                    <li>Lengkapi data pengiriman dan jenis pengiriman diinginkan, lalu klik <b>Selesai</b></li>
                                    <li>Invoice akan dikirim ke email Anda sebagai bukti bahwa Anda melakukan transaksi dengan kami</li>
                                    <li>Lakukan pembayaran sesuai prosedur yang ada di halaman terakhir atau di invoice Anda</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" data-toggle="collapse" href="#payment_confirmation" aria-expanded="true" aria-controls="collapseOne" style="cursor: pointer; display: block;">
                            Konfirmasi Pembayaran
                        </div>
                        <div id="payment_confirmation" class="panel-collapse collapse" role="tabpanel">
                            <div class="panel-body" style="padding-left: 30px; padding-right: 30px;">
                                <p>Fitur ini digunakan untuk pelanggan yang telah membayar sesuai invoice agar dapat cepat kami proses pesanannya.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>