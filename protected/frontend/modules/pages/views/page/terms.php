<?php echo $breadcrumb; ?>
<div class="main">
    <div class="container" style="margin-bottom: 30px;">
        <div class="row">
            <div class="col-md-12">
                <div class="page-title">
                    <h1><?php echo lang('term_condition'); ?></h1>
                </div>
                <blockquote style="font-size: 14px;">Anda harus membaca, memahami, menerima dan menyetujui semua persyaratan dan ketentuan dalam Perjanjian ini sebelum menggunakan aplikasi dan/atau menerima konten yang terdapat di dalamnya. Dengan mengakses atau menggunakan situs TokoKarawang.com, Pengguna dianggap telah memahami dan menyetujui semua isi dalam syarat dan ketentuan di bawah ini. Syarat dan ketentuan dapat diubah atau diperbaharui sewaktu-waktu tanpa ada pemberitahuan terlebih dahulu. Perubahan dari syarat dan ketentuan akan segera berlaku setelah dicantumkan di dalam situs TokoKarawang.com. Jika Pengguna merasa keberatan terhadap syarat dan ketentuan yang kami ajukan dalam Perjanjian ini, maka kami anjurkan untuk tidak menggunakan situs ini.</blockquote>
                <ol>
                    <li>Barang yang sudah dibeli dan lunas pembayarannya tidak bisa ditukar atau dikembalikkan.</li>
                    <li>Kami tidak akan memberikan/membagi informasi pribadi milik pelanggan kepada pihak manapun. Situs kami dioperasikan dengan menggunakan sistim database yang aman dan terenkripsi, dan seluruh informasi pribadi disimpan untuk digunakan hanya oleh kami. kami akan melindungi informasi pribadi milik pelanggan seperti yang dijelaskan di Kebijakan Privasi perusahaan kami.</li>
                    <li>Invoice bukan sebagai tanda reservasi barang. Harga tidak mengikat dan dapat berubah sewaktu-waktu. Invoice berlaku 1 x 24 jam sejak nota di terbitkan, silahkan konfirmasi ke sales kami apabila bermaksud untuk melakukan pelunasan untuk invoice yang sudah lewat dari masa berlaku tersebut terkait perubahan harga dan ketersediaan stok.</li>
                    <li>Bilamana pembayaran dilakukan 1 x 24 jam ( atau selambat2nya 1 hari setelahnya, atau selambat2nya 1 x 47 jam) maka customer berhak mendapatkan barang tersebut dengan harga lama atau harga yang sudah dibayar.</li>
                    <li>Untuk Invoice yang telah dibayarkan, apabila terjadi penurunan harga maka selisih dana tidak bisa di refund.</li>
                    <li>Konfirmasi pembayaran harus dilakukan agar kami dapat segera memproses pesanan.</li>
                    <li>Pengiriman barang diproses via Jasa Pengiriman yang ditunjuk pada malam hari dan informasi resi pengiriman akan dikirimkan secara otomatis via E-mail setelah barang dikirimkan. Keterlambatan input nomor resi di pihak perusahaan Jasa Pengiriman diluar jangkauan <?php echo settings('store_name'); ?> namun tetap menjadi tanggung jawab dan dibawah pengawasan kami.</li>
                    <li>Hasil penjumlahan berat oleh sistem <?php echo settings('store_name'); ?> bersifat mutlak dan tidak dapat disamakan dengan sistem perhitungan pihak ketiga (dalam hal ini pihak kurir yang dipilih).</li>
                    <li>Bilamana penjumlahan berat tidak bulat (dalam satuan desimal) maka pembulatan ke atas.</li>
                    <li>Bila konsumen sudah melakukan pembayaran, maka konsumen dianggap sudah menyetujui nominal pembayaran ongkos kirim yang tertera pada invoice.</li>
                    <li>Jika kemasan barang pesanan Anda dalam keadaan terbuka atau rusak, harap jangan diterima. Bilamana Anda sudah menandatangani resi pengiriman dari JNE, maka Anda dianggap sudah merima dan menyetujui bahwa barang yang Anda terima dalam kondisi kemasan yang baik.</li>
                    <li>Kendala pengiriman seperti stok habis, pending pengiriman, atau hal-hal lain mengenai barang yang di pesan akan kami informasikan via E-mail ke-esokan harinya.</li>
                    <li>Untuk barang yang sudah di pesan dan di bayarkan namun stok habis, kami akan informasikan ke pembeli dan pembeli berhak untuk tetap menunggu hingga barang tiba (keep order), tukar barang lain, atau di refund 100%. Permintaan refund akan diproses 7-14 Hari Kerja atau pada hari Sabtu setiap minggunya.</li>
                    <li>Bilamana pembelian barang-barang terdiri dari beberapa jenis barang dan salah satunya adalah barang PRE-ORDER (barang belum tiba), maka semua pengiriman akan ditunda dan dikirimkan pada saat semua barang telah tiba. Bila pelanggan menginginkan barang yang sudah ada dikirim terlebih dahulu, mohon melakukan proses belanja ulang dengan nota yang terpisah masing-masing (berbeda antara barang PRE-ORDER dan barang yang sudah siap). Koordinasi dengan sales kami sangat diperlukan.</li>
                    <li>Jika dalam keadaan tertentu, terdapat kesalahan harga dari sebuah produk yang disebabkan oleh kesalahan pengetikan (Typo) atau kesalahan harga dan informasi dari supplier maka <?php echo settings('store_name'); ?> berhak untuk menolak ataupun membatalkan pesanan yang menggunakan harga yang salah termasuk pesanan yang sudah dibayarkan.</li>
                    <li>Secara resmi kami hanya beroperasi di website dengan alamat www.tokokarawang.com dan beroperasi di Jalan Raya Syekh Quro Telagasari Karawang.</li>
                    <li>Keluhan pelanggan silahkan e-mail ke cs@tokokarawang.com.</li>
                </ol>
            </div>
        </div>
    </div>
</div>