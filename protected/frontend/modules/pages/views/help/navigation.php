<div class="col-sm-3 col-left">
    <div class="block block-layered-nav">
        <div class="block-content">
            <div class="layered layered-Category">
                <h2 style="text-align: center"><?php echo lang('help_title'); ?></h2>
                <div class="sidebar">
                    <ul>
                        <li class="<?php echo ($page == 'help/transaction') ? 'active' : ''; ?>"><a href="<?php echo site_url('help/transaction'); ?>"><?php echo lang('help_text_transaction'); ?></a></li>
                        <li class="<?php echo ($page == 'help/delivery') ? 'active' : ''; ?>"><a href="<?php echo site_url('help/delivery'); ?>"><?php echo lang('help_text_delivery'); ?></a></li>
                        <li class="<?php echo ($page == 'help/warranty') ? 'active' : ''; ?>"><a href="<?php echo site_url('help/warranty'); ?>"><?php echo lang('help_text_warranty'); ?></a></li>
                        <li class="<?php echo ($page == 'help/payment') ? 'active' : ''; ?>"><a href="<?php echo site_url('help/payment'); ?>"><?php echo lang('help_text_payment'); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>