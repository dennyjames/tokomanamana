<?php echo $breadcrumb; ?>
<div class="main">
    <div class="container" style="margin-bottom: 30px;">
        <div class="row">
            <?php $this->load->view('navigation'); ?>
            <div class="col-sm-9 col-right">
                <div class="page-title">
                    <h1><?php echo lang('help_text_delivery'); ?></h1>
                </div>
                <div class="panel-group" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" data-toggle="collapse" href="#weight" aria-expanded="true" aria-controls="collapseOne" style="cursor: pointer; display: block;">
                            Perhitungan Berat
                        </div>

                        <div id="weight" class="panel-collapse collapse in" role="tabpanel">
                            <div class="panel-body" style="padding-left: 30px; padding-right: 30px;">
                                <p>Perhitungan berat total pesanan akan dibulatkan keatas berdasarkan perhitungan JNE, yaitu setiap 0.3Kg akan dibulatkan keatas. Misalkan berat total pesanan Anda 1.3Kg maka akan dihitung sebagai 1Kg, dan apabila berat total pesanan Anda 1.4Kg maka akan dihitung sebagai 2Kg.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>