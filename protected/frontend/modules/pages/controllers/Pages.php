<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function view($id) {
        $data = $this->main->get('pages', array('id' => $id));
        if (!$data)
            show_404();

        $this->data['data'] = $data;
        $this->data['options'] = json_decode($this->data['data']->options);
        $this->template->_init();
        $this->output->set_title((($data->meta_title) ? $data->meta_title : $data->title) . ' - ' . settings('meta_title'));
        $this->load->view('page', $this->data);
    }

}
