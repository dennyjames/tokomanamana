<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('pages', settings('language'));
        $this->load->library('breadcrumb');
    }

    public function index() {
        redirect('page/about_us');
    }

    public function about_us() {
        $this->data['page'] = 'page/about_us';
        $this->data['meta_description'] = 'Tentang ' . settings('store_name') . '. Toko komputer online dengan harga murah dan jujur tanpa diskon.';
        $this->output->set_title('Tentang Kami - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('page/about_us', $this->data);
    }

    public function terms() {
        //breadcrumb
        $this->breadcrumb->add('Home', site_url());
        $this->breadcrumb->add(lang('term_condition'), site_url('page/terms'));
        $this->data['page'] = 'page/terms';
        $this->data['meta_description'] = lang('term_condition') . ' ' . settings('store_name') . '. Toko komputer online dengan harga murah dan jujur tanpa diskon.';
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->output->set_title(lang('term_condition') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('page/terms', $this->data);
    }

}
