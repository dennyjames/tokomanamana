<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Help extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('pages', settings('language'));
        $this->load->library('breadcrumb');
    }

    public function index() {
        redirect('help/transaction');
    }

    public function transaction() {
        //breadcrumb
        $this->breadcrumb->add('Home', site_url());
        $this->breadcrumb->add(lang('help_title'), site_url('help'));
        $this->breadcrumb->add(lang('help_text_transaction'), site_url('help/transaction'));
        $data = array(
            'page' => 'help/transaction',
            'meta_description' => lang('help_title') . ' belanja di ' . settings('store_name') . '. Toko komputer online dengan harga murah dan jujur tanpa diskon.',
            'breadcrumb' => $this->breadcrumb->output()
        );
        $this->output->set_title(lang('help_title') . ' :: ' . lang('help_text_transaction') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('help/transaction', $data);
    }

    public function delivery() {
        //breadcrumb
        $this->breadcrumb->add('Home', site_url());
        $this->breadcrumb->add(lang('help_title'), site_url('help'));
        $this->breadcrumb->add(lang('help_text_delivery'), site_url('help/delivery'));
        $data = array(
            'page' => 'help/delivery',
            'meta_description' => lang('help_title') . ' belanja di ' . settings('store_name') . '. Toko komputer online dengan harga murah dan jujur tanpa diskon.',
            'breadcrumb' => $this->breadcrumb->output()
        );
        $this->output->set_title(lang('help_title') . ' :: ' . lang('help_text_delivery') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('help/delivery', $data);
    }

    public function warranty() {
        //breadcrumb
        $this->breadcrumb->add('Home', site_url());
        $this->breadcrumb->add(lang('help_title'), site_url('help'));
        $this->breadcrumb->add(lang('help_text_warranty'), site_url('help/warranty'));
        $data = array(
            'page' => 'help/warranty',
            'meta_description' => lang('help_title') . ' belanja di ' . settings('store_name') . '. Toko komputer online dengan harga murah dan jujur tanpa diskon.',
            'breadcrumb' => $this->breadcrumb->output()
        );
        $this->output->set_title(lang('help_title') . ' :: ' . lang('help_text_warranty') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('help/warranty', $data);
    }

    public function payment() {
        //breadcrumb
        $this->breadcrumb->add('Home', site_url());
        $this->breadcrumb->add(lang('help_title'), site_url('help'));
        $this->breadcrumb->add(lang('help_text_payment'), site_url('help/payment'));
        $data = array(
            'page' => 'help/payment',
            'meta_description' => lang('help_title') . ' belanja di ' . settings('store_name') . '. Toko komputer online dengan harga murah dan jujur tanpa diskon.',
            'breadcrumb' => $this->breadcrumb->output()
        );
        $this->output->set_title(lang('help_title') . ' :: ' . lang('help_text_payment') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('help/payment', $data);
    }

}
