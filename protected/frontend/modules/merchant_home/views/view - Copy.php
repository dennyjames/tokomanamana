<style>
  .collection-title {
    margin-top: 25px;
    font-size: 18px !important;
    color: #a7c22a !important;
    font-weight: bold !important;
  }
  .breadcrumb a, i.fa.fa-home {color: #97C23C !important;}
  .breadcrumb a:hover, .breadcrumb a:hover i.fa.fa-home {color: #969EAA !important;}
  .product-item {border: none;}
  .product {
    margin-left: 7px;
    margin-right: 7px;
    margin-bottom: 12px !important;
    border: 1px solid #E8E8E8;
    box-shadow: 0px 0px 7px -3px rgba(0,0,0,0.3);
    min-height: 355px;
  }
  #collection .collection-mainarea .collection-items .product-item {border: none;}
  .product-footer {margin-top: 0px;}
  .garis-tengah {
    width: 100%;
    height: 1px;
    background-color: #E8E8E8;
    display: block;
    margin-top: 10px;
  }
  .tombol-cart {
    z-index: 1;
    background-color: white;
    position: absolute;
    top: 48%;
    right: 5%;
    padding: 0;
    display: inline-block;
    border: 1px solid rgba(167,194,42,0.3);
    height: 40px;
    border-radius: 50%;
    width: 40px;
    box-shadow: 0px 0px 7px 1px rgba(167,194,42,0.3);
    -webkit-transition: all .5s;
    -moz-transition: all .5s;
    -ms-transition: all .5s;
    -o-transition: all .5s;
    transition: all .5s;
  }
  .tombol-cart button {
    background: none;
    border: none;
    line-height: 0;
    padding: 0 7px;
    outline: none;
  }
  .tombol-cart button:active, .tombol-cart button:hover, .tombol-cart button-cart:active, .tombol-cart button-cart:hover {border: none;background: none;outline: none;}
  .tombol-cart a {padding: 12px 7px;line-height: 37px;}
  .tombol-cart:hover {background-color: #a7c22a;box-shadow: 0px 0px 7px 1px rgba(167,194,42,0.3);}
  .tombol-cart:hover .icon-cart i {color: white;}
  .pagination_group {padding: 10px 0px !important;text-align: right;}
  .pagination_group .pagination li a {
    border-top: 1px solid #E0E0E0;
    border-bottom: 1px solid #E0E0E0;
    border-left: 1px solid #E0E0E0;
    background-color: #fff;
    color: #97C23C;
    height: 30px;
    vertical-align: middle;
    padding: 0 13px;
    text-transform: capitalize;
    font-size: 13px;
    border-radius: 0;
    font-weight: 500;
    -webkit-transition: all .5s;
    -moz-transition: all .5s;
    -ms-transition: all .5s;
    -o-transition: all .5s;
    transition: all .5s;
  }
  .pagination_group .pagination li.active a {background-color: #F1F1F1;color: #CCCCCC;border: 1px solid #E0E0E0;}
  .pagination_group .pagination li:hover a {background-color: #F1F1F1;color: #CCCCCC;}
  .pre-order-label{
    color: #666;
    bottom: 0;
    display: inline-block;
    margin-top: 3px;
    position: relative;
    z-index: 1;
    padding: 2px 10px;
    background-color: #EEE;
    font-weight: bold;
    border-radius: 2px;
  }
  .sortBy{
    margin-left: 4%;
  }
  .fill_image img{
    width: 100%;height: auto;display:block;max-height: 235px;
  }

  .collection-leftsidebar {border: 1px solid #E8E8E8;box-shadow: 0px 0px 7px -3px rgba(0,0,0,0.3);}
  .collection-wrapper {margin-top: 20px;}
  @media (max-width: 320px) {
    .pre-order-label {position: absolute !important;margin-bottom: 10px;bottom: 0;}
    .product {min-height: 355px !important;}
    .tombol-cart {top: 48%;}
    .hoverBorder {width: 70% !important;}
    .img-responsive {margin-left: 23%;}
  }
  @media (max-width: 375px) {
    .pre-order-label {position: relative !important;margin-top: 5px;}
  }
  @media (max-width: 425px) and (min-width: 376px) {
    .tombol-cart {top: 48%;}
  }
  @media (max-width: 375px) and (min-width: 321px) {
    .tombol-cart {top: 48%;}
    .product {min-height: 357px !important;}
  }
  @media (max-width: 767px) and (min-width: 426px) {
    .tombol-cart {top: 48%;}
  }

  @media (max-width: 767px) {
    .pre-order-label {position: relative !important;margin-bottom: 5px !important;}
    .product {min-height: 360px;}
    .container {
    padding-left: 10px;
    padding-right: 22px;
      }
  }
  @media (max-width: 1024px) and (min-width: 768px) {
    .tombol-cart {top: 48%;margin-left: 20%;}
    .sortBy{
    margin-left: 2%;
  }
  }
  @media (min-width: 1024px) {
    .product {min-height: 355px;}
  }
  @media (max-width: 1199px) and (min-width: 992px) {
    .sortBy{
    margin-left: 2%;
    }

  }
 
    /*#collection .products .product-item {width: 25% !important;}*/

  @media (max-width: 991px) and (min-width: 585px) {
    #collection .products .product-item .sortBy {width: 33.333333% !important;}
  }
  .search_product{
    border :1px solid;
    border-radius: 3px;
    border-color: #a7c22a;
    outline: none;
    box-shadow:none !important;
    padding-left: 30px;
  }
  .form-control:focus{
        border-color: #a7c22a;
  }
  #search_addon{
    background-color: transparent;
    border:0px;
    position: relative;
    bottom: 29px;
    z-index: 2;
  }
  .tick_box{
    max-height: 56px;
    position: relative;
    left: 0;
    top: 0;
  }
  .merchant_info_panel{
    margin:0;
  }
  .merchant_info_panel_heading{
    background-color: #97C23C !important;  max-height: 42px;
  }
    .merchant_info_panel_heading .panel_head_text{
      position: relative;
        left: 15px;
        bottom: 4px;
        font-size: 18px;
    font-weight: bold; color: white;
  }
  .merchant_info_panel_body{
    box-shadow: 0px 0px 10px -3px rgba(0,0,0,0.3);
  }
  .profile_picture_container{

    margin-top: 8px;
    height: 120px;
    width:120px;
    max-width: 120px;
    max-height: 120px;
    box-shadow: 0px 0px 7px -3px rgba(0,0,0,0.3);
  }
  .profile_picture{
    height: 120px;
    width:120px;
    max-width: 120px;
    max-height: 120px;
  }
  .panel-line-seperator{
    border-left: 2px solid ;
    color: #ebebeb;
    height: 140px;
  }
  .panel-line-seperator_mobile{
    border-left: 2px solid ;
    color: #ebebeb;
    height: 110px;
    margin-top: 10px;
  }
  .shopname_text{
    position: relative;
    font-size: x-large;
    font-weight: bold;
  }
  .shopname_text_mobile{
    position: relative;
    font-size: large;
    font-weight: bold;
  }
  .location_text{
    position: relative;
    font-size: large;

  }
  .location_text_mobile{
    position: relative;
    font-size: small;
    margin-top: 10px;

  }
  .location_header{
    color: #D8C12C; 
  }
  #info_sub_header{
    padding-top: 5px;
    font-size: 15px;
  }
  #info_sub_header_mobile{
    font-size: 15px;
  }
  #share_header{
  margin-top: 10px;
    font-size: 15px;
  }
  .total_product_text{
    font-size: large;
    font-weight: bold;
  }
  #penilaian_text{
    font-size: smaller;
  }
  .etalase_mobile{
    border: 1px solid ;
    min-width: 38px;
    min-height: 30px;
    max-height: 35px;
    margin-left: 10px;
    border-radius: 8%;
    border-color: #a7c22a;
  }
  .etalase_bars{
    position: relative;
    left: 20%;
    top: 14%;
  }

.overlay { 
  height: 0%;
  width: 100%;
  position: fixed; 
  z-index: 4; 
  left: 0;
  bottom: 0;
  background-color: white;
  /*overflow-y: hidden; */
  overflow: auto;
  white-space: nowrap;
  transition: 0.5s; 
}

.overlay-content {
  top: 25%; 
  width: 100%; 
  text-align: center; 
  margin-top: 30px; 
}


.overlay a {
  text-decoration: none;
  font-size: 12px;
  color: black;
  display: block; 
  transition: 0.3s; 
}

.overlay a:hover, .overlay a:focus {
  color: #f1f1f1;
}

  .overlay .closebtn {
    position: absolute;
    top: 10px;
    right: 15px;
    font-size: 50px;
  }

  @media screen and (max-height: 450px) {
    .overlay {overflow-y: auto;}
    .overlay a {font-size: 20px}
    .overlay .closebtn {
    font-size: 40px;
    top: 15px;
    right: 35px;
    }
  }
  .overlay-content ul li:first-child{
    font-size: 18px;
    font-weight: bolder;
  }
  .sidebar-content{
    overflow: auto;
    white-space: nowrap;
    height:calc(39vh - 40px);

  }
  .active{
    color: #a7c22a !important;
  }

  .sort_button{
    position:fixed;
    width:100px;
    height:30px;
    bottom: 10%;
    right: 5%;
    background-color:white;
    color:#a7c22a;
    border-radius:8%;
    text-align:center;
    box-shadow: 2px 2px 3px #999;
    z-index: 3;
    border: .5px solid;
  }
  .sort_button b{
    font-size: medium;
  }
  .jssocials-shares{font-size: 24px;}
  .jssocials-share{padding-right: 10px;}
  .jssocials-share-link { border-radius: 50%;}
    
  
</style>
<?php if ($this->agent->is_mobile()) : ?>
  
<section class="collection-heading heading-content ">
  <div class="container" style="padding: 0" >
    <div class="row">
      <div class="fill_image" >
        <img src="<?php echo base_url('assets/frontend/images/mytech-banner1.jpg')?>" >
      </div>
    </div>
    <div class="row">
      <div class="merchant_info_panel panel panel-default">
        <div class="merchant_info_panel_heading panel-heading">
          <img class="tick_box"src="<?php echo base_url('assets/frontend/images/centang_02.png')?>" ><b class="panel_head_text">Merchant</b>
        </div>
        <div class="merchant_info_panel_body panel-body">
          <div class="col-xs-5 col-sm-3">
            <div class="profile_picture_container">
             <img class="profile_picture"src="<?php echo base_url('assets/frontend/images/smile.png')?>">
            </div>   
          </div>
          <div class="col-xs-7 col-sm-4" style="margin-top: 5px;">
            <div class="row"><b class="shopname_text_mobile">PT SINERGI KEREN KEREN</b></div>
            <div class="row">
              
              <div class="location_text_mobile"><i class="location_header fa fa-map-marker"></i>   Jakarta, Kemayoran Baru</div>
            </div>
            <div class ="row">
              <input type="button" class="store_info" name="store_info" value="Informasi Toko" style="background-color: #a7c22a;font-weight: bold;color: white;margin-top: 8px;margin-left: 15px;padding-left: 11px;padding-right: 11px;">

            </div>
          </div> 
             
          <div class="col-xs-5 col-sm-2" style="padding: 0">
              <div class="row"><div id="info_sub_header">Jumlah Produk</div><div class="total_product_text">200</div></div>
              <div class="row"><div id="share_header">Bagikan</div></div>
              <div class="row"><div id="shareRoundIcons"></div></div>
          </div> 
          <div class="col-xs-1" style="padding: 0"><div class="panel-line-seperator_mobile"></div></div> 
          <div class="col-xs-5 col-sm-2" style="padding: 0">
              <div class="row"><div id="info_sub_header">Rating Toko</div>
                <div class="total_product_text">4.5
                <img src="<?php echo base_url('assets/frontend/images/rating-star.png')?>" style="width:1110px;max-width: 110px;margin-top: -9px"></div>
                <div id="penilaian_text">(2000 Penilaian)</div>
              </div>
              <div class="row">
               <input type="button" class="store_statistic" name="store_statistic" value="Statistic Toko" style="background-color: #a7c22a;font-weight: bold;color: white;margin-top: 10px;">
              </div>
               
          </div> 
        </div>
      </div>
    </div>
    </div>
  </div>
</section>
<?php else: ?>


<section class="collection-heading heading-content ">
  <div class="container" style="padding: 0" >
    <div class="row">
      <div class="fill_image" >
        <img src="<?php echo base_url('assets/frontend/images/mytech-banner1.jpg')?>" >
      </div>
    </div>
    <div class="row">
      <div class="merchant_info_panel panel panel-default">
         
        <div class="merchant_info_panel_heading panel-heading">
          <img class="tick_box"src="<?php echo base_url('assets/frontend/images/centang_02.png')?>" ><b class="panel_head_text">Merchant</b>
        </div>
        <div class="merchant_info_panel_body panel-body">
          <div class="col-sm-2">
            <div class="profile_picture_container">
             <img class="profile_picture"src="<?php echo base_url('assets/frontend/images/smile.png')?>">
            </div>   
          </div>
          <div class="col-md-5">
            <div class="row"><b class="shopname_text">PT SINERGI KEREN KEREN</b></div>
            <div class="row">
              
              <div class="location_text"><i class="location_header fa fa-map-marker"></i>   Jakarta, Kemayoran Baru</div>
            </div>
            <div class ="row">
              <input type="button" class="store_info" name="store_info" value="Informasi Toko" style="background-color: #a7c22a;font-weight: bold;color: white;margin-top: 23px;">

            </div>
          </div> 
          <div class="col-sm-1" style="padding: 0"><div class="panel-line-seperator"></div></div>    
          <div class="col-md-2" style="padding: 0">
              <div class="row"><div id="info_sub_header">Jumlah Produk</div><div class="total_product_text">200</div></div>
              <div class="row"><div id="share_header">Bagikan</div></div>
              <div class="row"><div id="shareRoundIcons"></div></div>
          </div> 
          <div class="col-md-2" style="padding: 0">
              <div class="row"><div id="info_sub_header">Rating Toko</div>
                <div class="total_product_text">4.5
                <img src="<?php echo base_url('assets/frontend/images/rating-star.png')?>" style="width:1110px;max-width: 110px;margin-top: -9px"></div>
                <div id="penilaian_text">(2000 Penilaian)</div>
              </div>
              <div class="row">
               <input type="button" class="store_statistic" name="store_statistic" value="Statistic Toko" style="background-color: #a7c22a;font-weight: bold;color: white;margin-top: 16px;">
              </div>
               
          </div> 
        </div>
      </div>
    </div>
    </div>
  </div>
</section>

<?php endif; ?>
<form class="form-filter" method="GET">
<section class="collection-content">
  <?php if ($this->agent->is_mobile()){ ?>
  <div class="collection-wrapper" style="margin-right: 10px;">
  <?php }else{ ?>
  <div class="collection-wrapper" >
  <?php } ?>
    <div class="container">
      <div class="row">
        <div id="shopify-section-collection-template" class="shopify-section">
          <div class="collection-inner">
            <div id="tags-load" style="display:none;">
              <i class="fa fa-spinner fa-pulse fa-2x"></i>
            </div>

            <div id="collection">
              <div class="collection_inner">
               <?php if (!$this->agent->is_mobile()) :?>
                <div class="collection-leftsidebar sidebar col-sm-3 clearfix">
                  <div class="sidebar-block collection-block">
                    <div class="sidebar-title">
                      <span>Etalase</span>
                      <i class="fa fa-caret-down show_sidebar_content" aria-hidden="true"></i>
                    </div>

                    <div class="sidebar-content">
                      <ul class="list-cat">
                        
                          <li><a class="select-etalase active">Semua Etalase (<?= $total_etalase; ?>)</a></li>
                          <li><a id="0" class="select-etalase">Tidak Dalam Etalase (<?= $no_etalase; ?>)</a></li>
                          <?php
                          $etalases = $this->merchant_home->get_etalase($data->store_id);
                          foreach ($etalases->result() as $etalase) { ?>
                            <li>
                              <a id="<?= $etalase->id?>" class="select-etalase" ><?php echo $etalase->name.' ('.$etalase->count_total.')' ?></a>
                              <!-- <a href="<?php echo seo_url('catalog/categories/view/' . $etalase->id); ?>"><?php echo $etalase->name; ?></a> -->
                            </li>
                          <?php } ?>
                          <input type="hidden" name="etalase-choice" class="etalase-choice" >
                        
                      </ul>
                    </div>
                  </div>
                </div>
                <?php endif; ?>

                <div class="collection-mainarea col-sm-12 col-lg-9 clearfix" style="padding-right: 0">
                <?php
                  
                    if ($this->agent->is_mobile()) :?>
                    <div class="collection_toolbar" style="padding: 0;margin-top: -5px; margin-bottom: 0px">
                      <div class="toolbar_right">
                        <div class="group_toolbar" style="margin-top: 8px;"> 
                          <div class="input-group" >
                            
                              <div style="margin-bottom: -6px;">
                              <input type="text" name="search-specific-result" class="search_product form-control" size="65" placeholder="Cari disini.." aria-describedby="basic-addon1">
                              <span class="input-group-addon" id="search_addon"><a id="search-specific-button"><i class="fa fa-search" style="color: #a7c22a;"></i></a></span>
                              </div>
                           
                          </div>
                            <div class="etalase_mobile" onclick="openNav()">
                              <i class="etalase_bars fa fa-th-large fa-2x" style="color:#a7c22a; "></i>
                            </div>
                            <div id="etalase_mobile_view" class="overlay">
                              <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                              <div class="overlay-content">
                                  <ul>
                                    <li>Etalase<hr class="etalase_line_top" style="border-top-width:2px;border-color:#a7c22a; "></li>
                                    <li><a class="select-etalase active">Semua Etalase (<?= $total_etalase; ?>)</a><hr class="etalase_line"></li>
                                    <li><a id="0" class="select-etalase">Tidak Dalam Etalase (<?= $no_etalase; ?>)</a><hr class="etalase_line"></li>
                                  <?php
                                    $etalases2 = $this->merchant_home->get_etalase($data->store_id);
                                    foreach ($etalases2->result() as $etalase2) { ?>
                                    <li><a id="<?= $etalase2->id?>" class="select-etalase"><?php echo $etalase2->name.' ('.$etalase2->count_total.')'?></a><hr class="etalase_line"></li>
                                  <?php } ?>
                                    <input type="hidden" name="etalase-choice" class="etalase-choice" >
                                  </ul>
                                
                              </div>
                            </div>
                            <a href="#" class="sort_button" onclick="openNav_sort()"><b style="">Sort</b>  <i class="fa fa-sort fa-2x" aria-hidden="true" style="position: relative; left: 10%;top: 10%;"></i></a>
                            <div id="sort_mobile_view" class="overlay">
                              <a href="javascript:void(0)" class="closebtn" onclick="closeNav_sort()">&times;</a>
                              <div class="overlay-content">
                                
                                <ul>
                                  <li>Urutkan - <?= $sort_type; ?><hr class="etalase_line_top" style="border-top-width:2px;border-color:#a7c22a; "></li>
                                  <li><a id='popular'  class="select-sort" value="Terpopuler">Terpopuler</a><hr class="etalase_line"></li>
                                  <li><a id='nameAZ' class="select-sort" value="Nama (A-Z)">Nama (A-Z)</a><hr class="etalase_line"></li>
                                  <li><a  id='nameZA' class="select-sort" value="Nama (Z-A)">Nama (Z-A)</a><hr class="etalase_line"></li>
                                  <li><a  id='lowprice' class="select-sort" value="Termurah">Termurah</a><hr class="etalase_line"></li>
                                  <li><a  id='highprice' class="select-sort" value="Termahal">Termahal</a><hr class="etalase_line"></li>
                                  <li><a  id='newest' class="select-sort" value="Terbaru">Terbaru</a><hr class="etalase_line"></li>
                                  <?php if(($this->input->get('sort')) ? 'selected' : '')?>
                                  <input type="hidden" name="sort" id="selected-sort" >
                                  <input type="hidden" name="sort-name" id="selected-sort-name" >    
                                </ul>
                                
                              </div>
                            </div>
                        </div>
                      </div>
                    </div>
                    <?php else: ?>
                      <div class="collection_toolbar" style="padding-right: 13px;margin-top: -5px; margin-bottom: 0px;">
                      <div class="toolbar_right">
                        <div class="group_toolbar"> 
                          <div class="input-group" style="margin-top: 8px;">
                            
                              <div style="margin-bottom: -6px;">
                              <input type="text" name="search-specific-result" class="search_product form-control" size="65" placeholder="Cari disini.." aria-describedby="basic-addon1">
                              <span class="input-group-addon" id="search_addon"><a id="search-specific-button"><i class="fa fa-search" style="color: #a7c22a;"></i></a></span>
                              </div>
                            
                          </div>
                          
                            <div class="sortBy">
                              <span class="toolbar_title">Urutkan:</span>
                              <div class="control-container">
                                <select name="sort" tabindex="-1">
                                  <option value="popular" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'popular') ? 'selected' : ''; ?>>Terpopuler</option>
                                  <option value="nameAZ" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'nameAZ') ? 'selected' : ''; ?>>Nama (A-Z)</option>
                                  <option value="nameZA" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'nameZA') ? 'selected' : ''; ?>>Nama (Z-A)</option>
                                  <option value="lowprice" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'lowprice') ? 'selected' : ''; ?>>Termurah</option>
                                  <option value="highprice" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'highprice') ? 'selected' : ''; ?>>Termahal</option>
                                  <option value="newest" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'newest') ? 'selected' : ''; ?>>Terbaru</option>
                                </select>
                              </div>
                              <span class="toolbar_title">Tampilkan:</span>
                              <div class="control-container">
                                <select name="show" tabindex="-1">
                                  <option value="20" <?php echo ($this->input->get('show') && $this->input->get('show') == '20') ? 'selected' : ''; ?>>20</option>
                                  <option value="40" <?php echo ($this->input->get('show') && $this->input->get('show') == '40') ? 'selected' : ''; ?>>40</option>
                                  <option value="80" <?php echo ($this->input->get('show') && $this->input->get('show') == '80') ? 'selected' : ''; ?>>80</option>
                                </select>
                              </div>
                            </div>
                          
                        </div>
                      </div>
                    </div>
                  <?php endif;?>
</form>
                    <?php if ($products->num_rows() > 0) : ?>
                    <div class="collection-items clearfix">
                      <div class="products" style="margin-left: 5px;">
                        <?php foreach ($products->result() as $product) { 
                          $sale_on = FALSE;
                          if($product->promo == 1) {
                            // if ($product->merchant == 0) {
                            //   $merchant = $this->session->userdata('list_merchant')[0];
                            //   $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
                            //   if ($price_group) {
                            //     $product->price = $price_group->price;
                            //   }
                            // }
                            $promo_data = json_decode($product->promo_data,true);
                            if($promo_data['type'] == 'P') {
                              $disc_price = round(($product->price * $promo_data['discount']) / 100);
                              $label_disc = $promo_data['discount'].'% off';
                            } else {
                              $disc_price = $promo_data['discount'];
                              $label_disc = 'SALE';
                            }
                            $end_price= intval($product->price) - intval($disc_price);
                            $today = date('Y-m-d');
                            $today=date('Y-m-d', strtotime($today));
                            $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                            $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                            if (($today >= $DateBegin) && ($today <= $DateEnd)){
                              $sale_on = TRUE;
                            }
                          }    
                          ?>
                          <div class="product-item col-sm-3" style="border: none;">
                            <div class="product">
                              <?php if($sale_on) : ?>
                                <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;border-radius: 5px;"><?php echo $label_disc;?></span>
                              <?php endif; ?>
                              <?php //if ($product->preorder) : ?>
                                <!-- <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;">Pre Order</span> -->
                              <?php //endif; ?>

                              <div class="row-left">
                                <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="hoverBorder container_item" style="min-height: 180px;">
                                  <img src="<?php echo base_url('assets/frontend/images/loading.gif')?>" class="img-responsive lazyload" data-src="<?= ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" alt="<?php echo $product->name; ?>">
                                  <!-- <img src="<?php// echo ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" class="img-responsive" alt="<?php// echo $product->name; ?>"> -->
                                </a>

                                
                              </div>

                              <div class="garis-tengah"></div>
                              <div class="tombol-cart">
                                <?php if ($this->main->gets('product_option', array('product' => $product->product_id))) : ?>
                                  <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="icon-cart">
                                    <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                  </a>
                                <?php else : ?>
                                  <?php if ($product->quantity == 0) : ?>
                                    <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="icon-cart">
                                      <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                    </a>
                                  <?php else : ?>
                                    <button data-id="<?php echo encode($product->product_id); ?>" data-store="<?= $product->store_id ?>" onclick="addtoCart($(this).data('id'), $(this).data('store'))" class="icon-cart">
                                      <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                    </button>
                                  <?php endif; ?>
                                <?php endif; ?>
                              </div>

                              <div class="row-right animMix">
                                <div class="grid-mode">
                                  <div class="product-title">
                                    <a class="title-5" style="font-weight: bold; text-transform: uppercase;width: 75%;" href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>"><?php echo $product->name; ?></a>
                                  </div>

                                  <div class="product-price">
                                    <?php if($sale_on) {?>
                                      <span class="money" style="text-decoration: line-through;color:#8b8f8b"><?php echo rupiah($product->price); ?></span>
                                      <span class="money" style="color: #a7c22a"><?php echo rupiah($end_price); ?></span>
                                    <?php } else { ?>
                                      <span class="price_sale" style="color: #a7c22a"><span class="money"><?php echo rupiah($product->price); ?></span></span>
                                    <?php } ?>
                                  </div>

                                  <div>
                                    <span class="text-ellipsis"><?php echo $product->merchant_name; ?></span>
                                  </div>

                                  <div class="rating-star">
                                    <span class="spr-badge" data-rating="0.0">
                                      <span class="spr-starrating spr-badge-starrating">
                                        <?php
                                        $rating = $this->merchant_home->get_rating ($product->product_id, $product->store_id);

                                        if (round($rating->rating) > 0) {
                                          for ($i = 1; $i <= round($rating->rating); $i++) {
                                            ?>
                                            <i class="spr-icon spr-icon-star"></i>
                                            <?php
                                          }
                                          for ($i = round($rating->rating) + 1; $i <= 5; $i++) {
                                            ?>
                                            <i class="spr-icon spr-icon-star-empty"></i>
                                            <?php
                                          }
                                        }
                                        ?>
                                      </span>
                                    </span>
                                  </div>
                                  <div class="product-footer">
                                    <!-- <i class="spr-icon spr-icon-star"></i>4.8 -->
                                    <!-- &nbsp; -->
                                    <i class="fa fa-eye" aria-hidden="true"></i> <?= $product->viewed ?>
                                  </div>
                                  <?php if($product->preorder == 1) : ?>
                                    <div class="pre-order-label">Preorder</div>
                                  <?php endif; ?>

                                </div>
                              </div>
                            </div>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                  
                    <?php if ($pagination) { ?>
                      <div class="collection-bottom-toolbar">
                        <div class="product-pagination">
                          <div class="pagination_group">
                            <?php echo $pagination; ?>
                          </div>
                        </div>
                      </div>
                    <?php } ?>
                  <?php  else: ?>
                    <p style="text-align: center;">Tidak ada produk lagi.</p>
                    <div class="collection-bottom-toolbar">
                      <div class="product-pagination">
                        <div class="pagination_group">
                          <?php echo $pagination; ?>
                        </div>
                      </div>
                    </div>
                  <?php endif; ?>
                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<div id="modal_store_info" class="modal">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Informasi Toko</h5>
            </div>

            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
<div id="modal_store_stat" class="modal">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Statistik Toko</h5>
            </div>

            <div class="modal-body">

            </div>
        </div>
    </div>
</div>
