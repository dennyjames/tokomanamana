<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Merchant_home_model extends CI_Model {

    // function get_etalase($id) {
    //     $this->db->select('id,name,store_type')
    //             ->where('store_id', $id);
    //     $query = $this->db->get('etalase');
    //     return ($query->num_rows() > 0) ? $query : FALSE;
    // }

    function get_etalase($id) {
        $this->db->select('e.id,e.name,COUNT(p.etalase) as count_total')
                ->join('etalase e','e.id = p.etalase')
                ->where('p.store_id', $id)
                ->where('p.status',1)
                ->where('p.etalase!=',0)
                ->group_by('e.name')
                ->order_by('e.name ASC');
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function get_merchant_homepage($id) {
        $this->db->select('m.*,m.id AS store_id,m.name AS merchant_name,m.description AS merchant_description,m.type AS merchant_type,m.date_added AS merchant_date_added,pv.name AS province_name,c.name AS city_name,p.id AS product_id,p.meta_title,p.meta_description,p.meta_keyword,p.name AS product_name,COUNT(p.id) AS total_product', FALSE)
                ->join('products p','p.store_id ='.$id)
                ->join('provincies pv','pv.id = m.province')
                ->join('cities c','c.id = m.city')
                ->where('m.status', 1)
                ->where('p.status',1)
                ->where('p.quantity >',0)
                ->where('m.id', $id);
        $query = $this->db->get('merchants m');
        return ($query->num_rows() > 0) ? $query->row() : FALSE;
    }

    function get_total_transaction($id){
        $this->db->select('COUNT(oi.id) AS total_transaction', FALSE)
                    ->where('oi.order_status',7)
                    ->where('oi.merchant',$id);
        $query = $this->db->get('order_invoice oi');
        return $query->row();
    }

    function get_rating ($store) {
        $this->db->select('IFNULL((SUM(rating_speed) + SUM(rating_service) + SUM(rating_accuracy)) / (COUNT(id) * 3), 0) AS rating,
                            IFNULL(COUNT(id), 0) AS count_rating')
                ->where('merchant', $store)
                ->where('status', 1);

        $query = $this->db->get('product_review');
        return ($query->num_rows() > 0) ? $query->row() : FALSE;
    }

    function get_no_etalase($id) {
        $this->db->select('p.etalase', FALSE)
                ->where('p.store_id', $id)
                ->where('p.status',1)
                ->where('p.etalase',0);
        $query = $this->db->get('products p');
        return $query->num_rows();
    }

    function get_no_etalase_principal($id) {
        $this->db->select('p.etalase', FALSE)
                ->join('products_principal_stock pp','pp.product_id = p.id')
                ->where('pp.branch_id', $id)
                ->where('p.status',1)
                ->where('p.etalase',0);
        $query = $this->db->get('products p');
        return $query->num_rows();
    }

    

    function get_ekspedisi($id){
        $this->db->select('m.shipping', FALSE)
                ->where('m.id', $id)
                ->where('m.status',1);
        $query = $this->db->get('merchants m');
        return $query->row();
    }

    function count_row_speed($id,$rating) {
        $this->db->where(['rating_speed' => $rating]);
        $this->db->where('merchant',$id);

        $query =$this->db->get('product_review');
        return ($query->num_rows() > 0) ? $query->num_rows() : 0; 
    }

    function count_row_service($id,$rating) {
        $this->db->where(['rating_service' => $rating]);
        $this->db->where('merchant',$id);

        $query =$this->db->get('product_review');
        return ($query->num_rows() > 0) ? $query->num_rows() : 0; 
    }

    function count_row_accuracy($id,$rating) {
        $this->db->where(['rating_accuracy' => $rating]);
        $this->db->where('merchant',$id);

        $query =$this->db->get('product_review');
        return ($query->num_rows() > 0) ? $query->num_rows() : 0; 
    }

    function getProducts($category, $merchant, $merchant_group, $sort = 'popular', $limit = 20, $start = 0) {
        $query_order = 'ORDER BY ';
        switch ($sort) {
            default:
            case 'popular':
                $query_order .= 'viewed DESC';
                break;
            case 'newest' :
                $query_order .= 'date_added DESC';
                break;
            case 'lowprice' :
                $query_order .= 'price ASC';
                break;
            case 'highprice' :
                $query_order .= 'price DESC';
                break;
            case 'nameAZ' :
                $query_order .= 'name ASC';
                break;
            case 'nameZA' :
                $query_order .= 'name DESC';
                break;
        }
        $query_limit = '';
        if ($limit) {
            $query_limit = 'LIMIT ' . $start . ',' . $limit;
        }
        $merchant = implode(',', $merchant);

        return $this->db->query("SELECT products_principal_stock.product_id AS product_id,
                                        products_principal_stock.branch_id AS store_id,
                                        products_principal_stock.quantity,
                                        products.merchant,
                                        products.name,
                                        products.short_description,
                                        products.code,
                                        products.category,
                                        products.brand,
                                        IFNULL (product_price.price, products.price) AS price,
                                        products.discount,
                                        products.promo,
                                        products.preorder,
                                        products.promo_data,
                                        products.viewed,
                                        products.date_added,
                                        merchants.name AS merchant_name,
                                        product_image.image
                                FROM products_principal_stock
                                LEFT JOIN products ON products_principal_stock.product_id = products.id
                                LEFT JOIN product_price ON product_price.product = products.id AND product_price.merchant_group = $merchant_group
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
                                WHERE products.merchant = 0
                                AND products.status = 1
                                AND products.category IN (SELECT category FROM category_path WHERE path = $category)
                                AND (products_principal_stock.quantity > 0 OR products.preorder = 1)
                                
                                UNION

                                SELECT products_principal_stock.product_id AS product_id,
                                        products_principal_stock.branch_id AS store_id,
                                        products_principal_stock.quantity,
                                        products.merchant,
                                        products.name,
                                        products.short_description,
                                        products.code,
                                        products.category,
                                        products.brand,
                                        products.price AS price,
                                        products.discount,
                                        products.promo,
                                        products.preorder,
                                        products.promo_data,
                                        products.viewed,
                                        products.date_added,
                                        merchants.name AS merchant_name,
                                        product_image.image
                                FROM products_principal_stock
                                LEFT JOIN products ON products_principal_stock.product_id = products.id
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
                                WHERE products_principal_stock.branch_id IN (SELECT id FROM merchants WHERE id IN ($merchant) AND status = 1)
                                AND products.status = 1
                                AND products.category IN (SELECT category FROM category_path WHERE path = $category)
                                AND (products_principal_stock.quantity > 0 OR products.preorder = 1)
                                
                                $query_order
                                $query_limit");


        // return $this->db->query("SELECT * FROM (
        //             SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, IFNULL(pp.price, p.price) price, pi.image, pp.id ppid, p.viewed, p.date_added, p.quantity,p.promo,p.promo_data
        //             FROM products p
        //             LEFT JOIN product_price pp ON pp.product = p.id AND pp.merchant_group = $merchant_group
        //             LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
        //             WHERE p.merchant = 0 AND p.status = 1
        //             UNION
        //             SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, p.price, pi.image, 0, p.viewed, p.date_added, p.quantity,p.promo,p.promo_data
        //             FROM products p
        //             LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
        //             WHERE merchant IN (SELECT id FROM merchants WHERE id IN ($merchant) AND status = 1) AND p.status = 1
        //             ) p
        //          WHERE p.category IN (SELECT category FROM category_path WHERE path = $category) $query_order $query_limit");
    }


    function get_products($store_id,$merchant_type, $sort = 'popular', $limit = 20, $start = 0,$word = '',$etalase = NULL) {
        $query_order = 'ORDER BY ';
        switch ($sort) {
            default:
            case 'popular':
                $query_order .= 'viewed DESC';
                break;
            case 'newest' :
                $query_order .= 'date_added DESC';
                break;
            case 'lowprice' :
                $query_order .= 'price ASC';
                break;
            case 'highprice' :
                $query_order .= 'price DESC';
                break;
            case 'nameAZ' :
                $query_order .= 'name ASC';
                break;
            case 'nameZA' :
                $query_order .= 'name DESC';
                break;
        }


        $query_limit = '';
        if ($limit) {
            $query_limit = 'LIMIT ' . $start . ',' . $limit;
        }
        $where_etalase = '';
        if($etalase != NULL ){
            $where_etalase = 'AND products.etalase='.$etalase;
        }

        if($merchant_type == 'merchant'){

            return $this->db->query("SELECT  products.id AS product_id,
                                        products.store_id AS store_id,
                                        products.quantity,
                                        products.name,
                                        products.short_description,
                                        products.code,
                                        products.category,
                                        products.brand,
                                        products.price AS price,
                                        products.discount,
                                        products.promo,
                                        products.preorder,
                                        products.promo_data,
                                        products.viewed,
                                        products.date_added,
                                        products.free_ongkir,
                                        merchants.name AS merchant_name,
                                        merchants.type AS store_type,
                                        product_image.image
                                FROM products
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                LEFT JOIN merchants ON merchants.id = products.store_id
                                WHERE CASE WHEN products.variation = 1 THEN (products.quantity >= 0 OR products.preorder = 1 )  ELSE (products.quantity > 0 OR products.preorder = 1 ) END
                                AND products.status = 1
                                AND products.store_id = $store_id
                                -- AND (products.quantity > 0 OR products.preorder = 1 )
                                AND products.store_type = 'merchant'
                                AND (products.code LIKE '%$word%' OR products.name LIKE '%$word%' OR products.meta_keyword REGEXP '$word')
                                $where_etalase
                                $query_order
                                $query_limit");
        }else if($merchant_type == 'principle_branch'){
             return $this->db->query("SELECT products_principal_stock.product_id AS product_id,
                                        products_principal_stock.branch_id AS store_id,
                                        products_principal_stock.quantity,
                                        products.name,
                                        products.short_description,
                                        products.code,
                                        products.category,
                                        products.brand,
                                        products.price AS price,
                                        products.discount,
                                        products.promo,
                                        products.preorder,
                                        products.promo_data,
                                        products.viewed,
                                        products.date_added,
                                        products.free_ongkir,
                                        merchants.name AS merchant_name,
                                        merchants.type AS store_type,
                                        product_image.image
                                FROM products_principal_stock
                                LEFT JOIN products ON products_principal_stock.product_id = products.id
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
                                WHERE products.status = 1
                                AND products_principal_stock.branch_id = $store_id
                                AND (products_principal_stock.quantity > 0 OR products.preorder = 1)
                                AND products.store_type = 'principal'
                                AND (products.code LIKE '%$word%' OR products.name LIKE '%$word%' OR products.meta_keyword REGEXP '$word')
                                $where_etalase
                                $query_order
                                $query_limit
                                ");

        }

        // return $this->db->query("SELECT products_principal_stock.product_id AS product_id,
        //                                 products_principal_stock.branch_id AS store_id,
        //                                 products_principal_stock.quantity,
        //                                 products.name,
        //                                 products.short_description,
        //                                 products.code,
        //                                 products.category,
        //                                 products.brand,
        //                                 products.price AS price,
        //                                 products.discount,
        //                                 products.promo,
        //                                 products.preorder,
        //                                 products.promo_data,
        //                                 products.viewed,
        //                                 products.date_added,
        //                                 products.free_ongkir,
        //                                 merchants.name AS merchant_name,
        //                                 merchants.type AS store_type,
        //                                 product_image.image
        //                         FROM products_principal_stock
        //                         LEFT JOIN products ON products_principal_stock.product_id = products.id
        //                         LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
        //                         LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
        //                         WHERE products.status = 1
        //                         AND products.store_id = $store_id
        //                         AND (products_principal_stock.quantity > 0 OR products.preorder = 1)
        //                         AND products.store_type = 'principal'
        //                         AND (products.code LIKE '%$word%' OR products.name LIKE '%$word%' OR products.meta_keyword REGEXP '$word')
                                

        //                         UNION

        //                         SELECT  products.id AS product_id,
        //                                 products.store_id AS store_id,
        //                                 products.quantity,
        //                                 products.name,
        //                                 products.short_description,
        //                                 products.code,
        //                                 products.category,
        //                                 products.brand,
        //                                 products.price AS price,
        //                                 products.discount,
        //                                 products.promo,
        //                                 products.preorder,
        //                                 products.promo_data,
        //                                 products.viewed,
        //                                 products.date_added,
        //                                 products.free_ongkir,
        //                                 merchants.name AS merchant_name,
        //                                 merchants.type AS store_type,
        //                                 product_image.image
        //                         FROM products
        //                         LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
        //                         LEFT JOIN merchants ON merchants.id = products.store_id
        //                         WHERE products.status = 1
        //                         AND products.store_id = $store_id
        //                         AND (products.quantity > 0 OR products.preorder = 1)
        //                         AND products.store_type = 'merchant'
        //                         AND (products.code LIKE '%$word%' OR products.name LIKE '%$word%' OR products.meta_keyword REGEXP '$word')
        //                         $where_etalase
 
                                
        //                         $query_order
        //                         $query_limit");

    }

}
