<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Merchant_home extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('catalog', settings('language'));
        $this->load->model('merchant_home_model', 'merchant_home');
        $this->load->library('pagination');
        // $this->load->library('breadcrumb');
    }

    // public function view($id) {
    //     $this->load->library('user_agent');
    //     $merchant_home = $this->merchant_home->get_merchant_homepage($id);
    //     if (!$merchant_home)
    //         show_404();

    //     $word = $this->input->get('search-specific-result');
    //     $word = htmlspecialchars($word);

    //     //pagination
    //     $config['base_url'] = current_url();
    //     $config['total_rows'] = $this->merchant_home->get_products($id, '', '', '',$word,$this->input->get('etalase-choice'))->num_rows();
    //     $config['per_page'] = ($this->input->get('show')) ? $this->input->get('show') : 20;
    //     $this->pagination->initialize($config);
    //     $page_num = ($this->input->get('page')) ? ($this->input->get('page')) : 1;
    //     $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

    //     // //breadcrumb
    //     // $categoryPaths = $this->category->get_category_path($id);
    //     // $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
    //     // foreach ($categoryPaths->result() as $categoryPath) {
    //     //     $this->breadcrumb->add($categoryPath->name, seo_url('catalog/categories/view/' . $categoryPath->id));
    //     // }
    //     $this->data['data'] = $merchant_home;
    //     $ekspedisi = $this->merchant_home->get_ekspedisi($id);
    //     $this->data['ekspedisi'] = json_decode($ekspedisi->shipping);
    //     $this->data['rating'] = $this->merchant_home->get_rating($id);
    //     $this->data['rating']->rating = round($this->data['rating']->rating,1);
    //     $this->data['rating_modal'] = $this->merchant_home->get_rating($id);
    //     $this->data['rating_modal']->rating = round($this->data['rating_modal']->rating,1);
    //     $this->data['total_transaction'] = $this->merchant_home->get_total_transaction($id);

    //     $this->data['speed_rating5'] = $this->merchant_home->count_row_speed($id,5);
    //     $this->data['speed_rating4'] = $this->merchant_home->count_row_speed($id,4);
    //     $this->data['speed_rating3'] = $this->merchant_home->count_row_speed($id,3);
    //     $this->data['speed_rating2'] = $this->merchant_home->count_row_speed($id,2);
    //     $this->data['speed_rating1'] = $this->merchant_home->count_row_speed($id,1);

    //     $this->data['service_rating5'] = $this->merchant_home->count_row_service($id,5);
    //     $this->data['service_rating4'] = $this->merchant_home->count_row_service($id,4);
    //     $this->data['service_rating3'] = $this->merchant_home->count_row_service($id,3);
    //     $this->data['service_rating2'] = $this->merchant_home->count_row_service($id,2);
    //     $this->data['service_rating1'] = $this->merchant_home->count_row_service($id,1);

    //     $this->data['accuracy_rating5'] = $this->merchant_home->count_row_accuracy($id,5);
    //     $this->data['accuracy_rating4'] = $this->merchant_home->count_row_accuracy($id,4);
    //     $this->data['accuracy_rating3'] = $this->merchant_home->count_row_accuracy($id,3);
    //     $this->data['accuracy_rating2'] = $this->merchant_home->count_row_accuracy($id,2);
    //     $this->data['accuracy_rating1'] = $this->merchant_home->count_row_accuracy($id,1);

    //     $this->data['products'] = $this->merchant_home->get_products($id, ($this->input->get('sort')) ? $this->input->get('sort') : 'popular', $config['per_page'], $start, $word,$this->input->get('etalase-choice'));
    //     $this->data['pagination'] = $this->pagination->create_links();
    //     $this->data['total_etalase'] = $this->merchant_home->get_products($id, '', '', '','','')->num_rows();
    //     $this->data['no_etalase'] = $this->merchant_home->get_no_etalase($id);
    //     $this->data['sort_type'] = ($this->input->get('sort-name')) ? $this->input->get('sort-name') : 'Terpopuler';
    //     $this->data['etalase_label'] = ($this->input->get('etalase-label')) ? $this->input->get('etalase-label') : 'Semua Etalase';
    //     // $this->data['breadcrumb'] = $this->breadcrumb->output();
    //     $this->data['meta_description'] = ($merchant_home->meta_description) ? $merchant_home->meta_description : $merchant_home->product_name . ', ' . settings('meta_title');
    //     $this->data['meta_keyword'] = ($merchant_home->meta_keyword) ? $merchant_home->meta_keyword : settings('meta_keyword');
    //     $this->data['total_products'] = $config['total_rows'];
    //     $this->data['start'] = $start + 1;
    //     $this->data['to'] = ($this->data['total_products'] < ($page_num *  $config['per_page'])) ? $this->data['total_products'] : ($page_num * $config['per_page']);
    //     $this->output->set_title((($merchant_home->meta_title) ? $merchant_home->meta_title : $merchant_home->product_name) . ' - ' . settings('meta_title'));
    //     $this->template->_init();
    //     $this->load->js('assets/frontend/js/modules/merchant_homepage.js');
    //     $this->load->js('assets/frontend/js/jssocials.js');
    //     // $this->load->js('assets/frontend/css/jssocials-theme-flat.css');
    //     $this->load->view('merchant_home/view', $this->data);
    // }

    public function view($id) {
        $this->load->library('user_agent');
        $merchant_home = $this->merchant_home->get_merchant_homepage($id);
        if (!$merchant_home)
            show_404();



        // var_dump($merchant_home); exit();
        $this->data['data'] = $merchant_home;
        $this->data['users'] = $this->main->get('merchant_users',array('id' => $merchant_home->auth ));
        $ekspedisi = $this->merchant_home->get_ekspedisi($id);
        $this->data['ekspedisi'] = json_decode($ekspedisi->shipping);
        $this->data['rating'] = $this->merchant_home->get_rating($id);
        $this->data['rating']->rating = round($this->data['rating']->rating,1);
        $this->data['rating_modal'] = $this->merchant_home->get_rating($id);
        $this->data['rating_modal']->rating = round($this->data['rating_modal']->rating,1);
        $this->data['total_transaction'] = $this->merchant_home->get_total_transaction($id);

        //rating speed per rating
        $this->data['speed_rating5'] = $this->merchant_home->count_row_speed($id,5);
        $this->data['speed_rating4'] = $this->merchant_home->count_row_speed($id,4);
        $this->data['speed_rating3'] = $this->merchant_home->count_row_speed($id,3);
        $this->data['speed_rating2'] = $this->merchant_home->count_row_speed($id,2);
        $this->data['speed_rating1'] = $this->merchant_home->count_row_speed($id,1);
        //rating service per rating
        $this->data['service_rating5'] = $this->merchant_home->count_row_service($id,5);
        $this->data['service_rating4'] = $this->merchant_home->count_row_service($id,4);
        $this->data['service_rating3'] = $this->merchant_home->count_row_service($id,3);
        $this->data['service_rating2'] = $this->merchant_home->count_row_service($id,2);
        $this->data['service_rating1'] = $this->merchant_home->count_row_service($id,1);
        //rating accuracy per rating
        $this->data['accuracy_rating5'] = $this->merchant_home->count_row_accuracy($id,5);
        $this->data['accuracy_rating4'] = $this->merchant_home->count_row_accuracy($id,4);
        $this->data['accuracy_rating3'] = $this->merchant_home->count_row_accuracy($id,3);
        $this->data['accuracy_rating2'] = $this->merchant_home->count_row_accuracy($id,2);
        $this->data['accuracy_rating1'] = $this->merchant_home->count_row_accuracy($id,1);

        // $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * 20 : 0;
        $start = 0;
        $this->data['products'] = $this->merchant_home->get_products($id,$merchant_home->merchant_type,($this->input->get('sort')) ? $this->input->get('sort') : 'popular',20,$start,'',NULL);

        $this->data['start'] = $start;
        $this->data['total_etalase'] = $this->merchant_home->get_products($id,$merchant_home->merchant_type, '', '', '','','')->num_rows();
        if($merchant_home->merchant_type == 'merchant'){
           $this->data['no_etalase'] = $this->merchant_home->get_no_etalase($id); 
        }
        else if($merchant_home->merchant_type == 'principle_branch'){
            $this->data['no_etalase'] = $this->merchant_home->get_no_etalase_principal($id);
        }
        $this->data['etalase_label'] = 'Semua Etalase';
        $this->data['meta_description'] = ($merchant_home->meta_description) ? $merchant_home->meta_description : $merchant_home->product_name . ', ' . settings('meta_title');
        $this->data['meta_keyword'] = ($merchant_home->meta_keyword) ? $merchant_home->meta_keyword : settings('meta_keyword');

        if($this->data['total_etalase'] > 20){
            //pagination
            // $config['base_url'] = current_url();
            // $config['total_rows'] = $this->data['total_etalase'];
            // $config['per_page'] = 20;
            // $this->pagination->initialize($config);

            // $this->data['pagination'] = $this->pagination->create_links();
            $this->data['next_button'] = true;
        }
        else{
            $this->data['next_button'] = false;
        }


        // //breadcrumb
        // $categoryPaths = $this->category->get_category_path($id);
        // $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        // foreach ($categoryPaths->result() as $categoryPath) {
        //     $this->breadcrumb->add($categoryPath->name, seo_url('catalog/categories/view/' . $categoryPath->id));
        // }
    
        // $this->data['breadcrumb'] = $this->breadcrumb->output();

        $this->output->set_title((($merchant_home->meta_title) ? $merchant_home->meta_title : $merchant_home->product_name) . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->js('assets/frontend/js/modules/merchant_homepage_secure.js');
        $this->load->js('assets/frontend/js/jssocials.min.js');
        //$this->load->js('assets/frontend/css/jssocials-theme-flat.css');
        $this->load->view('merchant_home/view', $this->data);
    }

    public function ajax_filter(){
        $id = $this->input->get('m');
        $start = $this->input->get('start');
        if(empty($start) || $this->input->get('etalase-choice')){
            $start=0;
        }
        $start_end = $start + 20;
        $word = htmlspecialchars($this->input->get('search-specific-result'));
        $merchant_home = $this->merchant_home->get_merchant_homepage($id);
        $this->data['data'] = $merchant_home;
        $this->data['start'] = $start;
        $this->data['products'] = $this->merchant_home->get_products($id,$merchant_home->merchant_type,($this->input->get('sort')) ? $this->input->get('sort') : 'popular',20,$start,$word,$this->input->get('etalase-choice'));
        $count_products = $this->merchant_home->get_products($id,$merchant_home->merchant_type,($this->input->get('sort')) ? $this->input->get('sort') : 'popular','',0,$word,$this->input->get('etalase-choice'))->num_rows();
        $count_products_perpage = $this->merchant_home->get_products($id,$merchant_home->merchant_type,($this->input->get('sort')) ? $this->input->get('sort') : 'popular',20,$start,$word,$this->input->get('etalase-choice'))->num_rows();
        $this->data['total_etalase'] = $this->merchant_home->get_products($id,$merchant_home->merchant_type, '', '', '','','')->num_rows();
        if($merchant_home->merchant_type == 'merchant'){
           $this->data['no_etalase'] = $this->merchant_home->get_no_etalase($id); 
        }
        else if($merchant_home->merchant_type == 'principle_branch'){
            $this->data['no_etalase'] = $this->merchant_home->get_no_etalase_principal($id);
        }
        $this->data['etalase_label'] = ($this->input->get('etalase-label')) ? $this->input->get('etalase-label') : 'Semua Etalase';

        if($start - $count_products_perpage >=0){
            
            $this->data['prev_button'] = true;
        }
        else{
            $this->data['prev_button'] = false;
        }

        if($count_products_perpage < 20 || $count_products == $start_end  ){
            $this->data['next_button'] = false;
        }
        else{
            $this->data['next_button'] = true;
        }

        $this->load->view('merchant_home/view_ajax', $this->data);
    }



}