<div class="" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="banner-slider banner-slider15">
                    <div class="wrap-item" data-pagination="false" data-navigation="true" data-itemscustom="[[0,1]]">
                        <div class="item-banner">
                            <div class="banner-thumb">
                                <a href="#"><img src="<?php echo site_url('assets/frontend/images/banner/now-open.jpg'); ?>" alt="" /></a>
                            </div>
                        </div>
                        <div class="item-banner">
                            <div class="banner-thumb">
                                <a href="#"><img src="<?php echo site_url('assets/frontend/images/banner/now-open.jpg'); ?>" alt="" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="list-adv15">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="banner-image out-in">
                                <a href="<?php echo seo_url('catalog/categories/view/21'); ?>"><img src="<?php echo site_url('assets/frontend/images/banner/heuras-genggerong.jpg'); ?>" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="banner-image out-in">
                                <a href="<?php echo seo_url('catalog/categories/view/54'); ?>"><img src="<?php echo site_url('assets/frontend/images/banner/galeryva.jpg'); ?>" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="banner-image out-in">
                                <a href="<?php echo seo_url('catalog/categories/view/67'); ?>"><img src="<?php echo site_url('assets/frontend/images/banner/sandal.jpg'); ?>" alt=""></a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="banner-image out-in">
                                <a href="<?php echo seo_url('catalog/categories/view/70'); ?>"><img src="<?php echo site_url('assets/frontend/images/banner/tas.jpg'); ?>" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div style="margin-bottom: 30px">
        <div class="title-product-order8">
            <h2 class="title18">Oleh-oleh dari Karawang</h2>
        </div>
        <div class="content-product-order8">
            <div class="wrap-item" data-pagination="false" data-navigation="false" data-itemscustom="[[0,1],[480,2],[768,3],[1024,4],[1200,5]]">
                <?php
                $products = array(30, 34, 33, 24, 22);
                $products = $this->product_model->get_products_by_id($products);
                foreach ($products->result() as $product) {
                    ?>
                    <div class="item-product-order8">
                        <div class="item-product">
                            <div class="product-thumb">
                                <a href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>">
                                    <img src="<?php echo ($product->image) ? ($image = get_thumbnail($product->image)) ? $image : site_url($product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" alt="<?php echo $product->name; ?>">
                                </a>
                            </div>
                            <div class="product-info">
                                <h3 class="product-title"><a href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>"><?php echo $product->name; ?></a></h3>
                                <div class="product-price">
                                    <?php if ($product->discount > 0) { ?>
                                        <del><span><?php echo rupiah($product->price_old); ?></span></del>
                                    <?php } ?>
                                    <ins><span><?php echo rupiah($product->price); ?></span><?php echo ($product->discount > 0) ? '<span class="sale">-' . number($product->discount) . '%</span>' : ''; ?></ins>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php
    if ($categories) {
        $i = 0;
        foreach ($categories->result() as $category) {
            $i++;
            $subcategories = $this->home->get_sub_categories($category->id, 8);
            $products = $this->home->products($category->id, 8);
            if ($products) {
                ?>
                <div class="category-color category-box7 wanita">
                    <div class="header-cat-color">
                        <h2 class="title18"><?php echo $category->name; ?></h2>
                        <a href="<?php echo seo_url('catalog/categories/view/' . $category->id); ?>" class="cat-color-link wobble-top">View all</a>
                    </div>
                    <div class="content-catbox7 <?php echo ($i % 2 == 0) ? 'tags-right' : 'tags-left'; ?>">
                        <div class="clearfix">
                            <div class="banner-tags7">
                                <div class="banner-zoom">
                                    <a href="javascript:void(0);" class="thumb-zoom"><img src="<?php echo site_url('assets/frontend/images/categories/' . $category->id . '.jpg'); ?>" alt="<?php echo $category->name; ?>"></a>
                                </div>
                                <div class="hotkey-cat-color">
                                    <?php if ($subcategories) { ?>
                                        <ul>
                                            <?php foreach ($subcategories->result() as $subcategory) { ?>
                                                <li><a href="<?php echo seo_url('catalog/categories/view/' . $subcategory->id); ?>"><?php echo $subcategory->name; ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="content-cat7 content-pro-box1 left-justify">
                                <div class="clearfix">
                                    <?php
                                    if ($products) {
                                        foreach ($products->result() as $id => $product) {
                                            echo (in_array($id, array(0, 2, 4, 6))) ? '<div class="justify-box1">' : '';
                                            ?>
                                            <div class="item-product1">
                                                <div class="product-thumb">
                                                    <a href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>">
                                                        <img src="<?php echo ($product->image) ? ($image = get_thumbnail($product->image)) ? $image : site_url($product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" alt="<?php echo $product->name; ?>">
                                                    </a>

                                                </div>
                                                <div class="product-info">
                                                    <h3 class="product-title"><a href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>"><?php echo $product->name; ?></a></h3>
                                                    <div class="product-price">
                                                        <?php echo ($product->discount > 0) ? '<del><span>' . rupiah($product->price_old) . '</span></del>' : ''; ?>
                                                        <ins><span><?php echo rupiah($product->price); ?></span><?php echo ($product->discount > 0) ? '<span class="sale">-' . $product->discount . '%</span>' : ''; ?></ins>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            echo (in_array($id, array(1, 3, 5, 7)) || $id == ($products->num_rows() - 1)) ? '</div>' : '';
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
    }
    ?>
</div>
<div class="whyus-testimo">
    <div class="container">
        <div class="inner-testimo7">
            <div class="row">
                <div class="col-xs-12">
                    <div class="testimo7 white">
                        <h2 class="title1">KarawangShop.com: Marketplace UKM Karawang</h2>
                        <p>Berawal dari semangat untuk mendukung pertumbuhan UKM khususnya di Kota Karawang dan memudahkan masyarakat mencari barang kebutuhan secara online, kini telah hadir KarawangShop.</p>
                        <p>KarawangShop merupakan produk digital UKM Karawang dari Bogasoftware memberikan fasilitas marketplace online menyediakan berbagai produk berkualitas dari UKM Karawang, dengan KarawangShop ini kami juga memberikan kemudahan dan kenyamanan Anda dalam berbelanja.</p>
                        <p>Produk-produk yang tersedia di KarawangShop adalah produk-produk asli UKM Karawang dibawah binaan Rumah Kreatif Dinas Koperasi Dan Usaha Mikro Kecil Dan Menengah Kabupaten Karawang. Tersedia berbagai produk mulai dari batik, kerajinan, makanan ringan, tas, sepatu, lukisan, komputer, furniture, kaos hingga topi dan masih banyak lagi.</p>
                        <p>Nikmati berbelanja di KarawangShop dengan harga yang murah, nyaman, dan aman. Barang yang Anda terima Rusak atau Tidak sesuai, kami memberikan jaminan garansi 7 hari ganti baru.</p>
                        <p>Bagi yang berdomisili di Karawang, nikmati juga belanja langsung ke Rumah Kreatif Karawang di alamat Kantor Dinas Koperasi Dan Usaha Mikro Kecil Dan Menengah Kabupaten Karawang Jalan Husni Hamid, Nagasari, Karawang, Jawa Barat 41314.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>