<?php if ($products->num_rows() > 0) foreach ($products->result() as $product) { ?>
        <div class="col-sm-2 proban_product">
            <div class="row-container product list-unstyled clearfix">
                <div class="row-left">
                    <a href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>" class="hoverBorder container_item">
                        <div class="hoverBorderWrapper">
                            <img src="<?php echo get_image($product->image); ?>" class="img-responsive front" alt="<?php echo $product->name; ?>">
                        </div>
                    </a>
                </div>

                <div class="row-right animMix">
                    <div class="product-title"><a class="title-5" href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>"><?php echo $product->name; ?></a></div>
                    <div class="rating-star">
                        <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                        </span>
                    </div>
                    <div class="product-price">
                        <span class="price_sale"><span class="money"><?php echo rupiah($product->price); ?></span></span>
                    </div>
                </div>
            </div>
        </div>
    <?php
    } ?>