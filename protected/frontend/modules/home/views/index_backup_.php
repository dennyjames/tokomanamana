<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" />
<link rel="stylesheet" href="<?php echo site_url('assets/frontend/css/minified/home_min.css') ?>">
<div id="tags-load" style="display:none;"><i class="fa fa-spinner fa-pulse fa-2x"></i></div>
<?php if ($this->agent->is_mobile() || settings('layout_banner') == 2 || settings('layout_banner') == 3) { ?>
  <div class="shopify-section index-section index-section-slideshow">
    
        <section class="home_slideshow main-slideshow">
            <div class="home-slideshow-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="group-home-slideshow">
                          <div class="home-slideshow-inner col-xs-12 col-lg-6">
                            <div class="home-slideshow">
                              <div id="mobile-slider" class="carousel slide main-slider">
                                <div class="responsive-mobile">
                                  <?php 
                                    if ($banners) {
                                      foreach ($banners->result() as $banner) { 
                                  ?>
                                        <a href="<?php echo $banner->link;?>">
                                          <img class="lazyload" src="<?php echo base_url('assets/frontend/images/load_banner_mobile.gif')?>" data-src="<?php echo get_image($banner->image); ?>" alt="<?php echo $banner->name; ?>" title="<?php echo $banner->name; ?>">
                                        </a>
                                  <?php
                                      }
                                    }
                                  ?>
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    
  </div>
<?php } else { ?>
  <div id="banner-home-web" class="shopify-section index-section index-section-proban" style="background: none">
      
        <section class="home_proban_layout" >
            <div class="home_proban_wrapper" >
                <div class="container" >
                    <div class="row">
                        <div class="home_proban_inner">
                          <div id="desktop-slider" class="carousel slide">
                            <div class="carousel-inner">
                                    <div class="responsive-desktop">
                                    <?php
                                        if ($banners) {
                                            foreach ($banners->result() as $banner) {
                                                ?>
                                                    <a href="<?php echo $banner->link;?>">
                                                    <img class="lazyload" src="<?php echo base_url('assets/frontend/images/loading.gif')?>" data-src="<?php echo get_image($banner->image); ?>" alt="<?php echo $banner->name; ?>" title="<?php echo $banner->name; ?>">
                                                    </a>
                                                <?php
                                            }
                                        }
                                    ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    
  </div>
<?php } ?>

<?php if($categories) : ?>
    <!-- <div class="shopify-section index-section index-section-proban" id="categories">
        <section class="home_proban_layout">
            <div class="home_proban_wrapper">
                <div class="container">
                    <div class="row">
                        <div class="home_proban_inner">
                            <div class="enter"></div>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-6">
                                            <div class="row title-segment">KATEGORI</div>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <div class="row">
                                                <div class="hover-lihat-semua">
                                                    <div class=" lihat-semua-object move-right pull-right" style="font-size: 14px;"><a href="<?php echo seo_url('partner'); ?>">Lihat Semua ></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="home-banner-inner col-xs-12">
                                    <div class="responsive-category">
                                        <?php foreach($categories->result() as $category) : ?>
                                            <div class="category col-md-1">
                                                <a href="<?= seo_url('catalog/categories/view/' . $category->id); ?>">
                                                    <img src="<?= get_image($category->icon) ?>">
                                                    <span><?= $category->name ?></span>
                                                </a>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div> -->
<?php endif; ?>

<?php if ($brands) : ?>
    <div class="shopify-section index-section index-section-proban" id="banner">
        <section class="home_proban_layout">
            <div class="home_proban_wrapper">
                <div class="container">
                    <div class="row">
                        <div class="home_proban_inner">
                            <div class="enter"></div>
                            <?php if ($brands) { ?>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-6">
                                            <div class="row title-segment">PARTNER RESMI</div>
                                        </div>
                                        <div class="col-sm-6 col-xs-6">
                                            <div class="row">
                                                <div class="hover-lihat-semua">
                                                    <div class=" lihat-semua-object move-right pull-right" style="font-size: 14px;"><a href="<?php echo seo_url('partner'); ?>">Lihat Semua ></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="home-banner-inner col-xs-12">
                                    <div class="responsive-brand">
                                        <?php foreach ($brands->result() as $brand) { ?>
                                            <div class="box-brand">
                                                <?php 
                                                $path = FCPATH . '../files/images/' . $brand->image;
                                                if(file_exists($path)) {
                                                    $url = $brand->image;
                                                } else {
                                                    $url = 'principal/' . $brand->image;
                                                }
                                                ?>
                                                <!-- <a href="<?php //echo seo_url('catalog/brands/view/' . $brand->id) ?>">
                                                        <img style="margin:8px auto 8px auto" height=35 src="<?php //echo get_image($brand->image); ?>" alt="<?php //echo $brand->name; ?>">
                                                    </a> -->
                                                <a href="<?php echo seo_url('principal_home/view/' . $brand->principle_id); ?>">
                                                    <img class="lazyload" src="<?php echo base_url('assets/frontend/images/loading.gif') ?>" style="margin:8px auto 8px auto" height=35 data-src="<?php echo get_image($url); ?>" alt="<?php echo $brand->name; ?>">
                                                </a>
                                            </div>
                                        <?php }?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
<?php endif; ?>

<!-- <?php //foreach ($brands->result() as $brand) { ?>
    <a href="<?php //echo seo_url('catalog/brands/view/' . $brand->id) ?>">
        <img class="img-responsive" src="<?php //echo get_image($brand->image); ?>" alt="<?php //echo $brand->name; ?>">
    </a>
<?php //} ?> -->


<?php if ($promotions) { ?>
    <div id="promo" class="shopify-section index-section index-section-proban">
        <div>
            <section class="home_proban_layout">
                <div class="home_proban_wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="home_proban_inner home-content-inner">
                                <div class="enter"></div>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-8">
                                            <div class="row title-segment">PROMO HARI INI</div>
                                        </div>
                                        <?php
                                        $total_count = 0;
                                        foreach ($promotions->result() as $promo_count) {
                                            $promo_data = json_decode($promo_count->promo_data, true);

                                            $today = date('Y-m-d');
                                            $today = date('Y-m-d', strtotime($today));
                                            $date_begin = date('Y-m-d', strtotime($promo_data['date_start']));
                                            $date_end = date('Y-m-d', strtotime($promo_data['date_end']));

                                            if (($today >= $date_begin) && ($today <= $date_end)) {
                                                $total_count++;
                                            } else {
                                                continue;
                                            }
                                        }
                                        ?>
                                        <?php if ($total_count == 6) : ?>
                                            <div class="col-sm-6 col-xs-4">
                                                <div class="row">
                                                    <div class="hover-lihat-semua">
                                                        <div class="lihat-semua-object move-right pull-right" style="font-size: 14px; "><a href="<?php echo base_url('promotion'); ?>">Lihat Semua ></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="collection-items clearfix" style="border:none;">
                                    <div class="products" style="border:none;">
                                        <?php
                                        foreach ($promotions->result() as $key => $promotion) {

                                            if ($this->ion_auth->logged_in()) {
                                                $id_customer = $this->data['user']->id;
                                                $get_wishlist = $this->main->get('wishlist', ['product' => $promotion->product_id, 'branch_id' => $promotion->store_id, 'customer' => $id_customer]);
                                                if ($get_wishlist) {
                                                    $product_wishlist = true;
                                                } else {
                                                    $product_wishlist = false;
                                                }
                                            } else {
                                                $product_wishlist = false;
                                            }

                                            if ($promotion->store_id == 0) {
                                                $merchant = $this->session->userdata('list_merchant')[0];
                                                $price_group = $this->main->get('product_price', array('product' => $promotion->product_id, 'merchant_group' => $merchant['group']));
                                                if ($price_group) {
                                                    $promotion->price = $price_group->price;
                                                }
                                            }

                                            $promo_data = json_decode($promotion->promo_data, true);

                                            if ($promo_data['type'] == 'P') {
                                                $disc_price = round(($promotion->price * $promo_data['discount']) / 100);
                                                $label_disc = $promo_data['discount'] . '% off';
                                            } else {
                                                $disc_price = $promo_data['discount'];
                                                $label_disc = 'SALE';
                                            }

                                            $promotion->price = intval($promotion->price);
                                            $disc_price = intval($disc_price);

                                            $end_price = $promotion->price - $disc_price;
                                            $today = date('Y-m-d');
                                            $today = date('Y-m-d', strtotime($today));
                                            $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                            $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                            if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                        ?>
                                                <div class="col-sm-2 col-xs-6 product-item" style="padding: 0px;">
                                                    <div class="product" onmouseover="change_store(<?= $key ?>, 'over', 'promotion')" onmouseout="change_store(<?= $key ?>, 'out', 'promotion')">
                                                        <span class="product-discount-label"><?php echo $label_disc; ?></span>

                                                        <?php if ($this->ion_auth->logged_in()) : ?>
                                                            <a href="#" class="btn-add-to-wishlist" style="color:<?= ($product_wishlist) ? '#d9534f !important;' : '#BBB';  ?>" onclick="add_wishlist(this)" data-product="<?= encode($promotion->product_id) ?>" data-store="<?= encode($promotion->store_id) ?>" data-customer="<?= encode($id_customer) ?>">
                                                                <i class="fa fa-heart" aria-hidden="true"></i>
                                                            </a>
                                                        <?php else : ?>
                                                            <a href="#" class="btn-add-to-wishlist" style="color:#BBB" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                                                <i class="fa fa-heart" aria-hidden="true"></i>
                                                            </a>
                                                        <?php endif; ?>

                                                        <!--<div class="row-left">
                                                                <a href="<?php //echo seo_url('catalog/products/view/' . $promotion->product_id . '/' . $promotion->store_id); 
                                                                            ?>" class="hoverBorder container_item" style="min-height: 180px;">
                                                                        <img src="<?php //echo get_image($promotion->image); 
                                                                                    ?>" class="img-responsive" alt="<?php //echo $promotion->name;
                                                                                                                    ?>">
                                                                     <div class="hoverBorderWrapper">
                                                                    </div>
                                                                </a>
                                                            </div> -->

                                                        <div class="row-left">
                                                            <a href="<?php echo seo_url('catalog/products/view/' . $promotion->product_id . '/' . $promotion->store_id); ?>" class="hoverBorder container_item" style="min-height: 180px;">

                                                                <img src="<?php echo base_url('assets/frontend/images/loading.gif') ?>" alt="<?= $promotion->name; ?>" class="img-responsive lazyload" data-src="<?php echo ($promotion->image) ? site_url('files/images/' . $promotion->image) : site_url('assets/frontend/images/noimage.jpg'); ?>">
                                                            </a>
                                                        </div>

                                                        <div class="garis-tengah"></div>
                                                        <div class="tombol-cart">
                                                            <?php if ($this->ion_auth->logged_in()) : ?>
                                                                <?php if ($this->main->gets('product_option', array('product' => $promotion->product_id))) : ?>
                                                                    <a href="<?php echo seo_url('catalog/products/view/' . $promotion->product_id . '/' . $promotion->store_id); ?>" class="icon-cart">
                                                                        <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                    </a>
                                                                <?php else : ?>
                                                                    <button data-id="<?php echo encode($promotion->product_id); ?>" data-storeid="<?= encode($promotion->store_id) ?>" onclick="addtoCart($(this).data('id'), $(this).data('storeid'))" class="icon-cart">
                                                                        <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                    </button>
                                                                <?php endif; ?>
                                                            <?php else : ?>
                                                                <button type="button" class="icon-cart btn-cart-not-login" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                                                    <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                </button>
                                                                <!-- <a href="<?php //echo site_url('/?back=' . $this->input->get('back')) ?>" class="icon-cart" id="btn-cart-not-login" data-toggle="modal" data-target="#modal-guest">
                                                                    <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                </a> -->
                                                            <?php endif; ?>
                                                        </div>

                                                        <div class="row-right animMix">
                                                            <div class="product-title">
                                                                <a class="title-5" style="font-weight: bold; width: 75%;transition:.3s;" href="<?php echo seo_url('catalog/products/view/' . $promotion->product_id . '/' . $promotion->store_id); ?>"><?php echo $promotion->name; ?></a>
                                                            </div>

                                                            <div class="product-price">
                                                                <span class="price_sale">
                                                                    <span class="money" style="text-decoration: line-through;font-size:11px;color:#8b8f8b"><?php echo rupiah($promotion->price); ?></span>
                                                                    <span class="money"><?php echo rupiah($end_price); ?></span>
                                                                </span>
                                                            </div>

                                                            <div>
                                                                <?php
                                                                $merchant = $this->main->get('merchants', ['id' => $promotion->store_id]);
                                                                $city = $this->main->get('cities', ['id' => $merchant->city]);
                                                                ?>
                                                                <!-- <span class="text-ellipsis"><?php //echo $promotion->merchant_name; 
                                                                                                    ?></span> -->
                                                                <div style="height:15px;overflow:hidden;">
                                                                    <span id="text-ellipsis-city-promotion-<?= $key ?>" style="display:block;"><?= ($city->type == 'Kabupaten') ? 'Kab. ' . $city->name : '' . $city->name ?></span>
                                                                    <span id="text-ellipsis-store-promotion-<?= $key ?>" style="display: block;"><?= $promotion->merchant_name ?></span>
                                                                </div>
                                                            </div>

                                                            <div class="rating-star">
                                                                <span class="spr-badge" data-rating="0.0">
                                                                    <span class="spr-starrating spr-badge-starrating">
                                                                        <?php
                                                                        $rating = $this->home->get_rating($promotion->product_id, $promotion->store_id);

                                                                        if (round($rating->rating) > 0) {
                                                                            for ($i = 1; $i <= round($rating->rating); $i++) {
                                                                        ?>
                                                                                <i class="spr-icon spr-icon-star"></i>
                                                                            <?php
                                                                            }
                                                                            for ($i = round($rating->rating) + 1; $i <= 5; $i++) {
                                                                            ?>
                                                                                <i class="spr-icon spr-icon-star-empty"></i>
                                                                        <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </span>
                                                                    <!-- <span class="spr-badge-caption">No reviews</span> -->
                                                                </span>
                                                            </div>
                                                            <div class="product-footer">
                                                                <!-- <i class="spr-icon spr-icon-star"></i>4.8 -->
                                                                &nbsp;
                                                                <i class="fa fa-eye" aria-hidden="true"></i> <?= $promotion->viewed; ?>
                                                            </div>
                                                            <?php
                                                            $preorder = $promotion->preorder;
                                                            $free_ongkir = $promotion->free_ongkir;
                                                            if ($preorder == 1) {
                                                                echo '<div class="pre-order-label">Preorder</div>';
                                                            }
                                                            if ($free_ongkir == 1) {
                                                                echo '<div class="free-ongkir-label">Free Ongkir</div>';
                                                            } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php } ?>

<?php if ($best_selling) : ?>
    <div class="shopify-section index-section index-section-proban">
        <div>
            <section class="home_proban_layout">
                <div class="home_proban_wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="home_proban_inner">
                                <div class="enter"></div>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-8">
                                            <div class="row title-segment">PRODUK TERLARIS</div>
                                        </div>
                                        <div class="col-sm-6 col-xs-4">
                                            <div class="row">
                                                <div class="hover-lihat-semua">
                                                    <div class="lihat-semua-object move-right pull-right" style="font-size: 14px; "><a href="<?php echo base_url('best_selling'); ?>">Lihat Semua ></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="collection-items clearfix" style="border:none;">
                                    <div class="products" style="border:none;" id="list-product">
                                        <?php foreach ($best_selling->result() as $key => $product) : ?>
                                            <?php
                                            if ($this->ion_auth->logged_in()) {
                                                $id_customer = $this->data['user']->id;
                                                $get_wishlist = $this->main->get('wishlist', ['product' => $product->product_id, 'branch_id' => $product->store_id, 'customer' => $id_customer]);
                                                if ($get_wishlist) {
                                                    $product_wishlist = true;
                                                } else {
                                                    $product_wishlist = false;
                                                }
                                            } else {
                                                $product_wishlist = false;
                                            }

                                            $sale = false;

                                            if ($product->store_id == 0) {
                                                $merchant = $this->session->userdata('list_merchant')[0];
                                                $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
                                                if ($price_group) {
                                                    $product->price = $price_group->price;
                                                }
                                            }

                                            if ($product->promo == 1) {
                                                $promo_data = json_decode($product->promo_data, true);

                                                if ($promo_data['type'] == 'P') {
                                                    $disc_price = round(($product->price * $promo_data['discount']) / 100);
                                                    $label_disc = $promo_data['discount'] . ' % off';
                                                } else {
                                                    $disc_price = $promo_data['discount'];
                                                    $label_disc = 'SALE';
                                                }
                                                $product->price = intval($product->price);
                                                $disc_price = intval($disc_price);
                                                $end_price = $product->price - $disc_price;
                                                $today = date('Y-m-d');
                                                $today = date('Y-m-d', strtotime($today));
                                                $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                                if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                                    $sale = true;
                                                } else {
                                                    $sale = false;
                                                }
                                            }
                                            ?>
                                            <?php if ($product->store_id != null) : ?>
                                                <?php if ($product->quantity > 0) : ?>
                                                    <div class="product-item col-sm-2 col-xs-6" style="padding: 0;">
                                                        <div class="row-container product list-unstyled clearfix" onmouseover="change_store(<?= $key ?>, 'over', 'best_selling')" onmouseout="change_store(<?= $key ?>, 'out', 'best_selling')">
                                                            <?php if ($sale) : ?>
                                                                <span class="product-discount-label"><?php echo $label_disc; ?></span>
                                                            <?php endif; ?>

                                                            <?php if ($this->ion_auth->logged_in()) : ?>
                                                                <a href="#" class="btn-add-to-wishlist" style="color:<?= ($product_wishlist) ? '#d9534f;' : '#BBB'; ?>" onclick="add_wishlist(this)" data-product="<?= encode($product->product_id) ?>" data-store="<?= encode($product->store_id) ?>" data-customer="<?= encode($id_customer) ?>">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                                                </a>
                                                            <?php else : ?>
                                                                <a href="#" class="btn-add-to-wishlist" style="color:#BBB" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                                                </a>
                                                            <?php endif; ?>

                                                            <div class="row-left">
                                                                <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="hoverBorder container_item" style="min-height: 180px;">

                                                                    <img src="<?php echo base_url('assets/frontend/images/loading.gif') ?>" alt="<?= $product->name; ?>" class="img-responsive lazyload" data-src="<?php echo ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>">
                                                                </a>
                                                            </div>

                                                            <div class="garis-tengah"></div>
                                                            <div class="tombol-cart">
                                                                <?php if ($this->ion_auth->logged_in()) : ?>
                                                                    <?php if ($this->main->gets('product_option', array('product' => $product->product_id))) : ?>
                                                                        <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="icon-cart">
                                                                            <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                        </a>
                                                                    <?php else : ?>
                                                                        <button data-id="<?php echo encode($product->product_id); ?>" data-storeid="<?= encode($product->store_id) ?>" onclick="addtoCart($(this).data('id'), $(this).data('storeid'))" class="icon-cart">
                                                                            <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                        </button>
                                                                    <?php endif; ?>
                                                                <?php else : ?>
                                                                    <button type="button" class="icon-cart" data-toggle="modal" data-target="#modal-guest">
                                                                        <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                    </button>
                                                                <?php endif; ?>
                                                            </div>

                                                            <div class="row-right animMix">
                                                                <div class="product-title">
                                                                    <a class="title-5" style="font-weight: bold; width: 75%;transition:.3s;" href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>">
                                                                        <?php echo $product->name; ?>
                                                                    </a>
                                                                </div>

                                                                <div class="product-price">
                                                                    <span class="price_sale">
                                                                        <?php if ($sale) : ?>
                                                                            <span class="money" style="text-decoration: line-through;font-size:9px;color:#8b8f8b"><?php echo rupiah($product->price); ?></span>
                                                                            <span class="money"><?php echo rupiah($end_price); ?></span>
                                                                        <?php else : ?>
                                                                            <span class="money"><?php echo rupiah($product->price); ?></span>
                                                                        <?php endif; ?>
                                                                    </span>
                                                                </div>

                                                                <?php
                                                                $merchant = $this->main->get('merchants', ['id' => $product->store_id]);
                                                                $city = $this->main->get('cities', ['id' => $merchant->city]);
                                                                ?>
                                                                <div style="height: 15px;overflow: hidden;">
                                                                    <span id="text-ellipsis-city-best_selling-<?= $key ?>" style="display: block;"><?= ($city->type == 'Kabupaten') ? 'Kab. ' . $city->name : $city->name ?></span>
                                                                    <span id="text-ellipsis-store-best_selling-<?= $key ?>" style="display: block;"><?= $merchant->name ?></span>
                                                                </div>

                                                                <div class="rating-star">
                                                                    <span class="spr-badge" data-rating="0.0">
                                                                        <span class="spr-starrating spr-badge-starrating">
                                                                            <?php
                                                                            $rating = $this->home->get_rating($product->product_id, $product->store_id);

                                                                            if (round($rating->rating) > 0) {
                                                                                for ($i = 1; $i <= round($rating->rating); $i++) {
                                                                            ?>
                                                                                    <i class="spr-icon spr-icon-star"></i>
                                                                                <?php
                                                                                }
                                                                                for ($i = round($rating->rating) + 1; $i <= 5; $i++) {
                                                                                ?>
                                                                                    <i class="spr-icon spr-icon-star-empty"></i>
                                                                            <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </span>
                                                                    </span>
                                                                </div>

                                                                <div class="product-footer">
                                                                    &nbsp;
                                                                    <i class="fa fa-eye" aria-hidden="true"></i> <?= $product->viewed; ?>
                                                                </div>
                                                                <?php
                                                                $preorder = $product->preorder;
                                                                $free_ongkir = $product->free_ongkir;

                                                                if ($preorder == 1) {
                                                                    echo '<div class="pre-order-label">Preorder</div>';
                                                                }
                                                                if ($free_ongkir == 1) {
                                                                    echo '<div class="free-ongkir-label">Free Ongkir</div>';
                                                                } ?>

                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php endif; ?>

<?php //if ($last_seen_product->num_rows() > 0) { 
?>
<!-- <div class="shopify-section index-section index-section-proban" id="last-seen-content">
        <div>
            <section class="home_proban_layout">
                <div class="home_proban_wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="home_proban_inner home-content-inner">
                                <div class="home-content-header page-title">
                                    <div class="home-content-title group_title">
                                        <?php //if (isset($this->data['user']->id)) { 
                                        ?>
                                            <a href="javascript:void(0)" id="btn-last-seen" style="margin: 0 2rem 0 0;" class="active" onclick="lastseen(<?php //echo $this->data['user']->id; 
                                                                                                                                                            ?>, false)">Terakhir Dilihat</a>
                                            <a href="javascript:void(0)" id="btn-wishlist" style="margin-left: 0;" onclick="wishlist(<?php //echo $this->data['user']->id; 
                                                                                                                                        ?>, false)">Wishlist</a>
                                        <?php
                                        // }
                                        // else {
                                        ?>
                                            <a href="javascript:void(0)" id="btn-last-seen" style="margin: 0 2rem 0 0;" class="active" onclick="lastseen(false, <?php //echo $this->input->ip_address(); 
                                                                                                                                                                ?>)">Terakhir Dilihat</a>
                                            <a href="javascript:void(0)" id="btn-wishlist" style="margin-left: 0;">Wishlist</a>
                                        <?php // } 
                                        ?>
                                        
                                    </div>

                                    <a href="<?php //echo base_url('last_seen') 
                                                ?>" class="text-capitalize home-content-dinamic-link">Lihat semua &gt;</a>
                                </div>

                                <div class="home_proban_content" id="list-product">
                                    <div>
                                        <?php
                                        // foreach ($last_seen_product->result() as $lastSeenProduct) {

                                        //     if ($lastSeenProduct->merchant == 0) {
                                        //         $merchant = $this->session->userdata('list_merchant')[0];
                                        //         $price_group = $this->main->get('product_price', array('product' => $lastSeenProduct->product_id, 'merchant_group' => $merchant['group']));
                                        //         if ($price_group) {
                                        //             $lastSeenProduct->price = $price_group->price;
                                        //         }
                                        //     }

                                        //     $promo_data = json_decode($lastSeenProduct->promo_data,true);

                                        //     if($promo_data['type'] == 'P') {
                                        //         $disc_price = round(($lastSeenProduct->price * $promo_data['discount']) / 100);
                                        //         $label_disc = $promo_data['discount'].'% off';
                                        //     } else {
                                        //         $disc_price = $promo_data['discount'];
                                        //         $label_disc = 'SALE';
                                        //     }

                                        //     $end_price = $lastSeenProduct->price - $disc_price;
                                        //     $today = date('Y-m-d');
                                        //     $today=date('Y-m-d', strtotime($today));
                                        //     $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                        //     $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
                                        ?>
                                                <div class="col-md-2 col-sm-2 col-xs-6 home_proban_product_card">
                                                    <div class="row-container product list-unstyled clearfix">

                                                        <?php //if (($today >= $DateBegin) && ($today <= $DateEnd)){ 
                                                        ?>
                                                            <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;"><?php //echo $label_disc;
                                                                                                                                                                                                            ?></span>
                                                        <?php
                                                        // }
                                                        // elseif ($lastSeenProduct->preorder == 1) {
                                                        ?>
                                                            <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;">Pre Order</span>
                                                        <?php // } 
                                                        ?>
                                                        
                                                        <div class="row-left">
                                                            <a href="<?php //echo seo_url('catalog/products/view/' . $lastSeenProduct->product_id . '/' . $lastSeenProduct->store_id); 
                                                                        ?>" class="hoverBorder container_item">
                                                                <div class="hoverBorderWrapper">
                                                                    <img src="<?php //echo get_image($lastSeenProduct->image); 
                                                                                ?>" class="img-responsive front" alt="<?php //echo $lastSeenProduct->name; 
                                                                                                                        ?>">
                                                                </div>
                                                            </a>
                                                        </div>

                                                        <div class="row-right animMix">
                                                            <div class="product-title">
                                                                <a class="title-5" href="<?php //echo seo_url('catalog/products/view/' . $lastSeenProduct->product_id . '/' . $lastSeenProduct->store_id); 
                                                                                            ?>">
                                                                    <?php //echo $lastSeenProduct->name; 
                                                                    ?>
                                                                </a>
                                                            </div>

                                                            <div class="product-price">
                                                                <span class="price_sale">

                                                                    <?php //if (($today >= $DateBegin) && ($today <= $DateEnd)) { 
                                                                    ?>
                                                                        <span class="money" style="text-decoration: line-through;font-size:9px;color:#8b8f8b"><?php //echo rupiah($lastSeenProduct->price); 
                                                                                                                                                                ?></span>
                                                                        <span class="money"><?php //echo rupiah($end_price); 
                                                                                            ?></span>
                                                                    <?php
                                                                    // }
                                                                    // else {
                                                                    ?>
                                                                        <span class="money"><?php //echo rupiah($lastSeenProduct->price); 
                                                                                            ?></span>
                                                                    <?php //} 
                                                                    ?>

                                                                </span>
                                                            </div>

                                                            <div>
                                                                <span class="text-ellipsis"><?php //echo $lastSeenProduct->merchant_name; 
                                                                                            ?></span>
                                                            </div>

                                                            <div class="rating-star">
                                                                <span class="spr-badge" data-rating="0.0">
                                                                    <span class="spr-starrating spr-badge-starrating">
                                                                        <?php
                                                                        // $rating = $this->home->get_rating ($lastSeenProduct->product_id, $lastSeenProduct->store_id);

                                                                        // if (round($rating->rating) > 0) {

                                                                        //     for ($i = 1; $i <= round($rating->rating); $i++) {
                                                                        ?>
                                                                                    <i class="spr-icon spr-icon-star"></i>
                                                                        <?php
                                                                        // }
                                                                        // for ($n = (($rating->rating) + 1); $n <= 5; $n++) {
                                                                        ?>
                                                                                    <i class="spr-icon spr-icon-star-empty"></i>
                                                                        <?php
                                                                        //     }
                                                                        // }
                                                                        ?>
                                                                    </span>
                                                                </span>
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php
                                        // }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div> -->
<?php //} 
?>


<?php if ($recomendations) { ?>
    <div class="shopify-section index-section index-section-proban">
        <div>
            <section class="home_proban_layout">
                <div class="home_proban_wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="home_proban_inner">
                                <div class="enter"></div>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-8">
                                            <div class="row title-segment">REKOMENDASI UNTUK ANDA</div>
                                        </div>
                                        <?php if($recomendations->num_rows() > 17) : ?>
                                            <div class="col-sm-6 col-xs-4">
                                                <div class="row">
                                                    <div class="hover-lihat-semua">
                                                        <div class="lihat-semua-object move-right pull-right" style="font-size: 14px; "><a href="<?php echo base_url('recomendation'); ?>">Lihat Semua ></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <!-- <div class="home-content-header page-title">
                                    <div class="home-content-title group_title">
                                        <h4>Rekomendasi Untuk Anda</h4>
                                    </div>

                                    <a href="<?php //echo base_url('recomendation'); ?>" class="text-capitalize">Lihat semua &gt;</a>
                                </div> -->

                                <div class="collection-items clearfix" style="border:none;">
                                    <div class="products" style="border:none;" id="list-product-recomendations">
                                        <!-- <div> -->
                                        <?php
                                        foreach ($recomendations->result() as $key => $recomend) {

                                            if ($this->ion_auth->logged_in()) {
                                                $id_customer = $this->data['user']->id;
                                                $get_wishlist = $this->main->get('wishlist', ['product' => $recomend->product_id, 'branch_id' => $recomend->store_id, 'customer' => $id_customer]);
                                                if ($get_wishlist) {
                                                    $product_wishlist = true;
                                                } else {
                                                    $product_wishlist = false;
                                                }
                                            } else {
                                                $product_wishlist = false;
                                            }

                                            $sale = false;

                                            if ($recomend->store_id == 0) {
                                                $merchant = $this->session->userdata('list_merchant')[0];
                                                $price_group = $this->main->get('product_price', array('product' => $recomend->product_id, 'merchant_group' => $merchant['group']));
                                                if ($price_group) {
                                                    $recomend->price = $price_group->price;
                                                }
                                            }
                                            // $promo_data = json_decode($recomend->promo_data,true);

                                            // if($promo_data['type'] == 'P') {
                                            //     $disc_price = round(($recomend->price * $promo_data['discount']) / 100);
                                            //     $label_disc = $promo_data['discount'].'% off';
                                            // } else {
                                            //     $disc_price = $promo_data['discount'];
                                            //     $label_disc = 'SALE';
                                            // }

                                            // $end_price = $recomend->price - $disc_price;
                                            // $today = date('Y-m-d');
                                            // $today=date('Y-m-d', strtotime($today));
                                            // $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                            // $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                            if ($recomend->promo == 1) {
                                                $promo_data = json_decode($recomend->promo_data, true);

                                                if ($promo_data['type'] == 'P') {
                                                    $disc_price = round(($recomend->price * $promo_data['discount']) / 100);
                                                    $label_disc = $promo_data['discount'] . ' % off';
                                                } else {
                                                    $disc_price = $promo_data['discount'];
                                                    $label_disc = 'SALE';
                                                }
                                                $recomend->price = intval($recomend->price);
                                                $disc_price = intval($disc_price);
                                                $end_price = $recomend->price - $disc_price;
                                                $today = date('Y-m-d');
                                                $today = date('Y-m-d', strtotime($today));
                                                $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                                if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                                    $sale = true;
                                                } else {
                                                    $sale = false;
                                                }
                                            }
                                        ?>
                                            <?php if ($recomend->store_id != null) : ?>
                                                <?php if ($recomend->quantity > 0) : ?>
                                                    <div class="product-item col-sm-2 col-xs-6" style="padding: 0;">
                                                        <div class="row-container product list-unstyled clearfix" onmouseover="change_store(<?= $key ?>, 'over', 'recomend')" onmouseout="change_store(<?= $key ?>, 'out', 'recomend')">

                                                            <?php //if (($today >= $DateBegin) && ($today <= $DateEnd)){ 
                                                            ?>
                                                            <!-- <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;"><?php echo $label_disc; ?></span> -->
                                                            <?php
                                                            //}
                                                            // elseif ($recomend->preorder == 1) {
                                                            ?>
                                                            <!-- <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;">Pre Order</span> -->
                                                            <?php //} 
                                                            ?>

                                                            <!-- <div class="row-left">
                                                                        <a href="<?php echo seo_url('catalog/products/view/' . $recomend->product_id . '/' . $recomend->store_id); ?>" class="hoverBorder container_item">
                                                                                <img src="<?php echo get_image($recomend->image); ?>" class="img-responsive front" alt="<?php echo $recomend->name; ?>" style="min-height: 180px;">
                                                                            <div class="hoverBorderWrapper">
                                                                            </div>
                                                                        </a>
                                                                    </div> -->
                                                            <?php if ($sale) : ?>
                                                                <span class="product-discount-label"><?php echo $label_disc; ?></span>
                                                            <?php endif; ?>

                                                            <?php if ($this->ion_auth->logged_in()) : ?>
                                                                <a href="#" class="btn-add-to-wishlist" style="<?= ($product_wishlist) ? 'color:#d9534f;' : 'color:#BBB'  ?>" onclick="add_wishlist(this)" data-product="<?= encode($recomend->product_id) ?>" data-store="<?= encode($recomend->store_id) ?>" data-customer="<?= encode($id_customer) ?>">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                                                </a>
                                                            <?php else : ?>
                                                                <a href="#" class="btn-add-to-wishlist" style="color:#BBB" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                                                    <i class="fa fa-heart" aria-hidden="true"></i>
                                                                </a>
                                                            <?php endif; ?>

                                                            <div class="row-left">
                                                                <a href="<?php echo seo_url('catalog/products/view/' . $recomend->product_id . '/' . $recomend->store_id); ?>" class="hoverBorder container_item" style="min-height: 180px;">

                                                                    <img src="<?php echo base_url('assets/frontend/images/loading.gif') ?>" alt="<?= $recomend->name; ?>" class="img-responsive lazyload" data-src="<?php echo ($recomend->image) ? site_url('files/images/' . $recomend->image) : site_url('assets/frontend/images/noimage.jpg'); ?>">
                                                                </a>
                                                            </div>

                                                            <div class="garis-tengah"></div>
                                                            <div class="tombol-cart">
                                                                <?php if ($this->ion_auth->logged_in()) : ?>
                                                                    <?php if ($this->main->gets('product_option', array('product' => $recomend->product_id))) : ?>
                                                                        <a href="<?php echo seo_url('catalog/products/view/' . $recomend->product_id . '/' . $recomend->store_id); ?>" class="icon-cart">
                                                                            <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                        </a>
                                                                    <?php else : ?>
                                                                        <button data-id="<?php echo encode($recomend->product_id); ?>" data-storeid="<?= encode($recomend->store_id) ?>" onclick="addtoCart($(this).data('id'), $(this).data('storeid'))" class="icon-cart">
                                                                            <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                        </button>
                                                                    <?php endif; ?>
                                                                <?php else : ?>
                                                                    <button type="button" class="icon-cart" data-toggle="modal" data-target="#modal-guest">
                                                                        <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                    </button>
                                                                <?php endif; ?>
                                                            </div>

                                                            <div class="row-right animMix">
                                                                <div class="product-title">
                                                                    <a class="title-5" style="font-weight: bold; width: 75%;transition:.3s;" href="<?php echo seo_url('catalog/products/view/' . $recomend->product_id . '/' . $recomend->store_id); ?>">
                                                                        <?php echo $recomend->name; ?>
                                                                    </a>
                                                                </div>

                                                                <div class="product-price">
                                                                    <span class="price_sale">

                                                                        <?php //if (($today >= $DateBegin) && ($today <= $DateEnd)) { 
                                                                        ?>
                                                                        <!-- <span class="money" style="text-decoration: line-through;font-size:9px;color:#8b8f8b"><?php echo rupiah($recomend->price); ?></span>
                                                                                    <span class="money"><?php //echo rupiah($end_price); ?></span> -->
                                                                        <?php
                                                                        //}
                                                                        //else {
                                                                        ?>
                                                                        <!-- <span class="money"><?php //echo rupiah($recomend->price); ?></span> -->
                                                                        <?php //} 
                                                                        ?>
                                                                        <?php if ($sale) : ?>
                                                                            <span class="money" style="text-decoration: line-through;font-size:9px;color:#8b8f8b"><?php echo rupiah($recomend->price); ?></span>
                                                                            <span class="money"><?php echo rupiah($end_price); ?></span>
                                                                        <?php else : ?>
                                                                            <span class="money"><?php echo rupiah($recomend->price); ?></span>
                                                                        <?php endif; ?>

                                                                    </span>
                                                                </div>

                                                                <?php
                                                                $merchant = $this->main->get('merchants', ['id' => $recomend->store_id]);
                                                                $city = $this->main->get('cities', ['id' => $merchant->city]);
                                                                ?>
                                                                <div style="height: 15px;overflow: hidden;">
                                                                    <span id="text-ellipsis-city-recomendation-<?= $key ?>" style="display: block;"><?= ($city->type == 'Kabupaten') ? 'Kab. ' . $city->name : $city->name ?></span>
                                                                    <span id="text-ellipsis-store-recomendation-<?= $key ?>" style="display: block;"><?= $merchant->name ?></span>
                                                                </div>

                                                                <div class="rating-star">
                                                                    <span class="spr-badge" data-rating="0.0">
                                                                        <span class="spr-starrating spr-badge-starrating">
                                                                            <?php
                                                                            $rating = $this->home->get_rating($recomend->product_id, $recomend->store_id);

                                                                            if (round($rating->rating) > 0) {
                                                                                for ($i = 1; $i <= round($rating->rating); $i++) {
                                                                            ?>
                                                                                    <i class="spr-icon spr-icon-star"></i>
                                                                                <?php
                                                                                }
                                                                                for ($i = round($rating->rating) + 1; $i <= 5; $i++) {
                                                                                ?>
                                                                                    <i class="spr-icon spr-icon-star-empty"></i>
                                                                            <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </span>
                                                                    </span>
                                                                </div>

                                                                <div class="product-footer">
                                                                    &nbsp;
                                                                    <i class="fa fa-eye" aria-hidden="true"></i> <?= $recomend->viewed; ?>
                                                                </div>
                                                                <?php
                                                                $preorder = $recomend->preorder;
                                                                $free_ongkir = $recomend->free_ongkir;

                                                                if ($preorder == 1) {
                                                                    echo '<div class="pre-order-label">Preorder</div>';
                                                                }
                                                                if ($free_ongkir == 1) {
                                                                    echo '<div class="free-ongkir-label">Free Ongkir</div>';
                                                                } ?>

                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endif; ?>
                                        <?php
                                        }
                                        ?>
                                        <!-- </div> -->
                                        <?php if($recomendations->num_rows() > 17) : ?>
                                            <button class="btn-load-more" id="btn-load-more" onclick="btn_load_more($(this).data('lastview'), $(this).data('lastkey'))" data-lastview="<?= $recomendations->result()[17]->date_modified; ?>" data-lastkey="18">Muat Lainnya</button>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php } ?>
<div class="enter"></div>

<?php if(!$this->agent->is_mobile()) : ?>
<!-- Blog Section -->
<div class="shopify-section index-section index-section-prosli">
    <section class="home_blog_layout">
        <div class="home_blog_wrapper">
            <div class="container">
                <div class="row">
                    <div class="home_blog_inner home-content-inner">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-6 col-xs-8">
                                    <div class="row title-segment">BLOG</div>
                                </div>
                                <?php if (count($posts->result()) == 4) : ?>
                                    <div class="col-sm-6 col-xs-4">
                                        <div class="row">
                                            <div class="hover-lihat-semua">
                                                <div class="lihat-semua-object move-right pull-right" style="font-size: 14px;"><a href="#">Lihat Semua ></a></div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="home_blog_content row">

                            <?php
                            if ($posts) foreach ($posts->result() as $post) {
                            ?>
                                <div class="article-wrapper col-sm-3">
                                    <div class="article-inner">
                                        <div class="article-top">
                                            <a class="article-img" href="<?php echo seo_url('blogs/posts/view/' . $post->id); ?>" style=" max-width: 688px; max-height:430px;">
                                                <!-- <img src="<?php echo get_image($post->image); ?>" alt="<?php echo $post->title; ?>"> -->
                                                <img src="<?php echo base_url('assets/frontend/images/loading.gif') ?>" data-src="<?php echo get_image($post->image); ?>" class="lazyload" alt="<?php echo $post->title; ?>">
                                            </a>
                                        </div>
                                        <div class="article-bottom">
                                            <div class="article-name">
                                                <a href="<?php echo seo_url('blogs/posts/view/' . $post->id); ?>" style="font-weight: 900;"><?php echo $post->title; ?></a>
                                            </div>
                                            <ul class="article-info list-inline">
                                                <li class="article-date"><?php echo get_date($post->date_added); ?></li>
                                            </ul>
                                            <div class="article-content">
                                                <p><?php echo character_limiter(stripHTMLtags($post->content), 70, '...'); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php endif; ?>

<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<script type="text/javascript">
    $(window).on('resize orientationchange', function() {
        $('.responsive').slick('resize');
    });

    $(document).ready(function() {
        $('#home_main-banner').slick({
            autoplay: true,
            adaptiveHeight: false,
            autoplaySpeed: 3000,
            arrows: false,
            centerMode: false,
            slidesToShow: 1,
            focusOnSelect: true,
            variableWidth: false,
            dots: false,
            fade: true,
            cssEase: 'linear'
        });

        $('.responsive').slick({
            autoplay: true,
            adaptiveHeight: false,
            autoplaySpeed: 3000,
            arrows: false,
            centerMode: false,
            slidesToShow: 7,
            slideToScroll: 4,
            focusOnSelect: true,
            variableWidth: false,
            dots: false,
            responsive: [{
                    breakpoint: 768,
                    settings: {
                        centerMode: false,
                        // centerPadding: '10px',
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        centerMode: false,
                        // centerPadding: '10px',
                        slidesToShow: 3
                    }
                }
            ]
        });
        $('.btn-cart-not-login').on('click', function(e) {
            e.preventDefault();
            back_data = $(this).data('back');
            $('#back-modal').val(back_data);
        });
        $('.btn-add-to-wishlist').on('click', (e) => {
            e.preventDefault();
        })
    });

    function formatNumber(number) {
        return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }

    function lastseen(id, ip) {
        $("#btn-last-seen").addClass('active');
        $("#btn-wishlist").removeClass('active');
        var content_ = '';

        content_ += '<div class="spinner">';
        content_ += '  <div class="bounce1"></div>';
        content_ += '  <div class="bounce2"></div>';
        content_ += '  <div class="bounce3"></div>';
        content_ += '</div>';

        $('#list-product').html(content_);

        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>home/get_last_seen",
            cache: false,
            data: {
                id: id,
                ip: ip
            },
            success: function(result) {
                var obj = JSON.parse(result);
                $('#list-product').html('');
                var content = '';

                for (x = 0; x < obj.length; x++) {
                    content += '<div class="col-md-2 col-sm-2 col-xs-6 home_proban_product_card">';
                    content += '    <div class="row-container product list-unstyled clearfix">';
                    if (obj[x].label_disc) {
                        content += '        <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;">' + obj[x].label_disc + '</span>';
                    } else if (obj[x].preorder == 1) {
                        content += '        <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;">Pre Order</span>';

                    }
                    content += '        <div class="row-left">';
                    content += '            <a href="' + obj[x].url + '" class="hoverBorder container_item">';
                    content += '                <div class="hoverBorderWrapper">';
                    content += '                    <img src="' + obj[x].image + '" class="img-responsive front" alt="' + obj[x].name + '">';
                    content += '                </div>';
                    content += '            </a>';
                    content += '        </div>';
                    content += '        <div class="row-right animMix">';
                    content += '            <div class="product-title">';
                    content += '                <a class="title-5" href="' + obj[x].url + '">' + obj[x].name + '</a>';
                    content += '            </div>';
                    content += '            <div class="product-price">';
                    content += '                <span class="price_sale">';
                    if (obj[x].end_price) {
                        content += '                    <span class="money" style="text-decoration: line-through;font-size:9px;color:#8b8f8b">Rp. ' + formatNumber(obj[x].price) + '</span>';
                        content += '                    <span class="money">Rp. ' + formatNumber(obj[x].end_price) + '</span>';
                    } else {
                        content += '                    <span class="money">Rp. ' + formatNumber(obj[x].price) + '</span>';
                    }
                    content += '                </span>';
                    content += '            </div>';
                    content += '            <div>';
                    content += '                <span class="text-ellipsis">' + obj[x].merchant_name + '</span>';
                    content += '            </div>';
                    content += '            <div class="rating-star">';
                    content += '                <span class="spr-badge" data-rating="0.0">';
                    content += '                    <span class="spr-starrating spr-badge-starrating">';
                    for (let i = 1; i <= obj[x].rating; i++) {
                        content += '                        <i class="spr-icon spr-icon-star"></i>';
                    }
                    for (i = obj[x].rating + 1; i <= 5; i++) {
                        content += '                        <i class="spr-icon spr-icon-star-empty"></i>';
                    }
                    content += '                    </span>';
                    content += '                </span>';
                    content += '            </div>';
                    content += '        </div>';
                    content += '    </div>';
                    content += '</div>';

                    $('#list-product').html(content);
                }
                $("#last-seen-content .home-content-dinamic-link").attr('href', '<?php echo base_url('last_seen'); ?>');
            }
        });
        $("#list-product").focus();
    }

    function wishlist(id) {
        $("#btn-last-seen").removeClass('active');
        $("#btn-wishlist").addClass('active');
        var content_ = '';

        content_ += '<div class="spinner">';
        content_ += '  <div class="bounce1"></div>';
        content_ += '  <div class="bounce2"></div>';
        content_ += '  <div class="bounce3"></div>';
        content_ += '</div>';

        $('#list-product').html(content_);

        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>home/get_wishlist",
            cache: false,
            data: {
                id: id
            },
            success: function(result) {
                var obj = JSON.parse(result);
                $('#list-product').html('');
                var content = '';

                for (x = 0; x < obj.length; x++) {
                    content += '<div class="col-md-2 col-sm-2 col-xs-6 home_proban_product_card">';
                    content += '    <div class="row-container product list-unstyled clearfix">';
                    if (obj[x].label_disc) {
                        content += '        <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;">' + obj[x].label_disc + '</span>';
                    } else if (obj[x].preorder == 1) {
                        content += '        <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;">Pre Order</span>';

                    }
                    content += '        <div class="row-left">';
                    content += '            <a href="' + obj[x].url + '" class="hoverBorder container_item">';
                    content += '                <div class="hoverBorderWrapper">';
                    content += '                    <img src="' + obj[x].image + '" class="img-responsive front" alt="' + obj[x].name + '">';
                    content += '                </div>';
                    content += '            </a>';
                    content += '        </div>';
                    content += '        <div class="row-right animMix">';
                    content += '            <div class="product-title">';
                    content += '                <a class="title-5" href="' + obj[x].url + '">' + obj[x].name + '</a>';
                    content += '            </div>';
                    content += '            <div class="product-price">';
                    content += '                <span class="price_sale">';
                    if (obj[x].end_price) {
                        content += '                    <span class="money" style="text-decoration: line-through;font-size:9px;color:#8b8f8b">Rp. ' + formatNumber(obj[x].price) + '</span>';
                        content += '                    <span class="money">Rp. ' + formatNumber(obj[x].end_price) + '</span>';
                    } else {
                        content += '                    <span class="money">Rp. ' + formatNumber(obj[x].price) + '</span>';
                    }
                    content += '                </span>';
                    content += '            </div>';
                    content += '            <div>';
                    content += '                <span class="text-ellipsis">' + obj[x].merchant_name + '</span>';
                    content += '            </div>';
                    content += '            <div class="rating-star">';
                    content += '                <span class="spr-badge" data-rating="0.0">';
                    content += '                    <span class="spr-starrating spr-badge-starrating">';
                    for (let i = 1; i <= obj[x].rating; i++) {
                        content += '                        <i class="spr-icon spr-icon-star"></i>';
                    }
                    for (i = obj[x].rating + 1; i <= 5; i++) {
                        content += '                        <i class="spr-icon spr-icon-star-empty"></i>';
                    }
                    content += '                    </span>';
                    content += '                </span>';
                    content += '            </div>';
                    content += '        </div>';
                    content += '    </div>';
                    content += '</div>';

                    $('#list-product').html(content);
                }
                $("#last-seen-content .home-content-dinamic-link").attr('href', '<?php echo base_url('member/wishlist'); ?>');
            }
        });
        $("#list-product").focus();
    }

    function terbaru(id, home_banner) {
        $("#btn-terbaru-" + id).attr('style', 'color:#a6dd69; pointer-events: none;');
        $("#btn-terlaris-" + id).attr('style', 'color:#000; cursor:pointer;');
        var content_ = '';
        /*content_ += '<div class="col-sm-12" style="height:80px; " align="center">';
        content_ += '<div id="loader'+id+'" style="display: inline-block;" class="loader"></div>';
        content_ += '</div>';*/
        content_ += '<div class="spinner">';
        content_ += '  <div class="bounce1"></div>';
        content_ += '  <div class="bounce2"></div>';
        content_ += '  <div class="bounce3"></div>';
        content_ += '</div>';
        $('#list-product-' + id).html(content_);
        var div = document.getElementById('div_ajax' + id);

        var myForm = document.getElementById('form_product');
        var sa = new FormData(myForm);

        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>home/get_terbaru",
            cache: false,
            data: {
                id: id,
                banner: home_banner
            },
            success: function(result) {
                var obj = JSON.parse(result);
                $('#list-product-' + id).html('');
                console.log(obj);
                var content = '';
                content += '<div class="col-sm-6 proban_banner">';
                content += '                                <a href="#">';
                content += '                                    <img src="' + obj[0].banner + '" alt="">';
                content += '                                </a>';
                content += '                            </div>';

                for (x = 0; x < obj.length; x++) {
                    content += '<div class="col-sm-2 proban_product" id="proban_product">';
                    content += '    <div class="row-container product list-unstyled clearfix">';
                    content += '        <div class="row-left">';
                    content += '            <a href="' + obj[x].url + '" class="hoverBorder container_item">';
                    content += '                <div class="hoverBorderWrapper">';
                    content += '                    <img src="' + obj[x].image + '" class="img-responsive front" alt="' + obj[x].name + '">';
                    content += '                </div>';
                    content += '            </a>';
                    content += '        </div>';

                    content += '        <div class="row-right animMix">';
                    content += '            <div class="product-title"><a class="title-5" href="' + obj[x].url + '">' + obj[x].name + '</a></div>';
                    content += '            <div class="rating-star">';
                    content += '                <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>';
                    content += '                </span>';
                    content += '            </div>';
                    content += '            <div class="product-price">';
                    content += '                <span class="price_sale"><span class="money">' + obj[x].rupiah + '</span></span>';
                    content += '            </div>';
                    content += '        </div>';
                    content += '    </div>';
                    content += '</div>';
                    $('#list-product-' + id).html(content);
                }
            }
        });
        $("#list-product-" + id).focus();
    }

    function terlaris(id, home_banner) {
        $("#btn-terlaris-" + id).attr('style', 'color:#a6dd69; pointer-events: none;');
        $("#btn-terbaru-" + id).attr('style', 'color:#000; cursor:pointer;');

        var content_ = '';
        content_ += '<div class="spinner">';
        content_ += '  <div class="bounce1"></div>';
        content_ += '  <div class="bounce2"></div>';
        content_ += '  <div class="bounce3"></div>';
        content_ += '</div>';
        $('#list-product-' + id).html(content_);
        var div = document.getElementById('div_ajax' + id);

        var myForm = document.getElementById('form_product');
        var sa = new FormData(myForm);
        $.ajax({
            type: 'POST',
            url: "<?php echo base_url(); ?>home/get_terlaris",
            cache: false,
            data: {
                id: id,
                banner: home_banner
            },
            success: function(result) {
                var obj = JSON.parse(result);
                $("#list-product-" + id).html('');
                console.log(obj);
                var content = '';
                content += '<div class="col-sm-6 proban_banner">';
                content += '                                <a href="#">';
                content += '                                    <img src="' + obj[0].banner + '" alt="">';
                content += '                                </a>';
                content += '                            </div>';

                for (x = 0; x < obj.length; x++) {
                    content += '<div class="col-sm-2 proban_product" id="proban_product">';
                    content += '    <div class="row-container product list-unstyled clearfix">';
                    content += '        <div class="row-left">';
                    content += '            <a href="' + obj[x].url + '" class="hoverBorder container_item">';
                    content += '                <div class="hoverBorderWrapper">';
                    content += '                    <img src="' + obj[x].image + '" class="img-responsive front" alt="' + obj[x].name + '">';
                    content += '                </div>';
                    content += '            </a>';
                    content += '        </div>';

                    content += '        <div class="row-right animMix">';
                    content += '            <div class="product-title"><a class="title-5" href="' + obj[x].url + '">' + obj[x].name + '</a></div>';
                    content += '            <div class="rating-star">';
                    content += '                <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>';
                    content += '                </span>';
                    content += '            </div>';
                    content += '            <div class="product-price">';
                    content += '                <span class="price_sale"><span class="money">' + obj[x].rupiah + '</span></span>';
                    content += '            </div>';
                    content += '        </div>';
                    content += '    </div>';
                    content += '</div>';
                    $('#list-product-' + id).html(content);
                }
            }
        });
        $("#list-product-" + id).focus();
    }

    $(window).on('resize orientationchange', function() {
        $('.responsive-brand').slick('resize');
        $('responsive-desktop').slick('resize');
        $('responsive-mobile').slick('resize');
    });

    // function runSwalSetLocation() {
    //     Swal.fire({
    //         title: 'Silahkan set "LOKASI ANDA" terlebih dahulu!',
    //         type: 'info',
    //         showCancelButton: false,
    //         confirmButtonColor: '#a7c22a',
    //         showCloseButton: true,
    //         iconColor: '#a7c22a',
    //         confirmButtonText: 'Set Lokasi Saya!'

    //     }).then((result) => {
    //         if (result.value) {
    //             window.location.href = 'javascript:my_location();';
    //         }
    //     })

    // }
    $(document).ready(function() {
        $('.responsive-desktop').slick({
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 4000
        });
        $('.responsive-mobile').slick({
            arrows: false,
            dots: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 4000
        });

        $('.responsive-brand').slick({
            autoplay: true,
            autoplaySpeed: 3000,
            slidesToShow: 5,
            responsive: [{
                    breakpoint: 1000,
                    settings: {

                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {

                        slidesToShow: 1
                    }
                }
            ]

        });

        // if (!localStorage.getItem("visited")) {
        //     runSwalSetLocation();
        //     localStorage.setItem("visited", true);
        // }
    });

    function addtoCart(id, store_id) {
        $.ajax({
            url: site_url + 'catalog/products/add_to_cart_ajax_new',
            type: 'post',
            data: {
                product: id,
                store: store_id,
                qty: 1
            },
            beforeSend: function() {
                $("#tags-load").css('display', 'block');
            },
            complete: function() {
                $("#tags-load").css('display', 'none');
            },
            success: function(data) {
                data = JSON.parse(data);
                if (data.status == 'error') {
                    Swal.fire({
                        type: 'error',
                        title: 'Terjadi Kesalahan',
                        text: data.message
                    });
                } else {
                    var length = Object.keys(data.items).length;
                    var total = 0;
                    var total_qty = 0;
                    if(length > 0) {
                        var html = '<div style="max-height: 200px; overflow: auto">';
                        html += '<div class="carts-items">';
                        $.each(data.items, function(index, val) {
                            total += (Number(val.price * val.quantity));
                            total_qty += Number(val.quantity);
                            html += '<div class="row" id="cart-header-' + val.id_encrypt + '" style="padding: 10px 10px;border-bottom:1px solid #EEE;">';
                            html += '<div class="cart-left col-md-4" style="display: inline-block;">';
                            html += '<a class="cart-image" href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                            html += '<img style="width: 50px;" src="' + site_url + 'files/images/' + val.image + '" alt="" title="">';
                            html += '</a>';
                            html += '</div>';
                            html += '<div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">';
                            html += '<div class="">';
                            html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '" style="color:#000;">';
                            html += val.name;
                            html += '</a>';
                            html += '</div>';
                            html += '<div class="cart-price">';
                            html += '<span class="money" style="color: #97C23C;">' + formatRupiah(val.price, '.') + '</span>';
                            html += '<span class="x" style="color: #97C23C;">x ' + val.quantity + '</span>'
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        });
                        html += '</div>';
                        html += '</div>';
                        html += '<div class="subtotal" style="color: black;padding: 15px 20px;font-size:14px;border-top:1px solid #EEE;">';
                        html += '<span>Subtotal:</span><span class="cart-total-right money" style="float: right;">' + formatRupiah(total, '.') + '</span>'
                        html += '</div>';
                        html += '<div class="action" style="padding: 15px 20px;margin-top:-10px;">';
                        html += '<a href="' + site_url + 'cart" class="btn btn-show-all" style="font-size:10px; width: 100%;border-radius:3px;">Lihat Semua</a>'
                        html += '</div>';
                    } else {
                        var html = '<p style="font-size: 13px; font-weight: bold; padding: 15px;text-align:center;">Keranjang belanja Anda kosong. Yuk, belanja sekarang!</p>'
                    }

                    $('#cart-target .badge').html(total_qty);
                    $('#cart-button .badge').html(total_qty);
                    $('.icon-cart .cart_text .number').html(total_qty);
                    $('#cart-target-mobile .number').html(total_qty);
                    $('#cart-target .cart-info .cart-content').html(html);
                    $('#cart-target-mobile .cart-info .cart-content').html(html);

                    // new
                    $('.cart_icon #icon_cart .badge').html(total_qty);
                    $('.cart_icon .cart-dropdown').html(html);
                    Swal.fire({
                        type: 'success',
                        title: 'Produk Berhasil Ditambah!',
                        timer: 1000,
                        showConfirmButton: false,
                        customClass: 'swal-class'
                    });
                }
            }
        })
    }

    function formatRupiah(angka, prefix) {
        angka = angka.toString();
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
    }

    function change_store(id, type, type_product) {
        if (type_product == 'promotion') {
            if (type == 'over') {
                $('#text-ellipsis-city-promotion-' + id).attr('style', 'margin-top:-18px;display:block;transition:.3s;');
            } else {
                $('#text-ellipsis-city-promotion-' + id).attr('style', 'margin-top:0px;display:block;transition:.3s;')
            }
        } else if (type_product == 'recomend') {
            if (type == 'over') {
                $('#text-ellipsis-city-recomendation-' + id).attr('style', 'margin-top:-18px;display:block;transition:.3s;');
            } else {
                $('#text-ellipsis-city-recomendation-' + id).attr('style', 'margin-top:0px;display:block;transition:.3s;');
            }
        } else if (type_product == 'best_selling') {
            if (type == 'over') {
                $('#text-ellipsis-city-best_selling-' + id).attr('style', 'margin-top:-18px;display:block;transition:.3s;');
            } else {
                $('#text-ellipsis-city-best_selling-' + id).attr('style', 'margin-top:0px;display:block;transition:.3s;');
            }
        }
    }

    function add_wishlist(e) {
        let product = $(e).data('product');
        let store = $(e).data('store');
        let customer = $(e).data('customer');

        $.ajax({
            url: site_url + 'catalog/products/add_wishlist_new',
            data: {
                product: product,
                store: store,
                customer: customer
            },
            type: 'POST',
            success: function(data) {
                data = JSON.parse(data);
                if (data.status == 'add') {
                    $(e).attr('style', 'color:#d9534f;');
                    Swal.fire({
                        type: 'success',
                        title: 'Wishlist Berhasil Ditambah',
                        timer: 1000,
                        showConfirmButton: false
                    });
                    $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').html(data.total);
                    $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').attr('style', 'position: absolute;right:25px;');
                } else {
                    $(e).attr('style', 'color:#BBB;');
                    Swal.fire({
                        type: 'success',
                        title: 'Wishlist Berhasil Dihapus',
                        timer: 1000,
                        showConfirmButton: false
                    });
                    $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').html(data.total);
                    $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').attr('style', 'position: absolute;right:25px;');
                }
            }
        })
    }

    function btn_load_more(last_view, last_key) {
        $.ajax({
            url: site_url + 'home/load_more_products',
            data: {
                last_view: last_view,
                last_key: last_key
            },
            type: 'POST',
            beforeSend: function() {
                $('#btn-load-more').html('Memuat...');
            },
            success: function(data) {
                $('#btn-load-more').remove();
                $('#list-product-recomendations').append(data);
                // console.log(data);
            }
        });
    }
</script>