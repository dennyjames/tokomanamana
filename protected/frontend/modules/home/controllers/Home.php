<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->lang->load('home', settings('language'));
        $this->load->model('home_model', 'home');
        $this->load->model('catalog/product_model');
        $this->load->helper('text');
    }

    public function index() {
        $this->load->library('user_agent');
        $this->data['banners'] = $this->main->gets('banners', array('type' => 'slider', 'status' => 1));
        $this->data['brands'] = $this->main->gets('brands', array('status' => 1));

        if ($this->session->has_userdata('list_merchant')) {
            $merchants = $this->session->userdata('list_merchant');
            $this->data['merchant_group'] = $merchants[0]['group'];
            foreach ($merchants as $m) {
                $this->data['merchant_list'][] = $m['id'];
            }
        } else {
            $merchants = $this->db->where('group',3)->where('type','merchant')->get('merchants')->result();
            $this->data['merchant_group'] = 3;
            foreach ($merchants as $m) {
                $this->data['merchant_list'][] = $m->id;
            }
        }

        /* setting for last seen product section */
        if($this->ion_auth->logged_in()) {
            $auth_id = $this->data['user']->id;
            $last_seen_product_detail = $this->home->get_all_last_seen_product($auth_id, FALSE, 12);
            $keyword = $this->home->get_recomended_keyword($auth_id, FALSE);
            if(!$keyword) {
                $last_seen_product_detail = $this->home->get_all_last_seen_product(FALSE, $this->input->ip_address(), 12);
                $keyword = $this->home->get_recomended_keyword(FALSE, $this->input->ip_address());
            }
        }
        else {
            $auth_id = $this->input->ip_address();
            $last_seen_product_detail = $this->home->get_all_last_seen_product(FALSE, $auth_id, 12);
            $keyword = $this->home->get_recomended_keyword(FALSE, $auth_id);
        }
        /* if the last seen product is less than 6 data, it will be added with product related */
        if ($last_seen_product_detail) {
            if ($last_seen_product_detail->num_rows() < 12) {
                
                if ($this->ion_auth->logged_in()) {
                    $this->data['last_seen_product'] = $this->home->last_seen_with_additional($auth_id, FALSE, 12);
                } else {
                    $this->data['last_seen_product'] = $this->home->last_seen_with_additional(FALSE, $auth_id, 12);
                }
            }
            else {
                $this->data['last_seen_product'] = $last_seen_product_detail;
            }
        }

        /* if get keyword from product last seen is success
        ** the result will be imploding to an array and find the similiar product
        ** with regexp method 
        ** but, if it's doesn't recomendation product will be filled with most popular product */
        // if ($keyword->num_rows() > 0) {
        //     foreach ($keyword->result() as $k) {
        //         $array[] = $k->keyword;
        //     }

        //     $this->data['recomendations'] = $this->home->get_recomendations(implode('|', $array));
        // }
        // else {
        //     $this->data['recomendations'] = $this->home->get_all_products(12);
        // }

        if($keyword) {
            if ($keyword->num_rows() > 0) {
                foreach ($keyword->result() as $k) {
                    $array[] = $k->keyword;
                }

                $products_recomendations = $this->home->get_recomendations(implode('|', $array));
                if($products_recomendations) {
                    if($products_recomendations->num_rows() < 18) {
                        $this->data['recomendations'] = $this->home->get_all_products(18);
                    } else {
                        $this->data['recomendations'] = $products_recomendations;
                    }
                } else {
                    $this->data['recomendations'] = $this->home->get_all_products(18);
                }
            }
            else {
                $this->data['recomendations'] = $this->home->get_all_products(18);
            }
        } else {
            $this->data['recomendations'] = $this->home->get_all_products(18);
        }

        $this->data['posts'] = $this->main->gets_paging('blogs', 0, 4, array('status' => 1), 'date_added DESC');
        $this->data['categories'] = $this->main->gets('categories', ['active' => 1, 'parent' => 0, 'home_show' => 1], 'sort_order ASC');
        $this->data['promotions'] = $this->home->get_promotions(6);
        // var_dump($this->db->last_query()); die;
        $this->data['best_selling'] = $this->home->get_bestselling_products(6);
        $this->data['meta_description'] = settings('meta_description');
        $this->data['meta_keyword'] = settings('meta_keyword');
        // $this->data['all_products'] = $this->home->get_all_products();
        $this->template->_init();
        $this->output->set_title(settings('meta_title'));
        $this->load->view('index', $this->data);

    }

    public function partner(){
        $this->load->library('user_agent');
          
        $this->data['brands'] = $this->main->gets('brands', array('status' => 1));
        $this->data['meta_description'] = settings('meta_description');
        $this->data['meta_keyword'] = settings('meta_keyword');
        
        $this->template->_init();
        $this->output->set_title(settings('meta_title'));
        $this->load->view('home/partner',$this->data);
    }

    public function get_wishlist () {
        $this->input->is_ajax_request() or exit(redirect('error_403'));

        $user_id = $this->input->post('id');
        $wishlist = $this->home->get_wishlist_product($user_id, 'wishlist.date_added', 'DESC', 12)->result_array();

        foreach ($wishlist as $w => $value) {
            $wishlist[$w]['url'] = seo_url('catalog/products/view/' . $value['product_id'] . '/' . $value['store_id']);
            $wishlist[$w]['image'] = get_image($value['image']);
            $wishlist[$w]['name'] = $value['name'];
            $wishlist[$w]['merchant'] = $value['merchant_name'];
            $wishlist[$w]['price'] = $value['price'];

            $rating = $this->home->get_rating ($value['product_id'], $value['store_id']);

            if (round($rating->rating) > 0) {
                $wishlist[$w]['rating'] = round($rating->rating);
            }

            if ($value['merchant'] == 0) {
                $merchant = $this->session->userdata('list_merchant')[0];
                $price_group = $this->main->get('product_price', array('product' => $value['product_id'], 'merchant_group' => $merchant['group']));

                if ($price_group) {
                    $value['price'] = $price_group->price;
                }
            }

            $promo_data = json_decode($value['promo_data'], TRUE);

            if($promo_data['type'] == 'P') {
                $disc_price = round(($value['price'] * $promo_data['discount']) / 100);
                $label_disc = $promo_data['discount'].'% off';
            } else {
                $disc_price = $promo_data['discount'];
                $label_disc = 'SALE';
            }

            $end_price= $value['price'] - $disc_price;
            $today = date('Y-m-d');
            $today=date('Y-m-d', strtotime($today));
            $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
            $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

            if (($today >= $DateBegin) && ($today <= $DateEnd)){
                $wishlist[$w]['label_disc'] = $label_disc;
                $wishlist[$w]['end_price'] = $end_price;
            }
        }

        $json_response = json_encode($wishlist);
        echo $json_response;
        
    }

    public function get_last_seen () {
        $this->input->is_ajax_request() or exit(redirect('error_403'));

        $user_id = $this->input->post('id');
        $ip_address = $this->input->post('ip');

        /* setting for last seen product section */
        if($user_id) {
            $last_seen_product_detail = $this->home->get_all_last_seen_product($user_id, FALSE, 12, FALSE);
        }
        else {
            $last_seen_product_detail = $this->home->get_all_last_seen_product(FALSE, $ip_address, 12, FALSE);
        }

        /* if the last seen product is less than 6 data, it will be added with product related */
        if ($last_seen_product_detail) {
            if ($last_seen_product_detail->num_rows() < 12) {
                
                if ($user_id) {
                    $this->data['last_seen_product'] = $this->home->last_seen_with_additional($user_id, FALSE, 12);
                } else {
                    $this->data['last_seen_product'] = $this->home->last_seen_with_additional(FALSE, $ip_address, 12);
                }
            }
            else {
                $this->data['last_seen_product'] = $last_seen_product_detail;
            }
        }

        $list = $this->data['last_seen_product']->result_array();

        foreach ($list as $l => $value) {
            $list[$l]['url'] = seo_url('catalog/products/view/' . $value['product_id'] . '/' . $value['store_id']);
            $list[$l]['image'] = get_image($value['image']);
            $list[$l]['name'] = $value['name'];
            $list[$l]['merchant'] = $value['merchant_name'];
            $list[$l]['price'] = $value['price'];

            $rating = $this->home->get_rating ($value['product_id'], $value['store_id']);

            if (round($rating->rating) > 0) {
                $list[$l]['rating'] = round($rating->rating);
            }

            if ($value['merchant'] == 0) {
                $merchant = $this->session->userdata('list_merchant')[0];
                $price_group = $this->main->get('product_price', array('product' => $value['product_id'], 'merchant_group' => $merchant['group']));

                if ($price_group) {
                    $list[$l]['price'] = $price_group->price;
                }
            }

            $promo_data = json_decode($value['promo_data'], TRUE);

            if($promo_data['type'] == 'P') {
                $disc_price = round(($value['price'] * $promo_data['discount']) / 100);
                $label_disc = $promo_data['discount'].'% off';
            } else {
                $disc_price = $promo_data['discount'];
                $label_disc = 'SALE';
            }

            $end_price= $value['price'] - $disc_price;
            $today = date('Y-m-d');
            $today=date('Y-m-d', strtotime($today));
            $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
            $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

            if (($today >= $DateBegin) && ($today <= $DateEnd)){
                $list[$l]['label_disc'] = $label_disc;
                $list[$l]['end_price'] = $end_price;
            }
        }

        $json_response = json_encode($list);
        echo $json_response;
    }

    public function get_products() {
        $merchants = $this->session->userdata('list_merchant');
        $merchant_group = $merchants[0]['group'];
        if ($merchants) {
            foreach ($merchants as $m) {
                $list[] = $m['id'];
            }
            $data['products'] = $this->home->products($this->input->post('category'), $list, $merchant_group, 9);
            $this->load->view('product', $data);
        }
    }

    public function get_terbaru(){
        $id =$this->input->post('id');
        $banner =$this->input->post('banner');

        $merchants = $this->session->userdata('list_merchant');
        $merchant_group = $merchants[0]['group'];
        if ($merchants) {
            foreach ($merchants as $m) {
                $list[] = $m['id'];
            }
            $result = $this->home->products_terbaru($id, $list, $merchant_group, 9)->result_array();

            foreach($result as $key => $value){
                $result[$key]['url'] = seo_url('catalog/products/view/' . $value['id']);
                $result[$key]['image'] = get_image($value['image']);
                $result[$key]['rupiah'] = rupiah($value['price']);
                $result[$key]['banner'] = get_image($banner);
            }

            $json_response = json_encode($result);
            echo $json_response;
        }
    }

    public function get_terlaris(){
        $id =$this->input->post('id');
        $banner =$this->input->post('banner');

        $merchants = $this->session->userdata('list_merchant');
        $merchant_group = $merchants[0]['group'];
        if ($merchants) {
            foreach ($merchants as $m) {
                $list[] = $m['id'];
            }
            $result = $this->home->products_terlaris($id, $list, $merchant_group, 9)->result_array();

            foreach($result as $key => $value){
                $result[$key]['url'] = seo_url('catalog/products/view/' . $value['id']);
                $result[$key]['image'] = get_image($value['image']);
                $result[$key]['rupiah'] = rupiah($value['price']);
                $result[$key]['banner'] = get_image($banner);
            }

            $json_response = json_encode($result);
            echo $json_response;
        }
    }

    public function load_more_products() {
        $this->input->is_ajax_request() or exit(redirect('error_403'));

        $last_view = $this->input->post('last_view');
        $last_key = $this->input->post('last_key');

        $per_page = 24;

        if($this->ion_auth->logged_in()) {
            $auth_id = $this->data['user']->id;
            $keyword = $this->home->get_recomended_keyword($auth_id, FALSE);

            if($keyword) {
                foreach ($keyword->result() as $k) {
                    $array[] = $k->keyword;
                }
            }
        }
        else {
            $auth_id = $this->input->ip_address();
            $keyword = $this->home->get_recomended_keyword(FALSE, $auth_id);
        }
        if($keyword) {
            if($keyword->num_rows() > 0) {
                foreach ($keyword->result() as $k) {
                    $array[] = $k->keyword;
                }
                $products_recomendations = $this->home->get_recomendations(implode('|', $array));
                if($products_recomendations) {
                    if($products_recomendations->num_rows() < 18) {
                        $products = $this->home->get_more_products($last_view, $per_page);
                        $count = $this->home->get_more_products($last_view, NULL);
                    } else {
                        $products = $this->home->get_more_recomendations(implode('|', $array), $last_view, $per_page);
                        $count = $this->home->get_more_recomendations(implode('|', $array), $last_view, NULL);
                    }
                }
            } else {
                $products = $this->home->get_more_products($last_view, $per_page);
                $count = $this->home->get_more_products($last_view, NULL);
            }
        } else {
            $products = $this->home->get_more_products($last_view, $per_page);
            $count = $this->home->get_more_products($last_view, NULL);
        }
        $this->data['products'] = $products;
        $this->data['products_num'] = $count->num_rows();
        $this->data['products_limit'] = $per_page;
        $this->data['key'] = $last_key;

        $this->load->view('more_products', $this->data, false);
    }

}
