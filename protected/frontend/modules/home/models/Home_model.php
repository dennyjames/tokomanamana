<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home_model extends CI_Model {

    function get_all_last_seen_product ($user_id = NULL, $ip_address = NULL, $limit = NULL) {
        if ($user_id) {
            $this->db->where('product_last_review.user_id', $user_id);
        }
        else {
            $this->db->where('product_last_review.ip_address', $ip_address);
        }
        $this->db->select('product_last_review.id,
                            product_last_review.user_id,
                            product_last_review.ip_address,
                            product_last_review.modified_time,
                            products_principal_stock.id AS stock_id,
                            products_principal_stock.branch_id AS store_id,
                            products_principal_stock.product_id AS product_id,
                            merchants.name AS merchant_name,
                            products.merchant,
                            products.name,
                            products.price,
                            products.discount,
                            products.promo,
                            products.preorder,
                            products.promo_data,
                            products.variation,
                            products.price_grosir,
                            product_image.image')
                ->join('products_principal_stock', 'product_last_review.product_id = products_principal_stock.id', 'LEFT')
                ->join('products', 'products_principal_stock.product_id = products.id', 'LEFT')
                ->join('merchants', 'products_principal_stock.branch_id = merchants.id', 'LEFT')
                ->join('product_image', 'product_image.product = products.id AND product_image.primary = 1', 'LEFT')
                ->where('products.status != ', 0)
                ->where('(products_principal_stock.quantity > 0 OR products.preorder = 1)');

        if ($limit) {
            $this->db->limit($limit);
        }
        
        $this->db->order_by('product_last_review.modified_time', 'DESC');

        $query = $this->db->get('product_last_review');

        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function last_seen_with_additional ($user_id, $ip_address, $limit) {
        if ($user_id) {
            $auth = 'product_last_review.user_id = "' . $user_id . '"';
        }
        else {
            $auth = 'product_last_review.ip_address = "' . $ip_address . '"';
        }
        return $this->db->query("SELECT products_principal_stock.branch_id AS store_id,
                                        products_principal_stock.product_id AS product_id,
                                        merchants.name AS merchant_name,
                                        products.name,
                                        products.merchant,
                                        products.price,
                                        products.discount,
                                        products.promo,
                                        products.preorder,
                                        products.promo_data,
                                        product_image.image,
                                        products.variation,
                                        products.price_grosir
                                FROM product_last_review
                                LEFT JOIN products_principal_stock ON product_last_review.product_id = products_principal_stock.id
                                LEFT JOIN products ON products_principal_stock.product_id = products.id
                                LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                WHERE products.status != 0
                                AND $auth
                                AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
                                
                                UNION

                                SELECT products_principal_stock.branch_id AS store_id,
                                        products_principal_stock.product_id AS product_id,
                                        merchants.name AS merchant_name,
                                        products.name,
                                        products.merchant,
                                        products.price,
                                        products.discount,
                                        products.promo,
                                        products.preorder,
                                        products.promo_data,
                                        product_image.image,
                                        products.variation,
                                        products.price_grosir
                                FROM products_principal_stock
                                LEFT JOIN products ON products_principal_stock.product_id = products.id
                                LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                WHERE products.status != 0
                                AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation = 1)

                                LIMIT $limit");
    }

    function get_wishlist_product ($user_id, $sort = NULL, $sorting_rule = NULL, $limit = NULL) {
        $this->db->select('wishlist.customer AS user_id,
                            products_principal_stock.branch_id AS store_id,
                            products_principal_stock.product_id AS product_id,
                            products.name AS name,
                            products.merchant,
                            products.price,
                            products.discount,
                            products.promo,
                            products.promo_data,
                            products.preorder,
                            product_image.image,
                            products.variation,
                            products.price_grosir,
                            merchants.name AS merchant_name')
                ->join('products_principal_stock', 'wishlist.product = products_principal_stock.product_id AND wishlist.branch_id = products_principal_stock.branch_id', 'LEFT')
                ->join('merchants', 'wishlist.branch_id = merchants.id AND products_principal_stock.branch_id = merchants.id', 'LEFT')
                ->join('products', 'products_principal_stock.product_id = products.id', 'LEFT')
                ->join('product_image', 'product_image.product = products.id AND product_image.primary = 1')
                ->where('products.status !=', 0)
                ->where('wishlist.customer', $user_id);
        
        if ($sort) {
            $this->db->order_by($sort, $sorting_rule);
        }

        if ($limit) {
            $this->db->limit($limit);
        }
        $query = $this->db->get('wishlist');

        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function bestsellers($limit = 6) {
        $this->db->select('COUNT(op.quantity) total, p.id, p.name, p.price, pi.image')
                ->join('products p', 'p.id = op.product', 'left')
                ->join('product_image pi', 'op.product = pi.product AND primary = 1', 'left')
                ->where('p.status', 1)
                ->group_by('op.product')
                ->order_by('RAND()')
                ->limit($limit);
        $query = $this->db->get('order_product op');
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    // function promotions($limit = 6) {
    //     $this->db->select('p.id, pd.name, p.price, p.image')
    //             ->join('product_description pd', 'p.id = pd.id', 'left')
    //             ->where('p.brand', 11)
    //             ->where('p.status !=', 0)
    //             ->order_by('RAND()')
    //             ->limit($limit);
    //     $query = $this->db->get('products p');
    //     return ($query->num_rows() > 0) ? $query : FALSE;
    // }

    // function get_promotions ($limit = 6) {
    //     $this->db->select('products.name,
    //                         products.id AS product_id,
    //                         products.price,
    //                         products.discount,
    //                         products.promo,
    //                         products.preorder,
    //                         products.promo_data,
    //                         products.merchant,
    //                         products.viewed,
    //                         products.free_ongkir,
    //                         merchants.name AS store_name,
    //                         merchants.id AS store_id,
    //                         product_image.image')
    //             ->join('products', 'products_principal_stock.product_id = products.id', 'LEFT')
    //             ->join('merchants', 'products_principal_stock.branch_id = merchants.id', 'LEFT')
    //             ->join('product_image', 'product_image.product = products.id AND product_image.primary = 1', 'LEFT')
    //             ->where('products.promo', 1)
    //             ->where('products.status != ', 0)
    //             ->where('(products_principal_stock.quantity > 0 OR products.preorder = 1)')
    //             ->order_by('RAND()')
    //             ->limit($limit);
    //     $query = $this->db->get('products_principal_stock');
    //     return ($query->num_rows() > 0) ? $query : FALSE;
    // }

    function get_promotions ($limit = 6) {
        $query = "
        (SELECT DISTINCT products.name, products.id AS product_id, products.price, products.discount, products.promo, products.preorder, products.promo_data, products.viewed, products.free_ongkir, product_image.image, merchants.name AS merchant_name, products_principal_stock.branch_id AS store_id , merchants.type AS store_type, products_principal_stock.quantity AS quantity, products.variation, products.price_grosir
            FROM products_principal_stock
            LEFT JOIN products ON products_principal_stock.product_id = products.id
            LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
            LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
            WHERE products.promo = 1
            AND products.status != 0
            AND products.store_type = 'principal'
            AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
            GROUP BY products_principal_stock.branch_id, products_principal_stock.product_id)

            UNION

        (SELECT DISTINCT products.name, products.id AS product_id, products.price, products.discount, products.promo, products.preorder, products.promo_data, products.viewed, products.free_ongkir, product_image.image, merchants.name AS merchant_name, products.store_id AS store_id , merchants.type AS store_type, products.quantity AS quantity, products.variation, products.price_grosir
            FROM products
            LEFT JOIN merchants ON products.store_id = merchants.id
            LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
            WHERE products.promo = 1
            AND products.status != 0
            AND products.store_type = 'merchant'
            AND (products.quantity > 0 OR products.preorder = 1 OR products.variation = 1))

            ORDER BY viewed DESC

            LIMIT $limit
        ";
        $query = $this->db->query($query);
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function get_sub_categories($parent = 0, $limit = 4) {
        $this->db->select('c.id, c.viewed, c.name')
                ->join('categories c', 'cp.category = c.id', 'left')
                ->where('cp.path', $parent)
                ->where('cp.category !=', $parent)
                ->where('active', 1)
                ->order_by('c.viewed', 'DESC')
                ->limit($limit);
        $query = $this->db->get('category_path cp');
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    // function get_all_products ($limit = 12) {
    //     $this->db->distinct()
    //             ->select('products.name,
    //                         products.id AS product_id,
    //                         products.price,
    //                         products.discount,
    //                         products.promo,
    //                         products.preorder,
    //                         products.promo_data,
    //                         products.merchant,
    //                         products.free_ongkir,
    //                         merchants.name AS store_name,
    //                         merchants.id AS store_id,
    //                         product_image.image')
    //             ->join('products', 'products_principal_stock.product_id = products.id', 'LEFT')
    //             ->join('merchants', 'products_principal_stock.branch_id = merchants.id', 'LEFT')
    //             ->join('product_image', 'product_image.product = products.id AND product_image.primary = 1', 'LEFT')
    //             ->where('products.status != ', 0)
    //             ->where('(products_principal_stock.quantity > 0 OR products.preorder = 1)')
    //             ->order_by('RAND()')
    //             ->limit($limit);
    //     $query = $this->db->get('products_principal_stock');

    //     return ($query->num_rows() > 0) ? $query : FALSE;
    // }

    function get_all_products ($limit = 12) {
        $query = "
            (SELECT DISTINCT products.name, products.id AS product_id, products.price, products.discount, products.promo, products.preorder, products.promo_data, products.viewed, products.free_ongkir, product_image.image, merchants.name AS merchant_name, products_principal_stock.branch_id AS store_id , merchants.type AS store_type, products_principal_stock.quantity AS quantity, products.date_modified AS date_modified, products.variation, products.price_grosir

                FROM products_principal_stock 
                LEFT JOIN products ON products_principal_stock.product_id = products.id 
                LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id 
                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1 
                WHERE products.status !=0 
                AND products.store_type = 'principal'
                AND (products_principal_stock.quantity >0 OR products.preorder = 1 OR products.variation = 1)
                GROUP BY products_principal_stock.branch_id, products_principal_stock.product_id)

                UNION

            (SELECT DISTINCT products.name, products.id AS product_id, products.price, products.discount, products.promo, products.preorder, products.promo_data, products.viewed, products.free_ongkir, product_image.image, merchants.name AS merchant_name, products.store_id AS store_id, merchants.type AS store_type, products.quantity AS quantity, products.date_modified AS date_modified, products.variation, products.price_grosir

                FROM products 
                LEFT JOIN merchants ON products.store_id = merchants.id 
                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1 
                WHERE products.status !=0 
                AND products.store_type = 'merchant'
                AND (products.quantity >0 OR products.preorder = 1 OR products.variation = 1)
                )

                ORDER BY date_modified DESC
                LIMIT $limit
        ";
        $query = $this->db->query($query);
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    // function get_lastseen_products($limit) {
    //     return $this->db->query("SELECT products_principal_stock.branch_id,
    //                                     products_principal_stock.product_id,
    //                                     products_principal_stock.quantity AS product_quantity,
    //                                     products.name AS product_name,
    //                                     products.merchant,
    //                                     products.price,
    //                                     products.discount,
    //                                     products.promo,
    //                                     products.preorder,
    //                                     products.promo_data,
    //                                     products.merchant,
    //                                     merchants.name AS merchant_name,
    //                                     product_image.image
    //                             FROM products_principal_stock
    //                             LEFT JOIN products ON products_principal_stock.product_id = products.id
    //                             LEFT JOIN product_image ON products.id = product_image.product AND product_image.primary = 1
    //                             LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
    //                             WHERE products.status != 0 AND (products_principal_stock.quantity > 0 OR products.preorder = 1)
    //                             ORDER BY RAND()
    //                             LIMIT $limit");
    // }

    // function products($category, $merchant, $merchant_group, $limit = 12) {
    //     $merchant = implode(',', $merchant);
    //     return $this->db->query("SELECT * FROM (
    //                 SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, (CASE WHEN pp.price IS NULL THEN p.price ELSE pp.price END) price, pi.image, pp.id ppid
    //                 FROM products p
    //                 LEFT JOIN product_price pp ON pp.product = p.id AND pp.merchant_group = $merchant_group
    //                 LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
    //                 WHERE p.merchant = 0 AND p.status = 1
    //                 UNION
    //                 SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, p.price, pi.image, 0
    //                 FROM products p
    //                 LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
    //                 WHERE merchant IN ($merchant) AND p.status = 1
    //                 ) p
    //              WHERE p.category IN (SELECT category FROM category_path WHERE path = $category) LIMIT $limit");
    // }

    function products_terbaru($category, $merchant, $merchant_group, $limit = 9) {
        $merchant = implode(',', $merchant);
        return $this->db->query("SELECT * FROM (
                    SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, (CASE WHEN pp.price IS NULL THEN p.price ELSE pp.price END) price, pi.image, pp.id ppid
                    FROM products p
                    LEFT JOIN product_price pp ON pp.product = p.id AND pp.merchant_group = $merchant_group
                    LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
                    WHERE p.merchant = 0 AND p.status = 1
                    UNION
                    SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, p.price, pi.image, 0
                    FROM products p
                    LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
                    WHERE merchant IN ($merchant) AND p.status = 1
                    ) p
                 WHERE p.category IN (SELECT category FROM category_path WHERE path = $category) ORDER BY p.id DESC LIMIT $limit");
    }

    function products_terlaris($category, $merchant, $merchant_group, $limit = 9) {
        $merchant = implode(',', $merchant);
        return $this->db->query("SELECT p.*, count(po.id) as ord FROM (
                    SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, (CASE WHEN pp.price IS NULL THEN p.price ELSE pp.price END) price, pi.image, pp.id ppid
                    FROM products p
                    LEFT JOIN product_price pp ON pp.product = p.id AND pp.merchant_group = $merchant_group
                    LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
                    WHERE p.merchant = 0 AND p.status = 1
                    UNION
                    SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, p.price, pi.image, 0
                    FROM products p
                    LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
                    WHERE merchant IN ($merchant) AND p.status = 1
                    ) p LEFT JOIN order_product po ON po.product = p.id
                 WHERE p.category IN (SELECT category FROM category_path WHERE path = $category) GROUP BY po.product ORDER BY ord desc, p.id asc LIMIT $limit");
    }

    // function get_popular($limit) {
    //     $this->db->distinct()
    //             ->select('products.name,
    //                         products.id,
    //                         products.price,
    //                         product_image.image,
    //                         merchants.name AS store_name,
    //                         merchants.id AS store_id')
    //             ->join('product_image', 'product_image.product = products.id AND product_image.primary = 1', 'LEFT')
    //             ->join('products_principal_stock', 'products.id = products_principal_stock.product_id', 'LEFT')
    //             ->join('merchants', 'products_principal_stock.branch_id = merchants.id', 'LEFT')
    //             ->where('products.status', 1)
    //             ->where('products_principal_stock.quantity >', 0)
    //             ->order_by('products.viewed', 'DESC')
    //             ->limit($limit);
    //     $query = $this->db->get('products');

    //     return ($query->num_rows() > 0) ? $query : FALSE;
    // }

    // function get_recomended_keyword ($id, $ip) {
    //     if ($id) {
    //         $auth = 'product_last_review.user_id = ' . $id;
    //     }
    //     elseif ($ip) {
    //         $auth = 'product_last_review.ip_address = "' . $ip . '"';
    //     }
    //     $query = $this->db->query("SELECT DISTINCT SUBSTR(products.name, 1, INSTR(products.name, ' ')-1) AS keyword
    //                                 FROM product_last_review
    //                                 LEFT JOIN products_principal_stock
    //                                 ON product_last_review.product_id = products_principal_stock.id
    //                                 LEFT JOIN products
    //                                 ON products_principal_stock.product_id = products.id
    //                                 WHERE $auth
    //                                 AND products.status = 1
    //                                 ORDER BY product_last_review.modified_time");
        
    //     return ($query->num_rows() > 0) ? $query : FALSE;
    // }

    // NEW METHOD
    function get_recomended_keyword ($id, $ip) {
        if ($id) {
            $auth = 'product_last_review.user_id = ' . $id;
        }
        elseif ($ip) {
            $auth = 'product_last_review.ip_address = "' . $ip . '"';
        }
        $query = $this->db->query("(SELECT DISTINCT SUBSTR(products.name, 1, INSTR(products.name, ' ')-1) AS keyword, product_last_review.modified_time
                                    FROM product_last_review
                                    LEFT JOIN products ON product_last_review.product_id = products.id
                                    WHERE $auth
                                    AND products.status = 1)

                                    UNION

                                    (SELECT DISTINCT SUBSTR(products.name, 1, INSTR(products.name, ' ')-1) AS keyword, product_last_review.modified_time
                                    FROM product_last_review
                                    LEFT JOIN products_principal_stock
                                    ON product_last_review.product_id = products_principal_stock.id
                                    LEFT JOIN products
                                    ON products_principal_stock.product_id = products.id
                                    WHERE $auth
                                    AND products.status = 1)
                                    
                                    ORDER BY modified_time");
        
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    // function get_recomendations ($string) {
    //     $query = $this->db->query("SELECT DISTINCT products.name,
    //                                                 products.id AS product_id,
    //                                                 products.merchant,
    //                                                 products.price,
    //                                                 products.discount,
    //                                                 products.promo,
    //                                                 products.preorder,
    //                                                 products.promo_data,
    //                                                 products.merchant,
    //                                                 products.viewed,
    //                                                 products.free_ongkir,
    //                                                 product_image.image,
    //                                                 merchants.name AS merchant_name,
    //                                                 merchants.id AS store_id
    //                                 FROM products_principal_stock
    //                                 LEFT JOIN products ON products_principal_stock.product_id = products.id
    //                                 LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
    //                                 LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
    //                                 WHERE products.status = 1
    //                                 AND (products_principal_stock.quantity > 0 OR products.preorder = 1)
    //                                 AND products.name REGEXP '($string)'
    //                                 ORDER BY RAND()
    //                                 LIMIT 6");

    //     return ($query->num_rows() > 0) ? $query : FALSE;
    // }

    function get_recomendations($string) {
        $query = $this->db->query(" (SELECT DISTINCT products.name, 
                                    products.id AS product_id, 
                                    products.price, 
                                    products.discount, 
                                    products.promo, 
                                    products.preorder, 
                                    products.promo_data, 
                                    products.viewed AS viewed, 
                                    products.free_ongkir, 
                                    product_image.image, 
                                    merchants.name AS merchant_name, 
                                    products_principal_stock.branch_id AS store_id,
                                    merchants.type AS store_type, 
                                    products_principal_stock.quantity AS quantity,
                                    products.date_modified AS date_modified,
                                    products.variation,
                                    products.price_grosir
                                    FROM products_principal_stock 
                                    LEFT JOIN products ON products_principal_stock.product_id = products.id
                                    LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                    LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id 
                                    WHERE products.status = 1 
                                    AND products.store_type = 'principal'
                                    AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
                                    AND products.store_id != 0
                                    AND products.name REGEXP '($string)'
                                    GROUP BY products_principal_stock.branch_id, products_principal_stock.product_id)

                                    UNION

                                    (SELECT DISTINCT products.name, 
                                    products.id AS product_id, 
                                    products.price, 
                                    products.discount, 
                                    products.promo, 
                                    products.preorder, 
                                    products.promo_data, 
                                    products.viewed AS viewed, 
                                    products.free_ongkir, 
                                    product_image.image, 
                                    merchants.name AS merchant_name, 
                                    products.store_id AS store_id, 
                                    merchants.type AS store_type, 
                                    products.quantity AS quantity,
                                    products.date_modified AS date_modified,
                                    products.variation,
                                    products.price_grosir
                                    FROM products 
                                    LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                    LEFT JOIN merchants ON products.store_id = merchants.id
                                    WHERE products.status = 1 
                                    AND products.store_type = 'merchant'
                                    AND (products.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
                                    AND products.store_id != 0
                                    AND products.name REGEXP '($string)')

                                    ORDER BY date_modified DESC
                                    LIMIT 18
                                ");
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function get_rating ($product, $store) {
        $this->db->select('IFNULL((SUM(rating_speed) + SUM(rating_service) + SUM(rating_accuracy)) / (COUNT(id) * 3), 0) AS rating,
                            IFNULL(COUNT(id), 0) AS review')
                ->where('product', $product)
                ->where('merchant', $store)
                ->where('status', 1);

        $query = $this->db->get('product_review');
        return ($query->result() > 0) ? $query->row() : FALSE;
    }

    function get_bestselling_products($limit = 6) {
        $query = "
            (SELECT DISTINCT products.name, products.id AS product_id, products.price, products.discount, products.promo, products.preorder, products.promo_data, products.viewed AS viewed, products.free_ongkir, product_image.image, merchants.name AS merchant_name, products_principal_stock.branch_id AS store_id , merchants.type AS store_type, products_principal_stock.quantity AS quantity, products.date_modified AS date_modified, products.variation, products.price_grosir

                FROM products_principal_stock 
                LEFT JOIN products ON products_principal_stock.product_id = products.id 
                LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id 
                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1 
                WHERE products.status !=0 
                AND products.store_type = 'principal'
                AND (products_principal_stock.quantity >0 OR products.preorder = 1 OR products.variation = 1)
                GROUP BY products_principal_stock.branch_id, products_principal_stock.product_id)

                UNION

            (SELECT DISTINCT products.name, products.id AS product_id, products.price, products.discount, products.promo, products.preorder, products.promo_data, products.viewed AS viewed, products.free_ongkir, product_image.image, merchants.name AS merchant_name, products.store_id AS store_id, merchants.type AS store_type, products.quantity AS quantity, products.date_modified AS date_modified, products.variation, products.price_grosir

                FROM products 
                LEFT JOIN merchants ON products.store_id = merchants.id 
                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1 
                WHERE products.status !=0 
                AND products.store_type = 'merchant'
                AND (products.quantity >0 OR products.preorder = 1 OR products.variation = 1)
                )

                ORDER BY viewed DESC
                LIMIT $limit
        ";
        $query = $this->db->query($query);
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function get_more_recomendations($keyword, $last_view, $limit) {
        if($limit) {
            $query_limit = " LIMIT $limit";
        } else {
            $query_limit = NULL;
        }
        $query = "(SELECT DISTINCT products.name, 
        products.id AS product_id, 
        products.price, 
        products.discount, 
        products.promo, 
        products.preorder, 
        products.promo_data, 
        products.viewed AS viewed, 
        products.free_ongkir, 
        product_image.image, 
        merchants.name AS merchant_name, 
        products_principal_stock.branch_id AS store_id,
        merchants.type AS store_type, 
        products_principal_stock.quantity AS quantity,
        products.date_modified AS date_modified,
        products.variation,
        products.price_grosir
        FROM products_principal_stock 
        LEFT JOIN products ON products_principal_stock.product_id = products.id
        LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
        LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
        WHERE products.status = 1 
        AND products.store_type = 'principal'
        AND (products_principal_stock.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
        AND products.store_id != 0
        AND products.date_modified < '$last_view'
        AND products.name REGEXP '($keyword)'
        GROUP BY products_principal_stock.branch_id, products_principal_stock.product_id)

        UNION

        (SELECT DISTINCT products.name, 
        products.id AS product_id, 
        products.price, 
        products.discount, 
        products.promo, 
        products.preorder, 
        products.promo_data, 
        products.viewed AS viewed, 
        products.free_ongkir, 
        product_image.image, 
        merchants.name AS merchant_name, 
        products.store_id AS store_id, 
        merchants.type AS store_type, 
        products.quantity AS quantity,
        products.date_modified AS date_modified,
        products.variation,
        products.price_grosir
        FROM products 
        LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
        LEFT JOIN merchants ON products.store_id = merchants.id
        WHERE products.status = 1 
        AND products.store_type = 'merchant'
        AND (products.quantity > 0 OR products.preorder = 1 OR products.variation = 1)
        AND products.store_id != 0
        AND products.date_modified < '$last_view'
        AND products.name REGEXP '($keyword)')

        ORDER BY date_modified DESC" . $query_limit;
        $query = $this->db->query($query);
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function get_more_products($last_view, $limit) {
        if($limit != NULL) {
            $query_limit = " LIMIT $limit";
        } else {
            $query_limit = NULL;
        }
        $query = "
            (SELECT DISTINCT products.name, products.id AS product_id, products.price, products.discount, products.promo, products.preorder, products.promo_data, products.viewed, products.free_ongkir, product_image.image, merchants.name AS merchant_name, products_principal_stock.branch_id AS store_id , merchants.type AS store_type, products_principal_stock.quantity AS quantity, products.date_modified AS date_modified, products.variation, products.price_grosir

                FROM products_principal_stock 
                LEFT JOIN products ON products_principal_stock.product_id = products.id 
                LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id 
                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1 
                WHERE products.status !=0 
                AND products.store_type = 'principal'
                AND (products_principal_stock.quantity >0 OR products.preorder = 1 OR products.variation = 1)
                AND products.date_modified < '$last_view'
                GROUP BY products_principal_stock.branch_id, products_principal_stock.product_id)

                UNION

            (SELECT DISTINCT products.name, products.id AS product_id, products.price, products.discount, products.promo, products.preorder, products.promo_data, products.viewed, products.free_ongkir, product_image.image, merchants.name AS merchant_name, products.store_id AS store_id, merchants.type AS store_type, products.quantity AS quantity, products.date_modified AS date_modified, products.variation, products.price_grosir

                FROM products 
                LEFT JOIN merchants ON products.store_id = merchants.id 
                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1 
                WHERE products.status !=0 
                AND products.store_type = 'merchant'
                AND (products.quantity >0 OR products.preorder = 1 OR products.variation = 1)
                AND products.date_modified < '$last_view'
                )

                ORDER BY date_modified DESC" . $query_limit;
        $query = $this->db->query($query);
        return ($query->num_rows() > 0) ? $query : FALSE;
    }
}
