<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Wishlist extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!$this->ion_auth->logged_in()) {
            redirect('member/login?back=' . uri_string());
        }

        $this->load->library('form_validation');
        $this->load->library('breadcrumb');
        $this->load->library('pagination');
        $this->load->language('member', settings('language'));
        $this->load->model('member_model', 'member');

        $customer = $this->session->userdata('user');
        $this->data['notification_invoice'] = $this->member->get_invoice_for_notification($customer->id);
    }

    public function history($tab = 'all') {
        //pagination
        $config['base_url'] = site_url('member/history/' . $tab . '/');
        $config['total_rows'] = $this->member->getOrders($this->data['user']->id, '', '')->num_rows();
        $config['per_page'] = 8;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('account_order_history'), site_url('member/history'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['orders'] = $this->member->getOrders($this->data['user']->id, $config['per_page'], $start);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'member/history';

        $this->output->set_title(lang('account_order_history') . ' - ' . settings('meta_title'));
        $this->template->_init();
//        $this->load->js('assets/frontend/js/modules/member_history.js');
        $this->load->view('member/history', $this->data);
    }

    public function order_detail($id) {
        $this->data['data'] = $this->member->get_order(decode($id));
        $this->data['page'] = 'member/history';
        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('account_order_history'), site_url('member/history'));
        $this->breadcrumb->add($this->data['data']->code, '#');
        $this->data['breadcrumb'] = $this->breadcrumb->output();

        $this->output->set_title('Pesanan:: ' . $this->data['data']->code . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->js('assets/frontend/js/modules/member_order_detail.js');
        $this->load->view('member/order_detail', $this->data);
    }

    public function profile() {
        $this->form_validation->set_rules('fullname', 'Nama Lengkap', 'trim|required');
        $this->form_validation->set_rules('phone', 'No. Handphone', 'trim|required');
        $this->form_validation->set_rules('birthday', 'Tanggal Lahir', 'trim|callback_check_birthday');
        $this->form_validation->set_rules('gender', 'Jenis Kelamin', 'trim|required');

        if ($this->form_validation->run() == true) {
            do {
                $data = $this->input->post(null, true);
                $data['birthday'] = date('Y-m-d', strtotime($data['year'] . '-' . $data['month'] . '-' . $data['day']));
                $this->main->update('customers', array('fullname' => $data['fullname'], 'phone' => $data['phone'], 'birthday' => $data['birthday'], 'gender' => $data['gender']), array('id' => $this->data['user']->id));
                $this->session->unset_userdata('user');
                $this->session->set_flashdata('success', lang('member_message_success_update_profile'));
            } while (0);
        }

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('account_profile'), site_url('member/history'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'member/profile';

        $this->output->set_title(lang('account_profile') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('member/profile', $this->data);
    }

    public function review() {
        $config['base_url'] = seo_url('member/review');
        $config['total_rows'] = $this->member->get_orders_review($this->data['user']->id, '', '')->num_rows();
        $config['per_page'] = 10;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('account_review'), site_url('member/review'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['orders'] = $this->member->get_orders_review($this->data['user']->id, $config['per_page'], $start);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'member/review';

        $this->output->set_title(lang('account_review') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->js('assets/frontend/js/modules/member_review.js');
        $this->load->view('member/review', $this->data);
    }

    public function address() {
        $this->form_validation->set_rules('old_password', 'Password Lama', 'trim|required|callback_check_password_old');
        $this->form_validation->set_rules('new_password', 'Password Baru', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('re_password', 'Konfirmasi Password', 'trim|required|matches[new_password]');

        if ($this->form_validation->run() == true) {
            do {
                $data = $this->input->post(null, true);
                $this->ion_auth->change_password($this->data['user']->email, $data['old_password'], $data['re_password']);
                $this->session->set_flashdata('success', 'Password baru berhasil disimpan!');
            } while (0);
        }

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('account_address'), site_url('member/address'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['addresses'] = $this->member->addresses($this->data['user']->id);
        $this->data['provincies'] = $this->main->gets('provincies', array(), 'name asc');
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'member/address';

        $this->output->set_title(lang('account_address') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->js('assets/frontend/js/modules/member_address.js');
        $this->load->view('member/address', $this->data);
    }

    public function security() {
        $this->form_validation->set_rules('old_password', 'Password Lama', 'trim|required|callback_check_password_old');
        $this->form_validation->set_rules('new_password', 'Password Baru', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('re_password', 'Konfirmasi Password', 'trim|required|matches[new_password]');

        if ($this->form_validation->run() == true) {
            do {
                $data = $this->input->post(null, true);
                $this->ion_auth->change_password($this->data['user']->email, $data['old_password'], $data['re_password']);
                $this->session->set_flashdata('success', 'Password baru berhasil disimpan!');
            } while (0);
        }

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('account_security'), site_url('member/security'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'member/security';

        $this->output->set_title(lang('account_security') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('member/security', $this->data);
    }

    public function payment_confirmation() {
        $this->form_validation->set_rules('code', 'lang:member_payment_confirmation_field_code', 'trim|required');
        $this->form_validation->set_rules('to', 'lang:member_payment_confirmation_field_to', 'trim|required');
        $this->form_validation->set_rules('date', 'lang:member_payment_confirmation_field_date', 'trim|required');
        $this->form_validation->set_rules('total', 'lang:member_payment_confirmation_field_total', 'trim|required|numeric');
        $this->form_validation->set_rules('note', 'lang:member_payment_confirmation_field_note', 'trim');
        if ($this->form_validation->run() == true) {
            $data = array(
                'order_status' => settings('order_payment_confirm_status'),
                'payment_to' => $this->input->post('to'),
                'payment_date' => $this->input->post('date'),
                'payment_total' => $this->input->post('total'),
                'payment_note' => $this->input->post('note'),
            );
            $this->main->update('orders', $data, array('id' => $this->input->post('code')));
            $this->main->insert('order_history', array('order' => $this->input->post('code'), 'order_status' => $data['order_status']));
            send_mail('noreply@karawangshop.com', 'rifkysyaripudin@gmail.com', 'Konfirmasi Pembayaran', 'Konfirmasi Pembayaran:<br>Bayar Ke: ' . $data['payment_to'] . '<br>Tanggal: ' . $data['payment_date'] . '<br>Total: ' . $data['payment_total'] . '<br>Catatan: ' . $data['payment_note']);
            $this->session->set_flashdata('success', lang('member_message_success_payment_confirmation'));
        }

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('account_text_payment_confirmation'), site_url('member/payment_confirmation'));

        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['orders'] = $this->main->gets('orders', array('customer' => $this->data['user']->id, 'order_status' => settings('order_new_status')));
        $this->data['page'] = 'member/payment_confirmation';

        $this->output->set_title(lang('account_text_payment_confirmation') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->css('assets/frontend/css/libs/bootstrap-datepicker3.min.css');
//        $this->load->js('assets/frontend/js/moment.js');
        $this->load->js('assets/frontend/js/bootstrap-datepicker.min.js');
        $this->load->js('assets/frontend/js/modules/member_payment_confirmation.js');
        $this->load->view('member/payment_confirmation', $this->data);
    }

    public function save_review() {
        $this->form_validation->set_rules('rating_speed', 'Rating Kecepatan', 'trim|required');
        $this->form_validation->set_rules('rating_service', 'Rating Pelayanan', 'trim|required');
        $this->form_validation->set_rules('rating_accuracy', 'Rating Kecepatan', 'trim|required');
        $this->form_validation->set_rules('description', 'Isi Ulasan', 'trim|required');
        if ($this->form_validation->run() == true) {
            $data = $this->input->post(null, true);
            $order_product = $data['id'];
            unset($data['id']);
            $product = $this->main->get('order_product', array('id' => $order_product));
            $invoice = $this->main->get('order_invoice', array('id' => $product->invoice));
            $data['product'] = $product->product;
            $data['customer'] = $this->data['user']->id;
            $data['merchant'] = $invoice->merchant;
//            $this->main->insert('product_review', array('product' => $product, 'customer' => $this->data['user']->id, 'rating' => $data['rating'], 'description' => $data['description']));
            $this->main->insert('product_review', $data);
            $html = '<div class="row"><div class="col-sm-3">Kecepatan</div><div class="col-sm-9"><span class="spr-starratings spr-review-header-starratings">';
            for ($i = 1; $i <= $data['rating_speed']; $i++) {
                $html .= '<i class="spr-icon spr-icon-star"></i>';
            }
            for ($i = $data['rating_speed'] + 1; $i <= 5; $i++) {
                $html .= '<i class="spr-icon spr-icon-star-empty"></i>';
            }
            $html .= '</span></div></div>';
            $html .= '<div class="row"><div class="col-sm-3">Pelayanan</div><div class="col-sm-9"><span class="spr-starratings spr-review-header-starratings">';
            for ($i = 1; $i <= $data['rating_service']; $i++) {
                $html .= '<i class="spr-icon spr-icon-star"></i>';
            }
            for ($i = $data['rating_service'] + 1; $i <= 5; $i++) {
                $html .= '<i class="spr-icon spr-icon-star-empty"></i>';
            }
            $html .= '</span></div></div>';
            $html .= '<div class="row"><div class="col-sm-3">Akurasi</div><div class="col-sm-9"><span class="spr-starratings spr-review-header-starratings">';
            for ($i = 1; $i <= $data['rating_accuracy']; $i++) {
                $html .= '<i class="spr-icon spr-icon-star"></i>';
            }
            for ($i = $data['rating_accuracy'] + 1; $i <= 5; $i++) {
                $html .= '<i class="spr-icon spr-icon-star-empty"></i>';
            }
            $html .= '</span></div></div>';
            $html .= '<div class="spr-review-content">
                      <p class="spr-review-content-body">' . $data['description'] . '</p>
                      </div>';
            $return = array('html' => $html, 'status' => 'success');
        } else {
            $return = array('message' => '<div class="alert alert-danger fade in">' . validation_errors() . '</div>', 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function get_cities($province, $city_selected = '') {
        $cities = $this->main->gets('cities', array('province' => $province), 'name ASC');
        $output = '<option value="">Pilih kota</option>';
        if ($cities) {
            foreach ($cities->result() as $city) {
                $output .= '<option value="' . $city->id . '" ' . (($city_selected && $city_selected == $city->id) ? 'selected' : '') . '>' . $city->type . ' ' . $city->name . '</option>';
            }
        }
        echo $output;
    }

    public function get_districts($city, $district_selected = '') {
        $districts = $this->main->gets('districts', array('city' => $city), 'name ASC');
        $output = '<option value="">Pilih kecamatan</option>';
        if ($districts) {
            foreach ($districts->result() as $district) {
                $output .= '<option value="' . $district->id . '" ' . (($district_selected && $district_selected == $district->id) ? 'selected' : '') . '>' . $district->name . '</option>';
            }
        }
        echo $output;
    }

    public function get_address() {
        $address = $this->main->get('customer_address', array('id' => $this->input->post('id')));
        echo json_encode($address);
    }

    public function save_address() {
        $this->form_validation->set_rules('title', 'Nama Alamat', 'trim|required');
        $this->form_validation->set_rules('name', 'lang:checkout_shipping_field_name', 'trim|required');
        $this->form_validation->set_rules('phone', 'lang:checkout_shipping_field_phone', 'trim|required');
        $this->form_validation->set_rules('address', 'lang:checkout_shipping_field_address', 'trim|required');
        $this->form_validation->set_rules('province', 'lang:checkout_shipping_field_province', 'required');
        $this->form_validation->set_rules('city', 'lang:checkout_shipping_field_city', 'required');
        $this->form_validation->set_rules('district', 'lang:checkout_shipping_field_district', 'required');
        $this->form_validation->set_rules('postcode', 'lang:checkout_shipping_field_postcode', 'trim|required');

        $return['status'] = 'error';
        if ($this->form_validation->run() == true) {
            $data = $this->input->post(null, true);
            if ($data['id']) {
                $address = $data['id'];
                $this->main->update('customer_address', $data, array('id' => $address));
                $return['type'] = 'edit';
            } else {
                $data['customer'] = $this->data['user']->id;
                $address = $this->main->insert('customer_address', $data);
                $return['type'] = 'add';
            }
            $province = $this->main->get('provincies', array('id' => $data['province']));
            $city = $this->main->get('cities', array('id' => $data['city']));
            $district = $this->main->get('districts', array('id' => $data['district']));
            $return['id'] = $address;
            $return['status'] = 'success';
            $return['html'] = '<tr id="address-' . $address . '">
                                    <td>
                                        <div style="font-weight: 800">' . $data['name'] . '</div>
                                        <div>' . $data['phone'] . '</div>
                                    </td>
                                    <td>
                                        <div style="font-weight: 800">' . $data['title'] . '</div>
                                        <div>' . nl2br($data['address']) . '</div>
                                    </td>
                                    <td>' . $province->name . ', ' . $city->name . ', ' . $district->name . ' ' . $data['postcode'] . '<br>Indonesia</td>
                                    <td><a href="javascript:void(0);" onclick="edit(\'' . $address . '\')">Ubah</a> | <a href="javascript:void(0);" onclick="remove(\'' . $address . '\')">Hapus</a></td>
                                </tr>';
        } else {
            $return['message'] = '<div class="alert alert-danger fade in">' . validation_errors() . '</div>';
        }

        echo json_encode($return);
    }

    public function remove_address($id) {
        $this->main->delete('customer_address', array('id' => $id));
    }

    public function check_birthday() {
        if (!checkdate($this->input->post('month'), $this->input->post('day'), $this->input->post('year'))) {
            $this->form_validation->set_message('check_birthday', 'Isian tanggal lahir tidak benar.');
            return false;
        } else {
            return true;
        }
    }

    public function check_email($email) {
        if ($email == $this->data['user']->email)
            return true;
        if ($this->ion_auth->email_check($email)) {
            $this->form_validation->set_message('check_email', 'Email sudah terdaftar oleh akun lain.');
            return false;
        } else {
            return true;
        }
    }

    public function check_password_old($password) {
        if ($this->ion_auth->hash_password_db($this->data['user']->id, $this->input->post('old_password'))) {
            return true;
        } else {
            $this->form_validation->set_message('check_password_old', 'Password Lama tidak sesuai.');
            return false;
        }
    }

    public function tracking($invoice) {
        $invoice = decode($invoice);
        $invoice = $this->main->get('order_invoice', array('id' => $invoice));
        if ($invoice) {
            $courier = explode('-', $invoice->shipping_courier);
            $awb = $invoice->tracking_number;
            if ($awb) {
                $this->load->library('rajaongkir');
                $tracking = $this->rajaongkir->waybill($awb, $courier[0]);
                $data['tracking'] = json_decode($tracking)->rajaongkir;
                $this->load->view('member/tracking', $data);
            } else {
                echo '<p>No. Resi tidak valid</p>';
            }
        }
    }

}
