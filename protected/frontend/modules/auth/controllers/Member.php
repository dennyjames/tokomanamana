<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Member extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $customer = $this->session->userdata('user');
        //print_r($customer);
        $this->load->model('Model');

        $sess_point = $this->session->userdata('member_point');
        if(!$sess_point){
            $data_sess_point = array(
                'tab_point' => '0'
            );

            $this->session->set_userdata($data_sess_point);
        }

        $this->load->library('form_validation');
        $this->load->model('member_model', 'member');
        $this->load->model('orders');
        if (!$this->input->is_ajax_request()) {
            if (!$this->ion_auth->logged_in()) {
                redirect('member/login?back=' . uri_string());
            }
            $this->load->library('breadcrumb');
            $this->load->library('pagination');

            $this->data['notification_invoice'] = $this->member->get_invoice_for_notification($customer->id);
        }


        $this->load->language('member', settings('language'));

//        $this->data['notification_invoice'] = array();
    }

    public function point() {
        $param_redirect = $this->Model->cek_cart_detail($this->session->userdata('email'));
        if($param_redirect == '0'){
            redirect(base_url('checkout/cart_point'));
        }else if($param_redirect == '1'){
            redirect(base_url('checkout/checkout_reward'));
        }
        $config['base_url'] = site_url('member/point/');
        $config['total_rows'] = $this->main->count('wishlist', array('customer' => $this->data['user']->id));
        $config['per_page'] = 20;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add('Point', site_url('member/point'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $merchants = $this->session->userdata('list_merchant');
        $merchant_group = $merchants[0]['group'];
        $this->data['products'] = $this->member->get_wishlist($this->data['user']->id, $merchant_group, $config['per_page'], $start);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'member/point';

        $this->output->set_title('Poin Saya' . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('member/point', $this->data);
    }

    public function riwayat_tukar_poin() {
        $param_redirect = $this->Model->cek_cart_detail($this->session->userdata('email'));
        if($param_redirect == '0'){
            redirect(base_url('checkout/cart_point'));
        }else if($param_redirect == '1'){
            redirect(base_url('checkout/checkout_reward'));
        }
        $config['base_url'] = site_url('member/point/');
        $config['total_rows'] = $this->main->count('wishlist', array('customer' => $this->data['user']->id));
        $config['per_page'] = 20;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add('Point', site_url('member/point'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $merchants = $this->session->userdata('list_merchant');
        $merchant_group = $merchants[0]['group'];
        $this->data['products'] = $this->member->get_wishlist($this->data['user']->id, $merchant_group, $config['per_page'], $start);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'member/point';

        $this->output->set_title('Riwayat Tukar Poin' . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('member/riwayat_tukar_poin', $this->data);
    }

    public function coupons(){
        $param_redirect = $this->Model->cek_cart_detail($this->session->userdata('email'));
        if($param_redirect == '0'){
            redirect(base_url('checkout/cart_point'));
        }else if($param_redirect == '1'){
            redirect(base_url('checkout/checkout_reward'));
        }
        $config['base_url'] = site_url('member/point/');
        $config['total_rows'] = $this->main->count('wishlist', array('customer' => $this->data['user']->id));
        $config['per_page'] = 20;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add('Point', site_url('member/point'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $merchants = $this->session->userdata('list_merchant');
        $merchant_group = $merchants[0]['group'];
        $this->data['products'] = $this->member->get_wishlist($this->data['user']->id, $merchant_group, $config['per_page'], $start);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'member/point';

        $this->output->set_title('Coupon Saya' . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('member/coupons', $this->data);
    }

    public function order_reward_detail($id) {
        $m = $this->Model;
        $this->data['m'] = $m;
        $join = array(
            0 => 'reward r-r.id=rr.id_reward',
            1 => 'point_exchange_history peh-peh.id_reward_claim=rr.id'
        );
        $data = $m->get_data('', 'reward_claim_temp rr', $join, array('rr.id' => decode($id)))->row();
        //$data = $this->member->get_order(decode($id));
        $this->data['page'] = 'member/point';
        $this->data['data'] = $data;
        //$this->data['data']->payment_method_name = $this->main->get('payment_methods', ['name' => $this->data['data']->payment_method])->title;
        //$this->data['payment'] = json_decode($this->data['data']->payment_to);
        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add('Riwayat Tukar Poin', site_url('member/point'));
        $this->breadcrumb->add(decode($id), '#');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->output->set_title('Tukar Poin:: ' . $data->nama . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->js('assets/frontend/js/modules/member_order_detail.js');
        $this->load->view('member/order_reward_detail', $this->data);
    }

    public function history($tab = 'all') {
        //pagination
        $config['base_url'] = site_url('member/history/' . $tab . '/');
        $config['total_rows'] = $this->member->getOrders($this->data['user']->id, '', '')->num_rows();
        $config['per_page'] = 8;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('account_order_history'), site_url('member/history'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['orders'] = $this->member->getOrders($this->data['user']->id, $config['per_page'], $start);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'member/history';

        $this->output->set_title(lang('account_order_history') . ' - ' . settings('meta_title'));
        $this->template->_init();
//        $this->load->js('assets/frontend/js/modules/member_history.js');
        $this->load->view('member/history', $this->data);
    }

    public function order_detail($id) {
        $data = $this->member->get_order(decode($id));
//        if ($this->input->post('cancel') && $data->payment_method == 'kredivo') {
//            $this->main->update('orders', array('payment_status' => 'Cancel'), array('id' => $data->id));
//            $this->main->update('order_invoice', array('order_status' => settings('order_cancel_status')), array('order' => $data->id));
//            $data = $this->member->get_order(decode($id));
//        }
        $this->data['page'] = 'member/history';
        $this->data['data'] = $data;
        $this->data['data']->payment_method_name = $this->main->get('payment_methods', ['name' => $this->data['data']->payment_method])->title;
        $this->data['payment'] = json_decode($this->data['data']->payment_to);
        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('account_order_history'), site_url('member/history'));
        $this->breadcrumb->add($this->data['data']->code, '#');
        $this->data['breadcrumb'] = $this->breadcrumb->output();

        $this->output->set_title('Pesanan:: ' . $this->data['data']->code . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->js('assets/frontend/js/modules/member_order_detail.js');
        $this->load->view('member/order_detail', $this->data);
    }

    // public function profile() {
    //     $this->form_validation->set_rules('fullname', 'Nama Lengkap', 'trim|required');
    //     // $this->form_validation->set_rules('phone', 'No. Handphone', 'trim|required');
    //     $this->form_validation->set_rules('email', 'Email', 'trim|required');
    //     $this->form_validation->set_rules('birthday', 'Tanggal Lahir', 'trim|callback_check_birthday');
    //     $this->form_validation->set_rules('gender', 'Jenis Kelamin', 'trim|required');

    //     if ($this->form_validation->run() == true) {
    //         do {
    //             $data = $this->input->post(null, true);
    //             $data['birthday'] = date('Y-m-d', strtotime($data['year'] . '-' . $data['month'] . '-' . $data['day']));
    //             $customer = array('fullname' => $data['fullname'], 'phone' => $data['phone'], 'birthday' => $data['birthday'], 'gender' => $data['gender']);
    //             if ($data['phone'] != $this->data['user']->phone) {
    //                 $customer['verification_phone'] = 0;
    //                 $this->session->set_flashdata('warning', 'No. Handphone Anda berubah, segera lakukan verifikasi ulang. <a href="' . site_url('member/verification_phone') . '">disini</a>');
    //             }
    //             $this->main->update('customers', $customer, array('id' => $this->data['user']->id));
    //             $user = $this->ion_auth->user()->row();
    //             $this->session->set_userdata('user', $user);
    //             $this->session->set_flashdata('success', lang('message_success_update_profile'));
    //         } while (0);
    //     }

    //     $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
    //     $this->breadcrumb->add('Member', site_url('member'));
    //     $this->breadcrumb->add(lang('account_profile'), site_url('member/history'));
    //     // $this->data['bank_account'] = $this->member->get_bank_account($this->data['user']->id);
    //     // $this->data['bank'] = $this->main->gets('rekening_bank');
    //     $this->data['breadcrumb'] = $this->breadcrumb->output();
    //     $this->data['meta_description'] = settings('meta_description');
    //     $this->data['page'] = 'member/profile';

    //     $this->output->set_title(lang('account_profile') . ' - ' . settings('meta_title'));
    //     $this->template->_init();
    //     $this->load->view('member/profile', $this->data);
    // }

    public function profile() {
        $this->load->library('breadcrumb');
        // $this->form_validation->set_rules('fullname', 'Nama Lengkap', 'trim|required');
        // // $this->form_validation->set_rules('phone', 'No. Handphone', 'trim|required');
        // $this->form_validation->set_rules('email', 'Email', 'trim|valid_email');
        // $this->form_validation->set_rules('birthday', 'Tanggal Lahir', 'trim|callback_check_birthday');
        // $this->form_validation->set_rules('gender', 'Jenis Kelamin', 'trim|required');

        // if ($this->form_validation->run() == true) {
        //     do {
        //         $data = $this->input->post(null, true);
        //         $data['birthday'] = date('Y-m-d', strtotime($data['year'] . '-' . $data['month'] . '-' . $data['day']));
        //         $customer = array('fullname' => $data['fullname'], 'email' => $data['email'], 'birthday' => $data['birthday'], 'gender' => $data['gender']);
        //         $this->main->update('customers', $customer, array('id' => $this->data['user']->id));
        //         $user = $this->ion_auth->user()->row();
        //         $this->session->set_userdata('user', $user);
        //         $this->session->set_flashdata('success', lang('message_success_update_profile'));
        //     } while (0);
        // }

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('account_profile'), site_url('member/history'));
        $this->data['bank_accounts'] = $this->member->get_bank_account($this->data['user']->id);
        $this->data['bank'] = $this->main->gets('rekening_bank');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'member/profile';

        $this->output->set_title(lang('account_profile') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $customer = $this->main->get('customers', ['id' => $this->data['user']->id]);
        $this->data['customer'] = $customer;
        $this->load->js('assets/frontend/js/modules/member_profile.js');
        $this->load->css('https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css');
        $this->load->js('https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js');
        $this->load->view('member/profile', $this->data);
    }

    public function address() {
        $this->load->library('breadcrumb');
        // $this->form_validation->set_rules('old_password', 'Password Lama', 'trim|required|callback_check_password_old');
        // $this->form_validation->set_rules('new_password', 'Password Baru', 'trim|required|min_length[6]');
        // $this->form_validation->set_rules('re_password', 'Konfirmasi Password', 'trim|required|matches[new_password]');

        // if ($this->form_validation->run() == true) {
        //     do {
        //         $data = $this->input->post(null, true);
        //         $this->ion_auth->change_password($this->data['user']->email, $data['old_password'], $data['re_password']);
        //         $this->session->set_flashdata('success', 'Password baru berhasil disimpan!');
        //     } while (0);
        // }

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('account_address'), site_url('member/address'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['addresses'] = $this->member->addresses($this->data['user']->id);
        $this->data['provincies'] = $this->main->gets('provincies', array(), 'name asc');
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'member/address';

        $this->output->set_title(lang('account_address') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->js('assets/frontend/js/modules/member_address.js');
        $this->load->view('member/address', $this->data);
    }

    public function modal_pin_address() {
        $this->load->library('googlemaps');
        $config['loadAsynchronously'] = TRUE;
        $config['map_height'] = '300px';

        $customer = $this->session->userdata('user');
        $user_id = $customer->id;
        $cek_lokasi = $this->member->cek_address_pin($user_id)->row();
        $lat = $cek_lokasi->latitude;
        $lng = $cek_lokasi->longitude;

        if ($this->session->has_userdata('location')) {
            $config['center'] = $this->session->userdata('location')['lat'] . ',' . $this->session->userdata('location')['lng'];
        } else {
            $config['center'] = '-6.186486399999999,106.83409110000002';
        }

        $config['onboundschanged'] = 'if (!centreGot) {
                var mapCentre = map.getCenter();
                marker_0.setOptions({
                        position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng()) 
                });
                save_lat_lng(mapCentre.lat(), mapCentre.lng());
            }
            centreGot = true;';
        /*$config['kmlLayerURL'] = 'http://api.flickr.com/services/feeds/geo/?g=322338@N20&lang=en-us&format=feed-georss';*/

        $cek_postcode = $this->member->cek_postcode($user_id)->row();
        $postcode = $cek_postcode->postcode;
        $get_lokasi = $this->member->get_lnglat($postcode)->result();
        
        $polygon = array();
        foreach($get_lokasi as $lokasi){
                $polygon[] = $lokasi->area;
        }
        
        // START POLYGON //
        $polygon['points'] = $polygon;
        $polygon['strokeColor'] = '#000099';
        $polygon['fillColor'] = '#000099';
        $this->googlemaps->add_polygon($polygon);

        //var_dump($polygon['points']);

        // END POLYGON //

        $config['disableFullscreenControl'] = TRUE;
        $config['disableMapTypeControl'] = TRUE;
        $config['disableStreetViewControl'] = TRUE;
        $config['places'] = TRUE;
        $config['placesAutocompleteInputID'] = 'search-location';
        $config['placesAutocompleteBoundsMap'] = TRUE; // set results biased towards the maps viewport
        $config['placesAutocompleteOnChange'] = 'map.setCenter(this.getPlace().geometry.location); 
            marker_0.setOptions({
                        position: new google.maps.LatLng(this.getPlace().geometry.location.lat(), this.getPlace().geometry.location.lng()) 
                });
            save_lat_lng(this.getPlace().geometry.location.lat(), this.getPlace().geometry.location.lng());';
        $this->googlemaps->initialize($config);

        $marker = array();
        $marker['draggable'] = true;
        $marker['ondragend'] = 'save_lat_lng(event.latLng.lat(), event.latLng.lng());';
        $this->googlemaps->add_marker($marker);
        $this->data['map'] = $this->googlemaps->create_map();
        $this->data['lat'] = $lat;
        $this->data['lng'] = $lng;
        $this->load->view('member/address_pin', $this->data);
    }

    public function test(){
        $postcode = $this->input->post('postcode');
        $customer = $this->session->userdata('user');
        $user_id = $customer->id;
        $cek_postcode = $this->member->cek_postcode($user_id)->num_rows();
        if($cek_postcode>0){
            $this->main->update('postcode_temp', array('postcode' => $postcode), array('user_id' => $user_id));
            echo json_encode('UPDATE_');
        } else {
            $this->main->insert('postcode_temp', array('user_id' => $user_id, 'postcode' => $postcode));
            echo json_encode('INSERT_');
        }        
    }

    public function set_lokasi_pin() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $this->session->set_userdata('location', $data);
        $location = $data;
        $lat = $location[lat];
        $lng = $location[lng];
        $user_id = $this->data['user']->id;

        $cek_pin = $this->member->cek_address_pin($user_id)->num_rows();        
    }

    public function cek_lokasi_pin() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $customer = $this->session->userdata('user');
        $user_id = $customer->id;

        $cek_pin = $this->member->cek_address_pin($user_id)->row();

        $cek_pin_num = $this->member->cek_address_pin($user_id)->num_rows();

        if($cek_pin_num > 0){
            $address = $this->main->get('customer_address', array('customer' => $user_id));
        } else {
            $address = 'FALSER';
        }

        echo json_encode($address);          
    }

    public function security() {
        $this->form_validation->set_rules('old_password', 'Password Lama', 'trim|required|callback_check_password_old');
        $this->form_validation->set_rules('new_password', 'Password Baru', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('re_password', 'Konfirmasi Password', 'trim|required|matches[new_password]');
        $this->form_validation->set_rules('token', 'Kode Verifikasi', 'trim|required');

        $user = $this->main->get('customers', ['id' => $this->data['user']->id]);
        if ($this->form_validation->run() == true) {
            do {
                $data = $this->input->post(null, true);
                if ($data['token'] == $user->verification_code) {
                    $this->main->update('customers', array('verification_code' => NULL), array('id' => $user->id));
                    $this->ion_auth->change_password($user->phone, $data['old_password'], $data['re_password']);

                    $this->session->set_flashdata('success', 'Password baru berhasil disimpan!');
                    $this->session->set_userdata('user', $user);
                } else {
                    $this->session->set_flashdata('error', 'Kode verifikasi salah!');
                }
                redirect('member/security');
            } while (0);
        }

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('account_security'), site_url('member/security'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'member/security';
        $this->data['customer'] = $this->main->get('customers', ['id' => $this->data['user']->id]);

        $this->output->set_title(lang('account_security') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('member/security', $this->data);
    }

    public function payment_confirmation() {
        $this->form_validation->set_rules('to', 'Rekening Tujuan', 'trim|required');
        $this->form_validation->set_rules('amount', 'Total Transfer', 'trim|required|numeric');
        $this->form_validation->set_rules('from', 'Nama Pengirim', 'trim|required');
        $this->form_validation->set_rules('note', 'Catatan', 'trim');
        if ($this->form_validation->run() == true) {
            $data = $this->input->post(null, true);
            $order = $this->main->get('orders', array('id' => decode($data['id'])));
            unset($data['id']);
            $payment_to = json_decode($order->payment_to, true);
            $data['to'] = $payment_to[$data['to']];
            $this->main->update('orders', array('payment_to' => json_encode($data), 'payment_status' => 'Confirmed', 'payment_date' => date('Y-m-d')), array('id' => $order->id));
            $this->main->update('order_invoice', array('order_status' => settings('order_payment_confirmed_status')), array('order' => $order->id));
            $return = array('status' => 'success', 'message' => '<div class="alert alert-success">Konfirmasi pembayaran telah kami terima. Pembayaran akan segera kami verifikasi.</div>', 'id' => encode($order->id));
        } else {
            $return = array('status' => 'error', 'message' => '<div class="alert alert-danger">' . validation_errors() . '</div>');
        }
        echo json_encode($return);
    }

    public function receipt_confirmation() {
        $id = $this->input->post('id');
        $invoice = $this->main->get('order_invoice', array('id' => decode_id($id)));
        $merchant = $this->main->get('merchants', array('id' => $invoice->merchant));
        $this->main->update('order_invoice', array('order_status' => settings('order_finish_status'), 'due_date_finish_order' => date('Y-m-d H:i:s', strtotime(' +3 day'))), array('id' => decode_id($id)));
        $this->main->insert('order_history', array('order' => $invoice->order, 'invoice' => $invoice->id, 'order_status' => settings('order_finish_status')));

        //send balance to merchant
        $balance = $merchant->cash_balance + $invoice->total;
        $this->main->insert('merchant_balances', array('merchant' => $invoice->merchant, 'type' => 'in', 'invoice' => $invoice->id, 'amount' => $invoice->total, 'balance' => $balance));
        $this->main->update('merchants', array('cash_balance' => $balance), array('id' => $merchant->id));

        $this->load->helper('text');
        $this->data['invoice'] = $invoice;
        $this->data['order'] = $this->orders->get($this->data['invoice']->order);
        $this->data['merchant'] = $merchant;
        $this->data['products'] = $this->main->gets('order_product', array('invoice' => $this->data['invoice']->id));
        $customer = $this->main->get('customers', array('id' => $this->data['order']->customer));
        $message = $this->load->view('email/transaction/order_finish', $this->data, true);
        $email_data = array(
            'from' => settings('send_email_from'),
            'from_name' => settings('store_name'),
            'to' => $customer->email,
            'subject' => 'Pesanan Selesai: ' . $this->data['order']->code,
            'message' => $message
        );
        // $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
        send_mail($email_data['from'], $email_data['from_name'], $email_data['to'], $email_data['subject'], $email_data['message']);

        if ($customer->verification_phone) {
            $sms = json_decode(settings('sprint_sms'), true);
            $sms['m'] = 'Pesanan ' . $this->data['order']->code . ' telah selesai. Terimakasih telah berbelanja di ' . settings('store_name');
            $sms['d'] = $customer->phone;
            // $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
            $this->send_sms($sms);
        }
        echo 'success';
    }

    public function wishlist() {
        $config['base_url'] = site_url('member/wishlist/');
        $config['total_rows'] = $this->main->count('wishlist', array('customer' => $this->data['user']->id));
        $config['per_page'] = 20;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('account_wishlist'), site_url('member/wishlist'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        // $merchants = $this->session->userdata('list_merchant');
        // $merchant_group = $merchants[0]['group'];
        // $this->data['products'] = $this->member->get_wishlist($this->data['user']->id, $merchant_group, $config['per_page'], $start);
        $this->data['products'] = $this->member->get_wishlist($this->data['user']->id);
        // var_dump($this->db->last_query());die;
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'member/wishlist';

        $this->output->set_title(lang('account_wishlist') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('member/wishlist', $this->data);
    }

    public function delete_wishlist($id) {
        $id = decode($id);
        if ($data = $this->main->get('wishlist', array('customer' => $this->data['user']->id, 'id' => $id))) {
            $this->main->delete('wishlist', array('id' => $data->id));
            redirect('member/wishlist');
        }
        show_404();
    }

    public function review() {
        $config['base_url'] = seo_url('member/review');
        $config['total_rows'] = $this->member->get_orders_review($this->data['user']->id, '', '')->num_rows();
        $config['per_page'] = 10;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add(lang('account_review'), site_url('member/review'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['orders'] = $this->member->get_orders_review($this->data['user']->id, $config['per_page'], $start);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['meta_description'] = settings('meta_description');
        $this->data['page'] = 'member/review';

        $this->output->set_title(lang('account_review') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->js('assets/frontend/js/modules/member_review.js');
        $this->load->view('member/review', $this->data);
    }

    public function save_review() {
        $this->form_validation->set_rules('rating_speed', 'Rating Kecepatan', 'trim|required');
        $this->form_validation->set_rules('rating_service', 'Rating Pelayanan', 'trim|required');
        $this->form_validation->set_rules('rating_accuracy', 'Rating Kecepatan', 'trim|required');
        $this->form_validation->set_rules('description', 'Isi Ulasan', 'trim|required');
        if ($this->form_validation->run() == true) {
            $data = $this->input->post(null, true);
            $order_product = $data['id'];
            unset($data['id']);
            $product = $this->main->get('order_product', array('id' => $order_product));
            $invoice = $this->main->get('order_invoice', array('id' => $product->invoice));
            $data['product'] = $product->product;
            $data['customer'] = $this->data['user']->id;
            $data['merchant'] = $invoice->merchant;
            $data['order_id'] = $product->order;
//            $this->main->insert('product_review', array('product' => $product, 'customer' => $this->data['user']->id, 'rating' => $data['rating'], 'description' => $data['description']));
            $this->main->insert('product_review', $data);
            $html = '<div class="row"><div class="col-sm-3">Kecepatan</div><div class="col-sm-9"><span class="spr-starratings spr-review-header-starratings">';
            for ($i = 1; $i <= $data['rating_speed']; $i++) {
                $html .= '<i class="spr-icon spr-icon-star"></i>';
            }
            for ($i = $data['rating_speed'] + 1; $i <= 5; $i++) {
                $html .= '<i class="spr-icon spr-icon-star-empty"></i>';
            }
            $html .= '</span></div></div>';
            $html .= '<div class="row"><div class="col-sm-3">Pelayanan</div><div class="col-sm-9"><span class="spr-starratings spr-review-header-starratings">';
            for ($i = 1; $i <= $data['rating_service']; $i++) {
                $html .= '<i class="spr-icon spr-icon-star"></i>';
            }
            for ($i = $data['rating_service'] + 1; $i <= 5; $i++) {
                $html .= '<i class="spr-icon spr-icon-star-empty"></i>';
            }
            $html .= '</span></div></div>';
            $html .= '<div class="row"><div class="col-sm-3">Akurasi</div><div class="col-sm-9"><span class="spr-starratings spr-review-header-starratings">';
            for ($i = 1; $i <= $data['rating_accuracy']; $i++) {
                $html .= '<i class="spr-icon spr-icon-star"></i>';
            }
            for ($i = $data['rating_accuracy'] + 1; $i <= 5; $i++) {
                $html .= '<i class="spr-icon spr-icon-star-empty"></i>';
            }
            $html .= '</span></div></div>';
            $html .= '<div class="spr-review-content">
                      <p class="spr-review-content-body">' . $data['description'] . '</p>
                      </div>';
            $return = array('html' => $html, 'status' => 'success');
        } else {
            $return = array('message' => '<div class="alert alert-danger fade in">' . validation_errors() . '</div>', 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function get_cities($province, $city_selected = '') {
        $cities = $this->main->gets('cities', array('province' => $province), 'name ASC');
        $output = '<option value="">Pilih kota</option>';
        if ($cities) {
            foreach ($cities->result() as $city) {
                $output .= '<option value="' . $city->id . '" ' . (($city_selected && $city_selected == $city->id) ? 'selected' : '') . '>' . $city->type . ' ' . $city->name . '</option>';
            }
        }
        echo $output;
    }

    public function get_districts($city, $district_selected = '') {
        $districts = $this->main->gets('districts', array('city' => $city), 'name ASC');
        $output = '<option value="">Pilih kecamatan</option>';
        if ($districts) {
            foreach ($districts->result() as $district) {
                $output .= '<option value="' . $district->id . '" ' . (($district_selected && $district_selected == $district->id) ? 'selected' : '') . '>' . $district->name . '</option>';
            }
        }
        echo $output;
    }

    public function get_address() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $id = decode($this->input->post('id', true));
        $address = $this->main->get('customer_address', array('id' => $id, 'customer' => $this->data['user']->id));
        $return = [];
        if($address) {
            $return['status'] = 'success';
            $address->id = encode($address->id);
            $return['address'] = $address;
        } else {
            $return['status'] = 'error';
            $return['address'] = '';
            $return['message'] = '<div class="alert alert-danger" role="alert">
                                      Tidak dapat menemukan alamat!
                                    </div>';
        }
        echo json_encode($return);
    }

    public function save_address() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->form_validation->set_rules('title', 'Nama Alamat', 'trim|required');
        $this->form_validation->set_rules('name', 'Nama Penerima', 'trim|required');
        $this->form_validation->set_rules('phone', 'No Handphone', 'trim|required|numeric');
        $this->form_validation->set_rules('address', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('province', 'Provinsi', 'required');
        $this->form_validation->set_rules('city', 'Kota', 'required');
        $this->form_validation->set_rules('district', 'Kecamatan', 'required');
        $this->form_validation->set_rules('postcode', 'Kode Pos', 'trim|required');

        $return['status'] = 'error';
        $message = [];
        if ($this->form_validation->run() == true) {
            $data = $this->input->post(null, true);
            if ($data['id']) {
                $data['id'] = decode($data['id']);
                $address = $data['id'];
                $check = $this->main->get('customer_address', ['id' => $address, 'customer' => $this->data['user']->id]);
                if(!$check) {
                    $return = [
                        'status' => 'error_edit',
                        'message' => '<div class="alert alert-danger" role="alert">
                                      Tidak dapat merubah alamat!
                                    </div>'
                    ];
                    echo json_encode($return);
                    exit();
                } else {
                    $this->main->update('customer_address', $data, array('id' => $address, 'customer' => $this->data['user']->id));
                }
                $return['type'] = 'edit';
            } else {
                $data['id'] = '';
                $data['customer'] = $this->data['user']->id;
                $check_user_address = $this->main->gets('customer_address', ['customer' => $data['customer']]);
                if($check_user_address) {
                    $data['primary'] = 0;
                    $address = $this->main->insert('customer_address', $data);
                } else {
                    $data['primary'] = 1;
                    $address = $this->main->insert('customer_address', $data);
                }
                $return['type'] = 'add';
            }
            $province = $this->main->get('provincies', array('id' => $data['province']));
            $city = $this->main->get('cities', array('id' => $data['city']));
            $district = $this->main->get('districts', array('id' => $data['district']));
            $return['id'] = $address;
            $return['status'] = 'success';
            $return['html'] = '<tr id="address-' . $address . '">
                                    <td>
                                        <div style="font-weight: 800">' . $data['name'] . '</div>
                                        <div>' . $data['phone'] . '</div>
                                    </td>
                                    <td>
                                        <div style="font-weight: 800">' . $data['title'] . '</div>
                                        <div>' . nl2br($data['address']) . '</div>
                                    </td>
                                    <td>' . $province->name . ', ' . $city->name . ', ' . $district->name . ' ' . $data['postcode'] . '<br>Indonesia</td>
                                    <td><a href="javascript:void(0);" onclick="edit(\'' . $address . '\')">Ubah</a> | <a href="javascript:void(0);" onclick="remove(\'' . $address . '\')">Hapus</a></td>
                                </tr>';
        } else {
            // $return['message'] = '<div class="alert alert-danger fade in">' . validation_errors() . '</div>';
            if(form_error('title')) {
                array_push($message, [
                    'message' => form_error('title'),
                    'type' => 'title'
                ]);
            }
            if(form_error('name')) {
                array_push($message, [
                    'message' => form_error('name'),
                    'type' => 'name'
                ]);
            }
            if(form_error('phone')) {
                array_push($message, [
                    'message' => form_error('phone'),
                    'type' => 'phone'
                ]);
            }
            if(form_error('address')) {
                array_push($message, [
                    'message' => form_error('address'),
                    'type' => 'address'
                ]);
            }
            if(form_error('province')) {
                array_push($message, [
                    'message' => form_error('province'),
                    'type' => 'province'
                ]);
            }
            if(form_error('city')) {
                array_push($message, [
                    'message' => form_error('city'),
                    'type' => 'city'
                ]);
            }
            if(form_error('district')) {
                array_push($message, [
                    'message' => form_error('district'),
                    'type' => 'district'
                ]);
            }
            if(form_error('postcode')) {
                array_push($message, [
                    'message' => form_error('postcode'),
                    'type' => 'postcode'
                ]);
            }
            $return['message'] = $message;
        }

        echo json_encode($return);
    }

    public function remove_address() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $id = decode($this->input->post('id', true));
        $check = $this->main->get('customer_address', ['id' => $id, 'customer' => $this->data['user']->id]);
        if($check) {
            if($check->primary == 0) {
                $delete = $this->main->delete('customer_address', array('id' => $id, 'customer' => $this->data['user']->id));
                if($delete) {
                    $return['status'] = 'success';
                } else {
                    $return['status'] = 'error';
                }
            } else {
                $delete = $this->main->delete('customer_address', array('id' => $id, 'customer' => $this->data['user']->id));
                if($delete) {
                    $addresses = $this->main->gets('customer_address', ['customer' => $this->data['user']->id]);
                    if($addresses) {
                        $address = $addresses->result()[0];
                        $this->main->update('customer_address', ['primary' => 1], ['id' => $address->id, 'customer' => $this->data['user']->id]);
                    }
                    $return['status'] = 'success';
                } else {
                    $return['status'] = 'error';
                }
            }
        } else {
            $return['status'] = 'error';
        }
        echo json_encode($return);
    }

    public function check_birthday() {
        if (!checkdate($this->input->post('month'), $this->input->post('day'), $this->input->post('year'))) {
            $this->form_validation->set_message('check_birthday', 'Isian tanggal lahir tidak benar.');
            return false;
        } else {
            return true;
        }
    }

    public function check_email($email) {
        if ($email == $this->data['user']->email)
            return true;
        if ($this->ion_auth->email_check($email)) {
            $this->form_validation->set_message('check_email', 'Email sudah terdaftar oleh akun lain.');
            return false;
        } else {
            return true;
        }
    }

    public function check_password_old($password) {
        if ($this->ion_auth->hash_password_db($this->data['user']->id, $this->input->post('old_password'))) {
            return true;
        } else {
            $this->form_validation->set_message('check_password_old', 'Password Lama tidak sesuai.');
            return false;
        }
    }

    public function tracking($invoice) {
        $invoice = decode($invoice);
        $invoice = $this->main->get('order_invoice', array('id' => $invoice));
        if ($invoice) {
            $courier = explode('-', $invoice->shipping_courier);
            $awb = $invoice->tracking_number;
            if ($awb) {
                $this->load->library('rajaongkir');
                $tracking = $this->rajaongkir->waybill($awb, $courier[0]);
                $data['tracking'] = json_decode($tracking)->rajaongkir;
                $this->load->view('member/tracking', $data);
            } else {
                echo '<p>No. Resi tidak valid</p>';
            }
        }
    }

    public function verification_phone() {
        $customer = $this->main->get('customers', array('id' => $this->data['user']->id));
        if ($customer->verification_phone) {
            redirect('member');
        }

        $this->form_validation->set_rules('token', 'Kode Verifikasi', 'trim|required');
        if ($this->form_validation->run() == true) {
            $token = $this->input->post('token');
            if ($token == $customer->verification_code) {
                $this->main->update('customers', array('verification_phone' => 1, 'verification_code' => ''), array('id' => $customer->id));
                $this->session->set_flashdata('success', 'No. Handphone Anda berhasil diverifikasi.');
                $user = $this->ion_auth->user()->row();
                $this->session->set_userdata('user', $user);
                redirect('member');
            }
        } else {
            $this->data['message'] = validation_errors();

            $this->data['token_time_left'] = 0;
            if (!$customer->verification_code) {
                $this->request_token();
                $this->data['token_time_left'] = 120;
            } else {
                $verification_time = strtotime($customer->verification_sent_time);
                if ((strtotime(date('Y-m-d H:i:s')) - $verification_time) < 120) {
                    $this->data['token_time_left'] = strtotime(date('Y-m-d H:i:s')) - $verification_time;
                }
            }

            $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
            $this->breadcrumb->add('Member', site_url('member'));
            $this->breadcrumb->add('Verifikasi Akun', site_url('member/verification_phone'));
            $this->data['breadcrumb'] = $this->breadcrumb->output();

            $this->template->_init();
            $this->output->set_title('Verifikasi No. Handphone');
            $this->load->view('member/verification_phone', $this->data);
        }
    }

    public function request_token() {
        $this->load->library('sprint');
        $sms = json_decode(settings('sprint_sms'), true);
        $code = rand(1000, 9999);
        $sms['m'] = 'Hai, berikut kode verifikasi TokoManaMana Anda ' . $code;
        $sms['d'] = $this->data['user']->phone;
        $url = $sms['url'];
        unset($sms['url']);
        $this->load->library('sprint');
        $sprint_response = $this->sprint->sms($url, $sms);
        $sprint_response = explode('_', $sprint_response);
        if ($sprint_response[0] == 0) {
            $this->main->update('customers', array('verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')), array('id' => $this->data['user']->id));
        }
    }

    public function change_primary_address() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $id = decode($this->input->post('id', true));
        $customer = $this->data['user']->id;
        $check = $this->main->get('customer_address', ['id' => $id, 'customer' => $customer]);
        $return = [];
        if($check) {
            $this->main->update('customer_address', ['primary' => 0], ['customer' => $customer]);
            $this->main->update('customer_address', ['primary' => 1], ['id' => $id, 'customer' => $customer]);
            $return = [
                'status' => 'success',
                'message' => 'Alamat utama berhasil diubah'
            ];
        } else {
            $return = [
                'status' => 'error',
                'message' => 'Alamat utama gagal diubah!'
            ];
        }
        echo json_encode($return);
    }

    public function verification_email() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('breadcrumb');
        $id = decode($this->input->post('id'));
        $return = [];
        if($id == $this->data['user']->id) {
            $check = $this->main->get('customers', ['id' => $id]);
            if($check) {
                if($check->email_verification == 1) {
                    $return['status'] = 'error';
                    $return['message'] = 'Terjadi kesalahan!';
                    echo json_encode($return);
                    exit();
                }
                if($check->email_verification_sent_time) {
                    $now = time();
                    $email_verification_sent_time = strtotime($check->email_verification_sent_time);
                    $range = $now - $email_verification_sent_time;
                    if($range < 120) {
                        $return['status'] = 'error';
                        $return['message'] = 'Tunggu beberapa saat untuk mengirim ulang email verifikasi!';
                        echo json_encode($return);
                        exit();
                    }
                }
                $customer_name = $check->fullname;
                $customer_email = $check->email;
                // $token = bin2hex(random_bytes(32));
                $code = rand(100000, 999999);
                $this->data = [
                    'name' => $customer_name,
                    'code' => $code,
                    'email' => $customer_email
                ];
                $message = $this->load->view('email/register_confirmation', $this->data, TRUE);
                // $cronjob = [
                //     'from' => settings('send_email_from'),
                //     'from_name' => settings('store_name'),
                //     'to' => $customer_email,
                //     'subject' => 'Verifikasi Email Akun ' . settings('store_name'),
                //     'message' => $message
                // ];
                // $success = $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                $email_data = array(
                    'from' => settings('send_email_from'),
                    'from_name' => settings('store_name'),
                    'to' => $customer_email,
                    'subject' => 'Verifikasi Email Akun ' . settings('store_name'),
                    'message' => $message
                );
                $success = send_mail($email_data['from'], $email_data['from_name'], $email_data['to'], $email_data['subject'], $email_data['message']);
                if($success) {
                    $data_update = [
                        'email_verification_code' => $code,
                        'email_verification' => 2,
                        'email_verification_sent_time' => date('Y-m-d H:i:s')
                    ];
                    $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
                    $this->breadcrumb->add('Member', site_url('member'));
                    $this->breadcrumb->add(lang('account_profile'), site_url('member/history'));
                    $this->data['breadcrumb'] = $this->breadcrumb->output();
                    $this->main->update('customers', $data_update, ['id' => $id]);
                    $return['status'] = 'success';
                    $return['message'] = 'Email verifikasi berhasil dikirim!';
                    $return['load'] = '<script>$("#myTabContent").load(window.location.href + " #myTabContent");</script>';
                } else {
                    $return['status'] = 'error';
                    $return['message'] = 'Terjadi kesalahan!';
                }
            } else {
                $return['status'] = 'error';
                $return['message'] = 'Terjadi kesalahan!';
            }
        } else {
            $return['status'] = 'error';
            $return['message'] = 'Terjadi kesalahan!';
        }
        echo json_encode($return);
    }

    // public function email_confirmation($token) {
    //     $check_user = $this->main->get('customers', ['email_verification_token' => $token]);
    //     if($check_user) {
    //         $this->main->update('customers', ['email_verification' => 1, 'email_verification_token' => ''], ['id' => $check_user->id]);
    //         $this->session->set_flashdata('email_verification_message', $this->set_message_flashdata('success', 'Email berhasil dikonfirmasi!'));
    //         redirect(site_url('member/profile'));
    //     } else {
    //         $this->session->set_flashdata('email_verification_message', $this->set_message_flashdata('danger', 'Konfirmasi Gagal!'));
    //         redirect(site_url('member/profile'));
    //     }
    // }

    public function input_code_verification_email() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->form_validation->set_rules('code_verification', 'Kode Verifikasi', 'trim|required|numeric');
        $message = [];
        $customer = $this->data['user']->id;
        if ($this->form_validation->run() == TRUE) {
            $data = $this->input->post(null, true);
            $verification_code = $data['code_verification'];
            $get_customer = $this->main->get('customers', ['id' => $customer, 'email_verification_code' => $verification_code]);
            if($get_customer) {
                $now = date('Y-m-d H:i:s', time());
                $now = strtotime($now);
                $verification_sent_time = strtotime($get_customer->email_verification_sent_time);
                $range = $now - $verification_sent_time;
                if($range > 7200) {
                    $return['status'] = 'error_verification';
                    $return['message'] = $this->set_message_flashdata('danger', 'Kode verifikasi sudah kadaluarsa!');
                } else {
                    $this->main->update('customers', ['email_verification_sent_time' => NULL, 'email_verification_token' => NULL, 'email_verification' => 1], ['id' => $customer]);
                    $return['status'] = 'success';
                    $return['message'] = 'Email berhasil diverifikasi!';
                }
            } else {
                $return['status'] = 'error_validation';
                $return['message'] = '<span class="help-block">Kode Verifikasi salah!</span>';
            }
        } else {
            $return['status'] = 'error_validation';
            $return['message'] = form_error('code_verification');
        }
        echo json_encode($return);
    }

    private function set_message_flashdata($type, $message) {
        $return = '<div class="alert alert-' . $type . '" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <b>' . $message . '</b>
                  </div>';
        return $return;
    }

    public function save_email() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[customers.email]', [
            'is_unique' => 'Email tersebut sudah digunakan!'
        ]);
        $this->form_validation->set_rules('code_verification', 'Kode Verifikasi', 'trim|required');
        $return = [];
        $message = [];
        $customer = $this->data['user']->id;
        $check = $this->main->get('customers', ['id' => $customer, 'verification_phone' => 1]);

        if($check) {
            if ($this->form_validation->run() === TRUE) {

                if($check->verification_code == $data['code_verification']) {
                    $this->main->update('customers', ['email' => $data['email'], 'email_verification' => 0, 'verification_code' => '', 'verification_sent_time' => NULL], ['id' => $customer]);
                    $return['status'] = 'success';
                    $return['message'] = 'Email berhasil disimpan!';
                } else {
                    $return['status'] = 'error_otp';
                    $return['message'] = $this->set_message_flashdata('danger', 'Kode Verifikasi Salah!');
                }
                
            } else {
                $return['status'] = 'error';
                if(form_error('email')) {
                    array_push($message, [
                        'type' => 'email',
                        'message' => form_error('email')
                    ]);
                }
                if(form_error('code_verification')) {
                    array_push($message, [
                        'type' => 'code_verification',
                        'message' => form_error('code_verification')
                    ]);
                }
                $return['message'] = $message;
            }
        } else {
            $return['status'] = 'error_otp';
            $return['message'] = '';
        }
        echo json_encode($return);
    }

    public function get_email() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $get_email = $this->main->get('customers', ['id' => $this->data['user']->id]);
        $return = [];
        if($get_email->email) {
            $return['value'] = $get_email->email;
            $return['type'] = 'edit';
        } else {
            $return['type'] = 'add';
            $return['value'] = '';
        }
        echo json_encode($return);
    }

    public function get_name() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $get_name = $this->main->get('customers', ['id' => $this->data['user']->id]);
        $return = [];
        if($get_name->fullname) {
            $return['value'] = $get_name->fullname;
            $return['type'] = 'edit';
        } else {
            $return['type'] = 'add';
            $return['value'] = '';
        }
        echo json_encode($return);
    }

    public function save_name() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Nama', 'trim|required');
        $return = [];

        if ($this->form_validation->run() === TRUE) {

            $customer = $this->data['user']->id;
            $this->main->update('customers', ['fullname' => $data['name']], ['id' => $customer]);
            $return['status'] = 'success';
            $return['message'] = 'Nama berhasil disimpan!';
            
        } else {
            $return['status'] = 'error';
            $return['message'] = form_error('name');
        }
        echo json_encode($return);
    }

    public function save_birthday() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        if($data['day'] < 10) {
            $data['day'] = 0 . $data['day'];
        }
        $date_birthday = $data['year'] . '-' . $data['month'] . '-' . $data['day'];
        // $date_birthday = strtotime($date_birthday);
        $customer = $this->data['user']->id;
        $valid_date = date('Y-m-d', strtotime($date_birthday));
        if($date_birthday == $valid_date) {
            // var_dump(true, $date_birthday, $valid_date);
            $this->main->update('customers', ['birthday' => $date_birthday], ['id' => $customer]);
            echo 'success';
        } else {
            // var_dump(false, $date_birthday, $valid_date);
            echo 'error';
        }
        die;
    }

    public function save_gender() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $customer = $this->data['user']->id;
        if($data['gender'] == 'L' || $data['gender'] == 'P') {
            $this->main->update('customers', ['gender' => $data['gender']], ['id' => $customer]);
            echo 'success';
        } else {
            echo 'error';
        }
    }

    public function get_code_verification() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $customer = $this->data['user']->id;
        $get_customer = $this->main->get('customers', ['id' => $customer]);
        $code = rand(1000, 9999);
        date_default_timezone_set('Asia/Jakarta');

        if($get_customer->phone) {
            if($get_customer->verification_sent_time) {
                $now = time();
                $verification_sent_time = strtotime($get_customer->verification_sent_time);
                $range = $now - $verification_sent_time;
                if($range < 120) {
                    $return['status'] = 'error_send';
                    $return['message'] = $this->set_message_flashdata('danger', 'Silahkan tunggu beberapa saat untuk mengirimkan ulang kode verifikasi!');
                    $return['time'] = $range;
                } else {
                    $sms = json_decode(settings('sprint_sms'), true);
                    $sms['m'] = 'Hai, berikut kode verifikasi TokoManaMana anda ' . $code . ' jangan sampai orang lain mengetahuinya!';
                    $sms['d'] = $get_customer->phone;
                    // $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                    $this->send_sms($sms);
                    $this->main->update('customers', ['verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')], ['id' => $customer]);
                    $return['status'] = 'success';
                    $return['message'] = $this->set_message_flashdata('success', 'Kode verifikasi berhasil dikirim!');   
                }
            } else {
                $sms = json_decode(settings('sprint_sms'), true);
                $sms['m'] = 'Hai, berikut kode verifikasi TokoManaMana anda ' . $code . ' jangan sampai orang lain mengetahuinya!';
                $sms['d'] = $get_customer->phone;
                // $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                $this->send_sms($sms);
                $this->main->update('customers', ['verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')], ['id' => $customer]);
                $return['status'] = 'success';
                $return['message'] = $this->set_message_flashdata('success', 'Kode verifikasi berhasil dikirim!');   
            }
        } else {
            $return['status'] = 'error';
            $return['message'] = $this->set_message_flashdata('danger', 'No Handphone belum diisi!');
        }
        echo json_encode($return);
    }

    private function send_sms($sms)
    {
        $this->load->library('sprint');
        $url = $sms['url'];
        $to = $sms['d'];
        unset($sms['url']);
        $result = $this->sprint->sms($url, $sms);
        $result = explode('_', $result);
        if ($result[0] == 0) {
            $this->main->insert('log_sms', array('content' => json_encode($sms), 'to' => $to));
        }
    }

    public function save_norek() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $primary = 0;
        $check_rekening = $this->main->gets('customer_bank', ['customer' => $this->data['user']->id]);
        if($check_rekening) {
            $primary = 0;
            if($check_rekening->num_rows() == 3) {
                $return['status'] = 'error';
                $return['message'] = 'Jumlah rekening anda telah mencapai batas!';
                echo json_encode($return);
                exit();
            }
        } else {
            $primary = 1;
        }
        $data_bank = array(
            'bank_code' => $data['bank_code'],
            'bank_branch' => $data['bank_branch'],
            'bank_account_number' => $data['bank_account_number'],
            'bank_account_name' => $data['bank_account_name'],
            'customer' => $this->data['user']->id,
            'primary' => $primary
        );
        $query = $this->main->insert('customer_bank', $data_bank);
        if($query){
            $return['status'] = 'success';
            $return['message'] = '<div class="alert alert-success fade in">Berhasil Menyimpan Data</div>';
        } else {
            $return['status'] = 'error';
            $return['message'] = '<div class="alert alert-danger fade in">Oops! Terjadi Kesalahan</div>';
        }
        echo json_encode($return);
    }

    public function delete_account_bank() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $id = decode($data['id']);
        $customer = $this->data['user']->id;
        $bank_account = $this->main->get('customer_bank', ['id' => $id, 'customer' => $customer]);
        if($bank_account) {
            $this->main->delete('customer_bank', ['id' => $id, 'customer' => $customer]);
            $return = [
                'status' => 'success',
                'message' => 'Rekening berhasil dihapus!'
            ];
        } else {
            $return = [
                'status' => 'error',
                'message' => 'Terjadi suatu kesalahan!'
            ];
        }
        echo json_encode($return);
    }

    public function balance() {
        $param_redirect = $this->Model->cek_cart_detail($this->session->userdata('email'));
        if($param_redirect == '0'){
            redirect(base_url('checkout/cart_point'));
        }else if($param_redirect == '1'){
            redirect(base_url('checkout/checkout_reward'));
        }
        $config['base_url'] = site_url('member/balance/');

        $this->data['balance'] = $this->main->get('customer_balance', array('customer_id' => $this->data['user']->id));
        $this->data['balance_request'] = $this->main->get('customer_balance_request', array('customer_id' => $this->data['user']->id, 'status' => 0));
        $this->data['balance_history'] = $this->main->gets('customer_balance_history', array('customer_id' => $this->data['user']->id));
        $this->data['balance_request'] = $this->main->gets('customer_balance_request', array('customer_id' => $this->data['user']->id, 'status' => 0));
        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add('Member', site_url('member'));
        $this->breadcrumb->add('Point', site_url('member/point'));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['meta_description'] = settings('meta_description');
        $this->data['bank_account'] = $this->member->get_bank_account($this->data['user']->id)->row();
        $this->data['bank'] = $this->main->gets('rekening_bank');
        $this->data['page'] = 'member/balance';

        $this->output->set_title('Saldo' . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('member/balance', $this->data);
    }

    public function cek_norek(){
        $return = $this->member->get_bank_account($this->data['user']->id)->row();
        echo json_encode($return);
    }
    public function request_balance(){
        $data = $this->input->post(null, true);
        $customer = $this->member->get_bank_account($this->data['user']->id)->row();
        $cek_pass = $this->check_password($data['password']);
        $data_to = array('name' => $customer->name, 'branch' => $customer->bank_branch, 'account_number' => $customer->bank_account_number, 'account_name' => $customer->bank_account_name);
        $to = json_encode($data_to);
        if($cek_pass == true){
            $data_saldo = array(
                'customer_id' => $this->data['user']->id,
                'to' => $to,
                'amount' => $data['amount'],
                'status' => 0
            );
            $query = $this->main->insert('customer_balance_request', $data_saldo);
            $return['status'] = 'success';
        } else {
            $return['status'] = 'error';
            $return['message'] = 'Password Anda Salah!';
        }
        echo json_encode($return);
    }

    public function get_code_verification_new_phone() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $customer = $this->data['user']->id;
        $data = $this->input->post(null, true);
        $phone = $data['phone'];
        $code = rand(1000, 9999);
        date_default_timezone_set('Asia/Jakarta');
        if($phone != '') {
            $check = $this->main->get('customers', ['phone' => $phone]);
            if($check) {
                $return['status'] = 'error_validation';
                $return['message'] = 'No Handphone sudah digunakan!';
            } else {
                $get_customer = $this->main->get('customers', ['id' => $customer]);
                $now = time();
                $verification_sent_time = strtotime($get_customer->verification_sent_time);
                $range = $now - $verification_sent_time;
                if($range < 120) {
                    $return['status'] = 'error';
                    $return['message'] = $this->set_message_flashdata('danger', 'Silahkan tunggu beberapa saat untuk mengirimkan ulang kode verifikasi!');
                    $return['time'] = $range;
                } else {
                    $sms = json_decode(settings('sprint_sms'), true);
                    $sms['m'] = 'Hai, berikut kode verifikasi TokoManaMana anda ' . $code . ' jangan sampai orang lain mengetahuinya!';
                    $sms['d'] = $phone;
                    // $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                    $this->send_sms($sms);
                    $this->main->update('customers', ['verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')], ['id' => $customer]);
                    $return['status'] = 'success';
                    $return['message'] = $this->set_message_flashdata('success', 'Kode verifikasi berhasil dikirim!');   
                }
            }
        } else {
            $return['status'] = 'error_validation';
            $return['message'] = 'No Handphone harus diisi!';
        }
        echo json_encode($return);
    }

    public function save_phone() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $id_customer = $this->data['user']->id;
        $customer_not_phone = $this->main->get('customers', ['id' => $id_customer, 'verification_phone' => 0, 'phone' => '']);
        if($customer_not_phone) {
            $this->form_validation->set_rules('phone', 'No Handphone', 'trim|required|numeric|is_unique[customers.phone]', [
                'is_unique' => 'No Handphone sudah digunakan!'
            ]);
        }
        $this->form_validation->set_rules('code_verification', 'Kode Verifikasi', 'trim|required');
        $message = [];
        if ($this->form_validation->run() == TRUE) {
            $data = $this->input->post(null, true);
            if($customer_not_phone) {
                if($this->main->get('customers', ['id' => $id_customer, 'verification_code' => $data['code_verification']])) {
                    $update = $this->main->update('customers', ['phone' => $data['phone'], 'verification_phone' => 1, 'verification_code' => ''], ['id' => $id_customer]);
                    if($update) {
                        $return['status'] = 'success';
                        $return['message'] = 'No Handphone berhasil ditambah!';
                    } else {
                        $return['status'] = 'error_update';
                        $return['message'] = $this->set_message_flashdata('danger', 'Terjadi suatu kesalahan!');
                    }
                } else {
                    array_push($message, [
                        'type' => 'code_verification',
                        'message' => 'Kode verifikasi salah!'
                    ]);
                    $return['status'] = 'error';
                    $return['message'] = $message;
                }
            } else {
                if($this->main->get('customers', ['id' => $id_customer, 'verification_code' => $data['code_verification']])) {
                    $this->main->update('customers', ['verification_phone' => 1, 'verification_code' => ''], ['id' => $id_customer]);
                    $return['status'] = 'success';
                    $return['message'] = 'No Handphone berhasil diverifikasi!';
                } else {
                    array_push($message, [
                        'type' => 'code_verification',
                        'message' => 'Kode verifikasi salah!'
                    ]);
                    $return['status'] = 'error';
                    $return['message'] = $message;
                }
            }
        } else {
            if(form_error('phone')) {
                array_push($message, [
                    'message' => form_error('phone'),
                    'type' => 'phone'
                ]);
            }
            if(form_error('code_verification')) {
                array_push($message, [
                    'message' => form_error('code_verification'),
                    'type' => 'code_verification'
                ]);
            }
            $return['status'] = 'error';
            $return['message'] = $message;
        }
        echo json_encode($return);
    }

    public function change_picture() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('image_lib');
        $customer = $this->data['user']->id;
        $file_name = $_FILES['file']['name'];
        $file_name = str_replace(' ', '_', $file_name);
        $time = date('Y_m_d_H_i_s');
        $ext_jpg = strstr($file_name, 'jpg');
        $ext_jpeg = strstr($file_name, 'jpeg');
        $ext_png = strstr($file_name, 'png');
        if($ext_png || $ext_jpeg || $ext_jpg) {
            $config['upload_path']          = './files/images/customers_image/';
            $config['allowed_types']        = 'jpeg|jpg|png';
            $config['max_size']             = 4000;
            $config['file_name'] = $file_name;

            $this->load->library('upload', $config);
            if($this->upload->do_upload('file')) {
                $data_image = $this->upload->data();
                $this->compressImage($data_image['full_path'], $data_image['full_path'], 20);
                $check_image = $this->main->get('customers', ['id' => $customer]);
                if($check_image->image) {
                    unlink('files/images/customers_image/' . $check_image->image);
                    $this->main->update('customers', ['image' => $data_image['file_name']], ['id' => $customer]);
                    $return['status'] = 'success';
                    $return['message'] = 'Foto profil berhasil diubah!';
                } else {
                    $this->main->update('customers', ['image' => $data_image['file_name']], ['id' => $customer]);
                    $return['status'] = 'success';
                    $return['message'] = 'Foto profil berhasil diubah!';
                }
            } else {
                $return['status'] = 'error';
                $return['message'] = $this->upload->display_errors('', '');
            }
        } else {
            $return['status'] = 'error';
            $return['message'] = 'Jenis file tidak didukung!';
        }
        echo json_encode($return);
    }

    private function compressImage($source, $destination, $quality) {

        $info = getimagesize($source);

        if ($info['mime'] == 'image/jpeg') {
            $image = imagecreatefromjpeg($source);
            imagejpeg($image, $destination, $quality);
        } 

    }

    public function do_upload() {
        // $this->input->is_ajax_request() or exit(redirect('error_403'));
        $config['upload_path']          = './files/images/confirm_payment/';
        $config['allowed_types']        = 'jpg|png|jpeg|JPG|PNG|JPEG';
        $config['max_size']             = 300;
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;
        $data = $this->input->post(null, true);
        $path = $_FILES['image']['name'];
        $tgl = date('Ymd_his');
        $config['file_name'] = str_replace('/', '-', $data['code_order']).'_'.$tgl;
        $order = $this->main->get('orders', array('id' => decode_id($data['id']), 'code' => $data['code_order']));
        $image_name = $config['file_name'].".".pathinfo($path, PATHINFO_EXTENSION);

        if ($order) {
            unset($data['id']);
            unset($data['code_order']);
            $payment_to = json_decode($order->payment_to, true);
            $data['to'] = $payment_to[$data['to']];
            $this->load->library('upload', $config);

            if ( ! $this->upload->do_upload('image'))
            {
                $error = array('error' => $this->upload->display_errors());
                $this->data['title'] = '404';
                $return = array('status' => 'failed', 'message' => $error, 'id' => '');
                $this->load->view('error', $this->data);
            }
            else
            {   
                $datas = array('upload_data' => $this->upload->data());
                $this->main->update('orders', array('payment_to' => json_encode($data), 'payment_status' => 'Confirmed', 'payment_date' => date('Y-m-d'), 'confirm_image' => $image_name), array('id' => $order->id));
                $this->main->update('order_invoice', array('order_status' => settings('order_payment_confirmed_status')), array('order' => $order->id));
                
                $message = $this->load->view('email/confirm_order', $order, TRUE);
                // $cronjob = array(
                //     'from' => settings('send_email_from'),
                //     'from_name' => settings('store_name'),
                //     'to' => settings('admin_email'),
                //     'subject' => 'Konfirmasi Pesanan ' . $order->code,
                //     'message' => $message
                // );
                // $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));

                $email_data = array(
                    'from' => settings('send_email_from'),
                    'from_name' => settings('store_name'),
                    'to' => settings('admin_email'),
                    'subject' => 'Konfirmasi Pesanan ' . $order->code,
                    'message' => $message
                );
                send_mail($email_data['from'], $email_data['from_name'], $email_data['to'], $email_data['subject'], $email_data['message']);
    
                $return = array('status' => 'success', 'message' => '<div class="alert alert-success">Konfirmasi pembayaran telah kami terima. Pembayaran akan segera kami verifikasi.</div>', 'id' => encode($order->id));
                redirect('member/order_detail/'. encode($order->id));
            }
        } else {
            $return = array('status' => 'failed', 'message' => '<div class="alert alert-danger">Tidak dapat menemukan pesanan. Konfirmasi pemesanan gagal</div>', 'id' => '');
            $this->data['title'] = '404';
            $this->load->view('error', $this->data);
        }
    }

}
