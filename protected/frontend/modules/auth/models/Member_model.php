<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Member_model extends CI_Model {

    function getOrders($customer, $limit = 10, $start = 0) {
        $this->db->select("o.*, sd.name shipping_district_name, sc.name shipping_city_name, sp.name shipping_province_name", FALSE)
                ->join('cities sc', 'sc.id = o.shipping_city', 'left')
                ->join('districts sd', 'sd.id = o.shipping_district', 'left')
                ->join('provincies sp', 'sp.id = o.shipping_province', 'left')
                ->order_by('o.id DESC')
                ->where('o.customer', $customer);
        // $this->db->select("o.*, oi.merchant, m.name merchant_name, sd.name shipping_distric_name, sc.name shipping_city_name, sp.name shipping_province_name", FALSE)
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        $query = $this->db->get('orders o');
        return $query;
    }

    function get_order($id) {
        $this->db->select("o.*, sd.name shipping_district_name, sc.name shipping_city_name, sp.name shipping_province_name", FALSE)
                ->join('cities sc', 'sc.id = o.shipping_city', 'left')
                ->join('districts sd', 'sd.id = o.shipping_district', 'left')
                ->join('provincies sp', 'sp.id = o.shipping_province', 'left')
                ->order_by('o.id DESC')
                ->where('o.id', $id);
        $query = $this->db->get('orders o');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_order_invoice($order) {
        $this->db->select('oi.*, m.name merchant_name, os.name order_status_name')
                ->join('merchants m', 'm.id = oi.merchant', 'left')
                ->join('setting_order_status os', 'os.id = oi.order_status', 'left')
                ->where('order', $order);
        return $this->db->get('order_invoice oi');
    }

    function get_order_products($invoice) {
        $this->db->select('op.*, pi.image')
                ->join('product_image pi', 'pi.product = op.product AND pi.primary = 1', 'left')
                ->where('invoice', $invoice);
        return $this->db->get('order_product op');
    }

    function getOrderProducts($order) {
        $this->db->select('op.*, pi.image')
                ->join('product_image pi', 'pi.product = op.product AND pi.primary = 1', 'left');
        $query = $this->db->get_where('order_product op', array('op.order' => $order));
        return ($query->num_rows() > 0) ? $query : false;
    }

    function get_order_histories($invoice) {
        $this->db->select('oh.*, os.name, os.description, os.color')
                ->join('setting_order_status os', 'os.id = oh.order_status', 'left')
                ->order_by('oh.id DESC');
        $query = $this->db->get_where('order_history oh', array('oh.invoice' => $invoice));
        return ($query->num_rows() > 0) ? $query : false;
    }

    function getTotalQuantityOrderProduct($order) {
        $this->db->select('SUM(quantity) total');
        $query = $this->db->get_where('order_product', array('order' => $order));
        return ($query->row()->total > 0) ? $query->row()->total : 0;
    }

    function getProfile($customer) {
        $this->db->select('c.address, c.name, c.phone, a.address, a.province, a.city, a.district, a.postcode, au.email')
                ->join('address a', 'c.address = a.id', 'left')
                ->join('auth_users au', 'c.auth = au.id', 'left');
        $query = $this->db->get_where('customers c', array('c.id' => $customer));
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_total_shipping_cost($order) {
        $this->db->select('SUM(shipping_cost) shipping_cost')
                ->where('order', $order);
        return $this->db->get('order_invoice')->row()->shipping_cost;
    }

    function get_subtotal($order) {
        $this->db->select('SUM(subtotal) subtotal')
                ->where('order', $order);
        return $this->db->get('order_invoice')->row()->subtotal;
    }

    function addresses($customer) {
        $this->db->select('ca.*, p.name province_name, c.name city_name, d.name district_name')
                ->join('provincies p', 'p.id = ca.province')
                ->join('cities c', 'c.id = ca.city')
                ->join('districts d', 'd.id = ca.district')
                ->where('ca.customer', $customer);
        return $this->db->get('customer_address ca');
    }

    function get_orders_review($customer, $limit = 10, $start = 0) {
        $status = settings('order_finish_status');
        $this->db->select("o.id, o.code, o.date_added, GROUP_CONCAT(oi.id SEPARATOR ',') invoice")
                ->join('order_invoice oi', 'oi.order = o.id', 'left')
                ->where('oi.order_status', $status)
                ->where('o.customer', $customer)
                ->group_by('o.id')
                ->order_by('o.id', 'desc');
        if ($limit) {
            $this->db->limit($limit, $start);
        }
        return $this->db->get('orders o');
    }

    function get_products_review($order, $customer, $invoice = array()) {
        $status = settings('order_finish_status');
        $this->db->select('op.id, op.product, op.name, op.code, oi.merchant, pi.image, pr.rating_speed, pr.rating_service, pr.rating_accuracy, pr.description')
                ->join('product_image pi', 'pi.product = op.product AND pi.primary = 1', 'left')
                ->join('product_review pr', 'pr.order_id = ' . $order . ' AND pr.product = op.product AND pr.customer = ' . $customer, 'left')
                ->join('order_invoice oi', 'op.invoice = oi.id', 'LEFT')
                ->where('op.order', $order)
                ->where_in('op.invoice', $invoice)
                ->order_by('op.id', 'desc');
        return $this->db->get('order_product op');
    }

    function merchant($id) {
        $this->db->select('m.*, p.name province, c.name city, d.name district')
                ->join('provincies p', 'p.id = m.province', 'left')
                ->join('cities c', 'c.id = m.city', 'left')
                ->join('districts d', 'd.id = m.district', 'left')
                ->where('m.id', $id);
        return $this->db->get('merchants m')->row();
    }

    function get_invoice_for_notification($customer) {
        $status = [settings('order_process_status'),settings('order_shipped_status'),settings('order_ready_pickup_status'),settings('order_delivered_status'),settings('order_complete_pickup_status')];
        $this->db->select("GROUP_CONCAT(code,'-',oi.order ORDER BY date_modified DESC SEPARATOR ',') invoice, order_status status")
                ->where('oi.customer', $customer)
                ->where_in('oi.order_status', $status)
                ->group_by('oi.order_status');
        $query = $this->db->get('order_invoice oi');
        return ($query->num_rows() > 0) ? $query : false;
    }
    
    // function get_wishlist($customer, $merchant_group, $limit = 20, $start = 0){
    //     $this->db->select('wishlist.customer AS user_id,
    //                         products_principal_stock.branch_id AS store_id,
    //                         products_principal_stock.product_id,
    //                         products.name,
    //                         products.merchant,
    //                         products.price,
    //                         products.discount,
    //                         products.promo,
    //                         products.promo_data,
    //                         products.preorder,
    //                         product_image.image,
    //                         merchants.name AS store_name,
    //                         wishlist.id AS wishlist')
    //             ->join('products_principal_stock', 'wishlist.product = products_principal_stock.product_id AND wishlist.branch_id = products_principal_stock.branch_id', 'LEFT')
    //             ->join('merchants', 'wishlist.branch_id = merchants.id AND products_principal_stock.branch_id = merchants.id', 'LEFT')
    //             ->join('products', 'products_principal_stock.product_id = products.id', 'LEFT')
    //             ->join('product_image', 'product_image.product = products.id AND product_image.primary = 1', 'LEFT')
    //             ->where('products.status != ', 0)
    //             ->where('wishlist.customer', $customer)
    //             ->order_by('wishlist.date_added', 'DESC');
        
    //     if ($limit) {
    //         $this->db->limit($limit, $start);
    //     }

    //     $query = $this->db->get('wishlist');

    //     return ($query->num_rows() > 0) ? $query : FALSE;
    //     // $this->db->select('p.id, p.name, IFNULL(pp.price, p.price) price, pi.image, p.date_added, w.id wishlist')
    //     //         ->join('products p','p.id = w.product','left')
    //     //         ->join('product_price pp','pp.product = p.id AND pp.merchant_group = '. $merchant_group,'left')
    //     //         ->join('product_image pi','pi.product = p.id AND pi.primary = 1','left')
    //     //         ->where('w.customer', $customer)
    //     //         ->where('p.status', 1)
    //     //         ->order_by('w.date_added desc')
    //     //         ->limit($limit, $start);
    //     // return $this->db->get('wishlist w');
    // }

    function get_wishlist($customer) {
        $query = "(SELECT wishlist.customer AS user_id,
                                products_principal_stock.branch_id AS store_id,
                                products_principal_stock.product_id,
                                products.name,
                                products.merchant,
                                products.price,
                                products.discount,
                                products.promo,
                                products.promo_data,
                                products.preorder,
                                product_image.image,
                                merchants.name AS store_name,
                                wishlist.id AS wishlist,
                                wishlist.date_added AS date_added,
                                products_principal_stock.quantity AS quantity,
                                products.viewed AS viewed,
                                products.free_ongkir AS free_ongkir
                                FROM wishlist
                                LEFT JOIN products ON products.id = wishlist.product
                                LEFT JOIN products_principal_stock ON products_principal_stock.product_id = wishlist.product
                                LEFT JOIN merchants ON merchants.id = wishlist.branch_id
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                WHERE products.store_type = 'principal'
                                AND wishlist.customer = $customer
                                AND products.status != 0)

                        UNION
                        
                        (SELECT wishlist.customer AS user_id,
                                                merchants.id AS store_id,
                                                products.id AS product_id,
                                                products.name,
                                                products.merchant,
                                                products.price,
                                                products.discount,
                                                products.promo,
                                                products.promo_data,
                                                products.preorder,
                                                product_image.image,
                                                merchants.name AS store_name,
                                                wishlist.id AS wishlist,
                                                wishlist.date_added AS date_added,
                                                products.quantity AS quantity,
                                                products.viewed AS viewed,
                                                products.free_ongkir AS free_ongkir
                                                FROM wishlist
                                                LEFT JOIN products ON products.id = wishlist.product
                                                LEFT JOIN merchants ON merchants.id = wishlist.branch_id
                                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                                WHERE products.store_type = 'merchant'
                                                AND wishlist.customer = $customer
                                                AND products.status != 0)
                        ORDER BY date_added DESC";
        return $this->db->query($query);
    }

    function get_all_last_seen_product ($user_id = NULL, $ip_address = NULL, $limit = NULL, $stock_id = NULL) {
        if ($user_id) {
            $this->db->where('product_last_review.user_id', $user_id);
        }
        else {
            $this->db->where('product_last_review.ip_address', $ip_address);
        }

        $this->db->select('product_last_review.id,
                            product_last_review.user_id,
                            product_last_review.ip_address,
                            product_last_review.modified_time,
                            products_principal_stock.id AS stock_id,
                            products_principal_stock.branch_id AS store_id,
                            products_principal_stock.product_id AS product_id,
                            merchants.name AS merchant_name,
                            products.name,
                            products.price,
                            products.promo,
                            products.promo_data,
                            product_image.image')
                ->join('products_principal_stock', 'product_last_review.product_id = products_principal_stock.id', 'LEFT')
                ->join('products', 'products_principal_stock.product_id = products.id', 'LEFT')
                ->join('merchants', 'products_principal_stock.branch_id = merchants.id', 'LEFT')
                ->join('product_image', 'product_image.product = products.id AND product_image.primary = 1', 'LEFT')
                ->where('products.status != ', 0);

        if ($stock_id) {
            $this->db->where('product_last_review.product_id != ', $stock_id);
        }

        if ($limit) {
            $this->db->limit($limit);
        }
        
        $this->db->order_by('product_last_review.modified_time', 'DESC');

        $query = $this->db->get('product_last_review');

        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function cek_address_pin($id) {
        $this->db->select('c.*')
                ->where('c.customer', $id);
        return $this->db->get('customer_address c');
    }

    function cek_postcode($id) {
        $this->db->select('c.*')
                ->where('c.user_id', $id);
        return $this->db->get('postcode_temp c');
    }

    /*function get_lnglat($postcode) {
        $this->db->select('p.latitude, p.longitude')
                ->where('p.postcode', $postcode);
        return $this->db->get('post_code p');
    }*/

    function get_lnglat($postcode){
        return $this->db->query("SELECT CONCAT(latitude, ', ', longitude) as area FROM post_code WHERE postcode='$postcode'");
    }

    function cek_review($order, $customer, $product){
        return $this->db->query("SELECT * FROM product_review WHERE order_id='$order' AND customer='$customer' AND product='$product' ");
    }

    function get_rating ($product, $store) {
        $this->db->select('IFNULL((SUM(rating_speed) + SUM(rating_service) + SUM(rating_accuracy)) / (COUNT(id) * 3), 0) AS rating,
                            IFNULL(COUNT(id), 0) AS review')
                ->where('product', $product)
                ->where('merchant', $store)
                ->where('status', 1);

        $query = $this->db->get('product_review');
        return ($query->result() > 0) ? $query->row() : FALSE;
    }

    function get_bank_account($id) {
        $this->db->select('cb.*, rb.name AS bank_name')
                ->join('rekening_bank rb', 'rb.code = cb.bank_code', 'left')
                ->where('cb.customer', $id)->distinct()->order_by('primary', 'desc');
        return $this->db->get('customer_bank cb');
    }

}
