<!-- <section class="collection-heading heading-content ">
    <div class="container">
        <div class="row">
            <div class="collection-wrapper">
                <h1 class="collection-title"><span>Login</span></h1>
                <div class="breadcrumb-group">
                    <div class="breadcrumb clearfix">
                        <?php //echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> -->
<style>
    input{border-radius:3px}input:focus{border-color:#97c23c}input[type=submit]{border-radius:3px;background:#97c23c;border:1px solid #97c23c;transition:.3s}input[type=submit]:hover{background:#85a839;border:1px solid #85a839;color:#fff}#login-email a{transition:.3s}#login-email a:hover{color:#97c23c}#login_form input,#login_form select{background-image:none;background-color:transparent;-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;border:1px solid #ccc;border-radius:3px}#login_form input:focus,#login_form select:focus{border-color:#a7c22a;background-color:none!important;background-image:none}#login_form .text_register{margin-top:10px;margin-bottom:10px}#login_form .text_other{margin-top:10px}#login_form a{color:#a7c22a}#login_form a:hover{color:#e75a5f}#login_form .action{margin-bottom:-20px}#login_form .action button{width:100%;margin:10px 0}#login_form .action button:focus,#login_form .action button:focus i{outline:0;color:#fff}#login_form .action button.btn_login{border-radius:3px;background:#a7c22a;border:1px solid #a7c22a;text-transform:capitalize;font-weight:700;font-size:14px}#login_form .action button.btn_login:hover{color:#fff!important;background:#8ea623;border:1px solid #8ea623}#login_form .action button.btn_register_facebook{background:#3b5998;border:1px solid #3b5998;border-radius:3px;text-transform:capitalize;font-weight:700;font-size:14px}#login_form .action button.btn_register_facebook:hover{background:#2c4170!important;border:1px solid #2c4170!important;color:#fff}#login_form .action button.btn_register_facebook:hover i{color:#fff!important}#login_form .action button.btn_register_google{color:#444;background:#ccc;border:1px solid #ccc;border-radius:3px;text-transform:capitalize;font-weight:700;font-size:14px}#login_form .action button.btn_register_google:hover{background:#b3b3b3;border:1px solid #b3b3b3}#login_form .action button.btn_register_google i{color:#444}#login_form .action .col-md-3{padding-left:0}#login_form button#button_login_verification{height:34px;width:100%;border:1px solid #ccc;background:#ccc;border-radius:3px;font-weight:700;font-size:12px;color:#444}#login_form button#button_login_verification:focus{outline:0}#login_form button#button_login_verification:hover{background:#b3b3b3;border:1px solid #b3b3b3}#login_form .error_validation_login .alert,#login_form .send_otp_success .alert{border-radius:3px}#login_form input:active,#login_form input:focus,#login_form select:active,#login_form select:focus{border:1px solid #a7c22a!important}#login_form .error_validation span{color:#d9534f;margin-top:-15px}.lds-ring{display:inline-block;position:relative;width:100%;height:100%;padding:9px 2px}.lds-ring div{box-sizing:border-box;display:inline-block;position:absolute;width:20px;height:20px;border:3px solid #000;border-radius:50%;animation:lds-ring 1.2s cubic-bezier(.5,0,.5,1) infinite;border-color:#000 transparent transparent transparent}.lds-ring div:nth-child(1){animation-delay:-.45s}.lds-ring div:nth-child(2){animation-delay:-.3s}.lds-ring div:nth-child(3){animation-delay:-.15s}@keyframes lds-ring{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}#login_form .button_media_social div{display:inline-block;width:49%;padding:0!important}#login_form .action{margin-bottom:25px}#login_form .button_media_social div:last-child{margin-left:1%}#login_form .button_media_social{padding-right:15px;padding-left:15px}@media(max-width:326px){#login_form .button_media_social div:last-child{margin-left:0}}#form_login_merchant input,#form_login_merchant select{background-image:none;background-color:transparent;-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;border:1px solid #ccc;border-radius:3px}#form_login_merchant input:focus,#form_login_merchant select:focus{border-color:#a7c22a;background-color:none!important;background-image:none}#form_login_merchant .text_register{margin-top:10px;margin-bottom:10px}#form_login_merchant .text_other{margin-top:10px}#form_login_merchant a{color:#a7c22a}#form_login_merchant a:hover{color:#e75a5f}#form_login_merchant .action{margin-bottom:-20px}#form_login_merchant .action button{width:100%;margin:10px 0}#form_login_merchant .action button:focus,#form_login_merchant .action button:focus i{outline:0;color:#fff}#form_login_merchant .action button.btn_login{border-radius:3px;background:#a7c22a;border:1px solid #a7c22a;text-transform:capitalize;font-weight:700;font-size:14px}#form_login_merchant .action button.btn_login:hover{color:#fff!important;background:#8ea623;border:1px solid #8ea623}#form_login_merchant .action button.btn_register_facebook{background:#3b5998;border:1px solid #3b5998;border-radius:3px;text-transform:capitalize;font-weight:700;font-size:14px}#form_login_merchant .action button.btn_register_facebook:hover{background:#2c4170!important;border:1px solid #2c4170!important;color:#fff}#form_login_merchant .action button.btn_register_facebook:hover i{color:#fff!important}#form_login_merchant .action button.btn_register_google{color:#444;background:#ccc;border:1px solid #ccc;border-radius:3px;text-transform:capitalize;font-weight:700;font-size:14px}#form_login_merchant .action button.btn_register_google:hover{background:#b3b3b3;border:1px solid #b3b3b3}#form_login_merchant .action button.btn_register_google i{color:#444}#form_login_merchant .action .col-md-3{padding-left:0}#form_login_merchant button#button_login_verification{height:34px;width:100%;border:1px solid #ccc;background:#ccc;border-radius:3px;font-weight:700;font-size:12px;color:#444}#form_login_merchant button#button_login_verification:focus{outline:0}#form_login_merchant button#button_login_verification:hover{background:#b3b3b3;border:1px solid #b3b3b3}#form_login_merchant .error_validation_login .alert,#form_login_merchant .send_otp_success .alert{border-radius:3px}#form_login_merchant input:active,#form_login_merchant input:focus,#form_login_merchant select:active,#form_login_merchant select:focus{border:1px solid #a7c22a!important}#form_login_merchant .error_validation span{color:#d9534f;margin-top:-15px}#form_login_merchant .button_media_social div{display:inline-block;width:49%;padding:0!important}#form_login_merchant .action{margin-bottom:25px}#form_login_merchant .button_media_social div:last-child{margin-left:1%}#form_login_merchant .button_media_social{padding-right:15px;padding-left:15px}@media(max-width:326px){#form_login_merchant .button_media_social div:last-child{margin-left:0}}#form_login_merchant .alert{border-radius: 5px;}
</style>
<section class="login-content" id="login">
    <div class="login-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="panel">
                    <div class="tabbable">
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#login-email" data-toggle="tab">Masuk</a></li>
                        </ul>
                        <form action="<?php echo site_url('auth/new_login') ?>" style="margin-top: 25px;" id="login_form">
                            <div class="row">
                                <div class="">
                                    <div class="row">
                                        <div class="error_validation_login col-md-12">
                                        </div>
                                        <div class="send_otp_success col-md-12">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <label for="identity_login">No Handphone / Email</label>
                                            <input type="text" class="form-control" name="identity_login" id="identity_login" placeholder="No Handphone / Email">
                                            <div id="error_identity_login" class="error_validation"></div>  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <label for="password_login">Kata Sandi</label>
                                            <input type="password" class="form-control" id="password_login" name="password_login" placeholder="Kata Sandi">
                                            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"
                                            style="float: right;
                                            margin-top:-43px;
                                            margin-right:10px;
                                            position: relative;
                                            z-index: 2;
                                            cursor: pointer;" onclick="see_password(this)"></span>
                                            <div id="error_password_login" class="error_validation"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="text_other">
                                                <a href="<?php echo site_url('member/forgot_password') ?>">Lupa Kata Sandi?</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="text_register">
                                                Belum punya akun? <a href="<?php echo site_url('auth/register') ?>">Daftar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="back" value="<?php echo site_url('') ?>">
                            <div class="row action">
                                <div class="col-sm-12">
                                    <button class="btn btn_login" type="submit">Masuk</button>
                                </div>
                                <div class="row button_media_social">
                                    <div class="col-sm-12">
                                        <button class="btn btn_register_facebook" type="button"><i class="fa fa-facebook" aria-hidden="true"></i> &nbsp;Facebook</button>
                                    </div>
                                    <div class="col-sm-12">
                                        <button class="btn btn_register_google" type="button"><i class="fa fa-google" aria-hidden="true"></i> &nbsp;Google</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <hr>
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active"><a href="#login-email" data-toggle="tab">Masuk sebagai merchant</a></li>
                        </ul>
                        <form action="<?php echo site_url('auth/new_login_merchant') ?>" style="margin-top: 25px;" id="form_login_merchant">
                            <div class="row">
                                <div class="">
                                    <div class="row">
                                        <div class="error_validation_login_merchant col-md-12">
                                        </div>
                                        <div class="send_otp_success col-md-12">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <label for="identity_login_merchant">No Handphone / Email</label>
                                            <input type="text" class="form-control" name="identity_login_merchant" id="identity_login_merchant" placeholder="No Handphone / Email">
                                            <div id="error_identity_login_merchant" class="error_validation"></div>  
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 form-group">
                                            <label for="password_login_merchant">Kata Sandi</label>
                                            <input type="password" class="form-control" id="password_login_merchant" name="password_login_merchant" placeholder="Kata Sandi">
                                            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"
                                            style="float: right;
                                            margin-top:-43px;
                                            margin-right:10px;
                                            position: relative;
                                            z-index: 2;
                                            cursor: pointer;" onclick="password_field_login_merchant(this)"></span>
                                            <div id="error_password_login_merchant" class="error_validation"></div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="text_other">
                                                <a href="<?php echo site_url('ma/auth/forgot_password') ?>">Lupa Kata Sandi?</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="text_register">
                                                Belum punya akun? <a href="<?php echo site_url('ma/register') ?>">Daftar Merchant</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="back_merchant" value="<?php echo site_url('') ?>">
                            <div class="row action">
                                <div class="col-sm-12">
                                    <button class="btn btn_login" type="submit">Masuk Sebagai Merchant</button>
                                </div>
                                <div class="row button_media_social">
                                    <div class="col-sm-12">
                                        <button class="btn btn_register_facebook" type="button"><i class="fa fa-facebook" aria-hidden="true"></i> &nbsp;Facebook</button>
                                    </div>
                                    <div class="col-sm-12">
                                        <button class="btn btn_register_google" type="button"><i class="fa fa-google" aria-hidden="true"></i> &nbsp;Google</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</section>