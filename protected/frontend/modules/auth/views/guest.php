<section class="collection-heading heading-content ">
    <div class="container">
        <div class="row">
            <div class="collection-wrapper">
                <h1 class="collection-title"><span>Login sebagai tamu</span></h1>
                <div class="breadcrumb-group">
                    <div class="breadcrumb clearfix">
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="login-content">
    <div class="login-content-wrapper">
        <div class="container">
            <div class="row">
                <div class="panel">
                    <div class="panel-body">
                        <div class="login-content-inner">
                            <?php if ($message) { ?>
                                <div class="alert alert-danger fade in alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                    <?php echo $message; ?>
                                </div>
                            <?php } ?>
                            <form id="customer_login1" method="post" accept-charset="UTF-8">
                                <label class="label">Nama Lengkap</label>
                                <input type="text" class="text" name="name" value="<?php echo set_value('name'); ?>">
                                <label class="label">Email</label>
                                <input type="email" class="text" name="email" value="<?php echo set_value('email'); ?>">
                                <label class="label">No. Handphone</label>
                                <input type="text" class="text" name="phone" value="<?php echo set_value('phone'); ?>">
                                <label class="label">Password</label>
                                <input type="password" class="text" name="password" value="<?php echo set_value('password'); ?>">
                                <label class="label">Alamat</label>
                                <input type="text" class="text" name="address" value="<?php echo set_value('address'); ?>">
                                <label class="label">Provinsi</label>
                                <select name="province" id="province" class="form-control" style="margin-bottom:10px">
                                    <option value="">Pilih provinsi</option>
                                    <?php if ($provincies) { ?>
                                        <?php foreach ($provincies->result() as $province) { ?>
                                            <option value="<?php echo $province->id; ?>"><?php echo $province->name; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                                <label class="label">Kota</label>
                                <select name="city" id="city" class="form-control" style="margin-bottom:10px">
                                    <option value="">Pilih kota</option>
                                </select>
                                <label class="label">Kecamatan</label>
                                <select name="district" id="district" class="form-control" style="margin-bottom:10px">
                                    <option value="">Pilih kecamatan</option>
                                </select>
                                <label class="label">Kode Pos</label>
                                <input name="postcode" type="text" class="form-control" onkeypress="return isNumberKey(event)" style="margin-bottom:10px">
                                <div class="action_bottom">
                                    <input class="btn" type="submit" value="Login as guest">
                                </div>
                                <p>Sudah jadi member? <a href="<?php echo site_url('member/login?back=' . $this->input->get('back')); ?>" class="color">Login</a></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>