<style>
.product-item{border:none}.product{margin-left:7px;margin-right:7px;margin-bottom:15px!important;border:1px solid #e8e8e8;box-shadow:0 0 7px -3px rgba(0,0,0,.3);min-height:355px;border-radius:5px}#collection .collection-mainarea .collection-items .product-item{border:none}.product-footer{margin-top:0}.garis-tengah{width:100%;height:1px;background-color:#e8e8e8;display:block;margin-top:10px}.tombol-cart{z-index:1;background-color:#fff;position:absolute;top:48%;right:5%;padding:0;display:inline-block;border:1px solid rgba(167,194,42,.3);height:40px;border-radius:50%;width:40px;box-shadow:0 0 7px 1px rgba(167,194,42,.3);-webkit-transition:all .5s;-moz-transition:all .5s;-ms-transition:all .5s;-o-transition:all .5s;transition:all .5s}.tombol-cart button{background:0 0;border:none;line-height:0;padding:0 7px;outline:0}.tombol-cart button-cart:active,.tombol-cart button-cart:hover,.tombol-cart button:active,.tombol-cart button:hover{border:none;background:0 0;outline:0}.tombol-cart a{padding:12px 7px;line-height:37px}.tombol-cart:hover{background-color:#a7c22a;box-shadow:0 0 7px 1px rgba(167,194,42,.3)}.tombol-cart:hover .icon-cart i{color:#fff}.pre-order-label{color:#666;bottom:0;display:inline-block;margin-top:3px;position:relative;z-index:1;padding:2px 10px;background-color:#eee;font-weight:700;border-radius:2px}@media (max-width:320px){.pre-order-label{position:absolute!important;margin-bottom:10px;bottom:0}.product{min-height:355px!important}.tombol-cart{top:48%}.hoverBorder{width:70%!important}.img-responsive{margin-left:23%}}@media (max-width:375px){.pre-order-label{position:relative!important;margin-top:5px}}@media (max-width:425px) and (min-width:376px){.tombol-cart{top:48%}}@media (max-width:375px) and (min-width:321px){.tombol-cart{top:48%}.product{min-height:357px!important}}@media (max-width:767px) and (min-width:426px){.tombol-cart{top:48%}}@media (max-width:767px){.pre-order-label{position:relative!important;margin-bottom:5px!important}.product{min-height:360px}}@media (max-width:1024px) and (min-width:768px){.tombol-cart{top:48%}}@media (min-width:1024px){.product{min-height:355px}}@media (max-width:1199px) and (min-width:992px){#collection .products .product-item{width:25%!important}}@media (max-width:991px) and (min-width:585px){#collection .products .product-item{width:33.333333%!important}}.free-ongkir-label{color:#fff;bottom:0;display:inline-block;margin-top:3px;margin-left:1px;margin-right:1px;position:relative;z-index:1;padding:2px 5px;background-color:#a7c22a;font-weight:700;border-radius:2px}.btn-add-to-wishlist{position:absolute;z-index:1;top:6px;left:10px;font-size:18px;transition:.5s}.btn-add-to-wishlist:hover{color:#d9534f!important}.money{font-weight:600}.product-discount-label{position:absolute;top:8px;right:10px;z-index:1;background-color:#d9534f;color:#fff;padding:5px 10px;border-radius:5px}@media (max-width:382px) and (min-width:321px){.pre-order-label{font-size:10px;padding:2px 5px}.free-ongkir-label{font-size:10px;padding:2px 3px}}
    .collection-title {
      font-size: 18px !important;
      color: #a7c22a !important;
      font-weight: bold !important;
    }
    .alert {
        border-radius: 5px;
        font-size: 15px;
    }
</style>
<div class="my-account" id="wishlist">
    <section class="collection-heading heading-content ">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-sm-push-3" style="padding-left: 0px; padding-right: 0px;">
                    <div class="collection-wrapper block" style="padding: 10px 15px;background: none;">
                        <h1 class="collection-title">
                            <span><?php echo lang('account_wishlist'); ?></span>
                        </h1>
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="collection-content">
        <div class="collection-wrapper">
            <div class="container" style="margin-bottom: 30px;">
                <div class="row">
                    <?php echo $this->load->view('navigation'); ?>
                    <div id="collection">
                        <div class="collection-mainarea col-md-9 col-sm-8 col-xs-12">
                            <?php if ($products) { ?>
                                <?php if($products->num_rows() > 0) : ?>
                                    <div class="collection-items clearfix" style="border:none;">
                                        <div class="products" style="border:none;">
                                             <?php foreach ($products->result() as $key => $product) : ?>
                                                <?php
                                                if ($this->ion_auth->logged_in()) {
                                                    $id_customer = $this->data['user']->id;
                                                    $get_wishlist = $this->main->get('wishlist', ['product' => $product->product_id, 'branch_id' => $product->store_id, 'customer' => $id_customer]);
                                                    if ($get_wishlist) {
                                                        $product_wishlist = true;
                                                    } else {
                                                        $product_wishlist = false;
                                                    }
                                                } else {
                                                    $product_wishlist = false;
                                                }

                                                $sale = false;

                                                if ($product->store_id == 0) {
                                                    $merchant = $this->session->userdata('list_merchant')[0];
                                                    $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
                                                    if ($price_group) {
                                                        $product->price = $price_group->price;
                                                    }
                                                }

                                                if ($product->promo == 1) {
                                                    $promo_data = json_decode($product->promo_data, true);

                                                    if ($promo_data['type'] == 'P') {
                                                        $disc_price = round(($product->price * $promo_data['discount']) / 100);
                                                        $label_disc = $promo_data['discount'] . ' % off';
                                                    } else {
                                                        $disc_price = $promo_data['discount'];
                                                        $label_disc = 'SALE';
                                                    }
                                                    $product->price = intval($product->price);
                                                    $disc_price = intval($disc_price);
                                                    $end_price = $product->price - $disc_price;
                                                    $today = date('Y-m-d');
                                                    $today = date('Y-m-d', strtotime($today));
                                                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                                    if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                                                        $sale = true;
                                                    } else {
                                                        $sale = false;
                                                    }
                                                }
                                                ?>
                                                <?php if ($product->store_id != null) : ?>
                                                    <?php if ($product->quantity > 0) : ?>
                                                        <div class="product-item col-sm-3 col-xs-6" style="padding: 0;">
                                                            <div class="row-container product list-unstyled clearfix" onmouseover="change_store(<?= $key ?>, 'over', 'best_selling')" onmouseout="change_store(<?= $key ?>, 'out', 'best_selling')">
                                                                <?php if ($sale) : ?>
                                                                    <span class="product-discount-label"><?php echo $label_disc; ?></span>
                                                                <?php endif; ?>

                                                                <?php if ($this->ion_auth->logged_in()) : ?>
                                                                    <a href="#" class="btn-add-to-wishlist" style="color:<?= ($product_wishlist) ? '#d9534f;' : '#BBB'; ?>" onclick="add_wishlist(this)" data-product="<?= encode($product->product_id) ?>" data-store="<?= encode($product->store_id) ?>" data-customer="<?= encode($id_customer) ?>">
                                                                        <i class="fa fa-heart" aria-hidden="true"></i>
                                                                    </a>
                                                                <?php else : ?>
                                                                    <a href="#" class="btn-add-to-wishlist" style="color:#BBB" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                                                        <i class="fa fa-heart" aria-hidden="true"></i>
                                                                    </a>
                                                                <?php endif; ?>

                                                                <div class="row-left">
                                                                    <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="hoverBorder container_item" style="min-height: 180px;">

                                                                        <img src="<?php echo base_url('assets/frontend/images/loading.gif') ?>" alt="<?= $product->name; ?>" class="img-responsive lazyload" data-src="<?php echo ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>">
                                                                    </a>
                                                                </div>

                                                                <div class="garis-tengah"></div>
                                                                <div class="tombol-cart">
                                                                    <?php if ($this->ion_auth->logged_in()) : ?>
                                                                        <?php if ($this->main->gets('product_option', array('product' => $product->product_id))) : ?>
                                                                            <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="icon-cart">
                                                                                <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                            </a>
                                                                        <?php else : ?>
                                                                            <button data-id="<?php echo encode($product->product_id); ?>" data-storeid="<?= encode($product->store_id) ?>" onclick="addtoCart($(this).data('id'), $(this).data('storeid'))" class="icon-cart">
                                                                                <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                            </button>
                                                                        <?php endif; ?>
                                                                    <?php else : ?>
                                                                        <button type="button" class="icon-cart" data-toggle="modal" data-target="#modal-guest">
                                                                            <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                                                        </button>
                                                                    <?php endif; ?>
                                                                </div>

                                                                <div class="row-right animMix">
                                                                    <div class="product-title">
                                                                        <a class="title-5" style="font-weight: bold; width: 75%;transition:.3s;" href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>">
                                                                            <?php echo $product->name; ?>
                                                                        </a>
                                                                    </div>

                                                                    <div class="product-price">
                                                                        <span class="price_sale">
                                                                            <?php if ($sale) : ?>
                                                                                <span class="money" style="text-decoration: line-through;font-size:9px;color:#8b8f8b"><?php echo rupiah($product->price); ?></span>
                                                                                <span class="money"><?php echo rupiah($end_price); ?></span>
                                                                            <?php else : ?>
                                                                                <span class="money"><?php echo rupiah($product->price); ?></span>
                                                                            <?php endif; ?>
                                                                        </span>
                                                                    </div>

                                                                    <?php
                                                                    $merchant = $this->main->get('merchants', ['id' => $product->store_id]);
                                                                    $city = $this->main->get('cities', ['id' => $merchant->city]);
                                                                    ?>
                                                                    <div style="height: 15px;overflow: hidden;">
                                                                        <span id="text-ellipsis-city-<?= $key ?>" style="display: block;"><?= ($city->type == 'Kabupaten') ? 'Kab. ' . $city->name : $city->name ?></span>
                                                                        <span id="text-ellipsis-city-<?= $key ?>" style="display: block;"><?= $merchant->name ?></span>
                                                                    </div>

                                                                    <div class="rating-star">
                                                                        <span class="spr-badge" data-rating="0.0">
                                                                            <span class="spr-starrating spr-badge-starrating">
                                                                                <?php
                                                                                $rating = $this->member->get_rating($product->product_id, $product->store_id);

                                                                                if (round($rating->rating) > 0) {
                                                                                    for ($i = 1; $i <= round($rating->rating); $i++) {
                                                                                ?>
                                                                                        <i class="spr-icon spr-icon-star"></i>
                                                                                    <?php
                                                                                    }
                                                                                    for ($i = round($rating->rating) + 1; $i <= 5; $i++) {
                                                                                    ?>
                                                                                        <i class="spr-icon spr-icon-star-empty"></i>
                                                                                <?php
                                                                                    }
                                                                                }
                                                                                ?>
                                                                            </span>
                                                                        </span>
                                                                    </div>

                                                                    <div class="product-footer">
                                                                        &nbsp;
                                                                        <i class="fa fa-eye" aria-hidden="true"></i> <?= $product->viewed; ?>
                                                                    </div>
                                                                    <?php
                                                                    $preorder = $product->preorder;
                                                                    $free_ongkir = $product->free_ongkir;

                                                                    if ($preorder == 1) {
                                                                        echo '<div class="pre-order-label">Preorder</div>';
                                                                    }
                                                                    if ($free_ongkir == 1) {
                                                                        echo '<div class="free-ongkir-label">Free Ongkir</div>';
                                                                    } ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>

                                    <div style="display: block; text-align: center;">
                                        <div class="pagi-bar">
                                            <?php echo $pagination; ?>
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <div class="<?php echo ($this->agent->is_mobile()) ? 'col-sm-12 col-md-12' : ''  ?>">
                                        <div class="alert alert-warning">
                                            <center><b>Belum ada produk di wishlist.</b></center>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <?php } else { ?>
                                <div class="<?php echo ($this->agent->is_mobile()) ? 'col-sm-12 col-md-12' : ''  ?>">
                                    <div class="alert alert-warning">
                                        <center><b>Belum ada produk di wishlist.</b></center>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script>
    function addtoCart(id, store_id) {
        $.ajax({
            url: site_url + 'catalog/products/add_to_cart_ajax_new',
            type: 'post',
            data: {
                product: id,
                store: store_id,
                qty: 1
            },
            beforeSend: function () {
                $("#tags-load").css('display', 'block');
            },
            complete: function () {
                $("#tags-load").css('display', 'none');
            },
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == 'error') {
                    // $('.product-price').before('<div class="alert alert-danger">' + data.message + '</div>');
                    Swal.fire({
                        type: 'error',
                        title: 'Terjadi Kesalahan',
                        text: data.message
                    });
                } else {
                    var length = Object.keys(data.items).length;
                    var total = 0;
                    var total_qty = 0;
                    // var html = '<div class="items control-container" style="max-height: 150px; overflow: auto">';
                    // if (length > 0) {
                    //     $('#cart-target .cart-info').attr('style', 'display:none');
                    //     var redir_url = "'" + site_url + "cart" + "'";
                    //     $.each(data.items, function (index, val) {
                    //         // console.log(val);
                    //         total += (Number(val.price * val.quantity));
                    //         total_qty += Number(val.quantity);
                    //         html += '<div class="row" style="padding: 10px 10px;"><div class="cart-left">';
                    //         html += '<a class="cart-image" href="catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                    //         html += '<img src="' + site_url + 'files/images/' + val.image + '" alt="" title=""></a></div>';
                    //         html += '<div class="cart-right">';
                    //         html += '<div class=""><a href="catalog/products/view/' + val.product_id + '/' + val.merchant_id + '" style="color:#000;">' + val.name + '</a></div>';
                    //         html += '<div class="cart-price"><span class="money">' + formatRupiah(val.price, '.') + '</span><span class="x"> x ' + val.quantity + '</span></div>';
                    //         html += '</div></div>';
                    //     });
                    //     html += '</div> <div class="subtotal" style="border-top: 1px solid #EBEBEB"><span>Subtotal:</span><span class="cart-total-right money">' + formatRupiah(total, '.') + '</span></div>';
                    //     html += '<div class="action"><a href="cart" class="btn" style="font-size:10px; width: 100%; onclick="window.location = ' + redir_url + ';">Lihat Semua</a></div>';
                    // } else {
                    //     html += '<div class="items control-container"><center><p style="font-size: 13px; font-weight: bold; padding: 15px;">Keranjang belanja Anda kosong. Yuk, belanja sekarang!</p></center></div>';
                    // }
                    // html += '</div>';
                    if(length > 0) {
                        var html = '<div style="max-height: 200px; overflow: auto">';
                        html += '<div class="carts-items">';
                        $.each(data.items, function(index, val) {
                            total += (Number(val.price * val.quantity));
                            total_qty += Number(val.quantity);
                            html += '<div class="row" id="cart-header-' + val.id_encrypt + '" style="padding: 10px 10px;border-bottom:1px solid #EEE;">';
                            html += '<div class="cart-left col-md-4" style="display: inline-block;">';
                            html += '<a class="cart-image" href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                            html += '<img style="width: 50px;" src="' + site_url + 'files/images/' + val.image + '" alt="" title="">';
                            html += '</a>';
                            html += '</div>';
                            html += '<div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">';
                            html += '<div class="">';
                            html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '" style="color:#000;">';
                            html += val.name;
                            html += '</a>';
                            html += '</div>';
                            html += '<div class="cart-price">';
                            html += '<span class="money" style="color: #97C23C;">' + formatRupiah(val.price, '.') + '</span>';
                            html += '<span class="x" style="color: #97C23C;">x ' + val.quantity + '</span>'
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        });
                        html += '</div>';
                        html += '</div>';
                        html += '<div class="subtotal" style="color: black;padding: 15px 20px;font-size:14px;border-top:1px solid #EEE;">';
                        html += '<span>Subtotal:</span><span class="cart-total-right money" style="float: right;">' + formatRupiah(total, '.') + '</span>'
                        html += '</div>';
                        html += '<div class="action" style="padding: 15px 20px;margin-top:-10px;">';
                        html += '<a href="' + site_url + 'cart" class="btn btn-show-all" style="font-size:10px; width: 100%;border-radius:3px;">Lihat Semua</a>'
                        html += '</div>';
                    } else {
                        var html = '<p style="font-size: 13px; font-weight: bold; padding: 15px;text-align:center;">Keranjang belanja Anda kosong. Yuk, belanja sekarang!</p>'
                    }
                    $('#cart-target .badge').html(total_qty);
                    $('#cart-button .badge').html(total_qty);
                    $('.icon-cart .cart_text .number').html(total_qty);
                    $('#cart-target-mobile .number').html(total_qty);
                    $('#cart-target .cart-info .cart-content').html(html);
                    $('#cart-target-mobile .cart-info .cart-content').html(html);
                    // new
                    $('.cart_icon #icon_cart .badge').html(total_qty);
                    $('.cart_icon .cart-dropdown').html(html);
                    Swal.fire({
                        type: 'success',
                        title: 'Produk Berhasil Ditambah!',
                        timer: 1000,
                        showConfirmButton: false,
                        customClass: 'swal-class'
                    });
                }
            }
        })
    }

    function formatRupiah(angka, prefix) {
        angka = angka.toString();
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
            split = number_string.split(','),
            sisa = split[0].length % 3,
            rupiah = split[0].substr(0, sisa),
            ribuan = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
    }
    $(function() {
        $('.btn-add-to-wishlist').on('click', (e) => {
            e.preventDefault();
        })
    })
    function add_wishlist(e) {
        let product = $(e).data('product');
        let store = $(e).data('store');
        let customer = $(e).data('customer');

        $.ajax({
            url: site_url + 'catalog/products/add_wishlist_new',
            data: {
                product: product,
                store: store,
                customer: customer
            },
            type: 'POST',
            success: function(data) {
                data = JSON.parse(data);
                if (data.status == 'add') {
                    $(e).attr('style', 'color:#d9534f;');
                    Swal.fire({
                        type: 'success',
                        title: 'Wishlist Berhasil Ditambah',
                        timer: 1000,
                        showConfirmButton: false
                    });
                    $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').html(data.total);
                    $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').attr('style', 'position: absolute;right:25px;');
                } else {
                    $(e).attr('style', 'color:#BBB;');
                    Swal.fire({
                        type: 'success',
                        title: 'Wishlist Berhasil Dihapus',
                        timer: 1000,
                        showConfirmButton: false
                    });
                    $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').html(data.total);
                    $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').attr('style', 'position: absolute;right:25px;');
                }
            }
        })
    }
    function change_store(id, type) {
        if (type == 'over') {
            $('#text-ellipsis-city-' + id).attr('style', 'margin-top:-18px;display:block;transition:.3s;');
        } else {
            $('#text-ellipsis-city-' + id).attr('style', 'margin-top:0px;display:block;transition:.3s;')
        }
    }
</script>