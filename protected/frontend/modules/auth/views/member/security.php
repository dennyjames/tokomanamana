<style>
    .collection-title{font-size:18px!important;color:#a7c22a!important;font-weight:700!important}.alert{border-radius:5px;font-size:13px}#change_password input,#change_password select{background-image:none;background-color:transparent;-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;border:1px solid #ccc;border-radius:3px;outline:0}#change_password input:focus,#change_password select:focus{border-color:#a7c22a!important;background-color:none!important;background-image:none;outline:0;box-shadow:none}#change_password button#button_verification{height:34px;width:100%;border:1px solid #ccc;background:#ccc;border-radius:3px;font-weight:700;font-size:12px;color:#444}#change_password button#button_verification:focus{outline:0}#change_password button#button_verification:hover{background:#b3b3b3;border:1px solid #b3b3b3}#change_password button.btn_save{border-radius:3px;background:#a7c22a;border:1px solid #a7c22a;text-transform:capitalize;font-weight:700;font-size:14px}#change_password button.btn_save:hover{color:#fff!important;background:#8ea623;border:1px solid #8ea623}#change_password button:focus{outline:0;color:#fff}#change_password .has-error input{border:1px solid #a94442}
</style>
<div class="my-account">
    <section class="collection-heading heading-content ">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-sm-push-3" style="padding-left: 0px; padding-right: 0px;">
                    <div class="collection-wrapper block" style="padding: 10px 15px;background: none;">
                        <h1 class="collection-title"><span><?php echo lang('account_security'); ?></span></h1>
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container" style="margin-bottom: 30px;" id="section_password">
        <div class="row" id="body_section_password">
            <?php echo $this->load->view('navigation'); ?>
            <div class="send_otp_success col-md-9">
            </div>
            <div class="collection-mainarea col-md-9 col-sm-8 col-xs-12">
                <div class="block" <?php echo ($this->agent->is_mobile()) ? 'style="padding: 0px 15px 25px 15px"' : 'style="padding-top:20px;padding-right:0px;padding-left:0px;"' ?>>
                    <?php
                    if (validation_errors()) {
                        echo $this->template->alert('danger', 'Password baru gagal disimpan. Tinjau kembali isian Anda.');
                    }
                    if ($this->session->flashdata('error')) {
                        echo $this->template->alert('danger', $this->session->flashdata('error'));
                    }
                    if ($this->session->flashdata('success')) {
                        echo $this->template->alert('success', $this->session->flashdata('success'));
                    }
                    ?>
                    <form action="" method="post" class="form-horizontal" id="change_password">
                        <div class="form-group <?php echo (form_error('old_password')) ? 'has-error' : ''; ?>">
                            <label class="col-sm-3 control-label">Password Lama</label>
                            <div class="col-sm-6">
                                <input name="old_password" type="password" required class="form-control" placeholder="Masukkan password lama" value="<?php echo set_value('old_password') ?>">
                                <?php echo form_error('old_password'); ?>
                            </div>
                        </div>
                        <div class="form-group <?php echo (form_error('new_password')) ? 'has-error' : ''; ?>">
                            <label class="col-sm-3 control-label">Password Baru</label>
                            <div class="col-sm-6">
                                <input name="new_password" type="password" required class="form-control" placeholder="Masukkan password baru" value="<?php echo set_value('new_password') ?>">
                                <?php echo form_error('new_password'); ?>
                            </div>
                        </div>
                        <div class="form-group <?php echo (form_error('re_password')) ? 'has-error' : ''; ?>">
                            <label class="col-sm-3 control-label">Konfirmasi Password</label>
                            <div class="col-sm-6">
                                <input name="re_password" type="password" required class="form-control" placeholder="Konfirmasi password baru" value="<?php echo set_value('re_password') ?>">
                                <?php echo form_error('re_password'); ?>
                            </div>
                        </div>
                        <div class="form-group form-verification <?php echo (form_error('token')) ? 'has-error' : ''; ?>">
                            <label class="col-sm-3 control-label">Kode Verifikasi</label>
                            <div class="col-sm-3">
                                <input type="text" name="token" autocomplete="off" required class="form-control" placeholder="Masukkan kode" value="<?php echo set_value('token') ?>">
                            </div>
                            <div class="col-sm-3">
                                <button type="button" id="button_verification" onclick="request_verification(this, '<?php echo encode($customer->phone) ?>')" data-verif="true" style="<?php echo ($this->agent->is_mobile()) ? 'margin-top: 15px;' : '' ; ?>">Minta kode</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-push-3 col-sm-6">
                                <button type="submit" class="btn btn_save" style="<?php echo ($this->agent->is_mobile()) ? 'width: 100%;' : '' ; ?>">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var _0x514c=["\x76\x65\x72\x69\x66","\x64\x61\x74\x61","\x66\x6F\x72\x67\x6F\x74\x5F\x70\x61\x73\x73\x77\x6F\x72\x64","\x61\x75\x74\x68\x2F\x73\x65\x6E\x64\x5F\x6F\x74\x70\x5F\x6C\x6F\x67\x69\x6E","\x70\x6F\x73\x74","\x70\x61\x72\x73\x65","\x73\x74\x61\x74\x75\x73","\x65\x72\x72\x6F\x72","\x4D\x69\x6E\x74\x61\x20\x6B\x6F\x64\x65","\x74\x65\x78\x74","\x23\x63\x68\x61\x6E\x67\x65\x5F\x70\x61\x73\x73\x77\x6F\x72\x64\x20\x2E\x66\x6F\x72\x6D\x2D\x76\x65\x72\x69\x66\x69\x63\x61\x74\x69\x6F\x6E\x20\x23\x62\x75\x74\x74\x6F\x6E\x5F\x76\x65\x72\x69\x66\x69\x63\x61\x74\x69\x6F\x6E","\x4B\x69\x72\x69\x6D\x20\x55\x6C\x61\x6E\x67\x20\x28","\x20\x73\x29","\x6D\x65\x73\x73\x61\x67\x65","\x68\x74\x6D\x6C","\x23\x73\x65\x63\x74\x69\x6F\x6E\x5F\x70\x61\x73\x73\x77\x6F\x72\x64\x20\x2E\x73\x65\x6E\x64\x5F\x6F\x74\x70\x5F\x73\x75\x63\x63\x65\x73\x73","\x61\x6A\x61\x78"];function request_verification(_0xb9b1x2,_0xb9b1x3){let _0xb9b1x4=$(_0xb9b1x2)[_0x514c[1]](_0x514c[0]);let _0xb9b1x5=_0x514c[2];if(_0xb9b1x4== true){$[_0x514c[16]]({url:site_url+ _0x514c[3],type:_0x514c[4],data:{phone:_0xb9b1x3,type:_0xb9b1x5},success:function(_0xb9b1x6){_0xb9b1x6= JSON[_0x514c[5]](_0xb9b1x6);if(_0xb9b1x6[_0x514c[6]]== _0x514c[7]){$(_0x514c[10])[_0x514c[9]](_0x514c[8]);$(_0x514c[10])[_0x514c[1]](_0x514c[0],true)}else {let _0xb9b1x7=120;let _0xb9b1x8=setInterval(function(){if(--_0xb9b1x7){$(_0x514c[10])[_0x514c[9]](_0x514c[11]+ _0xb9b1x7+ _0x514c[12]);$(_0x514c[10])[_0x514c[1]](_0x514c[0],false)}else {clearInterval(_0xb9b1x8);$(_0x514c[10])[_0x514c[9]](_0x514c[8]);$(_0x514c[10])[_0x514c[1]](_0x514c[0],true)}},1000)};$(_0x514c[15])[_0x514c[14]](_0xb9b1x6[_0x514c[13]])}})}else {return false}}
</script>