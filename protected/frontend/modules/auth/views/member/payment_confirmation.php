<?php echo $breadcrumb; ?>
<div class="content-page my-account">
    <div class="container">
        <div class="row">
            <?php echo $this->load->view('navigation'); ?>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="title-single"><?php echo lang('account_text_payment_confirmation'); ?></h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <?php
                            if (validation_errors()) {
                                echo $this->template->alert('danger', 'Konfirmasi pembayaran gagal disimpan. Tinjau kembali isian Anda.');
                            }
                            if ($this->session->flashdata('error')) {
                                echo $this->template->alert('danger', $this->session->flashdata('error'));
                            }
                            if ($this->session->flashdata('success')) {
                                echo $this->template->alert('success', $this->session->flashdata('success'));
                            }
                            echo validation_errors();
                            if ($orders) {
                                ?>
                                <form action="" method="post" class="form-horizontal">
                                    <div class="form-group <?php echo (form_error('code')) ? 'has-error' : ''; ?>">
                                        <label class="col-sm-3 control-label"><?php echo lang('member_payment_confirmation_field_code'); ?></label>
                                        <div class="col-sm-6">
                                            <select name="code" required="" class="form-control">
                                                <?php foreach ($orders->result() as $order) { ?>
                                                    <option value="<?php echo $order->id; ?>"><?php echo $order->code . ' - ' . rupiah($order->total); ?></option>
                                                <?php } ?>
                                            </select>
                                            <?php echo form_error('code'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group <?php echo (form_error('to')) ? 'has-error' : ''; ?>">
                                        <label class="col-sm-3 control-label"><?php echo lang('member_payment_confirmation_field_to'); ?></label>
                                        <div class="col-sm-6">
                                            <select name="to" required="" class="form-control">
                                                <option value="BCA">BCA</option>
<!--                                                <option value="Mandiri">Mandiri</option>
                                                <option value="BRI">BRI</option>-->
                                            </select>
                                            <?php echo form_error('to'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group <?php echo (form_error('date')) ? 'has-error' : ''; ?>">
                                        <label class="col-sm-3 control-label"><?php echo lang('member_payment_confirmation_field_date'); ?></label>
                                        <div class="col-sm-6">
                                            <input name="date" type="text" id="date" required class="form-control" placeholder="<?php echo lang('member_payment_confirmation_field_date'); ?>" value="<?php echo date('Y-m-d'); ?>">
                                            <?php echo form_error('date'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group <?php echo (form_error('total')) ? 'has-error' : ''; ?>">
                                        <label class="col-sm-3 control-label"><?php echo lang('member_payment_confirmation_field_total'); ?></label>
                                        <div class="col-sm-6">
                                            <input name="total" autocomplete="off" type="text" required class="form-control" placeholder="<?php echo lang('member_payment_confirmation_field_total'); ?>">
                                            <?php echo form_error('total'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group <?php echo (form_error('note')) ? 'has-error' : ''; ?>">
                                        <label class="col-sm-3 control-label"><?php echo lang('member_payment_confirmation_field_note'); ?></label>
                                        <div class="col-sm-6">
                                            <textarea name="note" class="form-control" placeholder="<?php echo lang('member_payment_confirmation_field_note'); ?>"></textarea>
                                            <?php echo form_error('note'); ?>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <div class="col-sm-offset-3 col-sm-6">
                                            <button type="submit" class="btn btn-default"><?php echo lang('member_payment_confirmation_button_submit'); ?></button>
                                        </div>
                                    </div>
                                </form>
                                <?php
                            } else {
                                echo '<p>Tidak ada pesanan yang perlu di konfirmasikan pembayarannya.</p>';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>