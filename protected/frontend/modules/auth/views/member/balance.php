<style type="text/css">
    .collection-title{margin-top:25px;font-size:18px!important;color:#a7c22a!important;font-weight:700!important}.btn-pull{background:#a7c22a;border:none;border-radius:5px;text-transform:capitalize}.btn-pull:hover{background:#73861b;color:#fff}.btn-submit-bank{border-radius:5px;border:none;background:#a7c22a}.btn-submit-bank:hover{background:#91a72d}.btn-cancel-bank{border-radius:5px;border:none;background:#d9534f}#a03632 .btn-cancel-bank:hover{border-radius:5px;border:none;background:#e67672}
</style>
<div class="my-account" id="balance">
  <section class="collection-heading heading-content ">
    <div class="container">
      <div class="row">
        <div class="col-sm-9 col-sm-push-3" style="padding: 0px 15px;">
          <div class="collection-wrapper">
            <h1 class="collection-title" style="margin-top: 10px;"><span>Detail Saldo</span></h1>
            <?php echo $breadcrumb; ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="collection-content">
    <div class="collection-wrapper">
      <div class="container" style="margin-bottom: 30px;">
        <div class="row">
          <?php echo $this->load->view('navigation'); ?>
          <div id="collection">
            <div class="collection-mainarea col-md-9 col-sm-8 col-xs-12 block" style="padding: 5px 15px;">
              <br>
              <div class="col-xs-12" style="border:1px solid #e3e6d4; border-radius: 5px; padding: 20px;">
                <div class="row">
                  <div class="col-xs-8">
                    <label style="font-size: 16px;">Total Saldo</label>
                    <input type="hidden" name="OldBalance" id="OldBalance" value="<?php echo $balance->balance; ?>">
                    <div style="font-size: 22px; font-weight: bold;">
                      <?php if($balance){
                        echo rupiah($balance->balance);
                      } else {
                        echo rupiah(0);
                      } ?>
                    </div>
                    <br>
                    <div style="font-style: italic;">*Penarikan saldo minimum sebesar Rp 10.000</div>
                  </div>
                  <div class="col-xs-4">
                    <br>
                    <?php 
                    if($balance){
                      if($balance->balance < 10000){
                        $valid = 'disabled'; 
                      } else if($balance_request){
                        $valid = 'disabled';
                      } else {
                        $valid = ''; 
                      }
                    } else { 
                      $valid = 'disabled'; 
                    } ?>
                    <button id="btnGetSaldo" onclick="cek_account()" <?php echo $valid; ?>  class="btn btn-pull pull-right">Tarik Saldo</button>
                  </div>
                </div>
              </div>
              <div class="col-xs-12" style="border:1px solid #e3e6d4; border-radius: 5px; padding: 20px; margin-top: 15px;">
                <div class="row">
                  <div class="col-xs-12">
                    <div style="font-size: 22px; font-weight: bold;">Riwayat Saldo</div>
                    <br>
                    <div>
                      <?php
                      if($balance_history){ ?>
                      <table>
                        <thead>
                          <tr>
                            <th>Tanggal</th>
                            <th>Tipe</th>
                            <th>Nominal</th>
                            <th>Saldo</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                          if($balance_request){
                            foreach ($balance_request->result() as $request){ ?>
                            <tr>
                              <td style="text-align: center;"><?php echo $request->date_added; ?></td>
                              <td style="font-size: 14px; text-align: center;">Request</td>
                              <td style="font-size: 14px;font-weight: bold; text-align: center; color:#d9534f"><?php echo rupiah($request->amount); ?></td>
                              <td style="font-size: 14px;font-weight: bold; text-align: center; font-style: italic;">Menunggu Konfirmasi</td>
                            </tr>
                      <?php  } 
                          }
                              foreach ($balance_history->result() as $history) { 
                                if($history->type == 'IN'){
                                  $tipe = 'Masuk';
                                  $color = '#a7c22a';
                                } else {
                                  $tipe = 'Keluar';
                                  $color = '#d9534f';
                                }
                                if($history->status == 1){
                                  $balanceh =  'Selesai';
                                } else if($history->status == 2){
                                  $balanceh =  'Dibatalkan';
                                } else {
                                  $balanceh =  rupiah($history->balance);
                                }
                                ?>
                                <tr>
                                  <td style="text-align: center;"><?php echo $history->date_added; ?></td>
                                  <td style="font-size: 14px; text-align: center;"><?php echo $tipe; ?></td>
                                  <td style="font-size: 14px;font-weight: bold; text-align: center; color:<?php echo $color; ?>"><?php echo rupiah($history->amount); ?></td>
                                  <td style="font-size: 14px;font-weight: bold; text-align: center;"><?php echo $balanceh; ?></td>
                                </tr>
                       <?php  }
                          ?>
                        </tbody>
                      </table>
                    <?php } else {
                        echo '<center><div>Tidak Ada Transaksi</div></center>';
                      } ?>
                    </div>
                    <div>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<div class="modal fade" id="AddBank">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <br>
        <center><h4 class="modal-title">Tambah Rekening</h4>
        <div>Pastikan Nomor Rekening & Nama Pemilik Rekening Sesuai Buku Tabungan</div>
        </center>
      </div>
          <div class="modal-body" style="margin: 15px;">
            <div class="form-group">
                <label>Nama Bank</label>
                <select onchange="cek_norek()" id="bank_code" name="bank_code" style="width: 100%; color: #000" class="form-group">
                  <option value="0" selected="">Pilih Bank <i class="fa fa-home"></i></option>
                  <?php
                  foreach ($bank->result() as $b) { ?>
                      <option value="<?php echo $b->code ?>"><?= $b->name ?></option>
                  <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Nomor Rekening</label>
                <input oninput="cek_norek()" onkeypress="validate(event)" id="bank_account_number" name="bank_account_number" style="width: 100%" type="text" name="account_number" class="form-group" required="">
            </div>
            <div class="form-group">
                <label>Nama Pemiliki Rekening</label>
                <input oninput="cek_norek()" id="bank_account_name" name="bank_account_name" style="width: 100%" type="text" name="account_name" class="form-group" required="">
            </div>
          </div>
          <input type="hidden" id="cek_input">
          <div class="modal-footer">
            <button type="button" class="btn btn-cancel-bank" data-dismiss="modal">Batal</button>
            <button type="button" id="btn-submit-bank" onclick="submit_bank()" disabled="" class="btn btn-submit-bank" data-dismiss="modal">Simpan</button>
          </div>
    </div>
  </div>
</div>
<div class="modal fade" id="GetBalance">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <br>
        <center>
          <h4 class="modal-title">Konfirmasi Penarikan Saldo</h4>
          <div class="col-md-12" style="font-size: 14px;">Permohonan penarikan saldo Anda akan Kami proses dalam 1x24 jam hari kerja. Terima kasih</div>
        </center>
      </div>
          <div class="modal-body" style="margin: 15px">
            <br><br>
            <div class="row">
                <div id="error" style="display: none" class="alert alert-danger" role="alert"></div>
                <div class="col-md-4" style="font-weight: bold; font-size: 16px;">Saldo yang Tersedia</div>
                <div class="col-md-8">
                  <input readonly="" style="width: 100%; font-weight: bold" class="form-group" id="old_balance" value="<?php echo rupiah($balance->balance); ?>">
                </div>
                <div class="col-md-4" style="font-weight: bold; font-size: 16px;">Jumlah Penarikan</div>
                <div class="col-md-8">
                  <input style="width: 100%;" oninput="cekvalidreq()" type="number" min="10000" max="10000000" class="form-group number-separator" id="total_request" value="" name="amount">
                </div>
                <div class="col-md-4" style="font-weight: bold; font-size: 16px;">Rekening Penerima</div>
                <div class="col-md-8">
                  <input type="text" readonly="" style="width: 100%" class="form-group" id="total_request" value="<?php echo $bank_account->name.' - '.$bank_account->bank_account_number.' ('.$bank_account->bank_account_name.')'; ?>">
                </div>
                <div class="col-md-12" style="font-size: 12px; text-align: justify; margin-bottom: 30px;">Demi kelancaran proses pencairan saldo, mohon pastikan kembali nama dan nomor rekening yang tertera sudah sesuai dengan buku tabungan Anda.</div>
                <div class="col-md-4" style="font-weight: bold; font-size: 16px;">Kata Sandi</div>
                <div class="col-md-8">
                  <input style="width: 100%" oninput="cekvalidreq()" type="password" class="form-group" id="password" name="password" value="">
                </div>
            </div>
          </div>
          <input type="hidden" id="cek_input">
          <div class="modal-footer">
            <div class="col-md-12">
                <button type="button" class="btn btn-cancel-bank" data-dismiss="modal">Batal</button>
                <button type="button" id="btn-submit-saldo" onclick="submit_saldo()" disabled="" class="btn btn-submit-bank" data-dismiss="modal">Konfirmasi</button>
            </div>
          </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  let current_url1 = '<?=base_url()?>';
  let email = '<?=$this->session->userdata("email")?>';

  function delay(callback, ms) {
    var timer = 0;
    return function() {
      var context = this, args = arguments;
      clearTimeout(timer);
      timer = setTimeout(function () {
        callback.apply(context, args);
      }, ms || 0);
    };
  }
  function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  }
  function formatRupiah(angka, prefix){
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split       = number_string.split(','),
    sisa        = split[0].length % 3,
    rupiah        = split[0].substr(0, sisa),
    ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
   
    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }
   
    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }

  $('#total_request').keyup(delay(function (e) {
    var tot = $('#total_request').val();
    var max = parseInt($('#OldBalance').val());
    if(max > 10000000){
      var maks = 10000000;
    } else {
      var maks = max;
    }
    if(tot < 10000){
      $('#error').html('Minimum Penarikan Saldo Sebesar Rp. 10.000')
      $('#error').attr('style', 'display:block');
      $('#total_request').attr('style', 'width:100%; border:1px solid red');
    } else if(tot > maks) {
      $('#error').html('Maksimum Penarikan Saldo Sebesar Rp. '+formatNumber(maks));
      $('#error').attr('style', 'display:block');
      $('#total_request').attr('style', 'width:100%; border:1px solid red');
    } else {
      $('#error').attr('style', 'display:none');
      $('#total_request').attr('style', 'width:100%');
    }
  }, 500));

  function cek_norek(){
      var norek = $('#bank_account_number').val();
      var bank = $('#bank_code').val();
      var bank_acc_name = $('#bank_account_name').val();
      if(norek != '' && bank != 0 && bank_acc_name != ''){
          $('#btn-submit-bank').prop('disabled', false);
      } else {
          $('#btn-submit-bank').prop('disabled', true);
      }
  }
  function cekvalidreq(){
    var tot = $('#total_request').val();
    var max = parseInt($('#OldBalance').val());
    var pass = $('#password').val();
    var req = 0;
    if(max > 10000000){
      var maks = 10000000;
    } else {
      var maks = max;
    }
    if(tot < 10000){
      var req = 0;
    } else if(tot > maks) {
      var req = 0;
    } else {
      var req = 1;
    }
    if(req == 1 && pass != ''){
      $('#btn-submit-saldo').prop('disabled', false);
    } else {
      $('#btn-submit-saldo').prop('disabled', true);
    }
  }

  function validate(evt) {
    var theEvent = evt || window.event;

    // Handle paste
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain');
    } else {
    // Handle key press
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
    }
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }

  function cek_account(){
    $.ajax({
      url: site_url + 'auth/member/cek_norek',
      type: 'post',
      success: function (response) {
          response = JSON.parse(response);
          if(response.bank_account_number){
            $('#GetBalance').modal('toggle');
          } else {
            $('#AddBank').modal('toggle');
          }
      }
    })
  }
  function submit_bank(){
    $.ajax({
      url: site_url + 'auth/member/save_norek',
      type: 'post',
      data: $('#AddBank input, select'),
      success: function (response) {
          response = JSON.parse(response);
          $( "#norek" ).load(window.location.href + " #norek" );
          if (response.status == 'success') {
              Swal.fire({
                type: 'success',
                title: 'Rekening Bank Berhasil Disimpan',
                showConfirmButton: false,
                timer: 1500
              }).then(function(){ 
                 $('#GetBalance').modal('toggle');
              });
          } else {
              Swal.fire({
                type: 'error',
                title: 'Terjadi Kesalahan!',
                text: 'Oops! Terjadi Kesalahan',
                customClass: 'swal-class'
              });
          }
      }
    })
  }
  function submit_saldo(){
    $.ajax({
      url: site_url + 'auth/member/request_balance',
      type: 'post',
      data: $('#GetBalance input'),
      success: function (response) {
          response = JSON.parse(response);
          // $( "#norek" ).load(window.location.href + " #norek" );
          if (response.status == 'success') {
            // $('#btnGetSaldo').attr('disabled', true);
              Swal.fire({
                type: 'success',
                title: 'Permintaan Penarikan Saldo Berhasil',
                showConfirmButton: false,
                timer: 1500
              }).then(function(){ 
                 location.reload();
              });
          } else {
              Swal.fire({
                type: 'error',
                title: response.message,
                customClass: 'swal-class'
              });
          }
      }
    })
  }
</script>