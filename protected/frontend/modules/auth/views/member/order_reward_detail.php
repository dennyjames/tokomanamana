<div class="my-account" id="detail-order">
    <section class="collection-heading heading-content ">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-sm-push-3">
                    <div class="collection-wrapper">
                        <h1 class="collection-title"><span>Detail Tukar Poin</span></h1>
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container" style="margin-bottom: 30px;">
        <div class="row">
            <?php echo $this->load->view('navigation'); ?>
            <div class="collection-mainarea col-md-9 col-sm-8 col-xs-12" style="padding-left: 0;">
                <div class="row">
                    <div class="col-xs-12" style="padding-right: 0;">
                        <div class="panel">
                            <div class="panel-heading">
                                <h2 class="panel-title">Daftar Pembelian</h2>
                            </div>
                            <div class="panel-body">
                                <div class="order-invoice">
                                    <div class="order-invoice-header">
                                        <div class="order-invoice-header-cell">
                                            <div class="order-info-label">REWARD</div>
                                            <div class="order-info-content"><?php echo $data->nama; ?></div>
                                        </div>
                                        <div class="order-invoice-header-cell">
                                            <div class="order-info-label">PERIODE</div>
                                            <?php
                                                $e = explode(';', $data->periode);
                                                $periode0 = date('d/m/Y', strtotime($e[0].'/'.$e[1].'/'.$e[2]));
                                                $periode1 = date('d/m/Y', strtotime($e[3].'/'.$e[4].'/'.$e[5]));
                                            ?>
                                            <div class="order-info-content"><?php echo $periode0; ?> Sampai <?=$periode1?></div>
                                        </div>
                                    </div>
                                    <div class="order-invoice-product">
                                        <?php
                                            if($data->id_product != 0){
                                                $join_img = array(
                                                    0 => 'product_image pi-pi.product=p.id'
                                                );
                                                $p = $m->get_data('*, p.id as pid', 'products p', $join_img, array('p.id' => $data->id_product, 'primary' => 1))->row();
                                                ?>
                                        <div class="row">
                                            <div class="col-xs-2" style="padding-right: 0;">
                                                <a href="<?php echo seo_url('catalog/products/view/' . $p->pid); ?>" target="_blank">
                                                    <img class="product-image" src="<?php echo get_image($p->image); ?>">
                                                </a>
                                            </div>
                                            <div class="col-sm-7 col-xs-10">
                                                <div class="product-name">
                                                    <a href="<?php echo seo_url('catalog/products/view/' . $p->pid); ?>" target="_blank"><?php echo $p->name; ?></a>
                                                </div>
                                                <?php
                                                    if($data->status_pengiriman == 0){
                                                        ?>
                                                <span class="label label-warning">Belum Dikirim</span>
                                                        <?php
                                                    }else if($data->status_pengiriman == 1){
                                                        ?>
                                                <span class="label label-success">Telah Dikirim</span>
                                                        <?php
                                                    }
                                                ?>
                                                
                                                <div class="product-detail">
                                                    <div class="product-info">
                                                        <div><?php echo $p->code; ?></div>
                                                    </div>
                                                    <span>Jumlah: <?php echo $data->qty_product; ?></span>
                                                    <span>Berat: <?php echo number_format($p->weight) . ' gram'; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                                <?php                                                
                                            }

                                            if($data->id_product_paket != 0){
                                                $join_img = array(
                                                    0 => 'product_image pi-pi.product=p.id'
                                                );
                                                $p = $m->get_data('*, p.id as pid', 'products p', $join_img, array('p.id' => $data->id_product_paket, 'primary' => 1))->row();
                                                ?>
                                        <div class="row">
                                            <div class="col-xs-2" style="padding-right: 0;">
                                                <a href="<?php echo seo_url('catalog/products/view/' . $p->pid); ?>" target="_blank">
                                                    <img class="product-image" src="<?php echo get_image($p->image); ?>">
                                                </a>
                                            </div>
                                            <div class="col-sm-7 col-xs-10">
                                                <div class="product-name">
                                                    <a href="<?php echo seo_url('catalog/products/view/' . $p->pid); ?>" target="_blank"><?php echo $p->name; ?></a>
                                                </div>
                                                <?php
                                                    if($data->status_pengiriman == 0){
                                                        ?>
                                                <span class="label label-warning">Belum Dikirim</span>
                                                        <?php
                                                    }else if($data->status_pengiriman == 1){
                                                        ?>
                                                <span class="label label-success">Telah Dikirim</span>
                                                        <?php
                                                    }
                                                ?>
                                                
                                                <div class="product-detail">
                                                    <div class="product-info">
                                                        <div><?php echo $p->code; ?></div>
                                                    </div>
                                                    <span>Jumlah: <?php echo $data->qty_product; ?></span>
                                                    <span>Berat: <?php echo number_format($p->weight) . ' gram'; ?></span>
                                                </div>
                                            </div>
                                        </div>
                                                <?php                                                
                                            }


                                            if($data->id_coupon != 0){
                                                $q_coupon = $m->get_data('', 'coupons', null, array('id' => $data->id_coupon))->row();
                                                
                                                ?>
                                        <div class="row">
                                            <div class="col-xs-2" style="padding-right: 0;">
                                                <a href="" target="_blank">
                                                    <img class="product-image" src="<?php echo get_image('logo.png'); ?>">
                                                </a>
                                            </div>
                                            <div class="col-sm-7 col-xs-10">
                                                <div class="product-name">
                                                    <a href="#" target="_blank"><?php echo $q_coupon->name; ?></a>
                                                </div>
                                                <div class="product-detail">
                                                    <div>
                                                        <?=$q_coupon->deskripsi?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                                <?php                                                
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="modal-tracking" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;">
    <div class="modal-dialog fadeIn animated">
        <div class="modal-content">
            <form action="#">
                <input type="hidden" name="id">
                <div class="modal-header">
                    <h1 class="modal-title">Lacak Pesanan</h1>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn" data-dismiss="modal" style="background-color: #FFF; color: #000;">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="modal-confirm" class="modal" role="dialog" aria-hidden="true" tabindex="-1" style="display: none;">
    <div class="modal-dialog fadeIn animated">
        <div class="modal-content">
            <input type="hidden" name="id" value="">
            <div class="modal-header">
                <h1 class="modal-title">Apakah Anda yakin transaksi <small></small> sudah diterima?</h1>
            </div>
            <div class="modal-body">
                <p>Lakukan konfirmasi ini hanya jika pesanan Anda sudah benar-benar diterima dengan baik dan lengkap. Karena transaksi yang akan selesai dan dana Anda akan diteruskan ke Penjual.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" data-dismiss="modal" style="background-color: #FFF; color: #000;">TIDAK</button>
                <button type="button" onclick="submit_confirm();" class="btn">YA</button>
            </div>
        </div>
    </div>
</div>
