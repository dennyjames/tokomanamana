<style>
    .collection-title{font-size:18px!important;color:#a7c22a!important;font-weight:700!important}.alert{border-radius:5px;font-size:13px}#change_password input,#change_password select{background-image:none;background-color:transparent;-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;border:1px solid #ccc;border-radius:3px;outline:0}#change_password input:focus,#change_password select:focus{border-color:#a7c22a!important;background-color:none!important;background-image:none;outline:0;box-shadow:none}#change_password button#button_verification{height:34px;width:100%;border:1px solid #ccc;background:#ccc;border-radius:3px;font-weight:700;font-size:12px;color:#444}#change_password button#button_verification:focus{outline:0}#change_password button#button_verification:hover{background:#b3b3b3;border:1px solid #b3b3b3}#change_password button.btn_save{border-radius:3px;background:#a7c22a;border:1px solid #a7c22a;text-transform:capitalize;font-weight:700;font-size:14px}#change_password button.btn_save:hover{color:#fff!important;background:#8ea623;border:1px solid #8ea623}#change_password button:focus{outline:0;color:#fff}#change_password .has-error input{border:1px solid #a94442}
</style>
<div class="my-account">
    <section class="collection-heading heading-content ">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-sm-push-3" style="padding-left: 0px; padding-right: 0px;">
                    <div class="collection-wrapper block" style="padding: 10px 15px;background: none;">
                        <h1 class="collection-title"><span><?php echo lang('account_security'); ?></span></h1>
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container" style="margin-bottom: 30px;" id="section_password">
        <div class="row" id="body_section_password">
            <?php echo $this->load->view('navigation'); ?>
            <div class="send_otp_success col-md-9">
            </div>
            <div class="collection-mainarea col-md-9 col-sm-8 col-xs-12">
                <div class="block" <?php echo ($this->agent->is_mobile()) ? 'style="padding: 0px 15px 25px 15px"' : 'style="padding-top:20px;padding-right:0px;padding-left:0px;"' ?>>
                    <?php
                    if (validation_errors()) {
                        echo $this->template->alert('danger', 'Password baru gagal disimpan. Tinjau kembali isian Anda.');
                    }
                    if ($this->session->flashdata('error')) {
                        echo $this->template->alert('danger', $this->session->flashdata('error'));
                    }
                    if ($this->session->flashdata('success')) {
                        echo $this->template->alert('success', $this->session->flashdata('success'));
                    }
                    ?>
                    <form action="" method="post" class="form-horizontal" id="change_password">
                        <div class="form-group <?php echo (form_error('old_password')) ? 'has-error' : ''; ?>">
                            <label class="col-sm-3 control-label">Password Lama</label>
                            <div class="col-sm-6">
                                <input name="old_password" type="password" required class="form-control" placeholder="Masukkan password lama" value="<?php echo set_value('old_password') ?>">
                                <?php echo form_error('old_password'); ?>
                            </div>
                        </div>
                        <div class="form-group <?php echo (form_error('new_password')) ? 'has-error' : ''; ?>">
                            <label class="col-sm-3 control-label">Password Baru</label>
                            <div class="col-sm-6">
                                <input name="new_password" type="password" required class="form-control" placeholder="Masukkan password baru" value="<?php echo set_value('new_password') ?>">
                                <?php echo form_error('new_password'); ?>
                            </div>
                        </div>
                        <div class="form-group <?php echo (form_error('re_password')) ? 'has-error' : ''; ?>">
                            <label class="col-sm-3 control-label">Konfirmasi Password</label>
                            <div class="col-sm-6">
                                <input name="re_password" type="password" required class="form-control" placeholder="Konfirmasi password baru" value="<?php echo set_value('re_password') ?>">
                                <?php echo form_error('re_password'); ?>
                            </div>
                        </div>
                        <div class="form-group form-verification <?php echo (form_error('token')) ? 'has-error' : ''; ?>">
                            <label class="col-sm-3 control-label">Kode Verifikasi</label>
                            <div class="col-sm-3">
                                <input type="text" name="token" autocomplete="off" required class="form-control" placeholder="Masukkan kode" value="<?php echo set_value('token') ?>">
                            </div>
                            <div class="col-sm-3">
                                <button type="button" id="button_verification" onclick="request_verification(this, <?php echo encode($customer->phone) ?>)" data-verif="true" style="<?php echo ($this->agent->is_mobile()) ? 'margin-top: 15px;' : '' ; ?>">Minta kode</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-push-3 col-sm-6">
                                <button type="submit" class="btn btn_save" style="<?php echo ($this->agent->is_mobile()) ? 'width: 100%;' : '' ; ?>">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    // $.fn.countdown = function (duration) {
    //     var countdown = setInterval(function () {
    //         if (--duration) {
    //             $('#wait span').html(duration);
    //         } else {
    //             clearInterval(countdown);
    //             $('#wait').hide();
    //             $('#request-token').show();
    //         }
    //     }, 1000);
    // };
    // $('#request-token').on('click', function () {
    //     $.ajax({
    //         url: site_url + 'auth/member/request_token',
    //         success: function (data) {
    //             $('#wait').show();
    //             $(".countdown").countdown(120);
    //             $('#request-token').hide();
    //         }
    //     })
    // });
    function request_verification(e, phone) {
        let verif = $(e).data('verif');
        let type = 'forgot_password';
        if(verif == true) {
            $.ajax({
                url: site_url + 'auth/send_otp_login',
                type: 'post',
                data: {
                    phone: phone,
                    type: type
                },
                success: function(data) {
                    data = JSON.parse(data);
                    if(data.status == 'error') {
                        $('#change_password .form-verification #button_verification').text('Minta kode');
                        $('#change_password .form-verification #button_verification').data('verif', true);
                    } else {
                        let duration = 120;
                        let x = setInterval(function() {
                            if(--duration) {
                                $('#change_password .form-verification #button_verification').text('Kirim Ulang (' + duration + ' s)');
                                $('#change_password .form-verification #button_verification').data('verif', false);
                            } else {
                                clearInterval(x);
                                $('#change_password .form-verification #button_verification').text('Minta kode');
                                $('#change_password .form-verification #button_verification').data('verif', true);
                            }
                        }, 1000);
                    }
                    $('#section_password .send_otp_success').html(data.message);
                }
            });
        } else {
            return false;
        }
    }

</script>