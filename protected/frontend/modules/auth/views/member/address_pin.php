<script type="text/javascript">
    var centreGot = false;
</script>
<?php echo $map['js']; ?>
<script type="text/javascript">
    $(function () {
        if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (pos) {

                    console.log($('#temp-lat').val());
                    console.log($('#temp-lng').val());

                    if ($('#temp-lat').val()!=0){
                        var lat = $('#temp-lat').val();
                        var lng = $('#temp-lng').val();
                    } else {
                        var lat = pos.coords.latitude;
                        var lng = pos.coords.longitude;
                    }
                    
                    map.setCenter(new google.maps.LatLng(lat, lng));
                    var mapCentre = map.getCenter();
                    marker_0.setOptions({
                        position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
                    });
                    save_lat_lng(mapCentre.lat(), mapCentre.lng());
                });
        }
        $('#btn-set-location-pin').on('click', function () {
            $.ajax({
                type: 'post',
                url: site_url + 'auth/member/set_lokasi_pin',
                data: {lat: $('#temp-lat').val(), lng: $('#temp-lng').val()},
                success: function (data) {
                    var location = {
                        lat: $('#temp-lat').val(),
                        lng: $('#temp-lng').val(),
                    }
                    window.alert('Sukses Menambahkan Pin');
                        $('#modal-location-pin').modal('hide');
                        $('#modal-address').modal('show');
                    localStorage.setItem('tmmLoc', JSON.stringify(location));
                    //window.location.reload();
                }
            });
        });
        $('#my-location-pin').on('click', function () {
            $('#modal-location-pin').modal('hide');
            $('#modal-address').modal('show');
        });
    })
    function save_lat_lng(latitude, longitude) {
        $('#temp-lat').val(latitude);
        $('#temp-lng').val(longitude);
    }
</script>
<style>
    .pac-container{
        z-index: 1000000001;
    }
</style>
<label>Ketik alamat/nama jalan/nama gedung</label>
<input class="form-control" id="search-location">
<?php echo $map['html']; ?>
<label>Klik dan geser penanda untuk merubah lokasi</label>
<?php if(!$this->session->has_userdata('location')){ ?>
<div class="alert alert-danger" style="margin-bottom: 0; font-size: 14px;">
    Silahkan atur lokasi pengiriman Anda. Klik LOKASI SAYA atau ketik nama jalan.
</div>
<?php } ?>