<div class="my-account" id="wishlist">
    <section class="collection-heading heading-content ">
        <div class="container">
            <div class="row">
                <div class="col-sm-9 col-sm-push-3">
                    <div class="collection-wrapper">
                        <h1 class="collection-title"><span>Poin Saya</span></h1>
                        <?php echo $breadcrumb; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
        $total_point = 0;
        $m = $this->Model;
        $q_point = $m->get_data('point', 'point', null, array('id_customer' => $this->session->userdata('user_id')));

        if($q_point->num_rows() > 0){
            $total_point = $q_point->row()->point;
        }

        $tab_point = $this->session->userdata('tab_point');
        $t_point = array(
            0 => $tab_point == '0' ? "color: #97C23C" : "",
            1 => $tab_point == '1' ? "color: #97C23C" : "",
            2 => $tab_point == '2' ? "color: #97C23C" : ""
        );
    ?>
    <section class="collection-content">
      <div class="collection-wrapper">
        <div class="container" style="margin-bottom: 30px;">
          <div class="row">
            <?php echo $this->load->view('navigation'); ?>
            <div id="collection">
              <div class="collection-mainarea col-md-9 col-sm-8 col-xs-12">
                <?php if ($products->num_rows() > 0) { ?>
                  <div class="collection-items clearfix">
                    <div class="products">
                      <div class="col-xs-12 col-sm-6" style="padding: 4px;">
                        <div class="product">
                          <div class="row-right animMix">
                            <div class="grid-mode">
                              <div class="product-title">Poin Saya</div>
                              <div class="product-price" style="margin: 12px; font-size: 24px">
                                <h1><?=number_format($total_point)?></h1>
                              </div>
                            </div>
                            <!-- <div class="grid-mode">
                                <div class="product-title">Poin Saya</div>
                                <div class="product-price">
                                    <span class="price_sale"><span class="money">Price</span></span>
                                </div>
                            </div> -->
                          </div>
                          <div style="width: 100%; text-align: center;">
                            <?php
                              $periode = 'Belum Ada';

                              $qsettings = $m->get_data('', 'settings', null, array('key' => 'periode_tukar_point'));

                              if($qsettings->num_rows() > 0){
                                $exp = explode(';', $qsettings->row()->value);

                                if($exp[6] == '1'){
                                  $periode = date('d M Y', strtotime($exp[0].'/'.$exp[1].'/'.$exp[2])).' S/D '.date('d M Y', strtotime($exp[3].'/'.$exp[4].'/'.$exp[5]));
                                }
                              }
                            ?>
                            <p>Periode tukar point : </p><h3 style="color: red"><?=$periode?></h3>
                          </div>
                        </div>
                      </div>

                      <div class="col-xs-12 col-sm-6" style="padding: 4px;">
                        <div class="product">
                          <div class="row-right animMix">
                            <div class="grid-mode">
                              <div class="product-title">Poin Saya</div>
                              <div class="product-price" style="margin: 12px; font-size: 24px">
                                <h1><?=number_format($total_point)?></h1>
                              </div>
                            </div>
                            <!-- <div class="grid-mode">
                                <div class="product-title">Poin Saya</div>
                                <div class="product-price">
                                    <span class="price_sale"><span class="money">Price</span></span>
                                </div>
                            </div> -->
                          </div>
                          <div style="width: 100%; text-align: center;">
                            <a href="" style="background-color: #97C23C; border: none; color: white; padding: 8px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 14px; margin: 4px 2px; cursor: pointer;"  onclick="return confirm('Yakin hapus produk dari wishlist?');">Tukar Poin</a>
                          </div>
                        </div>
                      </div>

                      <div class="col-xs-12" style="padding: 4px;">
                        <div class="product">
                          <div class="row-right animMix">
                            <div class="grid-mode" style="padding-left: 8px">
                                <!-- <a href="#" style="font-size: 14px; margin-right: 16px; <?=$t_point[0]?>"><b>Tukar Poin</b></a>
                                <a href="#" style="font-size: 14px; margin-right: 16px; <?=$t_point[1]?>"><b>Voucher</b></a>
                                <a href="#" style="font-size: 14px; <?=$t_point[2]?>"><b>Riwayat Tukar Poin</b></a>
                                <div class="product-title">Poin Saya</div>
                                <div class="product-price" style="margin: 12px; font-size: 24px">
                                    <h1><?=number_format($total_point)?></h1>
                                </div> -->

                              <ul class="nav nav-tabs">
                                <li id="tukar_poin" class="active"><a href="#" onclick="changeTab(0)">Tukar Poin</a></li>
                                <li id="voucher"><a href="<?=base_url()?>member/coupons" onclick="changeTab(1)">Coupon</a></li>
                                <li id="riwayat_tukar_poin"><a href="<?=base_url()?>member/riwayat_tukar_poin" onclick="changeTab(2)">Riwayat Tukar Poin</a></li>
                              </ul>

                              <hr>

                              <!-- <ul class="nav-pills">
                                <li id="produk_tab" style="background-color: #97C23C; padding: 12px">
                                  <a onclick="changeTab1(0)" id="produk_tab_a" style="color: white">Produk</a>
                                </li>
                                <li id="pulsa_tab" style="padding: 12px">
                                  <a onclick="changeTab1(1)" id="pulsa_tab_a">Pulsa</a>
                                </li>
                                <li id="voucher_tab" style="padding: 12px">
                                  <a onclick="changeTab1(2)" id="voucher_tab_a">Voucher Belanja</a>
                                </li>
                                <li id="indomaret_tab" style="padding: 12px">
                                  <a onclick="changeTab1(3)" id="indomaret_tab_a">Voucher Indomaret</a>
                                </li>
                              </ul>

                              <hr> -->
                            <?php
                              $where_reward = array(
                                'r.is_active' => '1'
                              );

                              $where_reward0 = array(
                                0 => "r.customer_merchant='g' or r.customer_merchant='c'"
                              );
                              $reward_produk = $m->get_data('', 'reward r', null, $where_reward, '', 'r.point desc', 0, 0, $where_reward0);

                              foreach ($reward_produk->result() as $key) {
                                $gambar = $key->gambar == '' ? base_url().'files/images/logo.png' : $key->gambar;

                                $param = true;
                                if($qsettings->num_rows() > 0){
                                  $q_status_periode = $m->get_data('', 'reward_claim_temp', null, array('status_periode_aktif' => 1, 'id_reward' => $key->id), '', '', 0, 0, array(0 => "id_customer LIKE '%,".$this->session->userdata('user_id').",%'"));

                                  if($q_status_periode->num_rows() > 0){
                                    $param = false;
                                  }
                                }

                                if($param){
                                ?>
                              <div class="product-item col-sm-3" style="border-width: 1px; border-style: solid; border-color: #dddddd;">
                                <div class="product">
                                  <div class="row-left">
                                    <a href="<?=base_url('catalog/rewards/view/'.$key->id)?>" class="hoverBorder container_item" style="height: 200px">
                                      <img style="" src="<?=$gambar?>" class="img-responsive" alt="">
                                    </a>
                                  </div>
                                  <div class="row-right animMix">
                                    <div class="grid-mode">
                                      <div class="product-title">
                                        <a href="<?=base_url('catalog/rewards/view/'.$key->id)?>" onclick="get_detail_reward()" style="font-size: 14px"><?=$key->nama?></a>
                                      </div>
                                      <div class="product-price">
                                        <p>
                                          <?php
                                            if(strlen($key->deskripsi) > 35){
                                              echo substr($key->deskripsi, 0, 35).'...';
                                            }else{
                                              echo $key->deskripsi;
                                            }
                                          ?>
                                        </p>
                                        <span class=""><span class="money">Minimal Point: <b style="font-size: 14px"><?=number_format($key->point)?></b></span></span>
                                      </div>
                                      <p>Tersisia: <?=number_format($key->qty)?></p>
                                    </div>
                                  </div>
                                  <div style="width: 100%; text-align: center;">
                                    <?php
                                      if($periode != 'Belum Ada'){

                                        if($m->cek_periode_tukar_point()){
                                          ?>
                                    <button onclick="tukar_point('<?=$key->id?>')" style="background-color: #97C23C; border: none; color: white; padding: 8px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 14px; cursor: pointer;">Tukar Poin</button>
                                          <?php
                                        }
                                      }
                                    ?>
                                    
                                  </div>
                                </div>
                              </div>
                                <?php
                                }
                              }
                            ?>
                            </div>
                          </div>
                          
                        </div>
                      </div>

                      <script type="text/javascript">
                        function tukar_point(id) {
                          $.ajax({
                            type: 'POST',
                            url: current_url1 + 'checkout/cart_point/add_cart_point',
                            data: {
                              id_reward: id,
                              email: email
                            },
                            dataType: "json",
                            success: function (data) {
                              if (data.success == 1) {
                                window.location.replace(current_url1 + 'cart_point');
                              }else if(data.success == '0'){
                                alert('Ooops.. ' + data.msg);
                              }
                            }
                          });
                        }

                        function changeTab(argument) {
                          if(argument == 0){
                            $('#tukar_poin').addClass('active');
                            $('#voucher').removeClass('active');
                            $('#riwayat_tukar_poin').removeClass('active');
                          }else if(argument == 1){
                            $('#tukar_poin').removeClass('active');
                            $('#voucher').addClass('active');
                            $('#riwayat_tukar_poin').removeClass('active');
                          }else{
                            $('#tukar_poin').removeClass('active');
                            $('#voucher').removeClass('active');
                            $('#riwayat_tukar_poin').addClass('active');
                          }
                        }

                        function changeTab1(argument) {
                          if(argument == 0){
                            $('#produk_tab').css('background-color', '#97C23C');
                            $('#pulsa_tab').css('background-color', '');
                            $('#voucher_tab').css('background-color', '');
                            $('#indomaret_tab').css('background-color', '');

                            $('#produk_tab_a').css('color', 'white');
                            $('#pulsa_tab_a').css('color', 'black');
                            $('#voucher_tab_a').css('color', 'black');
                            $('#indomaret_tab_a').css('color', 'black');
                          }else if(argument == 1){
                            $('#produk_tab').css('background-color', '');
                            $('#pulsa_tab').css('background-color', '#97C23C');
                            $('#voucher_tab').css('background-color', '');
                            $('#indomaret_tab').css('background-color', '');

                            $('#produk_tab_a').css('color', 'black');
                            $('#pulsa_tab_a').css('color', 'white');
                            $('#voucher_tab_a').css('color', 'black');
                            $('#indomaret_tab_a').css('color', 'black');
                          }else if(argument == 2){
                            $('#produk_tab').css('background-color', '');
                            $('#pulsa_tab').css('background-color', '');
                            $('#voucher_tab').css('background-color', '#97C23C');
                            $('#indomaret_tab').css('background-color', '');

                            $('#produk_tab_a').css('color', 'black');
                            $('#pulsa_tab_a').css('color', 'black');
                            $('#voucher_tab_a').css('color', 'white');
                            $('#indomaret_tab_a').css('color', 'black');
                          }else if(argument == 3){
                            $('#produk_tab').css('background-color', '');
                            $('#pulsa_tab').css('background-color', '');
                            $('#voucher_tab').css('background-color', '');
                            $('#indomaret_tab').css('background-color', '#97C23C');

                            $('#produk_tab_a').css('color', 'black');
                            $('#pulsa_tab_a').css('color', 'black');
                            $('#voucher_tab_a').css('color', 'black');
                            $('#indomaret_tab_a').css('color', 'white');
                          }
                        }

                        function get_detail_reward() {
                          console.log('ccc');
                        }
                      </script>

                      <?php if(false){?>
                      <?php foreach ($products->result() as $product) { ?>
                        <div class="product-item col-sm-3" style="padding: 1px;">
                          <div class="product">
                            <div class="row-left">
                              <a href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>" class="hoverBorder container_item">
                                    <img src="<?php echo ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" class="img-responsive" alt="<?php echo $product->name; ?>">
                              </a>
                            </div>
                            <div class="row-right animMix">
                              <div class="grid-mode">
                                <div class="product-title">
                                  <a href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>"><?php echo $product->name; ?></a>
                                </div>
                                <div class="product-price">
                                  <span class="price_sale"><span class="money"><?php echo rupiah($product->price); ?></span></span>
                                </div>
                              </div>
                            </div>
                            <div class="delete">
                              <a href="<?php echo site_url('auth/member/delete_wishlist/' . encode($product->wishlist)); ?>" onclick="return confirm('Yakin hapus produk dari wishlist?');">Hapus</a>
                            </div>
                          </div>
                        </div>
                      <?php } ?>
                      <?php } ?>
                    </div>
                  </div>
                  <div style="display: block; text-align: center;">
                    <div class="pagi-bar">
                      <?php echo $pagination; ?>
                    </div>
                  </div>
                <?php } else { ?>
                  <div class="block">
                    <p>Belum ada produk di wishlist.</p>
                  </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
</div>

<script type="text/javascript">
  let current_url1 = '<?=base_url()?>';
  let email = '<?=$this->session->userdata("email")?>';
</script>