<div class="content-page">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 box">
                <div class="form-my-account">
                    <form class="login-form block-login" method="POST">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Masuk</h3>
                            </div>
                            <div class="col-sm-12">
                                <?php if ($message) { ?>
                                    <div class="alert alert-danger fade in alert-dismissable">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                        <?php echo $message; ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <p>
                                    <input name="identity" type="email" required="" placeholder="<?php echo lang('login_identity_label'); ?>" value="<?php echo set_value('identity'); ?>">
                                    <?php echo form_error('identity'); ?>
                                </p>
                                <p>
                                    <input name="password" type="password" required="" placeholder="<?php echo lang('login_password_label'); ?>">
                                    <?php echo form_error('password'); ?>
                                </p>
                                <p>
                                    <button type="submit" class="btn btn-default btn-login"><?php echo lang('login_submit_btn'); ?></button>
                                </p>
                                <div class="table create-account">
                                    <div class="text-left">
                                        <!--<a href="#" class="color">Lost your password?</a>-->
                                    </div>
                                    <div class="text-right">
                                        <p>Belum jadi member? <a href="<?php echo site_url('member/register'); ?>" class="color">Daftar</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>