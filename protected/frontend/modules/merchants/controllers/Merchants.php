<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Merchants extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('merchant_model', 'merchant');
        $this->load->library('pagination');
        $this->load->library('breadcrumb');
    }

    public function view($id) {
        $merchant = $this->merchant->get($id);
        if (!$merchant)
            show_404();
        //pagination
        $config['base_url'] = current_url();
        $config['total_rows'] = $this->merchant->getProducts($id, '', '', '')->num_rows();
        $config['per_page'] = ($this->input->get('show')) ? $this->input->get('show') : 20;
        $this->pagination->initialize($config);
        $start = ($this->input->get('page')) ? ($this->input->get('page') - 1) * $config['per_page'] : 0;

        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        $this->breadcrumb->add($merchant->name, seo_url('merchants/view/' . $merchant->id));
        
        $this->data['data'] = $merchant;
        $this->data['products'] = $this->merchant->getProducts($id, ($this->input->get('sort')) ? $this->input->get('sort') : 'popular', $config['per_page'], $start);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['meta_description'] = $merchant->short_description. ', ' . settings('meta_title');
        $this->data['total_products'] = $config['total_rows'];
        $this->data['start'] = $start + 1;
        $this->data['to'] = ($this->data['total_products'] < $config['per_page']) ? $this->data['total_products'] : ($start * $config['per_page']);

        $this->output->set_title($merchant->name . ' - ' . settings('meta_title'));
        $this->template->_init();
//        $this->load->js('assets/frontend/js/modules/categories.js');
        $this->load->view('view', $this->data);
    }

}
