<script type="text/javascript">
    var centreGot = false;
</script>
<?php echo $map['js']; ?>
<script type="text/javascript">
    $(function () {
        if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (pos) {
                    var lat = pos.coords.latitude;
                    var lng = pos.coords.longitude;
                    map.setCenter(new google.maps.LatLng(lat, lng));
                    var mapCentre = map.getCenter();
                    marker_0.setOptions({
                        position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
                    });
                    // save_lat_lng(mapCentre.lat(), mapCentre.lng());
                });
        }
        $('#btn-set-location').on('click', function () {
            $.ajax({
                type: 'post',
                url: site_url + 'location/set',
                data: {lat: $('#temp-lat').val(), lng: $('#temp-lng').val()},
                success: function (data) {
                    var location = {
                        lat: $('#temp-lat').val(),
                        lng: $('#temp-lng').val(),
                    }
                    localStorage.setItem('tmmLoc', JSON.stringify(location));
                    window.location.reload();
                }
            });
        });
        $('#my-location').on('click', function () {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function (pos) {
                    var lat = pos.coords.latitude;
                    var lng = pos.coords.longitude;
                    map.setCenter(new google.maps.LatLng(lat, lng));
                    var mapCentre = map.getCenter();
                    marker_0.setOptions({
                        position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng())
                    });
                    // save_lat_lng(mapCentre.lat(), mapCentre.lng());
                    save_lat_lng_new(mapCentre.lat(), mapCentre.lng());
                });
            }
        });
    })
    function save_lat_lng(latitude, longitude) {
        $('#temp-lat').val(latitude);
        $('#temp-lng').val(longitude);
    }
    function save_lat_lng_new(latitude, longitude) {
        $('#modal_set_address #temp_lat_register').val(latitude);
        $('#modal_set_address #temp_lng_register').val(longitude);
    }
    function save_location_new() {
        let lat = $('#modal_set_address #temp_lat_register').val(),
            lng = $('#modal_set_address #temp_lng_register').val();
        var location = {
            lat: lat,
            lng: lng,
        }
        <?php if($this->agent->is_mobile()) : ?>
            $('#register_form #lat_register').val(lat);
            $('#register_form #lng_register').val(lng);
        <?php else : ?>
            $('#modal_register #lat_register').val(lat);
            $('#modal_register #lng_register').val(lng);
        <?php endif; ?>
        localStorage.setItem('tmmLoc', JSON.stringify(location));
        $('#modal_set_address').modal('hide');
        $('#modal_register').modal('show');
        $('#modal_register .save_address_success').html(`<div class="alert alert-success" style="border-radius:3px;">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    <b>Pin alamat berhasil disimpan!</b>
                                </div>`);
    }
</script>
<style>
    .pac-container{
        z-index: 1000000001;
    }
</style>
<label>Ketik alamat/nama jalan/nama gedung</label>
<input class="form-control" id="search-location" placeholder="Masukkan alamat lengkap">
<?php echo $map['html']; ?>
<br>
<label>Klik dan geser penanda untuk merubah lokasi</label>
<?php if(!$this->session->has_userdata('location')){ ?>
<div class="alert alert-warning" style="margin-bottom: 0; font-size: 14px;">
    Silahkan atur alamat Anda. Klik LOKASI SAYA atau ketik nama jalan.
</div>
<?php } ?>