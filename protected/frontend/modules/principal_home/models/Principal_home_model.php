<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal_home_model extends CI_Model {

	// function get_etalase($id) {
 //        $this->db->select('e.id,e.name,COUNT(p.etalase) as count_total')
 //                ->join('etalase e','e.id = p.etalase')
 //                ->where('p.store_id', $id)
 //                ->where('p.status',1)
 //                ->where('p.etalase!=',0)
 //                ->group_by('e.name')
 //                ->order_by('e.name ASC');
 //        $query = $this->db->get('products p');
 //        return ($query->num_rows() > 0) ? $query : FALSE;
 //    }

    function get_etalase($id) {
        $this->db->select('e.id,e.name,COUNT(pps.id) as count_total')
                ->join('etalase e','e.id = p.etalase')
                ->join('products_principal_stock pps', 'p.id = pps.product_id', 'left')
                ->where('p.store_id', $id)
                ->where('p.store_type', 'principal')
                ->where('p.status',1)
                ->where('p.etalase!=',0)
                ->group_by('e.name')
                ->order_by('e.name ASC');
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    // function get_merchant_homepage($id) {
    //     $this->db->select('m.*,m.id AS store_id,m.name AS merchant_name,m.description AS merchant_description,m.type AS merchant_type,m.date_added AS merchant_date_added,pv.name AS province_name,c.name AS city_name,p.*,p.id AS product_id,p.meta_description,p.meta_keyword,p.name AS product_name,COUNT(p.id) AS total_product,mu.fullname AS full_name', FALSE)
    //             ->join('products p','p.store_id ='.$id)
    //             ->join('provincies pv','pv.id = m.province')
    //             ->join('cities c','c.id = m.city')
    //             ->join('merchant_users mu','mu.id = m.auth')
    //             ->where('m.status', 1)
    //             ->where('p.status',1)
    //             ->where('m.id', $id);
    //     $query = $this->db->get('merchants m');
    //     return ($query->num_rows() > 0) ? $query->row() : FALSE;
    // }

    function get_principal_homepage($id) {
    	$this->db->select('p.*, p.id AS store_id')->where('id', $id);
    	$query = $this->db->get('principles p');
    	return ($query->num_rows() > 0) ? $query->row() : FALSE;
    }

    // function get_total_transaction($id){
    //     $this->db->select('COUNT(oi.id) AS total_transaction', FALSE)
    //                 ->where('oi.order_status',7)
    //                 ->where('oi.merchant',$id);
    //     $query = $this->db->get('order_invoice oi');
    //     return $query->row();
    // }

    // function get_rating ($store) {
    //     $this->db->select('IFNULL((SUM(rating_speed) + SUM(rating_service) + SUM(rating_accuracy)) / (COUNT(id) * 3), 0) AS rating,
    //                         IFNULL(COUNT(id), 0) AS count_rating')
    //             ->where('merchant', $store)
    //             ->where('status', 1);

    //     $query = $this->db->get('product_review');
    //     return ($query->num_rows() > 0) ? $query->row() : FALSE;
    // }
    function get_rating ($store) {
        $this->db->select('IFNULL((SUM(rating_speed) + SUM(rating_service) + SUM(rating_accuracy)) / (COUNT(product_review.id) * 3), 0) AS rating,
                            IFNULL(COUNT(product_review.id), 0) AS count_rating')
        		->join('merchants', 'merchants.id = product_review.merchant', 'LEFT')
                ->where('merchants.principal_id', $store)
                ->where('product_review.status', 1);

        $query = $this->db->get('product_review');
        return ($query->num_rows() > 0) ? $query->row() : FALSE;
    }

    function get_no_etalase($id) {
        $this->db->select('p.etalase', FALSE)
                ->join('products_principal_stock pps', 'pps.product_id = p.id', 'right')
                ->where('p.store_id', $id)
                ->where('p.status',1)
                ->where('p.store_type', 'principal')
                ->where('p.etalase',0);
        $query = $this->db->get('products p');
        return $query->num_rows();
    }

    function get_ekspedisi($id){
        $this->db->select('m.shipping', FALSE)
                ->where('m.id', $id)
                ->where('m.status',1);
        $query = $this->db->get('merchants m');
        return $query->row();
    }

    function count_row_speed($id,$rating) {
    	$this->db->join('merchants', 'product_review.merchant = merchants.id', 'LEFT');
        $this->db->where(['product_review.rating_speed' => $rating]);
        $this->db->where('merchants.principal_id',$id);

        $query =$this->db->get('product_review');
        return ($query->num_rows() > 0) ? $query->num_rows() : 0; 
    }

    function count_row_service($id,$rating) {
    	$this->db->join('merchants', 'product_review.merchant = merchants.id', 'LEFT');
        $this->db->where(['product_review.rating_service' => $rating]);
        $this->db->where('merchants.principal_id',$id);

        $query =$this->db->get('product_review');
        return ($query->num_rows() > 0) ? $query->num_rows() : 0; 
    }

    function count_row_accuracy($id,$rating) {
    	$this->db->join('merchants', 'product_review.merchant = merchants.id', 'LEFT');
        $this->db->where(['product_review.rating_accuracy' => $rating]);
        $this->db->where('merchants.principal_id',$id);

        $query =$this->db->get('product_review');
        return ($query->num_rows() > 0) ? $query->num_rows() : 0; 
    }


    function get_products($store_id, $sort = 'popular', $limit = 20, $start = 0,$word = '',$etalase = NULL) {
        $query_order = 'ORDER BY ';
        switch ($sort) {
            default:
            case 'popular':
                $query_order .= 'viewed DESC';
                break;
            case 'newest' :
                $query_order .= 'date_added DESC';
                break;
            case 'lowprice' :
                $query_order .= 'price ASC';
                break;
            case 'highprice' :
                $query_order .= 'price DESC';
                break;
            case 'nameAZ' :
                $query_order .= 'name ASC';
                break;
            case 'nameZA' :
                $query_order .= 'name DESC';
                break;
        }

        $query_limit = '';
        if ($limit) {
            $query_limit = 'LIMIT ' . $start . ',' . $limit;
        }
        $where_etalase = '';
        if($etalase != NULL ){
            $where_etalase = 'AND products.etalase='.$etalase;
        }

        return $this->db->query("SELECT products_principal_stock.product_id AS product_id,
                                        products_principal_stock.branch_id AS store_id,
                                        products_principal_stock.quantity,
                                        products.name,
                                        products.short_description,
                                        products.code,
                                        products.category,
                                        products.brand,
                                        products.price AS price,
                                        products.discount,
                                        products.promo,
                                        products.preorder,
                                        products.promo_data,
                                        products.viewed,
                                        products.date_added,
                                        products.free_ongkir,
                                        merchants.name AS merchant_name,
                                        merchants.type AS store_type,
                                        product_image.image
                                FROM products_principal_stock
                                LEFT JOIN products ON products_principal_stock.product_id = products.id
                                LEFT JOIN product_image ON product_image.product = products.id AND product_image.primary = 1
                                LEFT JOIN merchants ON products_principal_stock.branch_id = merchants.id
                                WHERE products.status = 1
                                AND products.store_id = $store_id
                                AND (products_principal_stock.quantity > 0 OR products.preorder = 1)
                                AND products.store_type = 'principal'
                                AND (products.code LIKE '%$word%' OR products.name LIKE '%$word%' OR products.meta_keyword REGEXP '$word')
                                
                                $where_etalase
 
                                
                                $query_order
                                $query_limit");

    }

}