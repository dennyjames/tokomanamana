<style>
    .collection-title{margin-top:25px;font-size:18px!important;color:#e28935!important;font-weight:700!important}.breadcrumb a,i.fa.fa-home{color:#fe9020!important}.breadcrumb a:hover,.breadcrumb a:hover i.fa.fa-home{color:#969eaa!important}.product-item{border:none}.product{margin-left:7px;margin-right:7px;margin-bottom:12px!important;box-shadow:0 0 7px -1px rgba(0,0,0,.175);min-height:355px;border-radius:5px}#collection .collection-mainarea .collection-items .product-item{border:none}.product-footer{margin-top:0}.garis-tengah{width:100%;height:1px;background-color:#e8e8e8;display:block;margin-top:10px}.tombol-cart{z-index:1;background-color:#fff;position:absolute;top:48%;right:5%;padding:0;display:inline-block;border:1px solid rgba(226,137,53,.175);height:40px;border-radius:50%;width:40px;box-shadow:0 0 7px 1px rgba(226,137,53,.175);-webkit-transition:all .5s;-moz-transition:all .5s;-ms-transition:all .5s;-o-transition:all .5s;transition:all .5s}.tombol-cart button{background:0 0;border:none;line-height:0;padding:0 7px;outline:0}.tombol-cart button-cart:active,.tombol-cart button-cart:hover,.tombol-cart button:active,.tombol-cart button:hover{border:none;background:0 0;outline:0}.tombol-cart a{padding:12px 7px;line-height:37px}.tombol-cart:hover{background-color:#e28935;box-shadow:0 0 7px 1px rgba(226,137,53,.175)}.tombol-cart:hover .icon-cart i{color:#fff}.pagination_group{padding:10px 0!important;text-align:right}.pagination_group .pagination li a{border-top:1px solid #e0e0e0;border-bottom:1px solid #e0e0e0;border-left:1px solid #e0e0e0;background-color:#fff;color:#fe9020;height:30px;vertical-align:middle;padding:0 13px;text-transform:capitalize;font-size:13px;border-radius:0;font-weight:500;-webkit-transition:all .5s;-moz-transition:all .5s;-ms-transition:all .5s;-o-transition:all .5s;transition:all .5s}.pagination_group .pagination li.active a{background-color:#f1f1f1;color:#ccc;border:1px solid #e0e0e0}.pagination_group .pagination li:hover a{background-color:#f1f1f1;color:#ccc}.pre-order-label{color:#666;bottom:0;display:inline-block;margin-top:3px;position:relative;z-index:1;padding:2px 10px;background-color:#eee;font-weight:700;border-radius:2px}.product-discount-label{position:absolute;top:8px;right:10px;z-index:1;background-color:#d9534f;color:#fff;padding:5px 10px;border-radius:5px}.sortBy{margin-left:4%;padding-bottom:15px}.free-ongkir-label{color:#fff;bottom:0;display:inline-block;margin-top:3px;margin-left:1px;margin-right:1px;position:relative;z-index:1;padding:2px 5px;background-color:#e28935;font-weight:700;border-radius:2px}.btn-add-to-wishlist{position:absolute;z-index:1;top:6px;left:10px;font-size:18px;transition:.5s}.btn-add-to-wishlist:hover{color:#d9534f!important}.fill_image{box-shadow:0 0 7px -1px rgba(0,0,0,.175)}.fill_image img{width:1200px;height:235px;display:block}.fill_image_mobile img{width:100%;height:auto;display:block;max-height:117.5px;max-width:575px}.collection-leftsidebar{box-shadow:0 0 7px -1px rgba(0,0,0,.175)}.collection-wrapper{margin-top:20px}@media (max-width:320px){.pre-order-label{position:absolute!important;margin-bottom:10px;bottom:0}.product{min-height:355px!important}.tombol-cart{top:48%}.hoverBorder{width:70%!important}.img-responsive{margin-left:23%}.input-group{margin-left:12px}}@media (max-width:375px){.pre-order-label{position:relative!important;margin-top:5px}}@media (max-width:414px){.input-group{margin-left:12px}}@media (max-width:425px) and (min-width:376px){.tombol-cart{top:48%}}@media (max-width:375px) and (min-width:321px){.tombol-cart{top:48%}.product{min-height:357px!important}.input-group{margin-left:13px}}@media (max-width:767px) and (min-width:426px){.tombol-cart{top:48%}}@media (max-width:767px){.pre-order-label{position:relative!important;margin-bottom:5px!important}.product{min-height:360px}.container{padding-left:10px;padding-right:22px}}@media (max-width:1024px) and (min-width:768px){.tombol-cart{top:48%;margin-left:20%}.sortBy{margin-left:2%}.input-group{margin-left:12px}}@media (min-width:1024px){.product{min-height:355px}}@media (max-width:1199px) and (min-width:992px){.sortBy{margin-left:2%}}@media (max-width:991px) and (min-width:585px){#collection .products .product-item .sortBy{width:33.333333%!important}}.form-control{border-radius:4px!important}.search_product{border-color:#e28935;outline:0;box-shadow:none!important}.form-control:focus{border-color:#e28935}#search_addon{background-color:#e28935;border:0;position:absolute;z-index:2;cursor:pointer;color:#fff;right:0;bottom:0;top:0;display:inline-block;padding:0 20px;padding-top:9px;transition:.3s}#search_addon:hover{background:#bb712c}#search_addon a{margin-left:-6px}#search_addon a i{color:#fff}.tick_box{max-height:56px;position:relative;left:0;top:0}.merchant_info_panel{margin:0}.merchant_info_panel_heading{background-color:#e28935!important;max-height:42px}.merchant_info_panel_heading .panel_head_text{position:relative;left:15px;bottom:4px;font-size:18px;font-weight:700;color:#fff}.merchant_info_panel_body{box-shadow:0 0 10px -1px rgba(0,0,0,.175);margin-right:1px;margin-left:1px;border-radius:0 0 5px 5px}.profile_picture_container{margin-top:8px;height:120px;width:120px;max-width:120px;max-height:120px;box-shadow:0 0 7px -1px rgba(0,0,0,.175)}.profile_picture{height:120px;width:120px;max-width:120px;max-height:120px;border-radius:5px}.panel-line-seperator{border-left:2px solid;color:#ebebeb;height:140px}.panel-line-seperator_mobile{border-left:2px solid;color:#ebebeb;height:110px;margin-top:10px}.shopname_text{position:relative;font-size:x-large;font-weight:700}.shopname_text_mobile{position:relative;font-size:large;font-weight:700;overflow-wrap:break-word}.location_text{position:relative;font-size:large}.location_text_mobile{position:relative;font-size:small;margin-top:10px}.location_header{color:#d8c12c}#info_sub_header{padding-top:5px;font-size:14px}#info_sub_header_mobile{font-size:14px}#share_header{margin-top:10px;font-size:14px}.total_product_text{font-size:large;font-weight:700}.rating_text_modal{font-size:large;font-weight:700;text-align:center}.penilaian_text{font-size:smaller}.etalase_mobile{border:1px solid;min-width:38px;min-height:30px;max-height:35px;margin-left:10px;border-radius:8%;border-color:#e28935}.etalase_bars{position:relative;left:20%;top:14%}.overlay{height:0%;width:100%;position:fixed;z-index:4;left:0;bottom:0;background-color:#fff;overflow:auto;white-space:nowrap;transition:.5s}.overlay-content{top:25%;width:100%;text-align:center;margin-top:30px}.overlay a{text-decoration:none;font-size:12px;color:#000;display:block;transition:.3s}.overlay a:focus,.overlay a:hover{color:#f1f1f1}.overlay .closebtn{position:absolute;right:15px;font-size:50px}@media screen and (max-height:450px){.overlay{overflow-y:auto}.overlay a{font-size:20px}.overlay .closebtn{font-size:40px;right:35px}}.overlay-content ul li:first-child{font-size:18px;font-weight:bolder}.sidebar-content{overflow-y:auto;white-space:nowrap;height:calc(39vh - 40px)}.sidebar-content .active{color:#e28935!important;font-weight:700!important;background:#f1f1f1}.sort_button{position:fixed;width:100px;height:30px;bottom:10%;right:5%;background-color:#fff;color:#e28935;border-radius:8%;text-align:center;box-shadow:2px 2px 3px #999;z-index:3;border:.5px solid}.sort_button b{font-size:medium}.store_info{background-color:#e28935;font-weight:700;color:#fff;transition:.3s;padding:8px 25px}.store_info:hover{background-color:#bb712c;border:1px solid #bb712c}.store_statistic{background-color:#e28935;font-weight:700;color:#fff;transition:.3s;padding:8px 25px}.store_statistic:hover{background-color:#bb712c;border:1px solid #bb712c}.etalase-title{margin-top:-15px;padding-left:15px;margin-bottom:5px}.etalase-title span{font-size:20px;font-weight:bolder}.modal-page .modal-title{font-size:24px;font-weight:700}.modal-page .modal-sub-title{font-size:small}.modal-page .modal-sub-content{font-size:large}.modal-page .modal-sub-content-description{font-size:medium}.modal-page .modal-title-row{padding-left:15px;color:#c3c4c6}.expedition_box{min-width:80px;min-height:80px;box-shadow:0 1px 4px 0 #999;padding:5px;border-radius:5px}.expedition_box_mobile{min-width:70px;min-height:70px;box-shadow:0 1px 4px 0 #999;padding:5px;border-radius:5px}#modal-expedition{max-width:fit-content;display:grid;grid-template-columns:auto auto auto auto;grid-gap:.5em}.expedition-image{max-width:100px;margin:auto;display:block}.expedition-image_mobile{max-width:60px;margin:auto;display:block}.container-ratings{max-width:fit-content;margin-left:auto;margin-right:auto}.per-rating-count{font-size:medium}@media (max-width:767px){.fix-sticky{margin-top:65px!important}}.lds-ring{left:48%;display:inline-block;position:relative;width:64px;height:64px}.lds-ring div{box-sizing:border-box;display:block;position:absolute;width:51px;height:51px;margin:6px;border:6px solid #e28935;border-radius:50%;animation:lds-ring .8s cubic-bezier(.5,0,.5,1) infinite;border-color:#e28935 transparent transparent transparent}.lds-ring div:nth-child(1){animation-delay:0s}.lds-ring div:nth-child(2){animation-delay:-80ms}.lds-ring div:nth-child(3){animation-delay:-.1s}@keyframes lds-ring{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}#next_button,#prev_button{height:35px;padding:0 14px;background-color:#fff;border-color:#ccc;border-radius:50%;font-size:20px;outline:0;transition:.3s}#next_button:hover,#prev_button:hover{border-color:#e28935;color:#e28935}.select-etalase{cursor:pointer}.jssocials-shares{font-size:17px}.jssocials-share{padding-right:8px;padding-top:2px}.jssocials-share-link{border-radius:50%;padding:.3em}a.jssocials-share-link{margin-left:0}a.jssocials-share-link *{width:23px}#shareRoundIcons{margin-top:8px}.modal-page .modal-content{border-radius:3px;border:none}.store_info,.store_statistic{border-radius:5px;border:1px solid #e28935}.product .product-title a:hover{color:#e28935!important}.product-item:hover .product-title a{color:#e28935!important}.sidebar.collection-leftsidebar{border-radius:5px}.collection-heading .fill_image img{border-radius:5px 5px 0 0}.panel-heading{border-top-right-radius:0}.list-cat li{padding:0!important;margin-bottom:5px}.list-cat li a{width:100%;display:inline-block;padding:7px 10px;border-radius:5px}.list-cat li a:hover{border-radius:5px;background:#f1f1f1;color:#e28935!important;font-weight:700!important}@media(max-width:767px){.input-search-mobile{padding-left:15px}}@media(max-width:414px){.input-search-mobile{padding-left:0}}.overlay-content li{padding:0}.overlay-content li a{display:inline-block;width:100%;padding:15px}.overlay-content li hr{margin:0}.overlay-content li .etalase_line_top{margin-top:15px}.overlay-content .active{background:#f1f1f1;color:#e28935;font-weight:700}
</style>
<input type="hidden" id="hidden-id" name="hidden-id" value="<?php echo $data->store_id ?>">
<?php if ($this->agent->is_mobile()) : ?>

    <section class="collection-heading heading-content ">
        <div class="container" style="padding: 0">
            <div class="row">
                <div class="fill_image_mobile">
                    <?php if ($data->banner == NULL || $data->banner == '') { ?>
                        <img src="<?php echo base_url('files/images/banner_merchant/default_banner.jpeg'); ?>">
                    <?php } else { ?>
                        <img src="<?php echo base_url('files/images/' . $data->banner); ?>">
                    <?php } ?>
                </div>
            </div>
            <div class="row">
                <div class="merchant_info_panel panel panel-default">
                    <div class="merchant_info_panel_heading panel-heading">
                        <img class="tick_box" src="<?php echo base_url('assets/frontend/images/centang_.png') ?>"><b class="panel_head_text">Official Brand</b>
                        <!-- <img class="tick_box" src="<?php echo base_url('assets/frontend/images/centang_02.png') ?>"><b class="panel_head_text">Merchant</b> -->
                    </div>
                    <div class="merchant_info_panel_body panel-body">
                        <div class="col-xs-5 col-sm-3">
                            <div class="profile_picture_container">
                                <?php if ($data->image == NULL || $data->image == '') { ?>
                                    <img class="profile_picture" src="<?php echo base_url('files/images/profilepicture_merchant/default_image.jpeg'); ?>">
                                <?php } else { ?>
                                    <img class="profile_picture" src="<?php echo base_url('files/images/' . $data->image); ?>">
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-xs-7 col-sm-3" style="margin-top: 5px;margin-bottom: 9px;">
                            <div class="row"><b class="shopname_text_mobile"><?= $data->name; ?></b></div>
                            <div class="row">
                                <div class="location_text_mobile">
                                    <?php if($data->city && $data->province) : ?>
                                        <i class="location_header fa fa-map-marker"></i> <?= $data->city_name; ?>, <?= $data->province_name; ?>
                                    <?php else : ?>
                                        <i class="location_header fa fa-map-marker"></i> Indonesia
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="row">
                                <input type="button" class="store_info" name="store_info" value="Informasi Toko" style="margin-top: 31px;padding-left: 11px;padding-right: 11px;font-size: 13px;padding: 8px 25px;">

                            </div>
                        </div>

                        <div class="col-xs-5 col-sm-2">
                            <div class="row">
                                <div id="info_sub_header">Jumlah Produk</div>
                                <div class="total_product_text"><?= $data->total_product; ?></div>
                            </div>
                            <div class="row">
                                <div id="share_header">Bagikan</div>
                            </div>
                            <div class="row">
                                <div id="shareRoundIcons"></div>
                            </div>
                        </div>
                        <div class="col-xs-1" style="padding: 0">
                            <div class="panel-line-seperator_mobile"></div>
                        </div>
                        <div class="col-xs-6 col-sm-3" style="padding: 0">
                            <div class="row">
                                <div id="info_sub_header">Rating Toko</div>
                                <div class="total_product_text"><?= $rating->rating; ?>
                                    <span class="spr-starrating spr-badge-starrating">
                                        <?php for ($i = 1; $i <= $rating->rating; $i++) { ?>
                                            <i class="spr-icon spr-icon-star"></i>
                                        <?php } ?>
                                        <?php for ($i = $rating->rating + 1; $i < 6; $i++) { ?>
                                            <i class="spr-icon spr-icon-star-empty"></i>
                                        <?php } ?>
                                    </span></div>
                                <div class="penilaian_text">(<?= $rating->count_rating; ?> Penilaian)</div>
                            </div>
                            <div class="row">
                                <input type="button" class="store_statistic" name="store_statistic" value="Statistik Toko" style="margin-top: 18px;font-size: 13px;padding: 8px 25px;">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
<?php else : ?>


    <section class="collection-heading heading-content" style="margin-top: 20px;">
        <div class="container" style="padding: 0">
            <div class="row">
                <div class="fill_image">
                    <?php if ($data->banner == NULL || $data->banner == '') { 
                            ?>
                        <img src="<?php echo base_url('files/images/banner_merchant/default_banner.jpeg'); 
                                    ?>">
                    <?php } else { 
                    ?>
                        <img src="<?php echo base_url('files/images/' . $data->banner); 
                                    ?>">
                    <?php } 
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="merchant_info_panel panel panel-default">

                    <div class="merchant_info_panel_heading panel-heading">
                        <img class="tick_box" src="<?php echo base_url('assets/frontend/images/centang_.png') ?>"><b class="panel_head_text">Official Brand</b>
                    </div>
                    <div class="merchant_info_panel_body panel-body">
                        <div class="col-xs-4 col-sm-3 col-md-2">
                            <div class="profile_picture_container">
                                <?php if ($data->image == NULL || $data->image == '') { ?>
                                    <img class="profile_picture" src="<?php echo base_url('files/images/profilepicture_merchant/default_image.jpeg'); ?>">
                                <?php } else { ?>
                                    <img class="profile_picture" src="<?php echo base_url('files/images/' . $data->image); ?>">
                                <?php } ?>
                                <!-- <img class="profile_picture" src="<?php echo base_url('files/images/' . $brand->image); ?>"> -->
                            </div>
                        </div>
                        <div class="col-xs-7 col-sm-3 col-md-5">
                            <div class="row"><b class="shopname_text"><?= $data->name; ?></b></div>
                            <div class="row">

                                <?php if($data->city && $data->province) : ?>
                                    <div class="location_text"><i class="location_header fa fa-map-marker"></i> 
                                        <?php echo $data->province_name . ', Indonesia' ?>
                                    </div>
                                <?php else : ?>
                                    <div class="location_text"><i class="location_header fa fa-map-marker"></i> 
                                        Indonesia
                                    </div>
                                <?php endif; ?>
                            </div>
                            <div class="row">
                                <input type="button" class="store_info" name="store_info" value="Informasi Toko" style="margin-top: 29px;">

                            </div>
                        </div>
                        <div class="col-xs-1 col-sm-1" style="padding: 0">
                            <div class="panel-line-seperator"></div>
                        </div>
                        <div class="col-xs-5 col-sm-2 col-md-2" style="padding: 0">
                            <div class="row">
                                <div id="info_sub_header">Jumlah Produk</div>
                                <div class="total_product_text"><?= $data->total_product; ?></div>
                            </div>
                            <div class="row">
                                <div id="share_header">Bagikan</div>
                            </div>
                            <div class="row">
                                <div id="shareRoundIcons"></div>
                            </div>
                        </div>
                        <div class="col-xs-5 col-sm-2 col-md-2" style="padding: 0">
                            <div class="row">
                                <div id="info_sub_header">Rating Toko</div>
                                <div class="total_product_text"><?= $rating->rating; ?>
                                    <span class="spr-starrating spr-badge-starrating">
                                        <?php for ($i = 1; $i <= $rating->rating; $i++) { ?>
                                            <i class="spr-icon spr-icon-star"></i>
                                        <?php } ?>
                                        <?php for ($i = $rating->rating + 1; $i < 6; $i++) { ?>
                                            <i class="spr-icon spr-icon-star-empty"></i>
                                        <?php } ?>
                                    </span></div>
                                <div class="penilaian_text">(<?= $rating->count_rating; ?> Penilaian)</div>
                            </div>
                            <div class="row">
                                <input type="button" class="store_statistic" name="store_statistic" value="Statistik Toko" style="margin-top: 23px;">
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

<?php endif; ?>
<form class="form-filter">
    <section class="collection-content">
        <?php if ($this->agent->is_mobile()) { ?>
            <div class="collection-wrapper" style="margin-right: 10px;">
            <?php } else { ?>
                <div class="collection-wrapper">
                <?php } ?>
                <div class="container">
                    <div class="row">
                        <div id="shopify-section-collection-template" class="shopify-section">
                            <div class="collection-inner">
                                <div id="tags-load" style="display:none;">
                                    <i class="fa fa-spinner fa-pulse fa-2x"></i>
                                </div>

                                <div id="collection">
                                    <div class="collection_inner">
                                        <?php if (!$this->agent->is_mobile()) : ?>
                                            <div class="collection-leftsidebar sidebar col-sm-3 clearfix">
                                                <div class="sidebar-block collection-block" style="padding-bottom: 28px;">
                                                    <div class="sidebar-title">
                                                        <span>Etalase</span>

                                                        <i class="fa fa-caret-down show_sidebar_content" aria-hidden="true"></i>

                                                    </div>

                                                    <div class="sidebar-content">
                                                        <ul class="list-cat">

                                                            <li><a class="select-etalase active" value="Semua Etalase">Semua Etalase (<?= $total_etalase; ?>)</a></li>
                                                            <li><a id="0" class="select-etalase" value="Tidak Dalam Etalase">Tidak Dalam Etalase (<?= $no_etalase; ?>)</a></li>
                                                            <?php
                                                            $etalases = $this->principal_home->get_etalase($data->store_id);
                                                            if ($etalases) {
                                                                foreach ($etalases->result() as $etalase) { ?>
                                                                    <li>
                                                                        <a id="<?= $etalase->id ?>" class="select-etalase" value="<?= $etalase->name ?>"><?php echo $etalase->name . ' (' . $etalase->count_total . ')' ?></a>
                                                                    </li>
                                                            <?php }
                                                            } ?>
                                                            <input type="hidden" name="etalase-choice" class="etalase-choice" <?php echo ($this->input->get('etalase-choice')) ? 'value=' . $this->input->get("etalase-choice") : ''; ?>>
                                                            <input type="hidden" name="etalase-label" class="etalase-label" <?php echo ($this->input->get('etalase-label')) ? 'value=' . $this->input->get("etalase-label") : ''; ?>>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <div class="collection-mainarea col-sm-8 col-lg-9 clearfix" style="padding-right: 0">
                                            <?php

                                            if ($this->agent->is_mobile()) : ?>
                                                <div class="collection_toolbar" style="padding: 0;margin-top: -5px; margin-bottom: 0px">
                                                    <div class="toolbar_right">
                                                        <div class="group_toolbar" style="margin-top: -20px;margin-bottom: 25px;">
                                                            <div class="input-group input-search-mobile">

                                                                <div style="margin-bottom: -6px;">
                                                                    <input type="text" name="search-specific-result" class="search_product form-control" size="65" placeholder="Cari disini.." aria-describedby="basic-addon1">
                                                                    <span class="input-group-addon" id="search_addon"><a id="search-specific-button"><i class="fa fa-search" style="color: white;"></i></a></span>
                                                                </div>

                                                            </div>
                                                            <div class="etalase_mobile" onclick="openNav()">
                                                                <i class="etalase_bars fa fa-th-large fa-2x" style="color:#E28935; "></i>
                                                            </div>
                                                            <div id="etalase_mobile_view" class="overlay">
                                                                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                                                                <div class="overlay-content">
                                                                    <ul>
                                                                        <li>Etalase
                                                                            <hr class="etalase_line_top" style="border-top-width:2px;border-color:#E28935; ">
                                                                        </li>
                                                                        <li><a class="select-etalase-mobile active" value="Semua Etalase">Semua Etalase (<?= $total_etalase; ?>)</a>
                                                                            <hr class="etalase_line">
                                                                        </li>
                                                                        <li><a id="0" class="select-etalase-mobile" value="Tidak Dalam Etalase">Tidak Dalam Etalase (<?= $no_etalase; ?>)</a>
                                                                            <hr class="etalase_line">
                                                                        </li>
                                                                        <?php
                                                                        $etalases2 = $this->principal_home->get_etalase($data->store_id);
                                                                        if ($etalases2) {
                                                                            foreach ($etalases2->result() as $etalase2) { ?>
                                                                                <li><a id="<?= $etalase2->id ?>" class="select-etalase-mobile" value="<?= $etalase2->name ?>"><?php echo $etalase2->name . ' (' . $etalase2->count_total . ')' ?></a>
                                                                                    <hr class="etalase_line">
                                                                                </li>
                                                                        <?php }
                                                                        } ?>
                                                                        <input type="hidden" name="etalase-choice" class="etalase-choice" <?php echo ($this->input->get('etalase-choice')) ? 'value=' . $this->input->get("etalase-choice") : ''; ?>>
                                                                        <input type="hidden" name="etalase-label" class="etalase-label" <?php echo ($this->input->get('etalase-label')) ? 'value=' . $this->input->get("etalase-label") : ''; ?>>
                                                                    </ul>

                                                                </div>
                                                            </div>
                                                            <a href="javascript:void(0)" class="sort_button" onclick="openNav_sort()"><b style="">Sort</b> <i class="fa fa-sort fa-2x" aria-hidden="true" style="position: relative; left: 10%;top: 10%;"></i></a>
                                                            <div id="sort_mobile_view" class="overlay">
                                                                <a href="javascript:void(0)" class="closebtn" onclick="closeNav_sort()">&times;</a>
                                                                <div class="overlay-content">

                                                                    <ul>
                                                                        <li>Urutkan
                                                                            <hr class="etalase_line_top" style="border-top-width:2px;border-color:#E28935; ">
                                                                        </li>
                                                                        <li><a id='popular' class="select-sort active" value="Terpopuler">Terpopuler</a>
                                                                            <hr class="etalase_line">
                                                                        </li>
                                                                        <li><a id='nameAZ' class="select-sort" value="Nama (A-Z)">Nama (A-Z)</a>
                                                                            <hr class="etalase_line">
                                                                        </li>
                                                                        <li><a id='nameZA' class="select-sort" value="Nama (Z-A)">Nama (Z-A)</a>
                                                                            <hr class="etalase_line">
                                                                        </li>
                                                                        <li><a id='lowprice' class="select-sort" value="Termurah">Termurah</a>
                                                                            <hr class="etalase_line">
                                                                        </li>
                                                                        <li><a id='highprice' class="select-sort" value="Termahal">Termahal</a>
                                                                            <hr class="etalase_line">
                                                                        </li>
                                                                        <li><a id='newest' class="select-sort" value="Terbaru">Terbaru</a>
                                                                            <hr class="etalase_line">
                                                                        </li>
                                                                        <?php if (($this->input->get('sort')) ? 'selected' : '') ?>
                                                                        <input type="hidden" name="sort" id="selected-sort" <?php echo ($this->input->get('sort')) ? 'value=' . $this->input->get("sort") : ''; ?>>
                                                                        <input type="hidden" name="sort-name" id="selected-sort-name" <?php echo ($this->input->get('sort-name')) ? 'value=' . $this->input->get("sort-name") : ''; ?>>
                                                                    </ul>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php else : ?>
                                                <div class="collection_toolbar" style="padding-left: 13px; padding-right: 0px; margin-top: -5px; margin-bottom: 10px;">
                                                    <div class="toolbar_right">
                                                        <div class="group_toolbar">
                                                            <div class="input-group" style="margin-top: 8px;">

                                                                <div style="margin-bottom: -6px;">
                                                                    <input type="text" name="search-specific-result" class="search_product form-control" size="200" placeholder="Cari disini.." aria-describedby="basic-addon1">
                                                                    <span class="input-group-addon" id="search_addon"><a id="search-specific-button"><i class="fa fa-search"></i></a></span>
                                                                </div>

                                                            </div>

                                                            <div class="sortBy">
                                                                <span class="toolbar_title">Urutkan:</span>
                                                                <div class="control-container">
                                                                    <select name="sort" tabindex="-1">
                                                                        <option value="popular" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'popular') ? 'selected' : ''; ?>>Terpopuler</option>
                                                                        <option value="nameAZ" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'nameAZ') ? 'selected' : ''; ?>>Nama (A-Z)</option>
                                                                        <option value="nameZA" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'nameZA') ? 'selected' : ''; ?>>Nama (Z-A)</option>
                                                                        <option value="lowprice" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'lowprice') ? 'selected' : ''; ?>>Termurah</option>
                                                                        <option value="highprice" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'highprice') ? 'selected' : ''; ?>>Termahal</option>
                                                                        <option value="newest" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'newest') ? 'selected' : ''; ?>>Terbaru</option>
                                                                    </select>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
</form>

<div class="collection-items clearfix">
    <?php if ($products->num_rows() > 0) : ?>
        <input type="hidden" id="hidden-start" name="hidden-start" value="<?php echo $start ?>">
        <div class="etalase-title"><span><?php echo $etalase_label; ?></span></div>
        <div class="products" style="margin-left: 5px;">
            <?php foreach ($products->result() as $key => $product) {
                if ($this->ion_auth->logged_in()) {
                    $id_customer = $this->data['user']->id;
                    $get_wishlist = $this->main->get('wishlist', ['product' => $product->product_id, 'branch_id' => $product->store_id, 'customer' => $id_customer]);
                    if ($get_wishlist) {
                        $product_wishlist = true;
                    } else {
                        $product_wishlist = false;
                    }
                } else {
                    $product_wishlist = false;
                }

                $sale_on = FALSE;

                if ($product->promo == 1) {
                    // if ($product->merchant == 0) {
                    //   $merchant = $this->session->userdata('list_merchant')[0];
                    //   $price_group = $this->main->get('product_price', array('product' => $product->product_id, 'merchant_group' => $merchant['group']));
                    //   if ($price_group) {
                    //     $product->price = $price_group->price;
                    //   }
                    // }
                    $promo_data = json_decode($product->promo_data, true);
                    if ($promo_data['type'] == 'P') {
                        $disc_price = round(($product->price * $promo_data['discount']) / 100);
                        $label_disc = $promo_data['discount'] . '% off';
                    } else {
                        $disc_price = $promo_data['discount'];
                        $label_disc = 'SALE';
                    }
                    $end_price = intval($product->price) - intval($disc_price);
                    $today = date('Y-m-d');
                    $today = date('Y-m-d', strtotime($today));
                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                    if (($today >= $DateBegin) && ($today <= $DateEnd)) {
                        $sale_on = TRUE;
                    } else {
                        $sale_on = false;
                    }
                }
            ?>
                <div class="product-item col-sm-3" style="border: none;">
                    <div class="product" onmouseover="change_store(<?= $key ?>, 'over')" onmouseout="change_store(<?= $key ?>, 'out')">
                        <?php if ($sale_on) : ?>
                            <span class="product-discount-label"><?php echo $label_disc; ?></span>
                        <?php endif; ?>
                        <?php if ($this->ion_auth->logged_in()) : ?>
                            <a href="#" class="btn-add-to-wishlist" style="<?= ($product_wishlist) ? 'color:#d9534f;' : 'color:#BBB'  ?>" onclick="add_wishlist(this)" data-product="<?= encode($product->product_id) ?>" data-store="<?= encode($product->store_id) ?>" data-customer="<?= encode($id_customer) ?>">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        <?php else : ?>
                            <a href="#" class="btn-add-to-wishlist" style="color:#BBB" data-toggle="modal" data-target="#modal-guest" data-back="<?= site_url() ?>">
                                <i class="fa fa-heart" aria-hidden="true"></i>
                            </a>
                        <?php endif; ?>

                        <div class="row-left">
                            <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="hoverBorder container_item" style="min-height: 180px;">
                                <img src="<?php echo base_url('assets/frontend/images/loading.gif') ?>" class="img-responsive lazyload" data-src="<?= ($product->image) ? site_url('files/images/' . $product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" alt="<?php echo $product->name; ?>">
                            </a>


                        </div>

                        <div class="garis-tengah"></div>
                        <div class="tombol-cart">
                            <?php if ($this->ion_auth->logged_in()) : ?>
                                <?php if ($this->main->gets('product_option', array('product' => $product->product_id))) : ?>
                                    <a href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>" class="icon-cart">
                                        <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                    </a>
                                <?php else : ?>
                                    <button data-id="<?php echo encode($product->product_id); ?>" data-storeid="<?= encode($product->store_id) ?>" onclick="addtoCart($(this).data('id'), $(this).data('storeid'))" class="btn-add-to-cart icon-cart">
                                        <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                    </button>
                                <?php endif; ?>
                            <?php else : ?>
                                <button type="button" class="icon-cart" data-toggle="modal" data-target="#modal-guest">
                                    <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 18px;margin-left: 3px;line-height: 38px;"></i>
                                </button>
                            <?php endif; ?>
                        </div>

                        <div class="row-right animMix">
                            <div class="grid-mode">
                                <div class="product-title">
                                    <a class="title-5" style="font-weight: bold; text-transform: uppercase;width: 75%;" href="<?php echo seo_url('catalog/products/view/' . $product->product_id . '/' . $product->store_id); ?>"><?php echo $product->name; ?></a>
                                </div>

                                <div class="product-price">
                                    <?php if ($sale_on) { ?>
                                        <span class="money" style="text-decoration: line-through;color:#8b8f8b"><?php echo rupiah($product->price); ?></span>
                                        <span class="money" style="color: #E28935"><?php echo rupiah($end_price); ?></span>
                                    <?php } else { ?>
                                        <span class="price_sale" style="color: #E28935"><span class="money"><?php echo rupiah($product->price); ?></span></span>
                                    <?php } ?>
                                </div>

                                <?php
                                $merchant = $this->main->get('merchants', ['id' => $product->store_id]);
                                $city = $this->main->get('cities', ['id' => $merchant->city]);
                                ?>
                                <div style="height: 15px;overflow: hidden;">
                                    <span id="text-ellipsis-city-<?= $key ?>" style="display: block;"><?= ($city->type == 'Kabupaten') ? 'Kab. ' . $city->name : $city->name ?></span>
                                    <span id="text-ellipsis-store-<?= $key ?>" style="display: block;"><?= $merchant->name ?></span>
                                </div>

                                <div class="rating-star">
                                    <span class="spr-badge" data-rating="0.0">
                                        <span class="spr-starrating spr-badge-starrating">
                                            <?php
                                            $rating = $this->principal_home->get_rating($product->product_id, $product->store_id);

                                            if (round($rating->rating) > 0) {
                                                for ($i = 1; $i <= round($rating->rating); $i++) {
                                            ?>
                                                    <i class="spr-icon spr-icon-star"></i>
                                                <?php
                                                }
                                                for ($i = round($rating->rating) + 1; $i <= 5; $i++) {
                                                ?>
                                                    <i class="spr-icon spr-icon-star-empty"></i>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </span>
                                    </span>
                                </div>
                                <div class="product-footer">
                                    &nbsp;
                                    <i class="fa fa-eye" aria-hidden="true"></i> <?= $product->viewed; ?>
                                </div>
                                <?php
                                $preorder = $product->preorder;
                                $free_ongkir = $product->free_ongkir;

                                if ($preorder == 1) {
                                    echo '<div class="pre-order-label">Preorder</div>';
                                }
                                if ($free_ongkir == 1) {
                                    echo '<div class="free-ongkir-label">Free Ongkir</div>';
                                } ?>

                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>


        <?php if ($next_button) { ?>
            <div class="collection-bottom-toolbar">
                <div class="product-pagination">
                    <div class="pagination_group">
                        <button id='next_button'><i class="fa fa-angle-right" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php else : ?>
        <!-- <p style="text-align: center;">Tidak ada produk.</p> -->
        <?php if(!$this->agent->is_mobile()) : ?>
            <div style="text-align: center;">
                <img src="<?php echo site_url('assets/frontend/images/404-notfound.jpeg') ?>">
                <p style="font-size: 20px;font-weight: bold;margin-top: 20px;">Tidak ada produk.</p>
            </div>
        <?php else : ?>
            <div style="text-align: center;">
                <img src="<?php echo site_url('assets/frontend/images/404-notfound.jpeg') ?>" style="width: 250px;">
                <p style="font-size: 20px;font-weight: bold;margin-top: 20px;margin-bottom: 70px;">Tidak ada produk.</p>
            </div>
        <?php endif; ?>
    <?php endif; ?>

</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>

<div id="modal_store_info" class="modal modal-page">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <b class="modal-title">Informasi Toko</h5>
            </div>

            <div class="modal-body">
                <div>
                    <div class="modal-title-row row">
                        <span class="modal-sub-title">Nama Toko</span>
                    </div>
                    <span class="modal-sub-content"><?= $data->name; ?></span>
                    <div class="modal-title-row row">
                        <span class="modal-sub-title">Deskripsi Toko</span>
                    </div>
                    <div class="modal-sub-content-description"><?= $data->description; ?></div>
                    <div class="modal-title-row row">
                        <span class="modal-sub-title">Alamat</span>
                    </div>
                    <div class="modal-sub-content-description">
                        <?php if($data->address != '' && $data->district && $data->city && $data->province) : ?>
                            <?= $data->address . ' ' . $data->district_name . ', ' . $data->city_type . ' ' . $data->city_name . ', ' . $data->province_name ?>
                        <?php else : ?>
                            Toko ini tidak memberikan alamatnya!
                        <?php endif; ?>
                    </div>
                    <div class="modal-title-row row">
                        <span class="modal-sub-title">Buka Sejak</span>
                    </div>
                    <span class="modal-sub-content"><?= date('d F Y', strtotime($data->date_added)); ?></span>
                </div>

            </div>
        </div>
    </div>
</div>
<div id="modal_store_stat" class="modal modal-page">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <b class="modal-title">Statistik Toko</h5>
            </div>

            <div class="modal-body">
                <div>
                    <div class="modal-title-row row">
                        <span class="modal-sub-title">Detail Rating Toko</span>
                    </div>
                    <div class="row" style="max-width: fit-content; margin-left: auto; margin-right: auto;">
                        <div class="rating_text_modal">Rating Akhir</div>
                        <div style="text-align: center;"><?= $rating_modal->rating; ?></div>
                        <span class="spr-starrating spr-badge-starrating">
                            <?php for ($i = 1; $i <= $rating_modal->rating; $i++) { ?>
                                <i class="spr-icon spr-icon-star"></i>
                            <?php } ?>
                            <?php for ($i = $rating_modal->rating + 1; $i < 6; $i++) { ?>
                                <i class="spr-icon spr-icon-star-empty"></i>
                            <?php } ?>
                        </span>
                        <div style="font-size: small; text-align: center;">(<?= $rating_modal->count_rating; ?> Penilaian)</div>
                    </div>
                    <div class="row" style="margin-top: 13px;">
                        <div class="col-lg-4">
                            <div class="rating_text_modal">
                                Kecepatan
                            </div>
                            <div class="container-ratings">
                                <span class="spr-starrating spr-badge-starrating">
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                        <i class="spr-icon spr-icon-star" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <b class="per-rating-count"><?= $speed_rating5; ?></b>


                                </span>
                                <br>
                                <span class="spr-starrating spr-badge-starrating">
                                    <?php for ($i = 1; $i <= 4; $i++) { ?>
                                        <i class="spr-icon spr-icon-star" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <i class="spr-icon spr-icon-star-empty" style="font-size: 50%;"></i>
                                    <b class="per-rating-count"><?= $speed_rating4; ?></b>

                                </span>
                                <br>
                                <span class="spr-starrating spr-badge-starrating">
                                    <?php for ($i = 1; $i <= 3; $i++) { ?>
                                        <i class="spr-icon spr-icon-star" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <?php for ($i = 1; $i <= 2; $i++) { ?>
                                        <i class="spr-icon spr-icon-star-empty" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <b class="per-rating-count"><?= $speed_rating3; ?></b>
                                </span>
                                <br>
                                <span class="spr-starrating spr-badge-starrating">
                                    <?php for ($i = 1; $i <= 2; $i++) { ?>
                                        <i class="spr-icon spr-icon-star" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <?php for ($i = 1; $i <= 3; $i++) { ?>
                                        <i class="spr-icon spr-icon-star-empty" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <b class="per-rating-count"><?= $speed_rating2; ?></b>
                                </span>
                                <br>
                                <span class="spr-starrating spr-badge-starrating">
                                    <i class="spr-icon spr-icon-star" style="font-size: 50%;"></i>
                                    <?php for ($i = 1; $i <= 4; $i++) { ?>
                                        <i class="spr-icon spr-icon-star-empty" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <b class="per-rating-count"><?= $speed_rating1; ?></b>

                                </span>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="rating_text_modal">
                                Servis
                            </div>
                            <div class="container-ratings">
                                <span class="spr-starrating spr-badge-starrating">
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                        <i class="spr-icon spr-icon-star" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <b class="per-rating-count"><?= $service_rating5; ?></b>


                                </span>
                                <br>
                                <span class="spr-starrating spr-badge-starrating">
                                    <?php for ($i = 1; $i <= 4; $i++) { ?>
                                        <i class="spr-icon spr-icon-star" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <i class="spr-icon spr-icon-star-empty" style="font-size: 50%;"></i>
                                    <b class="per-rating-count"><?= $service_rating4; ?></b>

                                </span>
                                <br>
                                <span class="spr-starrating spr-badge-starrating">
                                    <?php for ($i = 1; $i <= 3; $i++) { ?>
                                        <i class="spr-icon spr-icon-star" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <?php for ($i = 1; $i <= 2; $i++) { ?>
                                        <i class="spr-icon spr-icon-star-empty" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <b class="per-rating-count"><?= $service_rating3; ?></b>
                                </span>
                                <br>
                                <span class="spr-starrating spr-badge-starrating">
                                    <?php for ($i = 1; $i <= 2; $i++) { ?>
                                        <i class="spr-icon spr-icon-star" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <?php for ($i = 1; $i <= 3; $i++) { ?>
                                        <i class="spr-icon spr-icon-star-empty" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <b class="per-rating-count"><?= $service_rating2; ?></b>
                                </span>
                                <br>
                                <span class="spr-starrating spr-badge-starrating">
                                    <i class="spr-icon spr-icon-star" style="font-size: 50%;"></i>
                                    <?php for ($i = 1; $i <= 4; $i++) { ?>
                                        <i class="spr-icon spr-icon-star-empty" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <b class="per-rating-count"><?= $service_rating1; ?></b>

                                </span>
                            </div>


                        </div>
                        <div class="col-lg-4">
                            <div class="rating_text_modal">
                                Akurasi
                            </div>
                            <div class="container-ratings">
                                <span class="spr-starrating spr-badge-starrating">
                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                        <i class="spr-icon spr-icon-star" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <b class="per-rating-count"><?= $accuracy_rating5; ?></b>


                                </span>
                                <br>
                                <span class="spr-starrating spr-badge-starrating">
                                    <?php for ($i = 1; $i <= 4; $i++) { ?>
                                        <i class="spr-icon spr-icon-star" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <i class="spr-icon spr-icon-star-empty" style="font-size: 50%;"></i>
                                    <b class="per-rating-count"><?= $accuracy_rating4; ?></b>

                                </span>
                                <br>
                                <span class="spr-starrating spr-badge-starrating">
                                    <?php for ($i = 1; $i <= 3; $i++) { ?>
                                        <i class="spr-icon spr-icon-star" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <?php for ($i = 1; $i <= 2; $i++) { ?>
                                        <i class="spr-icon spr-icon-star-empty" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <b class="per-rating-count"><?= $accuracy_rating3; ?></b>
                                </span>
                                <br>
                                <span class="spr-starrating spr-badge-starrating">
                                    <?php for ($i = 1; $i <= 2; $i++) { ?>
                                        <i class="spr-icon spr-icon-star" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <?php for ($i = 1; $i <= 3; $i++) { ?>
                                        <i class="spr-icon spr-icon-star-empty" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <b class="per-rating-count"><?= $accuracy_rating2; ?></b>
                                </span>
                                <br>
                                <span class="spr-starrating spr-badge-starrating">
                                    <i class="spr-icon spr-icon-star" style="font-size: 50%;"></i>
                                    <?php for ($i = 1; $i <= 4; $i++) { ?>
                                        <i class="spr-icon spr-icon-star-empty" style="font-size: 50%;"></i>
                                    <?php } ?>
                                    <b class="per-rating-count"><?= $accuracy_rating1; ?></b>

                                </span>
                            </div>


                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>