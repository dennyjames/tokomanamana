<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Merchant extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Nama Toko', 'trim|required|max_length[40]|min_length[3]');
        $this->form_validation->set_rules('username', 'Domain Toko', 'trim|required|alpha_dash|max_length[20]|min_length[4]|is_unique[merchants.username]');
        $this->form_validation->set_rules('telephone', 'No. Telepon', 'trim|required|max_length[40]');
        $this->form_validation->set_rules('address', 'Alamat', 'trim|required|max_length[255]');

        $this->form_validation->set_message('is_unique', 'Domain sudah digunakan.');
        
        $this->data['message'] = '';
        
        if ($this->form_validation->run() == true) {
            $data = $this->input->post(null, true);
            $data['status'] = 0;
            $data['auth'] = $this->data['user']->id;
            $this->main->insert('merchants',$data);
            $this->ion_auth->add_to_group(3);
            $this->email();
            redirect(current_url());
        }else{
            $this->data['message'] = validation_errors();
        }

        $this->data['meta_description'] = 'Berjualan online sangat mudah di KarawangShop. Yuk, daftar dan jadilah penjual di KarawangShop.com. Situs jual-beli online produk UKM Termurah &amp; Terpercaya';
        $this->output->set_title('Jual Produkmu di KarawangShop');
        $this->template->_init();
        $this->load->view('merchant', $this->data);
    }
    
    private function email(){
        $this->load->library('email');
        $this->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_user' => 'rifkysyaripudin@gmail.com',
            'smtp_pass' => '121fkysyaripudin',
            'smtp_port' => 587,
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'mailtype' => 'html'
        ));
        
        $this->email->from('rifkysyaripudin@gmail.com', 'KarawangShop');
        $this->email->to('rifkysyaripudin@yahoo.com');
        $this->email->subject('Konfirmasi Pendaftaran UKM KarawangShop');
        $message = $this->load->view('email/register_merchant', $this->data, TRUE);
        $this->email->message($message);

        $this->email->send();
    }

}
