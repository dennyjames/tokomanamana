<style>
    .merchant-register-header{
        height: 400px;
        background: url(<?php echo site_url('assets/frontend/images/merchant/bg.jpg'); ?>) center bottom/cover no-repeat;
        color: #fff;
        padding-top: 70px;
        margin-bottom: 100px;
        text-align: center;
    }
    .merchant-register-header .title{
        font-size: 40px;
        font-weight: bold;
    }
    .merchant-register-header .subtitle{
        font-size: 30px;
    }
    .merchant-register-form .title{
        padding-bottom: 10px;
        text-align: center;
        font-size: 30px;
        color: #000;
        font-weight: 700;
    }
    .merchant-register-form .subtitle{
        padding-bottom: 10px;
        text-align: center;
        font-size: 16px;
        color: #94919f;
    }
</style>
<div class="main">
    <section class="merchant-register-header">
        <div class="title">Tingkatkan transaksi penjualan Online Anda <br>dengan Fasilitas Ekslusif dari KarawangShop</div>
        <div class="subtitle">Bergabunglah menjadi Penjual kami dan raih keuntungan bersama UKM lain.</div>
    </section>
    <section class="merchant-register-form">
        <div class="title">Tingkatkan Bisnis-mu Sekarang!</div>
        <div class="subtitle">Anda tidak dikenakan biaya apapun untuk berjualan di KarawangShop.com</div>
        <form action="" method="post" class="form-horizontal" id="form-shipping_payment">
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-group <?php echo (form_error('email')) ? 'has-error' : ''; ?>">
                        <label class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-6">
                            <input name="email" type="email" required class="form-control" value="<?php echo set_value('email', ''); ?>">
                            <?php if (form_error('email')) { ?>
                                <span class="help-block"><?php echo form_error('email'); ?></span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo (form_error('password')) ? 'has-error' : ''; ?>">
                        <label class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-6">
                            <input name="password" type="password" required class="form-control" value="<?php echo set_value('password', ''); ?>">
                            <?php if (form_error('password')) { ?>
                                <span class="help-block"><?php echo form_error('password'); ?></span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo (form_error('password')) ? 'has-error' : ''; ?>">
                        <label class="col-sm-3 control-label">Konfirmasi Password</label>
                        <div class="col-sm-6">
                            <input name="password" type="password" required class="form-control" value="<?php echo set_value('password', ''); ?>">
                            <?php if (form_error('password')) { ?>
                                <span class="help-block"><?php echo form_error('password'); ?></span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo (form_error('name')) ? 'has-error' : ''; ?>">
                        <label class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-6">
                            <input name="name" type="text" required class="form-control"  value="<?php echo set_value('name', ''); ?>">
                            <?php if (form_error('name')) { ?>
                                <span class="help-block"><?php echo form_error('name'); ?></span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo (form_error('phone')) ? 'has-error' : ''; ?>">
                        <label class="col-sm-3 control-label"><?php echo lang('checkout_shipping_field_phone'); ?></label>
                        <div class="col-sm-6">
                            <input name="phone" type="text" required class="form-control" placeholder="<?php echo lang('checkout_shipping_field_phone'); ?>" value="<?php echo set_value('phone', $profile->phone); ?>">
                            <?php if (form_error('phone')) { ?>
                                <span class="help-block"><?php echo form_error('phone'); ?></span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo (form_error('address')) ? 'has-error' : ''; ?>">
                        <label class="col-sm-3 control-label"><?php echo lang('checkout_shipping_field_address'); ?></label>
                        <div class="col-sm-6">
                            <textarea name="address" required class="form-control" placeholder="<?php echo lang('checkout_shipping_field_address'); ?>"><?php echo set_value('address', $profile->address); ?></textarea>
                            <?php if (form_error('address')) { ?>
                                <span class="help-block"><?php echo form_error('address'); ?></span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo (form_error('province')) ? 'has-error' : ''; ?>">
                        <label class="col-sm-3 control-label"><?php echo lang('checkout_shipping_field_province'); ?></label>
                        <div class="col-sm-6">
                            <select name="province" required id="province" class="form-control">
                                <option value="">Pilih provinsi</option>
                                <?php if ($provincies) { ?>
                                    <?php foreach ($provincies->result() as $province) { ?>
                                        <option value="<?php echo $province->id; ?>" <?php echo set_select('province', $province->id, ($province->id == $profile->province) ? TRUE : FALSE); ?>><?php echo $province->name; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <?php if (form_error('province')) { ?>
                                <span class="help-block"><?php echo form_error('province'); ?></span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo (form_error('city')) ? 'has-error' : ''; ?>">
                        <label class="col-sm-3 control-label"><?php echo lang('checkout_shipping_field_city'); ?></label>
                        <div class="col-sm-6">
                            <select name="city" required id="city" class="form-control">
                                <option value="">Pilih kota</option>
                                <?php if ($cities) { ?>
                                    <?php foreach ($cities->result() as $city) { ?>
                                        <option value="<?php echo $city->id; ?>" <?php echo set_select('city', $city->id, ($city->id == $profile->city) ? TRUE : FALSE); ?>><?php echo $city->name; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <?php if (form_error('city')) { ?>
                                <span class="help-block"><?php echo form_error('city'); ?></span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo (form_error('district')) ? 'has-error' : ''; ?>">
                        <label class="col-sm-3 control-label"><?php echo lang('checkout_shipping_field_district'); ?></label>
                        <div class="col-sm-6">
                            <select name="district" required id="district" class="form-control">
                                <option value="">Pilih kecamatan</option>
                                <?php if ($districts) { ?>
                                    <?php foreach ($districts->result() as $district) { ?>
                                        <option value="<?php echo $district->id; ?>" <?php echo set_select('district', $district->id, ($district->id == $profile->district) ? TRUE : FALSE); ?>><?php echo $district->name; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                            <?php if (form_error('district')) { ?>
                                <span class="help-block"><?php echo form_error('district'); ?></span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo (form_error('postcode')) ? 'has-error' : ''; ?>">
                        <label class="col-sm-3 control-label"><?php echo lang('checkout_shipping_field_postcode'); ?></label>
                        <div class="col-sm-6">
                            <input name="postcode" type="text" required maxlength="6" class="form-control" placeholder="<?php echo lang('checkout_shipping_field_postcode'); ?>" value="<?php echo set_value('postcode', $profile->postcode); ?>">
                            <?php if (form_error('postcode')) { ?>
                                <span class="help-block"><?php echo form_error('postcode'); ?></span>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="form-group <?php echo (form_error('save_profile')) ? 'has-error' : ''; ?>">
                        <div class="col-sm-offset-3 col-sm-6">
                            <div class="checkbox">
                                <label for="save_profile"><input name="save_profile" value="1" type="checkbox" <?php echo set_checkbox('save_profile', '1', FALSE); ?> id="save_profile">Simpan sebagai profil</label>
                            </div>
                        </div>
                    </div>
                    <div class="title-group" style="margin-bottom: 20px; padding-top: 10px;"><h2><?php echo lang('checkout_shipping_courrier_text'); ?></h2></div>
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-md-12" id="select-shipping">
                            <?php
                            if ($shippings) {
                                foreach ($shippings->result() as $shipping) {
                                    $weightBulat = weight_rounding($weight, 0.3);

                                    //promo potongan ongkir
                                    $cost = $shipping->cost * $weightBulat;
//                                        $costPromo = ($shipping->id == 1 || $shipping->id == 2) ? ($cost > 50000) ? $cost - 50000 : 0 : $cost;
//                                        $promo = ($shipping->id == 1 || $shipping->id == 2) ? ($cost > 50000) ? 50000 : $cost : 0;
                                    ?>
                                    <div class="col-md-4 cart-shipping-block">
                                        <div class="cart-shipping-detail">
                                            <input type="radio" value="<?php echo $shipping->id; ?>" onclick="selectShipping(this);" data-cost="<?php echo $cost; ?>" name="shipping" required="" id="shipping-<?php echo $shipping->id; ?>">           
                                            <label for="shipping-<?php echo $shipping->id; ?>">              
                                                <div class="cart-shipping-title"><?php echo $shipping->name; ?></div>          
                                                <div class="cart-shipping-subtitle"><?php echo $shipping->estimate; ?> hari kerja</div> 
                                                <div class="cart-shipping-content"><?php echo rupiah($cost); ?></div> 
                                                <div class="cart-shipping-subtitle"><?php echo number($shipping->cost); ?> x <?php echo weight_rounding($weight, 0.3); ?> kg</div>             
                                            </label>       
                                        </div>
                                    </div>
                                    <?php
                                }
                            } else {
                                ?>
                                <p>Lengkapi alamat terlebih dahulu.</p>
                            <?php } ?>
                        </div>
                        <?php echo (form_error('shipping')) ? form_error('shipping') : ''; ?>
                        <div class="col-md-12" style="margin-top: 15px">
                            Catatan pengiriman
                            <textarea class="form-control" name="note"><?php echo set_value('note'); ?></textarea>
                        </div>
                    </div>
                    <div class="title-group" style="margin-bottom: 20px; padding-top: 10px;"><h2><?php echo lang('checkout_payment_text'); ?></h2></div>
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-md-12" id="select-payment">
                            <div class="col-md-12 cart-shipping-block <?php echo ($this->input->post('payment') && $this->input->post('payment') == 1) ? 'cart-shipping-block-active' : ''; ?>">
                                <div class="cart-shipping-detail">
                                    <input type="radio" value="1" onclick="selectPayment(this);" name="payment" required="" id="payment-1" <?php echo set_radio('payment', '1'); ?>>           
                                    <label for="payment-1">              
                                        <div class="cart-shipping-title">Transfer Bank</div>          
                                        <div class="cart-shipping-subtitle">BRI, Mandiri, BCA</div>
                                    </label>       
                                </div>
                            </div>
                            <?php echo (form_error('payment')) ? form_error('payment') : ''; ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="block block-layered-nav">
                        <div class="block-content">
                            <h2><?php echo lang('cart_text_summary'); ?></h2>
                            <div class="cart-listing">
                                <?php
                                if ($products) {
                                    $total = 0;
                                    foreach ($products->result() as $product) {
                                        $subtotal = $product->price * $product->quantity;
                                        $total += $subtotal;
                                        ?>
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>">
                                                    <img src="<?php echo ($product->image) ? site_url($product->image) : site_url('assets/frontend/images/noimage.jpg'); ?>" class="img-responsive">
                                                </a>
                                            </div>
                                            <div class="media-body">
                                                <h4><a href="<?php echo seo_url('catalog/products/view/' . $product->id); ?>"><?php echo $product->name; ?></a></h4>
                                                <div class="mini-cart-price"><?php echo $product->quantity . ' x ' . number($product->price); ?></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                            <input type="hidden" id="total" value="<?php echo $total; ?>">
                            <div class="mini-cart-subtotal">Subtotal: <span class="price"><?php echo rupiah($total); ?></span></div>
                            <div class="mini-cart-subtotal">Biaya Pengiriman: <span class="price" id="shipping-cost"><?php echo rupiah(0); ?></span></div>
                            <!--<div class="mini-cart-subtotal">Potongan Biaya Pengiriman: <span class="price" id="shipping-cost-promo"><?php echo rupiah(0); ?></span></div>-->
                            <div class="mini-cart-subtotal">Total: <span class="price" id="grand-total"><?php echo rupiah($total); ?></span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-4"></div>
                <div class="col-sm-4">
                    <button class="btn btn-default btn-md btn-login fwb" type="submit">Selesai</button>
                </div>
            </div>
        </form>
    </section>
</div>