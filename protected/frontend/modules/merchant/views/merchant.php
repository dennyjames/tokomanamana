<style>
    .merchant-register-header{
        height: 400px;
        background: url(<?php echo site_url('assets/frontend/images/merchant/bg.jpg'); ?>) center bottom/cover no-repeat;
        color: #fff;
        padding-top: 70px;
        margin-bottom: 50px;
        text-align: center;
    }
    .merchant-register-header .title{
        font-size: 40px;
        font-weight: bold;
    }
    .merchant-register-header .subtitle{
        font-size: 30px;
    }
    .merchant-register-form{
        margin-bottom: 100px;
    }
    .merchant-register-form .title{
        padding-top: 10px;
        padding-bottom: 10px;
        text-align: center;
        font-size: 30px;
        color: #000;
        font-weight: 700;
    }
    .merchant-register-form .subtitle{
        padding-bottom: 10px;
        text-align: center;
        font-size: 16px;
        color: #94919f;
    }
</style>
<!--<div class="content-page">-->
<section class="merchant-register-header">
    <!--<div class="title">Tingkatkan transaksi penjualan Online Anda <br>dengan Fasilitas Ekslusif dari KarawangShop</div>-->
    <div class="title">Tempat Jual Beli Online Produk UKM Karawang!</div>
    <!--<div class="subtitle">Bergabunglah menjadi Penjual kami dan raih keuntungan bersama UKM Karawang lain.</div>-->
    <div class="subtitle">Yuk, kita raih keuntungan lebih penjualan online produk Anda bersama KarawangShop.</div>
    <!--<div class="subtitle">.</div>-->
</section>
<section class="merchant-register-form">
    <div class="title">Tingkatkan Bisnis-mu Sekarang!</div>
    <div class="subtitle">Anda tidak dikenakan biaya apapun untuk berjualan di KarawangShop.com</div>
    <?php
    if ($this->ion_auth->logged_in()) {
        if ($merchant = $this->main->get('merchants', array('auth' => $user->id))) {
            echo '<p class="text-center">Halo <b>' . $user->fullname . '</b>. ';
            if ($merchant->status == 1) {
                echo 'Anda sudah terdaftar sebagai penjual di KarawangShop. Ayo upload produkmu dan hasilkan peningkatan penjualan online bisnismu.</p>';
            } else {
                echo 'Anda sudah mengirimkan pendaftaran menjadi penjual. Selanjutnya kami akan menghubungi Anda segera.</p>';
            }
        } else {
            ?>
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1 box">
                    <!--<form action="" method="post" class="form-horizontal" id="form-shipping_payment">-->
                    <div class="form-my-account">
                        <form class="login-form block-login" method="POST">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h3>Informasi Toko</h3>
                                </div>
                                <div class="col-sm-12">
                                    <?php if ($message) { ?>
                                        <div class="alert alert-danger fade in alert-dismissable">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                            <?php echo $message; ?>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <p><input type="text" required placeholder="Nama Toko" name="name" value="<?php echo set_value('name', ''); ?>"></p>
                                    <p><input type="text" required placeholder="Domain Toko" name="username" value="<?php echo set_value('username', ''); ?>"><span class="help-block">karawangshop.com/<b>domaintokoanda</b></span></p>
                                    <p><input type="text" required placeholder="No. Telepon" name="telephone" value="<?php echo set_value('telephone', ''); ?>"></p>
                                    <p><textarea name="address" required placeholder="Alamat"><?php echo set_value('address', ''); ?></textarea></p>
                                    <p>
                                        <button type="submit" class="btn btn-default btn-login">Daftar</button>
                                    </p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <?php
        }
    } else {
        ?>
        <a href="<?php echo site_url('member/login?back=' . uri_string()); ?>" class="btn btn-default btn-md fwb" style="display: block; margin: 0 auto; width: 200px;">Daftar Jadi Penjual</a>
    <?php } ?>
</section>
<!--</div>-->