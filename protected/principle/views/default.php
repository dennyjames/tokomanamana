<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo "{$title}"; ?></title>
    <?php
    foreach ($css as $file) {
        echo "\n";
        echo '<link href="' . $file . '?timestamp=' . time() . '" rel="stylesheet" type="text/css" />';
    }
    echo "\n";
    ?>
    <script type="text/javascript">
        var base_url = '<?php echo base_url(); ?>';
        var site_url = '<?php echo site_url(); ?>';
        var current_url = '<?php echo current_url(); ?>';
        var decimal_digit = '<?php echo settings('number_of_decimal'); ?>';
        var decimal_separator = '<?php echo settings('number_separator_decimal'); ?>';
        var thousand_separator = '<?php echo settings('number_separator_thousand'); ?>';
        var datatable_data = {};
    </script>
</head>

<body class="<?php echo (isset($body_class)) ? $body_class : ''; ?>">
    <div class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo site_url(); ?>">
                <!-- <img src="<?php echo site_url('../assets/backend/images/logo_light.png'); ?>" alt=""> -->
                <?php echo settings('store_name'); ?>
            </a>

            <ul class="nav navbar-nav visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <span><?php echo $user->username; ?></span>
                        <i class="caret"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <!--                            <li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
                            <li><a href="#"><i class="icon-coins"></i> My balance</a></li>
                            <li><a href="#"><span class="badge bg-teal-400 pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>-->
                        <li><a href="<?php echo site_url('auth/logout'); ?>"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="page-container">
        <div class="page-content">
            <div class="sidebar sidebar-main">
                <div class="sidebar-content">
                    <div class="sidebar-category sidebar-category-visible">
                        <div class="category-content no-padding">
                            <ul class="navigation navigation-main navigation-accordion">

                                <li class="<?php echo menu_active($menu, 'dashboard'); ?>"><a href="<?php echo site_url(); ?>"><i class="icon-home4"></i> <span><?php echo lang('dashboard'); ?></span></a></li>
                                <?php if ($this->aauth->is_allowed('catalog')) { ?>
                                    <li class="<?php echo menu_active($menu, 'catalog'); ?>">
                                        <a href="#"><i class="icon-chart"></i> <span><?php echo lang('catalog'); ?></span></a>
                                        <ul>
                                            <?php if ($this->aauth->is_allowed('catalog/product')) { ?>
                                                <li class="<?php echo menu_active($menu, 'catalog_product'); ?>"><a href="<?php echo site_url('catalog/products'); ?>"><?php echo lang('catalog_product'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('catalog/brand')) { ?>
                                                <li class="<?php echo menu_active($menu, 'catalog_brand'); ?>"><a href="<?php echo site_url('catalog/brands'); ?>"><?php echo lang('catalog_brand'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('catalog/stock')) : ?>
                                                <li class="<?php echo menu_active($menu, 'catalog_stock'); ?>"><a href="<?php echo site_url('catalog/stock'); ?>"><?php echo lang('catalog_stock'); ?></a></li>
                                            <?php endif; ?>
                                            <?php if ($this->aauth->is_allowed('catalog/riwayat_stock')) : ?>
                                                <li class="<?php echo menu_active($menu, 'catalog_riwayat_stock'); ?>"><a href="<?php echo site_url('catalog/riwayat_stock'); ?>"><?php echo lang('catalog_stock_history'); ?></a></li>
                                            <?php endif; ?>
                                            <?php if ($this->aauth->is_allowed('catalog/refund_stock')) : ?>
                                                <li class="<?= menu_active($menu, 'catalog_refund_stock') ?>"><a href="<?= site_url('catalog/refund_stock') ?>"><?= lang('catalog_refund_stock') ?></a></li>
                                            <?php endif; ?>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($this->aauth->is_allowed('branch')) { ?>
                                    <li class="<?php echo menu_active($menu, 'branch'); ?>">
                                        <a href="<?php echo site_url('branches'); ?>"><i class="icon-archive"></i> <span><?php echo lang('branch'); ?></span></a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->aauth->is_allowed('etalase')) { ?>
                                    <li class="<?= menu_active($menu, 'etalase') ?>">
                                        <a href="<?= site_url('etalase') ?>"><i class="glyphicon glyphicon-tag"></i> <span>Etalase</span></a>
                                    </li>
                                <?php } ?>
                                <?php if ($this->aauth->is_allowed('design')) { ?>
                                    <li class="<?= menu_active($menu, 'design') ?>">
                                        <a href="#"><i class="glyphicon glyphicon-blackboard"></i> <span><?php echo lang('design') ?></span></a>
                                        <ul>
                                            <?php if($this->aauth->is_allowed('design/banner')) : ?>
                                                <li class="<?php echo menu_active($menu, 'design_banner') ?>">
                                                    <a href="<?php echo site_url('design/banner') ?>">
                                                        <span><?php echo lang('banner') ?></span>
                                                    </a>
                                                </li>
                                            <?php endif; ?>
                                            <?php if($this->aauth->is_allowed('design/profile_picture')) : ?>
                                                <li class="<?php echo menu_active($menu, 'design_profile_picture') ?>">
                                                    <a href="<?php echo site_url('design/profile_picture') ?>">
                                                        <span><?php echo lang('profile_picture') ?></span>
                                                    </a>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($this->aauth->is_allowed('report')) { ?>
                                    <li class="<?php echo menu_active($menu, 'report'); ?>">
                                        <a href="#"><i class="icon-chart"></i> <span><?php echo lang('report'); ?></span></a>
                                        <ul>
                                            <?php if ($this->aauth->is_allowed('report/sales')) { ?>
                                                <li class="<?php echo menu_active($menu, 'report_sale'); ?>"><a href="<?php echo site_url('reports/sales'); ?>"><?php echo lang('report_sales'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('report/balances')) { ?>
                                                <li class="<?php echo menu_active($menu, 'report_balance'); ?>"><a href="<?php echo site_url('reports/balances'); ?>"><?php echo lang('report_balances'); ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($this->aauth->is_allowed('setting')) { ?>
                                    <li class="<?php echo menu_active($menu, 'setting'); ?>">
                                        <a href="#"><i class="icon-cog4"></i> <span><?php echo lang('setting'); ?></span></a>
                                        <!-- <ul> -->
                                    <!--<li class="<?php // echo menu_active($menu, 'setting_payment_method');    
                                                    ?>"><a href="<?php // echo site_url('settings/payment_methods');    
                                                                    ?>"><?php // echo lang('payment_method');    
                                                                                ?></a></li>-->
                                    <!-- <?php if ($this->aauth->is_allowed('setting/user')) { ?>
                                                <li class="<?php echo menu_active($menu, 'setting_user'); ?>">
                                                    <a href="#"><?php echo lang('user'); ?></a>
                                                    <ul>
                                                        <li class="<?php echo menu_active($menu, 'setting_user_user'); ?>"><a href="<?php echo site_url('settings/users'); ?>"><?php echo lang('user'); ?></a></li>
                                                        <?php if ($this->aauth->is_allowed('setting/user/group')) { ?>
                                                            <li class="<?php echo menu_active($menu, 'setting_user_group'); ?>"><a href="<?php echo site_url('settings/users/groups'); ?>"><?php echo lang('user_group'); ?></a></li>
                                                        <?php } ?>
                                                        <?php if ($this->aauth->is_allowed('setting/user/permission')) { ?>
                                                            <li class="<?php echo menu_active($menu, 'setting_user_permission'); ?>"><a href="<?php echo site_url('settings/users/permissions'); ?>"><?php echo lang('user_permission'); ?></a></li>
                                                        <?php } ?>
                                                    </ul>
                                                </li>
                                            <?php } ?> -->
                                        <!-- </ul> -->
                                        <ul>
                                            <?php if($this->aauth->is_allowed('setting/profile')) : ?>
                                                <li class="<?php echo menu_active($menu, 'setting_base'); ?>"><a href="<?php echo site_url('settings'); ?>"><?php echo lang('setting'); ?></a></li>
                                            <?php endif; ?>
                                        </ul>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <?php echo $output; ?>
        </div>
    </div>
    <?php
    foreach ($js as $file) {
        echo "\n    ";
        echo '<script src="' . $file . '?timestamp=' . time() . '"></script>';
    }
    echo "\n";
    ?>
    <script src="<?php echo site_url('../assets/backend/js/idle.js'); ?>"></script>
    <script>
        var awayCallback = function() {
            //console.log(new Date().toTimeString() + ": away");
            swal({
                title: 'Anda sudah idle terlalu lama!',
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#EF5350",
                confirmButtonText: 'Login',
                cancelButtonText: lang.button.cancel,
                closeOnConfirm: false
            }, function() {
                window.location = "<?php echo site_url('auth/logout'); ?>";
            });
        };

        var awayBackCallback = function() {
            //console.log(new Date().toTimeString() + ": back");
        };
        var onVisibleCallback = function() {
            //console.log(new Date().toTimeString() + ": now looking at page");
        };

        var onHiddenCallback = function() {
            //console.log(new Date().toTimeString() + ": not looking at page");
        };
        //this is one way of using it.
        /*
        var idle = new Idle();
        idle.onAway = awayCallback;
        idle.onAwayBack = awayBackCallback;
        idle.setAwayTimeout(2000);
        idle.start();
        */
        //this is another way of using it
        var idle = new Idle({
            onHidden: onHiddenCallback,
            onVisible: onVisibleCallback,
            onAway: awayCallback,
            onAwayBack: awayBackCallback,
            awayTimeout: 7200000 //away with 2 hours of inactivity
        }).start();
    </script>
</body>

</html>