<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2>Daftar Etalase</h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <?php if ($this->aauth->is_allowed('etalase/add')) { ?>
                        <a href="<?php echo site_url('etalase/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span>Tambah Etalase</span></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table table-hover" id="table" data-url="<?php echo site_url('etalase/get_list'); ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="asc" style="width: 20%">Nama</th>
                        <th class="no-sort">Produk Dalam Etalase</th>
                        <th class="no-sort text-center" style="width: 20%;">Opsi</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>