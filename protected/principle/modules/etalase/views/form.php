<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?= ($data) ? 'Edit Etalase' : 'Tambah Etalase' ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('etalase'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span>Daftar Etalase</span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('etalase/save'); ?>" method="post" class="form-horizontal" id="form">
                    <input type="hidden" name="id" value="<?php echo ($data) ? encode($data->id) : ''; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Nama Etalase</label>
                                <div class="col-md-4">
                                    <input type="name" class="form-control" required="" name="name" value="<?php echo ($data) ? $data->name : ''; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('etalase'); ?>">Batal</a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?= ($data) ? 'Simpan' : 'Tambah' ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>