<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Etalase_model extends CI_Model {

	function get_all($start = 0, $length, $search = '', $order = []) {
        $this->_where_like($search);
        if($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('e.*')
                ->where('e.store_id',$this->data['user']->principle_id)
                ->where('e.store_type', 'principal')
                ->limit($length, $start);

        return $this->db->get('etalase e');

    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 1: $key = 'name';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->_where_like($search);
        $this->db->select('e.*')
                ->where('e.store_id',$this->data['user']->principle_id)
                ->where('e.store_type', 'principal');
        return $this->db->count_all_results('etalase e');
    }

    function get_product($id_etalase) {
    	return $this->db->query("SELECT name FROM products WHERE etalase = $id_etalase");
    }

    private function _where_like($search = '') {
        $columns = ['e.name', 'p.name'];
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    public function get_product_by_etalase($id) {
    	return $this->db->query("SELECT id, etalase FROM products WHERE etalase = $id");
    }

}