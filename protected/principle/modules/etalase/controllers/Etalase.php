<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Etalase extends CI_Controller {

	public function __construct() {
        parent::__construct();

        $this->aauth->control('etalase');

        $this->load->model('etalase_model', 'etalase');
        $this->data['menu'] = 'etalase';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();
        $this->load->js('../assets/principle/js/modules/etalase/etalase.js');

        $this->breadcrumbs->unshift('Etalase', '/');
        // $this->breadcrumbs->push(lang('product'), '/catalog/products');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title('Etalase Toko');
        $this->load->view('list', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->etalase->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
            	$products = $this->etalase->get_product($data->id);
            	$view_product = '';
            	foreach($products->result() as $product) {
            		$view_product .= '<li>' . $product->name . '</li>';
            	}
                $output['data'][] = array(
                    $data->name,
                    $view_product,
                    ($this->aauth->is_allowed('etalase/edit') ? '<a class="btn btn-primary" href="' . site_url('etalase/form/' . encode($data->id)) . '" style="margin-right: 7px;">Edit</a>' : '') . 
                    ($this->aauth->is_allowed('etalase/delete') ? '<button class="btn btn-danger" onclick="delete_etalase(' . $data->id . ')">Delete</button>' : '')
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->etalase->count_all();
        $output['recordsFiltered'] = $this->etalase->count_all($search);
        echo json_encode($output);
    }

    public function form($id = '') {
        $this->data['data'] = array();
        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->main->get('etalase', ['id' => $id]);
        }
        $this->breadcrumbs->unshift('Etalase', '/');
        if($id) {
        	$this->breadcrumbs->push('Edit Etalase', '/etalase/form' . $id);
        } else {
        	$this->breadcrumbs->push('Tambah Etalase', '/etalase/form');
        }
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->form();
        $this->output->set_title(($id) ? 'Edit Etalase' : 'Tambah Etalase');
        $this->load->view('form', $this->data);
    }

    public function save() {
    	$this->input->is_ajax_request() or exit('No direct post submit allowed!');
    	$this->load->library('form_validation');

    	$this->form_validation->set_rules('name', 'Nama Etalase', 'trim|required');
    	if($this->form_validation->run() === true) {
    		$id = $this->input->post('id');
    		$name = $this->input->post('name', true);
    		$principle_id = $this->data['user']->principle_id;
	    	if($id) {
	    		$id = decode($id);
	    		$this->main->update('etalase', ['name' => $name], ['id' => $id, 'store_id' => $principle_id, 'store_type' => 'principal']);
	    		$return = [
	    			'status' => 'success',
	    			'message' => 'Etalase Berhasil Disimpan!',
	    			'redirect' => site_url('etalase')
	    		];
	    	} else {
	    		$data = [
	    			'name' => $name,
	    			'store_type' => 'principal',
	    			'store_id' => $this->data['user']->principle_id
	    		];
	    		$this->main->insert('etalase', $data);

	    		$return = [
	    			'status' => 'success',
	    			'message' => 'Etalase Berhasil Ditambah!',
	    			'redirect' => site_url('etalase')
	    		];
	    	}
    	} else {
    		$return = [
    			'status' => 'error',
    			'message' => validation_errors()
    		];
    	}

    	echo json_encode($return);
    }

    public function delete() {
    	$this->input->is_ajax_request() or exit('No direct post submit allowed!');

    	$id = $this->input->post('id');

    	// $id = decode($id);

    	$delete = $this->main->delete('etalase', ['id' => $id, 'store_type' => 'principal', 'store_id' => $this->data['user']->principle_id]);

    	if($delete) {
    		$products = $this->etalase->get_product_by_etalase($id)->result();
    		if($products) {
    			foreach($products as $product) {
    				$this->main->update('products', ['etalase' => null], ['id' => $product->id]);
    			}
    		}
    		$return = [
	    		'message' => 'Etalase Berhasil Dihapus!',
	    		'status' => 'success'
	    	];
    	} else {
    		$return = [
    			'message' => 'Etalase Gagal Dihapus!',
    			'status' => 'error'
    		];
    	}

    	echo json_encode($return);
    }

}