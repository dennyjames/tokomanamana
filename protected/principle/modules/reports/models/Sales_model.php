<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_model extends CI_Model {

    function daily($from, $to) {
        $status = settings('order_finish_status');
        $this->db->select('COUNT(o.id) transaction, SUM(o.total) sales, DATE(o.date_added) date, toi.total invoice, top.total product')
                ->join('order_invoice oi', 'oi.order = o.id', 'left')
                ->join('(SELECT COUNT(id) total, DATE(date_added) date FROM order_invoice 
                        WHERE order_status = ' . $status . ' AND DATE(date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'
                        GROUP BY DATE(date_added)) toi', 'toi.date = DATE(o.date_added)', 'left')
                ->join('(SELECT SUM(op.quantity) total, DATE(oi.date_added) date FROM order_product op
                        LEFT JOIN order_invoice oi ON oi.id = op.invoice
                        WHERE oi.order_status = ' . $status . ' AND DATE(oi.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'
                        GROUP BY DATE(oi.date_added)) top', 'top.date = DATE(o.date_added)', 'left')
                ->where('oi.order_status', $status)
                ->where('DATE(o.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'')
                ->group_by('DATE(o.date_added)')
                ->order_by('date DESC');
        return $this->db->get('orders o');
    }

    function monthly($year) {
        $status = settings('order_finish_status');
        $this->db->select('COUNT(id) invoice, SUM(oi.total) sales, MONTH(oi.date_added) month, o.total transaction')
                ->join('(SELECT COUNT(o.id) total, MONTH(o.date_added) month FROM orders o
                        LEFT JOIN order_invoice oi ON oi.order = o.id
                        WHERE YEAR(o.date_added) = ' . $year . '
                        AND oi.order_status = ' . $status . '
                        GROUP BY MONTH(o.date_added)) o', 'o.month = MONTH(oi.date_added)', 'left')
                ->where('oi.order_status', $status)
                ->where('YEAR(date_added)',$year)
                ->group_by('MONTH(date_added)')
                ->order_by('month ASC');
        return $this->db->get('order_invoice oi');
    }

    function product($from, $to) {
        $status = settings('order_finish_status');
        $this->db->select('op.name, op.code, SUM(op.quantity) quantity, SUM(op.total) sales, opi.total invoice, opo.total transaction')
                ->join('order_invoice oi', 'oi.id = op.invoice', 'left')
                ->join('(SELECT COUNT(invoice) total, op.product FROM order_product op
                        LEFT JOIN order_invoice oi ON op.invoice = oi.id 
                        WHERE oi.order_status = 7 AND DATE(oi.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'
                        GROUP BY op.product) opi', 'opi.product = op.product', 'left')
                ->join('(SELECT COUNT(op.order) total, op.product FROM order_product op
                        LEFT JOIN order_invoice oi ON oi.id = op.invoice
                        WHERE oi.order_status = ' . $status . ' AND DATE(oi.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'
                        GROUP BY op.product) opo', 'opo.product = op.product', 'left')
                ->where('oi.order_status', $status)
                ->where('DATE(oi.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'')
                ->group_by('op.product');
        return $this->db->get('order_product op');
    }

}
