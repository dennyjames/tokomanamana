<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_model extends CI_Model {

    // function principle_sales($start = 0, $length, $order = array(),$principle_id, $from, $to, $brands = '', $zone = '', $city = '') {
    //     $this->db->select('op.id,oi.code as invoice_code,o.date_added date,m.name as merchant_name,cst.name customer,p.name as product_name,op.quantity as qty,op.total as total_price,p.brand as brand_id,b.name as brand_name,mg.name as merchant_group')
    //             ->join('orders_merchant o', 'o.id = op.order', 'left')
    //             ->join('merchants cst', 'cst.id = o.customer', 'left')
    //             ->join('order_invoice_merchant oi', 'oi.id = op.invoice', 'left')
    //             ->join('products p', 'p.id = op.product', 'left')
    //             ->join('brands b', 'b.id = p.brand', 'left')
    //             ->join('merchants m', 'm.id = oi.merchant', 'left')
    //             ->join('merchant_groups mg', 'mg.id = m.group', 'left')
    //             ->where('DATE(o.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'')
    //             ->limit($length, $start);

    //     $this->db->where('o.payment_status', 'Paid');
    //     $this->db->where('b.principle_id', $principle_id);
    //     // if ($order_status)
    //     //     $this->db->where('oi.order_status', $order_status);
    //     // if ($payment_status)
    //     //     $this->db->where('o.payment_status', $payment_status);
    //     // if ($shipping)
    //     //     $this->db->like('oi.shipping_courier', $shipping);
    //     if ($zone)
    //         $this->db->where('m.group', $zone);
    //     if ($city) {
    //         $city = array_filter(explode(',', $city));
    //         $this->db->where_in('o.shipping_city', $city);
    //     }
    //     if ($order) {
    //         $col = $order['column'];
    //         switch ($order['column']) {
    //             case 0: $col = 'date';
    //                 break;
    //         }
    //         $this->db->order_by($col, $order['dir']);
    //     }
    //     return $this->db->get('order_product_merchant op');
    // }
    // function principle_sales_count_filter($principle_id,$from, $to, $brands = '', $zone = '', $city = '') {
    //     $this->db->join('orders_merchant o', 'o.id = op.order', 'left')
    //             ->join('order_invoice_merchant oi', 'oi.id = op.invoice', 'left')
    //             ->join('merchants m', 'm.id = oi.merchant', 'left')
    //             ->join('merchant_groups mg', 'mg.id = m.group', 'left')
    //             ->join('products p', 'p.id = op.product', 'left')
    //             ->join('brands b', 'b.id = p.brand', 'left')
    //             ->where('DATE(o.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'');

    //         $this->db->where('o.payment_status', 'Paid');
    //         $this->db->where('b.principle_id', $principle_id);
    //     if ($zone)
    //         $this->db->where('m.group', $zone);
    //     if ($city) {
    //         $city = array_filter(explode(',', $city));
    //         $this->db->where_in('o.shipping_city', $city);
    //     }
    //     return $this->db->count_all_results('order_product_merchant op');
    // }

    // function principle_sales_count($principle_id) {
    //     $this->db->join('orders_merchant o', 'o.id = op.order', 'left')
    //             ->join('order_invoice_merchant oi', 'oi.id = op.invoice', 'left')
    //             ->join('merchants m', 'm.id = oi.merchant', 'left')
    //             ->join('merchant_groups mg', 'mg.id = m.group', 'left')
    //             ->join('products p', 'p.id = op.product', 'left')
    //             ->join('brands b', 'b.id = p.brand', 'left');

    //         $this->db->where('o.payment_status', 'Paid');
    //         $this->db->where('b.principle_id', $principle_id);
    //     return $this->db->count_all_results('order_product_merchant op');
    // }

    function principle_sales($start = 0, $length, $order = array(),$principle_id, $from, $to, $brands = '', $branch = '', $shipping_courier = '') {
        $this->db->select('oi.code, oi.date_added AS date, m.name AS merchant_name, sos.name AS order_status, c.fullname, b.name AS brand_name, oi.shipping_courier, oi.total AS total_price')
                ->join('merchants m', 'm.id = oi.merchant', 'left')
                ->join('principles p', 'p.id = m.principal_id', 'left')
                ->join('brands b', 'b.principle_id = p.id', 'left')
                ->join('customers c', 'c.id = oi.customer', 'left')
                ->join('setting_order_status sos', 'sos.id = oi.order_status', 'left')
                ->where('DATE(oi.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'')
                ->where('oi.order_status', 7)
                ->limit($length, $start);
        $this->db->where('m.principal_id', $principle_id);
        // if ($order_status)
        //     $this->db->where('oi.order_status', $order_status);
        // if ($payment_status)
        //     $this->db->where('o.payment_status', $payment_status);
        // if ($shipping)
        //     $this->db->like('oi.shipping_courier', $shipping);
        // if ($brands) {
        //     $city = array_filter(explode(',', $city));
        //     $this->db->where_in('o.shipping_city', $city);
        // }
        if($brands) {
            $this->db->where('b.id', $brands);
        }
        if($branch){
            $this->db->where('m.id', $branch);
        }
        if($shipping_courier) {
            $this->db->like('oi.shipping_courier', $shipping_courier);
        }
        if ($order) {
            $col = $order['column'];
            switch ($order['column']) {
                case 0: $col = 'oi.date_added';
                    break;
            }
            $this->db->order_by($col, $order['dir']);
        }
        return $this->db->get('order_invoice oi');
    }
    function principle_sales_count_filter($principle_id,$from, $to, $brands = '', $branch = '', $shipping_courier = '') {
        $this->db->join('merchants m', 'm.id = oi.merchant', 'left')
                ->join('principles p', 'p.id = m.principal_id', 'left')
                ->join('brands b', 'b.principle_id = p.id', 'left')
                ->join('customers c', 'c.id = oi.customer', 'left')
                ->join('setting_order_status sos', 'sos.id = oi.order_status', 'left')
                ->where('oi.order_status', 7)
                ->where('DATE(oi.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'');

        $this->db->where('m.principal_id', $principle_id);
        if($brands) {
            $this->db->where('b.id', $brands);
        }
        if($branch){
            $this->db->where('m.id', $branch);
        }
        if($shipping_courier) {
            $this->db->like('oi.shipping_courier', $shipping_courier);
        }
        return $this->db->count_all_results('order_invoice oi');
    }

    function principle_sales_count($principle_id) {
        $this->db->join('merchants m', 'm.id = oi.merchant', 'left')
                ->join('principles p', 'p.id = m.principal_id', 'left')
                ->join('brands b', 'b.principle_id = p.id', 'left')
                ->join('customers c', 'c.id = oi.customer', 'left')
                ->join('setting_order_status sos', 'sos.id = oi.order_status', 'left')
                ->where('oi.order_status', 7);

        $this->db->where('m.principal_id', $principle_id);
        
        return $this->db->count_all_results('order_invoice oi');
    }

    function sales_merchant($from, $to, $start = 0, $length, $order = array(), $zone = '') {
        $this->db->select('m.id, m.name, mg.name merchant_group, m.type, oi.*, pm.product_tanaka, p.product_other')
                ->join('merchant_groups mg', 'm.group = mg.id', 'left')
                ->join('(SELECT 
                        oi.merchant,
                        SUM(CASE WHEN order_status IN (1,2) THEN 1 ELSE 0 END) pending_payment,
                        SUM(CASE WHEN order_status = 3 THEN 1 ELSE 0 END) pending_process,
                        SUM(CASE WHEN order_status = 4 THEN 1 ELSE 0 END) on_process,
                        SUM(CASE WHEN order_status IN (5, 6, 10, 11) THEN 1 ELSE 0 END) on_shipment,
                        SUM(CASE WHEN order_status = 7 THEN 1 ELSE 0 END) finish,
                        SUM(CASE WHEN order_status = 8 THEN 1 ELSE 0 END) cancel,
                        SUM(CASE WHEN order_status = 9 THEN 1 ELSE 0 END) reject
                        FROM order_invoice oi
                        WHERE DATE(oi.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'
                        GROUP BY oi.merchant) oi', 'oi.merchant = m.id', 'left')
                ->join('(SELECT merchant, SUM(quantity) product_tanaka FROM product_merchant GROUP BY merchant) pm', 'pm.merchant = m.id', 'left')
                ->join('(SELECT merchant, SUM(quantity) product_other FROM products WHERE merchant != 0 GROUP BY merchant) p', 'p.merchant = m.id', 'left')
                ->limit($length, $start);
        if($zone){
            $this->db->where('m.group',$zone);
        }
        if ($order) {
            $col = $order['column'];
            switch ($order['column']) {
                default:
                    $col = 'm.name';
                    break;
                case 1: $col = 'mg.name';
                    break;
                case 2: $col = 'product_tanaka';
                    break;
                case 3: $col = 'product_other';
                    break;
                case 4: $col = 'pending_payment';
                    break;
                case 5: $col = 'pending_process';
                    break;
                case 6: $col = 'on_process';
                    break;
                case 7: $col = 'on_shipment';
                    break;
                case 8: $col = 'finish';
                    break;
                case 9: $col = 'cancel';
                    break;
                case 10: $col = 'reject';
                    break;
            }
            $this->db->order_by($col, $order['dir']);
        }
        return $this->db->get('merchants m');
    }

    function sales_export($principal, $from, $to, $brands = '', $shipping = '', $branch = '') {
        $this->db->select('o.id order, oi.id, oi.code invoice, o.code, cst.fullname customer, oi.total, oi.shipping_courier,m.id as merchant_id,m.name as merchant_name, o.date_added date, oi.order_status status_code, sos.name order_status, oi.subtotal, oi.tracking_number, oi.shipping_cost')
                ->join('orders o', 'o.id = oi.order', 'left')
                ->join('customers cst', 'cst.id = o.customer', 'left')
                ->join('setting_order_status sos', 'sos.id = oi.order_status', 'left')
                ->join('merchants m', 'm.id = oi.merchant', 'left')
                ->join('brands b', 'b.principle_id = m.principal_id', 'left')
                ->where_in('m.principal_id', $principal)
                ->where('o.payment_status', 'PAID')
                ->where('DATE(o.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'');
        if ($brands)
            $this->db->where('oi.order_status', $order_status);
        if ($shipping)
            $this->db->like('oi.shipping_courier', $shipping);
        if($branch) 
            $this->db->where('m.id', $branch);
        return $this->db->get('order_invoice oi');
    }

    function get_coupon($order) {
        $query = $this->db->select('coupon_history.amount, coupons.code, coupons.type')
                        ->join('coupons', 'coupon_history.coupon = coupons.id', 'left')
                        ->where('coupon_history.order', $order)
                        ->get('coupon_history');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

//
//    function monthly($year) {
//        $status = settings('order_finish_status');
//        $this->db->select('COUNT(id) invoice, SUM(oi.total) sales, MONTH(oi.date_added) month, o.total transaction')
//                ->join('(SELECT COUNT(o.id) total, MONTH(o.date_added) month FROM orders o
//                        LEFT JOIN order_invoice oi ON oi.order = o.id
//                        WHERE YEAR(o.date_added) = ' . $year . '
//                        AND oi.order_status = ' . $status . '
//                        GROUP BY MONTH(o.date_added)) o', 'o.month = MONTH(oi.date_added)', 'left')
//                ->where('oi.order_status', $status)
//                ->where('YEAR(date_added)',$year)
//                ->group_by('MONTH(date_added)')
//                ->order_by('month ASC');
//        return $this->db->get('order_invoice oi');
//    }
//
//    function product($from, $to) {
//        $status = settings('order_finish_status');
//        $this->db->select('op.name, op.code, SUM(op.quantity) quantity, SUM(op.total) sales, opi.total invoice, opo.total transaction')
//                ->join('order_invoice oi', 'oi.id = op.invoice', 'left')
//                ->join('(SELECT COUNT(invoice) total, op.product FROM order_product op
//                        LEFT JOIN order_invoice oi ON op.invoice = oi.id 
//                        WHERE oi.order_status = 7 AND DATE(oi.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'
//                        GROUP BY op.product) opi', 'opi.product = op.product', 'left')
//                ->join('(SELECT COUNT(op.order) total, op.product FROM order_product op
//                        LEFT JOIN order_invoice oi ON oi.id = op.invoice
//                        WHERE oi.order_status = ' . $status . ' AND DATE(oi.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'
//                        GROUP BY op.product) opo', 'opo.product = op.product', 'left')
//                ->where('oi.order_status', $status)
//                ->where('DATE(oi.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'')
//                ->group_by('op.product');
//        return $this->db->get('order_product op');
//    }
}
