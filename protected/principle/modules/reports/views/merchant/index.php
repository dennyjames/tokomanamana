<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form id="filter-form">
                    <div class="row">
                        <div class="col-xs-6 col-sm-2">
                            <div class="form-group">
                                <label class="control-label">Filter By:</label>
                                <select id="filter-by" class="form-control">
                                    <option value="today">Hari Ini</option>
                                    <option value="yesterday">Kemarin</option>
                                    <option value="this month">Bulan Ini</option>
                                    <option value="last month">Bulan Lalu</option>
                                    <option value="this year">Tahun Ini</option>
                                    <option value="99">Kostum</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-2">
                            <div class="form-group">
                                <label class="control-label">Dari:</label>
                                <input type="text" class="form-control" readonly="" name="from" value="<?php echo date('Y-m-d'); ?>">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-2">
                            <div class="form-group">
                                <label class="control-label">Sampai:</label>
                                <input type="text" class="form-control" readonly="" name="to" value="<?php echo date('Y-m-d'); ?>">
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Zona:</label>
                                <select name="zone" class="form-control">
                                    <option value="">Semua</option>
                                    <?php if ($groups = $this->main->gets('merchant_groups', array(), 'name asc')) foreach ($groups->result() as $group) { ?>
                                            <option value="<?php echo $group->id; ?>"><?php echo $group->name; ?></option>
                                        <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <br>
                            <button class="btn btn-default" type="button" id="filter">Filter</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="panel panel-flat table-responsive">
            <table class="table table-hover" id="table-report" data-url="<?php echo site_url('reports/merchant/data'); ?>">
                <thead>
                    <tr>
                        <th rowspan="2" class="default-sort" data-sort="asc"><?php echo lang('name'); ?></th>
                        <th rowspan="2"><?php echo lang('zone'); ?></th>
                        <th colspan="2" class="text-center"><?php echo lang('stock'); ?></th>
                        <th colspan="6" class="text-center"><?php echo lang('transaction'); ?></th>
                    </tr>
                    <tr>
                        <th><?php echo lang('product_tanaka'); ?></th>
                        <th><?php echo lang('product_other'); ?></th>
                        <th><?php echo lang('pending_payment'); ?></th>
                        <th><?php echo lang('pending_process'); ?></th>
                        <th><?php echo lang('on_process'); ?></th>
                        <th><?php echo lang('on_shipment'); ?></th>
                        <th><?php echo lang('finish'); ?></th>
                        <th><?php echo lang('cancel'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>