<div class="sidebar-detached">
    <div class="sidebar sidebar-default">
        <div class="sidebar-content">
            <div class="sidebar-category">
                <div class="category-content no-padding" style="display: block;">
                    <ul class="navigation navigation-alt navigation-accordion no-padding-bottom">
                        <li><a href="<?php echo site_url('reports/sales/daily'); ?>"><?php echo lang('daily'); ?></a></li>
                        <li><a href="<?php echo site_url('reports/sales/monthly'); ?>"><?php echo lang('monthly'); ?></a></li>
                        <li><a href="<?php echo site_url('reports/sales/product'); ?>"><?php echo lang('product'); ?></a></li>
                        <li><a href="<?php echo site_url('reports/sales/city'); ?>"><?php echo lang('city'); ?></a></li>
                        <li><a href="<?php echo site_url('reports/sales/zone'); ?>"><?php echo lang('zone'); ?></a></li>
                        <li><a href="<?php echo site_url('reports/sales/merchant'); ?>"><?php echo lang('merchant'); ?></a></li>
                        <li><a href="<?php echo site_url('reports/sales/branch'); ?>"><?php echo lang('branch'); ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>