<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row">
                    <form id="filter-form" action="<?php echo site_url('reports/sales/data'); ?>" method="post">
                        <div class="col-sm-5">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Filter By:</label>
                                        <select id="filter-by" class="form-control">
                                            <option value="today">Hari Ini</option>
                                            <option value="yesterday">Kemarin</option>
                                            <option value="this month">Bulan Ini</option>
                                            <option value="last month">Bulan Lalu</option>
                                            <option value="this year">Tahun Ini</option>
                                            <option value="99">Kostum</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Dari:</label>
                                        <input type="text" class="form-control" readonly="" name="from" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 no-padding-right">
                                    <div class="form-group">
                                        <label class="control-label">Sampai:</label>
                                        <input type="text" class="form-control" readonly="" name="to" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="col-xs-6 col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Status Transaksi:</label>
                                    <select name="order_status" class="form-control">
                                        <option value="">Semua</option>
                                        <option value="1">Pesanan Baru</option>
                                        <option value="3">Sudah Dibayar</option>
                                        <option value="4">Sedang Diproses</option>
                                        <option value="5">Sudah Dikirim</option>
                                        <option value="6">Sampai Ditujuan</option>
                                        <option value="7">Selesai</option>
                                        <option value="8">Dibatalkan</option>
                                        <option value="9">Ditolak Penjual</option>
                                        <option value="10">Siap Pickup</option>
                                        <option value="11">Pickup Selesai</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Status Pembayaran:</label>
                                    <select name="payment_status" class="form-control">
                                        <option value="">Semua</option>
                                        <option value="Pending">Pending</option>
                                        <option value="Paid">Paid</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Pengiriman:</label>
                                    <select name="shipping" class="form-control">
                                        <option value="">Semua</option>
                                        <option value="jne">JNE</option>
                                        <option value="tiki">TIKI</option>
                                        <option value="pos">POS</option>
                                        <option value="pickup">Pickup</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Zona:</label>
                                    <select name="zone" class="form-control">
                                        <option value="">Semua</option>
                                        <?php if ($groups = $this->main->gets('merchant_groups', array(), 'name asc')) foreach ($groups->result() as $group) { ?>
                                                <option value="<?php echo $group->id; ?>"><?php echo $group->name; ?></option>
                                            <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-2">Kota</label>
                                    <div class="col-sm-10">
                                        <select multiple="" name="city" class="select2 form-control">
                                            <?php if ($cities = $this->main->gets('cities')) foreach ($cities->result() as $city) { ?>
                                                    <option value="<?php echo $city->id; ?>"><?php echo $city->name; ?></option>
                                                <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <button class="btn btn-default" type="button" id="filter">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel panel-flat">
            <table class="table table-hover" id="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Tanggal</th>
                        <th>Pelanggan</th>
                        <th>Total</th>
                        <th>Pembayaran</th>
                        <th>Status</th>
                        <th>Pengiriman</th>
                        <th>Zona</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($datas->num_rows()>0)
                        foreach ($datas->result() as $data) {
                        $shipping = explode('-', $data->shipping_courier);
                            echo '<tr>
                                <td><a href="'. site_url('orders/view/'. encode($data->order)).'">' . $data->invoice . '</a></td>
                                <td>' . get_date($data->date) . '</td>
                                <td>' . $data->customer . '</td>
                                <td>' . number($data->total) . '</td>
                                <td>' . $data->payment_status . '</td>
                                <td>' . $data->order_status . '</td>
                                <td>' . strtoupper($shipping[0]) . '</td>
                                <td>' . $data->merchant_group . '</td>
                            </tr>';
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>