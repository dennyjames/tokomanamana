<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Balances extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->lang->load('balances', settings('language'));
        $this->load->model('reports_branch_model', 'reports');
        $this->data['menu'] = 'report_balance';
    }

    public function index() {
        $this->breadcrumbs->unshift(lang('report'), '/');
        $this->breadcrumbs->push(lang('report_balance'), '/reports/balances');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->data['balance'] = $this->reports->balances_total($this->data['user']->principle_id)->row()->balance;
        $this->data['is_requested'] = $this->reports->balances_withdrawal($this->data['user']->principle_id)->row()->balance;

        $this->template->_init();
        $this->template->form();
        $this->template->table();
        $this->load->js('../assets/backend/js/modules/reports.js');
        $this->load->js('../assets/backend/js/merchant/modules/reports/balances.js');
        $this->load->js('../assets/backend/js/merchant/modules/auth/verification.js');
        $this->output->set_title(lang('heading'));
        $this->load->view('balances/index', $this->data);
    }

    public function data() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $input = $this->input->post(null, true);
        $filter = $input['filter'];
        $output['data'] = array();
        $datas = $this->reports->balances($this->data['user']->principle_id, $input['start'], $input['length'], $filter['from'], $filter['to']);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    get_date($data->date_added),
                    ($data->invoice_code) ? 'Transaksi Penjualan #' . $data->invoice_code : $data->note,
                    rupiah($data->amount),
                    rupiah($data->balance)
                );
            }
        }
        $this->db->join('merchants m', 'm.id = merchant_balances.merchant', 'left');
        $this->db->where('m.principal_id',$this->data['user']->principle_id);
        $output['recordsTotal'] = $this->main->count('merchant_balances', array());
        $output['recordsFiltered'] = $this->reports->balances_count($this->data['user']->principle_id, $filter['from'], $filter['to']);
        echo json_encode($output);
    }

    public function request() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('amount', 'Jumlah Penarikan', 'trim|required|is_natural_no_zero');
        $this->form_validation->set_rules('token', 'Kode Verifikasi', 'trim|required');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            do {
                if ($this->main->get('merchant_users', array('id' => $this->data['user']->id))->verification_code != $data['token']) {
                    $return = array('message' => 'Kode Verifikasi salah.', 'status' => 'error');
                    break;
                }
                if ($this->main->get('merchant_balance_withdrawal', array('merchant' => $this->data['user']->merchant, 'status' => 'Pending'))) {
                    $return = array('message' => 'Saat ini Anda memiliki permintaan Tarik Dana yang masih Diproses.', 'status' => 'error');
                    break;
                }
                $merchant = $this->main->get('merchants', array('id' => $this->data['user']->merchant));
                if ($merchant->cash_balance < $data['amount']) {
                    $return = array('message' => 'Saldo tidak mencukupi.', 'status' => 'error');
                    break;
                }
                $to = json_encode(array('name' => $merchant->bank_name, 'branch' => $merchant->bank_branch, 'account_number' => $merchant->bank_account_number, 'account_name' => $merchant->bank_account_name));
                $this->main->insert('merchant_balance_withdrawal', array('merchant' => $this->data['user']->merchant, 'amount' => $data['amount'], 'to' => $to, 'status' => 'Pending'));
                $this->main->update('merchant_users', array('verification_code' => NULL), array('id' => $this->data['user']->id));
                $return = array('message' => 'Permintaan Tarik Dana berhasil!', 'status' => 'success');
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

}
