<?php

defined('BASEPATH') or exit('No direct script access allowed!');

require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Sales extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('report/sales');
        $this->lang->load('sales', settings('language'));
        $this->load->model('reports_model', 'reports');
        $this->data['menu'] = 'report_sales';
        $this->data['body_class'] = 'has-detached-left';
    }

    public function index() {
        $this->breadcrumbs->unshift(lang('report'), '/');
        $this->breadcrumbs->push(lang('report_sales'), '/reports/sales');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->template->_init();
        $this->template->form();
        $this->template->table();
        $this->load->js('../assets/principle/js/modules/reports.js');
        $this->output->set_title(lang('heading'));
        $this->load->view('sales/index', $this->data);
    }
    
//     public function data() {
//         $this->input->is_ajax_request() or exit('No direct post submit allowed!');
//         $user = $this->session->userdata('p_user');
//         $input = $this->input->post(null, true);
//         $filter = $input['filter'];
//         $output['data'] = array();
//         $datas = $this->reports->principle_sales($input['start'], $input['length'], $input['order'][0],$user->principle_id, $filter['from'], $filter['to'],$filter['brands'],$filter['zone'],isset($filter['city'])?$filter['city']:'');
//         //var_dump($datas->result());exit();
//         //$datas = $this->reports->sales($input['start'], $input['length'], $input['order'][0], $filter['from'], $filter['to'],$filter['order_status'],$filter['payment_status'],$filter['shipping'],$filter['zone'],isset($filter['city'])?$filter['city']:'');
//         if ($datas) {
//             foreach ($datas->result() as $data) {
// //                $shipping = explode('-', $data->shipping_courier);
//                 // $output['data'][] = array(
//                 //     '<a href="'. site_url('orders/view/'. encode($data->order)).'" target="_blank">' . $data->invoice . '</a>',
//                 //     get_date($data->date),
//                 //     $data->customer,
//                 //     number($data->total),
//                 //     $data->payment_status,
//                 //     $data->order_status,
//                 //     strtoupper($data->shipping_courier),
//                 //     $data->merchant_group,
//                 // );
//                   $output['data'][] = array(
//                     get_date($data->date),
//                     $data->invoice_code,
//                     $data->product_name,
//                     number($data->total_price),
//                     $data->qty,
//                     $data->customer,
//                     $data->merchant_name,
//                     strtoupper($data->brand_name),
//                     $data->merchant_group,
//                 );
//             }
//         }
//         $output['recordsTotal'] = $this->reports->principle_sales_count($user->principle_id);
//         $output['recordsFiltered'] = $this->reports->principle_sales_count_filter($user->principle_id,$filter['from'], $filter['to'],$filter['brands'],$filter['zone'],isset($filter['city'])?$filter['city']:'');
//         echo json_encode($output);
//     }

    public function data() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $user = $this->session->userdata('p_user');
        $input = $this->input->post(null, true);
        $filter = $input['filter'];
        $output['data'] = array();
        $datas = $this->reports->principle_sales($input['start'], $input['length'], $input['order'][0],$user->principle_id, $filter['from'], $filter['to'],$filter['brands'],$filter['branch'],$filter['shipping']);
        //var_dump($datas->result());exit();
        //$datas = $this->reports->sales($input['start'], $input['length'], $input['order'][0], $filter['from'], $filter['to'],$filter['order_status'],$filter['payment_status'],$filter['shipping'],$filter['zone'],isset($filter['city'])?$filter['city']:'');
        if ($datas) {
            foreach ($datas->result() as $data) {
    //                $shipping = explode('-', $data->shipping_courier);
                // $output['data'][] = array(
                //     '<a href="'. site_url('orders/view/'. encode($data->order)).'" target="_blank">' . $data->invoice . '</a>',
                //     get_date($data->date),
                //     $data->customer,
                //     number($data->total),
                //     $data->payment_status,
                //     $data->order_status,
                //     strtoupper($data->shipping_courier),
                //     $data->merchant_group,
                // );
                $output['data'][] = array(
                    $data->code,
                    $data->date,
                    $data->fullname,
                    number($data->total_price),
                    $data->order_status,
                    $data->shipping_courier,
                    $data->merchant_name,
                    strtoupper($data->brand_name)
                );
            }
        }
        $output['recordsTotal'] = $this->reports->principle_sales_count($user->principle_id);
        $output['recordsFiltered'] = $this->reports->principle_sales_count_filter($user->principle_id,$filter['from'], $filter['to'],$filter['brands'],$filter['branch'],$filter['shipping']);
        echo json_encode($output);
    }

    public function export() {
        $filename = "laporan_penjualan";
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
        $heading_template = array( 
            'No. Invoice', 
            'Pelanggan', 
            'Cabang',
            'Produk', 
            'Jumlah', 
            'Harga', 
            'Tanggal', 
            'Logistik', 
            'No. Resi', 
            'Biaya Kirim', 
            'Total Pembayaran', 
            // 'Kupon', 
            'Nilai Potongan', 
            // 'Status', 
            'Tanggal Terakhir Diperbarui'
        );
        $sheet->fromArray($heading_template, NULL, 'A1');
        $data = $this->input->post(null, true);
        $fullFileName = "${filename}_".$data['from'].'_'.$data['to'];
        $principal = $this->data['user']->principle_id;
        $datas = $this->reports->sales_export($principal, $data['from'], $data['to'], $data['brands'], $data['shipping'], $data['branch']);
        $n = 1;
        $i = 2;
        if ($datas->num_rows() > 0) {
            foreach ($datas->result() as $data) {
                $coupon = $this->reports->get_coupon($data->order);
                if ($coupon) {
                    $invoices = $this->main->gets('order_invoice', array('order' => $data->order));
                    if ($invoices) {
                        $total_invoice_without_shipping = 0;
                        foreach ($invoices->result() as $invoice) {
                            $total_invoice_without_shipping += $invoice->subtotal;
                        }

                        $d = ($coupon->amount / $total_invoice_without_shipping) * 100;
                        $percentage = 100 - $d;
                        $discount_ammount = $data->subtotal * $percentage / 100;
                        $discount_ammount = $data->subtotal - $discount_ammount;
                    } else {
                        $discount_ammount = 0;
                    }
                } else {
                    $discount_ammount = 0;
                }

                $products = $this->main->gets('order_product', array('order' => $data->order, 'invoice' => $data->id));
                $log = $this->main->get('order_history', array('order' => $data->order, 'invoice' => $data->id, 'order_status' => $data->status_code));

                if ($products->num_rows() > 1) {
                    $row = $i + $products->num_rows() - 1;
                    $sheet->mergeCells('A'. $i . ':A' . $row); $sheet->setCellValue('A'. $i, $data->invoice);
                    $sheet->mergeCells('B'. $i . ':B' . $row); $sheet->setCellValue('B'. $i, $data->customer);
                    $sheet->mergeCells('C'. $i . ':C' . $row); $sheet->setCellValue('C'. $i, $data->merchant_name);
                    $x = $i;
                    foreach ($products->result() as $product) {
                        $sheet->setCellValue('D'. $x, $product->name);
                        $sheet->setCellValue('E'. $x, $product->quantity);
                        $sheet->setCellValue('F'. $x, $product->price);
                        $x++;
                    }
                    $sheet->mergeCells('G'. $i . ':G' . $row); $sheet->setCellValue('G'. $i, $data->date);
                    $sheet->mergeCells('H'. $i . ':H' . $row); $sheet->setCellValue('H'. $i, $data->shipping_courier);
                    $sheet->mergeCells('I'. $i . ':I' . $row); $sheet->setCellValue('I'. $i, $data->tracking_number);
                    $sheet->mergeCells('J'. $i . ':J' . $row); $sheet->setCellValue('J'. $i, $data->shipping_cost);
                    $sheet->mergeCells('K'. $i . ':K' . $row); $sheet->setCellValue('K'. $i, $data->total);
                    // $sheet->mergeCells('L'. $i . ':L' . $row); $sheet->setCellValue('L'. $i, $coupon ? $coupon->code : '');
                    $sheet->mergeCells('L'. $i . ':L' . $row); $sheet->setCellValue('L'. $i, $discount_ammount);
                    // $sheet->mergeCells('N'. $i . ':N' . $row); $sheet->setCellValue('N'. $i, $data->order_status);
                    $sheet->mergeCells('M'. $i . ':M' . $row); $sheet->setCellValue('M'. $i, $log ? $log->date_added : '-');
                    $i = $row++;
                    $i++;
                } else {
                    foreach ($products->result() as $product) {
                        $sheet->setCellValue('A'. $i, $data->invoice);
                        $sheet->setCellValue('B'. $i, $data->customer);
                        $sheet->setCellValue('C'. $i, $data->merchant_name);
                        $sheet->setCellValue('D'. $i, $product->name);
                        $sheet->setCellValue('E'. $i, $product->quantity);
                        $sheet->setCellValue('F'. $i, $product->price);
                        $sheet->setCellValue('G'. $i, $data->date);
                        $sheet->setCellValue('H'. $i, $data->shipping_courier);
                        $sheet->setCellValue('I'. $i, $data->tracking_number);
                        $sheet->setCellValue('J'. $i, $data->shipping_cost);
                        $sheet->setCellValue('K'. $i, $data->total);
                        // $sheet->setCellValue('L'. $i, $coupon ? $coupon->code : '');
                        $sheet->setCellValue('L'. $i, $discount_ammount);
                        // $sheet->setCellValue('N'. $i, $data->order_status);
                        $sheet->setCellValue('M'. $i, $log ? $log->date_added : '-');
                        $i++;
                    }
                }
                $n++;
            }
        }
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fullFileName . '.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        ob_start();
        $writer->save('php://output');
        $xlsData = ob_get_contents();
        ob_end_clean();
        $response = array(
            'op' => 200,
            'file' => "data: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($xlsData),
            'name' => $fullFileName . '.xls'
        );

        die(json_encode($response));
    }

}
