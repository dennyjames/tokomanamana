<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Seo_url_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array(), $additional = array()) {
        $this->where_like($search);
        $this->where_category($additional);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->limit($length, $start);

        return $this->db->get('seo_url');
    }

    function get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'query';
                break;
            case 1: $key = 'keyword';
                break;
        }
        return $key;
    }

    function count_all($search = '', $additional = array()) {
        $this->where_like($search);
        $this->where_category($additional);
        return $this->db->count_all_results('seo_url');
    }

    function where_like($search = '') {
        $columns = array('query', 'keyword');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    function where_category($data = array()) {
        if (isset($data['category'])) {
            $category = $data['category'];
            if ($category == 'other') {
                $this->db->where("SUBSTR(query, 1,7) != 'catalog'");
                $this->db->where("SUBSTR(query, 1,9) != 'merchants'");
            } else {
                $this->db->where("SUBSTR(query, 1," . strlen($category) . ") = '$category'");
            }
        }
    }

}
