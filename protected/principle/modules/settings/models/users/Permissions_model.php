<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->limit($length, $start);

        return $this->db->get('aauthp_perms');
    }

    function get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'name';
                break;
            case 1: $key = 'definition';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->where_like($search);
        return $this->db->count_all_results('aauthp_perms');
    }

    function where_like($search = '') {
        $columns = array('name', 'definition');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

}
