<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Settings extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('setting');
        $this->data['menu'] = 'setting_base';
        
        $this->lang->load('settings', settings('language'));
    }

    public function index() {
//        $this->session->unset_userdata('tnkSettings');
        
        $this->breadcrumbs->push(lang('setting'), '/');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->data['data'] = $this->main->get('principles', ['id' => $this->data['user']->principle_id]);
        $this->data['provinces'] = $this->main->gets('provincies');

        if ($this->data['data']->province)
            $this->data['cities'] = $this->main->gets('cities', array('province' => $this->data['data']->province), 'name asc');
        if ($this->data['data']->city)
            $this->data['districts'] = $this->main->gets('districts', array('city' => $this->data['data']->city), 'name asc');
        
        $this->template->_init();
        $this->template->form();
        $this->load->js('../assets/principle/js/modules/settings/settings.js');
        $this->output->set_title(lang('heading'));
        $this->load->view('settings', $this->data);
    }

//     public function save() {
//         $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        
//         $data = $this->input->post(null, true);
        
// //        print_r($data['footer']);
        
//         foreach ($data as $key => $value) {
//             if($key=='footer'){
//                 $value = json_encode($value);
//             }
//             $this->main->update('settings', array('value' => $value), array('key' => $key));
//         }
//         $this->main->delete('sessions',array('id !='=> session_id()));
//         $this->session->unset_userdata('tnkSettings');
        
//         $return = array('message' => "Pengaturan berhasil disimpan.", 'status' => 'success', 'redirect' => '');
//         echo json_encode($return);
//     }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $data['meta_title'] = $data['name'];
        $data['meta_description'] = $data['description'];
        $this->main->update('principles', $data, ['id' => $this->data['user']->principle_id]);
        $return = [
            'message' => 'Pengaturan berhasil disimpan!',
            'status' => 'success',
            'redirect' => ''
        ];
        echo json_encode($return);
    }

    public function get_cities() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $output = '<option value="">Pilih Kota</option>';
        if ($province = $this->input->post('id')) {
            $cities = $this->main->gets('cities', array('province' => $province), 'name asc');
            if ($cities)
                foreach ($cities->result() as $city) {
                    $output .= '<option value="' . $city->id . '">' . $city->type . ' ' . $city->name . '</option>';
                }
        }
        echo $output;
    }

    public function get_districts() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $output = '<option value="">Pilih Kecamatan</option>';
        if ($city = $this->input->post('id')) {
            $districts = $this->main->gets('districts', array('city' => $city), 'name asc');
            if ($districts)
                foreach ($districts->result() as $district) {
                    $output .= '<option value="' . $district->id . '">' . $district->name . '</option>';
                }
        }
        echo $output;
    }

}
