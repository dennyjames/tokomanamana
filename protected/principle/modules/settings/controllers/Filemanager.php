<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Filemanager extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('setting/filemanager');
        $this->lang->load('filemanager', settings('language'));
        $this->data['menu'] = 'setting_filemanager';
    }

    public function index() {
        $this->template->_init();
        
        $this->breadcrumbs->unshift(lang('setting'), '/');
        $this->breadcrumbs->push(lang('filemanager'), '/settings/filemanager');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('file_manager_heading'));
        $this->load->view('filemanager', $this->data);
    }

}
