<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-sm-12">
                <form action="<?php echo site_url('settings/save'); ?>" class="form-horizontal" method="post" id="form">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#general" data-toggle="tab"><?php echo lang('general'); ?></a></li>
                                    <li class=""><a href="#store" data-toggle="tab"><?php echo lang('store'); ?></a></li>
                                    <li class=""><a href="#terms" data-toggle="tab"><?php echo lang('terms'); ?></a></li>
                                    <li class=""><a href="#footer" data-toggle="tab"><?php echo lang('footer'); ?></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?php echo lang('meta_title'); ?> </label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" required="" name="meta_title" value="<?php echo settings('meta_title'); ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?php echo lang('meta_description'); ?> </label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" name="meta_description"><?php echo settings('meta_description'); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?php echo lang('meta_keyword'); ?> </label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" name="meta_keyword"><?php echo settings('meta_keyword'); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?php echo lang('layout_banner'); ?></label>
                                            <div class="col-sm-3">
                                                <select class="form-control"  name="layout_banner">
                                                    <option value="1" <?php echo (settings('layout_banner') == 1) ? 'selected' : ''; ?>>Layout 1</option>
                                                    <option value="2" <?php echo (settings('layout_banner') == 2) ? 'selected' : ''; ?>>Layout 2</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="store">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?php echo lang('logo'); ?> </label>
                                            <div class="col-sm-3" id="image-preview">
                                                <div class="thumbnail">
                                                    <div class="thumb">
                                                        <img src="<?php echo base_url('../files/images/' . settings('logo')); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3" id="add-image">
                                                <button type="button" id="edit-image-btn" class="btn btn-default" data-toggle="modal" data-target="#filemanager"><?php echo lang('button_edit'); ?></button>
                                                <input type="hidden" id="image" name="logo" value="<?php echo settings('logo'); ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?php echo lang('store_name'); ?> </label>
                                            <div class="col-sm-9">
                                                <input class="form-control" type="text" required="" name="store_name" value="<?php echo settings('store_name'); ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?php echo lang('store_description'); ?> </label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" name="store_description"><?php echo settings('store_description'); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?php echo lang('store_address'); ?> </label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" name="store_address"><?php echo settings('store_address'); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?php echo lang('email'); ?> </label>
                                            <div class="col-sm-9">
                                                <input type="email" class="form-control" name="store_email" value="<?php echo settings('store_email'); ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?php echo lang('telephone'); ?> </label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="store_telephone" value="<?php echo settings('store_telephone'); ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="terms">
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?php echo lang('term_customer_register'); ?></label>
                                            <div class="col-sm-9">
                                                <textarea cols="30" rows="2" class="form-control tinymce" name="term_customer_register" ><?php echo settings('term_customer_register'); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"><?php echo lang('term_merchant_register'); ?></label>
                                            <div class="col-sm-9">
                                                <textarea cols="30" rows="2" class="form-control tinymce" name="term_merchant_register" ><?php echo settings('term_merchant_register'); ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane " id="footer">
                                        <?php
                                        $footer = json_decode(settings('footer'), TRUE);
                                        for ($i = 0; $i <= 3; $i++) {
                                            ?>
                                            <fieldset class="content-group" id="footer-<?php echo $i; ?>">
                                                <legend class="text-bold">Kolom <?php echo $i + 1; ?> <input type="text" name="footer[<?php echo $i; ?>][title]" class="form-control" value="<?php echo $footer[$i]['title']; ?>" placeholder="Judul footer kolom <?php echo $i; ?>" style="margin-left: 20px; display: inline-block; width: 50%"></legend>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <label>Tipe</label>
                                                        <select class="form-control select-type" data-footer="<?php echo $i; ?>" name="footer[<?php echo $i; ?>][type]">
                                                            <option value="list" <?php echo ($footer[$i]['type'] == 'list') ? 'selected' : ''; ?>>List</option>
                                                            <option value="text" <?php echo ($footer[$i]['type'] == 'text') ? 'selected' : ''; ?>>Text</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <label>Konten</label>
                                                        <div class="footer-content-list" <?php echo ($footer[$i]['type'] == 'text') ? 'style="display:none"' : ''; ?>>
                                                            <?php
                                                            $j = 0;
                                                            if ($footer[$i]['type'] == 'list') {
                                                                foreach ($footer[$i]['content'] as $link) {
                                                                    ?>
                                                                    <div class="row mb-5" id="<?php echo 'footer-' . $i . '-link-' . $j; ?>">
                                                                        <div class="col-sm-4">
                                                                            <input type="text" class="form-control" name="footer[<?php echo $i; ?>][content][<?php echo $j ?>][title]" placeholder="Title" value="<?php echo $link['title']; ?>">
                                                                        </div>
                                                                        <div class="col-sm-7">
                                                                            <input type="text" class="form-control" name="footer[<?php echo $i; ?>][content][<?php echo $j ?>][url]" placeholder="URL" value="<?php echo $link['url']; ?>">
                                                                        </div>
                                                                        <div class="col-sm-1">
                                                                            <a href="javascript:delete_footer_link(<?php echo $i . ',' . $j; ?>)"><i class="icon-x"></i></a>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                    $j++;
                                                                }
                                                            } else {
                                                                ?>
                                                                <div class="row mb-5">
                                                                    <div class="col-sm-4">
                                                                        <input type="text" class="form-control" name="footer[<?php echo $i; ?>][content][0][title]" placeholder="Title">
                                                                    </div>
                                                                    <div class="col-sm-7">
                                                                        <input type="text" class="form-control" name="footer[<?php echo $i; ?>][content][0][url]" placeholder="URL">
                                                                    </div>
                                                                    <div class="col-sm-1">
                                                                        <a href="javascript:delete_footer_link(<?php echo $i . ',0'; ?>)"><i class="icon-x"></i></a>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                            <div class="button">
                                                                <div class="col-sm-12">
                                                                    <a class="btn btn-primary btn-sm btn-add-link" data-footer="<?php echo $i; ?>" data-last-link="<?php echo $j; ?>"><i class="icon-plus3"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="footer-content-text" <?php echo ($footer[$i]['type'] == 'list') ? 'style="display:none"' : ''; ?>>
                                                            <textarea name="footer[<?php echo $i; ?>][content]" class="form-control" <?php echo ($footer[$i]['type'] == 'list') ? 'disabled' : ''; ?>><?php echo ($footer[$i]['type'] == 'text')? $footer[$i]['content']:''; ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="filemanager" class="modal" data-backdrop="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo lang('filemanager'); ?></h5>
            </div>

            <div class="modal-body">
                <iframe  width="100%" height="550" frameborder="0" src="<?php echo base_url('filemanager/dialog.php?type=1&editor=false&field_id=image&relative_url=1'); ?>"></iframe>
            </div>
        </div>
    </div>
</div>