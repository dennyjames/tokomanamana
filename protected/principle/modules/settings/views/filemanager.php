<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('file_manager_heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="#" class="form-horizontal">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <iframe  width="100%" height="550" frameborder="0" src="<?php echo site_url('filemanager/dialog.php?type=0'); ?>"></iframe>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>