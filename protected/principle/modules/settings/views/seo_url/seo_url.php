<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2>
                    SEO Url &nbsp;
                    <div class="btn-group">
                        <button type="button" class="btn border-slate text-slate-800 btn-flat dropdown-toggle" data-toggle="dropdown" id="filter-category">All <span class="caret"></span></button>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="javascript:void(0);" onclick="set_filter_category(this);" data-category="">All</a></li>
                            <li><a href="javascript:void(0);" onclick="set_filter_category(this);" data-category="catalog/products">Product</a></li>
                            <li><a href="javascript:void(0);" onclick="set_filter_category(this);" data-category="catalog/categories">Category</a></li>
                            <li><a href="javascript:void(0);" onclick="set_filter_category(this);" data-category="catalog/pages">Page</a></li>
                            <li><a href="javascript:void(0);" onclick="set_filter_category(this);" data-category="catalog/brands">Brand</a></li>
                            <li><a href="javascript:void(0);" onclick="set_filter_category(this);" data-category="merchants">Merchant</a></li>
                            <li><a href="javascript:void(0);" onclick="set_filter_category(this);" data-category="other">Other</a></li>
                        </ul>
                    </div>
                </h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('settings/seo_url/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span>Tambah Seo Url</span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table" id="table" data-url="<?php echo site_url('settings/seo_url/get_list'); ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="asc">Query</th>
                        <th>Keyword</th>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions_th'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>