<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['heading'] = 'Merchant';
$lang['sub_heading'] = 'Kelola Data Merchant';
$lang['list_heading'] = 'Daftar Merchant';
$lang['add_heading'] = 'Tambah Merchant';
$lang['edit_heading'] = 'Edit Merchant';

$lang['general_tabs'] = 'Umum';
$lang['data_tabs'] = 'Data';
$lang['address_tabs'] = 'Alamat';
$lang['image_tabs'] = 'Gambar';
$lang['owner_tabs'] = 'Pemilik';

$lang['name'] = 'Nama';
$lang['description'] = 'Deskripsi';
$lang['province'] = 'Provinsi';
$lang['city'] = 'Kota';
$lang['district'] = 'Kecamatan';
$lang['address'] = 'Alamat';
$lang['postcode'] = 'Kode Pos';
$lang['telephone'] = 'Telepon';
$lang['status'] = 'Status';
$lang['owner_name'] = 'Nama Pemilik';
$lang['email'] = 'Alamat Email';
$lang['setting_profile_bank_account_name_label'] = 'Pemilik Rekening';
$lang['setting_profile_bank_account_number_label'] = 'No Rekening';
$lang['setting_profile_bank_name_label'] = 'Nama Bank';
$lang['setting_profile_bank_branch_label'] = 'Cabang';
$lang['password'] = 'Password';
$lang['group'] = 'Kelompok Merchant';
$lang['maps'] = 'Lokasi Maps';
$lang['cash_balance'] = 'Saldo';
$lang['status_0'] = '<span class="label label-danger">Tidak aktif</span>';
$lang['status_1'] = '<span class="label label-success">Aktif</span>';
$lang['password_update_help'] = 'Biarkan kosong jika tidak ingin mengganti password merchant';

$lang['name_placeholder'] = 'Masukkan nama merchant';
$lang['description_placeholder'] = 'Masukkan deskripsi dari merchant ini. Deskripsi akan ditampilkan di halaman profil merchant ini';

$lang['save_success_message'] = "Merchant '%s' berhasil disimpan.";
$lang['save_error_message'] = "Merchant '%s' gagal disimpan.";
$lang['delete_success_message'] = "Merchant '%s' berhasil dihapus.";
$lang['delete_error_message'] = "Merchant '%s' gagal dihapus.";
$lang['username_exist_message'] = "Domain Toko '%s' sudah terdaftar.";
$lang['activate_message'] = "Merchant '%s' berhasil diaktifkan.";