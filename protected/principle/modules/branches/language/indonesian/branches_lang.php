<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['branch_heading'] = 'Cabang';
$lang['branch_sub_heading'] = 'Kelola Data Cabang';
$lang['branch_list_heading'] = 'Daftar Cabang';
$lang['branch_add_heading'] = 'Tambah Cabang';
$lang['branch_edit_heading'] = 'Edit Cabang';

$lang['branch_image_th'] = 'Gambar';
$lang['branch_name_th'] = 'Nama Cabang';
$lang['branch_city_th'] = 'Kota';

$lang['branch_form_data_tabs'] = 'Data';
$lang['branch_form_address_tabs'] = 'Alamat';
$lang['branch_form_owner_tabs'] = 'Penanggung Jawab';

$lang['branch_form_name_label'] = 'Nama';
$lang['branch_form_description_label'] = 'Deskripsi';;
$lang['branch_form_province_label'] = 'Provinsi';
$lang['branch_form_city_label'] = 'Kota';
$lang['branch_form_district_label'] = 'Kecamatan';
$lang['branch_form_address_label'] = 'Alamat';
$lang['branch_form_postcode_label'] = 'Kode Pos';
$lang['branch_form_telephone_label'] = 'Telepon';
$lang['branch_form_w_price_label'] = 'Harga /m3';
$lang['branch_form_w_size_label'] = 'Ukuran dalam m3';
$lang['branch_form_status_label'] = 'Status';
$lang['branch_form_fullname_label'] = 'Nama Lengkap';
$lang['branch_form_email_label'] = 'Alamat Email';
$lang['branch_form_password_label'] = 'Password';
$lang['branch_form_phone_label'] = 'No HP penanggung jawab';

$lang['branch_form_password_update_help'] = 'Biarkan kosong jika tidak ingin mengganti password cabang';

$lang['branch_form_name_placeholder'] = 'Masukkan nama cabang';
$lang['branch_form_description_placeholder'] = 'Masukkan deskripsi cabang';
$lang['branch_form_telephone_placeholder'] = 'Masukkan nomor telepon';
$lang['branch_form_address_placeholder'] = 'Masukkan alamat cabang';
$lang['branch_form_province_placeholder'] = 'Pilih provinsi';
$lang['branch_form_city_placeholder'] = 'Pilih kota';
$lang['branch_form_district_placeholder'] = 'Pilih kabupaten';
$lang['branch_form_fullname_placeholder'] = 'Masukkan nama lengkap penanggung jawab';
$lang['branch_form_email_placeholder'] = 'Masukkan alamat email penanggung jawab';
$lang['branch_form_phone_placeholder'] = 'Masukkan no hp penanggung jawab';
$lang['branch_form_bank_account_name_placeholder'] = 'Masukkan nama pemilik rekening bank';
$lang['branch_form_bank_account_number_placeholder'] = 'Masukkan no rekening bank';
$lang['branch_form_bank_name_placeholder'] = 'Masukkan nama bank';
$lang['branch_form_bank_branch_placeholder'] = 'Masukkan nama cabang bank';
$lang['branch_form_password_placeholder'] = 'Masukkan password';

$lang['branch_save_success_message'] = "Cabang '%s' berhasil disimpan.";
$lang['branch_save_error_message'] = "Cabang '%s' gagal disimpan.";
$lang['branch_delete_success_message'] = "Cabang '%s' berhasil dihapus.";
$lang['branch_delete_error_message'] = "Cabang '%s' gagal dihapus.";

$lang['actions_th'] = 'Opsi';