<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
               <h2><?php echo lang('profile_picture') ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <?php echo form_open_multipart('design/profile_picture','id="banner_form"');?>
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="row">
                                <?php echo $message ?>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                <div style="margin: 10px;margin-left: 15px;"><h3>Foto Profil</h3></div>
                                <div class="col-md-3" id="image-preview">
                                    <div class="thumbnail">
                                        <div class="thumb">
                                            <?php if($data->image) : ?>
                                                <a href="<?php echo site_url('../files/images/' . $data->image .'?'.time()); ?>" data-lightbox="banner_merchant" target="_blank">
                                                    <img style="" src="<?php echo site_url('../files/images/' . $data->image .'?'.time()); ?>">
                                                </a>
                                            <?php else : ?>
                                                <a href="javascript:void(0)" data-lightbox="banner_merchant" target="_blank">
                                                    <img style="" src="<?php echo site_url('../files/images/profilepicture_merchant/default_image.jpeg'); ?>">
                                                </a>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8" id="add-image">
                                    <div class="form-group">
                                        <label class="col-sm-6 control-label">Foto Profil <b style="color:red;">(*Max Size 2MB)</b> :</label>
                                    <div class="col-sm-7 control-label" >
                                        <input type="file" name="profile_picture" /> 
                                    </div>
                                     <div class="col-sm-7 control-label" style="padding:10px">
                                        <i style="font-weight: 400;">
                                            *Kami anjurkan gambar banner ber-dimensi <strong>120x120 pixel</strong> untuk memastikan tampilan terbaik
                                        </i>
                                    </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                            <div class="heading-elements action-left">
                               
                                <div class="pull-right">
                                    <button type="submit" id="submit-banner" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
