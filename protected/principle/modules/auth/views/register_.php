<style>
    .wrapper {
        position: relative;
        width: 350px;
        height: 200px;
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    body {
        background-image: url('<?php echo site_url("../files/images/bg_register.jpeg") ?>');
        background-size: cover;
        background-attachment: fixed;
    }

    .signature-pad {
        position: absolute;
        left: 0;
        top: 0;
        width: 350px;
        height: 200px;
        background-color: #ccc;
    }

    .btn-clear-canvas {
        position: relative;
        width: 350px;
        height: 200px;
        text-align: right;
    }

    @media screen and (max-width: 480px) {
        .wrapper {
            width: 230px;
            height: 150px;
        }

        .signature-pad {
            width: 230px;
            height: 150px;
        }

        .btn-clear-canvas {
            width: 230px;
            height: 150px;
        }
    }

    .btn-is-disabled {
        pointer-events: none;
    }
</style>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="content">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <h3><?php echo lang('register_heading'); ?></h3>
            <div class="panel panel-body">
                <form class="stepy form-horizontal " action="<?php echo site_url('auth/save_register'); ?>" method="post" id="register" enctype="multipart/form-data">
                    <fieldset>
                        <legend class="text-semibold">Informasi Akun</legend>
                        <!-- <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('brand_name'); ?> *</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="brand_name" placeholder="<?php echo lang('brand_name_placeholder'); ?>">
                            </div>
                        </div> -->
                        <!-- <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('company_name'); ?> *</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="company_name" placeholder="Masukkan nama perusahaan">
                            </div>
                        </div> -->
                        <!-- <div class="form-group">
                            <label class="col-md-3 control-label">Username *</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="username" placeholder="Masukkan username yang unik">
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('pic_name'); ?> *</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="pic_name" placeholder="Masukkan nama lengkap pihak yang bisa dihubungi">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('email'); ?> *</label>
                            <div class="col-md-9">
                                <input type="text" id="email" class="form-control" required name="email" placeholder="Masukkan email perusahaan">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('owner_phone'); ?> *</label>
                            <div class="col-md-9">
                                <!-- <input id="owner_phone" type="text" class="form-control" required name="owner_phone" placeholder="<?php echo lang('owner_phone_placeholder'); ?>"> -->
                                <div class="row">
                                    <div class="col-md-8">
                                        <input id="owner_phone" type="text" class="form-control" name="owner_phone" placeholder="Masukkan nomor handphone">
                                    </div>
                                    <div class="col-md-4">
                                        <a id="verif_icon" data-toggle="modal" data-target="#requestOTPModal" class="btn btn-danger btn-sm openOTPModal">Verifikasi <i class="icon-cross position-right"></i></a>
                                        <a id="verif_icon_ok" style="display:none;" class="btn btn-success btn-sm">Verifikasi <i class="icon-check position-right"></i></a>
                                        <input id="verified_phone" type="text" class="form-control" name="verified_phone" value="" style="display:none;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Password *</label>
                            <div class="col-md-9">
                                <input id="password" type="password" class="form-control" required name="password" placeholder="Masukkan password">
                                <span style="float: right;
                                            padding-right: 10px;
                                            margin-top: -25px;
                                            position: relative;
                                            z-index: 2;" toggle="#password-field" class="fa fa-eye field-icon field-icon toggle-password">
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Konfirmasi Password *</label>
                            <div class="col-md-9">
                                <input id="password2" type="password" name="password_confirm" class="form-control" placeholder="Konfirmasi password" required>
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label class="col-md-3 control-label">Jenis Perusahaan</label>
                            <div class="col-md-9">
                                <select name="company_type" class="form-control">
                                    <option value="pkp">PKP</option>
                                    <option value="non-pkp">NON PKP</option>
                                </select>
                            </div>
                        </div> -->

                    </fieldset>
                    <!-- <fieldset>
                        <legend class="text-semibold">Step 2</legend>
                        <div class="form-group">
                            <label class="col-md-12 control-label">Jenis Perusahaan</label>
                            <div class="col-md-12">
                                <select name="company_type" class="form-control">
                                    <option value="pkp">PKP</option>
                                    <option value="non-pkp">NON  PKP</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('main_category'); ?> *</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" required name="main_category" placeholder="<?php echo lang('main_category_placeholder'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('sku_total'); ?></label>
                            <div class="col-md-12">
                                <select name="sku_total" class="form-control">
                                    <option></option>
                                    <?php if ($sku) { ?>
                                        <?php foreach ($sku->result() as $item) { ?>
                                            <option value="<?php echo $item->id; ?>"><?php echo $item->label; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('revenue'); ?></label>
                            <div class="col-md-12">
                                <select name="revenue" class="form-control">
                                    <option></option>
                                    <?php if ($revenue) { ?>
                                        <?php foreach ($revenue->result() as $item) { ?>
                                            <option value="<?php echo $item->id; ?>"><?php echo $item->label; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-12 control-label"><?php echo lang('document'); ?></label>
                        <div class="col-md-12">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="document[]" class="styled" value="haki">
                                Hak Merk Dagang (HAKI)
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="document[]" class="styled" value="ktp">
                                KTP
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="document[]" class="styled" value="npwp">
                                NPWP Perusahaan
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="document[]" class="styled" value="siup">
                                SIUP
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="document[]" class="styled" value="akta">
                                Akta Perusahaan
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="document[]" class="styled" value="sppkp">
                                SPPKP
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="document[]" class="styled" value="skt">
                                SKT
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="document[]" class="styled" value="bpom">
                                BPOM
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="document[]" class="styled" value="pirt">
                               PIRT
                            </label>
                        </div>
                    </fieldset> -->
                    <fieldset>
                        <legend class="text-semibold">Informasi Perusahaan</legend>
                        <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('brand_name'); ?> *</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" required name="brand_name" placeholder="Masukkan nama brand">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('company_name'); ?> *</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" required name="company_name" placeholder="Masukkan nama perusahaan">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 control-label">Jenis Perusahaan</label>
                            <div class="col-md-12">
                                <select name="company_type" class="form-control">
                                    <option value="pkp">PKP</option>
                                    <option value="non-pkp">NON PKP</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('main_category'); ?> *</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" required name="main_category" placeholder="Masukkan nama kategori">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('sku_total'); ?> *</label>
                            <div class="col-md-12">
                                <select name="sku_total" class="form-control">
                                    <option disabled selected>Pilih SKU</option>
                                    <?php if ($sku) { ?>
                                        <?php foreach ($sku->result() as $item) { ?>
                                            <option value="<?php echo $item->id; ?>"><?php echo $item->label; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('document'); ?> *</label>
                            <div class="col-md-12">
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="document[]" class="styled" value="haki" required>
                                    Hak Merk Dagang (HAKI)
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="document[]" class="styled" value="ktp" required>
                                    KTP
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="document[]" class="styled" value="npwp" required>
                                    NPWP Perusahaan
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="document[]" class="styled" value="siup" required>
                                    SIUP
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="document[]" class="styled" value="akta" required>
                                    Akta Perusahaan
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="document[]" class="styled" value="sppkp" required>
                                    SPPKP
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="document[]" class="styled" value="skt" required>
                                    SKT
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="document[]" class="styled" value="bpom" required>
                                    BPOM
                                </label>
                                <label class="checkbox-inline">
                                    <input type="checkbox" name="document[]" class="styled" value="pirt" required>
                                    PIRT
                                </label>
                            </div>
                        </div>
                        <div class="form-group upload-files">
                            <!-- <div class="col-md-3 control-label">
                                Upload File * <br>
                                (JPG / PDF, MAX 200kb)
                            </div>
                            <div class="custom-file col-md-9">
                                <input type="file" name="document_image[]" required>
                            </div> -->
                        </div>
                        <!-- <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('revenue'); ?> *</label>
                            <div class="col-md-12">
                                <select name="revenue" class="form-control">
                                    <option></option>
                                    <?php if ($revenue) { ?>
                                        <?php foreach ($revenue->result() as $item) { ?>
                                            <option value="<?php echo $item->id; ?>"><?php echo $item->label; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div> -->
                        <!-- <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('instagram_acc'); ?></label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" required name="instagram_acc" placeholder="<?php echo lang('instagram_acc_placeholder'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('other_ecommerce'); ?></label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" required name="other_ecommerce" placeholder="<?php echo lang('other_ecommerce_placeholder'); ?>">
                            </div>
                        </div> -->
                        <!-- <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('range_price'); ?> *</label>
                            <div class="col-md-12">
                                <select name="range_price" class="form-control" required>
                                    <option></option>
                                    <?php if ($range) { ?>
                                        <?php foreach ($range->result() as $item) { ?>
                                            <option value="<?php echo $item->id; ?>"><?php echo $item->label; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div> -->
                    </fieldset>
                    <fieldset>
                        <legend class="text-semibold">Informasi Lainnya</legend>
                        <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('instagram_acc'); ?></label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="instagram_acc" placeholder="Masukkan nama akun instagram">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('other_ecommerce'); ?></label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" required name="other_ecommerce" placeholder="Masukkan dimana anda menjual produk secara online">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('revenue'); ?> *</label>
                            <div class="col-md-12">
                                <select name="revenue" class="form-control">
                                    <option disabled selected>Pilih Revenue</option>
                                    <?php if ($revenue) { ?>
                                        <?php foreach ($revenue->result() as $item) { ?>
                                            <option value="<?php echo $item->id; ?>"><?php echo $item->label; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('range_price'); ?> *</label>
                            <div class="col-md-12">
                                <select name="range_price" class="form-control" required>
                                    <option disabled selected>Pilih range harga</option>
                                    <?php if ($range) { ?>
                                        <?php foreach ($range->result() as $item) { ?>
                                            <option value="<?php echo $item->id; ?>"><?php echo $item->label; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('range_price'); ?></label>
                            <div class="col-md-12">
                                <select name="range_price" class="form-control" required>
                                    <option></option>
                                    <?php if ($range) { ?>
                                        <?php foreach ($range->result() as $item) { ?>
                                            <option value="<?php echo $item->id; ?>"><?php echo $item->label; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="col-md-12 control-label"><?php echo lang('know_from'); ?></label>
                            <div class="col-md-12">
                                <select name="know_from" class="form-control" required onchange="changeKnowFrom()">
                                    <option disabled selected>Pilih salah satu</option>
                                    <?php if ($from) { ?>
                                        <?php foreach ($from->result() as $item) { ?>
                                            <option value="<?php echo $item->id; ?>"><?php echo $item->label; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div id="custom_know_form" class="form-group" style="display:none;">
                            <label class="col-md-12 control-label">Tahu Dari?</label>
                            <div class="col-md-12">
                                <input type="text" class="form-control" name="custom_know_form" placeholder="Tahu dari?" maxlength="20">
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="checkbox">
                                <label>
                                    <a href="#terms" data-toggle="modal" data-target="#terms">
                                        <input id="agreeSnk" type="checkbox" name="agree">
                                        <?php echo lang('terms_prefix'); ?> <?php echo lang('terms'); ?></a>
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class='image col-md-6'>
                                <label class="control-label">Kode Captcha</label>
                                <br>
                                <div id="img_capt">
                                    <?= $image ?>
                                </div>
                                <a href='#' class='refresh'></a>
                            </div>
                            <div class="col-md-6">
                                <label class="control-label">Ketik Captcha</label>
                                <input type="text" name="captcha" class="form-control" placeholder="Masukkan Kode Captcha">
                            </div>
                        </div>
                        <!-- <div class="form-group col-md-12">
                            <div id="captcha required">
                                <?php
                                echo $widget;
                                echo $script;
                                ?>
                            </div>
                        </div> -->
                    </fieldset>
                    <button type="submit" id="submit" class="btn btn-primary stepy-finish">Simpan <i class="icon-floppy-disk position-right"></i></button>
                </form>
                <div>Sudah punya akun?? <a href="<?php echo site_url('auth/login'); ?>">Login disini!</a></div>
            </div>
        </div>
    </div>

</div>
<div id="terms" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo lang('terms'); ?></h5>
            </div>
            <div class="modal-body">
                <?php echo settings('term_merchant_register'); ?>
                <div class="row">
                    <div class="pull-right">
                        <button type="submit" onclick="agreeSnk()" class="btn btn-success">Setuju <i class="icon-check position-right"></i></button>
                        <button type="submit" onclick="closeSnKmodal()" class="btn btn-danger">Tidak Setuju <i class="icon-cross position-right"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="requestOTPModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Request OTP
                </h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">

                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="no-hp">No HP</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inputNoHP" value="" disabled="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="inputPassword3">Kode Konfirmasi</label>
                        <div class="col-sm-6">
                            <input type="text" id="otpCode" class="form-control" placeholder="your otp code" />
                        </div>
                        <div class="col-sm-3">
                            <a id="btn-token" href="javascript:void(0)" class="btn btn-default" onclick="requestOTPButton()">Request OTP</a>
                        </div>
                    </div>
                </form>
            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close
                </button>
                <a href="javascript:void(0)" class="btn btn-primary" onclick="submitOTP()">
                    Konfirmasi
                </a>
            </div>
        </div>
    </div>
</div>