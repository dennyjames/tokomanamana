<style>
    body {
        background-image: url('<?php echo site_url("../files/images/bg_register.jpeg") ?>');
        background-size: cover;
        background-attachment: fixed;
    }

    .field-icon {
        float: right;
        margin-left: 0px;
        margin-right: 8px;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }

    .box-login {
        border-radius: 5px;
        padding: 0px;
        background: #fff;
    }

    .responsive {
        width: 100%;
        height: auto;
        min-height: 460px;
    }

    .polaroid {
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        text-align: center;
        min-width: 650px;
    }

    @media only screen and (max-width: 651px) {
        .responsive {
            width: 100%;
            height: auto;
            float: left;
            margin-bottom: 30px;
        }

        .right-login {
            width: 100%;
            height: auto;
        }

        .polaroid {
            min-width: 0px;
        }

    }
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<div class="content pb-20">
    <div class="col-sm-12">
        <br><br>
        <div class="row">
            <div class="col-sm-3"></div>
            <div class="col-sm-6 box-login polaroid">
                <div class="col-sm-6" style="padding: 0;">
                    <img src="<?php echo site_url('../assets/backend/images/bg-left-principal-login.png') ?>" class="responsive">
                </div>
                <div class="col-sm-6 right-login" style="padding-right: 20px; padding-left: 20px;">
                    <div>
                        <br><br>
                        <div class="text-center">
                            <img src="<?php echo site_url('../files/images/logo.png') ?>" width="120">
                        </div><br>
                        <form action="<?php echo current_url(); ?>" method="post" id="login">
                            <input type="hidden" value="<?php echo $this->input->get('back'); ?>" name="back">
                            <!-- <div class="panel panel-body login-form"> -->
                            <div class="text-center">
                                <h5 class="content-group-lg">Login Principal Application</h5>
                            </div>
                            <div class="message"></div>
                            <?php if ($this->session->flashdata('email_confirm_message')) : ?>
                                <?= $this->session->flashdata('email_confirm_message') ?>
                            <?php endif; ?>
                            <div class="form-group has-feedback has-feedback-left">
                                <input type="email" name="identity" id="identity" class="form-control" placeholder="Alamat Email" autocomplete="off">
                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group has-feedback has-feedback-left">
                                <input type="password" name="password" id="password" class="form-control" placeholder="Kata Sandi" autocomplete="off">
                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>
                            </div>

                            <div class="form-group login-options">
                                <div class="row">
                                    <div class="col-sm-6" style="text-align: left;">
                                        <label class="checkbox-inline">
                                            <input type="checkbox" class="styled" name="remember" value="1">
                                            Ingat saya
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" style="background-color:#a7c22a; color: #fff" class="btn btn-block">Login </button>
                                <!-- <button type="submit" class="btn bg-blue btn-block">Login <i class="icon-arrow-right14 position-right"></i></button> -->
                            </div>
                            <!-- </div> -->
                        </form>
                    </div>
                    <div style="color: #a5a5a5;">
                        <center>Belum punya akun?? <a style="color: #a7c22a" href="<?php echo site_url('auth/register'); ?>">Daftar disini</a></center>
                    </div><br><br>
                </div>
            </div>

        </div>
    </div>
</div>