<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Riwayat_stock_model extends CI_Model {

    function get($id) {
        $this->db->select('p.*, keyword seo_url')
                ->join('seo_url su', "su.query = CONCAT('catalog/products/view/',p.id)", 'left')
                // ->join('brands b', 'b.id = p.brand', 'left')
                // ->where('b.principle_id',$this->data['user']->principle_id)
                ->where('p.id', $id);
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_riwayat_stock($id){
        $this->db->select('m.name, m.id, IFNULL((SELECT quantity FROM products_principal_riwayat_stock pps WHERE pps.branch_id = m.id AND pps.product_id = '.$id.'),0) as quantity')
                ->where('m.principal_id', $this->data['user']->principle_id);

        return $this->db->get('merchants m');
    }

    function get_cabang($id){
        $this->db->select('id, nama')
                ->where('principal_id',$id);

        return $this->db->get('merchants');
    }

    // function get_all($start = 0, $length, $search = '', $order = array(), $tahun) {
    //     $this->_where_like($search);
    //     if ($order) {
    //         $order['column'] = $this->_get_alias_key($order['column']);
    //         $this->db->order_by($order['column'], $order['dir']);
    //     }
    //     $this->db->select('r.quantity, r.quantity_revisi, r.type, r.status, r.date_added, p.code, p.name, m.name as cabang')
    //             ->join('products p', 'p.id = r.product_id', 'left')
    //             ->join('merchants m', 'r.branch_id = m.id', 'left')
    //             ->where('r.principal_id',$this->data['user']->principle_id)
    //             ->where('YEAR(r.date_added)',$tahun)
    //             ->where('p.name !=', null)
    //             ->limit($length, $start);

    //     return $this->db->get('products_principal_history r');
    // }

    function get_all($start = 0, $length, $search = '', $order = array(), $from, $to, $branch = '') {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('r.quantity, r.quantity_revisi, r.type, r.status, r.date_added, p.code, p.name, m.name as cabang')
                ->join('products p', 'p.id = r.product_id', 'left')
                ->join('merchants m', 'r.branch_id = m.id', 'left')
                ->where('r.principal_id',$this->data['user']->principle_id)
                // ->where('YEAR(r.date_added)',$tahun)
                ->where('m.name !=', '')
                ->where('DATE(r.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'')
                ->where('p.name !=', null)
                ->limit($length, $start);

        if($branch) {
            $this->db->where('r.branch_id', $branch);
        }

        return $this->db->get('products_principal_history r');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'code';
                break;
            case 1: $key = 'p.name';
                break;
            case 2: $key = 'm.name';
                break;
            case 3: $key = 'r.type';
                break;
            case 4: $key = 'r.quantity';
                break;
            case 5: $key = 'r.status';
                break;
            case 6: $key = 'r.date_added';
                break;
        }
        return $key;
    }

    function count_all($search = '', $from = '', $to = '', $branch = '') {
        $this->_where_like($search);
        $this->db->select('r.quantity, r.quantity_revisi, r.type, r.status, r.date_added, p.code, p.name, m.name as cabang')
                ->join('products p', 'p.id = r.product_id', 'left')
                ->join('merchants m', 'r.branch_id = m.id', 'left')
                // ->where('YEAR(r.date_added)',$tahun)
                ->where('m.name !=', '')
                ->where('r.principal_id',$this->data['user']->principle_id);
        if($from && $to) {
            $this->db->where('DATE(r.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'');
        }
        if($branch) {
            $this->db->where('r.branch_id', $branch);
        }
        return $this->db->count_all_results('products_principal_history r');
    }

    private function _where_like($search = '') {
        $columns = array('p.code', 'p.name', 'price', 'm.name', 'r.type', 'r.quantity', 'r.quantity_revisi');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    function get_year(){
        return $this->db->query("select distinct(YEAR(date_added)) as tahun from products_principal_history order by tahun DESC");
    }

}
