<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Product_price_model extends MY_Model {

    public $table = 'product_prices';
    public $primary_key = 'id';

}
