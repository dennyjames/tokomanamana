<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reviews_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('pr.id, pr.rating, pr.title, pr.description, pr.status, c.fullname customer, p.name product, DATE(pr.date_added) date')
                ->join('customers c', 'pr.customer = c.id', 'left')
                ->join('products p', 'pr.product = p.id', 'left')
                ->limit($length, $start);

        return $this->db->get('product_review pr');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'p.name';
                break;
            case 1: $key = 'c.fullname';
                break;
            case 2: $key = 'pr.rating';
                break;
            case 3: $key = 'pr.title';
                break;
            case 5: $key = 'pr.status';
                break;
            case 6: $key = 'pr.date_added';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->_where_like($search);
        $this->db->join('customers c', 'pr.customer = c.id', 'left')
                ->join('products p', 'pr.product = p.id', 'left');
        return $this->db->count_all_results('product_review pr');
    }

    private function _where_like($search = '') {
        $columns = array('code', 'p.name', 'price', 'c.name');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    function get_categories() {
        $this->db->select("pcp.category id, GROUP_CONCAT(pc2.name ORDER BY pcp.level SEPARATOR ' > ') name", FALSE)
                ->join('categories pc1', 'pcp.category = pc1.id', 'left')
                ->join('categories pc2', 'pcp.path = pc2.id', 'left')
                ->order_by('pc2.name')
                ->group_by('pcp.category');

        return $this->db->get('category_path pcp');
    }

    function get_review_features($review) {
        $this->db->select('pf.review, pf.value, f.*')
                ->join('features f', 'pf.feature = f.id', 'left')
                ->where('pf.review', $review)
                ->group_by('pf.feature');
        return $this->db->get('review_feature pf');
    }

    function get_review_feature_variants($review, $feature) {
        $this->db->select('fv.*')
                ->join('feature_variant fv', 'pf.feature = fv.feature AND pf.value = fv.id', 'left')
                ->where('pf.review', $review)
                ->where('pf.feature', $feature);
        $query = $this->db->get('review_feature pf');
        return($query->num_rows() > 0) ? $query : false;
    }

    function get_review_feature_by_category($category, $review = 0) {
        $this->db->select('cf.category, f.*, fv.value variant')
                ->join('features f', 'f.id = cf.feature', 'left')
                ->join('feature_variant fv', 'fv.id = cf.feature', 'left')
                ->where('cf.category', $category)
                ->group_by('cf.feature')
                ->order_by('f.sort_order ASC');

        if ($review) {
            $this->db->select('pf.review, pf.value')
                    ->join('review_feature pf', 'pf.feature = cf.feature AND pf.review = ' . $review, 'left');
        }
        return $this->db->get('category_feature cf');
    }

    function get_features_by_category($category, $review = 0) {
        $this->db->select('f.*, pf.value')
                ->join('features f', 'cf.feature = f.id', 'left')
                ->join('review_feature pf', 'pf.feature = cf.feature AND pf.review = ' . $review, 'left')
                ->where('cf.category', $category)
                ->group_by('cf.feature')
                ->order_by('f.sort_order ASC');
        return $this->db->get('category_feature cf');
    }

    function get_combinations($review, $combination = '') {
        $this->db->select("pc.*, GROUP_CONCAT(CONCAT(ag.name, ' - ', a.value) SEPARATOR ', ') combination")
                ->join('review_combination_attribute pca', 'pc.id = pca.review_combination', 'left')
                ->join('attributes a', 'pca.attribute = a.id', 'left')
                ->join('attribute_group ag', 'a.attribute_group = ag.id', 'left')
                ->where('pc.review', $review)
                ->group_by('pc.id');
        if ($combination) {
            $this->db->where('pc.id', $combination);
        }
        return $this->db->get('review_combination pc');
    }

    function check_exist_code($code, $review = '', $is_combination = false) {
        $query = $this->db->query("SELECT id, code FROM reviews p WHERE code = '$code'"
                . (($review && $is_combination) ? " AND id != " . $review : "")
                . " UNION"
                . " SELECT id, code FROM review_combination pc WHERE code = '$code'"
                . (($review && !$is_combination) ? " AND id != " . $review : ""));
        return ($query->num_rows() > 0) ? true : false;
    }

    function check_exist_combination($review, $combination) {
        $this->db->select("pc.id, GROUP_CONCAT(pca.attribute ORDER BY pca.attribute ASC SEPARATOR ',') attribute_group")
                ->join('review_combination_attribute pca', 'pca.review_combination = pc.id', 'left')
                ->where('pc.review', $review)
                ->group_by('pca.review_combination')
                ->having('attribute_group', $combination);
        return ($this->db->get('review_combination pc')->num_rows() > 0) ? true : false;
    }

//    function get_pack_review_search($search, $list_pack) {
//        $this->db->select('name')
//                ->join('features f', 'pf.feature = f.id', 'left')
//                ->where('pf.review', $review)
//                ->group_by('pf.feature');
//        return $this->db->get('review_feature pf');
//    }
}
