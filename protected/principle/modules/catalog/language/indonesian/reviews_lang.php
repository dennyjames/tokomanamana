<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['review_heading'] = 'Ulasan';
$lang['review_list_heading'] = 'Daftar Ulasan';
$lang['review_add_heading'] = 'Tambah Ulasan';
$lang['review_edit_heading'] = 'Edit Ulasan';

$lang['review_product_th'] = 'Produk';
$lang['review_customer_th'] = 'Pelanggan';
$lang['review_rating_th'] = 'Bintang';
$lang['review_title_th'] = 'Judul';
$lang['review_description_th'] = 'Deskripsi';
$lang['review_status_th'] = 'Aktif';
$lang['review_date_th'] = 'Tanggal';

$lang['review_save_success_message'] = "Ulasan '%s' berhasil disimpan.";
$lang['review_save_error_message'] = "Ulasan '%s' gagal disimpan.";
$lang['review_delete_success_message'] = "Ulasan '%s' berhasil dihapus.";
$lang['review_delete_error_message'] = "Ulasan '%s' gagal dihapus.";
$lang['review_exist_code_message'] = "Kode produk sudah ada.";
$lang['review_empty_image_message'] = "Gambar kosong! Setidaknya pilih satu gambar.";
$lang['review_friendly_url_exist_message'] = "Friendly URL sudah digunakan.";