<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['heading'] = 'Riwayat Stok';
$lang['list_heading'] = 'Daftar Riwayat Stok';
$lang['add_heading'] = 'Tambah Riwayat Stok';
$lang['edit_heading'] = 'Edit Riwayat Stok';

$lang['name'] = 'Nama';
$lang['price'] = 'Harga';
$lang['code'] = 'Kode Produk';
$lang['type'] = 'Tipe';
$lang['status'] = 'Status';
$lang['cabang'] = 'Cabang';
$lang['quantity'] = 'Kuantitas';
$lang['date_added'] = 'Tanggal';
$lang['Riwayat Stok_heading'] = 'Riwayat Stok';
$lang['Riwayat Stok_old'] = 'Riwayat Stok Lama';
$lang['Riwayat Stok_new'] = 'Riwayat Stok Baru';
$lang['Riwayat Stok_total'] = 'Total Riwayat Stok';
$lang['Riwayat Stok_sub_heading'] = 'Kelola Data Riwayat Stok';
$lang['Riwayat Stok_list_heading'] = 'Daftar Riwayat Stok';
$lang['Riwayat Stok_add_heading'] = 'Tambah Riwayat Stok';
$lang['Riwayat Stok_edit_heading'] = 'Edit Riwayat Stok';

$lang['Riwayat Stok_image_th'] = 'Gambar';
$lang['Riwayat Stok_name_th'] = 'Nama';
$lang['Riwayat Stok_seo_url_th'] = 'Friendly URL';

$lang['Riwayat Stok_form_general_tabs'] = 'Umum';
$lang['Riwayat Stok_form_seo_tabs'] = 'SEO';

$lang['Riwayat Stok_form_name_label'] = 'Nama';
$lang['Riwayat Stok_form_image_label'] = 'Gambar';
$lang['Riwayat Stok_form_description_label'] = 'Deskripsi';
$lang['Riwayat Stok_form_seo_url_label'] = 'Friendly URL';
$lang['Riwayat Stok_form_meta_title_label'] = 'Meta Judul';
$lang['Riwayat Stok_form_meta_description_label'] = 'Meta Deskripsi';
$lang['Riwayat Stok_form_meta_keyword_label'] = 'Meta Keyword';

$lang['Riwayat Stok_form_name_placeholder'] = 'Masukkan nama Riwayat Stok';
$lang['Riwayat Stok_form_description_placeholder'] = 'Masukkan deskripsi dari Riwayat Stok ini. Deskripsi akan ditampilkan di halaman yang menampilkan semua Riwayat Stok dari Riwayat Stok ini';

$lang['Riwayat Stok_save_success_message'] = "Riwayat Stok '%s' berhasil disimpan.";
$lang['Riwayat Stok_save_error_message'] = "Riwayat Stok '%s' gagal disimpan.";
$lang['Riwayat Stok_delete_success_message'] = "Riwayat Stok '%s' berhasil dihapus.";
$lang['Riwayat Stok_delete_error_message'] = "Riwayat Stok '%s' gagal dihapus.";
$lang['page_friendly_url_exist_message'] = "Friendly URL sudah digunakan.";