<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('brand_heading') ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('catalog/brands/save_new'); ?>" method="post" class="form-horizontal" id="form" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo set_value('id', ($data) ? encode($data->id) : ''); ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="name"><?php echo lang('brand_form_name_label'); ?></label>
                                            <div class="col-md-5">
                                                <input type="text" class="form-control" required="" name="name" value="<?php echo ($data) ? $data->name : ''; ?>" onkeyup="convertToSlug(this.value);" placeholder="<?php echo lang('brand_form_name_placeholder') ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('brand_form_image_label') ?></label>
                                            <div class="col-md-2" id="image-preview">
                                                <div class="thumbnail">
                                                    <div class="thumb">
                                                        <a href="<?php echo site_url('../files/images/' . $data->image) ?>" target="_blank">
                                                            <img src="<?php echo site_url('../files/images/' . $data->image) ?>">
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <label>
                                                    Upload Gambar <b style="color:red;">(*Max Size 2MB)</b>
                                                    <br>
                                                    Ekstensi File : <b style="color: red">JPG/PNG/JPEG</b>
                                                </label>
                                                <input type="file" class="form-control" name="image">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('brand_form_description_label'); ?></label>
                                            <div class="col-md-5">
                                                <textarea cols="30" rows="5" class="form-control" name="description" placeholder="<?php echo lang('brand_form_description_placeholder') ?>"><?php echo ($data) ? $data->description : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('brand_form_meta_keyword_label'); ?></label>
                                            <div class="col-md-5">
                                                <input type="text" name="meta_keyword" class="tags-input" placeholder="<?php echo lang('brand_form_meta_keyword_placeholder') ?>" value="<?php echo ($data) ? $data->meta_keyword : ''; ?>">
                                            </div>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('brand_form_status_label'); ?></label>
                                            <div class="col-md-5 status-brand">
                                                <input type="checkbox" name="status" value="1" data-on-text="<?php echo lang('enabled'); ?>" data-off-text="<?php echo lang('disabled'); ?>" class="switch" <?php echo ($data) ? (($data->status == 1) ? 'checked' : '') : 'checked'; ?>>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <!-- <a class="btn btn-default" href="<?php echo site_url('catalog/brands'); ?>"><?php echo lang('button_cancel'); ?></a> -->
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>