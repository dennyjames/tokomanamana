<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('review_heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table table-hover" id="table" data-url="<?php echo site_url('catalog/reviews/get_list'); ?>">
                <thead>
                    <tr>
                        <th><?php echo lang('review_product_th'); ?></th>
                        <th><?php echo lang('review_customer_th'); ?></th>
                        <th><?php echo lang('review_rating_th'); ?></th>
                        <th><?php echo lang('review_title_th'); ?></th>
                        <th class="no-sort"><?php echo lang('review_description_th'); ?></th>
                        <th><?php echo lang('review_status_th'); ?></th>
                        <th class="default-sort" data-sort="desc"><?php echo lang('review_date_th'); ?></th>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions_th'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>