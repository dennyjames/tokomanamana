<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table table-hover" id="table" data-url="<?php echo site_url('catalog/stock/get_list'); ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="asc"><?php echo lang('code'); ?></th>
                        <th><?php echo lang('name'); ?></th>
                        <th><?php echo lang('price'); ?></th>
                        <th><?php echo lang('category'); ?></th>
                        <th><?php echo lang('status'); ?></th>
                        <th><?= lang('stock_total') ?></th>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>