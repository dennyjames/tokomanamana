<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo $data->name; ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('catalog/options/variant_form/' . $data->id); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span><?php echo lang('option_variant_add_heading'); ?></span></a>

                    <a href="<?php echo site_url('catalog/options'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i> <span><?php echo lang('option_list_heading'); ?></span></a>
                    <!--<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>-->
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table table-hover" id="table" data-url="<?php echo site_url('catalog/options/get_variant_list/' . encode($data->id)); ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="asc"><?php echo lang('option_variant_value_th'); ?></th>
                        <?php if($data->type=='c'){ ?>
                        <th class="no-sort"><?php echo lang('option_variant_color_th'); ?></th>
                        <?php } ?>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions_th'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>