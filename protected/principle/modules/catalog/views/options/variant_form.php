<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('option_variant_edit_heading') : lang('option_variant_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('catalog/options'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('option_list_heading'); ?></span></a>
                    <?php if ($option) { ?>
                        <a href="<?php echo site_url('catalog/options/variants/' . encode($option->id)); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('option_variant_list_heading'); ?></span></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('catalog/options/variant_save'); ?>" method="post" class="form-horizontal" id="form">
                    <input type="hidden" name="id" value="<?php echo set_value('id', ($data) ? encode($data->id) : ''); ?>">
                    <input type="hidden" id="option" name="group" value="<?php echo $option->id; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="value"><?php echo lang('option_variant_form_value_label'); ?></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" required="" id="value" name="value" placeholder="<?php echo lang('option_variant_form_value_placeholder'); ?>" value="<?php echo ($data) ? $data->value : ''; ?>">
                                </div>
                            </div>
                            <?php if ($option->type == 'c') { ?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label"><?php echo lang('option_variant_form_color_label'); ?></label>
                                    <div class="col-md-1">
                                        <input type="color" class="form-control" required="" name="color" value="<?php echo ($data) ? $data->color : ''; ?>">
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('catalog/options/variants/' . encode($option->id)); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary" name="submit_new" value="1"><?php echo lang('button_save_new'); ?></button>
                                    <button type="submit" class="btn btn-primary" ><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>