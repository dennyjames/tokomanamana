<style type="text/css">
    #table_x_length {
        margin-right: :20px !important;
    }
</style>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">
                <form id="filter-form">
                    <div class="col-sm-5">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Filter By:</label>
                                    <select id="filter-by" class="form-control bootstrap-select">
                                        <option value="today">Hari Ini</option>
                                        <option value="yesterday">Kemarin</option>
                                        <option value="this month">Bulan Ini</option>
                                        <option value="last month">Bulan Lalu</option>
                                        <option value="this year">Tahun Ini</option>
                                        <option value="99">Kostum</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-4">
                                <div class="form-group">
                                    <label class="control-label">Dari:</label>
                                    <input type="text" class="form-control" readonly="" name="from" value="<?php echo date('Y-m-d'); ?>">
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-4 no-padding-right">
                                <div class="form-group">
                                    <label class="control-label">Sampai:</label>
                                    <input type="text" class="form-control" readonly="" name="to" value="<?php echo date('Y-m-d'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="col-xs-6 col-sm-8">
                            <div class="form-group">
                                <label class="control-label">Cabang</label>
                                <div class="form-group">
                                    <select name="branch" class="form-control bootstrap-select">
                                        <option value="">Semua</option>
                                        <?php if($branches = $this->main->gets('merchants', ['type' => 'principle_branch', 'principal_id' => $user->principle_id])) : ?>
                                            <?php foreach($branches->result() as $branch) : ?>
                                                <option value="<?= $branch->id ?>"><?= $branch->name ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-4">
                            <div class="form-group">
                                <label class="control-label" style="color: white">s</label><br>
                                <button class="btn btn-default" type="button" id="filter">Filter</button>
                            </div>
                        </div>
                    </div>
                </form>
                </div>
        </div>
        <div class="panel panel-flat">
            <div class="panel-body">
                <table class="table table-hover" id="table_x" data-url="<?php echo site_url('catalog/riwayat_stock/get_list'); ?>" data="stave-save">
                    <thead>
                        <tr>
                            <th><?php echo lang('code'); ?></th>
                            <th><?php echo lang('name'); ?></th>
                            <th><?php echo lang('cabang'); ?></th>
                            <th><?php echo lang('type'); ?></th>
                            <th><?php echo lang('quantity'); ?></th>
                            <th><?php echo lang('status'); ?></th>
                            <th class="default-sort" data-sort="desc"><?php echo lang('date_added'); ?></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</div>