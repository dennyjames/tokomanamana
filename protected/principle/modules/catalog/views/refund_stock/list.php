<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?= lang('heading') ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <!-- <a href="<?php echo site_url('settings/users/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span><?php echo lang('user_add_heading'); ?></span></a> -->
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table" id="table" data-url="<?php echo site_url('catalog/refund_stock/get_list'); ?>" data="stave-save">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th >Nama Produk</th>
                        <th >Cabang</th>
                        <th >Stok Baru</th>
                        <th class="default-sort" data-sort="desc">Tanggal</th>
                        <th class="no-sort">Opsi</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>