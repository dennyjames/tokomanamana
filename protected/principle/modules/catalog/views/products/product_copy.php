<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h2>Copy Product</h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('catalog/products'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('product_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('catalog/products/copy_save'); ?>" class="form-horizontal" method="post" id="form">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#general" data-toggle="tab">General</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">From Zona</label>
                                            <div class="col-md-9">
                                            <select class="bootstrap-select" name="from_zona" data-live-search="true" data-width="100%">
                                                    <?php if ($zonas) { ?>
                                                        <?php foreach ($zonas->result() as $zona) { ?>
                                                            <option value="<?php echo $zona->id; ?>"><?php echo $zona->name; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                            </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">To Zona</label>
                                            <div class="col-md-9">
                                            <select class="bootstrap-select" name="to_zona" data-live-search="true" data-width="100%">
                                                    <?php if ($zonas) { ?>
                                                        <?php foreach ($zonas->result() as $zona) { ?>
                                                            <option value="<?php echo $zona->id; ?>"><?php echo $zona->name; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                            </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>