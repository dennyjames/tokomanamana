<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <?php if ($this->aauth->is_allowed('catalog/product/add')) { ?>
                        <a href="<?php echo site_url('catalog/products/add'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span><?php echo lang('add_heading'); ?></span></a>
                    <?php } ?>
                    <?php if ($this->aauth->is_allowed('catalog/product/edit')) { ?>
                        <a href="javascript:void(0);" class="btn btn-link btn-float has-text" id="btn-import"><i class="icon-upload7 text-primary"></i><span>Impor</span></a>
                        <a href="javascript:void(0);" class="btn btn-link btn-float has-text" id="btn-export"><i class="icon-download7 text-primary"></i><span>Expor</span></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table table-hover" id="table" data-url="<?php echo site_url('catalog/products/get_list'); ?>">
                <thead>
                    <tr>
                        <th class="no-sort"><?php echo lang('image') ?></th>
                        <th class="default-sort" data-sort="asc"><?php echo lang('code'); ?></th>
                        <th><?php echo lang('name'); ?></th>
                        <th><?php echo lang('price'); ?></th>
                        <th><?php echo lang('category'); ?></th>
                        <th><?php echo lang('status'); ?></th>
                        <th>Jumlah <?php echo lang('merchant'); ?></th>
                        <th><?php echo lang('stock') ?></th>
                        <th class="no-sort text-center" ><?php echo lang('actions'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
<?php if ($this->aauth->is_allowed('catalog/product/edit')) { ?>
    <div id="modal-import" class="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="top: 35%">&times;</button>
                    <h5 class="modal-title">Impor</h5>
                    <hr style="margin-bottom: 0px;">
                </div>
                <div class="modal-body">
                    <div class="panel panel-danger">
                      <div class="panel-heading" style="padding: 5px 20px;background: #D9534F;color: white;border-color: #D9534F;">
                        Langkah-langkah import produk
                      </div>
                      <div class="panel-body" style=" padding-bottom: 0;padding-top: 9px;">
                        <ol>
                            <li>Download template impor terbaru</li>
                            <li>Download list-list impor terbaru yang digunakan untuk template</li>
                            <li>Isi template dengan teliti mengikuti contoh dan list-list</li>
                            <li>Upload file template yang sudah terisi dengan benar</li>
                        </ol>
                      </div>
                    </div>
                    <form class="form-horizontal" action="<?php echo site_url('catalog/products/import'); ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="control-label col-sm-3">File Impor</label>
                            <div class="col-sm-9">
                                <input type="file" id="file" name="file" class="form-control" required="">
                            </div>
                            <i style="color:#D9534F;float: right;padding-right: 14px;">Gunakan template untuk mengimpor produk!</i>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <a href="<?php echo current_url() . '/download_template_import' ?>">
                        <button type="button" style="float:left; margin: 3px;background: #5CB85C;border-color: #5CB85C;" class="btn btn-success">Unduh Template Impor</button>
                    </a>
                    <a href="<?php echo current_url() . '/download_lists' ?>">
                        <button type="button" style="float:left; margin: 3px;background: #5CB85C;border-color: #5CB85C;" class="btn btn-success">Unduh List-list Impor</button>
                    </a>
                    <button type="button" class="btn btn-link" data-dismiss="modal">Tutup</button>
                    <button type="button" id="submit-import" class="btn btn-primary" style="background: #428BCA;border-color: #428BCA">Unggah</button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>