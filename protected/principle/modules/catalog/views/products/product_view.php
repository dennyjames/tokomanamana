<style>
    .form-group{
        margin-bottom: 5px !important;
    }
    .form-horizontal .control-label,
    .form-control-static{
        padding-top: 0!important;
        padding-bottom: 0!important;
        min-height: 1px;
        margin-bottom: 5px;
    }
</style>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo $product->name; ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('catalog/products'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('product_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <form class="form-horizontal" action="#">
            <div class="row">
                <div class="col-sm-4">
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h6 class="panel-title">Gambar</h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row align-center">
                                <div class="thumbnail">
                                    <div class="thumb">
                                        <img id="img-thumb" src="<?php echo site_url('../files/images/' . $product_images->row()->image); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?php foreach ($product_images->result() as $image) { ?>
                                    <div class="col-sm-3 pr-5 pl-5">
                                        <div class="thumbnail no-margin">
                                            <div class="thumb">
                                                <a href="javascript:change_image('<?php echo site_url('../files/images/' . $image->image); ?>')"><img src="<?php echo site_url('../files/images/' . $image->image); ?>"></a>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h6 class="panel-title">Informasi Produk</h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label col-sm-3 text-bold"><?php echo lang('product_form_code_label'); ?></label>
                                <div class="col-sm-9">
                                    <div class="form-control-static"><?php echo $product->code; ?></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 text-bold"><?php echo lang('product_form_name_label'); ?></label>
                                <div class="col-sm-9">
                                    <div class="form-control-static"><?php echo $product->name; ?></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 text-bold"><?php echo lang('product_form_category_label'); ?></label>
                                <div class="col-sm-9">
                                    <div class="form-control-static"><?php echo ($category) ? $category->name : '-'; ?></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 text-bold"><?php echo lang('product_form_brand_label'); ?></label>
                                <div class="col-sm-9">
                                    <div class="form-control-static"><?php echo ($brand) ? $brand->name : '-'; ?></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 text-bold"><?php echo lang('product_form_short_description_label'); ?></label>
                                <div class="col-sm-9">
                                    <div class="form-control-static"><?php echo nl2br($product->short_description); ?></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 text-bold"><?php echo lang('product_form_description_label'); ?></label>
                                <div class="col-sm-9">
                                    <div class="form-control-static"><?php echo tinymce_get_url_files($product->description); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h6 class="panel-title">Data</h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="control-label col-sm-3 text-bold"><?php echo lang('product_form_price_label'); ?></label>
                                <div class="col-sm-9">
                                    <div class="form-control-static"><?php echo number($product->price); ?></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 text-bold"><?php echo lang('product_form_weight_label'); ?></label>
                                <div class="col-sm-9">
                                    <div class="form-control-static"><?php echo $product->weight . 'gram'; ?></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 text-bold"><?php echo lang('product_form_seo_url_label'); ?></label>
                                <div class="col-sm-9">
                                    <div class="form-control-static"><?php echo $product->seo_url; ?></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 text-bold"><?php echo lang('product_form_meta_title_label'); ?></label>
                                <div class="col-sm-9">
                                    <div class="form-control-static"><?php echo $product->meta_title; ?></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 text-bold"><?php echo lang('product_form_meta_description_label'); ?></label>
                                <div class="col-sm-9">
                                    <div class="form-control-static"><?php echo $product->meta_description; ?></div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3 text-bold"><?php echo lang('product_form_meta_keyword_label'); ?></label>
                                <div class="col-sm-9">
                                    <div class="form-control-static"><?php echo $product->meta_keyword; ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h6 class="panel-title">Spesifikasi</h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($product_features->num_rows() > 0) {
                                foreach ($product_features->result() as $feature) {
                                    ?>
                                    <div class="form-group">
                                        <label class="control-label col-sm-3 text-bold"><?php echo $feature->name; ?></label>
                                        <div class="col-sm-9">
                                            <div class="form-control-static">
                                                <?php
                                                if ($feature->type == 'i' || $feature->type == 't') {
                                                    echo ($feature->value) ? $feature->value : '-';
                                                } else {
                                                    if ($feature->type == 's') {
                                                        echo ($feature->variant) ? $feature->variant : '-';
                                                    } elseif ($feature->type == 'c') {
                                                        $variants = $this->products->get_product_feature_variants($feature->product, $feature->id);
                                                        if ($variants) {
                                                            echo '<ul style="padding:0; margin: 0 0 0 16px">';
                                                            foreach ($variants->result() as $variant)
                                                                echo '<li>' . $variant->value . '</li>';
                                                            echo '</ul>';
                                                        }
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>