<style type="text/css">#description_ifr{width:99%!important}.product_title{margin-bottom:18px;margin-left:10px;margin-right:10px}.wajib{color:#fff;bottom:0;display:inline-block;margin-top:3px;margin-left:1px;margin-right:1px;position:relative;z-index:1;padding:2px 5px;background-color:#97c23c;font-size:10px;border-radius:2px}.circle{color:#fff;bottom:0;display:inline-block;margin-left:3px;position:relative;z-index:1;padding:4px;background-color:#97c23c;border-radius:50px}.nav-tabs{border-bottom:1px solid #97c23c}.nav-tabs>li.active>a{border:1px solid #97c23c;border-bottom-color:transparent}.page-title{width:auto !important;}#mceu_18{overflow-x: scroll;}.nav-tabs:before {content: 'Tabs .Produk';}#product-form #images{min-height: 163px;}h4{display: inline-block;}.info_price{font-size:small;display: inline-block;color:gray;}.inactive{background-color: #ccc;opacity: 0.3;}.panel-variasi-colour{margin-bottom: 10px !important;}.form-group-grosir{margin-left:0px !important;}#table-container{overflow:auto}.checkbox-status-variant{height:0;width:0;visibility:hidden;display:none}.label-status-variant{cursor:pointer;text-indent:-9999px;width:70px;height:35px;background:grey;display:block;border-radius:100px;position:relative}.label-status-variant:after{content:'';position:absolute;top:3px;left:4px;width:30px;height:30px;background:#fff;border-radius:90px;transition:.3s}.checkbox-status-variant:checked+.label-status-variant{background:#bada55}.checkbox-status-variant:checked+.label-status-variant:after{left:calc(100% - 4px);transform:translateX(-100%)}.checkbox-status-variant:active:after{width:70px}</style>
<div class="content-wrapper" id="product-form">
    <form action="<?php echo site_url('catalog/products/save') ?>" class="" method="post" id="form">
        <div class="page-header page-header-default">
            <div class="page-header-content">
                <div class="page-title">
                    <!-- <input class="form-control big-input" required="" type="text" name="name" value="<?= ($data) ? $data->name : ''; ?>" placeholder="<?php echo lang('name_placeholder'); ?>"> -->
                    <h1><?php echo lang('add_heading') ?></h1>
                </div>

                <div class="heading-elements">
                    <div class="heading-btn-group">
                        <a href="<?php echo site_url('catalog/products'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('list_heading'); ?></span></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12">
                    <input type="hidden" name="id" id="id" value="<?php echo $data->id ; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#general" data-toggle="tab"><?php echo lang('general_tabs'); ?></a></li>
                                    <li id="option-tab" class=""><a id="a_option" href="#option" data-toggle="tab"><?php echo lang('option_tabs'); ?></a></li>
                                    <li><a href="#price" data-toggle="tab"><?php echo lang('price_tabs'); ?></a></li>
                                    <!-- <li class=""><a href="#feature" data-toggle="tab"><?php //echo lang('feature_tabs'); ?></a></li> -->
                                    <li class=""><a href="#seo" data-toggle="tab"><?php echo lang('seo_tabs'); ?></a></li>
                                    <li id="package-tab" class=""><a href="#package" data-toggle="tab">Package</a></li>
                                    <li id="promo-tab" class=""><a href="#promo" data-toggle="tab">Promo</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="row">
                                            <div class="product_title">
                                                <label><?php echo lang('name_product'); ?></label>
                                                <div class="wajib">Wajib</div>
                                                <input class="form-control big-input" required="" type="text" name="name" minlength="4" maxlength="70" value="<?php echo $data->name; ?>" placeholder="<?php echo lang('name_placeholder'); ?>">
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label><?php echo lang('image'); ?></label>
                                                    <div class="wajib">Wajib</div>
                                                    <div id="images">
                                                        <?php
                                                        if ($images) {
                                                            foreach ($images->result() as $image) {
                                                                echo '<div class="image" id="img' . $image->id . '"><div class="image-bg" style="background-image: url(\'' . base_url('../files/images/' . $image->image) . '\')"></div>'
                                                                . '<div class="option">'
                                                                . '<div class="radio"><label><input type="radio" value="' . $image->id . '" ' . (($image->primary) ? 'checked' : '') . ' name="image_primary" id="image_primary' . $image->id . '">Default</label></div>'
                                                                . '<div><a href="javascript:delete_image(\'' . $image->id . '\')" class="text-muted pull-right"><i class="icon-trash"></i></a></div>'
                                                                . '</div>'
                                                                . '</div>';
                                                            }
                                                        }
                                                        ?>
                                                        <div class="image" id="add-image">
                                                            <div>
                                                                <span>+</span>
                                                            </div>
                                                            <input type="hidden" id="add-image-value">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('short_description'); ?></label>
                                                    <div class="wajib">Wajib</div>
                                                    <textarea cols="30" rows="2" class="form-control" id="short_description" name="short_description" placeholder="<?php echo lang('short_description_placeholder'); ?>"><?php echo ($data) ? $data->short_description : ''; ?></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('description'); ?></label>
                                                    <textarea cols="30" rows="2" class="form-control tinymce" id="description" name="description" placeholder="<?php echo lang('description_placeholder'); ?>"><?php echo ($data) ? $data->description : ''; ?></textarea>
                                                </div>
                                                <div class="form-group" style="border-bottom : 1px solid #999">
                                                    <label><?php echo lang('table_description'); ?></label>
                                                    <table class="table table-hover" id="table_desk">
                                                        <thead>
                                                            <tr>
                                                                <th>Param</th>
                                                                <th>Value</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <!-- ISI DARI AJAX -->
                                                            <tr>
                                                                <td colspan="2" style="font-style:italic"><center>*Silahkan Pilih Kategori 
                                                                 Terlebih dahulu</center></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <table class="table table-hover" id="param_other_table_desk">
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                    <input type="hidden" id="count_btn" value="0"/>
                                                    <div id="add_param_table_desk">
                                                        
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('status'); ?></label>
                                                    <input type="checkbox" name="status" value="1" data-on-text="<?php echo lang('enabled'); ?>" data-off-text="<?php echo lang('disabled'); ?>" class="switch" <?php echo ($data) ? (($data->status == 1) ? 'checked' : '') : 'checked'; ?>>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label><?php echo lang('code'); ?></label>
                                                    <input type="text" name="code" id="code"  class="form-control" placeholder="<?php echo lang('code_placeholder') ?>" value="<?php echo ($data) ? $data->code : ''; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Tipe Produk</label>
                                                    <select class="bootstrap-select"  name="type" id="type" data-live-search="true" data-width="100%">
                                                        <option value="s" <?php echo ($data) ? ($data->type == 's') ? 'selected' : '' : ''; ?>>Standard</option>
                                                        <option value="p" <?php echo ($data) ? ($data->type == 'p') ? 'selected' : '' : ''; ?>>Package</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Jenis</label>
                                                    <select class="bootstrap-select" name="preorder" id="preorder" data-width="100%">
                                                        <option value="0" <?= ($data) ? ($data->preorder == 0) ? 'selected' : '' : '' ?> >Ready Stock</option>
                                                        <option value="1" <?= ($data) ? ($data->preorder == 1) ? 'selected' : '' : '' ?> >Pre-Order</option>
                                                        <option value="2" <?= ($data) ? ($data->preorder == 3) ? 'selected' : '' : '' ?> >Coming Soon</option>
                                                    </select>
                                                </div>
                                                <div class="form-group" id="preorder-time">
                                                    <label class="label-time"></label>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <input style="width: 60px;" id="preorder-time-id" type="number" name="preorder_time" class="form-control" value="<?= ($data) ? ($data->preorder_time != null) ? $data->preorder_time : '' : '' ?>">
                                                                <span class="input-group-addon">Hari</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="coming-soon-time">
                                                    <label class="label-time"></label>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="input-group">
                                                                <span class="input-group-addon">Tanggal</span><input class="form-control" type="text" name="coming_soon_time" id="coming-soon-time-id" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Promo</label>
                                                    <select class="bootstrap-select"  name="promo" id="promo-slc" data-width="100%">
                                                        <option value="0" <?php echo ($data) ? ($data->promo == '0') ? 'selected' : '' : ''; ?>>No</option>
                                                        <option value="1" <?php echo ($data) ? ($data->promo == '1') ? 'selected' : '' : ''; ?>>Yes</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Variasi</label>
                                                    <select class="bootstrap-select" name="variation" id="option-slc" data-width="100%">
                                                        <option value="0" <?php echo ($data) ? ($data->variation == '0') ? 'selected' : '' : ''; ?>>No</option>
                                                        <option value="1" <?php echo ($data) ? ($data->variation == '1') ? 'selected' : '' : ''; ?>>Yes</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('category'); ?></label>
                                                    <div class="wajib">Wajib</div>
                                                    <select class="bootstrap-select"  name="category" id="category" data-live-search="true" data-width="100%" onchange="get_table_deskripsi()">
                                                        <option value=""></option>
                                                        <?php if ($categories) { ?>
                                                            <?php foreach ($categories->result() as $category) { ?>
                                                                <option value="<?php echo $category->id; ?>" <?php echo ($data) ? ($data->category == $category->id) ? 'selected' : '' : ''; ?>><?php echo $category->name; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('brand'); ?></label>
                                                    <select class="bootstrap-select" name="brand" id="brand" data-live-search="true" data-width="100%">
                                                        <option value=""></option>
                                                        <?php if ($brands) { ?>
                                                            <?php foreach ($brands->result() as $brand) { ?>
                                                                <option value="<?php echo $brand->id; ?>" <?php echo ($data) ? ($data->brand == $brand->id) ? 'selected' : '' : ''; ?>><?php echo $brand->name; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label><?= lang('etalase') ?></label>
                                                    <select name="etalase" id="etalase" class="bootstrap-select" data-width="100%">
                                                        <option value=""></option>
                                                        <?php if($store_fronts) : ?>
                                                            <?php foreach($store_fronts->result() as $store_front) : ?>
                                                                <option value="<?= $store_front->id ?>" <?= ($data) ? ($data->etalase == $store_front->id) ? 'selected' : '' : '' ?>><?= $store_front->name ?></option>
                                                            <?php endforeach; ?>
                                                        <?php else : ?>
                                                            <option value="" disabled>Buat Etalase Terlebih Dahulu</option>
                                                        <?php endif; ?>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('dimension'); ?></label>
                                                    <div class="wajib">Wajib</div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <input type="number" name="length" class="form-control" required="" value="<?php echo ($data) ? $data->length : 0; ?>">
                                                                <span class="input-group-addon">cm</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <input type="number" name="width" class="form-control" required="" value="<?php echo ($data) ? $data->width : 0; ?>">
                                                                <span class="input-group-addon">cm</span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="input-group">
                                                                <input type="number" name="height" class="form-control" required="" value="<?php echo ($data) ? $data->height : 0; ?>">
                                                                <span class="input-group-addon">cm</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('weight'); ?></label>
                                                    <div class="wajib">Wajib</div>
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <div class="input-group">
                                                                <input type="number" name="weight" class="form-control" value="<?php echo ($data) ? $data->weight : 0; ?>">
                                                                <span class="input-group-addon">gram</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label><?php echo lang('stock'); ?></label>
                                                    <div class="wajib">Wajib</div>
                                                    <input type="text" name="quantity" id="stock"  class="form-control" value="<?php echo ($data) ? $data->quantity : ''; ?>" onkeypress="if(event.which &lt; 48 || event.which &gt; 57 ) if(event.which != 8) return false;">
                                                </div>
                                                <div class="form-group">
                                                    <label>Free Ongkir</label>
                                                    <select class="bootstrap-select"  name="free_ongkir" id="free_ongkir" data-width="100%">
                                                        <option value="0" <?php echo ($data) ? ($data->free_ongkir == '0' || $data->free_ongkir == NULL) ? 'selected' : '' : ''; ?>>No</option>
                                                        <option value="1" <?php echo ($data) ? ($data->free_ongkir == '1') ? 'selected' : '' : ''; ?>>Yes</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane form-horizontal" id="price">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('price'); ?> <div class="wajib">Wajib</div></label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Rp</span>
                                                    <input type="text" name="price" class="form-control number" value="<?php echo ($data) ? $data->price : 0; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class="row p-10" style="max-height: 176px; overflow: auto;">
                                            <?php
                                            // if ($prices) {
                                            //     foreach ($prices->result() as $price) {
                                                    ?>
                                                    <div class="col-sm-4 well p-10">
                                                        <label><?php //echo $price->name; ?></label>
                                                        <div class="row">
                                                            <div class="col-xs-8">
                                                                <div class="has-feedback has-feedback-left">
                                                                    <input type="text" class="form-control input-sm number" name="prices[<?php //echo $price->merchant_group; ?>][price]" value="<?php //echo ($price->price) ? $price->price : 0; ?>">
                                                                    <div class="form-control-feedback">
                                                                        Rp
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                            //     }
                                            // }
                                            ?>
                                        </div> -->
                                        <hr>
                                        <h4><?php echo lang('price_level') ?></h4>
                                        <input type="hidden" id="price-count" value="1">
                                        <table id="price-list" class="table">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10%">Min QTY</th>
                                                    <th style="width: 30%">Harga</th>
                                                    <!-- <th>Kelompok Harga</th> -->
                                                    <th style="width: 50px"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if ($price_levels) {
                                                    foreach ($price_levels->result() as $price) {
                                                        $groups = $this->products->price_level_groups($price->id);
                                                        ?>
                                                        <tr>
                                                            <td class="price-qty"><input type="number" name="price_level[<?php echo $price->id; ?>][qty]" class="form-control" value="<?php echo $price->min_qty; ?>"></td>
                                                            <td>
                                                                <div class="input-group"><span class="input-group-addon">Rp</span><input type="text" name="price_level[<?php echo $price->id; ?>][price]" class="form-control number" value="<?php echo $price->price; ?>"></div>
                                                            </td>
                                                            <!-- <td> -->
                                                                <!-- <div class="row p-10" style="max-height: 176px; overflow: auto;"> -->
                                                                    <?php
                                                                    // if ($groups) {
                                                                    //     foreach ($groups->result() as $group) {
                                                                            ?>
                                                                            <!-- <div class="col-sm-6 well p-10">
                                                                                <label><?php echo $group->name; ?></label>
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <div class="has-feedback has-feedback-left">
                                                                                            <input type="text" class="form-control input-sm number" name="price_level[<?php echo $price->id; ?>][group][<?php echo $group->merchant_group; ?>][price]" value="<?php echo ($group->price) ? $group->price : 0; ?>">
                                                                                            <div class="form-control-feedback">
                                                                                                Rp
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div> -->
                                                                            <?php
                                                                    //     }
                                                                    // }
                                                                    ?>
                                                                <!-- </div> -->
                                                            <!-- </td> -->
                                                            <td><button type="button" class="btn btn-danger btn-xs remove-price">x</button></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                <?php } ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="4"><button type="button" id="add-price" class="btn btn-success">Add Price</button></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                        <hr>
                                        <h4><?php echo lang('price_reseller') ?></h4>
                                        <input type="hidden" id="price-count-reseller" value="1">
                                        <table id="price-list-reseller" class="table">
                                            <thead>
                                                <tr>
                                                    <th style="width: 10%">Min QTY</th>
                                                    <th style="width: 30%">Harga</th>
                                                    <!-- <th>Kelompok Harga</th> -->
                                                    <th style="width: 50px"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if ($price_reseller) {
                                                    foreach ($price_reseller->result() as $price) {
                                                        $groups = $this->products->price_level_groups($price->id);
                                                        ?>
                                                        <tr>
                                                            <td class="price-qty"><input type="number" name="price_reseller[<?php echo $price->id; ?>][qty]" class="form-control" value="<?php echo $price->min_qty; ?>"></td>
                                                            <td>
                                                                <div class="input-group"><span class="input-group-addon">Rp</span><input type="text" name="price_reseller[<?php echo $price->id; ?>][price]" class="form-control number" value="<?php echo $price->price; ?>"></div>
                                                            </td>
                                                            <!-- <td> -->
                                                                <!-- <div class="row p-10" style="max-height: 176px; overflow: auto;"> -->
                                                                    <?php
                                                                    // if ($groups) {
                                                                    //     foreach ($groups->result() as $group) {
                                                                            ?>
                                                                            <!-- <div class="col-sm-6 well p-10">
                                                                                <label><?php echo $group->name; ?></label>
                                                                                <div class="row">
                                                                                    <div class="col-xs-12">
                                                                                        <div class="has-feedback has-feedback-left">
                                                                                            <input type="text" class="form-control input-sm number" name="price_level[<?php echo $price->id; ?>][group][<?php echo $group->merchant_group; ?>][price]" value="<?php echo ($group->price) ? $group->price : 0; ?>">
                                                                                            <div class="form-control-feedback">
                                                                                                Rp
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div> -->
                                                                            <?php
                                                                    //     }
                                                                    // }
                                                                    ?>
                                                                <!-- </div> -->
                                                            <!-- </td> -->
                                                            <td><button type="button" class="btn btn-danger btn-xs remove-price">x</button></td>
                                                        </tr>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                <?php } ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="4"><button type="button" id="add-price-reseller" class="btn btn-success">Add Price</button></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                    <div class="tab-pane" id="option">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="alert alert-info">
                                                    <p>Untuk membuat variasi baru, pilih nilai variasi kemudian klik "Hasilkan"</p>
                                                </div>
                                                <table id="combination-list" class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Default</th>
                                                            <th>Variasi</th>
                                                            <th>Harga</th>
                                                            <th>Berat</th>
                                                            <th style="width: 130px"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php if ($product_options) { ?>
                                                            <?php foreach ($product_options->result() as $po) { ?>
                                                                <tr id="option-<?php echo $po->id; ?>">
                                                                    <td><input type="radio" class="form-control" name="default_option" <?php echo ($po->default==1)?'checked':''; ?> value="<?php echo $po->id; ?>"></td>
                                                                    <td><?php echo $po->option_combination; ?></td>
                                                                    <td><input type="text" class="number form-control" name="options[<?php echo $po->id; ?>][price]" value="<?php echo $po->price; ?>"></td>
                                                                    <td><input type="text" class="number form-control" name="options[<?php echo $po->id; ?>][weight]" value="<?php echo $po->weight; ?>"></td>
                                                                    <td>
                                                                        <button type="button" class="btn btn-default btn-xs" onclick="set_image_option(<?php echo $data->id; ?>,<?php echo $po->id; ?>)"><i class="icon-image2"></i></button>
                                                                        <button type="button" class="btn btn-danger btn-xs" onclick="delete_option(<?php echo $po->id; ?>)"><i class="icon-x"></i></button>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div id="option-list" class="col-md-3 panel-group content-group-lg">
                                                <?php if ($option_groups) { ?>
                                                    <?php foreach ($option_groups->result() as $og) { ?>
                                                        <div class="panel panel-white">
                                                            <div class="panel-heading">
                                                                <h6 class="panel-title">
                                                                    <a data-toggle="collapse" href="#option-group-<?php echo $og->id; ?>" aria-expanded="true" class=""><?php echo $og->name; ?></a>
                                                                </h6>
                                                            </div>
                                                            <div id="option-group-<?php echo $og->id; ?>" class="panel-collapse collapse in" aria-expanded="true" style="">
                                                                <div class="panel-body">
                                                                    <?php if ($options = $this->main->gets('options', array('group' => $og->id), 'sort_order')) { ?>
                                                                        <?php foreach ($options->result() as $option) { ?>
                                                                            <div class="checkbox">
                                                                                <label>
                                                                                    <input type="checkbox" name="option[<?php echo $og->id; ?>][<?php echo $option->id; ?>]" class="styled options" value="<?php echo $option->id; ?>"> <?php echo $option->value; ?>
                                                                                </label>
                                                                            </div>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                <?php } ?>
                                                <button type="button" id="option-generate" class="btn border-slate text-slate-800 btn-flat mt-10" style="width: 100%">Hasilkan</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane form-horizontal" id="feature" style="display: none;">
                                        <div id="feature-form">
                                            <?php
                                                if ($features->num_rows() > 0) {
                                                    foreach ($features->result() as $feature) {
                                                        ?>
                                                        <div class="form-group">
                                                            <label class="col-md-3 control-label"><?php echo $feature->name; ?></label>
                                                            <div class="col-md-9">
                                                                <?php
                                                                if ($feature->type == 'i') {
                                                                    echo '<input type="text" name="feature[' . $feature->id . '][value]" class="form-control" value="' . $feature->value . '">';
                                                                } elseif ($feature->type == 't') {
                                                                    echo '<textarea name="feature[' . $feature->id . '][value]" class="form-control">' . $feature->value . '</textarea>';
                                                                } else {
                                                                    $variants = $this->main->gets('feature_variant', array('feature' => $feature->id));
                                                                    if ($feature->type == 's') {
                                                                        echo '<select name="feature[' . $feature->id . '][value]" class="form-control">';
                                                                        echo '<option value=""></option>';
                                                                        foreach ($variants->result() as $variant) {
                                                                            echo '<option value="' . $variant->id . '"' . (($variant->id == $feature->value) ? 'selected' : '') . '>' . $variant->value . '</option>';
                                                                        }
                                                                        echo '</select>';
                                                                    } elseif ($feature->type == 'c') {
                                                                        $values = array();
                                                                        if ($product) {
                                                                            $feature_values = $this->main->gets('product_feature', array('product' => $product, 'feature' => $feature->id));
                                                                            if ($feature_values) {
                                                                                foreach ($feature_values->result() as $value) {
                                                                                    $values[] = $value->value;
                                                                                }
                                                                            }
                                                                        }
                                                                        foreach ($variants->result() as $variant) {
                                                                            echo '<div class="checkbox"><label><input name="feature[' . $feature->id . '][value][]" type="checkbox" value="' . $variant->id . '" ' . ((in_array($variant->id, $values)) ? 'checked' : '') . '> ' . $variant->value . ' </label></div>';
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    }
                                                }else{
                                                    echo lang('feature_hidden_info');
                                                }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="tab-pane form-horizontal" id="seo">
                                        <!-- <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('seo_url'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="seo_url" name="seo_url" value="<?php echo ($data) ? $data->seo_url : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('meta_title'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="meta_title" placeholder="<?php echo lang('meta_title_placeholder'); ?>" value="<?php echo ($data) ? $data->meta_title : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('meta_description'); ?></label>
                                            <div class="col-md-9">
                                                <textarea cols="30" rows="2" class="form-control" name="meta_description" placeholder="<?php echo lang('meta_description_placeholder'); ?>"><?php echo ($data) ? $data->meta_description : ''; ?></textarea>
                                            </div>
                                        </div> -->
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('meta_keyword'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="meta_keyword" class="tags-input" value="<?php echo ($data) ? $data->meta_keyword : ''; ?>" placeholder="Masukkan Kata Kunci">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane form-horizontal" id="package">
                                        <div id="package-lists">
                                        <?php
                                            if (isset($data) && isset($data->package_items)) {
                                                foreach ($data->package_items as $item) {
                                                    $key = $item->row_id;
                                        ?>
                                            <div class="row" id="rowID-<?php echo $key;?>">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Item Name</label>
                                                        <div class="col-md-9">
                                                            <input id="packageItem<?php echo $key;?>" class="autocompleteTxt form-control" type="text" name="package_items[<?php echo $key;?>][product_name]" value="<?php echo $item->product_name?>">
                                                            <input id="packageItemRowId<?php echo $key;?>" class="form-control" type="hidden" name="package_items[<?php echo $key;?>][row_id]" value="<?php echo $item->row_id?>">
                                                            <input id="packageItemId<?php echo $key;?>" class="form-control" type="hidden" name="package_items[<?php echo $key;?>][product_id]" value="<?php echo $item->product_id?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="col-md-3 control-label">Qty</label>
                                                        <div class="col-md-9">
                                                            <input class="form-control" type="number" name="package_items[<?php echo $key;?>][qty]" value="<?php echo $item->qty?>">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <button type="button" class="btn btn-danger btn-xs remove-item" data-id="<?php echo $key;?>">x</button>
                                                </div>
                                            </div>
                                        <?php
                                                }
                                            }
                                        ?>
                                        </div>
                                        <div class="text-center"><button type="button" id="add-package-item" class="btn btn-success">Add Package Item</button></div>
                                    </div>
                                    <div class="tab-pane form-horizontal" id="promo">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Jenis</label>
                                            <div class="col-md-3">
                                                <select class=" bootstrap-select" id="input_type_promo" name="promo_data[type]" required="">
                                                    <option id="fixed_amount" value="F" <?php if(($data ? ($data->promo_data ? $data->promo_data->type : NULL) : '') == 'F'){ echo "selected";}?>>Fixed Amount</option>
                                                    <option id="percentage" value="P" <?php if(($data ? ($data->promo_data ? $data->promo_data->type : NULL) : '') == 'P'){ echo "selected";}?>>Percentage</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Tipe Promo</label>
                                            <div class="col-md-3">
                                                <select class="form-control bootstrap-select" name="promo_data[coupon_type]" required="">
                                                    <option <?php if(($data ? ($data->promo_data ? $data->promo_data->coupon_type : '') : '') == 'all'){ echo "selected";}?> value="all">All</option>
                                                    <option <?php if(($data ? ($data->promo_data ? $data->promo_data->coupon_type : '') : '') == 'branch'){ echo "selected";}?> value="branch">Branch</option>
                                                    <option <?php if(($data ? ($data->promo_data ? $data->promo_data->coupon_type : '') : '') == 'merchant'){ echo "selected";}?> value="merchant">Merchant</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Available</label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control date" name="promo_data[date_start]" value="<?php echo ($data) ? ($data->promo_data ? $data->promo_data->date_start : '')  : '' ?>" placeholder="">
                                            </div>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control date" name="promo_data[date_end]" value="<?php echo ($data) ? ($data->promo_data ? $data->promo_data->date_end : '')  : '' ?>" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Discount</label>
                                            <div class="col-md-3">
                                                <input type="text" class="form-control number" name="promo_data[discount]" value="<?php echo ($data) ? ($data->promo_data ? $data->promo_data->discount : '')  : '' ?>">
                                            </div>
                                            <p id="percent_tag" style="margin-top:9px">%</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <!-- <a class="text-muted" href="<?php //echo site_url('catalog/products'); ?>"><i class="icon-trash"></i></a> -->
                                <a class="btn btn-default" href="<?php echo site_url('catalog/products'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <div class="btn-group dropup">
                                        <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?php echo site_url('catalog/products'); ?>">Ke Produk</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<input type="hidden" name="username" id="username" value="<?php echo $username; ?>">
<div id="filemanager" class="modal">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">File Manager</h5>
            </div>

            <div class="modal-body">
                <iframe  width="100%" height="550" frameborder="0" src="<?php echo site_url('filemanager/dialog.php?type=1&folder='.$username.'&editor=false&field_id=add-image-value&relative_url=1'); ?>"></iframe>
            </div>
        </div>
    </div>
</div>
<div id="option-image" class="modal">
    <input type="hidden" name="product_option">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Pilih Gambar Untuk Variasi Ini</h5>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" onclick="save_image_option()">Simpan</button>
            </div>
        </div>
    </div>
</div>
<script>
    var groups = JSON.parse('<?php echo json_encode($this->products->price_level_groups()->result()); ?>');
</script>