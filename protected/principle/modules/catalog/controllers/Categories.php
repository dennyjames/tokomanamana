<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Categories extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->aauth->control('catalog/category');

        $this->lang->load('categories', settings('language'));
        $this->load->model('categories_model', 'categories');
        $this->data['menu'] = 'catalog_category';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('category'), '/catalog/categories');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('category_heading'));
        $this->load->view('categories/categories', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->categories->get_all($start, $length, $search, $order);
//        log_message('debug', $this->db->last_query());
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->name,
                    ($data->home_show == 1) ? 'Ya' : 'Tidak',
                    $data->home_sort_order,
                    '<td class="text-center">
                    <ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="' . site_url('catalog/categories/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>
                    <li><a href="' . site_url('catalog/categories/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>
                    </ul>
                    </li>
                    </ul>
                    </td>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->categories->count_all();
        $output['recordsFiltered'] = $this->categories->count_all($search);
        echo json_encode($output);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->load->js('../assets/backend/js/plugins/forms/duallistbox.min.js');
        $this->load->js('../assets/backend/js/modules/catalog/category_form.js');

        $this->data['data'] = array();
        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('category'), '/catalog/categories');

        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->main->get('categories', array('id' => $id));
            $this->data['data']->description = tinymce_get_url_files($this->data['data']->description);
            $this->breadcrumbs->push(lang('category_edit_heading'), '/catalog/categories/form/' . encode($id));
            $this->breadcrumbs->push($this->data['data']->name, '/');
        } else {
            $this->breadcrumbs->push(lang('category_add_heading'), '/catalog/categories/form');
        }

        $this->data['categories'] = $this->categories->get_all_parents($id);
        $this->data['features'] = $this->main->gets('features', array(), 'name asc');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(($this->data['data']) ? lang('category_edit_heading') : lang('category_add_heading'));
        $this->load->view('categories/category_form', $this->data);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:category_form_name_label', 'trim|required');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            $features = array();
            if (isset($data['feature'])) {
                $features = $data['feature'];
                unset($data['feature']);
            }
            if (!isset($data['home_show']))
                $data['home_show'] = 0;

            $data['description'] = tinymce_parse_url_files($data['description']);

            if (!$data['id']) {
                $save = $this->main->insert('categories', $data);
                $data['id'] = $save;
            } else {
                $data['id'] = decode($data['id']);
                $save = $this->main->update('categories', $data, array('id' => $data['id']));
                $this->main->delete('category_feature', array('category' => $data['id']));
            }

            if (count($features) > 0) {
                foreach ($features as $feature) {
                    $this->main->insert('category_feature', array('category' => $data['id'], 'feature' => $feature));
                }
            }

            $this->main->delete('category_path', array('category' => $data['id']));
            $level = 0;
            $path_parents = $this->main->gets('category_path', array('category' => $data['parent']));
            if ($path_parents)
                foreach ($path_parents->result() as $path) {
                    $this->main->insert('category_path', array('category' => $data['id'], 'path' => $path->path, 'level' => $level));
                    $level++;
                }
            $this->main->replace('category_path', array('category' => $data['id'], 'path' => $data['id'], 'level' => $level));

            $slug = url_title($data['name'], '-', TRUE);
            if ($data['id'])
                $this->main->delete('seo_url', array('query' => 'catalog/categories/view/' . $data['id']));

            if ($this->main->get('seo_url', array('keyword' => $slug))) {
                $this->load->helper('string');
                $slug = $slug . random_string('alpha', '4');
            }
            $this->main->insert('seo_url', array('query' => 'catalog/categories/view/' . $data['id'], 'keyword' => $slug));

            if ($save !== false) {
                $return = array('message' => sprintf(lang('category_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('catalog/categories'));
            } else {
                $return = array('message' => sprintf(lang('category_save_error_message'), $data['name']), 'status' => 'error');
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('categories', array('id' => $id));

        $childs = $this->main->gets('categories', array('parent' => $id));
        if (!$childs) {
            $products = $this->main->get('products', array('category' => $id));
            if (!$products) {
                $delete = $this->main->delete('categories', array('id' => $id));
                if ($delete) {
                    $this->main->delete('seo_url', array('query' => 'catalog/categories/view/' . $data->id));
                    $return = array('message' => sprintf(lang('category_delete_success_message'), $data->name), 'status' => 'success');
                } else {
                    $return = array('message' => lang('category_delete_error_message'), 'status' => 'error');
                }
            } else {
                $return = array('message' => sprintf(lang('category_delete_exist_product_message'), $data->name), 'status' => 'error');
            }
        } else {
            $return = array('message' => sprintf(lang('category_delete_exist_child_message'), $data->name), 'status' => 'error');
        }
        echo json_encode($return);
    }

//    public function get_all_parents() {
//        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
//
//        $output = '';
//        $data = $this->categories->get_all_parents();
//        $selected = $this->input->post('selected');
//        if ($data->num_rows() > 0) {
//            $output = '<option value="0">' . lang('category_parent_root_label') . '</option>';
//            foreach ($data->result() as $d) {
//                $output .= '<option value="' . $d->id . '"' . (($selected == $d->id) ? 'selected' : '') . '>' . $d->name . '</option>';
//            }
//        } else {
//            $output = '<option value="0">' . lang('category_list_nothing_message') . '</option>';
//        }
//        echo $output;
//    }
}
