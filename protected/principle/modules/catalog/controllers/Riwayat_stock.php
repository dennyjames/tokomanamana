<?php

defined('BASEPATH') or exit('No direct script access allowed!');

require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Riwayat_stock extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->aauth->control('catalog/riwayat_stock');

        $this->lang->load('catalog/riwayat_stock', settings('language'));
        $this->load->model('riwayat_stock_model', 'riwayat_stocks');
        $this->load->helper('url');
        $this->data['menu'] = 'catalog_riwayat_stock';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();
        $this->template->form();
        $this->load->js('../assets/principle/js/modules/catalog/products.js');
        $this->load->js('../assets/principle/js/modules/catalog/riwayat_stock.js');

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('riwayat_stock'), '/catalog/riwayat_stock');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('heading'));
        $select_tahun = $this->riwayat_stocks->get_year()->result();
        $this->data['tahun'] = $select_tahun;
        $this->load->view('riwayat_stock/riwayat_stock', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));
        $filter = $this->input->post('filter');

        $output['data'] = array();
        // if($filter['tahun']){
        //     if($filter['tahun']=='99'){
        //         $tahun = date("Y");
        //     } else {
        //         $tahun = $filter['tahun'];
        //     }
        // } else {
        //     $tahun = date("Y");
        // }
        $datas = $this->riwayat_stocks->get_all($start, $length, $search, $order, $filter['from'], $filter['to'], $filter['branch']);
        if ($datas) {
            foreach ($datas->result() as $data) {
                if ($data->status == 1){
                    $data->status = 'Menunggu Konfirmasi';
                    $color = '#5286ff';
                } elseif ($data->status == 2) {
                    $data->status = 'Selesai';
                    $color = '#1ea644';
                } else {
                    $data->status = 'Batal';
                    $color = '#de2121';
                }
                if($data->quantity_revisi){
                    $data->quantity = '<span style="text-decoration:line-through;color:red">'.$data->quantity.'</span> <span>('.$data->quantity_revisi.')</span>';
                }
                $output['data'][] = array(
                    $data->code,
                    $data->name,
                    $data->cabang,
                    $data->type,
                    $data->quantity,
                    '<span style="color:'.$color.'">'.$data->status.'</span>',
                    $data->date_added
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->riwayat_stocks->count_all('');
        $output['recordsFiltered'] = $this->riwayat_stocks->count_all($search, $filter['from'], $filter['to'], $filter['branch']);
        echo json_encode($output);
    }

}
