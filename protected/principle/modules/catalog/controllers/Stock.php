<?php

defined('BASEPATH') or exit('No direct script access allowed!');

require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Stock extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->aauth->control('catalog/stock');

        $this->lang->load('catalog/stock', settings('language'));
        $this->load->model('stock_model', 'stocks');
        $this->load->helper('url');
        $this->data['menu'] = 'catalog_stock';
    }

    public function index()
    {
        $this->template->_init();
        $this->template->table();
        $this->load->js('../assets/principle/js/modules/catalog/products.js');

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('stock'), '/catalog/stock');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('heading'));
        $this->load->view('stock/stock', $this->data);
    }

    public function get_list()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->stocks->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->code,
                    $data->name,
                    ($data->variation) ? 0 : number($data->price),
                    $data->category,
                    $this->_status_stock($data->status),
                    ($data->variation) ? '<i>Produk Bervariasi</i>' : $data->quantity,
                    ($this->aauth->is_allowed('catalog/stock/edit') ? '<a href="' . site_url('catalog/stock/form/' . encode($data->id)) . '" class="btn btn-primary">Kirim Stok</a>' : '')
                    // '<ul class="icons-list">
                    // <li class="dropdown">
                    // <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    // <ul class="dropdown-menu dropdown-menu-right">' .
                    // ($this->aauth->is_allowed('catalog/stock/edit') ? '<li><a href="' . site_url('catalog/stock/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>' : '') .
                    // ($this->aauth->is_allowed('catalog/stock/delete') ? '<li><a href="' . site_url('catalog/stock/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>' : '') .
                    // '</ul>
                    // </li>
                    // </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->stocks->count_all();
        $output['recordsFiltered'] = $this->stocks->count_all($search);
        echo json_encode($output);
    }

    public function get_stock()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->stocks->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->code,
                    $data->name,
                    number($data->price),
                    $data->category,
                    $this->_status_stock($data->status),
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                        ($this->aauth->is_allowed('catalog/stock/edit') ? '<li><a href="' . site_url('catalog/stock/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>' : '') .
                        ($this->aauth->is_allowed('catalog/stock/delete') ? '<li><a href="' . site_url('catalog/stock/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>' : '') .
                        '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->stocks->count_all();
        $output['recordsFiltered'] = $this->stocks->count_all($search);
        echo json_encode($output);
    }

    public function add()
    {
        $stock = $this->main->insert('stocks', array('is_temp' => 1, 'user_added' => $this->data['user']->id));
        redirect('catalog/stocks/form/' . encode($stock));
    }

    public function form($id)
    {
        $id = decode($id);
        $this->data['brands'] = $this->main->gets('brands', array('principle_id' => $this->data['user']->principle_id), 'name ASC');
        $this->data['categories'] = $this->stocks->get_categories();
        $this->data['option_groups'] = $this->main->gets('option_group', array(), 'sort_order asc');
        $this->data['merchant_groups'] = $this->main->gets('merchant_groups', array(), 'name asc');
        $this->data['price_levels'] = array();
        $this->data['data'] = $this->stocks->get($id);
        if($this->data['data']->variation) {
            // $this->data['dta'] = $this->stocks->get_stock_option($id);
            $this->data['dta'] = $this->stocks->get_branch($this->data['user']->principle_id);
        } else {
            $this->data['dta'] = $this->stocks->get_stock($id);
        }
        $this->data['id_produk'] = $id;
        $this->data['features'] = $this->stocks->get_features_by_category($this->data['data']->category, $this->data['data']->id);
        if ($this->data['data']) {
            $this->data['data']->package_items = json_decode($this->data['data']->package_items);
            $this->data['data']->promo_data = json_decode($this->data['data']->promo_data);
            $this->data['data']->description = tinymce_get_url_files($this->data['data']->description);
        } else {
            $this->data['data']->package_items = NULL;
            $this->data['data']->promo_data = NULL;
            $this->data['data']->description = NULL;
        }
        $this->data['prices'] = $this->stocks->get_prices(($id) ? $id : 0);

        $this->template->_init();
        $this->template->form();
        $this->load->js('https://code.jquery.com/ui/1.10.4/jquery-ui.js');
        $this->load->js(site_url() . '../assets/principle/js/modules/catalog/stock_form.js');
        $this->output->set_title(lang('heading'));
        $this->load->view('stock/form', $this->data);
    }

    public function copy()
    {
        $this->data['data'] = array();
        $this->data['zonas'] = $this->main->gets('merchant_groups', array());
        $this->template->_init();
        $this->template->form();
        $this->load->js('https://code.jquery.com/ui/1.10.4/jquery-ui.js');
        $this->load->js('../assets/backend/js/modules/catalog/stock_form.js');
        $this->output->set_title(lang('heading'));
        $this->load->view('stocks/stock_copy', $this->data);
    }
    public function image()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $data = $this->input->post(null, true);
        switch ($data['act']) {
            case 'upload':
                $id = $this->main->insert('stock_image', array('stock' => $data['stock'], 'image' => $data['image'], 'primary' => $data['primary']));
                echo $id;
                break;
            case 'delete':
                $this->main->delete('stock_image', array('id' => $data['id']));
                $this->main->delete('stock_option_image', array('stock_image' => $data['id']));
                break;
            default:
                break;
        }
    }

    public function save()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $dta = $this->input->post(null, true);
        $no = 0;
        $histo = 0;
        $no_option = 0;
        $histo_option = 0;
        $product = $this->main->get('products', ['id' => decode($dta['id_produk'])]);
        if(isset($dta['id_option'])) {
            // check quantity
            foreach($dta['id_option'] as $id => $option) {
                $variation = $this->main->get('product_option', ['id' => decode($option)]);
                $total_quantity_option = $variation->quantity;

                $new_total_stock_quantity = 0;
                foreach($dta['id_branch'][$id] as $key => $val) {
                    if($dta['stk_new'][$id][$key] == "") {
                        $new_total_stock_quantity += 0;
                    } else {
                        $new_total_stock_quantity += $dta['stk_new'][$id][$key];
                    }
                }
                if($total_quantity_option < $new_total_stock_quantity) {
                    $return = [
                        'message' => 'Jumlah Stok Tidak Mencukupi!',
                        'status' => 'error'
                    ];
                    echo json_encode($return);
                    exit();
                }
            }

            foreach($dta['id_option'] as $id => $option) {
                foreach($dta['id_branch'][$id] as $key => $val) {
                    if($dta['stk_new'][$id][$key]) {
                        $histo = 1;
                        $data_option[$no_option] = [
                            'branch_id' => decode($dta['id_branch'][$id][$key]),
                            'quantity' => $dta['stk_new'][$id][$key],
                            'product_id' => decode($dta['id_produk']),
                            'principal_id' => $this->data['user']->principle_id,
                            'type' => $dta['tipe'][$id][$key],
                            'id_option' => decode($option),
                            'status' => 1
                        ];
                        $no_option++;
                    }
                }
            }

            if($histo == 1) {
                $query = $this->db->insert_batch('products_principal_history', $data_option);
            } else {
                $query = false;
            }
            if($query) {
                $return = array('message' => sprintf(lang('save_success_message'), 'Stock'), 'status' => 'success', 'redirect' => site_url('catalog/stock'));
            } else {
                $return = [
                    'status' => 'error',
                    'message' => 'Tidak ada quantity yang diisi!'
                ];
            }
        } else {
            $total_stock = $product->quantity;

            $new_total_stock = 0;
            foreach ($dta['id_branch'] as $key => $val) {
                if ($dta['stk_new'][$key] == "") {
                    $new_total_stock += 0;
                } else {
                    $new_total_stock += $dta['stk_new'][$key];
                }
            }

            if ($total_stock > $new_total_stock) {
                foreach ($dta['id_branch'] as $key => $val) {
                    // $data_[$no]= array(
                    //     'branch_id' => $val,
                    //     'quantity' => $dta['stk_total'][$key],
                    //     'product_id' => $dta['id_produk']
                    // );
                    if ($dta['stk_new'][$key]) {
                        $histo = 1;
                        $data2[$no] = array(
                            'branch_id' => decode($val),
                            'quantity' => $dta['stk_new'][$key],
                            'product_id' => decode($dta['id_produk']),
                            'principal_id' => $this->data['user']->principle_id,
                            'type' => $dta['tipe'][$key],
                            'status' => 1
                        );
                    }
                    // $this->main->delete('products_principal_stock', array('product_id' => $dta['id_produk'], 'branch_id' => $val));
                    $no++;
                }
                // $query = $this->db->insert_batch('products_principal_stock', $data_);
                if ($histo == 1) {
                    $query = $this->db->insert_batch('products_principal_history', $data2);
                }
                if ($query) {
                    $return = array('message' => sprintf(lang('save_success_message'), 'Stock'), 'status' => 'success', 'redirect' => site_url('catalog/stock'));
                } else {
                    $return = array('message' => validation_errors(), 'status' => 'error');
                }
            } else {
                $return = [
                    'status' => 'error',
                    'message' => 'Jumlah Stok Tidak Mencukupi'
                ];
            }
        }

        echo json_encode($return);
    }

    public function copy_save()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $check_zona = $this->main->gets('stock_price', array('merchant_group' => $data['from_zona']));
        $this->db->select('id');
        $this->db->where('merchant_group', $data['to_zona']);
        $check_zona_to = $this->db->get('stock_price');
        $check_price_level = $this->main->gets('stock_price_level_groups', array('merchant_group' => $data['from_zona']));
        $this->db->select('id');
        $this->db->where('merchant_group', $data['to_zona']);
        $check_price_level_to = $this->db->get('stock_price_level_groups');
        if ($check_zona->num_rows() > 0) {
            foreach ($check_zona->result_array() as $price) {
                if (isset($price['value']) || isset($price['percent'])) {
                    $data_prices[] = array(
                        'stock' => $price['stock'],
                        'merchant_group' => $data['to_zona'],
                        'value' => $price['value'],
                        'percent' => $price['percent'],
                        'percent_value' => $price['percent_value'],
                        'price' => $price['price']
                    );
                }
            }
            if ($check_zona_to->num_rows() > 0) {
                foreach ($check_zona_to->result_array() as $key => $value) {
                    $data_prices[$key]['id'] = $value['id'];
                }
                //var_dump($data_prices);exit();
                $this->db->update_batch('stock_price', $data_prices, 'id');
            } else {

                //var_dump('new');exit();
                $this->db->insert_batch('stock_price', $data_prices);
            }
        }
        if ($check_price_level->num_rows() > 0) {
            foreach ($check_price_level->result_array() as $price) {
                if (isset($price['value']) || isset($price['percent'])) {
                    $data_price_level_groups[] = array(
                        'stock_price_level' => $price['stock_price_level'],
                        'merchant_group' => $data['to_zona'],
                        'value' => $price['value'],
                        'percent' => $price['percent'],
                        'percent_value' => $price['percent_value'],
                        'price' => $price['price']
                    );
                }
            }
            if ($check_price_level_to->num_rows() > 0) {
                foreach ($check_price_level_to->result_array() as $key => $value) {
                    $data_price_level_groups[$key]['id'] = $value['id'];
                }
                $this->db->update_batch('stock_price_level_groups', $data_price_level_groups, 'id');
            } else {
                $this->db->insert_batch('stock_price_level_groups', $data_price_level_groups);
            }
        }
        $return = array('message' => 'Copy Zona Produk Berhasil', 'status' => 'success');

        echo json_encode($return);
    }

    public function view($id)
    {
        $this->load->model('catalog/categories_model', 'categories');

        $this->template->_init();
        $this->load->js('../assets/backend/js/modules/catalog/stock_view.js');

        $id = decode($id);
        $this->data['stock'] = $this->stocks->get($id);
        $this->data['stock_images'] = $this->main->gets('stock_image', array('stock' => $id));
        $this->data['stock_features'] = $this->stocks->get_stock_feature_by_category($this->data['stock']->category, $id);
        $this->data['brand'] = $this->main->get('brands', array('id' => $this->data['stock']->brand));
        $this->data['category'] = $this->categories->get($this->data['stock']->category);
        $this->data['merchant'] = $this->main->get('merchants', array('id' => $this->data['stock']->merchant));

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('stock'), '/catalog/stocks');
        $this->breadcrumbs->push($this->data['stock']->name, '/catalog/stocks/view/' . encode($id));

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title('Produk ' . $this->data['stock']->name);
        $this->load->view('stocks/stock_view', $this->data);
    }

    public function other()
    {
        $this->data['menu'] = 'catalog_stock_other';

        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('stock'), '/catalog/stocks');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('heading'));
        $this->load->view('stocks/other/list', $this->data);
    }

    public function get_list_other()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->stocks->get_all(-1, $start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    '<a href="' . site_url('catalog/stocks/view/' . encode($data->id)) . '">' . $data->code . '</a>',
                    $data->name,
                    number($data->price_old),
                    number($data->price) . '<small class="display-block text-muted">Diskon ' . number($data->discount) . '%</small>',
                    number($data->quantity),
                    $data->category,
                    $this->_status_stock($data->status),
                    $data->merchant_name,
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                        (($data->status == 2) ? '<li><a href="' . site_url('catalog/stocks/activate/' . encode($data->id)) . '" class="activate">' . lang('button_activate') . '</a></li>' : '') .
                        ($this->aauth->is_allowed('catalog/stock/edit') ? '<li><a href="' . site_url('catalog/stocks/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>' : '') .
                        ($this->aauth->is_allowed('catalog/stock/delete') ? '<li><a href="' . site_url('catalog/stocks/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>' : '') .
                        '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->stocks->count_all();
        $output['recordsFiltered'] = $this->stocks->count_all($search);
        echo json_encode($output);
    }

    public function delete($id)
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        var_dump($id);
        $data = $this->main->get('stocks', array('id' => $id));
        $delete = $this->main->delete('stocks', array('id' => $id));
        if ($delete) {
            $this->main->delete('seo_url', array('query' => 'catalog/stocks/view/' . $id));
            $this->main->delete('stock_image', array('stock' => $id));
            $this->main->delete('stock_merchant', array('stock' => $id));
            $this->db->where_in('stock_option', 'SELECT id FROM stock_option WHERE stock = ' . $id, false)->delete('stock_option_variant');
            $this->main->delete('stock_option', array('stock' => $id));
            $this->main->delete('stock_price', array('stock' => $id));
            $this->main->delete('stock_price_level', array('stock' => $id));
            $this->db->where_in('stock_review', 'SELECT id FROM stock_review WHERE stock = ' . $id, false)->delete('stock_review_likedislike');
            $this->main->delete('stock_review', array('stock' => $id));
            $return = array('message' => sprintf(lang('delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => sprintf(lang('delete_error_message'), $data->name), 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function activate($id)
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('stocks', array('id' => $id));
        $update = $this->main->update('stocks', array('status' => 1), array('id' => $id));
        if ($update) {
            $return = array('message' => 'Produk ' . $data->name . ' berhasil di aktifasi.', 'status' => 'success');
        } else {
            $return = array('message' => 'Produk ' . $data->name . ' gagal di aktifasi.', 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function disabled($id)
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('stocks', array('id' => $id));
        $update = $this->main->update('stocks', array('status' => 2), array('id' => $id));
        if ($update) {
            $return = array('message' => 'Produk ' . $data->name . ' berhasil di non-aktifkan.', 'status' => 'success');
        } else {
            $return = array('message' => 'Produk ' . $data->name . ' gagal di non-aktifkan.', 'status' => 'danger');
        }
        echo json_encode($return);
    }

    // public function get_stock_code()
    // {
    //     $this->input->is_ajax_request() or exit('No direct post submit allowed!');

    //     $merchant = $this->main->get('merchants', array('id' => $this->input->post('merchant')));
    //     echo stock_code($merchant->name, $merchant->id);
    // }

    private function _status_stock($status)
    {
        switch ($status) {
            case 0:
                $text = '<i class="icon-cross2 text-danger-400"></i>';
                break;
            case 1:
                $text = '<i class="icon-checkmark3 text-success"></i>';
                break;
            case 2:
                $text = '<i class="icon-cross2 text-danger-400"></i>';
        }
        return $text;
    }

    public function get_option($id)
    {
        $data = $this->main->get('options', array('id' => $id));
        $variants = $this->main->gets('option_variant', array('option' => $data->id))->result();
        echo json_encode(array('option' => $data, 'variants' => $variants));
    }

    private function is_decimal($val)
    {
        return is_numeric($val) && floor($val) != $val;
    }
}
