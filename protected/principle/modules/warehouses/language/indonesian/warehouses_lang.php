<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['heading'] = 'Sewa Gudang';
$lang['sub_heading'] = 'Kelola Data Sewa Gudang';
$lang['list_heading'] = 'Daftar Sewa Gudang';
$lang['add_heading'] = 'Tambah Sewa Gudang';
$lang['edit_heading'] = 'Edit Sewa Gudang';
$lang['view_heading'] = 'View Sewa Gudang';

$lang['rent_id'] = 'ID Sewa';
$lang['warehouse_name'] = 'Lokasi';
$lang['datefrom'] = 'Tanggal Mulai Sewa';
$lang['warehouse_items'] = 'Nama Barang';
$lang['warehouse_qty'] = 'Jumlah Barang';
$lang['warehouse_form_total_space_label'] = 'Total Space';
$lang['price'] = 'Harga';
$lang['size'] = 'Ukuran';
$lang['status'] = 'Status';
$lang['password'] = 'Password';
$lang['vehicle'] = 'Kendaraan';
$lang['maps'] = 'Lokasi Maps';
$lang['status_0'] = '<span class="label label-danger">Tidak aktif</span>';
$lang['status_1'] = '<span class="label label-success">Aktif</span>';
$lang['statusrent_0'] = '<span class="label label-warning">Menunggu</span>';
$lang['statusrent_1'] = '<span class="label label-success">OK</span>';
$lang['statusrent_2'] = '<span class="label label-danger">Ditolak</span>';

$lang['name_placeholder'] = 'Masukkan nama gudang';
$lang['price_placeholder'] = 'Masukkan harga gudang / m3';
$lang['size_placeholder'] = 'Masukkan ukuran gudang dalam m3';

$lang['warehouse_save_success_message'] = "Gudang '%s' berhasil disimpan.";
$lang['warehouse_save_error_message'] = "Gudang '%s' gagal disimpan.";
$lang['warehouse_delete_success_message'] = "Gudang '%s' berhasil dihapus.";
$lang['warehouse_delete_error_message'] = "Gudang '%s' gagal dihapus.";

$lang['warehouse_rent_save_success_message'] = "Penyewaan Gudang berhasil disimpan.";
$lang['warehouse_rent_save_error_message'] = "Penyewaan Gudang gagal disimpan.";
$lang['warehouse_rent_delete_success_message'] = "Penyewaan Gudang berhasil dihapus.";
$lang['warehouse_rent_delete_error_message'] = "Penyewaan Gudang gagal dihapus.";