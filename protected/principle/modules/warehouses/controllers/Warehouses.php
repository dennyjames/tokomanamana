<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Warehouses extends CI_Controller {

    public function __construct() {
        parent::__construct();
        //$this->aauth->control('warehouse');
        $this->lang->load('warehouses', settings('language'));
        $this->load->model('warehouses_model', 'warehouses');

        $this->data['menu'] = 'warehouse_list';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('warehouse'), '/');
        $this->breadcrumbs->push(lang('warehouse_rent'), '/warehouses/rent');
        
        $this->data['url_ajax'] = site_url('warehouses/get_list');
        
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->output->set_title(lang('heading'));
        $this->load->view('list', $this->data);
    }

    public function view($id = '') {
        $this->data['warehouses'] = $this->main->gets('merchants', array('type'=>'branch','status'=>1), 'name asc');
        //$this->data['invoices'] = $this->main->gets('merchants', array('type'=>'branch','status'=>1), 'name asc');
        $this->data['data'] = array();
        $config['center'] = 'auto';
            //$this->aauth->control('warehouse/view');
            $id = decode($id);
            $this->data['data'] = $this->warehouses->get($id);
            $this->data['warehouse_items'] = $this->warehouses->get_items($id)->result();
            $this->data['all_dimension'] = $this->warehouses->count_all_dimension($id)->row()->total;
            $this->data['all_request_dimension'] = $this->warehouses->count_all_request_dimension($id)->row()->total;
            $this->data['usage_dimension'] = $this->warehouses->count_usage_dimension($this->data['data']->warehouse_id)->row()->total;
            //var_dump($this->data['warehouse_items']);exit();
        $this->breadcrumbs->push(lang('warehouse'), '/warehouses');
        $this->breadcrumbs->push(lang('view_heading'), '/');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->form();
        $this->template->table();
        $this->data['url_ajax'] = site_url('warehouses/get_product_list?id='.$id);
        $this->data['url_ajax2'] = site_url('warehouses/get_product_request_list?id='.$id);
        $this->load->js('https://code.jquery.com/ui/1.10.4/jquery-ui.js');
        $this->load->js('../assets/principle/js/modules/warehouses/form.js');
        $this->load->js('../assets/backend/js/customTable.js');
        $this->output->set_title(lang('view_heading'));
        $this->load->view('view', $this->data);
    }

    public function get_detail_warehouse() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $m_id = $this->input->post('m_id');

        $datas = $this->main->get('merchants', array('id'=>$m_id), '');
        echo json_encode($datas);
    }

    public function get_product_request_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $id = $this->input->get('id');
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->warehouses->get_items_request_all($id,$start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $dimension = ($data->width/100)*($data->length/100)*($data->height/100);
                $output['data'][] = array(
                    $data->name,
                    $data->qty,
                    number_format($dimension, 2),
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->warehouses->count_all_request_product($id);
        $output['recordsFiltered'] = $this->warehouses->count_all_request_product($id,$search);
        echo json_encode($output);
    }
    
    public function form($id = '') {
        $this->load->library('googlemaps');

        $this->data['warehouses'] = $this->main->gets('merchants', array('type'=>'branch','status'=>1), 'name asc');
        $this->data['data'] = array();
        $config['center'] = 'auto';
        if ($id) {
            $this->aauth->control('warehouse/edit');
            $id = decode($id);
            $this->data['data'] = $this->warehouses->get($id);
        } else {
            $this->aauth->control('warehouse/add');
        }

        $this->breadcrumbs->push(lang('warehouse'), '/warehouses');
        $this->breadcrumbs->push(($this->data['data']) ? lang('edit_heading') : lang('add_heading'), '/');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->form();
        $this->load->js('https://code.jquery.com/ui/1.10.4/jquery-ui.js');
        $this->load->js('../assets/principle/js/modules/warehouses/form.js');
        $this->output->set_title(($this->data['data']) ? lang('edit_heading') : lang('add_heading'));
        $this->load->view('form', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        
        $datas = $this->warehouses->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                     '<a href="'.base_url('warehouses/view/').encode($data->id).'">'.$data->code.'</a>',
                    $data->name,
                    $data->date_added,
                    0,
                    lang('statusrent_' . $data->status),
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->warehouses->count_all();
        $output['recordsFiltered'] = $this->warehouses->count_all($search);
        echo json_encode($output);
    }

    public function get_product_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $id = $this->input->get('id');
        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->warehouses->get_items_all($id,$start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $dimension = ($data->width/100)*($data->length/100)*($data->height/100);
                $output['data'][] = array(
                    $data->name,
                    $data->qty,
                    number_format($dimension, 2),
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->warehouses->count_all_product($id);
        $output['recordsFiltered'] = $this->warehouses->count_all_product($id,$search);
        echo json_encode($output);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('warehouse_id', 'lang:name', 'trim|required');
        $this->form_validation->set_rules('datefrom', 'lang:datefrom', 'required');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            $data_rent = array();
            $data_invoice = array();
            $data_product = array();
            $data_product_history = array();
            do {
                
                $data['principle_id'] = $this->data['user']->principle_id;
                
                $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;
                $data_rent['code'] = rent_code();
                $data_rent['warehouse_id'] = $data['warehouse_id'];
                $data_rent['principle_id'] = $data['principle_id'];
                $data_rent['rent_date'] = date('Y-m-d',strtotime($data['datefrom']));
                $data_rent['status'] = $data['status'];
                $id = $this->main->insert('warehouse_rent', $data_rent);
                if (isset($data['warehouse_items'])) {
                    foreach($data['warehouse_items'] as $x){
                        // $data_product['product_id'] = $x['product_id'];
                        // $data_product['qty'] = $x['qty'];
                        // $data_product['rent_id'] = $data['rent_id'];
                        // $this->main->insert('warehouse_product', $data_product);
                        $data_product_history['product_id'] = $x['product_id'];
                        $data_product_history['qty'] = $x['qty'];
                        $data_product_history['rent_id'] = $id;
                        $data_product_history['type'] = 'in';
                        $data_product_history['status'] = $data['status'];
                        $this->main->insert('warehouse_product_history', $data_product_history);
                    }
                }
                // $data_invoice['code'] = invoice_code();
                // $data_invoice['rent_id'] = $id;
                // $data_invoice['subtotal'] = $data['w_price']*$data['w_size'];
                // $data_invoice['total'] = $data['w_price']*$data['w_size'];
                // $data_invoice['payment_status'] = 0;
                // $data_invoice['due_date'] = date('Y-m-d', strtotime(date('Y-m-d').' + 2 days'));
                // $invoice = $this->main->insert('warehouse_invoice', $data_invoice);

                $return = array('message' => lang('warehouse_save_success_message'), 'status' => 'success', 'redirect' => site_url('warehouses'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('merchant/delete');
        $id = decode($id);
        $data = $this->main->get('merchants', array('id' => $id));
        $this->main->delete('merchants', array('id' => $id));
        $this->main->delete('merchant_users', array('id' => $data->auth));
        $this->main->delete('merchant_user_to_group', array('user' => $data->auth));
        $this->main->delete('product_merchant', array('merchant' => $data->id));
        $products = $this->main->gets('products', array('merchant' => $data->id));
        if ($products) {
            foreach ($products->result() as $product) {
                $this->main->delete('product_price', array('product' => $product->id));
                $this->main->delete('product_price_level', array('product' => $product->id));
                $this->main->delete('product_feature', array('product' => $product->id));
                $this->main->delete('product_image', array('product' => $product->id));
                $reviews = $this->main->gets('product_review', array('product' => $product->id));
                if ($reviews) {
                    foreach ($reviews->result() as $review) {
                        $this->main->delete('product_review_likedislike', array('product_review' => $review->id));
                    }
                }
                $this->main->delete('product_review', array('product' => $product->id));
                $options = $this->main->gets('product_option', array('product' => $product->id));
                if ($options) {
                    foreach ($options->result() as $option) {
                        $this->main->delete('product_option_combination', array('product_option' => $option->id));
                        $this->main->delete('product_option_image', array('product_option' => $option->id));
                    }
                }
                $this->main->delete('product_option', array('product' => $product->id));
            }
        }
        $this->main->delete('products', array('merchant' => $data->id));

        $return = array('message' => sprintf(lang('delete_success_message'), $data->name), 'status' => 'success');

        echo json_encode($return);
    }

}
