<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('view_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('warehouses'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="tabbable">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#detail" data-toggle="tab">Detail</a></li>
                    <li><a href="#product" data-toggle="tab">Produk</a></li>
                    <li><a href="#product_request" data-toggle="tab">Request Produk</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="detail">
                        <div class="row">
                            <div class="col-sm-4">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Sewa Detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>No Sewa</td>
                                            <td><?php echo $data->code;?></td>
                                        </tr>
                                        <tr>
                                            <td>Nama Principal</td>
                                            <td><?php echo $data->principle_name;?></td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Dibuat</td>
                                            <td><?php echo get_date_time($data->date_added); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Mulai Sewa</td>
                                            <td><?php echo ($data->rent_date && $data->rent_date != '0000-00-00') ? get_date($data->rent_date) : ''; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td><?php echo lang('status_' . $data->status);?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-4">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Detail Gudang</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Nama Gudang</td>
                                            <td><?php echo $data->merchant_name;?></td>
                                        </tr>
                                        <tr>
                                            <td>Ukuran Gudang (pxlxt)</td>
                                            <td><?php echo $data->length.' m x '.$data->width.' m x '.$data->height.' m ( '.number_format($data->length*$data->width*$data->height,2).' m3 )';?></td>
                                        </tr>
                                        <tr>
                                            <td>Harga Sewa / m3 / hari</td>
                                            <td style="font-size: 15px; font-weight: bold;"><?php echo rupiah($data->price); ?></td>
                                        </tr>
                                        <tr>
                                            <td>Slot Terpakai</td>
                                            <td><?php echo ceil($usage_dimension);?> / <?php echo number_format($data->length*$data->width*$data->height,2);?> m3</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-4">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Pemakaian Bulan ini</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="font-size: 10px;text-align: right;">Total Dimensi terpakai : <?php echo number_format($all_dimension, 2);?> m3 (<?php echo ceil($all_dimension);?> m3)</td>
                                        </tr>
                                        <tr>
                                            <td style="font-size: 32px; font-weight: bold;text-align:center;"><?php echo rupiah($data->price*ceil($all_dimension)); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="product">
                        <div class="row">
                            <h6 style="text-align:right;">Total Dimensi terpakai : <?php echo number_format($all_dimension, 2);?> m3</h6>
                            <table class="table" id="table" data-url="<?php echo $url_ajax; ?>">
                                <thead>
                                    <tr>
                                        <th class="default-sort" data-sort="asc">Nama Barang</th>
                                        <th>Jumlah</th>
                                        <th>Total Dimensi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            <hr/>
                        </div>
                    </div>
                    <div class="tab-pane" id="product_request">
                        <div class="row">
                            <table class="table" id="tablex" data-url="<?php echo $url_ajax2; ?>">
                                <thead>
                                    <tr>
                                        <th class="default-sort" data-sort="asc">Nama Barang</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                            
                        <h6 style="text-align:right;font-size:12px;">Total Dimensi Semua Request Produk : <?php echo number_format($all_request_dimension, 2);?> m3 (<?php echo ceil($all_request_dimension);?> m3)</h6>
                        </div>
                        <div class="row">
                            <hr />
                            <div class="col-md-12">
                            <div class="text-right" style="margin:10px;"><button type="button" id="add-warehouse-item" class="btn btn-success">Tambah Barang</button></div>
                            <form action="<?php echo site_url('warehouses/save'); ?>" class="form-horizontal" method="post" id="form">
                                <input type="hidden" name="id" value="<?php echo ($data) ? encode($data->id) : ''; ?>">
                                <div class="form-group">
                                    <div id="item-lists">
                                    </div>
                                </div>
                                <h5 class="text-right" style="font-size:14px;">Total Dimensi Barang : <span id="sum">0.00</span> m3</h5>
                                <div class="panel-footer">
                                    <div class="heading-elements action-left">
                                        <a class="btn btn-default" href="<?php echo site_url('warehouses'); ?>"><?php echo lang('button_cancel'); ?></a>
                                        <div class="pull-right">
                                            <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>