<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sprint {

    function __construct() {
        $this->app = & get_instance();
//        $this->app->load->model('transactions_model');
//        $this->app->load->model('transactions_meta_model');
//        $this->app->load->model('products_model');
    }
    
    private function get($path, $data = array()) {
        $curl = curl_init();

        $headers = array();
        $headers[] = 'apikey: ' . $this->secret_api_key;
        $headers[] = 'Content-Type: application/json';

        $end_point = $this->server_domain . $path . '?' . http_build_query($data, '', '&');

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_USERPWD, $this->secret_api_key . ":");
        curl_setopt($curl, CURLOPT_URL, $end_point);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        curl_close($curl);

        $responseObject = json_decode($response, true);
        return $responseObject;
    }

    private function post($url, $data = array()) {
        $curl = curl_init();

        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';

        $payload = json_encode($data);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        curl_close($curl);

        $responseObject = json_decode($response, true);
        return $responseObject;
    }

    function sms($url, $data = array()) {
        $curl = curl_init();

        $end_point = $url . '?' . http_build_query($data, '', '&');

        curl_setopt($curl, CURLOPT_URL, $end_point);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }
}
