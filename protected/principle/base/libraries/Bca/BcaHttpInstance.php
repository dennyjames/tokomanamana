<?php

namespace Bca;

class BcaHttpInstance
{
    private static $instance      = null;
    private static $corp_id       = 'h2hauto008';
    private static $account_number = '0613005908'; // Fill With Your Account Number. 0201245680 is Sandbox Account
    private static $client_id     = 'b095ac9d-2d21-42a3-a70c-4781f4570704';
    private static $client_secret = 'bedd1f8d-3bd6-4d4a-8cb4-e61db41691c9';
    private static $api_key       = 'dcc99ba6-3b2f-479b-9f85-86a09ccaaacf';
    private static $secret_key    = '5e636b16-df7f-4a53-afbe-497e6fe07edc';
    // private static $corp_id       = 'BCAAPI2016';
    // private static $account_number = '0201245680'; // Fill With Your Account Number. 0201245680 is Sandbox Account
    // private static $client_id     = 'd3179a90-564d-4474-9ed3-3e80675cda16';
    // private static $client_secret = 'ee031139-4721-4e09-9b44-43a103e02d8d';
    // private static $api_key       = '5f2ac156-6b57-41c6-b487-05b5d865bfa2';
    // private static $secret_key    = '0ad2eca3-8c98-40e1-803a-1f05b7e6eb65';
    // private static $corp_id       = 'IBSTOKOMAN';
    // private static $account_number = '3351418883'; // Fill With Your Account Number. 0201245680 is Sandbox Account
    // private static $client_id     = '7d34bc86-1b9a-4836-af71-360d95fc8449';
    // private static $client_secret = '77d3e0f3-e3d7-418a-8a08-2cf57f065204';
    // private static $api_key       = '67546ea7-0ecc-43b7-8373-3c89ac163597';
    // private static $secret_key    = 'de49cae7-703b-4a19-8008-783f2f742f6e';
    private static $options = array(
        'scheme'        => 'https',
        'port'          => 443,
        'timezone'      => 'Asia/Jakarta',
        'timeout'       => null,
        'development'   => true,
    );
    
    private function __construct()
    {
 
    }

    private function __clone()
    {
    }
    public static function getBcaHttp()
    {
        if (self::$instance !== null) {
            return self::$instance;
        }

        self::$instance = new BcaHttp(
            self::$corp_id,
            self::$client_id,
            self::$client_secret,
            self::$api_key,
            self::$secret_key,
            self::$options
        );
        return self::$instance;
    }

    public static function login() {
        $bca = new BcaHttp(self::$corp_id,  self::$client_id,  self::$client_secret,  self::$api_key,  self::$secret_key);
        //$bca = self::$instance;
        // Request Login dan dapatkan nilai OAUTH
        $response = $bca->httpAuth();

        // LIHAT HASIL OUTPUT
        return json_encode($response);
    }

    public static function get_balance($token) {
        $bca = new BcaHttp(self::$corp_id,  self::$client_id,  self::$client_secret,  self::$api_key,  self::$secret_key);
        
        $arrayAccNumber = array('3351418883');
        //$arrayAccNumber = array('0613005908');

        $response = $bca->getBalanceInfo($token, $arrayAccNumber);
        
        // LIHAT HASIL OUTPUT
        echo json_encode($response);
    }

    public static function transfer($token,$trans_id,$amount,$to_account) {
        $bca = new BcaHttp(self::$corp_id,  self::$client_id,  self::$client_secret,  self::$api_key,  self::$secret_key);
        $response = $bca->fundTransfers($token, 
                            $amount,
                            self::$account_number,
                            $to_account,
                            'PO'.date('Y').'-'.$trans_id,
                            'Tokomanamana',
                            'Withdraw Merchant',
                            $trans_id);
        // LIHAT HASIL OUTPUT
        return json_encode($response);
    }

    public static function transferDomestic($token,$trans_id,$amount,$to_account) {
        $bca = new BcaHttp(self::$corp_id,  self::$client_id,  self::$client_secret,  self::$api_key,  self::$secret_key);
        $response = $bca->fundTransfersDomestic($token, 
                            $amount,
                            self::$account_number,
                            $to_account,
                            'PO'.date('Y').'-'.$trans_id,
                            'Tokomanamana',
                            'Withdraw Merchant',
                            $trans_id,
                            'BRINIDJA',
                            'Tester',
                            'LLG',
                            '1',
                            '1'
                        );
        // LIHAT HASIL OUTPUT
        return json_encode($response);
    }

    public static function get_statement($token,$startDate,$endDate) {
        $bca = new BcaHttp(self::$corp_id,  self::$client_id,  self::$client_secret,  self::$api_key,  self::$secret_key);

        $response = $bca->getAccountStatement($token,self::$account_number,$startDate,$endDate);
        
        // LIHAT HASIL OUTPUT
        return json_encode($response);
    }
}
