<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Crons extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

   public function create_history_usage() {
        // cek jika tanggal mulai sama dengan hari ini
        $history = $this->db->where('rent_date <= CURDATE()')->where('status', '1')->get('warehouse_rent');
        //var_dump($history->result());exit();
        if ($history->num_rows() > 0) {
            // create daily history usage
            foreach ($history->result() as $hst) {
                $dimension = 0;
                // get data branch
                $branch = $this->main->get('merchants', array('id' => $hst->warehouse_id));
                $ppm = $branch->warehouse_price;
                //get data product inventory
                $this->db->select('wp.*,p.length,p.width,p.height');
                $this->db->join('products p','p.id = wp.product_id','left');
                $products = $this->main->gets('warehouse_product wp', array('wp.rent_id' => $hst->id));
                if($products->num_rows() > 0){
                    foreach($products->result() as $product){
                        $count = ($product->length / 100) * ($product->width / 100) * ($product->height / 100);
                        $count = number_format($count, 2);
                        $dimension = number_format($dimension + $count,2);
                    }
                }
                // check data invoice history today
                $inv_history = $this->db->where('history_date = CURDATE()')->where('rent_id', $hst->id)->get('warehouse_invoice_history');
                if($inv_history->num_rows() == 0){
                    // Jika blum ada invoice history
                    $this->main->insert('warehouse_invoice_history', array('rent_id' => $hst->id, 'history_date' => date('Y-m-d'), 'dimension' => $dimension,'dimension_round' => ceil($dimension),'ppm'=>$ppm,'total'=>$ppm*ceil($dimension)));
                } else {
                    // Jika sudah ada invoice history
                    $this->main->update('warehouse_invoice_history', array('dimension' => $dimension,'dimension_round' => ceil($dimension),'ppm'=>$ppm,'total'=>$ppm*ceil($dimension)), array('rent_id' => $hst->id));
                }
                //var_dump(($inv_history->result()));exit();
            }
        }

   }
}
