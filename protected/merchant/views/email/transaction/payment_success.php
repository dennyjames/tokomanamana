<div bgcolor="#FFFFFF" style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;width:100%!important;height:100%;font-size:14px;color:#404040;margin:0;padding:0">
    <table style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0" bgcolor="transparent">
        <tbody>
            <tr style="margin:0;padding:0">
                <td style="margin:0;padding:0"></td>
                <td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0" bgcolor="#FFFFFF">
                    <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;border:1px solid #e7e7e7">
                        <table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:20px;" bgcolor="transparent">
                            <tbody>
                                <tr style="margin:0;padding:0">
                                    <td>
                                        <img src="<?php echo site_url('assets/frontend/images/logo.png'); ?>" alt="" style="margin:10px 0">
                                    </td>
                                </tr>

                                <tr style="margin:0;padding:0">
                                    <td style="margin:0;padding:0">
                                        <h5 style="line-height:32px;color:#666;font-weight:700;font-size:24px;margin:0 0 20px;padding:0">Pembayaran #<?php echo $order->code; ?> Sukses</h5>
                                        <p style="font-weight:normal;color:#999;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0">
                                            Selamat, pembayaran Anda untuk tagihan #<?php echo $order->code; ?> telah <b>berhasil</b>. Terimakasih atas kepercayaanmu berbelanja di <?php echo settings('store_name'); ?>.
                                        </p>
                                        <p style="font-weight:normal;color:#999;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0">
                                            Pesananmu akan diteruskan ke penjual, klik <a href="<?php echo site_url('member/order_detail/'. encode($order->id)); ?>">disini</a> untuk memantau pesananmu.
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table style="width:100%;margin-bottom:24px;padding:0 20px;">
                            <thead>
                                <tr><td style="width:50%">
                                        <span style="font-size:12px;color:#666;font-weight:600">Total Pembayaran</span>
                                    </td>
                                    <td style="width:50%">
                                        <span style="font-size:12px;color:#666;font-weight:600">Status Pembayaran</span>
                                    </td>
                                </tr></thead>
                            <tbody>
                                <tr>
                                    <td style="vertical-align:top;">

                                        <p style="font-size:16px;color:#999;margin:0 0 5px 0"><?php echo rupiah($order->total); ?></p>

                                    </td>
                                    <td style="vertical-align:top;">
                                        <p style="font-size:16px;color:#999;margin:0 0 5px 0">Lunas</p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table style="width:100%;margin-bottom:24px;padding:0 20px; ">
                            <thead>
                                <tr><td style="width:50% ">
                                        <span style="font-size:12px;color:#666;font-weight:600 ">Metode Pembayaran</span>
                                    </td>
                                    <td style="width:50% ">
                                        <span style="font-size:12px;color:#666;font-weight:600 ">Referensi Pembayaran</span>
                                    </td>
                                </tr></thead>
                            <tbody>
                                <tr>
                                    <td style="vertical-align:top">
                                        <p style="font-size:16px;color:#999;margin:5px 0">Bank Transfer</p>
                                    </td>
                                    <td style="vertical-align:top">
                                        <p style="font-size:16px;color:#999;"><?php echo $order->code; ?></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table style="width:100%;margin-bottom:24px;padding:0 20px;">
                            <tbody>
                                <tr>
                                    <td>
                                        <div style="border: 1px solid #e7e7e7;border-radius: 5px;padding: 0px 20px;">
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <h2 style="font-size:14px;font-weight:600;color:#666;margin:50px 0 16px 0;padding:0 20px;">Detail Pemesanan</h2>
                        <span style="width:30px;border:2px solid #FF5722;display:inline-block;margin-left:20px;"></span>
                        <table style="width:100%;margin-top:16px;margin-bottom:18px;border-bottom:1px solid #E0E0E0;padding: 0 20px;">
                            <tbody>
                                <?php foreach ($invoices->result() as $invoice) { ?>
                                    <tr>
                                        <td style="padding-top: 16px;" colspan="2">
                                            <p style="color:#97C23C;text-decoration:none;"><?php echo $invoice->code; ?></p>
                                        </td>
                                    </tr>
                                    <?php foreach ($this->main->gets('order_product', array('invoice' => $invoice->id))->result() as $product) { ?>
                                        <tr>
                                            <td style="vertical-align:top">
                                                <p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;"><?php echo $product->name; ?></p>
                                                <p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:8px;"><?php echo $product->quantity; ?> X <?php echo rupiah($product->price); ?></p>
                                            </td>
                                            <td style="vertical-align:top;text-align:right;width:120px;">
                                                <span style="float:left;font-size:14px;color:#999;margin-top:5px;">Rp</span>
                                                <p style="float:right;font-size:14px;color:#999;margin-top:5px;"><?php echo number($product->total); ?></p>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td style="vertical-align:top">
                                            <p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;">
                                                <?php
                                                if ($invoice->shipping_courier != 'pickup') {
                                                    $invoice->shipping_courier = explode('-', $invoice->shipping_courier);
                                                    $invoice->shipping_courier = strtoupper($invoice->shipping_courier[0]) . ' ' . $invoice->shipping_courier[1];
                                                } else {
                                                    $invoice->shipping_courier = 'Ambil Sendiri';
                                                }
                                                echo $invoice->shipping_courier;
                                                ?>
                                            </p>
                                        </td>
                                        <td style="vertical-align:top;text-align:right;width:120px;">
                                            <span style="float:left;font-size:14px;color:#999;margin-top:5px;">Rp</span>
                                            <p style="float:right;font-size:14px;color:#999;margin-top:5px;"><?php echo number($invoice->shipping_cost); ?></p>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <table style="width:100%;margin-bottom:20px;padding:0 20px;">
                            <tbody>
                                <tr style="margin-bottom:16px;">
                                    <td style="vertical-align:top">
                                        <p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;font-weight:bold;">Total Pembayaran</p>
                                    </td>
                                    <td style="vertical-align:top;text-align:right;width:120px;">
                                        <span style="float:left;font-size:14px;color:#999;margin-top:5px;font-weight:bold;">Rp</span>
                                        <p style="float:right;font-size:14px;color:#999;margin-top:5px;font-weight:bold;"><?php echo number($order->total); ?></p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table style="width:100%;margin-bottom:24px;padding:0 20px;">
                            <tbody>
                                <tr>
                                    <td>
                                        <div style="border: 1px solid #e7e7e7;border-radius: 5px;padding: 0px 20px;">
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <h2 style="font-size:14px;font-weight:600;color:#666;margin:50px 0 16px 0;padding:0 20px;">Alamat Pengiriman</h2>
                        <span style="width:30px;border:2px solid #FF5722;display:inline-block;margin-left:20px;"></span>
                        <table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:20px;" bgcolor="transparent">
                            <tbody>
                                <tr style="margin:0;padding:0">
                                    <td style="margin:0;padding:0">
                                        <p style="font-weight:normal;color:#999;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0">
                                            <b><?php echo $order->shipping_name; ?></b><br>
                                            <?php echo nl2br($order->shipping_address); ?><br>
                                            <?php echo $this->main->get('districts',array('id'=>$order->shipping_district))->name; ?>
                                            <?php echo $this->main->get('cities',array('id'=>$order->shipping_city))->name; ?><br>
                                            <?php echo $this->main->get('provincies',array('id'=>$order->shipping_province))->name; ?> - 
                                            <?php echo $order->shipping_postcode; ?><br>
                                            No. Telp: <?php echo $order->shipping_phone; ?>
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="padding:0 20px">
                            <p style="font-size:14px;color:#999;padding:24px 0 10px;margin:0;border-top: 1px solid #E0E0E0;">
                                Email dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.
                            </p>
                        </div>
                    </div>
                </td>
                <td style="margin:0;padding:0"></td>
            </tr>
        </tbody>
    </table>
</div>