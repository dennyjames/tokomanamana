<div bgcolor="#FFFFFF" style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;width:100%!important;height:100%;font-size:14px;color:#404040;margin:0;padding:0">
    <table style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0" bgcolor="transparent">
        <tbody>
            <tr style="margin:0;padding:0">
                <td style="margin:0;padding:0"></td>
                <td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0" bgcolor="#FFFFFF">
                    <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;border:1px solid #e7e7e7">
                        <table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:20px;" bgcolor="transparent">
                            <tbody>
                                <tr style="margin:0;padding:0">
                                    <td>
                                        <img src="<?php echo site_url('../assets/frontend/images/logo.png'); ?>" alt="" style="margin:10px 0;height: 35px">
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="<?php echo site_url('../assets/frontend/images/diproses.png'); ?>" alt="" title="" style="margin-bottom: 16px;
                                            max-height: 128px;
                                            margin-left: auto;
                                            margin-right: auto;
                                            display: block;">
                                    </td>
                                </tr>
                                <tr style="margin:0;padding:0">
                                    <td style="margin:0;padding:0">
                                        <h5 style="line-height:32px;font-weight:700;font-size:20px;margin:0 0 20px;padding:0">Hai <?php echo $order->customer_name; ?></h5>
                                        <p style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0">
                                            Pesanan <b><?php echo character_limiter($products->row()->name, 30, '...'); ?></b> kamu sedang diproses oleh penjual.
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table style="width:100%;margin-bottom:24px;padding:0 20px;">
                            <tbody>
                                <tr>
                                    <td style="width:25%;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 5px 0;">Nomor Faktur:</td>
                                    <td style="margin:0;padding:0;font-size:14px;"><?php echo $order->code; ?></td>
                                </tr>
                                <tr><td colspan="2" style="border-bottom: 1px solid #eee;"></td></tr>
                                <tr>
                                    <td style="width:25%;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 5px 0;">Penjual:</td>
                                    <td style="margin:0;padding:0;font-size:14px;"><?php echo $merchant->name; ?></td>
                                </tr>
                                <tr><td colspan="2" style="border-bottom: 1px solid #eee;"></td></tr>
                                <tr>
                                    <td style="width:25%;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 5px 0;">Jasa Pengiriman:</td>
                                    <td style="margin:0;padding:0;font-size:14px;">
                                        <?php
                                        $shipping = explode('-', $order->shipping_courier);
                                        echo strtoupper($shipping[0]) . (isset($shipping[1]) ? ' ' . $shipping[1] : '');
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <h2 style="font-size:14px;font-weight:600;margin:50px 0 16px 0;padding:0 20px;">Rincian Pesanan</h2>
                        <span style="width:30px;border:2px solid #FF5722;display:inline-block;margin-left:20px;"></span>
                        <div style="padding-top:20px;padding-bottom:20px;padding-right:20px;padding-left:20px;border-width:1px;background-color:#e6e6e6;margin-bottom:20px">
                            <div style="margin:0;padding:0">
                                <ol style="font-weight:normal;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0 0 0 21px">
                                    <?php foreach ($products->result() as $product) { ?>
                                         <?php if($product->options){ 
                                                    $products_option = json_decode($product->options);
                                                    if(count($products_option) > 1 || !isset($products_option[0]->type)){
                                                        foreach($products_option as $product_option) {  ?>
                                                            <li style="font-size:13px;margin:0 0 15px;padding:0">
                                                                <b style="margin:0;padding:0"><?php echo $product->name; ?></b>
                                                                <br style="margin:0;padding:0">
                                                                 <?php $options = json_decode($product_option->data_option);
                                                                        $count = false;
                                                                        foreach($options as $option) { ?>
                                                                            <?php if($count == true) echo ', '; ?>
                                                                            <?php echo $option->type . ' ' . $option->value; ?>
                                                                            <?php $count = true; ?>
                                                                        <?php } ?>
                                                                <br style="margin:0;padding:0">
                                                                Jumlah: <?php echo $product_option->quantity; ?> Buah (@ <?php echo rupiah($product_option->price); ?>) 
                                                            </li>
                                                        <?php } ?>
                                                    <?php } ?>
                                        <?php }else{ ?>
                                            <li style="font-size:13px;margin:0 0 15px;padding:0">
                                                <b style="margin:0;padding:0"><?php echo $product->name; ?></b>
                                                <br style="margin:0;padding:0">
                                                Jumlah: <?php echo $product->quantity; ?> Buah (@ <?php echo rupiah($product->price); ?>) 
                                            </li>
                                        
                                        <?php } ?>
                                    <?php } ?>
                                </ol>

                                <div style="font-size:13px;line-height:18px;margin:0 0 15px;padding:0">
                                    <div style="margin:0 0 5px;padding:0"><b style="margin:0;padding:0">Tujuan Pengiriman:</b></div>
                                    <?php echo strtoupper($order->shipping_name); ?> <br style="margin:0;padding:0">
                                    <?php echo $order->shipping_address; ?> <br style="margin:0;padding:0">
                                    <?php echo $order->shipping_district.', '.$order->shipping_city.', '.$order->shipping_postcode; ?> <br style="margin:0;padding:0">
                                    <?php echo $order->shipping_province; ?> <br style="margin:0;padding:0">
                                    Telp: <?php echo $order->shipping_phone; ?>
                                </div>

                                <div style="margin:20px 0 0;padding:0">
                                    <table style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0" bgcolor="transparent">
                                        <tbody style="margin:0;padding:0">
                                            <tr style="margin:0;padding:0">
                                                <td style="width:50%;font-weight:700;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0" valign="top">Total Harga Produk:</td>
                                                <td style="text-align:right;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0" align="right" valign="top"><?php echo rupiah($order->subtotal); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <div style="border-bottom-width:1px;border-bottom-color:#eee;border-bottom-style:solid;margin:0;padding:0"></div>
                                    <table style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0" bgcolor="transparent">
                                        <tbody style="margin:0;padding:0">
                                            <tr style="margin:0;padding:0">
                                                <td style="width:50%;font-weight:700;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0" valign="top">Ongkos kirim:</td>
                                                <td style="text-align:right;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0" align="right" valign="top"><?php echo rupiah($order->shipping_cost); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                      <?php if($order->payment_method == 'transfer'){ ?>
                                    <div style="border-bottom-width:1px;border-bottom-color:#eee;border-bottom-style:solid;margin:0;padding:0"></div>
                                    <table style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0" bgcolor="transparent">
                                        <tbody style="margin:0;padding:0">
                                            <tr style="margin:0;padding:0">
                                                <td style="width:50%;font-weight:700;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0" valign="top">Kode Unik:</td>
                                                <td style="text-align:right;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0" align="right" valign="top"><?php echo rupiah($order->unique_code); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <?php } ?>
                                    <div style="border-bottom-width:1px;border-bottom-color:#eee;border-bottom-style:solid;margin:0;padding:0"></div>
                                    <table style="width:100%;max-width:100%;border-collapse:collapse;border-spacing:0;background-color:transparent;margin:5px 0;padding:0" bgcolor="transparent">
                                        <tbody style="margin:0;padding:0">
                                            <tr style="margin:0;padding:0">
                                                <td style="width:50%;font-weight:700;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0" valign="top">Total Pembayaran:</td>
                                                <td style="text-align:right;font-size:13px;vertical-align:top;line-height:18px;margin:0;padding:0 10px 0 0" align="right" valign="top"><?php echo ($order->payment_method == 'transfer') ? rupiah($order->total_plus_kode) : rupiah($invoice->total); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div style="padding:0 20px">
                            <p style="font-size:14px;color:#999;padding:24px 0 10px;margin:0;border-top: 1px solid #E0E0E0;">
                                Email dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.
                            </p>
                        </div>
                    </div>
                </td>
                <td style="margin:0;padding:0"></td>
            </tr>
        </tbody>
    </table>
</div>