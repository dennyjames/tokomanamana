<!DOCTYPE html>
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js"> <!--<![endif]-->
    <head>
        <title><?php echo "{$title}"; ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
        <link rel="canonical" href="<?php echo current_url(); ?>" />
        <link rel="shortcut icon" href="<?php echo site_url('../assets/frontend/images/tokomanamana.ico'); ?>">


        <?php
        foreach ($css as $file) {
            echo "\n    ";
            echo '<link href="' . $file . '?timestamp='.time().'" rel="stylesheet" type="text/css" />';
        } echo "\n";
        ?>
        <script type="text/javascript">
            var site_url = '<?php echo site_url(); ?>';
            var current_url = '<?php echo current_url(); ?>';
            var decimal_digit = '<?php echo settings('number_of_decimal'); ?>';
            var decimal_separator = '<?php echo settings('number_separator_decimal'); ?>';
            var thousand_separator = '<?php echo settings('number_separator_thousand'); ?>';
            var location_session = <?php echo ($this->session->has_userdata('location')) ? 'true' : 'false'; ?>;
        </script>
        <?php
        foreach ($js as $file) {
            echo "\n\t\t";
            ?><script type="text/javascript" src="<?php echo $file; ?>?timestamp=<?php echo time(); ?>"></script><?php
        } echo "\n\t";
        ?>
        <script src="<?php echo site_url('../'); ?>assets/backend/js/merchant/modules/shop/app.js?timestamp=<?php echo time(); ?>"></script>
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5b08a5a6b677991d"></script>
        
    </head>
    <body class="index-template sarahmarket_1">
        <header id="top" class="header clearfix">
            <div id="shopify-section-theme-header" class="shopify-section">
                <div data-section-id="theme-header" data-section-type="header-section">
                    <section class="top-header ma-shop">
                        <div class="top-header-wrapper">
                            <div class="container">
                                <div class="row">
                                    <div class="top-header-inner">
                                        <ul class="unstyled top-menu">
                                        <?php if ($this->ion_auth->logged_in()) { ?>
                                                <li class="toolbar-customer login-account">
                                                    <span id="loginButton" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="sub-dropdown1 visible-sm visible-md visible-lg"></i>
                                                        <i class="sub-dropdown visible-sm visible-md visible-lg"></i>
                                                        <a href="#"><?php echo $user->fullname; ?> <?php echo (isset($total_notif)) ? '<span class="badge badge-notification" style="margin-left: 5px;">' . $total_notif . '</span>' : ''; ?></a>
                                                    </span>
                                                    <div class="dropdown-menu text-left" style="display: none;">
                                                        <ul class="control-container customer-accounts list-unstyled">
                                                            <li class="clearfix">
                                                                <a href="<?php echo site_url('shop/member/history'); ?>"><?php echo lang('account_order_history'); ?> <?php echo ($total_active_order) ? '<span class="badge badge-notification">' . $total_active_order . '</span>' : ''; ?></a>
                                                            </li>
                                                            <li class="clearfix">
                                                                <a href="<?php echo site_url('auth/logout'); ?>"><?php echo lang('logout'); ?></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            <?php } else { ?>
                                                <li class="toolbar-customer"><a href="<?php echo site_url('auth/login'); ?>">Masuk</a> <span>/</span> <a href="<?php echo site_url('auth/register'); ?>">Daftar</a></li>
                                                <!-- <li class="toolbar-customer"><a href="<?php echo site_url('shop/member/login'); ?>">Masuk</a> <span>/</span> <a href="<?php echo site_url('shop/member/register'); ?>">Daftar</a></li> -->
                                            <?php } ?>
                                            <li>
                                                <div class="nav-cart " id="cart-target">
                                                    <div class="cart-info-group">
                                                        <a href="<?php echo site_url('shop/cart'); ?>" class="cart dropdown-toggle dropdown-link" data-toggle="dropdown">
                                                            <i class="sub-dropdown1 visible-sm visible-md visible-lg"></i>
                                                            <i class="sub-dropdown visible-sm visible-md visible-lg"></i>
                                                            <div class="num-items-in-cart">
                                                                <div class="items-cart-left">
                                                                    <img class="cart_img" src="<?php echo site_url('../assets/frontend/images/bg-cart-2.png'); ?>">
                                                                </div>
                                                                <div class="items-cart-right">
                                                                    <span class="badge badge-cart"><?php echo $this->cart->total_items(); ?></span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div class="dropdown-menu cart-info" style="display: none;">
                                                            <div class="cart-content">
                                                                <?php if ($this->cart->contents()) { ?>
                                                                    <div class="items control-container">
                                                                        <?php
                                                                        foreach ($this->cart->contents() as $item) {
                                                                            $id = explode('-', $item['id'])
                                                                            ?>
                                                                            <div class="row">
                                                                                <div class="cart-left">
                                                                                    <a class="cart-image" href="<?php echo seo_url('catalog/products/view/' . $id[0]); ?>">
                                                                                        <img src="<?php echo get_image($item['image']); ?>" alt="" title="">
                                                                                    </a>
                                                                                </div>
                                                                                <div class="cart-right">
                                                                                    <div class="cart-title"><a href="<?php echo seo_url('catalog/products/view/' . $id[0]); ?>"><?php echo $item['name']; ?></a></div>
                                                                                    <div class="cart-price"><span class="money"><?php echo rupiah($item['price']); ?></span><span class="x"> x <?php echo number($item['qty']); ?></span></div>
                                                                                </div>
                                                                            </div>
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="subtotal"><span>Subtotal:</span><span class="cart-total-right money"><?php echo rupiah($this->cart->total()); ?></span></div>
                                                                    <div class="action"><button class="btn" style="font-size:10px" onclick="window.location = '<?php echo site_url('shop/cart'); ?>'">Lihat Keranjang<i class="fa fa-caret-right"></i></button><button class="btn float-right" style="font-size:10px" onclick="window.location = '<?php echo site_url('shop/cart'); ?>'">Beli Sekarang<i class="fa fa-caret-right"></i></button></div>
                                                                <?php } else { ?>
                                                                    <div class="items control-container">
                                                                        <p style="padding: 15px;">Keranjang masih kosong.</p>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="main-header">
                        <div class="main-header-wrapper">
                            <div class="container clearfix">
                                <div class="row">
                                    <div class="main-header-inner">
                                        <div class="nav-top">
                                            <div class="nav-logo">
                                                <a href="<?php echo site_url('shop'); ?>"><img src="<?php echo site_url('../assets/frontend/images/logo-merchant.png'); ?>" alt="Logo <?php echo settings('store_name'); ?>" title="<?php echo settings('store_name'); ?>"></a>
                                                <h1 style="display:none"><a href="<?php echo site_url(); ?>"><?php echo settings('store_name'); ?></a></h1>
                                            </div>
                                            <div class="group-search-cart">
                                                <div class="nav-search ma-shop">
                                                    <form class="search" action="<?php echo site_url('shop/search'); ?>">
                                                        <input type="text" name="word" id="search_box" class="search_box" placeholder="Cari produk ...">
                                                        <button class="search_submit" type="submit">
                                                            Cari
                                                        </button>
                                                        <div id="search_suggest"></div>
                                                    </form>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="mobile-navigation">
                                            <?php if ($this->uri->segment(2) != '') {?>
                                            <a class="kembali" href="javascript:void(0);" onclick="window.history.back()"><i class="fa fa-arrow-left"></i></a>
                                            <?php } else { ?>
                                            <button id="showLeftPush" class="visible-xs"><i class="fa fa-bars fa-2x"></i></button>
                                            <?php } ?>
                                            <div class="nav-logo visible-xs">
                                                <div><a href="<?php echo base_url('shop');?>"><img src="<?php echo site_url('../assets/frontend/images/logo.png'); ?>" alt="<?php echo settings('store_name'); ?>" title="<?php echo settings('store_name'); ?>"></a></div>
                                            </div>
                                            <a id="cart-button" href="<?php echo site_url('shop/cart'); ?>" class="visible-xs">
                                                <i class="fa fa-icon fa-shopping-cart"></i>
                                                <span class="badge badge-cart"><?php echo $this->cart->total_items(); ?></span>
                                            </a>
                                            <div class="nav-search visible-xs">
                                                <form class="search" action="<?php echo site_url('shop/search'); ?>">
                                                    <input type="text" name="word" class="search_box" placeholder="Cari produk ...">
                                                    <button class="search_submit" type="submit">
                                                        <svg aria-hidden="true" role="presentation" class="icon icon-search" viewBox="0 0 37 40"><path d="M35.6 36l-9.8-9.8c4.1-5.4 3.6-13.2-1.3-18.1-5.4-5.4-14.2-5.4-19.7 0-5.4 5.4-5.4 14.2 0 19.7 2.6 2.6 6.1 4.1 9.8 4.1 3 0 5.9-1 8.3-2.8l9.8 9.8c.4.4.9.6 1.4.6s1-.2 1.4-.6c.9-.9.9-2.1.1-2.9zm-20.9-8.2c-2.6 0-5.1-1-7-2.9-3.9-3.9-3.9-10.1 0-14C9.6 9 12.2 8 14.7 8s5.1 1 7 2.9c3.9 3.9 3.9 10.1 0 14-1.9 1.9-4.4 2.9-7 2.9z"></path></svg>
                                                    </button>
                                                </form>
                                            </div>
                                            <div class="mobile-navigation-inner">
                                                <div class="mobile-navigation-content">
                                                    <div class="mobile-top-navigation visible-xs">
                                                        <div class="mobile-content-top">
                                                            <div class="mobile-top-account">
                                                                <div class="is-mobile-login">
                                                                    <ul class="customer">
                                                                        <?php if ($this->ion_auth->logged_in()) { ?>
                                                                            <li>
                                                                                <a href="<?php echo site_url('shop/member/profile'); ?>"><i class="fa fa-user" aria-hidden="true"></i>
                                                                                    <span><?php echo $user->fullname; ?></span>
                                                                                </a>
                                                                            </li>
                                                                            <li >
                                                                                <a href="<?php echo site_url('shop/member/history'); ?>"><?php echo lang('account_order_history'); ?> <?php echo ($total_active_order) ? '<span class="badge badge-notification">' . $total_active_order . '</span>' : ''; ?></a>
                                                                            </li>
                                                                            <li>
                                                                                <a href="<?php echo site_url('auth/logout'); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i>
                                                                                    <span>Keluar</span>
                                                                                </a>
                                                                            </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="nav-menu visible-xs leftnavi" id="is-mobile-nav-menu">
                                                        <div class="is-mobile-menu-content">
                                                            <div class="mobile-content-link">
                                                                <ul class="nav navbar-nav hoverMenuWrapper">
                                                                    <!-- <li class="nav-item">
                                                                        <a href="<?php echo site_url('shop'); ?>">Beranda</a>
                                                                    </li> -->
                                                                    <li class="nav-item  navigation_mobile">
                                                                        <a href="javascript:void(0)" class="menu-mobile-link">Kategori</a>
                                                                        <a href="javascript:void(0)" class="arrow">
                                                                            <i class="fa fa-angle-down"></i>
                                                                        </a>
                                                                        <div class="menu-mobile-container" style="display: none;">
                                                                            <ul class="sub-mega-menu">
                                                                                <?php if ($categories) foreach ($categories->result() as $category) { ?>
                                                                                        <li class=" li-sub-mega">
                                                                                            <a href="<?php echo base_url('shop/catalog/categories/view/' . $category->id); ?>"><?php echo $category->name; ?></a>
                                                                                        </li>
                                                                                    <?php } ?>
                                                                            </ul>
                                                                        </div>
                                                                    </li>
                                                                    <?php if ($this->ion_auth->logged_in()) { ?>
                                                                        <li class="nav-item  navigation_mobile">
                                                                            <a href="javascript:void(0)" class="menu-mobile-link">Akun Saya</a>
                                                                            <a href="javascript:void(0)" class="arrow">
                                                                                <i class="fa fa-angle-down"></i>
                                                                            </a>
                                                                            <div class="menu-mobile-container" style="display: none;">
                                                                                <ul class="sub-mega-menu">
                                                                                    <li class="li-sub-mega"><a href="<?php echo site_url('shop/member/history'); ?>"><?php echo lang('account_order_history'); ?></a></li>
                                                                                </ul>
                                                                            </div>
                                                                        </li>
                                                                    <?php } ?>
                                                                    <!-- <li class="nav-item">
                                                                        <a href="<?php echo site_url('blogs'); ?>">Artikel</a>
                                                                    </li> -->
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="navigation-header">
                        <div class="navigation-header-wrapper">
                            <div class="container clearfix">
                                <div class="row">
                                    <div class="main-navigation-inner">
                                        <div class="navigation_area">
                                            <div class="navigation_left">
                                                <div class="group_navbtn">
                                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                                        <span class="dropdown-toggle">
                                                            Kategori
                                                        </span>
                                                        <i class="fa fa-bars" aria-hidden="true"></i>
                                                    </a>
                                                    <ul class="navigation_links_left dropdown-menu" style="display: none;">
                                                        <?php if ($categories) foreach ($categories->result() as $category) { ?>
                                                                <li class="nav-item _icon">
                                                                    <a href="<?php echo seo_url('shop/catalog/categories/view/' . $category->id); ?>" class="parCat">
                                                                        <img src="<?php echo get_image($category->icon); ?>">
                                                                        <span><?php echo $category->name; ?></span>
                                                                    </a>
                                                                    <?php if ($subs = $this->main->gets_paging('categories', 0, 6, array('parent' => $category->id, 'active' => 1), 'sort_order asc')) { ?>
                                                                        <div class="navSubCat">
                                                                            <?php foreach ($subs->result() as $sub) { ?>
                                                                                <a href="<?php echo seo_url('shop/catalog/categories/view/' . $sub->id); ?>"><?php echo $sub->name; ?></a>
                                                                            <?php } ?>
                                                                            <?php if ($subs->num_rows() == 6) { ?>
                                                                                <a href="<?php echo seo_url('shop/catalog/categories/view/' . $category->id); ?>">Lihat semua</a>
                                                                            <?php } ?>
                                                                        </div>
                                                                    <?php } ?>
                                                                </li>
                                                            <?php } ?>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="navigation_right">
                                                <ul class="navigation_links">
                                                    <li class="nav-item">
                                                        <a href="<?php echo site_url('shop'); ?>">
                                                            <span>Beranda</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="navigation_icon" id="cart-target-mobile">
                                                <div class="navigation_icon_group">
                                                    <div class="icon_cart">
                                                        <div class="cart-info-group">
                                                            <a href="<?php echo site_url('shop/cart'); ?>" class="cart dropdown-toggle dropdown-link" data-toggle="dropdown">
                                                                <i class="sub-dropdown1 visible-sm visible-md visible-lg"></i>
                                                                <i class="sub-dropdown visible-sm visible-md visible-lg"></i>
                                                                <div class="num-items-in-cart">
                                                                    <div class="items-cart-left">
                                                                        <img class="cart_img" src="<?php echo site_url('../assets/frontend/images/bg-cart.png'); ?>">
                                                                        <?php if ($this->cart->contents()) { ?>
                                                                            <span class="cart_text icon"><span class="number"><?php echo $this->cart->total_items(); ?></span></span>
                                                                        <?php } ?>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            <div class="dropdown-menu cart-info">
                                                                <div class="cart-content">
                                                                    <?php if ($this->cart->contents()) { ?>
                                                                        <div class="items control-container">
                                                                            <?php
                                                                            foreach ($this->cart->contents() as $item) {
                                                                                $id = explode('-', $item['id'])
                                                                                ?>
                                                                                <div class="row">
                                                                                    <div class="cart-left">
                                                                                        <a class="cart-image" href="<?php echo seo_url('catalog/products/view/' . $id[0]); ?>">
                                                                                            <img src="<?php echo get_image($item['image']); ?>" alt="" title="">
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="cart-right">
                                                                                        <div class="cart-title"><a href="<?php echo seo_url('catalog/products/view/' . $id[0]); ?>"><?php echo $item['name']; ?></a></div>
                                                                                        <div class="cart-price"><span class="money"><?php echo rupiah($item['price']); ?></span><span class="x"> x <?php echo number($item['qty']); ?></span></div>
                                                                                    </div>
                                                                                </div>
                                                                            <?php } ?>
                                                                        </div>
                                                                        <div class="subtotal"><span>Subtotal:</span><span class="cart-total-right money"><?php echo rupiah($this->cart->total()); ?></span></div>
                                                                        <div class="action"><button class="btn" onclick="window.location = '<?php echo site_url('shop/cart'); ?>'">View Cart<i class="fa fa-caret-right"></i></button><button class="btn float-right" onclick="window.location = '<?php echo site_url('shop/cart'); ?>'">CHECKOUT<i class="fa fa-caret-right"></i></button></div>
                                                                    <?php } else { ?>
                                                                        <div class="items control-container">
                                                                            <p style="padding: 15px;">Keranjang masih kosong.</p>
                                                                        </div>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <script>
                    function addaffix(scr) {
                        if ($(window).innerWidth() >= 992) {
                            if (scr > 170) {
                                if (!$('#top').hasClass('affix')) {
                                    $('#top').addClass('affix').addClass('fadeInDown animated');
                                }
                            } else {
                                if ($('#top').hasClass('affix')) {
                                    $('#top').prev().remove();
                                    $('#top').removeClass('affix').removeClass('fadeInDown animated');
                                }
                            }
                        } else
                            $('#top').removeClass('affix');
                    }
                    $(window).scroll(function () {
                        var scrollTop = $(this).scrollTop();
                        addaffix(scrollTop);
                    });
                    $(window).resize(function () {
                        var scrollTop = $(this).scrollTop();
                        addaffix(scrollTop);
                    });
                </script>
            </div>
        </header>
        <div class="fix-sticky"></div>
        <div class="notification">
            <?php
            if ($this->ion_auth->logged_in()) {
                if (!$user->verification_phone && $this->uri->segment(2) != 'verification_phone') {
                    ?>
                    <div class="alert alert-danger text-center no-margin">Segera lakukan <b>Verifikasi No. Handphone</b> Anda <a class="link" href="<?php echo site_url('shop/member/verification_phone'); ?>">disini</a></div>
                    <?php
                }
                if ($orders_pending) {
                    if ($orders_pending->num_rows() > 1) {
                        ?>
                        <div class="alert alert-warning text-center no-margin"><b>Segera lakukan pembayaran</b> untuk <b><?php echo $orders_pending->num_rows(); ?></b> tagihan sebelum kadaluarsa. Selengkapnya lihat <a class="link" href="<?php echo site_url('shop/member/history'); ?>">disini</a></div>
                        <?php
                    } else {
                        $op = $orders_pending->row();
                        ?>
                        <div class="alert alert-warning text-center no-margin">Segera lakukan pembayaran</b> sebesar <b><?php echo rupiah($op->total); ?></b> sebelum <b><?php echo get_date_indo_full($op->due_date); ?></b>. Selengkapnya lihat <a class="link" href="<?php echo site_url('shop/member/order_detail/' . encode($op->id)); ?>">disini</a></div>
                        <?php
                    }
                }
                if ($this->router->fetch_class() == 'member') {
                    if ($notification_invoice) {
                        foreach ($notification_invoice->result() as $invoice) {
                            $invoice_code = explode(',', $invoice->invoice);
                            $code = '';
                            foreach ($invoice_code as $i => $ic) {
                                $ic = explode('-', $ic);
                                $code .= '<a class="link" href="' . site_url('shop/member/order_detail/' . encode($ic[1])) . '"><b>' . $ic[0] . '</b></a>' . ($i + 1 < count($invoice_code) ? ', ' : '');
                            }
                            echo '<div class="alert alert-success text-center no-margin">Invoice ' . $code;
                            if ($invoice->status == settings('order_process_status')) {
                                echo ' sedang <b>Diproses</b> oleh penjual.';
                            } elseif ($invoice->status == settings('order_shipped_status')) {
                                echo ' sudah <b>Dikirim</b>.';
                            } elseif ($invoice->status == settings('order_ready_pickup_status')) {
                                echo ' sudah bisa <b>Diambil</b> di alamat penjual.';
                            } elseif ($invoice->status == settings('order_delivered_status')) {
                                echo ' sudah <b>Diterima</b>. Segera konfirmasi penerimaan.';
                            } elseif ($invoice->status == settings('order_complete_pickup_status')) {
                                echo ' sudah <b>Diambil</b>. Segera konfirmasi penerimaan.';
                            }
                            echo '</div>';
                        }
                    }
                }
            }
            ?>
        </div>
        <div class="page-container" id="PageContainer">
            <main class="main-content" id="MainContent" role="main">
                <?php echo $output; ?>
            </main>
        </div>
        <footer class="footer">
            <div id="shopify-section-theme-footer" class="shopify-section">
                <section class="footer_newsoc_block">
                    <div class="footer_newsoc_wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="footer_newsoc_inner">
                                    <div class="footer_newsoc_content ma-shop">
                                        <div class="footer_newsletter_content">
                                            <span class="newsletter-title">
                                                Dapatkan Promo &amp; info terbaru
                                            </span>
                                            <div class="newsletter_content">
                                                <form>
                                                    <input type="email" class="form-control" id="email-subscribe" required="" name="email" placeholder="Masuka alamat email anda">
                                                    <button type="button" id="btn-subscribe" class="btn_newsletter_send btn" style="background-color: #97C23C;">Kirim</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="footer_linklist_block">
                    <div class="footer_linklist_wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="footer_linklist_inner">
                                    <?php
                                    $footer = json_decode(settings('footer'), TRUE);
                                    $n = 0;
                                    foreach ($footer as $foot) {
                                    ?>
                                        <div class="col-sm-3 linklist_item">
                                            <span class="footer-title"><?php echo $foot['title'] ?></span>
                                            <div class="linklist_content">
                                                <?php if ($foot['type'] == 'list') {?>
                                                    <ul class="linklist_menu">
                                                        <?php foreach ($foot['content'] as $link) { ?>
                                                            <li><a href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a></li>
                                                        <?php } ?>
                                                    </ul>
                                                <?php } else { ?>
                                                    <div class="linklist_content">
                                                        <p style="color: #333; margin: 0px; margin-bottom: 5px;"><?php echo nl2br($foot['content']); ?></p>
                                                        <?php $n++; ?>
                                                        <?php if ($n == 1) { ?>
                                                            <a href="https://wa.me/628118332018" target="_blank"><img src="<?php echo base_url('../assets/frontend/images/bank/whatsapp.png') ?>"></a>
                                                        <?php } ?>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="copy-right">
                    <div class="copy-right-wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="copy-right-inner">
                                    <div class="copy-right-group">
                                        <div class="col-md-1" style="padding-left: 0px; padding-right: 0px;">
                                            <div class="social_content">
                                                <?php
                                                $social_media_link = json_decode(settings('social_media_link'), true);
                                                if ($social_media_link) {
                                                    foreach ($social_media_link as $name => $link) {
                                                        echo '<a href="' . $link . '" class="icon-social ' . $name . '"><i class="fa fa-' . $name . '"></i></a>';
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-md-11" style="padding-left: 0px;">
                                            <div class="" style="text-align: left; padding-left: 0px;">
                                                <a target="_blank" href="https://play.google.com/store/apps/details?id=com.tokomanamana.tokomanamana"><img style="height:30px;" src="<?php echo base_url('../assets/frontend/images/dl-googleplay.png'); ?>"></a>
                                            </div>
                                        </div>
                                        <div class="col-md-7" style="padding-left: 0px; padding-right: 0px;">
                                            <div style="text-align: left;">
                                                <p style="font-weight: bold; font-size: 14px; text-align: left; margin: 15px auto;">Metode Pembayaran</p>
                                                <img src="<?php echo base_url('../assets/frontend/images/bank/bca.png'); ?>">
                                                <img src="<?php echo base_url('../assets/frontend/images/bank/mandiri.webp'); ?>">
                                                <img src="<?php echo base_url('../assets/frontend/images/bank/bni.webp'); ?>">
                                                <img src="<?php echo base_url('../assets/frontend/images/bank/bri.webp'); ?>">
                                                <img src="<?php echo base_url('../assets/frontend/images/bank/klikpay.webp'); ?>">
                                                <img src="<?php echo base_url('../assets/frontend/images/bank/sakuku.jpeg'); ?>" style="width: 20px;">
                                                <img src="<?php echo base_url('../assets/frontend/images/bank/kredivo.jpeg'); ?>" style="width: 40px;">
                                                <img src="<?php echo base_url('../assets/frontend/images/bank/visa.png'); ?>">
                                                <img src="<?php echo base_url('../assets/frontend/images/bank/mastercard.webp'); ?>">
                                                <img src="<?php echo base_url('../assets/frontend/images/bank/alfamart.png'); ?>" style="width: 60px;">
                                                <img src="<?php echo base_url('../assets/frontend/images/bank/ovo.png'); ?>" style="width: 60px;">
                                            </div>
                                        </div>
                                        <div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">
                                            <div style="text-align: left;">
                                                <p style="font-weight: bold; font-size: 14px; text-align: left; margin: 15px auto;">Jasa Pengiriman</p>
                                                <img src="<?php echo base_url('../assets/frontend/images/bank/jne.jpg') ?>">
                                                <img src="<?php echo base_url('../assets/frontend/images/bank/tiki.jpg') ?>">
                                                <img src="<?php echo base_url('../assets/frontend/images/bank/pos.jpg') ?>">
                                                <img src="<?php echo base_url('../assets/frontend/images/bank/grab.png') ?>" style="width: 60px;">
                                            </div>
                                        </div>
                                        <div class="col-md-2" style="padding-left: 0px; padding-right: 0px;">
                                            <div style="text-align: left;">
                                                <p style="font-weight: bold; font-size: 14px; text-align: left; margin: 15px auto;">Keamanan Belanja</p>
                                                <img src="<?php echo base_url('../assets/frontend/images/bank/vbv.jpeg') ?>" style="width: 50px;">
                                                <img src="<?php echo base_url('../assets/frontend/images/bank/mastercard.jpeg') ?>" style="width: 50px;">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </footer>
        <div id="modal-location" class="modal" role="dialog" aria-hidden="true" tabindex="-1" data-width="800" style="display: none;">
            <div class="modal-dialog fadeIn animated">
                <div class="modal-content">
                    <div class="modal-header">
                        <h1>Set Lokasi Anda</h1>
                    </div>
                    <div class="modal-body"></div>
                    <input type="hidden" id="temp-lat">
                    <input type="hidden" id="temp-lng">
                    <div class="modal-footer">
                        <button type="button" id="my-location" class="btn btn-success" style="background: #fff; color: #000; float: left;">Lokasi Saya</button>
                        <button type="button" id="btn-set-location" class="btn btn-primary">Set Lokasi</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="float-right-icon">
            <ul>
                <li>
                    <div id="scroll-to-top" data-toggle="" data-placement="left" title="Scroll to Top" class="off ma-shop">
                        <i class="fa fa-angle-up"></i>
                    </div>
                </li>
            </ul>
        </div>
        <!-- <div class="mobile-menu-footer">
            <div class="navbar-fixed-footer">
                <a href="javascript:void(0);" onclick="window.location = '<?php echo base_url('shop');?>'"><i class="fa fa-home"></i></a>
                <a href="javascript:void(0);" onclick="window.history.back()"><i class="fa fa-file-text-o"></i></a>
                <a href="javascript:void(0);" onclick="window.location = '<?php echo base_url('cart/total_items'); ?>'"><i class="fa fa-cart-arrow-down"></i></a>
                <a href="javascript:void(0);" onclick="window.scrollTo({ top: 0, behavior: 'smooth' });"><i class="fa fa-angle-up"></i></a>
                <a href="javascript:void(0);" onclick="window.history.back()"><i class="fa fa-arrow-left"></i></a>
            </div> 
        </div> -->

    </body>
</html>
