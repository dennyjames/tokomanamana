    <!DOCTYPE html>
    <html lang="en">

    <head>
        <link href="<?php echo site_url('../assets/backend/css/lightbox.css');?>" rel="stylesheet" />
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php echo "{$title}"; ?></title>
        <?php
        foreach ($css as $file) {
            echo "\n    ";
            echo '<link href="' . $file . '?timestamp=' . time() . '" rel="stylesheet" type="text/css" />';
        }
        echo "\n";
        ?>
        <script type="text/javascript">
            var base_url = '<?php echo base_url(); ?>';
            var current_url = '<?php echo current_url(); ?>';
            var decimal_digit = '<?php echo settings('number_of_decimal'); ?>';
            var decimal_separator = '<?php echo settings('number_separator_decimal'); ?>';
            var thousand_separator = '<?php echo settings('number_separator_thousand'); ?>';
            var datatable_data = {};
        </script>
        <style>
           #overlayWindow{opacity:.5;background-color:#ccc;position:absolute;top:50;left:0;height:100%;width:100%;z-index:999}.navbar-brand{padding-bottom:0;padding-top:0}.navbar-brand img{display:inline-block;height:85%}.navigation>li.active>a,.navigation>li.active>a:focus,.navigation>li.active>a:hover{background-color:#97c23c}.sidebar-xs .sidebar-main .navigation>li>a>span{background-color:#97c23c;border:1px solid #97c23c}.profile_picture_container{margin-top:10px;margin-bottom:5px;height:120px;width:120px;max-width:120px;max-height:120px;margin-left: auto;margin-right: auto;}.profile_picture_container_small{margin-top:10px;margin-bottom:5px;height:50px;width:50px;max-width:50px;max-height:50px;margin-left: auto;margin-right: auto;}.profile_picture{height:120px;width:120px;max-width:120px;max-height:120px;border-radius: 50%;border:solid 3px;border-color: #97c23c;}.profile_picture_small{height:50px;width:50px;max-width:50px;max-height:50px;border-radius: 50%;border:solid 3px;border-color: #97c23c;}.store_name{text-align:center;font-weight: bold; font-size: large;}.store_name_small{text-align:center;font-weight: bold; font-size: x-small;}
        </style>
    </head>

    <body>
        <div class="navbar navbar-inverse">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?php echo site_url(); ?>">
                    <img src="<?php echo site_url('../assets/backend/images/logo-white.png'); ?>" alt="">

                </a>

                <ul class="nav navbar-nav visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>

            <div class="navbar-collapse collapse" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-notification2"></i>
                            <span class="visible-xs-inline-block position-right">Notifikasi</span>
                            <span class="badge bg-warning-400" id="notification-delivery-late-count"><?php echo count($notifications); ?></span>
                        </a>

                        <div class="dropdown-menu dropdown-content">
                            <div class="dropdown-content-heading">
                                Pemberitahuan Pesanan
                            </div>
                            <?php if ($notifications) { ?>
                                <ul class="media-list dropdown-content-body width-350">
                                    <?php foreach ($notifications as $notif) { ?>
                                        <li class="media" onclick="window.location.href = '<?php echo $notif['url']; ?>'">
                                            <div class="media-body">
                                                <?php echo $notif['msg']; ?>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul>

                                <div class="dropdown-content-footer">
                                    <a href="<?php echo site_url('orders'); ?>" target="_blank" data-popup="tooltip" title="Pesanan"><i class="icon-menu display-block"></i></a>
                                </div>
                            <?php } else { ?>
                                <p class="pl-20 pr-20">Tidak ada pemberitahuan.</p>
                            <?php } ?>
                        </div>
                    </li>
                    <?php  if($user->merchant_type == 'branch') { ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-notification2"></i>
                            <span class="visible-xs-inline-block position-right">Notifikasi Pesanan Merchant</span>
                            <span class="badge bg-warning-400" id="notification-delivery-late-count"><?php echo count($merchant_notifications); ?></span>
                        </a>

                            <div class="dropdown-menu dropdown-content">
                                <div class="dropdown-content-heading">
                                    Pemberitahuan Pesanan Merchant
                                </div>
                                <?php if ($merchant_notifications) { ?>
                                    <ul class="media-list dropdown-content-body width-350">
                                        <?php foreach ($merchant_notifications as $notif) { ?>
                                            <li class="media" onclick="window.location.href = '<?php echo $notif['url']; ?>'">
                                                <div class="media-body">
                                                    <?php echo $notif['msg']; ?>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </ul>

                                    <div class="dropdown-content-footer">
                                        <a href="<?php echo site_url('orders/orders_merchant'); ?>" target="_blank" data-popup="tooltip" title="Pesanan"><i class="icon-menu display-block"></i></a>
                                    </div>
                                <?php } else { ?>
                                    <p class="pl-20 pr-20">Tidak ada pemberitahuan.</p>
                                <?php } ?>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
                <div class="navbar-text" style="padding: 0">
                    <?php if ($user->merchant_status == 0) { ?>
                        <div class="alert alert-warning" style="padding: 12px; margin: 0; display: inline-block; float: left;">
                            <p>Akun Anda sedang dalam proses verifikasi.</p>
                        </div>
                    <?php } ?>
                    <?php if($user->merchant_type == 'merchant'){ ?>
                    <?php if (!isset($user->signature_img) && ($user->id == $user->merchant_owner)) { ?>
                        <div class="alert alert-warning" style="padding: 12px; margin: 0; display: inline-block; float: left;">
                            <p>Signature Image belum ada, Tanda tangani <a href="<?php echo site_url('auth/verification_signature'); ?>">disini</a> !</p>
                        </div>
                    <?php }} ?>
                    <?php if ($user->verification_phone == 0) { ?>
                        <div class="alert alert-danger" style="padding: 12px; margin: 0; display: inline-block; float: left;">
                            <p>Segera verifikasi nomor handphone Anda <a href="<?php echo site_url('auth/verification_phone'); ?>">disini</a>.</p>
                        </div>
                    <?php } ?>
                    <?php if ($user->token_confirm_status == 0) { ?>
                        <div class="alert alert-danger" id="email_confirm" style="padding: 12px; margin: 0; display: inline-block; float: left;">
                            <p>Segera verifikasi email Anda <a href="<?php echo site_url('auth/send_email_verification'); ?>">disini</a>.</p>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('verification_success')) { ?>
                        <div class="alert alert-success" style="padding: 12px; margin: 0; display: inline-block; float: left;">
                            <p><?php echo $this->session->flashdata('verification_success'); ?></p>
                        </div>
                    <?php } ?>
                </div>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <span><?php echo $user->fullname; ?></span>
                            <i class="caret"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="<?php echo site_url('settings/profile'); ?>"><i class="icon-user"></i> Profil Saya</a></li>
                            <li><a href="<?php echo site_url('auth/logout'); ?>"><i class="icon-switch2"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <?php if ($user->merchant_type == 'merchant') {
                if ($user->verification_phone == 0 || $user->token_confirm_status == 0 || (!isset($user->signature_img))) { ?>
                    <div id="overlayWindow"></div>
        <?php }
            }else{
                if ($user->verification_phone == 0 || $user->token_confirm_status == 0 ) { ?>
                    <div id="overlayWindow"></div>

        <?php }} ?>

        <div class="page-container">
            <div class="page-content">
                <div class="sidebar sidebar-main">
                    <div class="sidebar-content">
                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <div>
                                    <div id="pp_container" class="profile_picture_container">
                                      <?php if($user->profile_picture == NULL || $user->profile_picture == ''){ ?>
                                     <a href="<?php echo site_url('../'.$user->merchant_username); ?>"><img class="profile_picture" id="pp" src="<?php echo base_url('../files/images/profilepicture_merchant/default_image.jpeg'); ?>"></a> 
                                      <?php }else{ ?>
                                     <a href="<?php echo site_url('../'.$user->merchant_username); ?>"><img class="profile_picture" id="pp" src="<?php echo base_url('../files/images/' . $user->profile_picture); ?>"></a>
                                     <?php } ?>
                                    </div> 
                                    <div id="s_name" class="store_name"><?= $user->merchant_name;?></div>  
                                </div>
                                <ul class="navigation navigation-main navigation-accordion">
                                    <li class="<?php echo menu_active($menu, 'dashboard'); ?>"><a href="<?php echo site_url(); ?>"><i class="icon-home4"></i> <span><?php echo lang('dashboard'); ?></span></a></li>
                                    <?php if ($user->merchant_type == 'principle_branch'){ ?>
                                        <?php if ($this->ion_auth->logged_in() || $this->aauth->is_allowed('product/principal')) { ?>
                                            <li class="<?php echo menu_active($menu, 'product'); ?>"><a href="<?php echo site_url('products/principal'); ?>"><i class="icon-archive"></i> <span><?php echo lang('product'); ?></span></a></li>
                                        <?php } ?>
                                    <?php } else if ($user->merchant_type == 'merchant') { ?>
                                        <?php if ($this->ion_auth->logged_in() || $this->aauth->is_allowed('products/productv2')) { ?>
                                            <li class="<?php echo menu_active($menu, 'product'); ?>">
                                                <a href="<?php echo site_url('products/productv2'); ?>"><i class="icon-archive"></i> <span><?php echo lang('product'); ?></span></a>
                                            </li>

                                        <?php } ?>
                                    <?php } ?>
                                     <?php if ($this->ion_auth->logged_in() || $this->aauth->is_allowed('etalase')) { ?>
                                        <?php if ($user->merchant_type != 'principle_branch') : ?>
                                            <li class="<?php echo menu_active($menu, 'etalase'); ?>">
                                                <a href="<?php echo site_url('products/etalase'); ?>"><i class="glyphicon glyphicon-tag"></i> <span><?php echo lang('storefront'); ?></span></a>
                                            </li>
                                        <?php endif; ?>
                                    <?php } ?>
                                    <?php if ($this->ion_auth->logged_in() || $this->aauth->is_allowed('order')) { ?>
                                        <li class="<?php echo menu_active($menu, 'order'); ?>">
                                            <a href="<?php echo site_url('orders'); ?>"><i class="icon-cart"></i> <span><?php echo lang('order'); ?></span></a>
                                        </li>
                                    <?php } ?>
                                     <?php if ($user->merchant_type == 'principle_branch') : ?>
                                        <li class="<?php echo menu_active($menu, 'stock'); ?>">
                                            <a href="<?php echo site_url('stock'); ?>"><i class="glyphicon glyphicon-folder-open"></i> <span>Manajemen Stok</span></a>
                                        </li>
                                    <?php endif; ?>
                                    <?php if ($this->ion_auth->logged_in() || $this->aauth->is_allowed('config_merchant_page')) { ?>
                                        <li class="<?php echo menu_active($menu, 'config_merchant_page'); ?>">
                                            <a href="#"><i class="glyphicon glyphicon-blackboard"></i> <span><?php echo lang('config_merchant_page'); ?></span></a>
                                            <ul>
                                                <li class="<?php echo menu_active($menu, 'banner_merchant'); ?>">
                                                    <a href="<?php echo site_url('config_merchant_page/banner_merchant'); ?>"><span>Banner</span></a>
                                                </li>   
                                                <li class="<?php echo menu_active($menu, 'profile_picture'); ?>"><a href="<?php echo site_url('config_merchant_page/profile_picture'); ?>"><span>Profile Picture</span></a></li>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                    <?php if ($this->ion_auth->logged_in() || $this->aauth->is_allowed('reward')) { ?>
                                        <?php if ($user->merchant_type != 'principle_branch') : ?>
                                        <li class="<?php echo menu_active($menu, 'reward'); ?>">
                                            <a href="#"><i class="icon-stars"></i> <span>Reward</span></a>
                                            <ul>
                                                <li class="<?php echo menu_active($menu, 'reward'); ?>">
                                                    <a href="<?php echo site_url('orders/reward'); ?>"><span>Tukar Poin</span></a>
                                                </li>
                                                <li class="<?php echo menu_active($menu, 'history_reward'); ?>"><a href="<?php echo site_url('orders/reward/history'); ?>">History</a></li>
                                            </ul>
                                        </li>
                                        <?php endif; ?>
                                    <?php } ?>
                    
                                    <?php if ($this->ion_auth->logged_in() || $this->aauth->is_allowed('report')) { ?>
                                        <li class="<?php echo menu_active($menu, 'report'); ?>">
                                            <a href="#"><i class="icon-chart"></i> <span><?php echo lang('report'); ?></span></a>
                                            <ul>
                                                <li class="<?php echo menu_active($menu, 'report_sale'); ?>"><a href="<?php echo site_url('reports/sales'); ?>"><?php echo lang('report_sales'); ?></a></li>
                                                
                                                    <li class="<?php echo menu_active($menu, 'report_balance'); ?>"><a href="<?php echo site_url('reports/balances'); ?>"><?php echo lang('report_balance'); ?></a></li>
                                             
                                            </ul>
                                        </li>
                                    <?php } ?>
                                    <?php if ($user->merchant_status) { ?>
                                        <?php if ($this->ion_auth->logged_in() || $this->aauth->is_allowed('setting')) { ?>
                                            <li class="<?php echo menu_active($menu, 'setting'); ?>">
                                                <a href="#"><i class="icon-cog4"></i> <span><?php echo lang('setting'); ?></span></a>
                                                <ul>
                                                    <li class="<?php echo menu_active($menu, 'setting_store'); ?>"><a href="<?php echo site_url('settings'); ?>"><?php echo lang('setting'); ?></a></li>
                                                    <?php if (($this->ion_auth->logged_in() || $this->aauth->is_allowed('setting/user')) && $user->merchant_type != 'principle_branch') { ?>
                                                        <li class="<?php echo menu_active($menu, 'setting_user'); ?>">
                                                            <a href="<?php echo site_url('settings/users'); ?>"><?php echo lang('user'); ?></a>
                                                            <!-- <ul>
                                                        <li class="<?php echo menu_active($menu, 'setting_user_user'); ?>"><a href="<?php echo site_url('settings/users'); ?>"><?php echo lang('user'); ?></a></li>
                                                        <?php if ($this->ion_auth->logged_in() || $this->aauth->is_allowed('setting/user/group')) { ?>
                                                            <li class="<?php echo menu_active($menu, 'setting_user_group'); ?>"><a href="<?php echo site_url('settings/users/groups'); ?>"><?php echo lang('user_group'); ?></a></li>
                                                        <?php } ?>
                                                        <?php if ($this->ion_auth->logged_in() || $this->aauth->is_allowed('setting/user/permission')) { ?>
                                                            <li class="<?php echo menu_active($menu, 'setting_user_permission'); ?>"><a href="<?php echo site_url('settings/users/permissions'); ?>"><?php echo lang('user_permission'); ?></a></li>
                                                        <?php } ?>
                                                    </ul> -->
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                    <!-- <?php if ($this->ion_auth->logged_in() && $user->merchant_type == 'merchant' && $total_stock > 0) { ?>
                                    <li class="<?php echo menu_active($menu, 'shop'); ?>"><a href="<?php echo site_url('shop'); ?>" target="_blank"><i class="icon-cart"></i> <span>Belanja Yuk!</span></a></li>
                                    <?php } ?> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo $output; ?>
            </div>
        </div>
        <script type="text/javascript">


            var site_url = '<?php echo site_url(); ?>';
            var current_url = '<?php echo current_url(); ?>';



        </script>
        <?php
        foreach ($js as $file) {
            echo "\n    ";
            echo '<script src="' . $file . '?timestamp=' . time() . '"></script>';
        }
        echo "\n";
        ?>
        <script src="<?php echo site_url('../assets/backend/js/idle.js'); ?>"></script>
        <script>
             $(function () {
                $('.sidebar-main-toggle').on('click',function(){
                    $('#pp').toggleClass('profile_picture profile_picture_small');
                    $('#pp_container').toggleClass('profile_picture_container profile_picture_container_small');
                    $('#s_name').toggleClass('store_name store_name_small');
                })
                $('#email_confirm').on('click', function () {
                     swal({
                        title: "Segera check email anda!",
                        showConfirmButton: false,
                        type: "info",
                        html: true
                    });
                });
            });
            var awayCallback = function() {
                //console.log(new Date().toTimeString() + ": away");
                swal({
                    title: 'Anda sudah idle terlalu lama!',
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#EF5350",
                    confirmButtonText: 'Login',
                    cancelButtonText: lang.button.cancel,
                    closeOnConfirm: false
                }, function() {
                    window.location = "<?php echo site_url('auth/logout'); ?>";
                });
            };

            var awayBackCallback = function() {
                //console.log(new Date().toTimeString() + ": back");
            };
            var onVisibleCallback = function() {
                //console.log(new Date().toTimeString() + ": now looking at page");
            };

            var onHiddenCallback = function() {
                //console.log(new Date().toTimeString() + ": not looking at page");
            };
            //this is one way of using it.
            /*
            var idle = new Idle();
            idle.onAway = awayCallback;
            idle.onAwayBack = awayBackCallback;
            idle.setAwayTimeout(2000);
            idle.start();
            */
            //this is another way of using it
            var idle = new Idle({
                onHidden: onHiddenCallback,
                onVisible: onVisibleCallback,
                onAway: awayCallback,
                onAwayBack: awayBackCallback,
                awayTimeout: 7200000 //away with 2 hours of inactivity
            }).start();
        </script>
        <script src="<?php echo site_url('../assets/backend/js/lightbox.js');?>"></script>
    </body>

    </html>