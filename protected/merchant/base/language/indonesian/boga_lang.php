<?php

$lang['dashboard'] = 'Dasbor';
$lang['product'] = 'Produk';
$lang['product_tanaka'] = 'Produk Tanaka';
$lang['product_other'] = 'Produk Lainnya';
$lang['category'] = 'Kategori';
$lang['brand'] = 'Merek';
$lang['order'] = 'Pesanan';
$lang['storefront'] = 'Etalase';
$lang['config_merchant_page'] = 'Desain';
$lang['order'] = 'Pesanan';
$lang['customer'] = 'Konsumen';
$lang['report'] = 'Laporan';
$lang['report_sales'] = 'Penjualan';
$lang['report_balance'] = 'Saldo';
$lang['setting'] = 'Pengaturan';
$lang['user'] = 'Pengguna';
$lang['user_group'] = 'Grup Pengguna';
$lang['user_permission'] = 'Hak Akses';

$lang['actions_th'] = 'Opsi';

$lang['button_save'] = '<i class="icon-floppy-disk position-left"></i> Simpan';
$lang['button_save_new'] = '<i class="icon-floppy-disk position-left"></i> Simpan dan Tambah Baru';
$lang['button_cancel'] = '<i class="icon-cancel-square2 position-left"></i> Batal';
$lang['button_view'] = '<i class="icon-search4"></i> Lihat';
$lang['button_delete'] = '<i class="icon-trash"></i> Hapus';
$lang['button_edit'] = '<i class="icon-pencil"></i> Edit';
$lang['button_add_image'] = '<i class="icon-plus3 position-left"></i> Tambah Gambar';
$lang['button_edit_image'] = '<i class="icon-pencil position-left"></i> Ubah Gambar';
$lang['button_delete_image'] = '<i class="icon-cross2 position-left"></i> Hapus Gambar';

$lang['banner_warning'] = '*Kami anjurkan gambar banner ber-dimensi <b style="font-weight:900;">1200x235 pixel</b> untuk memastikan tampilan terbaik';
$lang['pp_warning'] = '*Kami anjurkan profile picture ber-dimensi <b style="font-weight:900;">120x120 pixel</b> untuk memastikan tampilan terbaik';

$lang['message_save_success'] = 'Data berhasil disimpan.';
$lang['message_delete_success'] = 'Data berhasil dihapus.';
$lang['message_save_error'] = 'Data gagal disimpan.';
$lang['message_delete_error'] = 'Data gagal dihapus.';

$lang['January'] = 'Januari';
$lang['February'] = 'Februari';
$lang['March'] = 'Maret';
$lang['April'] = 'April';
$lang['May'] = 'Mei';
$lang['June'] = 'Juni';
$lang['July'] = 'Juli';
$lang['August'] = 'Agustus';
$lang['September'] = 'September';
$lang['October'] = 'Oktober';
$lang['November'] = 'Nopember';
$lang['December'] = 'Desember';

$lang['yes'] = 'Ya';
$lang['no'] = 'Tidak';
$lang['enabled'] = 'Aktif';
$lang['disabled'] = 'Nonaktif';

// Direct Shop
$lang['account_order_history'] = 'Riwayat Pesanan';
$lang['account_profile'] = 'Profil Saya';
$lang['account_security'] = 'Ubah Password';
$lang['account_wishlist'] = 'Wishlist';
$lang['favourite_shop'] = 'Toko Favorit';
$lang['account_review'] = 'Ulasan';
$lang['account_address'] = 'Alamat Pengiriman';
$lang['account_payment_confirmation'] = 'Konfirmasi Pembayaran';

$lang['categories'] = 'Semua Kategori';
$lang['search_product'] = 'Cari produk ...';
$lang['contact_us'] = 'Kontak Kami';
$lang['my_account'] = 'Akun Saya';
$lang['login_register'] = 'Login/Register';
$lang['register'] = 'Register';
$lang['login'] = 'Login';
$lang['logout'] = 'Logout';