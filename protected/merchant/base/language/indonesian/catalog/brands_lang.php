<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['brand_heading'] = 'Merek';
$lang['brand_sub_heading'] = 'Kelola Data Merek';
$lang['brand_list_heading'] = 'Daftar Merek';
$lang['brand_add_heading'] = 'Tambah Merek';
$lang['brand_edit_heading'] = 'Edit Merek';

$lang['brand_image_th'] = 'Gambar';
$lang['brand_name_th'] = 'Nama Merek';

$lang['brand_form_general_tabs'] = 'Umum';
$lang['brand_form_seo_tabs'] = 'SEO';

$lang['brand_form_name_label'] = 'Nama';
$lang['brand_form_image_label'] = 'Gambar';
$lang['brand_form_description_label'] = 'Deskripsi';
$lang['brand_form_meta_title_label'] = 'Meta Judul';
$lang['brand_form_meta_description_label'] = 'Meta Deskripsi';
$lang['brand_form_meta_keyword_label'] = 'Meta Keyword';

$lang['brand_form_name_placeholder'] = 'Masukkan nama merek';
$lang['brand_form_description_placeholder'] = 'Masukkan deskripsi dari merek ini. Deskripsi akan ditampilkan di halaman yang menampilkan semua produk dari merek ini';

$lang['brand_form_add_image_button'] = 'Tambah Gambar';
$lang['brand_form_edit_image_button'] = 'Ubah Gambar';
$lang['brand_form_delete_image_button'] = 'Hapus Gambar';

$lang['brand_save_success_message'] = "Merek '%s' berhasil disimpan.";
$lang['brand_save_error_message'] = "Merek '%s' gagal disimpan.";
$lang['brand_delete_success_message'] = "Merek '%s' berhasil dihapus.";
$lang['brand_delete_error_message'] = "Merek '%s' gagal dihapus.";