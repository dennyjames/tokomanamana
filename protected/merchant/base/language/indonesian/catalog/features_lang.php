<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['feature_heading'] = 'Fitur';
$lang['feature_list_heading'] = 'Daftar Fitur';
$lang['feature_add_heading'] = 'Tambah Fitur';
$lang['feature_edit_heading'] = 'Edit Fitur';

$lang['feature_name_th'] = 'Nama Fitur';
$lang['feature_variant_value_th'] = 'Jumlah Variasi';

$lang['feature_form_name_label'] = 'Nama';
$lang['feature_form_type_label'] = 'Jenis Inputan';

$lang['feature_form_name_placeholder'] = 'Masukkan nama fitur';

$lang['feature_save_success_message'] = "Fitur '%s' berhasil disimpan.";
$lang['feature_save_error_message'] = "Fitur '%s' gagal disimpan.";
$lang['feature_delete_success_message'] = "Fitur '%s' berhasil dihapus.";
$lang['feature_delete_error_message'] = "Fitur '%s' gagal dihapus.";

//feature variant 

$lang['feature_variant_heading'] = 'Variasi Fitur';
$lang['feature_variant_list_heading'] = 'Daftar Variasi Fitur';
$lang['feature_variant_add_heading'] = 'Tambah Variasi';
$lang['feature_variant_edit_heading'] = 'Edit Variasi';

$lang['feature_variant_value_th'] = 'Nilai Variasi';

$lang['feature_variant_form_feature_label'] = 'Fitur';
$lang['feature_variant_form_value_label'] = 'Nilai Variasi';

$lang['feature_variant_form_value_placeholder'] = 'Masukkan nilai variasi dari fitur yang dipilih';