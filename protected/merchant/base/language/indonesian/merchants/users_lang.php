<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['user_heading'] = 'Admin UKM';
$lang['user_sub_heading'] = 'Kelola Data Admin UKM';
$lang['user_list_heading'] = 'Daftar Admin UKM';
$lang['user_add_heading'] = 'Tambah Admin UKM';
$lang['user_edit_heading'] = 'Edit Admin UKM';

$lang['user_name_th'] = 'Nama';
$lang['user_email_th'] = 'Email';
$lang['user_phone_th'] = 'Telepon';
$lang['user_merchant_th'] = 'Nama UKM';

$lang['user_form_name_label'] = 'Nama';
$lang['user_form_email_label'] = 'Email';
$lang['user_form_telephone_label'] = 'Telepon';
$lang['user_form_password_label'] = 'Password';

$lang['user_save_success_message'] = "Admin UKM '%s' berhasil disimpan.";
$lang['user_save_error_message'] = "Admin UKM '%s' gagal disimpan.";
$lang['user_delete_success_message'] = "Admin UKM '%s' berhasil dihapus.";
$lang['user_delete_error_message'] = "Admin UKM '%s' gagal dihapus.";
$lang['user_email_exist_message'] = "Email '%s' sudah terdaftar.";