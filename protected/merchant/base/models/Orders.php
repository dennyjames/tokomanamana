<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends CI_Model {

    public function get($id) {
        $this->db->select('o.*, mu.fullname customer_name, mu.email customer_email, mu.phone customer_phone, p.name shipping_province_name, c.name shipping_city_name, d.name shipping_district_name')
                ->join('merchants m', 'm.id = o.customer', 'left')
                ->join('merchant_users mu', 'mu.id = m.auth', 'left')
                ->join('provincies p', 'p.id = o.shipping_province', 'left')
                ->join('cities c', 'c.id = o.shipping_city', 'left')
                ->join('districts d', 'd.id = o.shipping_district', 'left')
                ->where('o.id', $id);
        $query = $this->db->get('orders_merchant o');
        return ($query->num_rows()>0)?$query->row():false;
    }
    
}
