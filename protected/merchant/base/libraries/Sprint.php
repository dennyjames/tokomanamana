<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Sprint {

    function __construct() {
        $this->app = & get_instance();
//        $this->app->load->model('transactions_model');
//        $this->app->load->model('transactions_meta_model');
//        $this->app->load->model('products_model');
    }

    function process($transaction, $vendor = false) {
        $payment_method = $this->app->main->get('payment_methods', array('name' => $transaction->payment_method));
        $setting = json_decode($payment_method->setting);
        $payload = [
            'channelId' => $setting->channel_id,
            'currency' => 'IDR',
            'transactionNo' => 'MS'.$transaction->id,
            'transactionAmount' => $transaction->total,
            'transactionFee' => 0,
            'transactionDate' => $transaction->date_added,
            'transactionExpire' => $transaction->due_date,
            'transactionFeature' => '',
            'callbackURL' => site_url('member/order_detail/' . encode($transaction->id)),
            'description' => $transaction->code,
//            'customerAccount' => $customer_account,
            'customerName' => $transaction->customer_name,
            'customerEmail' => $transaction->customer_email,
            'customerPhone' => $transaction->customer_phone,
            'customerBillAddress' => $transaction->shipping_address,
            'customerBillCity' => $transaction->shipping_city_name,
            'customerBillState' => $transaction->shipping_province_name,
            'customerBillCountry' => 'ID',
            'customerBillZipCode' => $transaction->shipping_postcode,
            'authCode' => hash('sha256', 'MS'.$transaction->id . $transaction->total . $setting->channel_id . $setting->secret_key),
        ];
        if ($transaction->payment_method == 'bca_va') {
            $payload['customerAccount'] = $setting->code . preg_replace("/[^0-9]/", "", $transaction->code);
        } elseif ($transaction->payment_method == 'kredivo') {
            $products = $this->app->main->gets('order_product', array('order' => $transaction->id));
            $items = array();
            foreach ($products->result() as $product) {
                $item = array(
                    'itemId' => $product->product,
                    'itemName' => $product->name,
                    'quantity' => $product->quantity,
                    'price' => $product->price,
                    'itemURL' => seo_url('catalog/products/view/' . $product->product),
                    'itemType' => 'Product'
                );
                array_push($items, $item);
            }
            $payload['transactionFee'] = json_encode(['shippingfee'=>$transaction->shipping_cost]);
            $payload['itemDetails'] = json_encode($items);
        }
        $payment_request = $this->post($setting->payment_url, $payload);

//        $this->app->main->insert($transaction->id, 'sprint_response', $payment_request);
//
//        if ($payment_request['insertStatus'] == '00') {
        return array('request' => $payload, 'response' => $payment_request);
//             return  json_encode($payment_request);
        // print_r($payment_request); exit;
//            if ($transaction->payment_type == 'bca_klikpay') {
//                $this->bca_klikpay($payment_request['redirectURL'], $payment_request['redirectData']);
//            } elseif ($transaction->payment_type == 'kredivo') {
//                $this->kredivo($payment_request['redirectURL'], $payment_request['redirectData']);
//            } else {
//                $this->virtual_account($transaction, $payment_request);
//            }
//        } else {
//            throw new Exception(json_encode($payment_request), 1);
//        }
    }

    function creditcard_charge($data) {
        return $this->post('/payment/creditcard_charge', $data);
    }

    function bca_klikpay($redirectURL, $redirectData) {
        echo '<form name="redirect" action="' . $redirectURL . '" method="POST">
        <input type="hidden" name="klikPayCode" value="' . $redirectData['klikPayCode'] . '">
        <input type="hidden" name="transactionNo" value="' . $redirectData['transactionNo'] . '">
        <input type="hidden" name="totalAmount" value="' . $redirectData['totalAmount'] . '">
        <input type="hidden" name="currency" value="' . $redirectData['currency'] . '">
        <input type="hidden" name="payType" value="' . $redirectData['payType'] . '">
        <input type="hidden" name="callback" value="' . $redirectData['callback'] . '">
        <input type="hidden" name="transactionDate" value="' . $redirectData['transactionDate'] . '">
        <input type="hidden" name="descp" value="' . $redirectData['descp'] . '">
        <input type="hidden" name="miscFee" value="' . $redirectData['miscFee'] . '">
        <input type="hidden" name="signature" value="' . $redirectData['signature'] . '">
        <input type="submit" style="visibility:hidden;">
        </form>
       <script type="text/javascript">document.redirect.submit();</script>';
    }
    
    function sms($url, $data = array()) {
        $curl = curl_init();

        $end_point = $url . '?' . http_build_query($data, '', '&');

        curl_setopt($curl, CURLOPT_URL, $end_point);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    private function get($path, $data = array()) {
        $curl = curl_init();

        $headers = array();
        $headers[] = 'apikey: ' . $this->secret_api_key;
        $headers[] = 'Content-Type: application/json';

        $end_point = $this->server_domain . $path . '?' . http_build_query($data, '', '&');

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_USERPWD, $this->secret_api_key . ":");
        curl_setopt($curl, CURLOPT_URL, $end_point);
        curl_setopt($curl, CURLOPT_POST, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        curl_close($curl);

        $responseObject = json_decode($response, true);
        return $responseObject;
    }

    private function post($url, $data = array()) {
        $curl = curl_init();

        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';

        $payload = json_encode($data);

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);
        curl_close($curl);

        $responseObject = json_decode($response, true);
        return $responseObject;
    }

}
