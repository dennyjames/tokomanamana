<?php

$config['page_query_string'] = TRUE;
$config['reuse_query_string'] = TRUE;
$config['use_page_numbers'] = TRUE;
$config['query_string_segment'] = 'page';
$config['full_tag_open'] = '<ul class="pagination">';
$config['full_tag_close'] = '</ul>';
$config['prev_link'] = 'Prev';
$config['prev_tag_open'] = '<li class="prev">';
$config['prev_tag_close'] = '</li>';
$config['next_link'] = 'Next';
$config['next_tag_open'] = '<li class="next">';
$config['next_tag_close'] = '</li>';
$config['num_tag_open'] = '<li class="">';
$config['num_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class="active"><a href="#">';
$config['cur_tag_close'] = '</a></li>';
$config['first_link'] = FALSE;
$config['last_link'] = FALSE;
