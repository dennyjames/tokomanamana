<style>
.wrapper {
  position: relative;
  width: 350px;
  height: 200px;
  -moz-user-select: none;
  -webkit-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.signature-pad {
  position: absolute;
  left: 0;
  top: 0;
  width:350px;
  height:200px;
  background-color: #ccc;
}
.btn-clear-canvas {
  position: relative;
  width:350px;
  height:200px;
  text-align:right;  
}
@media screen and (max-width: 480px) {
    .wrapper {
        width: 230px;
        height: 150px;
    }
    .signature-pad {
        width: 230px;
        height: 150px;
    }
    .btn-clear-canvas {
        width: 230px;
        height: 150px;
    }
}
.btn-is-disabled {
  pointer-events: none;
}
</style>
<div class="content">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <h3><?php echo lang('register_heading'); ?></h3>
            <div class="panel panel-body">
                <form class="stepy form-horizontal " action="<?php echo current_url(); ?>" method="post" id="register">
                    <fieldset>
                        <legend class="text-semibold"><?php echo lang('account'); ?></legend>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('email'); ?></label>
                            <div class="col-md-9">
                                <input type="text" id="email" class="form-control" required name="email" placeholder="<?php echo lang('email_placeholder'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('password'); ?></label>
                            <div class="col-md-9">
                                <input id="password" type="password" class="form-control" required name="password" placeholder="<?php echo lang('password_placeholder'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('confirm_password'); ?></label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" required name="confirm_password" placeholder="<?php echo lang('confirm_password_placeholder'); ?>">
                            </div>
                        </div>
                        <hr>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('owner_name'); ?></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="owner_name" placeholder="<?php echo lang('owner_name_placeholder'); ?>" onchange="changeName(this.value)">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('owner_phone'); ?></label>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-8">
                                        <input id="owner_phone" type="text" class="form-control" required name="owner_phone" placeholder="<?php echo lang('owner_phone_placeholder'); ?>">
                                    </div>
                                    <div class="col-md-4">
                                        <a id="verif_icon" data-toggle="modal" data-target="#requestOTPModal" class="btn btn-danger btn-sm openOTPModal">Verifikasi <i class="icon-cross position-right"></i></a>
                                        <a id="verif_icon_ok" style="display:none;" class="btn btn-success btn-sm">Verifikasi <i class="icon-check position-right"></i></a>
                                        <input id="verified_phone" type="text" class="form-control" required name="verified_phone" value="" style="display:none;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('owner_birthday'); ?></label>
                            <div class="col-md-9">
                                <input type="text" data-provide="datepicker" class="form-control" required readonly="" name="owner_birthday" placeholder="Masukkan tanggal lahir anda">
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('owner_id'); ?></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" required name="owner_id" placeholder="<?php echo lang('owner_id_placeholder'); ?>">
                                </div>
                            </div>
                    </fieldset>
                    <fieldset>
                        <legend class="text-semibold"><?php echo lang('store'); ?></legend>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('store_name'); ?></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="store_name" placeholder="<?php echo lang('store_name_placeholder'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('store_telephone'); ?></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="store_telephone" placeholder="<?php echo lang('store_telephone_placeholder'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('store_address'); ?></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="store_address" placeholder="<?php echo lang('store_address_placeholder'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('store_province'); ?></label>
                            <div class="col-md-9">
                                <select class="select" required="" name="store_province" id="province" data-placeholder="<?php echo lang('store_province_placeholder'); ?>" data-width="100%">
                                    <option></option>
                                    <?php if ($provincies) { ?>
                                        <?php foreach ($provincies->result() as $province) { ?>
                                            <option value="<?php echo $province->id; ?>"><?php echo $province->name; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('store_city'); ?></label>
                            <div class="col-md-9">
                                <select class="select" required="" name="store_city" id="city" data-placeholder="<?php echo lang('store_city_placeholder'); ?>" data-width="100%">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('store_district'); ?></label>
                            <div class="col-md-9">
                                <select class="select" required="" name="store_district" id="district" data-placeholder="<?php echo lang('store_district_placeholder'); ?>" data-width="100%">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('store_postcode'); ?></label>
                            <div class="col-md-5">
                                <input type="text" class="form-control" required name="store_postcode" placeholder="<?php echo lang('store_postcode_placeholder'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-3 control-label">Ekspedisi</label>
                        <div class="col-md-9">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="shipping[]" class="styled" value="jne">
                                JNE
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="shipping[]" class="styled" value="tiki">
                                TIKI
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="shipping[]" class="styled" value="pos">
                                POS Indonesia
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                            <label class="col-md-3 control-label">Lokasi</label>
                            <div class="col-md-9">
                                <script type="text/javascript">
                                    var centreGot = false;
                                </script>
                                <?php echo $map['js']; ?>
                                <input type="hidden" name="lat" id="lat" value="">
                                <input type="hidden" name="lng" id="lng" value="">
                                <input class="form-control" id="search-location">
                                <?php echo $map['html']; ?>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend class="text-semibold"><?php echo lang('bank'); ?></legend>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('bank_name'); ?></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="bank_name" placeholder="<?php echo lang('bank_name_placeholder'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('bank_branch'); ?></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="bank_branch" placeholder="<?php echo lang('bank_branch_placeholder'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('bank_account_number'); ?></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" required name="bank_account_number" placeholder="<?php echo lang('bank_account_number_placeholder'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('bank_account_name'); ?></label>
                            <div class="col-md-9">
                                <input type="text" id="bank_account_name" readonly="readonly" class="form-control" required name="bank_account_name" placeholder="<?php echo lang('bank_account_name_placeholder'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Tahu Tokomana-mana Dari</label>
                            <div class="col-md-9">
                                <select name="know_from" class="form-control" onchange="changeKnowFrom()">
                                    <option value="google">Google</option>
                                    <option value="medsos">Medsos</option>
                                    <option value="sales">Sales</option>
                                </select>
                            </div>
                        </div>
                        <div id="sales_name" class="form-group" style="display:none;">
                            <label class="col-md-3 control-label">Nama Sales</label>
                            <div class="col-md-9">
                            <input type="text" class="form-control" name="sales_name" placeholder="" maxlength="20">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Signature</label>
                            <div class="col-md-9">
                                <div class="wrapper">
                                    <canvas id="signature-pad" class="signature-pad"></canvas>
                                    <input type="hidden" id="base64_img" name="base64_img">
                                </div>
                                <div class="btn-clear-canvas">
                                    <a href="javascript:void(0)" class="btn btn-default" id="clear-canvas">Clear</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <a href="#terms" data-toggle="modal" data-target="#terms">
                                        <input id="agreeSnk" type="checkbox" name="agree" required="">
                                    <?php echo lang('terms_prefix'); ?> <?php echo lang('terms'); ?></a>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div id="captcha">
                                <?php
                                echo $widget;
                                echo $script;
                                ?>
                            </div>
                        </div>
                    </fieldset>
                    <button type="submit" id="submit" class="btn btn-primary stepy-finish">Simpan <i class="icon-floppy-disk position-right"></i></button>
                </form>
                <div>Sudah punya akun?? <a href="<?php echo site_url('login');?>">Login disini!</a></div>
            </div>
        </div>
    </div>

</div>
<div id="terms" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo lang('terms'); ?></h5>
            </div>
            <div class="modal-body">
                <?php echo settings('term_merchant_register'); ?>
                <div class="row">
                    <div class="pull-right">
                        <button type="submit" onclick="agreeSnk()" class="btn btn-success">Setuju <i class="icon-check position-right"></i></button>
                        <button type="submit" onclick="closeSnKmodal()" class="btn btn-danger">Tidak Setuju <i class="icon-cross position-right"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="requestOTPModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" 
                    data-dismiss="modal">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        Request OTP
                    </h4>
                </div>
                
                <!-- Modal Body -->
                <div class="modal-body">
                    
                    <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label  class="col-sm-3 control-label"
                                for="no-hp">No HP</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="inputNoHP" value="" disabled=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"
                            for="inputPassword3" >Kode Konfirmasi</label>
                        <div class="col-sm-6">
                            <input type="text" id="otpCode" class="form-control" placeholder="your otp code"/>
                        </div>
                        <div class="col-sm-3">
                        <a id="btn-token" href="javascript:void(0)" class="btn btn-default" onclick="requestOTPButton()">Request OTP</a>
                        </div>
                    </div>
                    </form>
                </div>
                
                <!-- Modal Footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">
                                Close
                    </button>
                    <a href="javascript:void(0)" class="btn btn-primary" onclick="submitOTP()">
                        Konfirmasi
                    </a>
                </div>
            </div>
        </div>
    </div>