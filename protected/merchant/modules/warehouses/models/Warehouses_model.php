<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouses_model extends CI_Model {

    function get($id) {
        $this->db->select('wr.*,m.name as merchant_name,m.warehouse_price as price,m.length,m.width,m.height,(m.length * m.height * m.width) as total_size,p.name as principle_name')
        ->join('merchants m', 'm.id = wr.warehouse_id', 'left')
        ->join('principles p', 'p.id = wr.principle_id', 'left')
        ->where('m.id',$this->data['user']->merchant)
        ->where('wr.id', $id);
        $query = $this->db->get('warehouse_rent wr');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_items($id) {
        $this->db->select('wp.*,p.name,p.width,p.length,p.height')
        ->join('products p', 'p.id = wp.product_id', 'left')
        ->join('warehouse_rent wr', 'wr.id = wp.rent_id', 'left')
        ->where('wr.id', $id);
        return $this->db->get('warehouse_product wp');
    }

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('p.name,m.warehouse_price as price,wr.*')
        ->where('m.id',$this->data['user']->merchant)
        ->join('merchants m', 'm.id = wr.warehouse_id', 'left')
        ->join('principles p', 'p.id = wr.principle_id', 'left')
        ->limit($length, $start); 

        return $this->db->get('warehouse_rent wr');
    }

    function get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'm.name';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->where_like($search);
        $this->db->where('m.id',$this->data['user']->merchant);
        $this->db->join('merchants m', 'm.id = wr.warehouse_id', 'left')
        ->join('principles p', 'p.id = wr.principle_id', 'left');
        return $this->db->count_all_results('warehouse_rent wr');
    }

    function where_like($search = '') {
        $columns = array('m.name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

    // Get Product Warehouse table
    function get_items_all($id,$start = 0, $length, $search = '', $order = array()) {
        $this->where_like_product($search);
        if ($order) {
            $order['column'] = $this->get_product_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('wp.*,p.name,p.width,p.length,p.height')
                ->join('products p', 'p.id = wp.product_id', 'left')
                ->join('warehouse_rent wr', 'wr.id = wp.rent_id', 'left')
                ->where('wr.id', $id)
                ->limit($length, $start);
        return $this->db->get('warehouse_product wp');
    }

    function count_all_dimension($id){
        $this->db->select('SUM((p.width/100)*(p.length/100)*(p.height/100)) as total')
                ->join('products p', 'p.id = wp.product_id', 'left')
                ->join('warehouse_rent wr', 'wr.id = wp.rent_id', 'left')
                ->where('wr.id', $id);
        return $this->db->get('warehouse_product wp');
    }

    function count_usage_dimension($id){
        $this->db->select('SUM((p.width/100)*(p.length/100)*(p.height/100)) as total')
                ->join('products p', 'p.id = wp.product_id', 'left')
                ->join('warehouse_rent wr', 'wr.id = wp.rent_id', 'left')
                ->where('wr.status', 1)
                ->where('wr.warehouse_id', $id);
        return $this->db->get('warehouse_product wp');
    }

    function get_product_alias_key($key) {
        switch ($key) {
            case 0: $key = 'p.name';
                break;
        }
        return $key;
    }

    function count_all_product($id,$search = '') {
        $this->where_like_product($search);
        $this->db->join('products p', 'p.id = wp.product_id', 'left')
                ->join('warehouse_rent wr', 'wr.id = wp.rent_id', 'left')
                ->where('wr.id', $id);
        return $this->db->count_all_results('warehouse_product wp');
    }

    function where_like_product($search = '') {
        $columns = array('p.name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }
    
    // Get Product Request Warehouse table
     function get_items_request_all($id,$start = 0, $length, $search = '', $order = array()) {
        $this->where_like_product($search);
        if ($order) {
            $order['column'] = $this->get_product_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('wph.*,p.name,p.width,p.length,p.height')
                ->join('products p', 'p.id = wph.product_id', 'left')
                ->join('warehouse_rent wr', 'wr.id = wph.rent_id', 'left')
                ->where('wr.id', $id)
                ->where('wph.status', 0)
                ->where('wph.type', 'in')
                ->limit($length, $start);
        return $this->db->get('warehouse_product_history wph');
    }

    function get_product_request_alias_key($key) {
        switch ($key) {
            case 0: $key = 'p.name';
                break;
        }
        return $key;
    }

    function count_all_request_product($id,$search = '') {
        $this->where_like_product($search);
        $this->db->join('products p', 'p.id = wph.product_id', 'left')
                ->join('warehouse_rent wr', 'wr.id = wph.rent_id', 'left')
                ->where('wr.id', $id);
        return $this->db->count_all_results('warehouse_product_history wph');
    }

    function where_like_request_product($search = '') {
        $columns = array('p.name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

    function count_all_request_dimension($id){
        $this->db->select('SUM((p.width/100)*(p.length/100)*(p.height/100)) as total')
                ->join('products p', 'p.id = wph.product_id', 'left')
                ->join('warehouse_rent wr', 'wr.id = wph.rent_id', 'left')
                ->where('wph.status', 0)
                ->where('wph.type', 'in')
                ->where('wr.id', $id);
        return $this->db->get('warehouse_product_history wph');
    }

    function get_product_req_sum($id) {
        $this->db->select('wph.*,sum(qty) as total_qty')
                ->where('wph.rent_id', $id)
                ->where('wph.status', 'on')
                ->where('wph.type', 'in')
                ->group_by('wph.rent_id,wph.product_id');
        return $this->db->get('warehouse_product_history wph');
    }
    function check_request_product_id($id,$rent_id) {
        $this->db->where('wp.product_id', $id)
                ->where('wp.rent_id', $rent_id);
        return $this->db->get('warehouse_product wp');
    }
}
