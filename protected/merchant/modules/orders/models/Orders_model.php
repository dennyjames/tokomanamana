<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Orders_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('oi.*, o.shipping_name, os.name_for_merchant order_status_name')
                ->join('orders o', 'oi.order = o.id', 'left')
                ->join('setting_order_status os', 'oi.order_status = os.id', 'left')
                ->where_not_in('oi.order_status', "SELECT `value` FROM settings WHERE `key` IN ('order_new_status','order_payment_confirmed_status')", false)
                ->where('merchant', $this->data['user']->merchant)
                ->limit($length, $start);

        return $this->db->get('order_invoice oi');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'oi.code';
                break;
            case 1: $key = 'oi.date_added';
                break;
            case 2: $key = 'o.shipping_name';
                break;
            case 3: $key = 'oi.total';
                break;
            case 4: $key = 'o.shipping_courier';
                break;
            case 5: $key = 'os.name_for_merchant';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->_where_like($search);
        $this->db->join('orders o', 'oi.order = o.id', 'left')
                ->join('setting_order_status os', 'oi.order_status = os.id', 'left')
                ->where_not_in('oi.order_status', "SELECT `value` FROM settings WHERE `key` IN ('order_new_status','order_payment_confirmed_status')", false)
                ->where('merchant', $this->data['user']->merchant);
        return $this->db->count_all_results('order_invoice oi');
    }

    private function _where_like($search = '') {
        $columns = array('oi.code', 'o.shipping_name', 'oi.date_added', 'oi.total', 'o.shipping_courier', 'os.name');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    function get($id) {
        $this->db->select('oi.*, o.id AS order_id, o.shipping_name, o.shipping_address, o.payment_method,o.unique_code,o.total_plus_kode,o.total, p.name shipping_province, c.name shipping_city, d.name shipping_district, o.shipping_postcode, o.shipping_phone,cst.fullname customer_name, os.name_for_merchant order_status_name, o.customer, cst.email customer_email, cst.phone customer_phone,cst.verification_phone customer_verif')
                ->join('orders o', 'oi.order = o.id', 'left')
                ->join('setting_order_status os', 'oi.order_status = os.id', 'left')
                ->join('customers cst','cst.id = o.customer','left')
                ->join('provincies p', 'p.id = o.shipping_province', 'left')
                ->join('cities c', 'c.id = o.shipping_city', 'left')
                ->join('districts d', 'd.id = o.shipping_district', 'left')
                ->where('oi.id', $id)
                ->where('oi.merchant', $this->data['user']->merchant);
        $query = $this->db->get('order_invoice oi');
        //var_dump($this->db->last_query());exit();
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_products($id) {
        $this->db->select('op.*, pi.image')
        ->join('product_image pi', 'pi.product = op.product AND pi.primary = 1', 'left')
        ->join('products p', 'p.id = op.product', 'left')
                ->where('op.invoice', $id);

        return $this->db->get('order_product op');
    }
    
    function get_histories($id){
        $this->db->select('oh.date_added date, os.name_for_merchant status')
                ->join('setting_order_status os','oh.order_status = os.id AND name_for_merchant != "" AND name_for_merchant IS NOT NULL','right')
                ->where('oh.invoice',$id)
                ->order_by('oh.date_added DESC');
        return $this->db->get('order_history oh');
    }
    
    function merchant($id) {
        $this->db->select('m.*, p.name province_name, c.name city_name, d.name district_name')
                ->join('provincies p', 'p.id = m.province', 'left')
                ->join('cities c', 'c.id = m.city', 'left')
                ->join('districts d', 'd.id = m.district', 'left')
                ->where('m.id', $id);
        $query = $this->db->get('merchants m');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

}
