<!DOCTYPE html>
<html lang="id">
    <head>
        <title>Invoice</title>
    </head>
    <body style="font-family: open sans, tahoma, sans-serif; margin: 0; -webkit-print-color-adjust: exact">
        <div style="width: 600px;">
            <table width="600" cellspacing="0" cellpadding="0" class="container" style="width: 600px; padding: 20px;">
                <tr>
                    <td>
                        <table width="100%" cellspacing="0" cellpadding="0" style="width: 100%; padding-bottom: 20px;">
                            <tbody>
                                <tr>
                                    <td style="padding-right: 10px;">
                                        <table style="width: 100%; border-collapse: collapse;" width="100%" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td>
                                                    <div style="text-align:right;width:100%;">
                                                        <a style="color: #97C23C; font-size: 14px; text-decoration: none;" href="javascript:window.print()">
                                                            <span style="vertical-align: middle">Cetak</span>
                                                        </a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr style="font-size: 13px;">
                                                <td>
                                                    <table style="width: 100%; border-collapse: collapse;" width="100%" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="font-size:18px;font-weight:bold;"> Tokomanamana</td>
                                                                <td style="text-align:right;padding-left:150px;font-size:18px;font-weight:bold;">
                                                                    Label Pengiriman
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="100%" cellspacing="0" cellpadding="0" style="width: 100%; padding: 10px 0px;border-top: 1px dashed #DDD;">
                            <tr>
                                <td style="width:25%;font-size:12px;">
                                    <div></div>
                                    <div></div>
                                </td>
                                <td style='width:25%;font-size:12px;'>
                                    <?php 
                                    if($order->shipping_courier == 'pickup') {
                                        $shipping_courier = '-';
                                        $shipping_courier_type = 'Ambil Sendiri';
                                    } else {
                                        $exp_shipping = explode('-',$order->shipping_courier);
                                        $shipping_courier = $exp_shipping[0];
                                        $shipping_courier_type = $exp_shipping[1];
                                    }
                                    ?>
                                    <div style="font-weight:bold;text-transform:uppercase;"><?php echo $shipping_courier;?></div>
                                    <div><?php echo $shipping_courier_type;?></div>
                                </td>
                                <td style='width:50%;font-size:12px;'>
                                    <div>Nomor invoice</div>
                                    <div><?php echo $order->code;?></div>
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="0" cellpadding="0" style="width: 100%; padding: 10px 0px;">
                            <tr>
                                <td style="width:25%;font-size:12px;">
                                    <div>Administrasi</div>
                                    <div>+Rp. 0</div>
                                </td>
                                <td style='width:25%;font-size:12px;'>
                                    <div style="font-weight:bold;">Asuransi</div>
                                    <div>+Rp. 0</div>
                                </td>
                                <td style='width:25%;font-size:12px;'>
                                    <div style="font-weight:bold;">Ongkir</div>
                                    <div><?php echo rupiah($order->shipping_cost);?></div>
                                </td>
                                <td style='width:25%;font-size:12px;'>
                                    <div style="font-weight:bold;">Berat</div>
                                    <div><?php echo number_format((float)($order->shipping_weight/1000), 2, '.', '');;?> Kg</div>
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="0" cellpadding="0" style="width: 100%; padding: 10px 0px;">
                            <tr>
                                <td style="width:25%;font-size:12px;">
                                    <div>Kepada</div>
                                </td>
                                <td style='width:75%;font-size:12px;'>
                                    <div>
                                        <span style="font-weight:bold;">
                                            <?php echo $order->shipping_name;?>
                                        </span>
                                        -  <?php echo $order->shipping_phone;?>
                                    </div>
                                    <div>
                                    <?php echo $order->shipping_address;?>
                                    </div>
                                    <div><?php echo $order->shipping_district.', '.$order->shipping_city.', '.$order->shipping_province;?></div>
                                </td>
                            </tr>
                        </table>
                        <table width="100%" cellspacing="0" cellpadding="0" style="width: 100%; padding: 10px 0px;">
                            <tr>
                                <td style="width:25%;font-size:12px;">
                                    <div>Dari</div>
                                </td>
                                <td style='width:75%;font-size:12px;'>
                                <div>
                                        <span style="font-weight:bold;">
                                            <?php echo $merchant->name;?>
                                        </span>
                                        -  <?php echo $merchant->telephone;?>
                                    </div>
                                    <div>
                                    <?php echo $merchant->address;?>
                                    </div>
                                    <div><?php echo $merchant->district_name.', '.$merchant->city_name.', '.$merchant->province_name;?></div>
                                </td>
                            </tr>
                        </table>
                        <div style="margin-bottom:10px;font-weight:bold;font-size:14px;">Detail produk</div>
                        <table width="100%" cellspacing="0" cellpadding="0" style="border-top: 1px dashed #DDD;border-bottom: 1px dashed #DDD;width: 100%; padding: 10px 0px;">
                            <?php foreach ($products->result() as $product) { ?>
                            <?php if($product->options) : ?>
                                <?php $products_option = json_decode($product->options) ?>
                                <?php if(count($products_option) > 1 || !isset($products_option[0]->type)) : ?>
                                    <?php foreach($products_option as $product_option) : ?>
                                        <tr style="padding-top:10px;">
                                            <td style="width:25%;font-size:12px;">
                                                <div><?php echo $product_option->quantity;?> buah</div>
                                            </td>
                                            <td style='width:75%;font-size:12px;'>
                                                <div>
                                                    <?php echo $product->name;?>
                                                    <?php $options = json_decode($product_option->data_option) ?>
                                                    <?php foreach($options as $option) : ?>
                                                        (<?php echo $option->type . ' ' . $option->value ?>)
                                                    <?php endforeach; ?>     
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php else : ?>
                                    <tr style="padding-top:10px;">
                                        <td style="width:25%;font-size:12px;">
                                            <div><?php echo $product->quantity;?> buah</div>
                                        </td>
                                        <td style='width:75%;font-size:12px;'>
                                            <div>
                                                <?php echo $product->name;?>
                                                <?php foreach($products_option as $option) : ?>
                                                    (<?php echo $option->type . ' ' . $option->value ?>)
                                                <?php endforeach; ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endif; ?>
                            <?php else : ?>
                                <tr style="padding-top:10px;">
                                    <td style="width:25%;font-size:12px;">
                                        <div><?php echo $product->quantity;?> buah</div>
                                    </td>
                                    <td style='width:75%;font-size:12px;'>
                                        <div><?php echo $product->name;?></div>
                                    </td>
                                </tr>
                            <?php endif; ?>
                            <?php } ?>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </body>

</html>
