<div class="content-wrapper" style="background-color: #e0e0e0">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2>Tukar Poin</h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="container" style="padding-left: 24px; padding-right: 24px; padding-top: 18px">
                <div class="row">
                    <div class="col-xs-12 thumbnail" style="padding: 8px">
                        <p style="color: grey; margin-top: 8px; margin-bottom: -16px">Poin Saya</>
<?php
                            $point = 0;
                            $q_point = $m->get_data('', 'point', null, array('id_merchant' => $this->session->userdata('user_id')));

                            if($q_point->num_rows() > 0){
                                $point = $q_point->row()->point;
                            }
?>
                        <h3 style="margin-left: 16px"><?=number_format($point)?></h3>
                        <p style="color: grey; margin-bottom: -16px">Periode Tukar Poin</p>
<?php
                            $periode = 'Belum Ada';
                            $s_periode = 'color: red';
                            $q_periode = $m->get_data('', 'settings', null, array('key' => 'periode_tukar_point'));

                            if($q_periode->num_rows() > 0){
                                $e = explode(';', $q_periode->row()->value);
                                if($e[6] == '1'){
                                    $p0 = date('Y/m/d', strtotime($e[0].'/'.$e[1].'/'.$e[2]));
                                    $p1 = date('Y/m/d', strtotime($e[3].'/'.$e[4].'/'.$e[5]));
                                    $periode = $e[2].'/'.$e[1].'/'.$e[0].' Sampai '.$e[5].'/'.$e[4].'/'.$e[3];

                                    if($p0 <= date('Y/m/d') && $p1 >= date('Y/m/d')){
                                        $s_periode = 'color: green';
                                    }
                                }
                            }
?>
                        <h3 style="margin-left: 14px; <?=$s_periode?>"><?=$periode?></h3>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

    <div class="content" style="margin-top:-62px; display: none" id="content_detail_reward">
        <div class="panel panel-flat">
            <div class="container">
                <div class="row" id="data_detail">
                </div>
            </div>
        </div>
    </div>

    <div class="content" style="margin-top:-62px">
        <div class="panel panel-flat">
            <div class="container">
                <div class="row">
<?php
        $q_reward = $m->get_data('', 'reward', null, array('is_active' => 1), '', '', 0, 0, array(0 => "customer_merchant='g' or customer_merchant='m'"));

        foreach ($q_reward->result() as $key) {
            $param = true;
            if($q_periode->num_rows() > 0){
              $q_status_periode = $m->get_data('', 'reward_claim_temp', null, array('status_periode_aktif' => 1, 'id_reward' => $key->id), '', '', 0, 0, array(0 => "id_merchant LIKE '%,".$this->session->userdata('user_id').",%'"));

              if($q_status_periode->num_rows() > 0){
                $param = false;
              }
            }

            if($param){
                $desc = $key->deskripsi;
                if(strlen($desc) > 35){
                    $desc = substr($desc, 0, 35).'...';
                }
                $gambar = 'https://tokomanamana.com/files/images/logo.png';
                if($key->gambar != '' && $key->gambar != null){
                    $gambar = $key->gambar;
                }
?>
                    <div class="col-xs-12 col-md-4 col-lg-3" style="height: 500px; ">
                        <div class="content" style="height: 500px; ">
                            <div class="panel panel-flat">
                                <div class="row">
                                    <div class="col-xs-12" style="">
                                        <img src="<?=$gambar?>" class="" style="margin-top: 8px; height: 176px; width: 100%; max-height: 176px; object-fit: cover;" >
                                        <hr>
                                        <center><button class="btn btn-flat" type="button" onclick="detail_reward(<?=$key->id?>)"><p style="font-size: 18px; text-align: center;"><?=$key->nama?></p></button>
                                            <hr style="margin-top: -18px">
                                        </center>
                                        <p style="padding: 12px; margin-top: -12px">
                                            <?=$desc?>
                                        </p>
                                        <p style="padding-left: 12px; padding-left: 12px; color: grey;">Minimal Poin</p>
                                        <p style="padding-left: 12px; padding-left: 12px; font-size: 14px; margin-top: -12px; "><?=$key->point?></p>
                                        <p style="padding-left: 12px; padding-left: 12px; color: grey;">QTY</p>
                                        <p style="padding-left: 12px; padding-left: 12px; font-size: 14px; margin-top: -12px; "><?=$key->qty?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<?php
            }
        }
?>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function detail_reward(val) {
        $.ajax({
            type: 'POST',
            url: '<?=base_url('orders/reward/get_detail_reward')?>',
            data: {id_reward: val, is_history: 0},
            dataType: "json",
            success: function (data) {
                $('#data_detail').html(data.data);
                $('#content_detail_reward').show();
                $('html, body').animate({
                    scrollTop: $("#content_detail_reward").offset().top
                }, 500);
            }
        });
        
    }

    function tukar_point(id_reward) {
        $.ajax({
            type: 'POST',
            url: '<?=base_url('orders/reward/tukar_point')?>',
            data: {id_reward: id_reward},
            dataType: "json",
            success: function (data) {
                if(data.success == 1){
                    window.location.reload(true);
                }else{
                    swal({
                        title: "Ooops",
                        text: data.msg,
                        type: "error",
                        html: true
                    }, function () {
                    });
                }
                
            }
        });
        
    }
</script>