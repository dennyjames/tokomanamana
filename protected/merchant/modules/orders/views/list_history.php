<div class="content-wrapper" style="background-color: #e0e0e0">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2>History Tukar Poin</h2>
            </div>
        </div>
    </div>

    <div class="content" style="margin-top: 0px; display: none" id="content_detail_reward">
        <div class="panel panel-flat">
            <div class="container">
                <div class="row" id="data_detail">
                </div>
            </div>
        </div>
    </div>

    <div class="content" style="margin-top: 0px">
        <div class="panel panel-flat">
            <div class="container">
                <div class="row">
<?php
        $join0 = array(
            0 => 'reward r-r.id=peh.id_reward'
        );

        $where0 = array(
            'id_merchant' => $this->session->userdata('user_id')
        );
        $q_history = $m->get_data('r.*, peh.id as peh_id', 'point_exchange_history peh', $join0, $where0, '', 'last_update desc');
        
        foreach ($q_history->result() as $key) {
            $param = true;

            if($param){
                $gambar = 'https://tokomanamana.com/files/images/logo.png';
                if($key->gambar != '' && $key->gambar != null){
                    $gambar = $key->gambar;
                }
?>
                    <div class="col-xs-12 col-md-4 col-lg-3" style="height: 420px; ">
                        <div class="content" style="height: 420px; ">
                            <div class="panel panel-flat">
                                <div class="row">
                                    <div class="col-xs-12" style="">
                                        <img src="<?=$gambar?>" class="" style="margin-top: 8px; height: 176px; width: 100%; max-height: 176px; object-fit: cover;" >
                                        <hr>
                                        <center><a href="<?=base_url('orders/reward/history/'.$key->peh_id)?>" class="btn btn-flat" ><p style="font-size: 18px; text-align: center;"><?=$key->nama?></p></a></center>
                                        <p style="padding-left: 12px; padding-left: 12px; color: grey;">Minimal Poin</p>
                                        <p style="padding-left: 12px; padding-left: 12px; font-size: 14px; margin-top: -12px; "><?=$key->point?></p>
                                        <p style="padding-left: 12px; padding-left: 12px; color: grey;">QTY</p>
                                        <p style="padding-left: 12px; padding-left: 12px; font-size: 14px; margin-top: -12px; "><?=$key->qty?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<?php
            }
        }
?>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function detail_reward(val) {
        $.ajax({
            type: 'POST',
            url: '<?=base_url('orders/reward/get_detail_reward')?>',
            data: {id_reward: val, is_history: 1},
            dataType: "json",
            success: function (data) {
                $('#data_detail').html(data.data);
                $('#content_detail_reward').show();
                $('html, body').animate({
                    scrollTop: $("#content_detail_reward").offset().top
                }, 500);
            }
        });
        
    }

    function tukar_point(id_reward) {
        $.ajax({
            type: 'POST',
            url: '<?=base_url('orders/reward/tukar_point')?>',
            data: {id_reward: id_reward},
            dataType: "json",
            success: function (data) {
                if(data.success == 1){
                    window.location.reload(true);
                }else{
                    swal({
                        title: "Ooops",
                        text: data.msg,
                        type: "error",
                        html: true
                    }, function () {
                    });
                }
                
            }
        });
        
    }
</script>