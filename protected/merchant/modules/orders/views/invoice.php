<!DOCTYPE html>
<html lang="id">
    <head>
        <title>Invoice</title>
    </head>
    <body style="font-family: open sans, tahoma, sans-serif; margin: 0; -webkit-print-color-adjust: exact">
        <div style="width: 790px;">
            <table width="790" cellspacing="0" cellpadding="0" class="container" style="width: 790px; padding: 20px;">
                <tr>
                    <td>
                        <table width="100%" cellspacing="0" cellpadding="0" style="width: 100%; padding-bottom: 20px;">
                            <tbody>
                                <tr style="margin-top: 8px; margin-bottom: 8px;">
                                    <td>
                                        <img src="<?php echo base_url('../assets/frontend/images/logo.png'); ?>" alt="<?php echo settings('store_name'); ?>">
                                    </td>
                                    <td style="text-align: right; padding-right: 15px;">
                                        <a style="color: #97C23C; font-size: 14px; text-decoration: none;" href="javascript:window.print()">
                                            <span style="vertical-align: middle">Cetak</span>
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="100%" cellspacing="0" cellpadding="0" style="width: 100%; padding-bottom: 20px;">
                            <tbody>
                                <tr style="font-size: 20px; font-weight: 600;">
                                    <td style="padding-bottom: 5px;">
                                        <span>Invoice</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-right: 10px;">
                                        <table style="width: 100%; border-collapse: collapse;" width="100%" cellspacing="0" cellpadding="0">
                                            <tr style="font-size: 13px;">
                                                <td>
                                                    <table style="width: 100%; border-collapse: collapse;" width="100%" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="width: 80px; font-weight: 600; padding: 3px 20px 3px 0;" width="80">Nomor</td>
                                                                <td style="padding: 3px 0;"><?php echo $order->code; ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="font-size: 13px;">
                                                <td>
                                                    <table style="width: 100%; border-collapse: collapse;" width="100%" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td style="width: 80px; font-weight: 600; padding: 3px 20px 3px 0;" width="80">Tanggal</td>
                                                                <td style="padding: 3px 0;"><?php echo get_date($order->date_added); ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>

                                        </table>
                                    </td>
                                    <td style="padding-left: 10px;vertical-align: text-top;">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table style="width: 100%; text-align: center; border-top: 1px solid rgba(0,0,0,0.1); border-bottom: 1px solid rgba(0,0,0,0.1); padding: 15px 0;" width="100%" cellspacing="0" cellpadding="0">
                            <thead style="font-size: 14px;">
                                <tr><th style="font-weight: 600; text-align: left; padding: 0 5px 15px 15px;">Nama Produk</th>
                                    <th style="width: 120px; font-weight: 600; padding: 0 5px 15px;" width="120">Jumlah Barang</th>
                                    <th style="width: 65px; font-weight: 600; padding: 0 5px 15px;" width="65">Berat</th>
                                    <th style="width: 115px; font-weight: 600; padding: 0 5px 15px;" width="115">Harga Barang</th>
                                    <th style="width: 115px; font-weight: 600; text-align: right; padding: 0 30px 15px 5px;" width="115">Subtotal</th>
                                </tr></thead>
                            <tbody>
                                <?php foreach ($products->result() as $product) { ?>
                                    <?php if($product->options) : ?>
                                        <?php $products_option = json_decode($product->options) ?>
                                        <?php if(count($products_option) > 1 || !isset($products_option[0]->type)) : ?>
                                            <?php foreach($products_option as $product_option) : ?>
                                                <tr style="font-size: 13px;">
                                                    <td style="text-align: left; padding: 8px 5px 8px 15px;">
                                                        <span style="display: block; width: 270px;"> 
                                                            <?php echo $product->name; ?>
                                                            <?php $options = json_decode($product_option->data_option) ?>
                                                            <?php foreach($options as $option) : ?>
                                                                (<?php echo $option->type . ' ' . $option->value ?>)
                                                            <?php endforeach; ?>
                                                        </span>
                                                    </td>
                                                    <td style="width: 120px; padding: 8px 5px;" width="120"><?php echo $product_option->quantity; ?></td>
                                                    <td style="width: 65px; padding: 8px 5px;" width="65"><?php echo $product_option->total_weight; ?> gr</td>
                                                    <td style="width: 115px; padding: 8px 5px;" width="115"><?php echo rupiah($product_option->price); ?></td>
                                                    <td style="width: 115px; text-align: right; padding: 8px 30px 8px 5px;" width="115"><?php echo rupiah($product_option->price * $product_option->quantity); ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        <?php else : ?>
                                            <tr style="font-size: 13px;">
                                                <td style="text-align: left; padding: 8px 5px 8px 15px;">
                                                    <span style="display: block; width: 270px;"> 
                                                        <?php echo $product->name; ?>
                                                        <?php foreach($products_option as $option) : ?>
                                                            (<?php echo $option->type . ' ' . $option->value ?>)
                                                        <?php endforeach; ?>
                                                    </span>
                                                </td>
                                                <td style="width: 120px; padding: 8px 5px;" width="120"><?php echo $product->quantity; ?></td>
                                                <td style="width: 65px; padding: 8px 5px;" width="65"><?php echo $product->weight; ?> gr</td>
                                                <td style="width: 115px; padding: 8px 5px;" width="115"><?php echo rupiah($product->price); ?></td>
                                                <td style="width: 115px; text-align: right; padding: 8px 30px 8px 5px;" width="115"><?php echo rupiah($product->total); ?></td>
                                            </tr>
                                        <?php endif; ?>
                                    <?php else : ?>
                                        <tr style="font-size: 13px;">
                                            <td style="text-align: left; padding: 8px 5px 8px 15px;">
                                                <span style="display: block; width: 270px;"> <?php echo $product->name; ?> </span>
                                            </td>
                                            <td style="width: 120px; padding: 8px 5px;" width="120"><?php echo $product->quantity; ?></td>
                                            <td style="width: 65px; padding: 8px 5px;" width="65"><?php echo $product->weight; ?> gr</td>
                                            <td style="width: 115px; padding: 8px 5px;" width="115"><?php echo rupiah($product->price); ?></td>
                                            <td style="width: 115px; text-align: right; padding: 8px 30px 8px 5px;" width="115"><?php echo rupiah($product->total); ?></td>
                                        </tr>
                                    <?php endif; ?>
                                   
                                <?php } ?>
                                <tr style="font-size: 13px; background-color: rgba(0,0,0,0.1);" bgcolor="#F1F1F1">
                                    <td colspan="4" style="font-weight: 600; text-align: left; padding: 8px 5px 8px 15px;">Subtotal</td>
                                    <td style="width: 115px; font-weight: 600; text-align: right; padding: 8px 30px 8px 5px;" width="115"><?php echo rupiah($order->subtotal); ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <table style="width: 100%; text-align: center; padding: 15px 0;" width="100%" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr style="font-size: 13px;">
                                    <?php $shipping = explode('-', $order->shipping_courier); ?>
                                    <td style="font-weight: 600; text-align: left; padding: 8px 0 8px 15px;"><?php echo strtoupper($shipping[0]) . (isset($shipping[1]) ? ' ' . $shipping[1] : ''); ?></td>
                                    <td style="width: 120px; padding: 8px 5px;" width="120"></td>
                                    <td style="width: 65px; padding: 8px 5px;" width="65"><?php echo $order->shipping_weight; ?> gr</td>
                                    <td style="width: 115px; padding: 8px 5px;" width="115"></td>
                                    <td style="width: 115px; text-align: right; padding: 8px 30px 8px 5px;" width="115"><?php echo rupiah($order->shipping_cost); ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <table width="100%" cellspacing="0" cellpadding="0" style="width: 100%; padding: 0 0 20px;">
                            <tbody>
                                <tr>
                                    <td width="35%" valign="top" style="width: 35%; vertical-align: top; padding-right: 5px;"></td>
                                    <td width="65%" valign="top" style="width: 65%; vertical-align: top; padding-left: 5px;">
                                        <table width="100%" cellspacing="0" cellpadding="0" width="100%" style="width: 100%; border-collapse: collapse;">
                                            <tr bgcolor="#F1F1F1" style="font-size: 15px; color: #97C23C; background-color: rgba(0,0,0,0.1);">
                                                <td style="padding: 15px 0 15px 30px; font-weight: 600;">Total</td>
                                                <td style="padding: 15px 30px 15px 0; font-weight: 600; text-align: right; "><?php echo rupiah($order->total); ?></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="35%" valign="top" style="width: 35%; vertical-align: top; padding-right: 5px;"></td>
                                    <td width="65%" valign="top" style="width: 65%; vertical-align: top; padding-left: 5px;">
                                        <table width="100%" cellspacing="0" cellpadding="0" width="100%" style="width: 100%; border-collapse: collapse;">
                                        </table>

                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table width="100%" cellspacing="0" cellpadding="0" style="width: 100%; border-top: 1px dashed #DDD; padding: 25px 0px;">
                            <thead>
                            <th style="text-align: left; padding: 0;">
                                <h3 style="font-size: 15px; font-weight: 600; margin: 0;">Tujuan Pengiriman</h3>
                            </th>

                            </thead>
                            <tbody>
                                <tr>
                                    <td style="font-size: 13px; line-height: 20px; padding: 10px 0;">
                                        <p style="font-size: 14px; font-weight: 600; padding-bottom: 5px; margin: 0;"><?php echo $order->shipping_name; ?></p>
                                    <?php echo $order->shipping_address; ?> <br>
                                    <?php echo $order->shipping_district.', '.$order->shipping_city.', '.$order->shipping_postcode; ?> <br>
                                    <?php echo $order->shipping_province; ?> <br>
                                    <?php echo $order->shipping_phone; ?>
                                    </td>
                                    <td style="font-size: 13px; line-height: 20px; padding: 10px 0;" width="50%" valign="top">

                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </body>

</html>
