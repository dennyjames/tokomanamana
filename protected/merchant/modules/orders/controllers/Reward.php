<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Reward extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->lang->load('orders', settings('language'));
        $this->load->model('orders_model', 'orders');
        $this->data['menu'] = 'reward';
        $this->load->model('Model', 'm');
        $this->data['m'] = $this->m;
    }

    public function coba($pass){
        echo sha1($pass);
    }

    public function history($id_history=''){
        $this->template->_init();
        $this->template->table();
        $this->load->js('../assets/backend/js/modules/order_reward.js');
        $this->data['menu'] = 'history_reward';

        $this->breadcrumbs->push(lang('menu_order'), '/orders/reward');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->output->set_title('Riwayat Tukar Poin');
        if($id_history != ''){
            $this->data['id_history'] = $id_history;
            $this->load->view('list_history_detail', $this->data);
        }else{
            $this->load->view('list_history', $this->data);
        }
        
    }

    public function confirm_tukar_poin(){
        $id_reward = $this->input->post('id_reward');
        $m = $this->m;

        $q_reward_point = $m->get_data('prcd.id_reward, prcd.id, prcd.point, prcd.qty', 'point_reward_cart_detail prcd', array(0 => 'point_reward_cart prc-prc.id=prcd.id_cart'), array('prc.id_merchant' => $this->session->userdata('user_id')));

        if(!$m->cek_periode_tukar_point()){
            $m->delete_data1('point_reward_cart_detail', array('id' => $q_reward_point->row()->id));
            die(json_encode(array('success' => 0, 'msg' => 'Periode tukar poin sudah berakhir')));
        }

        if(!$m->cek_point_merchant($id_reward)){
            $m->delete_data1('point_reward_cart_detail', array('id' => $q_reward_point->row()->id));
            die(json_encode(array('success' => 0, 'msg' => 'Poin Anda tidak cukup')));
        }

        if(!$m->cek_stok_reward($id_reward)){
            $m->delete_data1('point_reward_cart_detail', array('id' => $q_reward_point->row()->id));
            die(json_encode(array('success' => 0, 'msg' => 'Reward tidak tersedia')));
        }

        $m->update_data('point_reward_cart_detail', array('status' => '2'), array('id' => $q_reward_point->row()->id));

        $m->update_data('point', array('point' => 0, 'last_update' => date('Y/m/d H:i:s')), array('id_merchant' => $this->session->userdata('user_id')));

        $reward = $m->get_data('', 'reward', null, array('id' => $q_reward_point->row()->id_reward));
        //kurangi qty reward
        $m->update_data('reward', array('qty' => ($reward->row()->qty - 1)), array('id' => $q_reward_point->row()->id_reward));

        $q_reward_claim = $m->get_data('', 'reward_claim_temp', null, array('id_reward' => $q_reward_point->row()->id_reward, 'status_periode_aktif' => 1));

        $q_setting = $m->get_data('', 'settings', null, array('key' => 'periode_tukar_point'))->row()->value;
        $exp = explode(';', $q_setting);
        $periode_tukar_point = $exp[0].';'.$exp[1].';'.$exp[2].';'.$exp[3].';'.$exp[4].';'.$exp[5];

        //insert/update reward_claim_temp
        if($q_reward_claim->num_rows() > 0){
            if($q_reward_claim->row()->id_merchant != ''){
                $add_['id_merchant'] = $q_reward_claim->row()->id_merchant.','.$this->session->userdata('user_id').',';
            }else{
                $add_['id_merchant'] = ','.$this->session->userdata('user_id').',';
            }
            $m->update_data('reward_claim_temp', $add_, array('id_reward' => $q_reward_point->row()->id_reward, 'status_periode_aktif' => 1));

            $id_reward_claim_ = $q_reward_claim->row()->id;
        }else{
            $m->insert_data('reward_claim_temp', array('id_reward' => $q_reward_point->row()->id_reward, 'id_merchant' => ','.$this->session->userdata('user_id').',', 'id_customer' => '', 'periode' => $periode_tukar_point, 'status_periode_aktif' => 1));

            $id_reward_claim_ = $m->get_data('', 'reward_claim_temp', null ,array('id_reward' => $q_reward_point->row()->id_reward, 'id_merchant' => ','.$this->session->userdata('user_id').',', 'periode' => $periode_tukar_point))->row()->id;
        }

        $data = array(
            'id_merchant' => $this->session->userdata('user_id'),
            'id_customer' => 0,
            'id_reward' => $q_reward_point->row()->id_reward,
            'last_update' => date('Y/m/d H:i:s'),
            'status_pengiriman' => 0,
            'id_reward_claim' => $id_reward_claim_,
            'kode' => $m->get_id('peh')
        );
        //insert point_exchange_history
        $m->insert_data('point_exchange_history', $data);

        $id_history = $m->get_data('id', 'point_exchange_history', null, array('id_merchant' => $this->session->userdata('user_id'), 'id_reward' => $q_reward_point->row()->id_reward, 'id_reward_claim' => $id_reward_claim_), '', 'id desc', 1)->row()->id;

        die(json_encode(array('success' => 1, 'id_history' => $id_history)));
    }
    public function tukar_point(){
        $id_reward = $this->input->post('id_reward');
        $m = $this->m;

        if($m->cek_point_merchant($id_reward)){
            if($m->cek_stok_reward($id_reward)){
                $where0 = array(
                    'prc.id_merchant' => $this->session->userdata('user_id')
                );

                $q_reward = $m->get_data('', 'reward', null, array('id' => $id_reward))->row();

                $q_cart = $m->get_data('', 'point_reward_cart prc', null, $where0);
                if($q_cart->num_rows() > 0){
                    $m->insert_data(
                        'point_reward_cart_detail',
                        array(
                            'id_cart' => $q_cart->row()->id,
                            'id_reward' => $id_reward,
                            'date_add' => date('Y/m/d H:i:s'),
                            'date_modified' => date('Y/m/d H:i:s'),
                            'qty' => 1,
                            'point' => $q_reward->point,
                            'status' => 1,
                            'id_address' => 0
                        )
                    );
                }else{
                    $m->insert_data(
                        'point_reward_cart',
                        array(
                            'date_add' => date('Y/m/d H:i:s'),
                            'date_modified' => date('Y/m/d H:i:s'),
                            'id_merchant' => $this->session->userdata('user_id'),
                            'id_customer' => 0
                        )
                    );

                    $q_cart1 = $m->get_data('', 'point_reward_cart prc', null, $where0)->row();

                    $m->insert_data(
                        'point_reward_cart_detail',
                        array(
                            'id_cart' => $q_cart1->id,
                            'id_reward' => $id_reward,
                            'date_add' => date('Y/m/d H:i:s'),
                            'date_modified' => date('Y/m/d H:i:s'),
                            'qty' => 1,
                            'point' => $q_reward->point,
                            'status' => 1,
                            'id_address' => 0
                        )
                    );
                }
                die(json_encode(array('success' => 1)));
            }else{
                die(json_encode(array('success' => 0, 'msg' => 'Reward tidak tersedia!')));
            }
        }else{
            die(json_encode(array('success' => 0, 'msg' => 'Poin Anda tidak cukup!')));
        }
    }

    public function get_detail_reward(){
        $id_reward = $this->input->post('id_reward');
        $is_history = $this->input->post('is_history');
        $m = $this->m;

        $q = $m->get_data('', 'reward', null, array('id' => $id_reward))->row();

        $data_detail = '
            <div class="col-xs-12" style="text-align: center">
                <h3>'.$q->nama.'</h3>
                <p style="padding: 18px; text-align: justify">'.$q->deskripsi.'</p>
            </div>
        ';
        if($q->id_product != 0){
            $q_p = $m->get_data('', 'products p', array(0 => 'product_image i-i.product=p.id'), array('p.id' => $q->id_product, 'i.primary' => 1))->row();

            $data_detail .= '
                <div class="col-xs-6 col-sm-3 thumbnail" style="height: 300px; ">
                    <div class="content" style="height: 300px; ">
                        <div class="panel panel-flat">
                            <div class="row">
                                <div class="col-xs-12" style="">
                                    <img src="https://tokomanamana.com/files/images/'.$q_p->image.'" class="" style="margin-top: 8px; height: 266px; width: 100%; max-height: 266px; object-fit: cover;" >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-9 thumbnail" style="height: 300px; ">
                    <div class="content" style="height: 300px; ">
                        <div class="panel panel-flat">
                            <div class="row">
                                <div class="col-xs-12" style="height: 274px">
                                    <p style="padding-left: 12px; font-size: 18px;">'.$q_p->name.'</p>
                                    <p style="padding-left: 12px; color: grey; font-size: 10px">Code</p>
                                    <p style="margin-top: -8px; padding-left: 22px;">'.$q_p->code.'</p>
                                    <p style="padding-left: 12px; color: grey; font-size: 10px">QTY</p>
                                    <p style="margin-top: -8px; padding-left: 22px;">'.$q->qty_product.'</p>
                                    <p style="padding-left: 12px; color: grey; font-size: 10px">Berat</p>
                                    <p style="margin-top: -8px; padding-left: 22px;">'.number_format($q_p->weight).' gram</p>
                                    <p style="padding-left: 12px; color: grey; font-size: 10px">Short Desc</p>
                                    <p style="margin-top: -8px; padding-left: 22px;">'.$q_p->short_description.'</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            ';
        }


        if($q->id_product_paket != 0){
            $q_p = $m->get_data('', 'products p', array(0 => 'product_image i-i.product=p.id'), array('p.id' => $q->id_product_paket, 'i.primary' => 1))->row();

            $data_detail .= '
                <div class="col-xs-6 col-sm-3 thumbnail" style="height: 300px; ">
                    <div class="content" style="height: 300px; ">
                        <div class="panel panel-flat">
                            <div class="row">
                                <div class="col-xs-12" style="">
                                    <img src="https://tokomanamana.com/files/images/'.$q_p->image.'" class="" style="margin-top: 8px; height: 266px; width: 100%; max-height: 266px; object-fit: cover;" >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-9 thumbnail" style="height: 300px; ">
                    <div class="content" style="height: 300px; ">
                        <div class="panel panel-flat">
                            <div class="row">
                                <div class="col-xs-12" style="height: 274px">
                                    <p style="padding-left: 12px; font-size: 18px;">'.$q_p->name.'</p>
                                    <p style="padding-left: 12px; color: grey; font-size: 10px">Code</p>
                                    <p style="margin-top: -8px; padding-left: 22px;">'.$q_p->code.'</p>
                                    <p style="padding-left: 12px; color: grey; font-size: 10px">QTY</p>
                                    <p style="margin-top: -8px; padding-left: 22px;">'.$q->qty_paket.'</p>
                                    <p style="padding-left: 12px; color: grey; font-size: 10px">Berat</p>
                                    <p style="margin-top: -8px; padding-left: 22px;">'.number_format($q_p->weight).' gram</p>
                                    <p style="padding-left: 12px; color: grey; font-size: 10px">Short Desc</p>
                                    <p style="margin-top: -8px; padding-left: 22px;">'.$q_p->short_description.'</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            ';
        }

        if($m->cek_periode_tukar_point() && $is_history == '0'){
            $data_detail .= '
                <div class="col-xs-12" style="text-align: center; margin-bottom: 12px">
                    <button onclick="tukar_point('.$q->id.')" class="btn btn-info" type="button">Tukar Poin ('.$q->point.')</button>
                </div>
            ';
        }
        

        die(json_encode(array('data' => $data_detail)));
    }

    public function index() {

        $this->template->_init();
        $this->template->table();
        $this->load->js('../assets/backend/js/modules/order_reward.js');

        $this->breadcrumbs->push(lang('menu_order'), '/orders/reward');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $param_redirect = $this->m->cek_cart_detail();
        if($param_redirect == '1'){
            $this->output->set_title('Konfirmasi Tukar Poin');
            $this->load->view('confirm_reward', $this->data);
        }else{
            $this->output->set_title('Tukar Poin');
            $this->load->view('list_reward', $this->data);
        }
    }

    public function view($id) {
        $this->template->_init();
        $this->load->js('../assets/backend/js/merchant/modules/orders/view.js');

        $id = decode($id);
        $this->data['order'] = $this->orders->get($id);
        $this->data['products'] = $this->orders->get_products($id);
        $this->data['histories'] = $this->orders->get_histories($id);

        $this->breadcrumbs->unshift(lang('menu_order'), '/orders');
        $this->breadcrumbs->push($this->data['order']->code, '/orders/view/' . encode($id));

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('order_heading') . ' ' . $this->data['order']->code);
        $this->load->view('view', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->orders->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                if ($data->shipping_courier != 'pickup') {
                    $data->shipping_courier = explode('-', $data->shipping_courier);
                    $data->shipping_courier = strtoupper($data->shipping_courier[0]) . ' ' . $data->shipping_courier[1];
                }
                $output['data'][] = array(
                    '<a href="' . site_url('orders/view/' . encode($data->id)) . '">' . $data->code . '</a>',
                    get_date_time($data->date_added),
                    $data->shipping_name,
                    number($data->total),
                    $data->shipping_courier,
                    $data->order_status_name,
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                    '<li><a href="' . site_url('orders/view/' . encode($data->id)) . '">' . lang('button_view') . '</a></li>
                    </ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->orders->count_all();
        $output['recordsFiltered'] = $this->orders->count_all($search);
        echo json_encode($output);
    }

    public function respon($respon = 1, $order) {
        $id = decode($order);
        $invoice = $this->main->get('order_invoice', array('id' => $id));
        if ($invoice) {
            if ($respon == 1) {
                $order_status = settings('order_process_status');
                $this->main->update('order_invoice', array('order_status' => $order_status, 'due_date' => date('Y-m-d H:i:s', strtotime(' +2 day'))), array('id' => $id));
                $this->main->insert('order_history', array('order' => $invoice->order, 'invoice' => $invoice->id, 'order_status' => $order_status));
                redirect('orders/view/' . encode($id));
            } elseif ($respon == 2) {
                $merchant_group = $this->main->get('merchant_groups', array('id' => $this->data['user']->merchant_group));
//                $merchant = $this->main->get('merchants', array('id' => $merchant_group->branch));
                $product= $this->orders->get_products($id)->result()[0];
                $this->main->update('order_invoice', array('merchant' => $merchant_group->branch), array('id' => $id));
                $this->db->query("UPDATE product_merchant SET quantity = (quantity + $product->quantity) WHERE product = $product->product AND merchant = " . $invoice->merchant);
                redirect('orders');
            }
        }
    }

    public function sent($order) {
        $id = decode($order);
        $invoice = $this->main->get('order_invoice', array('id' => $id));
        if ($invoice && $this->input->post('airway')) {
            $order_status = settings('order_shipped_status');
            $this->main->update('order_invoice', array('order_status' => $order_status, 'tracking_number' => $this->input->post('airway')), array('id' => $id));
            $this->main->insert('order_history', array('order' => $invoice->order, 'invoice' => $invoice->id, 'order_status' => $order_status));

            $this->data['order'] = $this->orders->get($invoice->id);
            $this->data['products'] = $this->main->gets('order_product', ['invoice' => $invoice->id]);
            $this->data['merchant'] = $this->orders->merchant($this->data['order']->merchant);

            $this->load->helper('text');
            $message = $this->load->view('email/shipping_confirmation', $this->data, true);
            $cronjob = array(
                'from' => settings('send_email_from'),
                'from_name' => settings('store_name'),
                'to' => $this->data['order']->customer_email,
                'subject' => 'Pesanan ' . $this->data['order']->code . ' Sudah Dikirim',
                'message' => $message
            );
            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
            $customer = $this->main->get('customers', array('id' => $this->data['order']->customer));
            if ($customer->verification_phone) {
                $sms = json_decode(settings('sprint_sms'), true);
                $sms['m'] = 'Pesanan ' . $this->data['order']->code . ' sudah dikirim dengan nomor resi ' . $this->data['order']->tracking_number;
                $sms['d'] = $customer->phone;
                $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
            }

            redirect('orders/view/' . encode($id));
        }
    }

    public function update($order, $status) {
        $id = decode($order);
        $status = decode($status);
        $invoice = $this->main->get('order_invoice', array('id' => $id));
        $this->main->update('order_invoice', array('order_status' => $status), array('id' => $id));
        $this->main->insert('order_history', array('order' => $invoice->order, 'invoice' => $invoice->id, 'order_status' => $status));
        if ($status == settings('order_ready_pickup_status')) {
            $this->data['order'] = $this->orders->get($invoice->id);
            $this->data['products'] = $this->main->gets('order_product', ['invoice' => $invoice->id]);
            $this->data['merchant'] = $this->orders->merchant($this->data['order']->merchant);
            $customer = $this->main->get('customers', array('id' => $this->data['order']->customer));
            $this->load->helper('text');
            $message = $this->load->view('email/shipping_confirmation', $this->data, true);
            $cronjob = array(
                'from' => settings('send_email_from'),
                'from_name' => settings('store_name'),
                'to' => $customer->email,
                'subject' => 'Siap Diambil: ' . $this->data['order']->code,
                'message' => $message
            );
            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));

            if ($customer->verification_phone) {
                $this->load->library('sprint');
                $sms = json_decode(settings('sprint_sms'), true);
                $sms['m'] = 'Pesanan ' . $this->data['order']->code . ' siap diambil. Lihat alamat pengambilan di detail pesanan Anda';
                $sms['d'] = $customer->phone;
                $url = $sms['url'];
                unset($sms['url']);
                $sprint_response = $this->sprint->sms($url, $sms);
                $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
            }
        }
        if ($status == settings('order_complete_pickup_status')) {
            $this->main->update('order_invoice', array('date_received' => date('Y-m-d H:i:s'), 'due_date' => date('Y-m-d H:i:s', strtotime(' +2 day'))), array('id' => $invoice->id));
            $this->data['order'] = $this->orders->get($invoice->id);
            $this->data['products'] = $this->main->gets('order_product', ['invoice' => $invoice->id]);
            $this->data['merchant'] = $this->orders->merchant($this->data['order']->merchant);
            $customer = $this->main->get('customers', array('id' => $this->data['order']->customer));
            $this->load->helper('text');
            $message = $this->load->view('email/pickup_completed', $this->data, true);
            $cronjob = array(
                'from' => settings('send_email_from'),
                'from_name' => settings('store_name'),
                'to' => $customer->email,
                'subject' => 'Konfirmasi Pengambilan: ' . $this->data['order']->code,
                'message' => $message
            );
            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
        }
        if ($status == settings('order_cancel_status')) {
            $this->main->update('order_invoice', array('order_status' => $status), array('id' => $invoice->id));
            $this->data['order'] = $this->orders->get($invoice->id);
            $this->data['products'] = $this->main->gets('order_product', ['invoice' => $invoice->id]);
            $this->data['merchant'] = $this->orders->merchant($this->data['order']->merchant);
            $products = $this->main->gets('order_product', array('invoice' => $invoice->id));
            if ($products) {
                foreach ($products->result() as $product) {
                    $product_detail = $this->db->query("select type, package_items from products where id = $product->product ");
                    $row = $product_detail->row();
                    if($row->type == 'p') {
                        $package_items = json_decode($row->package_items, true);
                        foreach ($package_items as $package) {
                            $this->db->query("UPDATE product_merchant SET quantity = (quantity + $product->quantity) WHERE product = ".$package['product_id']." AND merchant = " . $this->data['merchant']->id); 
                        }
                    } else {
                        $this->db->query("UPDATE product_merchant SET quantity = (quantity + $product->quantity) WHERE product = $product->product AND merchant = " . $this->data['merchant']->id);
                    }
                }
            }
        }
        redirect('orders/view/' . encode($id));
    }

    public function request_token() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $order_id = $this->input->post('order_id');
        $phone = $this->input->post('phone');
        //$phone = '087777546456';
        $invoice = $this->main->get('order_invoice', array('id' => $order_id));
        $return = ['status' => 'error','message' => ''];
        $date = date('Y-m-d H:i:s');
        // $check_expired= $this->main->get('orders', array('id' => $order_id,'otp_expired <=' => $date));
        // if($check_expired){
                 $this->load->library('sprint');
            $sms = json_decode(settings('sprint_sms'), true);
            $code = rand(1000, 9999);
            // $currentDate = strtotime($date);
            // $futureDate = $currentDate+(60*2);
            // $formatDate = date('Y-m-d H:i:s', $futureDate);
            $sms['m'] = 'Hai, berikut kode verifikasi TokoManaMana Anda ' . $code;
            $sms['d'] = $phone;
            $url = $sms['url'];
            unset($sms['url']);
            $sprint_response = $this->sprint->sms($url, $sms);
            $this->main->update('orders', array('otp_code' => $code, 'otp_sent_time' => $date), array('id' => $invoice->order));
            $return['status'] = 'success';
            $return['message'] = 'Kode OTP berhasil dikirim!';
        //   } else {
        //     $return['status'] = 'failed';
        //     $return['message'] = 'Kode OTP sudah pernah dikirim. Harap tunggu 2 menit untuk request OTP baru!';
        // }
        echo json_encode($return);
    }

    public function check_token(){
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $order_id = $this->input->post('order_id');
        $code = $this->input->post('code');
        $invoice = $this->main->get('order_invoice', array('id' => $order_id));
        $checking = $this->main->get('orders', array('id' => $invoice->order,'otp_code' => $code));
        if($checking){
            //$datenow = date('Y-m-d H:i:s');
            //$check_expired= $this->main->get('orders', array('id' => $order_id,'otp_expired <=' => $datenow));
            // if($check_expired){
            //     $return['status'] = 'failed';
            //     $return['message'] = 'Kode OTP sudah expired!';
            // } else {
                $return['status'] = 'success';
                $return['message'] = 'Verifikasi berhasil!';
            //}
        } else {
            $return['status'] = 'failed';
            $return['message'] = 'Kode OTP tidak sesuai!';
        }
        echo json_encode($return);   
    }

    public function invoice($id) {
        $id = decode($id);
        $this->data['order'] = $this->orders->get($id);
        $this->data['products'] = $this->orders->get_products($id);
        $this->load->view('invoice', $this->data);
    }

    public function resi($id) {
        $id = decode($id);
        $this->data['order'] = $this->orders->get($id);
        $this->data['merchant'] = $this->orders->merchant($this->data['order']->merchant);
        $this->data['products'] = $this->orders->get_products($id);
        $this->load->view('resi', $this->data);
    }

}
