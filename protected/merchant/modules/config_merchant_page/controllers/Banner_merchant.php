<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Banner_merchant extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->data['menu'] = 'banner_merchant';
    }

    public function index() {
        if($this->aauth->is_loggedin()){
            $this->aauth->control('config_merchant_page/banner_merchant');
        }
        
        $this->data['errors'] = '';

        if($this->input->post('id')){
            $config['upload_path']          = FCPATH.'../files/images/banner_merchant';
            $config['allowed_types']        = 'jpg|jpeg|png';
            $config['file_name']            = 'banner_merchant_'.$this->input->post('id');
            $config['overwrite']            = true;
            $config['max_size']             = 2048; //2MB

            $this->load->library('upload', $config);
            $id = $this->input->post('id');
            $old_filename=$_FILES['banner_merchant_file']['name'];
            $extension = substr($old_filename, strpos($old_filename, ".") + 1);  
            $file_name = 'banner_merchant_'.$this->input->post('id').'.'.$extension;

            if ( ! $this->upload->do_upload('banner_merchant_file'))
            {
                
                    $error = array('error' => $this->upload->display_errors());
                    $this->data['errors'] = $error;
            }
            else
            {   
                   $this->main->update('merchants', array('banner_merchant' => 'banner_merchant/'.$file_name),array('id' => $id));
            }
        }

        $this->template->_init();
        $this->template->form();


        $this->data['data'] = $this->main->get('merchants', array('auth' => $this->data['user']->id));

        $this->load->view('view_banner', $this->data);
    }

    // public function save(){
    //     $config['upload_path']          = FCPATH.'../files/images/banner_merchant';
    //     $config['allowed_types']        = 'jpg|jpeg|png';
    //     $config['file_name']            = 'banner_merchant_'.$this->input->post('id');
    //     $config['overwrite']            = true;
    //     $config['max_size']             = 4096; //4mb

    //     $this->load->library('upload', $config);
    //     $id = $this->input->post('id');
    //     $old_filename=$_FILES['banner_merchant_file']['name'];
    //     $extension = substr($old_filename, strpos($old_filename, ".") + 1);  
    //     $file_name = 'banner_merchant_'.$this->input->post('id').'.'.$extension;

    //     if ( ! $this->upload->do_upload('banner_merchant_file'))
    //     {
            
    //             $error = array('error' => $this->upload->display_errors());

    //             $this->template->_init();
    //             $this->template->form();
    //             $this->data['data'] = $this->main->get('merchants', array('auth' => $this->data['user']->id));
    //             $this->data['errors'] = $error;
    //             $this->load->view('view_banner', $this->data);
    //     }
    //     else
    //     {   
    //            $this->main->update('merchants', array('banner_merchant' => 'banner_merchant/'.$file_name),array('id' => $id));
    //            return redirect('config_merchant_page/banner_merchant/index');
    //     }



    // }

   

}
