<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Profile_picture extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->data['menu'] = 'profile_picture';
    }

    public function index() {
        if($this->aauth->is_loggedin()){
            $this->aauth->control('config_merchant_page/profile_picture');
        }
        $this->data['errors'] = '';
        if($this->input->post('id')){
            $config['upload_path']          = FCPATH.'../files/images/profilepicture_merchant';
            $config['allowed_types']        = 'jpg|jpeg|png';
            $config['file_name']            = 'profilepicture_'.$this->input->post('id');
            $config['overwrite']            = true;
            $config['max_size']             = 1024; //1MB

            $this->load->library('upload', $config);
            $id = $this->input->post('id');
            $old_filename=$_FILES['profilepicture_file']['name'];
            $extension = substr($old_filename, strpos($old_filename, ".") + 1);  
            $file_name = 'profilepicture_'.$this->input->post('id').'.'.$extension;

            if ( ! $this->upload->do_upload('profilepicture_file'))
            {
                
                    $error = array('error' => $this->upload->display_errors());
                    $this->data['errors'] = $error;
            }
            else
            {   

                   $this->main->update('merchants', array('merchant_profile_image' => 'profilepicture_merchant/'.$file_name),array('id' => $id));
                   $this->session->unset_userdata('merchant_user');

            }
        }

        $this->template->_init();
        $this->template->form();
        


        $this->data['data'] = $this->main->get('merchants', array('auth' => $this->data['user']->id));

        $this->load->view('view_pp', $this->data);
    }  

}
