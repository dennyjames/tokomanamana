<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['etalase_heading'] = 'Etalase';
$lang['etalase_sub_heading'] = 'Kelola Data Etalase';
$lang['etalase_list_heading'] = 'Daftar Etalase';
$lang['etalase_add_heading'] = 'Tambah Etalase';
$lang['etalase_edit_heading'] = 'Edit Etalase';
$lang['etalase_related_product'] = 'Produk dalam etalase';

$lang['etalase_not_unique'] = 'Anda sudah memiliki etalase dengan nama yang sama!';

$lang['etalase_image_th'] = 'Gambar';
$lang['etalase_name_th'] = 'Nama';
$lang['etalase_seo_url_th'] = 'Friendly URL';

$lang['etalase_form_general_tabs'] = 'Umum';
$lang['etalase_form_seo_tabs'] = 'SEO';

$lang['etalase_form_name_label'] = 'Nama';
$lang['etalase_form_image_label'] = 'Gambar';
$lang['etalase_form_description_label'] = 'Deskripsi';
$lang['etalase_form_seo_url_label'] = 'Friendly URL';
$lang['etalase_form_meta_title_label'] = 'Meta Judul';
$lang['etalase_form_meta_description_label'] = 'Meta Deskripsi';
$lang['etalase_form_meta_keyword_label'] = 'Meta Keyword';

$lang['etalase_form_name_placeholder'] = 'Masukkan nama etalase';
$lang['etalase_form_description_placeholder'] = 'Masukkan deskripsi dari merek ini. Deskripsi akan ditampilkan di halaman yang menampilkan semua produk dari merek ini';

$lang['etalase_save_success_message'] = "Etalase '%s' berhasil disimpan.";
$lang['etalase_save_error_message'] = "Etalase '%s' gagal disimpan.";
$lang['etalase_delete_success_message'] = "Etalase '%s' berhasil dihapus.";
$lang['etalase_delete_error_message'] = "Etalase '%s' gagal dihapus.";
$lang['page_friendly_url_exist_message'] = "Friendly URL sudah digunakan.";