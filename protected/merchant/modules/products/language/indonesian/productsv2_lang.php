<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['product_heading'] = 'Produk';
$lang['product_list_heading'] = 'Daftar Produk';
$lang['product_add_heading'] = 'Tambah Produk';
$lang['product_edit_heading'] = 'Edit Produk';

$lang['product_image_th'] = 'Gambar';
$lang['product_name_th'] = 'Nama';
$lang['product_code_th'] = 'Kode Produk';
$lang['product_price_old_th'] = 'Harga Normal';
$lang['product_price_th'] = 'Harga Jual';
$lang['product_status_th'] = 'Aktif';
$lang['product_discount_th'] = 'Diskon';
$lang['product_category_th'] = 'Kategori';
$lang['product_merchant_th'] = 'Penjual';
$lang['product_quantity_th'] = 'Stok';

$lang['product_form_general_tabs'] = 'Umum';
$lang['product_form_data_tabs'] = 'Data';
$lang['product_form_image_tabs'] = 'Gambar';
$lang['product_form_feature_tabs'] = 'Fitur';
$lang['product_form_seo_tabs'] = 'SEO';
$lang['product_form_pack_product_tabs'] = 'Paket Produk';
$lang['product_form_attribute_tabs'] = 'Atribut Kombinasi';
$lang['product_form_price_tabs'] = 'Harga';

$lang['product_form_code_label'] = 'Kode Produk';
$lang['product_form_product_name'] = 'Nama Produk';
$lang['product_form_barcode_label'] = 'Barcode';
$lang['product_form_name_label'] = 'Nama';
$lang['product_form_description_label'] = 'Deskripsi';
$lang['product_form_short_description_label'] = 'Deskripsi Singkat';
$lang['product_form_status_label'] = 'Status';
$lang['product_form_brand_label'] = 'Merek';
$lang['product_form_category_label'] = 'Kategori';
$lang['product_form_price_label'] = 'Harga';
$lang['product_form_weight_label'] = 'Berat Kemasan Pengiriman';
$lang['product_form_quantity_label'] = 'Stok';
$lang['product_form_seo_url_label'] = 'Friendly URL';
$lang['product_form_meta_title_label'] = 'Meta Judul';
$lang['product_form_meta_description_label'] = 'Meta Deskripsi';
$lang['product_form_meta_keyword_label'] = 'Meta Kata Kunci';
$lang['product_form_type_label'] = 'Jenis Produk';
$lang['product_form_type_standar_label'] = 'Produk Standar';
$lang['product_form_type_pack_label'] = 'Produk Paket';
$lang['product_form_type_digital_label'] = 'Produk Digital';

$lang['product_form_feature_hidden_info'] = 'Silahkan pilih kategori dahulu untuk mengisi fitur.';

$lang['product_form_name_placeholder'] = 'Masukkan nama produk';
$lang['product_form_short_description_placeholder'] = 'Tuliskan deskripsi singkat sebagai referensi awal pembeli melihat produk ini';
$lang['product_form_description_placeholder'] = 'Tuliskan deskripsi lengkap tentang produk ini, dapat disertakan dengan gambar dan video';
$lang['product_form_meta_title_placeholder'] = 'Tulis jika ingin menggunakan judul yang berbeda dari nama produk';
$lang['product_form_meta_description_placeholder'] = 'Tulis disini untuk membuat deskripsi yang berbeda dengan deskripsi singkat produk di hasil pencarian';
$lang['product_combination_select_attribut_placeholder'] = 'Pilih atribut yang akan dijadikan kombinasi produk';

$lang['product_form_combination_help'] = 'Kombinasi produk dapat dipilih setelah data disimpan';

$lang['product_form_add_image_button'] = 'Tambah Gambar';
$lang['product_form_add_feature_button'] = 'Tambah Fitur';
$lang['product_form_add_pack_product_button'] = 'Tambah Produk';

$lang['product_combination_add_button'] = 'Tambah Kombinasi';
$lang['product_combination_button'] = '<i class=" icon-grid5"></i> Kombinasi';
$lang['product_activated_button'] = '<i class=" icon-checkmark4"></i> Aktifkan';

$lang['product_save_success_message'] = "Produk '%s' berhasil disimpan.";
$lang['product_save_error_message'] = "Produk '%s' gagal disimpan.";
$lang['product_delete_success_message'] = "Produk '%s' berhasil dihapus.";
$lang['product_delete_error_message'] = "Produk '%s' gagal dihapus.";
$lang['product_exist_code_message'] = "Kode produk sudah ada.";
$lang['product_empty_image_message'] = "Gambar kosong! Setidaknya pilih satu gambar.";
$lang['product_friendly_url_exist_message'] = "Friendly URL sudah digunakan.";
$lang['product_productname_url_exist_message'] = "Anda sudah memiliki produk dengan nama yang sama!";
$lang['product_variant_no_status'] = "Status variasi anda tidak ada yang aktif!";

$lang['heading'] = 'Produk';
$lang['list_heading'] = 'Daftar Produk';
$lang['add_heading'] = 'Tambah Produk';
$lang['edit_heading'] = 'Edit Produk';

$lang['image'] = 'Gambar';
$lang['name'] = 'Nama';
$lang['code'] = 'SKU';
$lang['price_old'] = 'Harga Normal';
$lang['price'] = 'Harga Jual';
$lang['status'] = 'Aktif';
$lang['discount'] = 'Diskon';
$lang['promo_range_start'] = 'Jangka waktu promo awal';
$lang['promo_range_end'] = 'Jangka waktu promo akhir';
$lang['category'] = 'Kategori';
$lang['merchant'] = 'Penjual';
$lang['quantity'] = 'Stok';
$lang['package_item'] = 'Barang Paket';

$lang['general_tabs'] = 'Dasar';
$lang['feature_tabs'] = 'Fitur';
$lang['seo_tabs'] = 'SEO';
$lang['option_tabs'] = 'Variasi';
$lang['price_tabs'] = 'Harga';

$lang['code'] = 'Kode Produk';
$lang['barcode'] = 'Barcode';
$lang['name'] = 'Nama';
$lang['description'] = 'Deskripsi';
$lang['short_description'] = 'Deskripsi Singkat';
$lang['table_description'] = 'Spesifikasi';
$lang['status'] = 'Status';
$lang['brand'] = 'Merek';
$lang['guarantee'] = 'Garansi';
$lang['category'] = 'Kategori';
$lang['price'] = 'Harga';
$lang['weight'] = 'Berat Kemasan Pengiriman';
$lang['dimension'] = 'Dimensi (P x L x T)';
$lang['quantity'] = 'Stok';
$lang['seo_url'] = 'Friendly URL';
$lang['meta_title'] = 'Meta Judul';
$lang['meta_description'] = 'Meta Deskripsi';
$lang['meta_keyword'] = 'Meta Kata Kunci';
$lang['type'] = 'Jenis Produk';
$lang['type_standar'] = 'Produk Standar';
$lang['type_pack'] = 'Produk Paket';
$lang['type_digital'] = 'Produk Digital';

$lang['feature_hidden_info'] = 'Silahkan pilih kategori dahulu untuk mengisi fitur.';

$lang['name_placeholder'] = 'Masukkan nama produk';
$lang['short_description_placeholder'] = 'Tuliskan deskripsi singkat sebagai referensi awal pembeli melihat produk ini';
$lang['description_placeholder'] = 'Tuliskan deskripsi lengkap tentang produk ini';
$lang['meta_title_placeholder'] = 'Tulis jika ingin menggunakan judul yang berbeda dari nama produk';
$lang['meta_description_placeholder'] = 'Tulis disini untuk membuat deskripsi yang berbeda dengan deskripsi singkat produk di hasil pencarian';
$lang['combination_select_attribut_placeholder'] = 'Pilih atribut yang akan dijadikan kombinasi produk';

$lang['add_image_button'] = 'Tambah Gambar';
$lang['add_feature_button'] = 'Tambah Fitur';
$lang['add_pack_button'] = 'Tambah Produk';

$lang['combination_add_button'] = 'Tambah Kombinasi';
$lang['combination_button'] = '<i class=" icon-grid5"></i> Kombinasi';

$lang['save_success_message'] = "Produk '%s' berhasil disimpan.";
$lang['save_error_message'] = "Produk '%s' gagal disimpan.";
$lang['delete_success_message'] = "Produk '%s' berhasil dihapus.";
$lang['delete_error_message'] = "Produk '%s' gagal dihapus.";
$lang['exist_code_message'] = "Kode produk sudah ada.";
$lang['empty_image_message'] = "Gambar kosong! Setidaknya tambahkan satu gambar.";
$lang['empty_variation'] = "Tidak ada data variasi!";
$lang['image_primary_not_set_message'] = "Pilih gambar default dahulu.";
$lang['default_option_not_set_message'] = "Pilih variasi default dahulu.";
$lang['friendly_url_exist_message'] = "Friendly URL sudah digunakan.";