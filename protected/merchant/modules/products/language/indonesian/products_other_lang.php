<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['product_heading'] = 'Produk Lainnya';
$lang['product_list_heading'] = 'Daftar Produk Lainnya';
$lang['product_add_heading'] = 'Tambah Produk Lainnya';
$lang['product_edit_heading'] = 'Edit Produk Lainnya';

$lang['product_image_th'] = 'Gambar';
$lang['product_name_th'] = 'Nama';
$lang['product_code_th'] = 'SKU';
$lang['product_category_th'] = 'Kategori';
$lang['product_price_old_th'] = 'Harga Normal';
$lang['product_price_th'] = 'Harga Jual';
$lang['product_discount_th'] = 'Diskon';
$lang['product_status_th'] = 'Status';
$lang['product_quantity_th'] = 'Stok';
$lang['product_viewed_th'] = 'Dilihat';
$lang['product_purchased_th'] = 'Dibeli';

$lang['product_form_general_step'] = 'Informasi Produk';
$lang['product_form_data_step'] = 'Data';
$lang['product_form_image_step'] = 'Gambar';
$lang['product_form_feature_step'] = 'Fitur';
$lang['product_form_preview_step'] = 'Pratinjau';

$lang['product_form_code_label'] = 'SKU';
$lang['product_form_barcode_label'] = 'Barcode';
$lang['product_form_name_label'] = 'Nama';
$lang['product_form_description_label'] = 'Deskripsi';
$lang['product_form_short_description_label'] = 'Deskripsi Singkat';
$lang['product_form_brand_label'] = 'Merek';
$lang['product_form_category_label'] = 'Kategori';
$lang['product_form_price_label'] = 'Harga Jual';
$lang['product_form_cost_label'] = 'Harga Beli';
$lang['product_form_discount_label'] = 'Diskon';
$lang['product_form_weight_label'] = 'Berat Produk';
$lang['product_form_quantity_label'] = 'Stok';
$lang['product_form_meta_title_label'] = 'Meta Judul';
$lang['product_form_meta_description_label'] = 'Meta Deskripsi';
$lang['product_form_meta_keyword_label'] = 'Meta Kata Kunci';
$lang['product_form_type_label'] = 'Jenis Produk';
$lang['product_form_type_standar_label'] = 'Produk Standar';
$lang['product_form_type_pack_label'] = 'Produk Paket';
$lang['product_form_type_digital_label'] = 'Produk Digital';
$lang['product_form_type_combination_label'] = 'Produk Kombinasi';

$lang['product_form_category_placeholder'] = 'Pilih kategori';
$lang['product_form_name_placeholder'] = 'Masukkan nama produk';
$lang['product_form_short_description_placeholder'] = 'Tuliskan deskripsi singkat sebagai referensi awal pembeli melihat produk ini';
$lang['product_form_description_placeholder'] = 'Tuliskan deskripsi lengkap tentang produk ini, dapat disertakan dengan gambar dan video';
$lang['product_form_meta_title_placeholder'] = 'Tulis jika ingin menggunakan judul yang berbeda dari nama produk';
$lang['product_form_meta_description_placeholder'] = 'Tulis disini untuk membuat deskripsi yang berbeda dengan deskripsi singkat produk di hasil pencarian';
$lang['product_form_brand_empty'] = 'Tidak ada merek';

$lang['product_form_combination_help'] = 'Kombinasi produk dapat dipilih setelah data disimpan';

$lang['product_form_add_image_button'] = 'Tambah Gambar';
$lang['product_form_add_feature_button'] = 'Tambah Fitur';
$lang['product_form_add_pack_product_button'] = 'Tambah Produk';

$lang['product_combination_add_button'] = 'Tambah Kombinasi';
$lang['product_combination_button'] = '<i class=" icon-grid5"></i> Kombinasi';

$lang['product_save_success_message'] = "Produk '%s' berhasil disimpan dan akan di verifikasi dahulu sebelum ditayangkan.";
$lang['product_save_error_message'] = "Produk '%s' gagal disimpan.";
$lang['product_delete_success_message'] = "Produk '%s' berhasil dihapus.";
$lang['product_delete_error_message'] = "Produk '%s' gagal dihapus.";
$lang['product_exist_code_message'] = "SKU produk sudah terdaftar untuk produk lain.";

$lang['product_combination_save_success_message'] = "Kombinasi produk berhasil disimpan.";
$lang['product_combination_empty_attribute_message'] = "Untuk membuat kombinasi produk atribut harus dipilih salah satu.";
$lang['product_combination_exist_data_message'] = "Kombinasi produk yang dipilih sudah ada.";