<?php

defined('BASEPATH') or exit('No direct script access allowed!');
require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


class Productv2 extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->lang->load('productsv2', settings('language'));
        $this->load->model('productv2_model', 'products');
        $this->data['menu'] = 'product_2.0';
    }

    public function index() {
        if($this->data['user']->merchant_type != 'merchant'){
            $this->load->view('errors/html/error_notallowed');
            return false;
        }
        $this->template->_init();
        $this->template->table();
        $this->template->form();
        $this->load->js('../assets/backend/js/merchant/modules/products/productv2.js');


        $this->breadcrumbs->push(lang('menu_product'), '/products/productv2');
        // $this->breadcrumbs->push(lang('menu_product_tanaka'), '/products/tanaka');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('product_heading'));
        $this->load->view('v2/list', $this->data);
    }

    public function view($id) {
        $this->template->_init();
        $this->load->js('../assets/backend/js/modules/catalog/product_view.js');

        $id = decode($id);
        $this->data['product'] = $this->products->get($id);
        $this->data['product_images'] = $this->main->gets('product_image', array('product' => $id));
        $this->data['product_features'] = $this->products->get_product_feature_by_category($this->data['product']->category, $id);
        $this->data['brand'] = $this->main->get('brands', array('id' => $this->data['product']->brand));
        $this->data['category'] = $this->categories->get_category_view_frontend($this->data['product']->category);
        $this->data['merchant'] = $this->main->get('merchants', array('id' => $this->data['product']->merchant));

        $this->breadcrumbs->unshift(lang('menu_catalog'), '/');
        $this->breadcrumbs->push(lang('menu_product'), '/catalog/products');
        $this->breadcrumbs->push($this->data['product']->name, '/catalog/products/view/' . encode($id));

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title('Produk ' . $this->data['product']->name);
        $this->load->view('product_view', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->products->get_all($this->data['user']->merchant, $start, $length, $search, $order);
        $username = $this->products->get_username($this->data['user']->merchant);
        
        

        if ($datas) {
            foreach ($datas->result() as $data) {
                $row_data = array(

                    '<a href="' . site_url('../files/images/' . $data->image) . '" data-lightbox="list_image"><img src="' . site_url('../files/images/' . $data->image) . '" height="60" class="list_image" </a>',
                    $data->code,
        
                    '<a target="_blank" rel="noopener noreferrer" href="' . site_url('../' . strtolower($username->username . '/' . str_replace(' ', '-', $data->category) . '/'. str_replace(' ', '-', $data->name))) . '">' .  $data->name . '</a>',

                    "Rp ".number($data->price),
                    $data->category,
                    $data->date_added,
                    '<a href="#" class="status" data-id="' . $data->id . '" data-status="' . $data->status . '"  >'.$this->status_product($data->status).'</i>',
                    // '<a href="#" data-id="' . $data->id . '" data-status="' . $data->status . '" class="status">' . $this->_status_product($data->status) . '</a>',

                   

                );
                //if ($this->ion_auth->in_group(3)) {
                

                    if($data->variation == 1){
                        $row_data[] = '<i>Produk Bervariasi</i>';

                    }
                    else{
                        $row_data[] = '<input type="number" min="0" data-id="' . $data->id . '"   class="quantity form-control" oninput="this.value = Math.abs(this.value)" value="' . $data->quantity . '">';
                    }
                   
                    $row_data[] = '<a href="' . site_url('products/productv2/form_merchant/' . encode($data->id)) . '"><button type="button"   class="edit btn btn-info">Edit</button></a>  <button type="button" data-id="' . $data->id . '" class="remove btn btn-danger">Remove</button>';      

                    
                    // $pricing_level = $this->products->price_level_group($data->id, $this->data['user']->merchant_group);
                    // $input_pricing_level = '';
                    // if ($pricing_level->num_rows()>0) {
                    //     $html_pricing_level = '<table class=\'table\'>
                    //                         <thead>
                    //                         <tr><th style=\'padding:4px\'>Jumlah</th><th style=\'padding:4px\'>Harga / unit</th></tr>
                    //                         </thead><tbody>';
                    //     foreach ($pricing_level->result() as $row => $pl) {
                    //         $html_pricing_level .= '<tr><td style=\'padding:4px\'>';
                    //         if (($row + 1) == $pricing_level->num_rows()) {
                    //             $html_pricing_level .= '>= ' . $pl->min_qty;
                    //         } else {
                    //             $html_pricing_level .= $pl->min_qty . ' - ' . ($pricing_level->row($row + 1)->min_qty - 1);
                    //         }
                    //         $html_pricing_level .= '</td><td style=\'padding:4px\'>' . rupiah($pl->price) . '</td></tr>';
                    //     }
                    //     $html_pricing_level .= '</tbody></table>';
                    //     $input_pricing_level = '<input type="checkbox" class="pricing_level" data-id="' . $data->product_merchant . '" ' . ($data->pricing_level == 1 ? 'checked' : '') . ' ><br><a href="javascript:void(0);" data-popup="popover" data-content="' . $html_pricing_level . '" data-placement="bottom" data-html="true">Lihat harga grosir</a>';
                    // }
                    // $row_data[] = $input_pricing_level;
                //}
                $output['data'][] = $row_data;
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->products->count_all($this->data['user']->merchant);
        $output['recordsFiltered'] = $this->products->count_all($this->data['user']->merchant, $search);
        echo json_encode($output);
    }

    //  public function update_status() {
    //     $this->input->is_ajax_request() or exit('No direct post submit allowed!');
    //     $data = $this->input->post(null, true);
    //     $data['merchant'] = $this->data['user']->merchant;
    //     if ($data['id']) {
    //         $this->main->update('product_merchant', $data, array('id' => $data['id']));
    //     } else {
    //         $data['id'] = $this->main->insert('product_merchant', $data);
    //     }
    //     echo $data['id'];
    // }

    public function update_status_merchant() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        if ($data['id']) {
            $this->main->update('products', $data, array('id' => $data['id']));
        } else {
            return error;
        }
        echo $data['status'];
        
    }

    // public function update_quantity() {
    //     $this->input->is_ajax_request() or exit('No direct post submit allowed!');
    //     $data = $this->input->post(null, true);
    //     $data['merchant'] = $this->data['user']->merchant;
    //     if ($data['id']) {
    //         $this->main->update('product_merchant', $data, array('id' => $data['id']));
    //     } else {
    //         $data['id'] = $this->main->insert('product_merchant', $data);
    //     }
    //     echo $data['id'];
    // }

    public function update_quantity_merchant() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        if ($data['id']) {
            $this->main->update('products', $data, array('id' => $data['id']));
        } else {
            return error;
        }
        echo $data['id'];
    }

    public function pricing_level() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $this->main->update('product_merchant', array('pricing_level' => $data['pricing_level']), array('id' => $data['id']));
    }

     public function add_merchant() {
        $product = $this->main->insert('products', array('is_temp' => 1, 'user_added' => $this->data['user']->id, 'store_type'=>'merchant','store_id' =>$this->data['user']->merchant));
        redirect('products/productv2/form_merchant/' . encode($product));
    }

    // public function form_merchant() {
    //     if(isset($_SESSION['image_id_session'])) unset($_SESSION['image_id_session']);
        
    //     $this->data['brands'] = $this->main->gets('brands', array(), 'name ASC');
    //     $this->data['etalases'] = $this->main->gets('etalase', array('merchant_id' => $this->data['user']->merchant), 'name ASC');
    //     $this->data['categories'] = $this->products->get_categories();
    //     $this->data['option_groups'] = $this->main->gets('option_group', array(), 'sort_order asc');
    //     $this->data['merchant_groups'] = $this->main->gets('merchant_groups', array(), 'name asc');
    //     $this->data['price_levels'] = array();

    //     $this->data['prices'] = $this->products->get_prices(0);
    //     $this->template->_init();
    //     $this->template->form();
    //     // $this->load->js('https://code.jquery.com/ui/1.10.4/jquery-ui.js');
    //     $this->load->js('../assets/backend/js/merchant/modules/products/product_form.js');
    //     $this->lang->load('productsv2', settings('language'));
    //     $this->output->set_title(lang('heading'));
    //     $this->load->view('v2/form', $this->data);
    // }

    public function form_merchant($id) {
//        $id ? $this->aauth->control('catalog/product/edit') : $this->aauth->control('catalog/product/add');

        $id = decode($id);
//        $this->data['data'] = $this->main->get('products',array('id'=>$id));
        $this->data['merchant_id'] = $this->data['user']->merchant;
        $username = $this->products->get_username($this->data['user']->merchant);
        $this->data['username'] = $username->username;
        $this->data['brands'] = $this->main->gets('brands', array(), 'name ASC');
        $this->data['etalases'] = $this->main->gets('etalase', array('store_id' => $this->data['user']->merchant,'store_type' => 'merchant'), 'name ASC');
        $this->data['categories'] = $this->products->get_categories();
        // $this->data['variations'] = $this->products->get_variations();
        // $this->data['option_groups'] = $this->main->gets('option_group', array(), 'sort_order asc');
        // $this->data['merchant_groups'] = $this->main->gets('merchant_groups', array(), 'name asc');
        // $this->data['price_levels'] = array();
//        $this->breadcrumbs->unshift(lang('catalog'), '/');
//        $this->breadcrumbs->push(lang('product'), '/catalog/products');
//
//        if ($id) {
//            $id = decode($id);
        $this->data['data'] = $this->products->get($id);

        //security if directly accessed from url from another account
        if($this->data['data']->store_id != $this->data['user']->merchant){
            $this->load->view('errors/html/error_notallowed');
            return false;
        }

        $this->data['data']->description = str_replace("<br />", "\n", $this->data['data']->description); 
        $this->data['data']->package_items = json_decode($this->data['data']->package_items);
        $this->data['data']->promo_data = json_decode($this->data['data']->promo_data);
        $this->data['data']->table_description = json_decode($this->data['data']->table_description);
        $this->data['images'] = $this->main->gets('product_image', array('product' => $id));
        // $this->data['features'] = $this->products->get_features_by_category($this->data['data']->category, $this->data['data']->id);
        // $this->data['option_groups'] = $this->products->get_variations_by_category($this->data['data']->category);
        $this->data['data']->description = tinymce_get_url_files($this->data['data']->description);
        $this->data['product_options'] = $this->products->get_product_options($this->data['data']->id);
        $this->data['price_levels'] = $this->main->gets('product_price_level', array('product' => $id), 'min_qty asc');
        $this->data['price_resellers'] = $this->main->gets('product_price_reseller', array('product' => $id), 'min_qty asc');
        $this->data['package_product_options'] = $this->products->get_products_only($this->data['user']->merchant);

//            $this->breadcrumbs->push(lang('edit_heading'), '/catalog/products/form/' . encode($id));
//            $this->breadcrumbs->push($this->data['data']->name, '/');
//        } else {
//            $this->breadcrumbs->push(lang('add_heading'), '/catalog/products/form');
//        }
        $this->data['prices'] = $this->products->get_prices(($id) ? $id : 0);
//        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->form();
        $this->load->js('https://code.jquery.com/ui/1.10.4/jquery-ui.js');
        $this->load->js('../assets/backend/js/merchant/modules/products/product_form_old.js');
        $this->output->set_title(lang('heading'));
        $this->load->view('v2/form', $this->data);
    }

    //  public function save() {
    //     $this->input->is_ajax_request() or exit('No direct post submit allowed!');

    //     $this->load->library('form_validation');

    //     $this->form_validation->set_rules('code', 'lang:code', 'trim|required|alpha_dash|is_unique[products.code]');
    //     $this->form_validation->set_rules('price', 'lang:price', 'trim|required|numeric');
    //     $this->form_validation->set_rules('weight', 'lang:weight', 'trim|required|numeric');
    //     $this->form_validation->set_rules('name', 'lang:name', 'trim|required');
    //     $this->form_validation->set_rules('short_description', 'lang:short_description', 'trim|required');
    //     $this->form_validation->set_rules('category', 'lang:category', 'trim|required');
    //     $this->form_validation->set_rules('seo_url', 'lang:seo_url', 'trim|seo_url');

    //     if ($this->form_validation->run() === true) {
    //         $data = $this->input->post(null, true);

    //          // if($_POST["next"]) {



    //          // }
        
    //         do {
    //             $product = $this->main->insert('products', array('is_temp' => 1, 'user_added' => $this->data['user']->id, 'store_type'=>'merchant','store_id' =>$this->data['user']->merchant));

    //             $features = array();
    //             $prices = array();
    //             $level_prices = array();
    //             $options = array();
    //             $package_items = array();
    //             $table_description = array();
    //             $promo_data = array();

    //             if (isset($data['prices'])) {
    //                 $prices = $data['prices'];
    //                 unset($data['prices']);
    //             }
    //             if (isset($data['price_level'])) {
    //                 $level_prices = $data['price_level'];
    //                 unset($data['price_level']);
    //             }
    //             if (isset($data['feature'])) {
    //                 $features = array_filter($data['feature']);
    //                 unset($data['feature']);
    //             }
    //             if (isset($data['options'])) {
    //                 $options = $data['options'];
    //                 unset($data['options']);
    //             }
    //             if (isset($data['package_items'])) {
    //                 $data['package_items'] = json_encode($data['package_items']);
    //             }
    //             if (isset($data['table_description'])) {
    //                 $data['table_description'] = json_encode($data['table_description']);
    //             }
    //             if (isset($data['promo_data'])) {
    //                 $data['promo_data']['date_start'] = date('Y-m-d',strtotime($data['promo_data']['date_start']));
    //                 $data['promo_data']['date_end'] = date('Y-m-d',strtotime($data['promo_data']['date_end']));
    //                 $data['promo_data'] = json_encode($data['promo_data']);
    //             }
    //             if(isset($data['coming_soon_time'])){
    //                 $data['coming_soon_time'] = date('Y-m-d', strtotime($data['coming_soon_time']));
    //             }

    //             $image_id_sessions =  $this->session->userdata('image_id_session');

    //             if($image_id_sessions){ 
    //                 foreach ($image_id_sessions as $image_id_session) {
    //                     $this->main->update('product_image', array('product' => $product), array('id' => $image_id_session));
    //                 }
    //             }else{
    //                 $return = array('message' => lang('empty_image_message'), 'status' => 'error');
    //                 break; 
    //             }

    //             if (!isset($data['image_primary'])) {
    //                 $return = array('message' => lang('image_primary_not_set_message'), 'status' => 'error');
    //                 break;
    //             }

    //             if ($this->main->gets('product_option', array('product' => $product))) {
    //                 if (!isset($data['default_option'])) {
    //                     $return = array('message' => lang('default_option_not_set_message'), 'status' => 'error');
    //                     break;
    //                 }
    //                 $this->main->update('product_option', array('default' => 0), array('product' => $product));
    //                 $this->main->update('product_option', array('default' => 1), array('product' => $product, 'id' => $data['default_option']));
    //                 unset($data['default_option']);
    //             }
    //             $category_url = $this->products->get_category_seourl($data['category']);
    //             $url = 'products/productv2/view/' . $product;
    //             $seo_url = $this->data['user']->username . '/' . str_replace(' ', '', $category_url) . '/' . str_replace(' ', '', $data['name']);
    //             $seo_url_check = $this->products->check_seo_url_unique($seo_url);
    //             if($seo_url_check){
    //                  $return = array('message' => lang('product_productname_url_exist_message'), 'status' => 'error');
    //                     break;
    //             }
    //             unset($data['seo_url']);

    //             $this->main->insert('seo_url', array('query' => $url, 'keyword' => $seo_url));
    //             $data['meta_title'] = $data['name'];
    //             $data['meta_description'] = $data['short_description'];

    //             unset($data['image_primary']);

    //             $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;
    //             $data['description'] = tinymce_parse_url_files($data['description']);
    //             $data['price'] = parse_number($data['price']);

    //             $this->main->update('products', $data, array('id' => $product));

    //             if (count($options) > 0) {
    //                 foreach ($options as $id => $option) {
    //                     $this->main->update('product_option', array('price' => $option['price'], 'weight' => $option['weight']), array('id' => $id));
    //                 }
    //             }

    //             $data_products = array();
    //             if (count($features) > 0) {
    //                 foreach ($features as $feature_id => $feature) {
    //                     if ($feature['value']) {
    //                         if (is_array($feature['value'])) {
    //                             if (count($feature['value']) > 0) {
    //                                 foreach ($feature['value'] as $variant) {
    //                                     $data_products[] = array(
    //                                         'product' => $product,
    //                                         'feature' => $feature['id'],
    //                                         'value' => $variant
    //                                     );
    //                                 }
    //                             }
    //                         } else {
    //                             $data_products[] = array(
    //                                 'product' => $product,
    //                                 'feature' => $feature_id,
    //                                 'value' => $feature['value']
    //                             );
    //                         }
    //                     }
    //                 }
    //                 if ($data_products)
    //                     $this->db->insert_batch('product_feature', $data_products);
    //             }
    //             if (count($level_prices) > 0) {
    //                 foreach ($level_prices as $price) {
    //                     if (isset($price['qty']) && isset($price['price'])) {
    //                         $price_level_parent = $price['price'];
    //                         $price_level = $this->main->insert('product_price_level', array('product' => $product, 'min_qty' => $price['qty'], 'price' => $price['price']));
    //                     }
    //                 }
    //             }
    //             $return = array('message' => sprintf(lang('save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('products/productv2'));
    //         } while (0);
    //     } else {
    //         $return = array('message' => validation_errors(), 'status' => 'error');
    //     }

    //     echo json_encode($return);
    // }

     public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');

        // $code_check = '';
        // if ($this->input->post('id')) {
        //     $code = $this->main->get('products', array('id' => $this->input->post('id')))->code;
        //     if ($code != $this->input->post('code')) {
        //         $code_check = '|is_unique[products.code]';
        //     }
        // }

        if($this->input->post('promo')){
            $promo = $this->input->post('promo');
            if($promo==1){
                $this->form_validation->set_rules('promo_data[discount]', 'lang:discount', 'trim|required|numeric|greater_than[0]');
                $this->form_validation->set_rules('promo_data[date_start]', 'lang:promo_range_start', 'trim|required');
                $this->form_validation->set_rules('promo_data[date_end]', 'lang:promo_range_end', 'trim|required');
            }

        }

   
        if($this->input->post('variation') == 0){
            $this->form_validation->set_rules('price', 'lang:price', 'trim|required|numeric|greater_than_equal_to[100]');
        }
        
        
        $this->form_validation->set_rules('code', 'lang:code', 'trim|alpha_dash');
        $this->form_validation->set_rules('weight', 'lang:weight', 'trim|required|numeric');
        $this->form_validation->set_rules('name', 'lang:name', 'trim|required|callback_badword_check');
        $this->form_validation->set_rules('short_description', 'lang:short_description', 'trim|required');
        $this->form_validation->set_rules('category', 'lang:category', 'trim|required');
        $this->form_validation->set_rules('seo_url', 'lang:seo_url', 'trim|seo_url');


        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            do {
                $username = $this->products->get_username($this->data['user']->merchant);
                $features = array();
                $prices = array();
                $level_prices = array();
                $reseller_prices = array();
                $options = array();
                $package_items = array();
                $table_description = array();
                $promo_data = array();

                if (isset($data['prices'])) {
                    $prices = $data['prices'];
                    unset($data['prices']);
                }
                if (isset($data['price_level'])) {
                    $level_prices = $data['price_level'];
                    unset($data['price_level']);
                }
                if (isset($data['price_reseller_list'])) {
                    $reseller_prices = $data['price_reseller_list'];
                    unset($data['price_reseller_list']);
                }

                // if (isset($data['feature'])) {
                //     $features = array_filter($data['feature']);
                //     unset($data['feature']);
                // }
                if($data['variation'] == 1){
                    if (!isset($data['options'])) {
                         $return = array('message' => lang('empty_variation'), 'status' => 'error');
                    break;
                    }
                }
                if (isset($data['options'])) {
                    $options = $data['options'];
                    unset($data['options']);
                }
                if (isset($data['package_items'])) {
                    $data['package_items'] = json_encode($data['package_items']);
                }
                if (isset($data['table_description'])) {
                    $data['table_description'] = json_encode($data['table_description']);
                }
                if (isset($data['promo_data'])) {
                    $data['promo_data']['date_start'] = date('Y-m-d',strtotime($data['promo_data']['date_start']));
                    $data['promo_data']['date_end'] = date('Y-m-d',strtotime($data['promo_data']['date_end']));
                    $data['promo_data'] = json_encode($data['promo_data']);
                }
                if(isset($data['coming_soon_time'])){
                    $data['coming_soon_time'] = date('Y-m-d', strtotime($data['coming_soon_time']));
                }
                if (!$product_images = $this->main->gets('product_image', array('product' => $data['id']))) {
                    $return = array('message' => lang('empty_image_message'), 'status' => 'error');
                    break;
                }
                if (!isset($data['image_primary'])) {
                    $return = array('message' => lang('image_primary_not_set_message'), 'status' => 'error');
                    break;
                }

                $data['description'] = str_replace("\n", "<br />",  $data['description']); 


                if ($this->main->gets('product_option', array('product' => $data['id']))) {
                    if (!isset($data['default_option'])) {
                        $return = array('message' => lang('default_option_not_set_message'), 'status' => 'error');
                        break;
                    }
                    $this->main->update('product_option', array('default' => 0), array('product' => $data['id']));
                    $this->main->update('product_option', array('default' => 1), array('product' => $data['id'], 'id' => $data['default_option']));
                    unset($data['default_option']);
                }
                $url = 'catalog/products/view/' . $data['id'] .'/'. $this->data['user']->merchant;
                if ($seo_url = $data['seo_url']) {
                    if (check_url($seo_url, $url)) {
                        $return = array('message' => lang('friendly_url_exist_message'), 'status' => 'error');
                        break;
                    }
                } else {
                    $category_url = $this->products->get_category_seourl($data['category']);
                    $seo_url = strtolower($username->username . '/' . str_replace(' ', '-', $category_url) . '/' . str_replace(' ', '-', $data['name']));
                }
                unset($data['seo_url']);

                 $data['meta_title'] = $data['name'];
                 $data['meta_description'] = $data['short_description'];

                $this->main->update('product_image', array('primary' => 0), array('product' => $data['id']));
                $this->main->update('product_image', array('primary' => 1), array('product' => $data['id'], 'id' => $data['image_primary']));
                unset($data['image_primary']);

                $this->main->delete('seo_url', array('query' => $url));
                $this->main->insert('seo_url', array('query' => $url, 'keyword' => $seo_url));

                $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;
                $data['description'] = tinymce_parse_url_files($data['description']);
                $data['price'] = parse_number($data['price']);


                // $this->main->delete('product_feature', array('product' => $data['id']));
                //$this->main->delete('product_price', array('product' => $data['id']));
                // $this->db->where_in('product_price_level', '(SELECT id FROM product_price_level WHERE product = ' . $data['id'] . ')', false)
                //         ->delete('product_price_level_groups');
                $this->main->delete('product_price_level', array('product' => $data['id']));
                $this->main->delete('product_price_reseller', array('product' => $data['id']));
                

                $this->main->update('products', $data, array('id' => $data['id']));

                if (count($options) > 0) {
                    $count_variant_status = 0;
                    foreach ($options as $id => $option) {
                        if($option['status'] == 1) {
                            $count_variant_status++;
                            break;
                        }
                    }
                    if($count_variant_status > 0){
                        foreach ($options as $id => $option) {
                            if($option['status'] == 1) $count_variant_status++;
                            $this->main->update('product_option', array('price' => $option['price'], 'quantity' => $option['stock'],'status' => $option['status']), array('id' => $id));
                        }
                    }
                    else{
                        $return = array('message' => lang('product_variant_no_status'), 'status' => 'error');
                        break;
                    }
                }

                // $data_products = array();
                // if (count($features) > 0) {
                //     foreach ($features as $feature_id => $feature) {
                //         if ($feature['value']) {
                //             if (is_array($feature['value'])) {
                //                 if (count($feature['value']) > 0) {
                //                     foreach ($feature['value'] as $variant) {
                //                         $data_products[] = array(
                //                             'product' => $data['id'],
                //                             'feature' => $feature['id'],
                //                             'value' => $variant
                //                         );
                //                     }
                //                 }
                //             } else {
                //                 $data_products[] = array(
                //                     'product' => $data['id'],
                //                     'feature' => $feature_id,
                //                     'value' => $feature['value']
                //                 );
                //             }
                //         }
                //     }
                //     if ($data_products)
                //         $this->db->insert_batch('product_feature', $data_products);
                // }
               
                if (count($level_prices) > 0) {
                    foreach ($level_prices as $price) {
                        if (isset($price['qty']) && isset($price['price'])) {
                            $price_level = $this->main->insert('product_price_level', array('product' => $data['id'], 'min_qty' => $price['qty'], 'price' => $price['price']));
                        }
                    }
                }
                if (count($reseller_prices) > 0) {
                    foreach ($reseller_prices as $reseller) {
                        if (isset($reseller['qty']) && isset($reseller['price'])) {
                            $price_reseller = $this->main->insert('product_price_reseller', array('product' => $data['id'], 'min_qty' => $reseller['qty'], 'price' => $reseller['price']));
                        }
                    }
                }

                $return = array('message' => sprintf(lang('save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('products/productv2'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function products_package(){
        $products_package = $this->products->get_products_only($this->data['user']->merchant);
        if($products_package){
            echo json_encode($products_package);
        }
        else{
            echo "FALSE";
        }


    }

    public function get_new_variation_list(){
        $id = $this->input->post('id');
        $product_options = $this->products->get_product_options($id);
        // var_dump($product_options->result()); exit();
         if($product_options){
            echo json_encode(array('status' => 'success', 'data' => $product_options->result()));
        }
        else{
            echo "FALSE";
        }

    }


    //   public function image() {
    //     $this->input->is_ajax_request() or exit('No direct post submit allowed!');
    //     $data = $this->input->post(null, true);
    //     switch ($data['act']) {
    //         case 'upload':
    //             if(empty($data['product'])){
    //                 $id = $this->main->insert('product_image', array('image' => $data['image'], 'primary' => $data['primary']));
                

    //                 $image_id_session =  $this->session->userdata('image_id_session');
    //                 if (!is_array($image_id_session)) $image_id_session= array();
    //                 $image_id_session[] = $id;
    //                 $this->session->set_userdata('image_id_session', $image_id_session);
    //                 echo $id;
    //                 break;
    //             }
    //             else{
    //                 $id = $this->main->insert('product_image', array('product' => $data['product'], 'image' => $data['image'], 'primary' => $data['primary']));

    //                 echo $id;
    //                 break;
    //             }
                                   
                
    //         case 'delete':
    //             $this->main->delete('product_image', array('id' => $data['id']));
    //             $this->main->delete('product_option_image', array('product_image' => $data['id']));
    //             break;
    //         default:break;
    //     }
    // }

    public function image() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        switch ($data['act']) {
            case 'upload':
                
                    $id = $this->main->insert('product_image', array('product' => $data['product'],'image' => $data['image'], 'primary' => $data['primary']));
                    echo $id;
                    break;
                
               
                                   
                
            case 'delete':
                $this->main->delete('product_image', array('id' => $data['id']));
                $this->main->delete('product_option_image', array('product_image' => $data['id']));
                break;
            default:break;
        }
    }



      public function delete_merchant() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        // $id = decode($id);
        // log_message('error',$id);
        $data = $this->input->post(null, true);
        
        if ( $data['id']) {
            $this->main->delete('seo_url', array('query' => 'products/productv2/view/' . $data['id']));
            $this->main->delete('products', array('id' => $data['id']));
            $this->main->delete('product_image',array('product' => $data['id'])); 
        }
        else{
            return error;
        }
        echo $data['id'];
    }

     public function option($act) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        switch ($act) {
            case 'generate':
                if(empty($this->input->post('id'))){
                    $options = $this->input->post('option');
                    if ($options) {
                        $count_variants = 0;
                        $variant_name = array();

                        $previous_variation = $this->products->get_variation_previously($product);
                        foreach($options as $option => $option_value) {
                            $count_variants++;
                            array_push($variant_name,$option);       
                        }

                        if($previous_variation != $variant_name[0]){
                             if($count_variants == 1){
                                if($variant_name[0] == 'Warna'){
                                     $change_variation_check = $this->products->get_change_variation_check($product,'Ukuran');
                                     if($change_variation_check){
                                      
                                        $this->products->delete_variants($product);
                                     }
                                }
                                else if($variant_name[0] == 'Ukuran'){
                                     $change_variation_check = $this->products->get_change_variation_check($product,'Warna');
                                     if($change_variation_check){
                                      
                                        $this->products->delete_variants($product);
                                     }
                                }

                            }
                            else if($count_variants == 2){
                                $size_and_color_check = $this->products->duplicate_variation_check($product);   
                                if($size_and_color_check <= 0){
                                  
                                    $this->products->delete_variants($product);
                                }

                            }
                        }   

                        $options = array_filter($options);
                    
                        $opt = array();
                        $options = array_values($options);
                        $options = $this->_create_combination($options);
                        $ids = array();
                        foreach ($options as $option) {
                            if (!$this->_check_combination($product, $option)) {
                                $product_option = $this->main->insert('product_option', array('product' => $product));
                                array_push($ids, $product_option);
                                $product_option_combination = array();
                                foreach ($option as $opt) {
                                    array_push($product_option_combination, array('product_option' => $product_option, 'option' => $opt));
                                }
                                $this->db->insert_batch('product_option_combination', $product_option_combination);
                            }
                        }
                        // if ($ids) {
                            $options = $this->products->get_product_options($product, $ids);
                            echo json_encode(array('status' => 'success', 'data' => $options->result()));
                        // }
                    } else {
                        $return = array('message' => 'Variasi tidak ada.', 'status' => 'error');
                    }
                }
                else{
                    $options = $this->input->post('option');
                    $product = $this->input->post('id');
                    if ($options) {
                        $count_variants = 0;
                        $variant_name = array();
                
                        $previous_variation = $this->products->get_variation_previously($product);
                        foreach($options as $option => $option_value) {
                            $count_variants++;
                            array_push($variant_name,$option);       
                        }

                        if($previous_variation != $variant_name[0]){
                            if($count_variants == 1){
                                if($variant_name[0] == 'Warna'){
                                     $change_variation_check = $this->products->get_change_variation_check($product,'Ukuran');
                                     if($change_variation_check){
                                      
                                        $this->products->delete_variants($product);
                                     }
                                }
                                else if($variant_name[0] == 'Ukuran'){
                                     $change_variation_check = $this->products->get_change_variation_check($product,'Warna');
                                     if($change_variation_check){
                                      
                                        $this->products->delete_variants($product);
                                     }
                                }

                            }
                            else if($count_variants == 2){
                                $size_and_color_check = $this->products->duplicate_variation_check($product);   
                                if($size_and_color_check <= 0){
                                    $this->products->delete_variants($product);
                                }

                            }
                        }   

                        $options = array_filter($options);
                   
                        $opt = array();
                        $options = array_values($options);
                        $options = $this->_create_combination($options);
                        $ids = array();
                        foreach ($options as $option) {
                            if (!$this->_check_combination($product, $option)) {
                                $product_option = $this->main->insert('product_option', array('product' => $product));
                                array_push($ids, $product_option);
                                $product_option_combination = array();
                                foreach ($option as $opt) {
                                    array_push($product_option_combination, array('product_option' => $product_option, 'option' => $opt));
                                }
                                $this->db->insert_batch('product_option_combination', $product_option_combination);
                            }
                        }
                        // if ($ids) {
                            $options = $this->products->get_product_options($product);
                            echo json_encode(array('status' => 'success', 'data' => $options->result()));
                        // }
                    } else {
                        $return = array('message' => 'Variasi tidak ada.', 'status' => 'error');
                    }
                }
                break;
            case 'get_image':
                $product_option = $this->input->post('product_option');
                $product = $this->input->post('product');
                $images = $this->products->get_product_option_image($product, $product_option);
                echo json_encode($images->result());
                break;
            case 'save_image':
                $product_option = $this->input->post('product_option');
                $this->main->delete('product_option_image', array('product_option' => $product_option));
                $images = $this->input->post('images');
                $images = array_filter($images);
                foreach ($images as $image) {
                    $this->main->insert('product_option_image', array('product_option' => $product_option, 'product_image' => $image));
                }
                break;
            case 'delete':
                $product_option = $this->input->post('product_option');
                $this->main->delete('product_option', array('id' => $product_option));
                $this->main->delete('product_option_combination', array('product_option' => $product_option));
                $this->main->delete('product_option_image', array('product_option' => $product_option));
                break;
            default:break;
        }
    }


    private function _create_combination($options) {
        if (count($options) <= 1) {
            return count($options) ? array_map(function($v){return (array($v));}, $options[0]) : $options;
        }
        $res = array();
        $first = array_pop($options);
        foreach ($first as $option) {
            $tab = $this->_create_combination($options);
            foreach ($tab as $to_add) {
                $res[] = is_array($to_add) ? array_merge($to_add, array($option)) : array($to_add, $option);
            }
        }
        return $res;
    }

    private function _check_combination($product, $options) {
        $exists = array();
        $options_exist = $this->db->select('poc.*')
                ->join('product_option po', 'po.id = poc.product_option', 'left')
                ->where('po.product', $product)
                ->get('product_option_combination poc');
        if ($options_exist->num_rows() > 0) {
            foreach ($options_exist->result() as $oe) {
                $exists[$oe->product_option][] = $oe->option;
            }
        } else {
            return false;
        }
        foreach ($exists as $key => $oe) {
            if (count($oe) == count($options)) {
                $diff = false;
                for ($i = 0; isset($oe[$i]); $i++) {
                    if (!in_array($oe[$i], $options) || $key == false) {
                        $diff = true;
                    }
                }
                if (!$diff) {
                    return true;
                }
            }
        }
        return false;
    }

    public function get_table_description(){
        $id = $this->input->post('id');
        $data = $this->products->get_table_deskripsi($id)->result();
        echo json_encode($data);
    }

   

    public function activated($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('products', array('id' => $id));
        $update = $this->main->update('products', array('status' => 1), array('id' => $id));
        if ($update) {
            $return = array('message' => 'Produk ' . $data->name . ' berhasil di aktifasi.', 'status' => 'success');
        } else {
            $return = array('message' => 'Produk ' . $data->name . ' gagal di aktifasi.', 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function disabled($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('products', array('id' => $id));
        $update = $this->main->update('products', array('status' => 2), array('id' => $id));
        if ($update) {
            $return = array('message' => 'Produk ' . $data->name . ' berhasil di non-aktifkan.', 'status' => 'success');
        } else {
            $return = array('message' => 'Produk ' . $data->name . ' gagal di non-aktifkan.', 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function get_feature_values($feature, $id) {
        $feature = $this->main->get('features', array('id' => $feature));
        $products = $this->main->gets('feature_variant', array('feature' => $feature->id));
        if ($feature->type == 'i') {
            $output = '<input type="text" name="feature[' . $id . '][value]" class="form-control">';
        } else {
            if ($products) {
                if ($feature->type == 't') {
                    $output = '<textarea name="feature[' . $id . '][value]" class="form-control">';
                } elseif ($feature->type == 's') {
                    $output = '<select name="feature[' . $id . '][value]" class="form-control">';
                    foreach ($products->result() as $feature) {
                        $output .= '<option value="' . $feature->id . '">' . $feature->value . '</option>';
                    }
                    $output .= '</select>';
                } elseif ($feature->type == 'c') {
                    $output = '';
                    foreach ($products->result() as $feature) {
                        $output .= '<div class="checkbox"><label><input type="checkbox" name="feature[' . $id . '][value][]" value="' . $feature->id . '">' . $feature->value . '</label></div>';
                    }
                }
            } else {
                header('HTTP/1.1 500 Internal Server Error');
//            header('Content-Type: application/json; charset=UTF-8');
//            die(json_encode(array('message' => 'ERROR', 'code' => 1337)));
            }
        }
        echo $output;
    }

    public function get_feature_form() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        if ($category = $this->input->post('category')) {
            $data['product'] = ($this->input->post('product')) ? $this->input->post('product') : 0;
            // var_dump($this->input->post('product')); exit();
            $data['features'] = $this->products->get_features_by_category($category, $data['product']);

            // log_message('debug', $this->db->last_query());
            $this->load->view('feature_form', $data);
        } else {
            echo "";
        }
    }

    public function get_variation_form() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        if ($category = $this->input->post('category')) {
            // $data['product'] = ($this->input->post('product')) ? $this->input->post('product') : 0;
            // var_dump($this->input->post('product')); exit();
            $data['option_groups'] = $this->products->get_variations_by_category($category);

            // log_message('debug', $this->db->last_query());
            $this->load->view('variation_form', $data);
        } else {
            echo "";
        }
    }

    public function get_package_item() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $items = $this->products->get_package_item($this->input->get('id'),$this->input->get('query'));
        echo json_encode($items);
    }

    public function get_product_code() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $merchant = $this->main->get('merchants', array('id' => $this->input->post('merchant')));
        echo product_code($merchant->name, $merchant->id);
    }

    public function pack_product_search() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $results = array();

        $this->db->select('id, type, name')
                ->where('type !=', 'p')
                ->like('name', $this->input->post('search'));
        $products = $this->db->get('products');

        if ($products->num_rows() > 0) {
            foreach ($products->result() as $product) {
                if ($product->type == 'c') {
//                    $this->db->
                }
            }
        }
        $results = $this->products->pack_product_search($this->input->post('search'), $this->input->post('pack_list'));

        echo json_encode($results);
    }

    private function status_product($status) {

      
        switch ($status) {
            case 0: $text = '<i style="color:red;text-decoration:underline;">Aktifkan produk ini</i>';
                break;
            case 1: $text = '<i style="color:green;text-decoration:underline;">Aktif</i>';
                break;
        }
        return $text;
    }

    public function badword_check($str)
    {   
        $check = $this->main->get_bad($str);
        if ($check == FALSE)
        {
                return TRUE;
                
        }
        else
        {
                $this->form_validation->set_message('badword_check', 'Namakan produk anda dengan benar!');
                return FALSE;
        }
    }

    public function download_template_import(){
        $this->load->helper('download');
        force_download('../files/Template/Template_impor_produk_merchant.xlsx',NULL);
    }



    public function import()
    {
        $res = array('status' => 'error', 'message' => 'File Kosong');
        if (isset($_FILES['file']['name'])) {
            $this->load->library('upload');
            $config['upload_path'] = '../files/import/';
            $config['allowed_types'] = 'xlsx';
            $config['overwrite'] = TRUE;
            $this->upload->initialize($config);
            if ($this->upload->do_upload('file')) {
                $file = $this->upload->data();
                $input_file = '../files/import/' . $file['file_name'];
                $username = $this->products->get_username($this->data['user']->merchant);
                try {
                    $input_file_type = \PhpOffice\PhpSpreadsheet\IOFactory::identify($input_file);
                    $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($input_file_type);
                    $reader->setReadDataOnly(true);
                    $reader->setReadEmptyCells(false);
                    $spreadsheet = $reader->load($input_file);
                } catch(Exception $e) {
                    die('Error loading file "'.pathinfo($input_file,PATHINFO_BASENAME).'": '.$e->getMessage());
                }
                // $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('../files/import/' . $file['file_name']);
                $sheet = $spreadsheet->getSheet(0);

                //primitive file checker
                $data = $sheet->rangeToArray('A' . 6 . ':' . 'H'. 6, NULL, TRUE, FALSE);
                $file_check_count = 0;
                for($t=0;$t<=2;$t++){
                    switch ($t) {
                        case 0:
                            if($data[0][$t] == 'Kode Produk'){
                                $file_check_count++;
                            }
                            break;
                        case 1:
                            if($data[0][$t] == 'Nama Produk*'){
                                $file_check_count++;
                            }
                            break;
                        case 2:
                            if($data[0][$t] == 'Deskripsi Pendek*'){
                                $file_check_count++;
                            }
                            break;
                    }
                }
                if($file_check_count == 3){

                    for ($row = 7; $row <= $sheet->getHighestDataRow(); $row++) {
                        $data = $sheet->rangeToArray('A' . $row . ':' . 'S'. $row, NULL, TRUE, FALSE);
                        $brand_other = '';
                        $count_empty_cell = 0;
                        $res = array('status' => 'success', 'message' => 'Import berhasil disimpan');

                        //check if row is empty
                        for($j=0;$j<=18;$j++){  
                            if($data[0][$j] == '' || $data[0][$j] == NULL){
                                $count_empty_cell++;
                            }
                        }
                        if($count_empty_cell>=18) break;
                        else{
        
                            //check if empty cell
                            for($j=1;$j<=16;$j++){
                                if($j==3 || $j==4 || $j==6 || $j==11 || $j==14) continue;
                               
                                if($data[0][$j] == '' || $data[0][$j] == NULL){
                                    $res = array('status' => 'error', 'message' => 'Ada data wajib yang tidak terisi!');
                                    break 2;
                                }
                            }

                            if($data[0][15]=='AKTIF')
                                $status_produk=1;
                            else if ($data[0][15]=='NONAKTIF')
                                $status_produk=0;

                            if(!is_numeric($data[0][4])){
                                $brand_other = $data[0][4];
                                $data[0][4] = 0;
                            }


                            $product = array(
                                'store_type' => 'merchant',
                                'store_id' => $this->data['user']->merchant,
                                'meta_title'=> $data[0][1],
                                'meta_description' => $data[0][2],
                                'code' => $data[0][0],
                                'name' => $data[0][1],
                                'short_description' => $data[0][2],
                                'description' => $data[0][3],
                                'brand' => $data[0][4],
                                'category' => $data[0][5],
                                'etalase' => $data[0][6],
                                'weight' => $data[0][7],
                                'length' => $data[0][8],
                                'width' => $data[0][9],
                                'height' => $data[0][10],
                                'guarantee' => $data[0][11],
                                'price' => $data[0][12],
                                'quantity' => $data[0][13],
                                'meta_keyword' => $data[0][14],
                                'status' => $status_produk,
                                'brand_other' => $brand_other
                            );

                            $this->db->insert('products', $product);
                            $insert_id = $this->db->insert_id();

                            //insert seourl
                            $url = 'catalog/products/view/' . $insert_id .'/'. $this->data['user']->merchant;
                            $category_url = $this->products->get_category_seourl($product['category']);
                            $seo_url = strtolower($username->username . '/' . str_replace(' ', '-', $category_url) . '/' . str_replace(' ', '-', $product['name']));
                            $this->main->insert('seo_url', array('query' => $url, 'keyword' => $seo_url));


                            $primary = 1;
                            $count = 1;
                            for($i=16;$i<=18;$i++){
                                if($data[0][$i] == '' || $data[0][$i] == NULL) continue;
                                
                                $image_download = $this->file_get_contents_curl($data[0][$i]); 
                                $image_name = 'import_product_image_'.$insert_id.'_'.$count.'.jpg';
                                $folder_name = '../files/images/merchant/' . $username->username . '/' . $image_name;
                                  
                                file_put_contents($folder_name, $image_download ); 

                                $product_image = array(
                                    'product' => $insert_id,
                                    'image' => 'merchant/'.$username->username.'/'.$image_name,
                                    'primary' => $primary

                                );
                                $this->db->insert('product_image',$product_image);

                                $primary = 0;
                                $count++;
                            }
                        }
                    }
                } else{
                    $res = array('status' => 'error', 'message' => 'Gunakan File Template!');
                }
                
            } else {
                $res['message'] = $this->upload->display_errors();
            }
        }
        echo json_encode($res);
    }

    public function file_get_contents_curl($url) { 
    $ch = curl_init(); 
  
    curl_setopt($ch, CURLOPT_HEADER, 0); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    curl_setopt($ch, CURLOPT_URL, $url); 
  
    $data = curl_exec($ch); 
    curl_close($ch); 
  
    return $data; 
    } 

    public function export() {
        $filename = "merchant_product_export";
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        foreach(range('A','O') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }
        \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
        $sheet->fromArray(array('Kode Produk', 'Nama','Tipe','Deskripsi Pendek','Deskripsi','Brand', 'Kategori','Etalase','Berat','Panjang','Lebar','Tinggi','Harga Jual','Tanggal', 'Stok'), NULL, 'A1');
        $fullFileName = "${filename}_".date('Y-m-d');
        $datas = $this->products->export_data($this->data['user']->merchant);
        $i = 2;
        if($datas){
            if ($datas->num_rows() > 0) {
                foreach ($datas->result() as $data) {
                    $row_data = array(
                        $data->code,
                        $data->name,
                        $data->type,
                        $data->short_description,
                        $data->description,
                        $data->brand_name,
                        $data->category_name,
                        $data->etalase_name,
                        $data->weight,
                        $data->length,
                        $data->width,
                        $data->height,
                        number_format($data->price,2,',','.'),
                        $data->date_added,
                        $data->quantity
                    );
                    $sheet->fromArray($row_data, null, 'A' . $i);
                    $i++;
                }
            }
        }
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fullFileName . '.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        ob_end_clean();
        $writer->save('php://output');
    }

    public function download_lists() {
        $filename = "lists_merchant_import";
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('List Kategori');
        $sheet->getColumnDimension('B')->setAutoSize(true);
        \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
        $sheet->fromArray(array('ID','Kategori'), NULL, 'A1');
        $fullFileName = "${filename}_".date('Y-m-d');
        $data_categories = $this->main->gets('categories',[],'id ASC');
        $i = 2;
        if ($data_categories->num_rows() > 0) {
            foreach ($data_categories->result() as $data_category) {
                $row_data = array(
                    $data_category->id,
                    $data_category->name
                    
                );
                $sheet->fromArray($row_data, null, 'A' . $i);
                $i++;
            }
        };
        $spreadsheet->createSheet(1);
        $spreadsheet->setActiveSheetIndex(1);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('List Brand');
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->fromArray(array('ID','Brand'), NULL, 'A1');
        $data_brands = $this->main->gets('brands',[],'id ASC');
        $i = 2;
        if ($data_brands->num_rows() > 0) {
            foreach ($data_brands->result() as $data_brand) {
                $row_data = array(
                    $data_brand->id,
                    $data_brand->name
                    
                );
                $sheet->fromArray($row_data, null, 'A' . $i);
                $i++;
            }
        };
        $spreadsheet->createSheet(2);
        $spreadsheet->setActiveSheetIndex(2);
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('List Etalase');
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->fromArray(array('ID','Etalase'), NULL, 'A1');
        $data_etalases = $this->main->gets('etalase',['store_id' => $this->data['user']->merchant],'id ASC');
        $i = 2;
        if($data_etalases){
            if ($data_etalases->num_rows() > 0) {
                foreach ($data_etalases->result() as $data_etalase) {
                    $row_data = array(
                        $data_etalase->id,
                        $data_etalase->name
                        
                    );
                    $sheet->fromArray($row_data, null, 'A' . $i);
                    $i++;
                }
            }
        }


        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fullFileName . '.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        ob_end_clean();
        $writer->save('php://output');
    }

}
