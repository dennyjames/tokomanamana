<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Products extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->lang->load('products', settings('language'));
        $this->load->model('products_model', 'products');

        $this->data['menu'] = 'product';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();
        $this->breadcrumbs->push(lang('menu_product'), '/products');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('product_heading'));
        $this->load->view('products', $this->data);
    }

    public function add() {
        $this->template->_init();
        $this->template->form();
        $this->load->js('../assets/backend/js/merchant/modules/products/product_add.js');

        $this->breadcrumbs->unshift(lang('menu_catalog'), '/');
        $this->breadcrumbs->push(lang('menu_product'), '/products');
        $this->breadcrumbs->push(lang('product_add_heading'), '/products/add');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('product_add_heading'));
        $this->load->view('products/product_add', $this->data);
    }

    public function form_tanaka($product = '', $id = '') {
        $product = decode($product) or redirect('products/add');
        $this->data['product'] = $this->main->get('products', array('id' => $product, 'merchant' => 0)) or show_404();
        if (!$id && $check_exist = $this->main->get('product_merchant', array('product' => $product, 'merchant' => $this->data['user']->merchant))) {
            $id = encode($check_exist->id);
        }

        $this->template->_init();
        $this->template->form();
        $this->load->js('../assets/backend/js/merchant/modules/products/product_form_tanaka.js');

        $this->data['data'] = array();

        $this->breadcrumbs->push(lang('menu_product'), '/products');
        if ($id) {
            $this->data['data'] = $this->main->get('product_merchant', array('id' => decode($id), 'merchant' => $this->data['user']->merchant));
            $this->breadcrumbs->push(lang('product_edit_heading'), '/products/form_tanaka/' . encode($product) . '/' . encode($id));
        } else {
            $this->breadcrumbs->push(lang('product_add_heading'), '/products/form_tanaka/' . encode($product));
        }
        $this->breadcrumbs->push($this->data['product']->name, '/');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(($this->data['data']) ? lang('product_edit_heading') : lang('product_add_heading'));
        $this->load->view('products/product_form_tanaka', $this->data);
    }

    public function save_tanaka() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('discount', 'lang:product_form_discount_label', 'trim|numeric');
        $this->form_validation->set_rules('price', 'lang:product_form_price_label', 'trim|required|numeric');
        $this->form_validation->set_rules('weight', 'lang:product_form_weight_label', 'trim|required|numeric');
        $this->form_validation->set_rules('quantity', 'lang:product_form_quantity_label', 'trim|required|numeric');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            do {
                $data['price'] = parse_number($data['price']);
                $data['quantity'] = parse_number($data['quantity']);
                $data['price_old'] = $data['price'];
                if ($data['discount'] > 0) {
                    $data['price_old'] = $data['price'];
                    $data['price'] = $data['price'] - ($data['price'] * $data['discount'] / 100);
                }
                if ($data['id']) {
                    $data['id'] = decode($data['id']);
                    $this->main->update('product_merchant', $data, array('id' => $data['id']));
                    $id = $data['id'];
                } else {
                    $data['merchant'] = $this->data['user']->merchant;
                    $id = $this->main->insert('product_merchant', $data);
                }
                $return = array('message' => 'Produk berhasil disimpan.', 'status' => 'success', 'redirect' => site_url('products'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->load->js('../assets/backend/js/plugins/forms/dropzone.min.js');
        $this->load->js('../assets/backend/js/merchant/modules/products/product_form.js');

        $this->data['data'] = array();

        $this->data['brands'] = $this->main->gets('brands', array(), 'name ASC');
        $this->data['categories'] = $this->main->gets('categories', array('parent' => 0, 'active' => 1), 'sort_order ASC');

        $this->breadcrumbs->unshift(lang('menu_catalog'), '/');
        $this->breadcrumbs->push(lang('menu_product'), '/products');

        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->main->get('products', array('id' => $id, 'merchant' => $this->data['user']->merchant))or show_404();
            $this->data['data_images'] = $this->main->gets('product_image', array('product' => $id));
            $this->data['data_features'] = $this->products->get_product_features($id);

            $this->breadcrumbs->push(lang('product_edit_heading'), '/products/form/' . encode($id));
            $this->breadcrumbs->push($this->data['data']->name, '/');
        } else {
            $this->breadcrumbs->push(lang('product_add_heading'), '/products/form');
        }

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(($this->data['data']) ? lang('product_edit_heading') : lang('product_add_heading'));
        $this->load->view('products/product_form', $this->data);
    }

    public function view($id) {
        $this->template->_init();
        $this->load->js('../assets/backend/js/modules/catalog/product_view.js');

        $id = decode($id);
        $this->data['product'] = $this->main->get('products', array('id' => $id));
        $this->data['product_images'] = $this->main->gets('product_image', array('product' => $id));
        $this->data['product_features'] = $this->products->get_product_feature_by_category($this->data['product']->category, $id);
        $this->data['brand'] = $this->main->get('brands', array('id' => $this->data['product']->brand));
        $this->data['category'] = $this->products->get_category($this->data['product']->category);
        $this->data['merchant'] = $this->main->get('merchants', array('id' => $this->data['product']->merchant));

        $this->breadcrumbs->unshift(lang('menu_catalog'), '/');
        $this->breadcrumbs->push(lang('menu_product'), '/products');
        $this->breadcrumbs->push($this->data['product']->name, '/products/view/' . encode($id));

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title('Produk ' . $this->data['product']->name);
        $this->load->view('product_view', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->products->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->code,
                    $data->name,
                    $data->category,
                    number($data->price_old),
                    number($data->price) . '<small class="display-block text-muted">Diskon ' . number($data->discount) . '%</small>',
                    number($data->quantity),
                    $this->_status_product($data->status),
                    ($data->status == 2) ? '' :
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                    '<li><a href="' . site_url('products/view/' . encode($data->id)) . '">' . lang('button_view') . '</a></li>' .
                    '<li><a href="' . site_url((($data->pm != 'pm') ? 'products/form_tanaka/' . encode($data->id) . '/' . encode($data->pm) : 'products/form/' . encode($data->id))) . '">' . lang('button_edit') . '</a></li>' .
                    '<li><a href="' . site_url('products/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>' .
                    '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->products->count_all();
        $output['recordsFiltered'] = $this->products->count_all($search);
        echo json_encode($output);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');

        $code_check = '';
        if ($this->input->post('id')) {
            $code = $this->main->get('products', array('id' => decode($this->input->post('id'))))->code;
            if ($code != $this->input->post('code')) {
                $code_check = '|is_unique[products.code]';
            }
        }
        $this->form_validation->set_rules('code', 'lang:product_form_code_label', 'trim|required|alpha_dash' . $code_check);
        $this->form_validation->set_rules('barcode', 'lang:product_form_barcode_label', 'trim|alpha_numeric');
        $this->form_validation->set_rules('discount', 'lang:product_form_discount_label', 'trim|numeric');
        $this->form_validation->set_rules('price', 'lang:product_form_price_label', 'trim|required|numeric');
        $this->form_validation->set_rules('weight', 'lang:product_form_weight_label', 'trim|required|numeric');
        $this->form_validation->set_rules('name', 'lang:product_form_name_label', 'trim|required');
        $this->form_validation->set_rules('short_description', 'lang:product_form_short_description_label', 'trim|required');
        $this->form_validation->set_rules('description', 'lang:product_form_description_label', 'trim');
        $this->form_validation->set_rules('category', 'lang:product_form_category_label', 'trim|required');
        $this->form_validation->set_rules('brand', 'lang:product_form_brand_label', 'trim');
        $this->form_validation->set_rules('image_primary', 'lang:product_form_image_label', 'trim');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            do {
                $features = array();
                if (isset($data['feature'])) {
                    $features = $data['feature'];
                    unset($data['feature']);
                }
                if (isset($data['image_primary'])) {
                    $image_primary = $data['image_primary'];
                    unset($data['image_primary']);
                } else {
                    $return = array('message' => 'Gambar kosong! Setidaknya pilih satu gambar.', 'status' => 'error');
                    break;
                }

                $data['price'] = parse_number($data['price']);
                $data['price_old'] = $data['price'];
                if ($data['discount'] > 0) {
                    $data['price_old'] = $data['price'];
                    $data['price'] = $data['price'] - ($data['price'] * $data['discount'] / 100);
                }

                $id_temp = $data['id_temp'];
                unset($data['id_temp']);
                unset($data['category1']);
                unset($data['category2']);

                if ($data['id']) {
                    $data['id'] = decode($data['id']);
                    $this->db->delete('product_image', array('product' => $data['id']));
                    $this->db->delete('product_feature', array('product' => $data['id']));
                    $this->main->delete('seo_url', array('query' => 'products/view/' . $data['id']));
                    $this->main->update('products', $data, array('id' => $data['id']));
                    $id = $data['id'];
                } else {
                    $data['status'] = 2;
                    $data['merchant'] = $this->data['user']->merchant;
                    $id = $this->main->insert('products', $data);
                }

                $data_products = array();
                if (($features) && count($features) > 0) {
//                    log_message('debug', print_r($features));
                    foreach ($features as $feature_id => $feature) {
//                        echo $feature_id;
                        if ($feature['value']) {
//                            log_message('debug', $feature['value']);
                            if (is_array($feature['value'])) {
                                if (count($feature['value']) > 0) {
                                    foreach ($feature['value'] as $variant) {
                                        $data_products[] = array(
                                            'product' => $id,
                                            'feature' => $feature['id'],
                                            'value' => $variant
                                        );
                                    }
                                }
                            } else {
                                $data_products[] = array(
                                    'product' => $id,
                                    'feature' => $feature_id,
                                    'value' => $feature['value']
                                );
                            }
                        }
                    }
                }
                if ($data_products) {
                    $this->db->insert_batch('product_feature', $data_products);
                }

                $slug = create_url($data['name'], $data['merchant']);
                $this->main->insert('seo_url', array('query' => 'catalog/products/view/' . $id, 'keyword' => $slug));

                $this->main->update('product_image', array('product' => $id), array('product_id_temp' => $id_temp));
                log_message('debug', $this->db->last_query());
                $return = array('message' => sprintf(lang('product_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('products'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function image_upload() {
        $this->load->library('upload');
        $config['upload_path'] = '../files/images/' . $this->data['user']->merchant_username;
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = '500';
        $config['overwrite'] = FALSE;
//        $config['encrypt_name'] = TRUE;
        $config['file_name'] = url_title(strtolower($this->input->post('name')));

        if (!file_exists($config['upload_path'])) {
            mkdir($config['upload_path']);
        }

        $files = $_FILES;
//        print_r($files);
        $cpt = count($_FILES['file']['name']);
        $images = array();
        $primary = 0;
        for ($i = 0; $i < $cpt; $i++) {
            $_FILES['file']['name'] = $files['file']['name'][$i];
            $_FILES['file']['type'] = $files['file']['type'][$i];
            $_FILES['file']['tmp_name'] = $files['file']['tmp_name'][$i];
            $_FILES['file']['error'] = $files['file']['error'][$i];
            $_FILES['file']['size'] = $files['file']['size'][$i];

            $this->upload->initialize($config);
//            $this->upload->do_upload('file');
//            $images[] = $this->upload->data();
            if ($this->upload->do_upload('file')) {
                $image = $this->upload->data();
                $data = array('product_id_temp' => $this->input->post('id_temp'), 'image' => $this->data['user']->merchant_username . '/' . $image['file_name']);
                if ($primary == 0) {
                    if ($files['file']['name'][$i] == $this->input->post('primary')) {
                        $primary = 1;
                        $data['primary'] = 1;
                    }
                }
                $this->main->insert('product_image', $data);
            }
//                echo $this->upload->display_errors();
//            } else {
//                echo 'oke';
//            }
        }
//        if ($images) {
//            foreach ($images as $image) {
//                $this->main->insert('product_image', array('));
//            }
//        }
//        $data = array(
//            'name' => $this->input->post('pd_name'),
//            'prod_image' => $dataInfo[0]['file_name'],
//            'prod_image1' => $dataInfo[1]['file_name'],
//            'prod_image2' => $dataInfo[2]['file_name'],
//            'created_time' => date('Y-m-d H:i:s')
//        );
//        $result_set = $this->tbl_products_model->insertUser($data);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('products', array('id' => $id));
        $delete = $this->main->delete('products', array('id' => $id));
        if ($delete) {
            $this->main->delete('seo_url', array('query' => 'catalog/products/view/' . $id));
            $return = array('message' => sprintf(lang('product_delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => sprintf(lang('product_delete_error_message'), $data->name), 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function get_feature_values($feature, $id) {
        $feature = $this->main->get('features', array('id' => $feature));
        $products = $this->main->gets('feature_variant', array('feature' => $feature->id));
        if ($feature->type == 'i') {
            $output = '<input type = "text" name = "feature[' . $id . '][value]" class = "form-control">';
        } else {
            if ($products) {
                if ($feature->type == 't') {
                    $output = '<textarea name = "feature[' . $id . '][value]" class = "form-control">';
                } elseif ($feature->type == 's') {
                    $output = '<select name = "feature[' . $id . '][value]" class = "form-control">';
                    foreach ($products->result() as $feature) {
                        $output .= '<option value = "' . $feature->id . '">' . $feature->value . '</option>';
                    }
                    $output .= '</select>';
                } elseif ($feature->type == 'c') {
                    $output = '';
                    foreach ($products->result() as $feature) {
                        $output .= '<div class = "checkbox"><label><input type = "checkbox" name = "feature[' . $id . '][value][]" value = "' . $feature->id . '">' . $feature->value . '</label></div>';
                    }
                }
            } else {
                header('HTTP/1.1 500 Internal Server Error');
//            header('Content-Type: application / json;charset = UTF-8');
//            die(json_encode(array('message' => 'ERROR', 'code' => 1337)));
            }
        }
        echo $output;
    }

    public function get_child_category() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $results = array();

        if ($this->input->post('parent') > 0) {
            $this->db->select('id, name')
                    ->where('parent', $this->input->post('parent'))
                    ->where('active', 1);
            $categories = $this->db->get('categories');

            $list = '';
            if ($categories->num_rows() > 0) {
                foreach ($categories->result() as $category) {
                    $list .= '<option value = "' . $category->id . '">' . $category->name . '</option>';
                }
                $results['list'] = $list;
                $results['status'] = 'success';
            } else {
                $results['status'] = 'failed';
            }
        } else {
            $results['status'] = 'failed';
        }

        echo json_encode($results);
    }

    public function get_feature_form() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        if ($category = $this->input->post('category')) {
            $data['product'] = ($this->input->post('product')) ? decode($this->input->post('product')) : 0;
            $data['features'] = $this->products->get_features_by_category($category, $data['product']);
            log_message('debug', $this->db->last_query());
            $this->load->view('feature_form', $data);
        } else {
            echo "";
        }
    }

    private function _status_product($status) {
        switch ($status) {
            case 0: $text = '<span class="text-danger">Tidak aktif</span>';
                break;
            case 1: $text = 'Aktif';
                break;
            case 2: $text = '<span class="text-info">Proses Validasi</span>';
        }
        return $text;
    }

    public function search_products($text, $page) {
        $products = $this->products->search($page, $text);
        $return['status'] = 'null';
        if ($products->num_rows() > 0) {
            $output = '';
            $return['status'] = 'success';
            $return['page_max'] = ceil($this->products->search($page, $text, true) / 9);
            $return['page'] = ($return['page_max'] <= $page) ? $page : $page + 1;
            foreach ($products->result() as $product) {
                $output .= '<div class="col-lg-4 col-sm-6">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="thumb thumb-fixed">
                                    <a href="' . site_url('../files/images/' . $product->image) . '" data-popup="lightbox">
                                        <img src="' . site_url('../files/images/' . $product->image) . '">
                                    </a>
                                </div>
                            </div>

                            <div class="panel-body panel-body-accent text-center">
                                <h6 class="text-semibold no-margin"><a href="#" class="text-default">' . $product->name . '</a></h6>
                                <ul class="list-inline list-inline-separate mb-10">
                                    <li><a href="#" class="text-muted">' . $product->code . '</a></li>
                                </ul>
                                <div class="col-sm-12">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <a href="' . site_url('products/form_tanaka/' . encode($product->id)) . '" class="btn btn-primary">Jual Produk Ini</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
            }
            $return['content'] = $output;
        }
        echo json_encode($return);
    }

}
