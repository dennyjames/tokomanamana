<?php

defined('BASEPATH') or exit('No direct script access allowed!');
require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Principal extends CI_Controller {

    public function __construct() {
        parent::__construct();

        // if (!$this->ion_auth->in_group(4))
        // show_404();
        
        $this->lang->load('products_principal', settings('language'));
        $this->load->model('principal_model', 'products');
        $this->data['menu'] = 'product_principal';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();
        $this->template->form();
        $this->load->js('../assets/backend/js/merchant/modules/products/tanaka.js');

        $this->breadcrumbs->unshift(lang('menu_product'), '/products');
        $this->breadcrumbs->push(lang('menu_product_principal'), '/products/principal');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('product_heading'));
        $this->load->view('principal/list', $this->data);
    }

    public function view($id) {
        $this->load->model('catalog/categories_model', 'categories');

        $this->template->_init();
        $this->load->js('../assets/backend/js/modules/catalog/product_view.js');

        $id = decode($id);
        $this->data['product'] = $this->products->get($id);
        $this->data['product_images'] = $this->main->gets('product_image', array('product' => $id));
        $this->data['product_features'] = $this->products->get_product_feature_by_category($this->data['product']->category, $id);
        $this->data['brand'] = $this->main->get('brands', array('id' => $this->data['product']->brand));
        $this->data['category'] = $this->categories->get($this->data['product']->category);
        $this->data['merchant'] = $this->main->get('merchants', array('id' => $this->data['product']->merchant));

        $this->breadcrumbs->unshift(lang('menu_catalog'), '/');
        $this->breadcrumbs->push(lang('menu_product'), '/catalog/products');
        $this->breadcrumbs->push($this->data['product']->name, '/catalog/products/view/' . encode($id));

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title('Produk ' . $this->data['product']->name);
        $this->load->view('products/product_view', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->products->get_all($this->data['user']->merchant, $this->data['user']->merchant_group, $start, $length, $search, $order);
        // var_dump($this->db->last_query()); die;
        if ($datas) {
            foreach ($datas->result() as $data) {
                $row_data = array(
                    '<a href="#"><img src="' . site_url('../files/images/' . $data->image) . '" height="60" class=""></a>',
                    // '<a href="' . site_url('catalog/products/view/' . encode($data->id)) . '">' . $data->code . '</a>',
                    $data->code,
                    '<a href="' . site_url('../' . strtolower($this->data['user']->username . '/' . str_replace(' ', '-', $data->category) . '/'. str_replace(' ', '-', $data->name))) . '">' .  $data->name . '</a>',
                    ($data->variation && $data->id_option) ? $this->get_variation($data->id, $data->id_option) : '-',
                    'Rp. ' . number($data->price),
                    $data->category,
                    $data->date_added,
                );
                //if ($this->ion_auth->in_group(3)) {
                    // $row_data[] = '<input type="number" min="0" data-product="' . $data->id . '" data-id="' . $data->product_merchant . '" class="quantity form-control" value="' . $data->quantity . '">';
                    $row_data[] = $data->quantity;
                    // $pricing_level = $this->products->price_level_group($data->id, $this->data['user']->merchant_group);
                    // $input_pricing_level = '';
                    // if ($pricing_level->num_rows()>0) {
                    //     $html_pricing_level = '<table class=\'table\'>
                    //                         <thead>
                    //                         <tr><th style=\'padding:4px\'>Jumlah</th><th style=\'padding:4px\'>Harga / unit</th></tr>
                    //                         </thead><tbody>';
                    //     foreach ($pricing_level->result() as $row => $pl) {
                    //         $html_pricing_level .= '<tr><td style=\'padding:4px\'>';
                    //         if (($row + 1) == $pricing_level->num_rows()) {
                    //             $html_pricing_level .= '>= ' . $pl->min_qty;
                    //         } else {
                    //             $html_pricing_level .= $pl->min_qty . ' - ' . ($pricing_level->row($row + 1)->min_qty - 1);
                    //         }
                    //         $html_pricing_level .= '</td><td style=\'padding:4px\'>' . rupiah($pl->price) . '</td></tr>';
                    //     }
                    //     $html_pricing_level .= '</tbody></table>';
                    //     $input_pricing_level = '<input type="checkbox" class="pricing_level" data-id="' . $data->product_merchant . '" ' . ($data->pricing_level == 1 ? 'checked' : '') . ' ><br><a href="javascript:void(0);" data-popup="popover" data-content="' . $html_pricing_level . '" data-placement="bottom" data-html="true">Lihat harga grosir</a>';
                    // }
                    // $row_data[] = $input_pricing_level;
                //}
                $output['data'][] = $row_data;
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->products->count_all($this->data['user']->merchant, $this->data['user']->merchant_group);
        $output['recordsFiltered'] = $this->products->count_all($this->data['user']->merchant, $this->data['user']->merchant_group, $search);
        echo json_encode($output);
    }

    private function get_variation($product, $option) {
        $product_variations = $this->products->get_variation($product, $option);
        $return = '';
        foreach($product_variations->result() as $variation) {
            $return .= $variation->type . ' ' . $variation->value . ' ';
        }
        return $return;
    }

    public function update_quantity() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $data['merchant'] = $this->data['user']->merchant;
        if ($data['id']) {
            $this->main->update('product_merchant', $data, array('id' => $data['id']));
        } else {
            $data['id'] = $this->main->insert('product_merchant', $data);
        }
        echo $data['id'];
    }

    public function pricing_level() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $this->main->update('product_merchant', array('pricing_level' => $data['pricing_level']), array('id' => $data['id']));
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');

        $code_check = '';
        if ($this->input->post('id')) {
            $code = $this->main->get('products', array('id' => decode($this->input->post('id'))))->code;
            if ($code != $this->input->post('code')) {
                $code_check = '|is_unique[products.code]';
            }
        }
        $this->form_validation->set_rules('code', 'lang:product_form_code_label', 'trim|required|alpha_dash' . $code_check);
        $this->form_validation->set_rules('price', 'lang:product_form_price_label', 'trim|required|numeric');
        $this->form_validation->set_rules('weight', 'lang:product_form_weight_label', 'trim|required|numeric');
        $this->form_validation->set_rules('name', 'lang:product_form_name_label', 'trim|required');
        $this->form_validation->set_rules('short_description', 'lang:product_form_short_description_label', 'trim|required');
        $this->form_validation->set_rules('category', 'lang:product_form_category_label', 'trim|required');
        $this->form_validation->set_rules('seo_url', 'lang:product_form_seo_url_label', 'trim|seo_url');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            do {
                if ($data['id'])
                    $data['id'] = decode($data['id']);

                $features = array();
                $images = array();
                $prices = array();
                if (isset($data['prices'])) {
                    $prices = $data['prices'];
                    unset($data['prices']);
                }
                if (isset($data['feature'])) {
                    $features = array_filter($data['feature']);
                    unset($data['feature']);
                }
                if (isset($data['images'])) {
                    $images = array_filter($data['images']);
                    $image_primary = $data['image_primary'];
                    unset($data['images']);
                    unset($data['image_primary']);
                } else {
                    $return = array('message' => lang('product_empty_image_message'), 'status' => 'error');
                    break;
                }
                $url = 'catalog/products/view/' . $data['id'];
                if ($seo_url = $data['seo_url']) {
                    if (check_url($seo_url, $url)) {
                        $return = array('message' => lang('product_friendly_url_exist_message'), 'status' => 'error');
                        break;
                    }
                } else {
                    $seo_url = $url;
                }
                unset($data['seo_url']);

                $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;
                $data['description'] = tinymce_parse_url_files($data['description']);
                $data['price'] = parse_number($data['price']);

                if ($data['id']) {
                    $this->db->delete('product_image', array('product' => $data['id']));
                    $this->db->delete('product_feature', array('product' => $data['id']));
                    $this->db->delete('product_price', array('product' => $data['id']));
                    $this->main->delete('seo_url', array('query' => $url));
                    $this->main->update('products', $data, array('id' => $data['id']));
                    $id = $data['id'];
                } else {
                    $id = $this->main->insert('products', $data);
                    $url .= $id;
                    if (!$this->input->post('seo_url')) {
                        $seo_url .= $id;
                    }
                }
                $this->main->insert('seo_url', array('query' => $url, 'keyword' => $seo_url));

                $data_images = array();
                if (count($images) > 0) {
                    foreach ($images as $key => $image) {
                        $data_images[] = array(
                            'product' => $id,
                            'image' => $image,
                            'primary' => ($key == $image_primary) ? 1 : 0
                        );
                    }
                    $this->db->insert_batch('product_image', $data_images);
                }

                $data_products = array();
                if (count($features) > 0) {
                    foreach ($features as $feature_id => $feature) {
                        if ($feature['value']) {
                            if (is_array($feature['value'])) {
                                if (count($feature['value']) > 0) {
                                    foreach ($feature['value'] as $variant) {
                                        $data_products[] = array(
                                            'product' => $id,
                                            'feature' => $feature['id'],
                                            'value' => $variant
                                        );
                                    }
                                }
                            } else {
                                $data_products[] = array(
                                    'product' => $id,
                                    'feature' => $feature_id,
                                    'value' => $feature['value']
                                );
                            }
                        }
                    }
                    if ($data_products)
                        $this->db->insert_batch('product_feature', $data_products);
                }
                $data_prices = array();
                if (count($prices) > 0) {
                    foreach ($prices as $merchant_group => $price) {
                        if ($price['type'] == 'P') {
                            $data_prices[] = array(
                                'product' => $id,
                                'type' => $price['type'],
                                'value' => $price['value'],
                                'merchant_group' => $merchant_group,
                                'price' => $data['price'] + ($data['price'] * $price['value'] / 100)
                            );
                        } else {
                            $data_prices[] = array(
                                'product' => $id,
                                'type' => $price['type'],
                                'value' => $price['value'],
                                'merchant_group' => $merchant_group,
                                'price' => $data['price'] + $price['value']
                            );
                        }
                    }
                    if ($data_prices)
                        $this->db->insert_batch('product_price', $data_prices);
                }

                $return = array('message' => sprintf(lang('product_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('catalog/products'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('products', array('id' => $id));
        $delete = $this->main->delete('products', array('id' => $id));
        if ($delete) {
            $this->main->delete('seo_url', array('query' => 'catalog/products/view/' . $id));
            $return = array('message' => sprintf(lang('product_delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => sprintf(lang('product_delete_error_message'), $data->name), 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function activated($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('products', array('id' => $id));
        $update = $this->main->update('products', array('status' => 1), array('id' => $id));
        if ($update) {
            $return = array('message' => 'Produk ' . $data->name . ' berhasil di aktifasi.', 'status' => 'success');
        } else {
            $return = array('message' => 'Produk ' . $data->name . ' gagal di aktifasi.', 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function disabled($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('products', array('id' => $id));
        $update = $this->main->update('products', array('status' => 2), array('id' => $id));
        if ($update) {
            $return = array('message' => 'Produk ' . $data->name . ' berhasil di non-aktifkan.', 'status' => 'success');
        } else {
            $return = array('message' => 'Produk ' . $data->name . ' gagal di non-aktifkan.', 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function get_feature_values($feature, $id) {
        $feature = $this->main->get('features', array('id' => $feature));
        $products = $this->main->gets('feature_variant', array('feature' => $feature->id));
        if ($feature->type == 'i') {
            $output = '<input type="text" name="feature[' . $id . '][value]" class="form-control">';
        } else {
            if ($products) {
                if ($feature->type == 't') {
                    $output = '<textarea name="feature[' . $id . '][value]" class="form-control">';
                } elseif ($feature->type == 's') {
                    $output = '<select name="feature[' . $id . '][value]" class="form-control">';
                    foreach ($products->result() as $feature) {
                        $output .= '<option value="' . $feature->id . '">' . $feature->value . '</option>';
                    }
                    $output .= '</select>';
                } elseif ($feature->type == 'c') {
                    $output = '';
                    foreach ($products->result() as $feature) {
                        $output .= '<div class="checkbox"><label><input type="checkbox" name="feature[' . $id . '][value][]" value="' . $feature->id . '">' . $feature->value . '</label></div>';
                    }
                }
            } else {
                header('HTTP/1.1 500 Internal Server Error');
//            header('Content-Type: application/json; charset=UTF-8');
//            die(json_encode(array('message' => 'ERROR', 'code' => 1337)));
            }
        }
        echo $output;
    }

    public function get_feature_form() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        if ($category = $this->input->post('category')) {
            $data['product'] = ($this->input->post('product')) ? decode($this->input->post('product')) : 0;
            $data['features'] = $this->products->get_features_by_category($category, $data['product']);
//            log_message('debug', $this->db->last_query());
            $this->load->view('products/feature_form', $data);
        } else {
            echo "";
        }
    }

    public function get_product_code() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $merchant = $this->main->get('merchants', array('id' => $this->input->post('merchant')));
        echo product_code($merchant->name, $merchant->id);
    }

    public function pack_product_search() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $results = array();

        $this->db->select('id, type, name')
                ->where('type !=', 'p')
                ->like('name', $this->input->post('search'));
        $products = $this->db->get('products');

        if ($products->num_rows() > 0) {
            foreach ($products->result() as $product) {
                if ($product->type == 'c') {
//                    $this->db->
                }
            }
        }
        $results = $this->products->pack_product_search($this->input->post('search'), $this->input->post('pack_list'));

        echo json_encode($results);
    }

    private function _status_product($status) {
        switch ($status) {
            case 0: $text = '<i class="icon-cross2 text-danger-400"></i>';
                break;
            case 1: $text = '<i class="icon-checkmark3 text-success"></i>';
                break;
            case 2: $text = '<i class="icon-cross2 text-danger-400"></i>';
        }
        return $text;
    }

    public function export() {
        $filename = "product_stock";
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
        $sheet->fromArray(array('SKU', 'Nama', 'Harga Jual', 'Kategori', 'Stok'));
        $fullFileName = "${filename}_".date('Y-m-d');
        $datas = $this->products->export_data($this->data['user']->merchant,$this->data['user']->merchant_group);
        $i = 2;
        if ($datas->num_rows() > 0) {
            foreach ($datas->result() as $data) {
                $row_data = array(
                    $data->code,
                    $data->name,
                    number($data->price),
                    $data->category,
                    $data->quantity
                );
                $sheet->fromArray($row_data, null, 'A' . $i);
                $i++;
            }
        };
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fullFileName . '.xlsx"');
        header('Cache-Control: max-age=0');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
    }

}
