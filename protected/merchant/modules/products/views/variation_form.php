 <?php if ($option_groups->num_rows() > 0) { ?>
    <?php foreach ($option_groups->result() as $og) { ?>
        <div class="panel panel-white">
            <div class="panel-heading">
                <h6 class="panel-title">
                    <a data-toggle="collapse" href="#option-group-<?php echo $og->id; ?>" aria-expanded="true" class=""><?php echo $og->name; ?></a>
                </h6>
            </div>
            <div id="option-group-<?php echo $og->id; ?>" class="panel-collapse collapse in" aria-expanded="true" style="">
                <div class="panel-body">
                    <?php if ($options = $this->main->gets('options', array('group' => $og->id), 'sort_order')) { ?>
                        <?php foreach ($options->result() as $option) { 
                           if($og->name == 'Warna'): ?>
                                <div class="checkbox">
                                    <label>
                                        <div style="width: 10px;height: 10px;border-radius: 100%;border: 0.5px solid #959595;display: inherit;background-color:<?= $option->color;?>"></div>
                                        <input type="checkbox" name="option[<?php echo $og->id; ?>][<?php echo $option->id; ?>]" class="styled options" value="<?php echo $option->id; ?>"> <?php echo $option->value; ?>
                                    </label>
                                </div>
                                <?php else: ?>
                                    <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="option[<?php echo $og->id; ?>][<?php echo $option->id; ?>]" class="styled options" value="<?php echo $option->id; ?>"> <?php echo $option->value; ?>
                                    </label>
                                </div>

                            <?php endif; ?>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>