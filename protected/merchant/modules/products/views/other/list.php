<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('product_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('products/other/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span><?php echo lang('product_add_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table table-hover" id="table" data-url="<?php echo site_url('products/other/get_list'); ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="asc"><?php echo lang('product_code_th'); ?></th>
                        <th><?php echo lang('product_name_th'); ?></th>
                        <th><?php echo lang('product_category_th'); ?></th>
                        <th><?php echo lang('product_price_th'); ?></th>
                        <th><?php echo lang('product_quantity_th'); ?></th>
                        <th><?php echo lang('product_status_th'); ?></th>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions_th'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>