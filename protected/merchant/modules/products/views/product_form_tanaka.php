<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('product_edit_heading') : lang('product_add_heading'); ?> :: <?php echo $product->name; ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('products'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('product_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <form class="form-horizontal " action="<?php echo site_url('products/save_tanaka'); ?>" method="post" id="form">
                <input type="hidden" name="id" value="<?php echo ($data) ? encode($data->id) : ''; ?>">
                <input type="hidden" name="product" value="<?php echo $product->id; ?>">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('product_form_price_label'); ?></label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon">Rp</span>
                                <input type="text" id="price" name="price" required="" class="form-control number" value="<?php echo ($data) ? $data->price : $product->price; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('product_form_discount_label'); ?></label>
                        <div class="col-md-2">
                            <div class="input-group">
                                <input type="number" id="discount" name="discount" min="0" max="99" class="form-control " value="<?php echo ($data) ? $data->discount : 0; ?>">
                                <span class="input-group-addon">%</span>
                            </div>
                        </div>
                        <label class="col-md-2 control-label">Harga Setelah Diskon</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon">Rp</span>
                                <input type="text" id="price-discount" class="form-control number" disabled="" value="<?php echo ($data) ? ($data->price - ($data->price * $data->discount / 100)) : 0; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('product_form_weight_label'); ?></label>
                        <div class="col-md-2">
                            <div class="input-group">
                                <input type="text" name="weight" required="" class="form-control number" value="<?php echo ($data) ? $data->weight : $product->weight; ?>">
                                <span class="input-group-addon">gram</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('product_form_quantity_label'); ?></label>
                        <div class="col-md-2">
                            <input type="text" id="quantity" name="quantity" required="" class="form-control number" value="<?php echo ($data) ? $data->quantity : 0; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Dapat Dibeli</label>
                        <div class="col-md-9">
                            <input type="checkbox" name="status" value="1" data-on-text="Ya" data-off-text="Tidak" class="switch" <?php echo ($data) ? (($data->status == 1) ? 'checked' : '') : ''; ?>>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="heading-elements action-left">
                        <a class="btn btn-default" href="<?php echo site_url('products'); ?>"><?php echo lang('button_cancel'); ?></a>
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>