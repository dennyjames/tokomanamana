<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Productv2_model extends CI_Model {

    function get($id) {
        $this->db->select('p.*, keyword seo_url')
                ->join('seo_url su', "su.query = CONCAT('catalog/products/view/',p.id)", 'left')
                ->where('p.id', $id);
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_all($merchant,$start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('p.id, pi.image, p.code, p.name,p.status, p.category AS category_product, c.name category,p.date_added, p.price, p.quantity,p.variation')
        // $this->db->select('p.id, pi.image, p.code, p.name,p.status, p.category, c.name category, IFNULL(pp.price, p.price) price, IFNULL(pm.quantity,0) quantity, pm.id product_merchant, pm.pricing_level')
                ->join('product_image pi','p.id = pi.product AND pi.primary = 1','left')
                ->join('categories c', 'c.id = p.category', 'left')
                // ->join('product_price pp', 'pp.product = p.id AND pp.merchant_group ='.$merchant_group, 'left')
                // ->join('product_merchant pm', 'pm.product = p.id AND pm.merchant ='.$merchant, 'left')

                 // ->where('p.store_type','merchant')
                 ->where('p.store_id',$merchant)
                 ->where('p.name!=','')
                // ->where('p.status = 1')
                // ->where('p.merchant = 0')
                ->limit($length, $start);
        return $this->db->get('products p');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 1: $key = 'p.code';
                break;
            case 2: $key = 'p.name';
                break;
            case 4: $key = 'c.name';
                break;
            case 3: $key = 'price';
                break;
            case 7: $key = 'p.quantity';
                break;
            case 5: $key = 'p.date_added';
                break;
            case 6: $key = 'p.status';
                break;
        }
        return $key;
    }

    function get_username($merchant){
        $this->db->select('mu.username')
             ->join('merchant_users mu','mu.id = m.auth')
             ->where('m.id',$merchant);
        return $this->db->get('merchants m')->row();
    }

    function count_all($merchant, $search = '') {
        $this->_where_like($search);
        $this->db->join('product_image pi','p.id = pi.product AND pi.primary = 1','left')
                ->join('categories c', 'c.id = p.category', 'left')
                // ->join('product_price pp', 'pp.product = p.id AND pp.merchant_group ='.$merchant_group, 'left')
                // ->join('product_merchant pm', 'pm.product = p.id AND pm.merchant ='.$merchant, 'left')
                // ->where('p.status = 1')
                // ->where('p.merchant = 0');
                // ->where('p.store_type','merchant')
                ->where('p.store_id',$merchant);
        return $this->db->count_all_results('products p');
    }

    private function _where_like($search = '') {
        $columns = array('p.code', 'p.name', 'c.name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $col) {
                $this->db->or_like("IFNULL($col,'')", $search);
            }
            $this->db->group_end();
        }
    }

    function get_products_only($id){
        $this->db->select('*')
                ->where('store_id',$id)
                ->order_by('name','ASC');
        return $this->db->get('products')->result();
    }

    function check_seo_url_unique($seo_url){
        $this->db->select('s.id')
                ->where('s.keyword',$seo_url);
        $query = $this->db->get('seo_url s');
        return($query->num_rows() > 0) ? true : false;
    }

    function get_categories() {
        $this->db->select("pcp.category id, GROUP_CONCAT(pc2.name ORDER BY pcp.level SEPARATOR ' > ') name", FALSE)
                ->join('categories pc1', 'pcp.category = pc1.id', 'left')
                ->join('categories pc2', 'pcp.path = pc2.id', 'left')
                ->order_by('pc2.name')
                ->group_by('pcp.category');

        return $this->db->get('category_path pcp');
    }

     function get_category_seourl($id){
        $query = $this->db->get_where('categories c',array('c.id'=>$id));
        $ret = $query->row();
        return $ret->name;
    }

    //  function get_variations() {
    //     $this->db->select("o.value")
    //              ->join('options_group og', 'og.id = o.group')
    //              ->order_by('o.value')
    //              ->group_by('o.group');


    //     return $this->db->get('options o');
    // }



    function get_product_features($product) {
        $this->db->select('pf.product, pf.value, f.*')
                ->join('features f', 'pf.feature = f.id', 'left')
                ->where('pf.product', $product)
                ->group_by('pf.feature');
        return $this->db->get('product_feature pf');
    }

    function get_product_feature_variants($product, $feature) {
        $this->db->select('fv.*')
                ->join('feature_variant fv', 'pf.feature = fv.feature AND pf.value = fv.id', 'left')
                ->where('pf.product', $product)
                ->where('pf.feature', $feature);
        $query = $this->db->get('product_feature pf');
        return($query->num_rows() > 0) ? $query : false;
    }

    function get_product_feature_by_category($category, $product = 0) {
        $this->db->select('cf.category, f.*, fv.value variant')
                ->join('features f', 'f.id = cf.feature', 'left')
                ->join('feature_variant fv', 'fv.id = cf.feature', 'left')
                ->where('cf.category', $category)
                ->group_by('cf.feature')
                ->order_by('f.sort_order ASC');

        if ($product) {
            $this->db->select('pf.product, pf.value')
                    ->join('product_feature pf', 'pf.feature = cf.feature AND pf.product = ' . $product, 'left');
        }
        return $this->db->get('category_feature cf');
    }

    function get_features_by_category($category, $product = 0) {
        $this->db->select('f.*, pf.value')
                ->join('features f', 'cf.feature = f.id', 'left')
                ->join('product_feature pf', 'pf.feature = cf.feature AND pf.product = ' . $product, 'left')
                ->where('cf.category', $category)
                ->group_by('cf.feature')
                ->order_by('f.sort_order ASC');
        return $this->db->get('category_feature cf');
    }
    //   ->join('product_option po', 'po.feature = cf.feature AND pf.product = ' . $product, 'left')
    function get_variations_by_category($category) {
        $this->db->select('og.*')
                ->join('option_group og','og.id=cv.variation')
                ->where('cv.category', $category)
                ->order_by('og.name ASC');
        return $this->db->get('category_variation cv');
    }

    function get_product_options($product, $ids = array()) {
        $this->db->select("po.*, GROUP_CONCAT(o.type, ': ', o.value  SEPARATOR ', ') option_combination")
                ->join('product_option_combination poc', 'po.id = poc.product_option', 'left')
                ->join('options o', 'poc.option = o.id', 'left')
                // ->join('option_group og', 'og.id = o.group', 'left')
                ->group_by('po.id')
                ->where('po.product', $product);
        if($ids)
            $this->db->where_in('po.id',$ids);
        return $this->db->get('product_option po');
    }

    function get_change_variation_check($product,$type){
        $this->db->select('o.type')
                 ->join('product_option_combination poc','poc.product_option = po.id')
                 ->join('options o','o.id = poc.option')
                 ->where('o.type',$type)
                 ->where('po.product',$product);
        return $this->db->get('product_option po')->row();
    }

    function get_variation_previously($product){
        $this->db->select('o.type')
                 ->join('product_option_combination poc','poc.product_option = po.id')
                 ->join('options o','o.id = poc.option')
                 ->where('po.product',$product);
        return $this->db->get('product_option po')->row();
    }

    function delete_variants($product){
        $this->db->query("DELETE product_option,product_option_combination
        FROM product_option,product_option_combination
        WHERE product_option.product = $product 
        AND product_option_combination.product_option = product_option.id");

    }

    // function delete_variant_image($product){
    //     $this->db->query("DELETE product_option,product_option_combination
    //     FROM product_option,product_option_combination
    //     WHERE product_option.product = $product 
    //     AND product_option_combination.product_option = product_option.id");

    // }

    function duplicate_variation_check($product){
       $query = $this->db->query("SELECT product_option_combination.product_option
                FROM product_option_combination
                JOIN product_option ON product_option.id = product_option_combination.product_option
                WHERE product_option.product = $product
                GROUP BY product_option_combination.product_option
                HAVING ( COUNT(*) > 1 )");
        return $query->num_rows();
    }



    function get_product_option_image($product, $product_option) {
        $this->db->select('id, product, image, product_option')
                ->join('product_option_image poi', 'pi.id = poi.product_image AND poi.product_option = ' . $product_option, 'left')
                ->where('pi.product', $product);

        return $this->db->get('product_image pi');
    }

    function get_package_item($id,$q) {
        $this->db->select("p.id , p.name as value");
        $this->db->limit(6);
        $this->db->where('store_id',$id);
        $this->db->like('name',$q);
        $query = $this->db->get('products p');
        return $query->result();
    }

    function get_prices($product = 0) {
        $this->db->select('pp.*, mg.id merchant_group, mg.name')
                ->join('product_price pp', 'mg.id = pp.merchant_group AND pp.product = ' . $product, 'left')
                ->order_by('mg.name');
        return $this->db->get('merchant_groups mg');
    }

    function get_table_deskripsi($id){
        $this->db->select('dr.*, d.nama')
             ->join('deskripsi d', 'dr.id_deskripsi = d.id_deskripsi', 'left')
             ->where('dr.id_kategori', $id);
        return $this->db->get('deskripsi_relasi dr');
    }

    function get_category_view_frontend($id) {
        $this->db->select("cp.category id, GROUP_CONCAT(c2.name ORDER BY cp.level SEPARATOR ' > ') name, c1.image, c1.active, c1.sort_order, c1.home_show, c1.home_sort_order", FALSE)
                ->join('categories c1', 'cp.category = c1.id', 'left')
                ->join('categories c2', 'cp.path = c2.id', 'left')
                ->group_by('cp.category');
        return $this->db->get_where('category_path cp', array('cp.category' => $id))->row();
    }
    
    // function price_level_group($product,$group=0){
    //     $this->db->select('pclg.*, pcl.min_qty, pcl.product')
    //             ->join('product_price_level pcl','pcl.id = pclg.product_price_level','left')
    //             ->where_in('product_price_level','(SELECT id FROM product_price_level WHERE product = '.$product.')',false)
    //             ->where('merchant_group',$group)
    //             ->order_by('pcl.min_qty asc');
    //     return $this->db->get('product_price_level_groups pclg');
    // }
    function price_level_groups($price_level = 0) {
        $this->db->select('pp.*, mg.id merchant_group, mg.name')
                ->join('product_price_level_groups pp', 'mg.id = pp.merchant_group AND pp.product_price_level = ' . $price_level, 'left')
                ->order_by('mg.name');
        return $this->db->get('merchant_groups mg');
    }
     
    function export_data($merchant) {
        $this->db->select('p.*,c.name AS category_name,e.name AS etalase_name,b.name AS brand_name')
                ->join('categories c','p.category = c.id')
                ->join('etalase e','p.etalase = e.id','left')
                ->join('brands b','p.brand = b.id','left')
                ->where('p.store_id',$merchant)
                ->order_by('p.name','ASC');

        return $this->db->get('products p');
    }
}
