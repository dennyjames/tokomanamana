<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tanaka_model extends CI_Model {

    function get($id) {
        $this->db->select('p.*, keyword seo_url')
                ->join('seo_url su', "su.query = CONCAT('catalog/products/view/',p.id)", 'left')
                ->where('p.id', $id);
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_all($merchant, $merchant_group, $start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('p.id, pi.image, p.code, p.name, p.category, c.name category, IFNULL(pp.price, p.price) price, IFNULL(pm.quantity,0) quantity, pm.id product_merchant, pm.pricing_level')
                ->join('product_image pi','p.id = pi.product AND pi.primary = 1','left')
                ->join('categories c', 'c.id = p.category', 'left')
                ->join('product_price pp', 'pp.product = p.id AND pp.merchant_group ='.$merchant_group, 'left')
                ->join('product_merchant pm', 'pm.product = p.id AND pm.merchant ='.$merchant, 'left')
                ->where('p.status = 1')
                ->where('p.merchant = 0')
                ->limit($length, $start);

        return $this->db->get('products p');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 1: $key = 'p.code';
                break;
            case 2: $key = 'p.name';
                break;
            case 4: $key = 'c.name';
                break;
            case 3: $key = 'price';
                break;
            case 5: $key = 'pm.quantity';
                break;
        }
        return $key;
    }

    function count_all($merchant, $merchant_group, $search = '') {
        $this->_where_like($search);
        $this->db->join('product_image pi','p.id = pi.product AND pi.primary = 1','left')
                ->join('categories c', 'c.id = p.category', 'left')
                ->join('product_price pp', 'pp.product = p.id AND pp.merchant_group ='.$merchant_group, 'left')
                ->join('product_merchant pm', 'pm.product = p.id AND pm.merchant ='.$merchant, 'left')
                ->where('p.status = 1')
                ->where('p.merchant = 0');
        return $this->db->count_all_results('products p');
    }

    private function _where_like($search = '') {
        $columns = array('p.code', 'p.name', 'c.name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $col) {
                $this->db->or_like("IFNULL($col,'')", $search);
            }
            $this->db->group_end();
        }
    }

    function get_categories() {
        $this->db->select("pcp.category id, GROUP_CONCAT(pc2.name ORDER BY pcp.level SEPARATOR ' > ') name", FALSE)
                ->join('categories pc1', 'pcp.category = pc1.id', 'left')
                ->join('categories pc2', 'pcp.path = pc2.id', 'left')
                ->order_by('pc2.name')
                ->group_by('pcp.category');

        return $this->db->get('category_path pcp');
    }

    function get_product_features($product) {
        $this->db->select('pf.product, pf.value, f.*')
                ->join('features f', 'pf.feature = f.id', 'left')
                ->where('pf.product', $product)
                ->group_by('pf.feature');
        return $this->db->get('product_feature pf');
    }

    function get_product_feature_variants($product, $feature) {
        $this->db->select('fv.*')
                ->join('feature_variant fv', 'pf.feature = fv.feature AND pf.value = fv.id', 'left')
                ->where('pf.product', $product)
                ->where('pf.feature', $feature);
        $query = $this->db->get('product_feature pf');
        return($query->num_rows() > 0) ? $query : false;
    }

    function get_product_feature_by_category($category, $product = 0) {
        $this->db->select('cf.category, f.*, fv.value variant')
                ->join('features f', 'f.id = cf.feature', 'left')
                ->join('feature_variant fv', 'fv.id = cf.feature', 'left')
                ->where('cf.category', $category)
                ->group_by('cf.feature')
                ->order_by('f.sort_order ASC');

        if ($product) {
            $this->db->select('pf.product, pf.value')
                    ->join('product_feature pf', 'pf.feature = cf.feature AND pf.product = ' . $product, 'left');
        }
        return $this->db->get('category_feature cf');
    }

    function get_features_by_category($category, $product = 0) {
        $this->db->select('f.*, pf.value')
                ->join('features f', 'cf.feature = f.id', 'left')
                ->join('product_feature pf', 'pf.feature = cf.feature AND pf.product = ' . $product, 'left')
                ->where('cf.category', $category)
                ->group_by('cf.feature')
                ->order_by('f.sort_order ASC');
        return $this->db->get('category_feature cf');
    }

    function get_prices($product = '') {
        $this->db->select('pp.*, mg.id merchant_group, mg.name')
                ->join('product_price pp', 'mg.id = pp.merchant_group '.(($product)?'AND pp.product = '.$product:''), 'left');
        return $this->db->get('merchant_groups mg');
    }
    
    function price_level_group($product,$group=0){
        $this->db->select('pclg.*, pcl.min_qty, pcl.product')
                ->join('product_price_level pcl','pcl.id = pclg.product_price_level','left')
                ->where_in('product_price_level','(SELECT id FROM product_price_level WHERE product = '.$product.')',false)
                ->where('merchant_group',$group)
                ->order_by('pcl.min_qty asc');
        return $this->db->get('product_price_level_groups pclg');
    }

    function export_data($merchant,$merchant_group) {
        $this->db->select('p.id, pi.image, p.code, p.name, p.category, c.name category, IFNULL(pp.price, p.price) price, IFNULL(pm.quantity,0) quantity, pm.id product_merchant, pm.pricing_level')
                ->join('product_image pi','p.id = pi.product AND pi.primary = 1','left')
                ->join('categories c', 'c.id = p.category', 'left')
                ->join('product_price pp', 'pp.product = p.id AND pp.merchant_group ='.$merchant_group, 'left')
                ->join('product_merchant pm', 'pm.product = p.id AND pm.merchant ='.$merchant, 'left')
                ->where('p.status = 1')
                ->where('p.merchant = 0')
                ->order_by('quantity','DESC')
                ->order_by('p.name','ASC');

        return $this->db->get('products p');
    }
}
