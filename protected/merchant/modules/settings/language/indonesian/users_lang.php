<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['user_heading'] = 'Pengguna';
$lang['user_list_heading'] = 'Daftar Pengguna';
$lang['user_add_heading'] = 'Tambah Pengguna';
$lang['user_edit_heading'] = 'Edit Pengguna';

$lang['user_email'] = 'Alamat Email';
$lang['user_group'] = 'Grup Pengguna';
$lang['user_username'] = 'Username';
$lang['user_password'] = 'Kata Sandi';
$lang['owner_password'] = 'Kata Sandi Pemilik Akun';

$lang['permission_heading'] = 'Hak Akses';
$lang['permission_list_heading'] = 'Daftar Hak Akses';
$lang['permission_add_heading'] = 'Tambah Hak Akses';
$lang['permission_edit_heading'] = 'Edit Hak Akses';

$lang['permission_name'] = 'Kode Hak Akses';
$lang['permission_definition'] = 'Deskripsi';

$lang['group_heading'] = 'Grup Pengguna';
$lang['group_list_heading'] = 'Daftar Grup Pengguna';
$lang['group_add_heading'] = 'Tambah Grup Pengguna';
$lang['group_edit_heading'] = 'Edit Grup Pengguna';

$lang['group_name'] = 'Kode Grup Pengguna';
$lang['group_definition'] = 'Deskripsi';
$lang['group_permission'] = 'Hak Akses';

$lang['leave_blank'] = 'Biarkan kosong jika tidak ingin merubah kata sandi';
$lang['leave_blank_email'] = 'Biarkan kosong jika tidak ingin merubah email';

$lang['save_success'] = "%s berhasil disimpan.";
$lang['save_error'] = "%s gagal disimpan.";
$lang['delete_success'] = "%s berhasil dihapus.";
$lang['delete_error'] = "%s gagal dihapus.";