<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Settings extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->lang->load('settings', settings('language'));
        $this->data['menu'] = 'setting_store';
    }

    public function index() {
        redirect('settings/store');
    }

    public function store() {
        if($this->aauth->is_loggedin()){
            $this->aauth->control('setting/store');
        }
        if ($this->input->is_ajax_request()) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('name', 'lang:setting_store_name_label', 'trim|required');
            $this->form_validation->set_rules('postcode', 'lang:setting_store_postcode_label', 'trim|required|numeric');
            $this->form_validation->set_rules('province', 'lang:setting_store_province_label', 'trim|required');
            $this->form_validation->set_rules('city', 'lang:setting_store_city_label', 'trim|required');
            $this->form_validation->set_rules('shipping[]', 'lang:setting_shipping_label', 'trim|required');
//            $this->form_validation->set_rules('district', 'lang:setting_store_district_label', 'trim|required');
            if ($this->form_validation->run() === true) {
                $data = $this->input->post(null, true);
                do {
                    $data['shipping'] = json_encode($data['shipping']);
                    $this->main->update('merchants', $data, array('id' => $this->data['user']->merchant));
                    $this->session->unset_userdata('merchant_user');
                    $return = array('message' => lang('setting_store_save_success_message'), 'status' => 'success', 'redirect' => site_url('settings/store'));
                } while (0);
            } else {
                $return = array('message' => validation_errors(), 'status' => 'error');
            }
            echo json_encode($return);
        } else {
            $this->load->library('googlemaps');
            
            $this->breadcrumbs->push(lang('setting'), '/settings');
            $this->breadcrumbs->push(lang('setting_store_tabs'), '/settings/store');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
            $this->data['menu'] = 'setting_store';
            $this->data['data'] = $this->main->get('merchants', array('id' => $this->data['user']->merchant));
            $this->data['shipping'] = $this->main->get('merchants', array('id' => $this->data['user']->merchant))->shipping;
            if ($this->data['shipping']) {
                $this->data['shipping'] = json_decode($this->data['shipping']);
            } else {
                $this->data['shipping'] = array();
            }

            $this->data['provinces'] = $this->main->gets('provincies', array(), 'name asc');
            if ($this->data['data']->province)
                $this->data['cities'] = $this->main->gets('cities', array('province' => $this->data['data']->province), 'name asc');
            if ($this->data['data']->city)
                $this->data['districts'] = $this->main->gets('districts', array('city' => $this->data['data']->city), 'name asc');

            $config['center'] = $this->data['data']->lat . ',' . $this->data['data']->lng;
            $config['loadAsynchronously'] = TRUE;
            $config['map_height'] = '500px';
            $config['onboundschanged'] = 'if (!centreGot) {
                var mapCentre = map.getCenter();
                marker_0.setOptions({
                        position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng()) 
                });
                setLocation(mapCentre.lat(), mapCentre.lng());
            }centreGot = true;';
            $config['disableFullscreenControl'] = TRUE;
            $config['disableMapTypeControl'] = TRUE;
            $config['disableStreetViewControl'] = TRUE;
            $config['places'] = TRUE;
            $config['placesAutocompleteInputID'] = 'search-location';
            $config['placesAutocompleteBoundsMap'] = TRUE; // set results biased towards the maps viewport
            $config['placesAutocompleteOnChange'] = 'map.setCenter(this.getPlace().geometry.location); 
            marker_0.setOptions({
                        position: new google.maps.LatLng(this.getPlace().geometry.location.lat(), this.getPlace().geometry.location.lng()) 
                });
                setLocation(this.getPlace().geometry.location.lat(), this.getPlace().geometry.location.lng());';
            $this->googlemaps->initialize($config);
            $marker = array();
            $marker['draggable'] = true;
            $marker['ondragend'] = 'setLocation(event.latLng.lat(), event.latLng.lng());';
            $this->googlemaps->add_marker($marker);
            $this->data['map'] = $this->googlemaps->create_map();

            $this->template->_init();
            $this->template->form();
            $this->load->js('../assets/backend/js/merchant/modules/settings.js');
            $this->output->set_title(lang('setting_store_heading'));
            $this->load->view('store', $this->data);
        }
    }

    // public function shipping() {
    //     if($this->aauth->is_loggedin()){
    //         $this->aauth->control('setting/shipping');
    //     }
    //     if ($this->input->is_ajax_request()) {
    //         $this->load->library('form_validation');

    //         $this->form_validation->set_rules('shipping[]', 'lang:setting_shipping_label', 'trim|required');
    //         if ($this->form_validation->run() === true) {
    //             $data = $this->input->post(null, true);
    //             do {
    //                 $data['shipping'] = json_encode($data['shipping']);
    //                 $this->main->update('merchants', $data, array('id' => $this->data['user']->merchant));
    //                 $return = array('message' => lang('setting_shipping_save_success_message'), 'status' => 'success', 'redirect' => site_url('settings/shipping'));
    //             } while (0);
    //         } else {
    //             $return = array('message' => validation_errors(), 'status' => 'error');
    //         }
    //         echo json_encode($return);
    //     } else {
    //         $this->template->_init();
    //         $this->template->form();
    //         $this->breadcrumbs->push(lang('setting'), '/settings');
    //         $this->breadcrumbs->push(lang('setting_shipping_tabs'), '/settings/shipping');
    //         $this->data['breadcrumbs'] = $this->breadcrumbs->show();
    //         $this->data['menu'] = 'setting_shipping';
    //         $this->data['data'] = $this->main->get('merchants', array('id' => $this->data['user']->merchant))->shipping;
    //         if ($this->data['data']) {
    //             $this->data['data'] = json_decode($this->data['data']);
    //         } else {
    //             $this->data['data'] = array();
    //         }
    //         $this->output->set_title(lang('setting_shipping_heading'));
    //         $this->load->view('shipping', $this->data);
    //     }
    // }

    public function profile() {
        if($this->ion_auth->logged_in()){
            $merchant_user = $this->main->get('merchant_users', array('id' => $this->data['user']->id));
        }
        if($this->aauth->is_loggedin()){
            $this->aauth->control('setting/profile');
        }
        if ($this->input->is_ajax_request()) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('fullname', 'lang:setting_profile_name_label', 'trim|required');
            $this->form_validation->set_rules('token', 'Kode Verifikasi', 'trim|required');
            // $this->form_validation->set_rules('bank_name', 'lang:setting_profile_bank_name_label', 'trim|required');
            // $this->form_validation->set_rules('bank_account_number', 'lang:setting_profile_bank_account_name_label', 'trim|required');
            // $this->form_validation->set_rules('bank_account_name', 'lang:setting_profile_bank_account_number_label', 'trim|required');
//            $this->form_validation->set_rules('phone', 'lang:setting_profile_phone_label', 'trim|required');
//            $this->form_validation->set_rules('email', 'lang:setting_profile_phone_label', 'trim|required');
            if ($this->input->post('password') || $this->input->post('password_old')) {
                $this->form_validation->set_rules('password', 'lang:setting_profile_password_old_label', 'trim|required');
                $this->form_validation->set_rules('password_old', 'lang:setting_profile_password_label', 'trim|required|min_length[6]');
            }
            if ($this->form_validation->run() === true) {
                $data = $this->input->post(null, true);
                do {
                    if($this->ion_auth->logged_in()){
                        if ($merchant_user->verification_code != $data['token']) {
                            $return = array('message' => 'Kode verifikasi salah!', 'status' => 'error');
                            break;
                        }
                    }
                    if ($data['password']) {
                        $change_password = $this->ion_auth->change_password($this->data['user']->email, $data['password_old'], $data['password']);
                        if (!$change_password) {
                            $return = array('message' => $this->ion_auth->errors(), 'status' => 'error');
                            break;
                        }
                    }
//                    $this->main->update('merchant_users', array('phone' => $data['phone'], 'fullname' => $data['fullname']), array('id' => $this->data['user']->id));
                    $this->main->update('merchant_users', array('email' => $data['email'], 'fullname' => $data['fullname'], 'verification_code' => ''), array('id' => $this->data['user']->id));
                    if($data['bank_name'] == 'BANK BCA'){
                            $data['bank_name'] = 'BCA';
                        }
                     $this->main->update('merchants', array('bank_account_name' => $data['bank_account_name'], 'bank_account_number' => $data['bank_account_number'], 'bank_name' => $data['bank_name'], 'bank_branch' => $data['bank_branch']), array('id' => $this->data['user']->merchant));

                    // $this->main->update('merchants', array('bank_name' => $data['bank_name'], 'bank_branch' => $data['bank_branch'], 'bank_account_number' => $data['bank_account_number'], 'bank_account_name' => $data['bank_account_name']), array('id' => $this->data['user']->merchant));

                    $this->session->unset_userdata('merchant_user');
                    $return = array('message' => lang('setting_profile_save_success_message'), 'status' => 'success', 'redirect' => site_url('settings/profile'));
                } while (0);
            } else {
                $return = array('message' => validation_errors(), 'status' => 'error');
            }
            echo json_encode($return);
        } else {
            $this->data['merchant'] = $this->main->get('merchants', array('id' => $this->data['user']->merchant));
            $this->data['bank_lists'] = $this->main->gets('rekening_bank',[],'id asc');
            $this->breadcrumbs->push(lang('setting'), '/settings');
            $this->breadcrumbs->push(lang('setting_profile_tabs'), '/settings/profile');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
            $this->data['menu'] = 'setting_profile';

            $this->data['token_time_left'] = 0;
            if($this->ion_auth->logged_in()){
                if ($merchant_user->verification_code) {
                    $verification_time = strtotime($merchant_user->verification_sent_time);
                    if ((strtotime(date('Y-m-d H:i:s')) - $verification_time) < 120) {
                        $this->data['token_time_left'] = strtotime(date('Y-m-d H:i:s')) - $verification_time;
                    }
                }
            }

            $this->template->_init();
            $this->template->form();
            $this->output->set_title(lang('setting_profile_heading'));
            $this->load->js('../assets/backend/js/merchant/modules/auth/verification.js');
            $this->load->view('profile', $this->data);
        }
    }

    public function request_token() {
        $this->load->library('sprint');
        $sms = json_decode(settings('sprint_sms'), true);
        $code = rand(1000, 9999);
        $sms['m'] = 'Hai, berikut kode verifikasi TokoManaMana Anda ' . $code;
        $sms['d'] = $this->data['user']->phone;
        $url = $sms['url'];
        unset($sms['url']);
        $sprint_response = $this->sprint->sms($url, $sms);
        $sprint_response = explode('_', $sprint_response);
        if ($sprint_response[0] == 0) {
            $this->main->update('merchant_users', array('verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')), array('id' => $this->data['user']->id));
        }
    }

    public function get_cities() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $output = '<option value=""></option>';
        if ($province = $this->input->post('id')) {
            $cities = $this->main->gets('cities', array('province' => $province), 'name asc');
            if ($cities)
                foreach ($cities->result() as $city) {
                    $output .= '<option value="' . $city->id . '">' . $city->type . ' ' . $city->name . '</option>';
                }
        }
        echo $output;
    }

    public function get_districts() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $output = '<option value=""></option>';
        if ($city = $this->input->post('id')) {
            $districts = $this->main->gets('districts', array('city' => $city), 'name asc');
            if ($districts)
                foreach ($districts->result() as $district) {
                    $output .= '<option value="' . $district->id . '">' . $district->name . '</option>';
                }
        }
        echo $output;
    }

}
