<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Users extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if($this->aauth->is_loggedin()){
            $this->aauth->control('setting/user');
        }

        $this->lang->load('users', settings('language'));
        $this->load->model('users/users_model', 'users');

        $this->data['menu'] = 'setting_user';
    }

    public function index() {
        if($this->data['user']->merchant_type != 'merchant'){
            $this->load->view('errors/html/error_notallowed');
            return false;
        }
        $this->breadcrumbs->unshift(lang('setting'), '/');
        $this->breadcrumbs->push(lang('user'), '/settings/users');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->table();
        $this->output->set_title(lang('user_heading'));
        $this->load->view('users/list', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->users->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->username,
                    $data->email,
                    $data->group_name,
                    '<td class="text-center">
                    <ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="' . site_url('settings/users/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>
                    <li><a href="' . site_url('settings/users/delete/' . encode($data->id)) . '">' . lang('button_delete') . '</a></li>
                    </ul>
                    </li>
                    </ul>
                    </td>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->users->count_all();
        $output['recordsFiltered'] = $this->users->count_all($search);
        echo json_encode($output);
    }

    public function form($id = '') {
         if($this->data['user']->merchant_type != 'merchant'){
            $this->load->view('errors/html/error_notallowed');
            return false;
        }
        $this->data['groups'] = $this->main->gets('aauthm_groups');
        $this->data['data'] = array();
        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->aauth->get_user($id);
            $this->data['data']->group = $this->main->get('aauthm_user_to_group', array('user_id' => $id))->group_id;
        }
        $this->breadcrumbs->unshift(lang('setting'), '/');
        $this->breadcrumbs->push(lang('user'), '/settings/users');
        $this->breadcrumbs->push(($this->data['data']) ? lang('user_edit_heading') : lang('user_add_heading'), '/settings/users/form/'.$id);
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->form();
        $this->output->set_title(($this->data['data']) ? lang('edit_heading') : lang('add_heading'));
        $this->load->view('users/form', $this->data);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('group', 'lang:group', 'trim|required');
        $this->form_validation->set_rules('username', 'lang:username', 'trim|required');

        
        if (!$this->input->post('id')) {
            $this->form_validation->set_rules('password', 'lang:password', 'trim|required');
            $this->form_validation->set_rules('email', 'lang:email', 'trim|required|valid_email|callback_email_same_check');
        } else {
            $this->form_validation->set_rules('password', 'lang:password', 'trim');
            $this->form_validation->set_rules('email', 'lang:email', 'trim|valid_email|callback_email_same_check');
            $this->form_validation->set_rules('owner_password', 'lang:password', 'trim|required|callback_owner_password_check');
        }

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            if (!$data['id']) {
                $save = $this->aauth->create_user($data['email'], $data['password'], $data['username'], $data['group'],$this->data['user']->merchant);
            } else {
                $data['id'] = decode($data['id']);
                $save = $this->aauth->update_user($data['id'], $data['email'], $data['password'], $data['username'], $data['group']);
            }

            if (!$save) {
                $return = array('message' => $this->aauth->print_errors(), 'status' => 'error');
            } else {
                $return = array('message' => sprintf(lang('save_success'), $data['username']), 'status' => 'success', 'redirect' => site_url('settings/users'));
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function email_same_check($str)
    {
        $merchant_email = $this->main->get('merchant_users',array('email' => $str));
        $aauthm_email = $this->main->get('aauthm_users',array('email' => $str));
        $owner_email = $this->data['user']->email;
        if($merchant_email || $aauthm_email || $owner_email == $str){
            $this->form_validation->set_message('email_same_check', 'Email sudah terdaftar!');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

     public function owner_password_check($str)
    {

        $password_matches = $this->ion_auth->hash_password_db($this->data['user']->id, $str);
        if($password_matches === TRUE){
            
            return TRUE;
        }
        else
        {
            $this->form_validation->set_message('owner_password_check', 'Password pemilik salah!');
            return FALSE;
        }
    }

    public function delete($id) {

        $id = decode($id);
        $delete = $this->aauth->delete_user($id);
        redirect('settings/users');
   
    }

    public function permissions($act = '', $id = '') {
        $this->load->model('users/permissions_model', 'permissions');
        $this->data['menu'] = 'setting_user_permission';

        $this->breadcrumbs->unshift(lang('setting'), '/');
        $this->breadcrumbs->push(lang('user'), '/settings/users');

        if ($act == 'form') {
            $this->permission_form($id);
        } elseif ($act == 'get_list') {
            $this->permission_list();
        } else {
            $this->breadcrumbs->push(lang('user_permission'), '/settings/users/permissions');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
            $this->template->_init();
            $this->template->table();
            $this->output->set_title(lang('permissions'));
            $this->load->view('users/permissions/list', $this->data);
        }
    }

    public function permission_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->model('permissions_model', 'permissions');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->permissions->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->name,
                    $data->definition,
                    '<td class="text-center">
                    <ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="' . site_url('settings/users/permissions/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>
                    <li><a href="' . site_url('settings/users/permission_delete/' . $data->id) . '" class="delete">' . lang('button_delete') . '</a></li>
                    </ul>
                    </li>
                    </ul>
                    </td>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->permissions->count_all();
        $output['recordsFiltered'] = $this->permissions->count_all($search);
        echo json_encode($output);
    }

    public function permission_form($id = '') {
        $this->data['data'] = array();
        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->main->get('aauthm_perms', array('id' => $id));
        }
        $this->breadcrumbs->push(lang('user_permission'), '/settings/users/permissions');
        $this->breadcrumbs->push(($this->data['data']) ? lang('permission_edit_heading') : lang('permission_add_heading'), '/settings/users/permissions/'.$id);
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->form();
        $this->output->set_title(($this->data['data']) ? lang('permission_edit_heading') : lang('permission_add_heading'));
        $this->load->view('users/permissions/form', $this->data);
    }

    public function permission_save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:name', 'trim|required');
        $this->form_validation->set_rules('definition', 'lang:definition', 'trim');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            if (!$data['perm']) {
                $save = $this->aauth->create_perm($data['name'], $data['definition']);
                if (!$save) {
                    $return = array('message' => $this->aauth->print_infos(), 'status' => 'error');
                } else {
                    $return = array('message' => sprintf(lang('save_success'), lang('permission') . ' ' . $data['name']), 'status' => 'success', 'redirect' => site_url('settings/users/permissions'));
                }
            } else {
                $this->aauth->update_perm($data['perm'], $data['name'], $data['definition']);
                $return = array('message' => sprintf(lang('save_success'), lang('permission') . ' ' . $data['name']), 'status' => 'success', 'redirect' => site_url('settings/users/permissions'));
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function permission_delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $delete = $this->aauth->delete_perm($id);
        if (!$delete) {
            $return = array('message' => $this->aauth->print_infos(), 'status' => 'error');
        } else {
            $return = array('message' => sprintf(lang('save_success'), lang('permission')), 'status' => 'success');
        }

        echo json_encode($return);
    }

    public function groups($act = '', $id = '') {
        $this->load->model('users/groups_model', 'groups');
        $this->data['menu'] = 'setting_user_group';
        
        $this->breadcrumbs->unshift(lang('setting'), '/');
        $this->breadcrumbs->push(lang('user'), '/settings/users');

        if ($act == 'form') {
            $this->group_form($id);
        } elseif ($act == 'get_list') {
            $this->group_list();
        } else {
            $this->breadcrumbs->push(lang('user_group'), '/settings/users/groups');
            $this->data['breadcrumbs'] = $this->breadcrumbs->show();
            
            $this->template->_init();
            $this->template->table();
            $this->output->set_title(lang('groups'));
            $this->load->view('users/groups/list', $this->data);
        }
    }

    public function group_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->model('groups_model', 'groups');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->groups->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->name,
                    $data->definition,
                    '<td class="text-center">
                    <ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="' . site_url('settings/users/groups/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>
                    <li><a href="' . site_url('settings/users/group_delete/' . $data->id) . '" class="delete">' . lang('button_delete') . '</a></li>
                    </ul>
                    </li>
                    </ul>
                    </td>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->groups->count_all();
        $output['recordsFiltered'] = $this->groups->count_all($search);
        echo json_encode($output);
    }

    public function group_form($id = '') {
        $this->data['data'] = array();
        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->main->get('aauthm_groups', array('id' => $id));
            $this->data['perm_exist'] = array();
            $permissions = $this->main->gets('aauthm_perm_to_group', array('group_id' => $id));
            if ($permissions) {
                foreach ($permissions->result() as $perm) {
                    array_push($this->data['perm_exist'], $perm->perm_id);
                }
            }
        }
        $this->data['permissions'] = $this->main->gets('aauthm_perms', array(), 'name asc');
        $this->breadcrumbs->push(lang('user_permission'), '/settings/users/permissions');
        $this->breadcrumbs->push(($this->data['data']) ? lang('permission_edit_heading') : lang('permission_add_heading'), '/settings/users/permissions/'.$id);
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->template->_init();
        $this->template->form();
        $this->output->set_title(($this->data['data']) ? lang('group_edit_heading') : lang('group_add_heading'));
        $this->load->view('users/groups/form', $this->data);
    }

    public function group_save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:name', 'trim|required');
        $this->form_validation->set_rules('definition', 'lang:definition', 'trim');
        $this->form_validation->set_rules('permissions', 'lang:permissions', 'trim');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            if (!$data['id']) {
                $save = $this->aauth->create_group($data['name'], $data['definition']);
                if (!$save) {
                    $return = array('message' => $this->aauth->print_infos(), 'status' => 'error');
                } else {
                    $permissions = array_filter($data['permissions']);
                    if ($permissions) {
                        $data_perm = array();
                        foreach ($permissions as $perm) {
                            array_push($data_perm, array('perm_id' => $perm, 'group_id' => $save));
                        }
                        $this->db->insert_batch('aauthm_perm_to_group', $data_perm);
                    }
                    $return = array('message' => sprintf(lang('save_success'), lang('group') . ' ' . $data['name']), 'status' => 'success', 'redirect' => site_url('settings/users/groups'));
                }
            } else {
                $this->main->update('aauthm_groups', array('name' => $data['name'], 'definition' => $data['definition']), array('id' => $data['id']));
                $this->main->delete('aauthm_perm_to_group', array('group_id' => $data['id']));
                $permissions = array_filter($data['permissions']);
                if ($permissions) {
                    $data_perm = array();
                    foreach ($permissions as $perm) {
                        array_push($data_perm, array('perm_id' => $perm, 'group_id' => $data['id']));
                    }
                    $this->db->insert_batch('aauthm_perm_to_group', $data_perm);
                }
                $return = array('message' => sprintf(lang('save_success'), lang('group') . ' ' . $data['name']), 'status' => 'success', 'redirect' => site_url('settings/users/groups'));
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function group_delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $delete = $this->aauth->delete_group($id);
        if ($delete) {
            $return = array('message' => sprintf(lang('delete_success'), lang('group')), 'status' => 'success');
        } else {
            $return = array('message' => $this->aauth->print_infos(), 'status' => 'error');
        }

        echo json_encode($return);
    }

}
