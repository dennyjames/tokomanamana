<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('au.id, username, email, ag.definition group_name')
                ->join('aauthm_user_to_group aug', 'aug.user_id = au.id', 'left')
                ->join('aauthm_groups ag', 'ag.id = aug.group_id', 'left')
                ->limit($length, $start);

        return $this->db->get('aauthm_users au');
    }

    function get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'username';
                break;
            case 1: $key = 'email';
                break;
            case 2: $key = 'ag.definition';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->where_like($search);
        $this->db->join('aauthm_user_to_group aug', 'aug.user_id = au.id', 'inner')
                ->join('aauthm_groups ag', 'ag.id = aug.group_id', 'inner');
        return $this->db->count_all_results('aauthm_users au');
    }

    function where_like($search = '') {
        $this->db->where('merchant_id',$this->data['user']->merchant);
        $columns = array('username', 'email', 'ag.definition');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

}
