<style type="text/css">
    #map_canvas{height:390px!important;width:80%!important}#search-location{width:80%!important}
</style>
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('setting_store_heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <form action="" method="post" class="form-horizontal" id="form">
                <?php $this->load->view('tabs_navigation'); ?>
                <input type="hidden" name="lat" id="lat" value="<?php echo ($data) ? $data->lat : ''; ?>">
                <input type="hidden" name="lng" id="lng" value="<?php echo ($data) ? $data->lng : ''; ?>">
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('setting_store_status_label'); ?></label>
                        <div class="col-md-2">
                            <select name="status" class="form-control">
                                <option value="1" <?php echo ($data->status == 1) ? 'selected' : ''; ?>>Buka</option>
                                <option value="2" <?php echo ($data->status == 2) ? 'selected' : ''; ?>>Tutup</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('setting_store_name_label'); ?></label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" required="" name="name" value="<?php echo $data->name; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('setting_store_description_label'); ?></label>
                        <div class="col-md-6">
                            <textarea cols="30" rows="2" class="form-control" name="description"><?php echo $data->description; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('setting_store_telephone_label'); ?></label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="telephone" value="<?php echo $data->telephone; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('setting_shipping_label'); ?></label>
                        <div class="col-md-6">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="shipping[]" class="styled" <?php echo (in_array('pickup', $shipping)) ? 'checked' : ''; ?> value="pickup">
                                Pickup
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="shipping[]" class="styled" <?php echo (in_array('jne', $shipping)) ? 'checked' : ''; ?> value="jne">
                                JNE
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="shipping[]" class="styled" <?php echo (in_array('tiki', $shipping)) ? 'checked' : ''; ?> value="tiki">
                                TIKI
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="shipping[]" class="styled" <?php echo (in_array('pos', $shipping)) ? 'checked' : ''; ?> value="pos">
                                POS Indonesia
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="shipping[]" class="styled" <?php echo (in_array('jnt', $shipping)) ? 'checked' : ''; ?> value="jnt">
                                J&T
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="shipping[]" class="styled" <?php echo (in_array('sicepat', $shipping)) ? 'checked' : ''; ?> value="sicepat">
                                Sicepat
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('setting_store_postcode_label'); ?></label>
                        <div class="col-md-2">
                            <input type="text" class="form-control" maxlength="5" name="postcode" value="<?php echo $data->postcode; ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('setting_store_address_label'); ?></label>
                        <div class="col-md-6">
                            <textarea cols="30" rows="2" class="form-control" id="address" name="address"><?php echo $data->address; ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('setting_store_province_label'); ?></label>
                        <div class="col-md-4">
                            <select class="select"  name="province" id="province" data-live-search="true" data-width="100%">
                                <option value=""></option>
                                <?php if ($provinces) { ?>
                                    <?php foreach ($provinces->result() as $province) { ?>
                                        <option value="<?php echo $province->id; ?>" <?php echo ($data->province == $province->id) ? 'selected' : ''; ?>><?php echo $province->name; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('setting_store_city_label'); ?></label>
                        <div class="col-md-4">
                            <select class="select"  name="city" id="city" data-live-search="true" data-width="100%">
                                <option value=""></option>
                                <?php if ($cities) { ?>
                                    <?php foreach ($cities->result() as $city) { ?>
                                        <option value="<?php echo $city->id; ?>" <?php echo ($data->city == $city->id) ? 'selected' : ''; ?>><?php echo $city->name; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('setting_store_district_label'); ?></label>
                        <div class="col-md-4">
                            <select class="select"  name="district" id="district" data-live-search="true" data-width="100%">
                                <option value=""></option>
                                <?php if ($districts) { ?>
                                    <?php foreach ($districts->result() as $district) { ?>
                                        <option value="<?php echo $district->id; ?>" <?php echo ($data->district == $district->id) ? 'selected' : ''; ?>><?php echo $district->name; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('maps'); ?></label>
                        <div class="col-md-9">
                            <script type="text/javascript">
                                var centreGot = false;
                            </script>
                            <?php echo $map['js']; ?>
                            <input class="form-control" id="search-location">
                            <?php echo $map['html']; ?>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="heading-elements action-left">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>