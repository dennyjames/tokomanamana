<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('setting_shipping_heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <form action="" method="post" class="form-horizontal" id="form">
                <?php $this->load->view('tabs_navigation'); ?>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo lang('setting_shipping_label'); ?></label>
                        <div class="col-md-12">
                            <label class="checkbox-inline">
                                <input type="checkbox" name="shipping[]" class="styled" <?php echo (in_array('pickup', $data)) ? 'checked' : ''; ?> value="pickup">
                                Pickup
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="shipping[]" class="styled" <?php echo (in_array('jne', $data)) ? 'checked' : ''; ?> value="jne">
                                JNE
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="shipping[]" class="styled" <?php echo (in_array('tiki', $data)) ? 'checked' : ''; ?> value="tiki">
                                TIKI
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="shipping[]" class="styled" <?php echo (in_array('pos', $data)) ? 'checked' : ''; ?> value="pos">
                                POS Indonesia
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="shipping[]" class="styled" <?php echo (in_array('jnt', $data)) ? 'checked' : ''; ?> value="jnt">
                                J&T
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" name="shipping[]" class="styled" <?php echo (in_array('sicepat', $data)) ? 'checked' : ''; ?> value="sicepat">
                                Sicepat
                            </label>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="heading-elements action-left">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>