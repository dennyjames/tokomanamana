<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('setting_profile_heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <form action="" method="post" class="form-horizontal" id="form">
                <?php $this->load->view('tabs_navigation'); ?>
                <div class="panel-body">
                    <fieldset class="content-group">
                        <legend class="text-bold"><?php echo lang('setting_profile_label'); ?></legend>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_name_label'); ?></label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" required="" name="fullname" value="<?php echo $user->fullname; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_phone_label'); ?></label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" disabled="" value="<?php echo $user->phone; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                        <label class="col-md-3 control-label"><?php echo "Username" ?></label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" required="" name="username" value="<?php echo $user->username; ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_email_label'); ?></label>
                            <div class="col-md-4">
                                <input type="email" class="form-control" name="email" value="<?php echo $user->email; ?>">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="content-group">
                        <legend class="text-bold"><?php echo lang('setting_profile_bank_label'); ?></legend>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_bank_account_name_label'); ?></label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" name="bank_account_name" value="<?php echo $merchant->bank_account_name; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_bank_account_number_label'); ?></label>
                            <div class="col-md-4">
                                <input type="text" class="form-control"  name="bank_account_number" value="<?php echo $merchant->bank_account_number; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_bank_name_label'); ?></label>
                            <div class="col-md-4">
                               <select class="bootstrap-select" name="bank_name" data-live-search="true" id="bank_name" data-width="100%">
                                    <option></option>
                                    <?php if ($bank_lists) {  ?>
                                        <?php foreach ($bank_lists->result() as $bank_list) {
                                            if($merchant->bank_name == 'BCA') $merchant->bank_name = 'BANK BCA'; ?>
                                            <option value="<?php echo $bank_list->name;?>" <?php echo ($merchant) ? ($merchant->bank_name == $bank_list->name) ? 'selected' : '' : ''; ?>><?= $bank_list->name;?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_bank_branch_label'); ?></label>
                            <div class="col-md-4">
                                <input type="text" class="form-control"  name="bank_branch" value="<?php echo $merchant->bank_branch; ?>">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="content-group">
                        <legend class="text-bold"><?php echo lang('setting_profile_security_label'); ?></legend>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_password_old_label'); ?></label>
                            <div class="col-md-4">
                                <input type="password" class="form-control" name="password_old">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><?php echo lang('setting_profile_password_label'); ?></label>
                            <div class="col-md-4">
                                <input type="password" class="form-control" name="password">
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="content-group">
                        <legend class="text-bold">OTP</legend>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Masukkan Kode Verifikasi</label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" name="token" value="">
                            </div>
                            <div class="col-md-7">
                                <p>Belum punya kode? <span id="wait" style="<?php echo (!$token_time_left) ? 'display:none;' : ''; ?>">Tunggu <span></span> detik</span><a id="request-token" href="javascript:void(0)" style="<?php echo ($token_time_left) ? 'display:none;' : ''; ?>">Kirim Kode</a></p>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="panel-footer">
                    <div class="heading-elements action-left">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    var time_left_verification = <?php echo $token_time_left; ?>;
</script>