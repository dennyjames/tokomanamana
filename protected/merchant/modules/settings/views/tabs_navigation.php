<div class="tabbable">
    <ul class="nav nav-tabs nav-tabs-solid">
        <li class="<?php echo menu_active($menu, 'setting_store'); ?>"><a href="<?php echo site_url('settings/store'); ?>"><?php echo lang('setting_store_tabs'); ?></a></li>
        <li class="<?php echo menu_active($menu, 'setting_profile'); ?>"><a href="<?php echo site_url('settings/profile'); ?>"><?php echo lang('setting_profile_tabs'); ?></a></li>
    </ul>
</div>