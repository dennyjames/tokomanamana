<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row">
                    <form id="filter-form">
                        <div class="col-sm-5">
                            <div class="row">
                                <div class="col-xs-12 col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Filter By:</label>
                                        <select id="filter-by" class="form-control">
                                            <option value="today">Hari Ini</option>
                                            <option value="yesterday">Kemarin</option>
                                            <option value="this month">Bulan Ini</option>
                                            <option value="last month">Bulan Lalu</option>
                                            <option value="this year">Tahun Ini</option>
                                            <option value="99">Kostum</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Dari:</label>
                                        <input type="text" class="form-control" readonly="" name="from" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 no-padding-right">
                                    <div class="form-group">
                                        <label class="control-label">Sampai:</label>
                                        <input type="text" class="form-control" readonly="" name="to" value="<?php echo date('Y-m-d'); ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="col-xs-4 col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Status Transaksi:</label>
                                    <select name="order_status" class="form-control">
                                        <option value="">Semua</option>
                                        <option value="3">Pesanan Baru</option>
                                        <option value="4">Sedang Diproses</option>
                                        <option value="5">Sudah Dikirim</option>
                                        <option value="6">Sampai Ditujuan</option>
                                        <?php if ($user->merchant_type == 'merchant') { ?>
                                            <option value="10">Siap Diambil</option>
                                            <option value="11">Sudah Diambil</option>
                                        <?php } ?>
                                        <option value="7">Selesai</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-4 col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Pengiriman:</label>
                                    <select name="shipping" class="form-control">
                                        <option value="">Semua</option>
                                        <option value="jne">JNE</option>
                                        <option value="tiki">TIKI</option>
                                        <option value="pos">POS</option>
                                        <option value="pickup">JNT</option>
                                        <option value="pickup">Sicepat</option>
                                        <option value="pickup">Pickup</option>
                                    </select>
                                </div>
                            </div>
                        
                            <div class="col-xs-4 col-sm-3" style="margin-top: 27px">
                                <button class="btn btn-default" type="button" id="filter">Filter</button>
                                <a href="javascript:void(0);" class="btn btn-default" type="button" id="btn-export">
                                    <i class="icon-download7 text-primary"></i><span> Export</span>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="panel panel-flat">
            <table class="table table-hover" id="table-report" data-url="<?php echo site_url('reports/sales/data'); ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="desc">ID</th>
                        <th>Tanggal</th>
                        <th>Merchant</th>
                        <th>Pelanggan</th>
                        <th>Total</th>
                        <th>Status</th>
                        <th>Pengiriman</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>