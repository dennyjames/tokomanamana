<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    function total_order_by_status($status) {
//        $status_new_order = array(settings('order_payment_received_status'), settings('order_process_status'));
        $this->db->where('merchant', $this->data['user']->merchant)
                ->where('order_status', $status);

        return $this->db->count_all_results('order_invoice oi');
    }
    
    function order_status_monthly($merchant) {
        $month = date('m');
        $year = date('Y');
        $this->db->select("COUNT(id) count, DAY(date_added) date, 
                            SUM(CASE WHEN order_status = 7 THEN 1 ELSE 0 END) finish, 
                            SUM(CASE WHEN order_status NOT IN (7,8,9) THEN 1 ELSE 0 END) progress,
                            SUM(CASE WHEN order_status = 9 THEN 1 ELSE 0 END) reject")
                ->group_by('DAY(date_added)')
                ->where(array('MONTH(oi.date_added)' => $month, 'YEAR(oi.date_added)' => $year,'oi.merchant'=>$merchant));
        return $this->db->get('order_invoice oi');
    }

    function order_status_chart($merchant,$month,$year) {
        $this->db->select("SUM(CASE WHEN order_status = 7 THEN 1 ELSE 0 END) finish, 
                            SUM(CASE WHEN order_status NOT IN (7,8,9) THEN 1 ELSE 0 END) progress,
                            SUM(CASE WHEN order_status = 9 THEN 1 ELSE 0 END) reject")
                ->where(array('MONTH(oi.date_added)' => $month, 'YEAR(oi.date_added)' => $year,'oi.merchant'=>$merchant));
        return $this->db->get('order_invoice oi');
    }
    
    function last_balances($merchant){
        $this->db->select('mb.*, oi.code invoice_code')
                ->join('order_invoice oi','oi.id = mb.invoice','left')
                ->where('mb.merchant',$merchant)
                ->order_by('mb.id desc')
                ->limit(4);
        return $this->db->get('merchant_balances mb');
    }

}
