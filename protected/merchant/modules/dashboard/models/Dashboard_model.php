<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    function sales_summary() {
        $status = settings('order_finish_status');
        $this->db->select('IFNULL(COUNT(op.id),0) transaction, IFNULL(SUM(oi.total),0) sales, DAY(oi.date_modified) date, DATE(oi.date_modified) date_summary')
                ->join('order_invoice oi', 'op.invoice = oi.id', 'left')
                ->join('products p', 'op.product = p.id', 'left')
                ->where(array('oi.order_status' => $status, 'YEAR(oi.date_added)' => date('Y'), 'MONTH(oi.date_added)' => date('m')))
                ->where('p.store_id', $this->data['user']->merchant)
                ->where('p.store_type', 'merchant')
                ->group_by('DAY(oi.date_added)')
                ->order_by('DAY(oi.date_added)', 'ASC');
        return $this->db->get('order_product op')->result();
    }

    function sales_summary_principal() {
        $status = settings('order_finish_status');
        $this->db->select('IFNULL(COUNT(op.id),0) transaction, IFNULL(SUM(oi.total),0) sales, DAY(oi.date_modified) date, DATE(oi.date_modified) date_summary')
                ->join('order_invoice oi', 'op.invoice = oi.id', 'left')
                ->join('products p', 'op.product = p.id', 'left')
                ->join('products_principal_stock pps','p.id = pps.product_id')
                ->where(array('oi.order_status' => $status, 'YEAR(oi.date_added)' => date('Y'), 'MONTH(oi.date_added)' => date('m')))
                ->where('pps.branch_id', $this->data['user']->merchant)
                ->where('p.store_type', 'principal')
                ->group_by('DAY(oi.date_added)')
                ->order_by('DAY(oi.date_added)', 'ASC');
        return $this->db->get('order_product op')->result();
    }

    function last_orders() {
        $this->db->select('oi.code, oi.order, oi.total, c.fullname customer_name, oi.customer, m.name merchant_name, oi.merchant, op.quantity, oi.order_status as status')
                ->join('order_invoice oi', 'order_product.invoice = oi.id', 'left')
                ->join('products p', 'order_product.product = p.id', 'left')
                ->join('brands b', 'p.brand = b.id', 'left')
                ->join('customers c', 'oi.customer = c.id', 'left')
                ->join('merchants m', 'oi.merchant = m.id', 'left')
                ->join('(SELECT SUM(quantity) quantity, invoice FROM order_product op GROUP BY invoice) op', 'op.invoice = oi.id', 'left')
                // ->where(array('YEAR(oi.date_added)' => date('Y'), 'MONTH(oi.date_added)' => date('m')))
                ->where('m.id',  $this->data['user']->merchant)
                ->limit(10)
                ->order_by('order_product.id DESC');
        return $this->db->get('order_product');
    }

    function last_balances($merchant){
        $this->db->select('mb.*, oi.code invoice_code')
                ->join('order_invoice oi','oi.id = mb.invoice','left')
                ->where('mb.merchant',$merchant)
                ->order_by('mb.id desc')
                ->limit(4);
        return $this->db->get('merchant_balances mb');
    }

    function total_order_by_status($status) {
//        $status_new_order = array(settings('order_payment_received_status'), settings('order_process_status'));
        $this->db->where('merchant', $this->data['user']->merchant)
                ->where('order_status', $status);

        return $this->db->count_all_results('order_invoice oi');
    }

    function last_search() {
        $this->db->select('*, COUNT(keyword) uses')
                ->limit(5)
                ->group_by('keyword')
                ->order_by('time DESC');
        return $this->db->get('search');
    }

    function top_search() {
        $this->db->select('*, COUNT(keyword) uses')
                ->limit(5)
                ->group_by('keyword')
                ->order_by('uses DESC');
        return $this->db->get('search');
    }

    function bestsellers() {
        $status = settings('order_finish_status');
        $this->db->select('op.name, SUM(op.quantity) quantity, op.price, op.discount, p.price, c.name as category_name, p.id,e.name as etalase_name')
                ->join('order_invoice oi', 'op.invoice = oi.id', 'left')
                ->join('products p', 'op.product = p.id', 'left')
                ->join('categories c', 'c.id = p.category', 'left')
                ->join('etalase e', 'e.id = p.etalase', 'left')
                ->limit(5)
                ->group_by('op.product')
                ->where('oi.order_status', $status)
                ->where('p.store_id', $this->data['user']->merchant)
                ->where('p.store_type', 'merchant')
                ->order_by('quantity DESC');
        return $this->db->get('order_product op');
    }

    function bestsellers_principal() {
        $status = settings('order_finish_status');
        $this->db->select('op.name, SUM(op.quantity) quantity, op.price, op.discount, p.price, c.name as category_name, p.id,e.name as etalase_name')
                ->join('order_invoice oi', 'op.invoice = oi.id', 'left')
                ->join('products p', 'op.product = p.id', 'left')
                ->join('categories c', 'c.id = p.category', 'left')
                ->join('etalase e', 'e.id = p.etalase', 'left')
                ->join('products_principal_stock pps','p.id = pps.product_id')
                ->limit(5)
                ->group_by('op.product')
                ->where('oi.order_status', $status)
                ->where('pps.branch_id', $this->data['user']->merchant)
                ->where('p.store_type', 'principal')
                ->order_by('quantity DESC');
        return $this->db->get('order_product op');
    }


    function mostviewed() {
        $this->db->select('p.name, p.price, p.viewed, c.name as category_name, p.id,e.name as etalase_name')
                ->join('categories c', 'c.id = p.category', 'left')
                ->join('etalase e', 'e.id = p.etalase', 'left')
                ->where('p.status', 1)
                ->where('p.store_id', $this->data['user']->merchant)
                ->where('p.store_type', 'merchant')
                ->order_by('p.viewed desc')
                ->limit(5);
        return $this->db->get('products p');
    }

    function mostviewed_principal() {
        $this->db->select('p.name, p.price, p.viewed, c.name as category_name, p.id,e.name as etalase_name')
                ->join('categories c', 'c.id = p.category', 'left')
                ->join('etalase e', 'e.id = p.etalase', 'left')
                ->join('products_principal_stock pps','p.id = pps.product_id')
                ->where('p.status', 1)
                ->where('pps.branch_id', $this->data['user']->merchant)
                ->where('p.store_type', 'principal')
                ->order_by('p.viewed desc')
                ->limit(5);
        return $this->db->get('products p');
    }

}
