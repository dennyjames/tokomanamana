<style>
    .panel {
        border-radius: 5px;
    }
    .tab-content-bordered {
        border-radius: 5px;
    }
    .panel-div {
        width: 100%;
        margin-bottom: 20px;
    }
    .panel-div-left, .panel-div-right {
        width: 45%;
        display: inline-block;
        padding: 0px 25px;
    }
    .panel-div-title {
        font-weight: 500;
        color: #AAA;
    }
    .panel-div-body {
        font-size: 20px;
        font-weight: 500;
    }
    .panel-heading-left, .panel-heading-right {
        display: inline-block;
        width: 45%;
        vertical-align: top;    
    }
    .panel-heading-right {
        text-align: right;  
        color: #AAA;
    }
    a:hover {
        color: #E75A5F;
    }
    @media (max-width: 768px) {.nav-tabs > li {display: table-cell;width: 1%;}.nav-tabs>li>a{border-bottom: 1px solid #ddd !important;border-radius: 4px 4px 0 0 !important;margin-bottom: 0 !important;}}.nav-tabs{padding:0;margin-bottom: 0;}.nav-tabs:before{content: 'Informasi Produk'}.nav-tabs.nav-tabs-highlight>li.active>a, .nav-tabs.nav-tabs-highlight>li.active>a:focus, .nav-tabs.nav-tabs-highlight>li.active>a:hover{border-top-color: #97C23C;}
</style>
<div class="content-wrapper">
    <div class="content">
        <div class="row">
            <div class="col-sm-6">
                <div class="panel">
                    <div class="panel-body">
                        <div class="media no-margin">
                            <div class="media-body">
                                <h3 class="no-margin text-semibold"><?php echo number($order_new); ?></h3>
                                <span class="text-uppercase text-size-mini text-muted">pesanan baru</span>
                            </div>
                        </div>
                    </div>
                    <div class="bg-grey-300 p-15">
                        <h7 class="no-margin text-semibold text-uppercase">status pesanan</h7>
                        <div class="mt-15">
                            <label class="no-margin">Pesanan Dalam Proses</label>
                            <span class="pull-right-sm"><?php echo number($order_process); ?></span>
                        </div>
                        <div class="">
                            <label class="no-margin">Pesanan Dalam Pengiriman</label>
                            <span class="pull-right-sm"><?php echo number($order_shipped); ?></span>
                        </div>
                        <div class="">
                            <label class="no-margin">Pesanan Selesai</label>
                            <span class="pull-right-sm"><?php echo number($order_finish); ?></span>
                        </div>
                        <div class="">
                            <label class="no-margin">Pesanan Ditolak</label>
                            <span class="pull-right-sm"><?php echo number($order_reject); ?></span>
                        </div>
                    </div>
                </div>
                <div class="tab-content-bordered content-group">
                    <ul class="nav nav-tabs nav-tabs-highlight nav-lg nav-justified">
                        <li class="active"><a href="#bestsellers" data-toggle="tab">Terlaris</a></li>
                        <li><a href="#most-viewed" data-toggle="tab">Banyak Dilihat</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane has-padding active" id="bestsellers">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Produk</th>
                                            <th>Etalase</th>
                                            <th>Kategori</th>
                                            <th>Harga</th>
                                            <th>Terjual</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if ($bestsellers->num_rows() > 0) { ?>
                                            <?php foreach ($bestsellers->result() as $bestseller) { ?>
                                                <tr>
                                                    <td>
                                                        <a href="<?php echo site_url('../' . strtolower($this->data['user']->username . '/' . str_replace(' ', '-', $bestseller->category_name) . '/'. str_replace(' ', '-', $bestseller->name))) ?>" target="_blank">
                                                            <?php echo $bestseller->name; ?>
                                                        </a>
                                                    </td>
                                                    <td><?php echo $bestseller->etalase_name ?></td>
                                                    <td><?php echo $bestseller->category_name ?></td>
                                                    <td><?php echo rupiah($bestseller->price) ?></td>
                                                    <td><?php echo number($bestseller->quantity); ?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td class="text-center" colspan="4">Tidak ada data</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane has-padding" id="most-viewed">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Produk</th>
                                            <th>Etalase</th>
                                            <th>Kategori</th>
                                            <th>Harga</th>
                                            <th>Dilihat</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if ($mostviewed->num_rows() > 0) { ?>
                                            <?php foreach ($mostviewed->result() as $viewed) { ?>
                                                <tr>
                                                    <td>
                                                        <a href="<?php echo site_url('../' . strtolower($this->data['user']->username . '/' . str_replace(' ', '-', $viewed->category_name) . '/'. str_replace(' ', '-', $viewed->name))) ?>" target="_blank">
                                                            <?php echo $viewed->name; ?>
                                                        </a>
                                                    </td>
                                                    <td><?php echo $viewed->etalase_name ?></td>
                                                    <td><?php echo $viewed->category_name ?></td>
                                                    <td><?php echo rupiah($viewed->price); ?></td>
                                                    <td><?php echo number($viewed->viewed); ?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td class="text-center" colspan="4">Tidak ada data</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if($sub_account != NULL): ?>
            <div class="col-sm-6">
                <div class="panel">
                    <div class="panel-body">
                        <div class="media no-margin">
                            <div class="media-body">
                                <h3 class="no-margin text-semibold"><?php echo rupiah($balance, 'Rp. '); ?></h3>
                                <span class="text-uppercase text-size-mini text-muted">total saldo</span>
                            </div>
                        </div>
                    </div>
                    <div class="bg-grey-300 p-15">
                        <h7 class="no-margin text-semibold text-uppercase">riwayat terakhir</h7>
                        <?php if ($balances->num_rows() > 0) { ?>
                            <?php foreach ($balances->result() as $key => $b) { ?>
                                <div class="<?php echo $key == 0 ? 'mt-15' : ''; ?>">
                                    <label class="no-margin"><?php echo ($b->type == 'out') ? 'Penarikan' : '#' . $b->invoice_code; ?></label>
                                    <span class="pull-right-sm"><?php echo ($b->type == 'out' ? '-' : '+') . ' ' . number($b->amount); ?></span>
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                        <p class="mt-15">-</p>
                        <?php } ?>
                    </div>
                </div>

                <div class="panel">
                    <div class="panel-heading" style="padding-bottom: 0px;">
                        <div class="panel-heading-left">
                            <h6 class="panel-title" style="font-weight: bold;">Ringkasan Penjualan</h6>
                            <span style="color: #AAA;">Minggu Ini</span>
                        </div>
                        <div class="panel-heading-right">
                            <h6 class="panel-title"><?php echo date('d M Y', strtotime($past_date)) . ' - ' . date('d M Y', strtotime($future_date)) ?></h6>
                        </div>
                    </div>
                    <div class="panel-body" style="padding-top: 0px;">
                        <hr>
                        <div class="panel-div">
                            <div class="panel-div-left">
                                <div class="panel-div-title">
                                    Jumlah Transaksi
                                </div>
                                <div class="panel-div-body">
                                    <?php echo $transaction ?>
                                </div>
                            </div>
                            <div class="panel-div-right">
                                <div class="panel-div-title">
                                    Total Pendapatan Minggu Ini
                                </div>
                                <div class="panel-div-body">
                                    <?php echo rupiah($sales, 'Rp. ') ?>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="chartdiv" style="height: 300px"></div>
                    </div>
                    <div class="panel-footer" style="padding-left: 12px; padding-top: 20px; padding-bottom: 20px;">
                        <a href="<?php echo site_url('reports/sales') ?>" target="_blank">Lihat Selengkapnya ></a>
                    </div>
                </div>
                <input type="hidden" name="merchant_type" id="merchant_type" value="<?= $merchant_type; ?>">
                <!-- <div class="panel">
                    <div class="panel-heading">
                        <h6 class="panel-title" style="font-weight: bold;">10 Pesanan Terakhir</h6>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Pelanggan</th>
                                    <th>Cabang</th>
                                    <th>Status</th>
                                    <th>Jumlah</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($last_orders->num_rows() > 0) { ?>
                                    <?php foreach ($last_orders->result() as $order) { ?>
                                        <?php $status = $this->main->get('setting_order_status', ['id' => $order->status]); ?>
                                        <tr>
                                            <td><?php echo $order->code; ?></td>
                                            <td><?php echo $order->customer_name; ?></td>
                                            <td><?php echo $order->merchant_name; ?></td>
                                            <td><?php echo $status->name; ?></td>
                                            <td style="text-align: center;"><?php echo number($order->quantity); ?></td>
                                            <td><?php echo rupiah($order->total); ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="4">Tidak ada transaksi</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div> -->
            </div>   
            <?php endif; ?>    
        </div>
    </div>
</div>