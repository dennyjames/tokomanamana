<style>
    #chartdiv {
    width: 100%;
    height: 500px;
    }
</style>

<div class="content-wrapper">
    <div class="content">
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <div class="panel-body">
                        <div class="media no-margin">
                            <div class="media-body">
                                <h3 class="no-margin text-semibold"><?php echo number($order_new); ?></h3>
                                <span class="text-uppercase text-size-mini text-muted">pesanan baru</span>
                            </div>
                        </div>
                    </div>
                    <div class="bg-grey-300 p-15">
                        <h7 class="no-margin text-semibold text-uppercase">status pesanan</h7>
                        <div class="mt-15">
                            <label class="no-margin">Pesanan Dalam Proses</label>
                            <span class="pull-right-sm"><?php echo number($order_process); ?></span>
                        </div>
                        <div class="">
                            <label class="no-margin">Pesanan Dalam Pengiriman</label>
                            <span class="pull-right-sm"><?php echo number($order_shipped); ?></span>
                        </div>
                        <div class="">
                            <label class="no-margin">Pesanan Selesai</label>
                            <span class="pull-right-sm"><?php echo number($order_finish); ?></span>
                        </div>
                        <div class="">
                            <label class="no-margin">Pesanan Ditolak</label>
                            <span class="pull-right-sm"><?php echo number($order_reject); ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="panel">
                    <div class="panel-body">
                        <div class="media no-margin">
                            <div class="media-body">
                                <h3 class="no-margin text-semibold"><?php echo number($balance); ?></h3>
                                <span class="text-uppercase text-size-mini text-muted">saldo</span>
                            </div>
                        </div>
                    </div>
                    <div class="bg-grey-300 p-15">
                        <h7 class="no-margin text-semibold text-uppercase">riwayat terakhir</h7>
                        <?php if ($balances->num_rows() > 0) { ?>
                            <?php foreach ($balances->result() as $key => $b) { ?>
                                <div class="<?php echo $key == 0 ? 'mt-15' : ''; ?>">
                                    <label class="no-margin"><?php echo ($b->type == 'out') ? 'Penarikan' : '#' . $b->invoice_code; ?></label>
                                    <span class="pull-right-sm"><?php echo ($b->type == 'out' ? '-' : '+') . ' ' . number($b->amount); ?></span>
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                        <p class="mt-15">-</p>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="panel">
                    <div class="panel-heading pt-5 pb-5">
                        <h5 class="panel-title">PEMBERITAHUAN PENTING</h5>
                    </div>
                    <div class="panel-body">
                        <?php if ($announcements) { ?>
                            <ul class="media-list media-list-bordered">
                                <?php foreach ($announcements->result() as $announcement) { ?>
                                    <li class="media">
                                        <div class="media-body">
                                            <h6 class="media-heading"><a href="<?php echo site_url('announcements/view/' . encode($announcement->id)); ?>"><?php echo $announcement->subject; ?></a></h6>
                                            <p><?php echo character_limiter(stripHTMLtags($announcement->content), 100, '...'); ?></p>
                                            <div class="media-annotation mt-5"><?php echo get_date($announcement->date_added); ?></div>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } else { ?>
                            <p>Tidak ada pemberitahuan terbaru.</p>
                        <?php } ?>
                    </div>
                </div>
                <!-- <div class="panel">
                    <div class="panel-heading pt-5 pb-5">
                        <h5 class="panel-title">Add to Favourite Shop</h5>
                    </div>
                    <div class="panel-body">
                        <ul class="media-list media-list-bordered">
                            <li class="media">
                                <div class="media-body">
                                    <p>
                                    <div class="input-group">
                                        <input type="text" id="myInput" class="form-control"  value="<?php //echo $copy_link;?>" readonly="">
                                        <span class="input-group-btn">
                                            <button type="button"  onclick="copyLink()" class="btn btn-default">
                                            Copy Link</button>
                                        </span>
                                    </div>
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div> -->
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="panel panel-flat">
                    <div class="panel-heading pt-5 pb-5">
                        <h5 class="panel-title">Statistik Pesanan</h5>
                    </div>
                    <div class="panel-body">
                        <div id="chartdiv"></div>
                    </div>
                </div>
            </div>
            <!-- <div class="col-sm-12 col-md-6">
                <div class="panel panel-flat">
                    <div class="panel-heading pt-5 pb-5">
                        <h5 class="panel-title">Statistik Pesanan</h5>
                    </div>
                    <div class="panel-body">
                        <div class="chart-container">
                            <div class="chart" id="chart-order"></div>
                        </div>
                    </div>
                </div>
            </div> -->
            <!--            <div class="col-sm-12 col-md-6">
            
                        </div>-->
        </div>
    </div>
</div>