<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('dashboard_model', 'dashboard');
        $this->data['menu'] = 'dashboard';
    }

    public function index() {
        $this->load->helper('text');
        $this->data['sess_merchant'] = $this->session->userdata('merchant_user');
        $this->data['copy_link'] = str_replace("/ma","",base_url()).'member/favourite_shop/add/'.encode_id($this->data['sess_merchant']->merchant); 
        $this->data['order_new'] = $this->dashboard->total_order_by_status(settings('order_payment_received_status'));
        $this->data['order_process'] = $this->dashboard->total_order_by_status(settings('order_process_status'));
        $this->data['order_cancel'] = $this->dashboard->total_order_by_status(settings('order_cancel_status'));
        $this->data['order_reject'] = $this->dashboard->total_order_by_status(settings('order_rejected_status'));
        $this->data['order_shipped'] = $this->dashboard->total_order_by_status(settings('order_shipped_status'));
        $this->data['order_finish'] = $this->dashboard->total_order_by_status(settings('order_finish_status'));
        $this->data['announcements'] = $this->main->gets('merchant_announcements', [], 'id desc', null, 5);
        $this->data['balance'] = $this->main->get('merchants',array('id'=> $this->data['user']->merchant))->cash_balance;
        $this->data['balances'] = $this->dashboard->last_balances($this->data['user']->merchant);

        $this->template->_init();
        $this->load->js('https://www.amcharts.com/lib/4/core.js');
        $this->load->js('https://www.amcharts.com/lib/4/charts.js');
        $this->load->js('https://www.amcharts.com/lib/4/themes/animated.js');
        $this->load->js('../assets/backend/js/plugins/visualization/d3/d3.min.js');
        $this->load->js('../assets/backend/js/plugins/visualization/c3/c3.min.js');
        $this->load->js(base_url('../').'assets/backend/js/modules/dashboard.js');
        $this->output->set_title('Dasbor');
        $this->load->view('dashboard', $this->data);
    }

    public function chart_order() {
        $date = array('x');
        $completed = array('Selesai');
        $progress = array('Belum Selesai');
        $rejected = array('Ditolak');
        $datas = $this->dashboard->order_status_monthly($this->data['user']->merchant);
        $row = 0;
        for ($i = 1; $i <= date('t'); $i++) {
            $data = $datas->row_array($row);
            if ($data['date'] == $i) {
                $row++;
            } else {
                $data['finish'] = 0;
                $data['progress'] = 0;
                $data['reject'] = 0;
            }
            array_push($date, $i);
            array_push($progress, $data['progress']);
            array_push($rejected, $data['reject']);
            array_push($completed, $data['finish']);
        }
        $output = array();
        array_push($output, $date);
        array_push($output, $completed);
        array_push($output, $progress);
        array_push($output, $rejected);
        echo json_encode($output);
    }

    public function chart_order_stats() {
        $data = array('finish' => '0','progress' => '0','reject' => '0',);
        $month = date('m');
        $year = date('Y');
        $datas = $this->dashboard->order_status_chart($this->data['user']->merchant,$month,$year);
       if($datas->num_rows() > 0) {
            $data = $datas->result()[0];
       }
        echo json_encode($data);
    }

}
