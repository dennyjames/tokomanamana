<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('dashboard_model','dashboard');
        $this->data['menu'] = 'dashboard';
    }

    public function index() {
        
        if($this->data['user']->merchant_type == 'merchant'){
            $summaries = $this->dashboard->sales_summary();
            $this->data['bestsellers'] = $this->dashboard->bestsellers();
            $this->data['mostviewed'] = $this->dashboard->mostviewed();
        }
        else if($this->data['user']->merchant_type == 'principle_branch'){
            $summaries = $this->dashboard->sales_summary_principal();
            $this->data['bestsellers'] = $this->dashboard->bestsellers_principal();
            $this->data['mostviewed'] = $this->dashboard->mostviewed_principal();
        }
        $transaction = 0;
        $sales = 0;
        $today = date('Y-m-d');
        $past_date = date('Y-m-d', strtotime('-6 day'));
        $future_date = $today;
        foreach($summaries as $summary) {
            $date_summary = date('Y-m-d', strtotime($summary->date_summary));
            if($past_date <= $today && $future_date >= $today) {
                $transaction += $summary->transaction;
                $sales += $summary->sales;
            }
        }
        //sub account to check if main account or not
        $this->data['sub_account'] = $this->data['user']->id;
        $this->data['merchant_type'] = $this->data['user']->merchant_type;
        $this->data['past_date'] = $past_date;
        $this->data['future_date'] = $future_date;
        $this->data['transaction'] = $transaction;
        $this->data['sales'] = $sales;
        $this->data['last_orders'] = $this->dashboard->last_orders();
        $this->data['order_new'] = $this->dashboard->total_order_by_status(settings('order_payment_received_status'));
        $this->data['order_process'] = $this->dashboard->total_order_by_status(settings('order_process_status'));
        $this->data['order_cancel'] = $this->dashboard->total_order_by_status(settings('order_cancel_status'));
        $this->data['order_reject'] = $this->dashboard->total_order_by_status(settings('order_rejected_status'));
        $this->data['order_shipped'] = $this->dashboard->total_order_by_status(settings('order_shipped_status'));
        $this->data['order_finish'] = $this->dashboard->total_order_by_status(settings('order_finish_status'));

//        $this->data['last_search'] = $this->dashboard->last_search();
//        $this->data['top_search'] = $this->dashboard->top_search();
        
        
 
        $this->data['balance'] = $this->main->get('merchants',array('id'=> $this->data['user']->merchant))->cash_balance;
        $this->data['balances'] = $this->dashboard->last_balances($this->data['user']->merchant);
        
        $this->template->_init();
        $this->load->js('https://www.amcharts.com/lib/4/core.js');
        $this->load->js('https://www.amcharts.com/lib/4/charts.js');
        $this->load->js('https://www.amcharts.com/lib/4/themes/animated.js');
        $this->load->js('https://www.amcharts.com/lib/4/themes/kelly.js');
        $this->load->js('../assets/backend/js/plugins/visualization/d3/d3.min.js');
        $this->load->js('../assets/backend/js/plugins/visualization/c3/c3.min.js');
        $this->load->js('../assets/backend/js/merchant/dashboard.js');
        $this->output->set_title('Dasbor');
        $this->load->view('dashboard', $this->data);
    }

    public function sales_summary() {
        $data = array('finish' => '0','progress' => '0','reject' => '0',);
        $month = date('m');
        $year = date('Y');
        if($this->data['user']->merchant_type == 'merchant'){
            $datas = $this->dashboard->sales_summary();
        }
        else if($this->data['user']->merchant_type == 'principle_branch'){
            $datas = $this->dashboard->sales_summary_principal();
        }
        $data = [];
        foreach($datas as $key => $as) {
            $data[] = $as;
        }
        
        echo json_encode($data);
    }

}
