<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['downline_heading'] = 'Downline';
$lang['downline_list_heading'] = 'Daftar Downline';
$lang['downline_add_heading'] = 'Tambah Downline';
$lang['downline_edit_heading'] = 'Edit Downline';
$lang['view_heading'] = 'View Downline';

$lang['data_tabs'] = 'Data';
$lang['name'] = 'Nama';
$lang['phone'] = 'Telepon';
$lang['address'] = 'Alamat';

$lang['downline_code_th'] = 'ID';
$lang['downline_date_th'] = 'Tanggal';
$lang['downline_customer_th'] = 'Merchant';
$lang['downline_status_th'] = 'Status';
$lang['downline_total_th'] = 'Total';
$lang['downline_shipping_th'] = 'Pengiriman';

$lang['downline_button_paid'] = '<i class="icon-cash"></i> Pembayaran Diterima';

$lang['downline_save_success_message'] = "Downline '%s' berhasil disimpan.";
$lang['downline_save_error_message'] = "Downline '%s' gagal disimpan.";
$lang['downline_delete_success_message'] = "Downline '%s' berhasil dihapus.";
$lang['downline_delete_error_message'] = "Downline '%s' gagal dihapus.";