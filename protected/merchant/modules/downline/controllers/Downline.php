<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Downline extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->lang->load('downline', settings('language'));
        $this->load->model('downline_model', 'downline');

        $this->lang->load('ion_auth', settings('language'));
        $this->lang->load('auth', settings('language'));
        $this->load->library('recaptcha');
        $this->data['menu'] = 'downline';
    }

    public function index() {
        $user = $this->session->userdata('merchant_user');
        if($this->aauth->is_loggedin()){
            $this->aauth->control('downline');
        }
        $this->template->_init();
        $this->template->table();
        $this->load->js('../assets/backend/js/modules/orders.js');

        $this->breadcrumbs->push(lang('menu_order'), '/downline');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('downline_heading'));
        $this->load->view('list', $this->data);
    }

    public function form($id = '') {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', 'lang:email', 'required|valid_email|callback_check_email');
            $this->form_validation->set_rules('password', 'lang:password', 'required|min_length[6]');
            $this->form_validation->set_rules('confirm_password', 'lang:confirm_password', 'required|matches[password]');
            $this->form_validation->set_rules('owner_name', 'lang:owner_name', 'required');
            $this->form_validation->set_rules('owner_phone', 'lang:owner_phone', 'required|numeric');
            $this->form_validation->set_rules('verified_phone', 'lang:verified_phone', 'required');
            $this->form_validation->set_rules('owner_birthday', 'lang:owner_birthday_day_placeholder', 'required');
            $this->form_validation->set_rules('owner_id', 'lang:owner_id', 'required');
            $this->form_validation->set_rules('store_name', 'lang:store_name', 'required');
            $this->form_validation->set_rules('store_telephone', 'lang:store_telephone', 'required|numeric');
            $this->form_validation->set_rules('store_address', 'lang:store_address', 'required');
            $this->form_validation->set_rules('store_province', 'lang:store_province', 'required');
            $this->form_validation->set_rules('store_city', 'lang:store_city', 'required');
            $this->form_validation->set_rules('store_district', 'lang:store_district', 'required');
            $this->form_validation->set_rules('store_postcode', 'lang:store_postcode', 'required');
            $this->form_validation->set_rules('bank_name', 'lang:bank_name', 'required');
            $this->form_validation->set_rules('bank_branch', 'lang:bank_branch', 'required');
            $this->form_validation->set_rules('bank_account_number', 'lang:bank_account_number', 'required');
            $this->form_validation->set_rules('bank_account_name', 'lang:bank_account_name', 'required');
            $this->form_validation->set_rules('agree', 'lang:terms', 'required', lang('terms_required'));
            $this->form_validation->set_rules('g-recaptcha-response', 'lang:captcha', 'required|callback_check_recaptcha');
        if ($this->input->is_ajax_request()) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', 'lang:email', 'required|valid_email|callback_check_email');
            $this->form_validation->set_rules('password', 'lang:password', 'required|min_length[6]');
            $this->form_validation->set_rules('confirm_password', 'lang:confirm_password', 'required|matches[password]');
            $this->form_validation->set_rules('owner_name', 'lang:owner_name', 'required');
            $this->form_validation->set_rules('owner_phone', 'lang:owner_phone', 'required|numeric');
            $this->form_validation->set_rules('verified_phone', 'lang:verified_phone', 'required');
            $this->form_validation->set_rules('owner_birthday', 'lang:owner_birthday_day_placeholder', 'required');
            $this->form_validation->set_rules('owner_id', 'lang:owner_id', 'required');
            $this->form_validation->set_rules('store_name', 'lang:store_name', 'required');
            $this->form_validation->set_rules('store_telephone', 'lang:store_telephone', 'required|numeric');
            $this->form_validation->set_rules('store_address', 'lang:store_address', 'required');
            $this->form_validation->set_rules('store_province', 'lang:store_province', 'required');
            $this->form_validation->set_rules('store_city', 'lang:store_city', 'required');
            $this->form_validation->set_rules('store_district', 'lang:store_district', 'required');
            $this->form_validation->set_rules('store_postcode', 'lang:store_postcode', 'required');
            $this->form_validation->set_rules('bank_name', 'lang:bank_name', 'required');
            $this->form_validation->set_rules('bank_branch', 'lang:bank_branch', 'required');
            $this->form_validation->set_rules('bank_account_number', 'lang:bank_account_number', 'required');
            $this->form_validation->set_rules('bank_account_name', 'lang:bank_account_name', 'required');
            $this->form_validation->set_rules('agree', 'lang:terms', 'required', lang('terms_required'));
            $this->form_validation->set_rules('g-recaptcha-response', 'lang:captcha', 'required|callback_check_recaptcha');
            if ($this->form_validation->run() == true) {
                $data = $this->input->post(null, true);
                $data['shipping'] = json_encode($data['shipping']);
                do {
                    $birthday = date('Y-m-d',strtotime($data['owner_birthday']));
                    $user = $this->ion_auth->register($data['email'], $data['password'], $data['email'], array('fullname' => $data['owner_name'], 'phone' => $data['owner_phone'],'verification_phone' => 1, 'birthday' => $birthday, 'id_number' => $data['owner_id']), array(3));
                    if (!$user) {
                        $return = array('message' => $this->ion_auth->errors(), 'status' => 'error');
                        break;
                    }
                    $this->main->insert('merchants', array(
                        'auth' => $user,
                        'type' => 'merchant',
                        'name' => $data['store_name'],
                        'address' => $data['store_address'],
                        'province' => $data['store_province'],
                        'city' => $data['store_city'],
                        'district' => $data['store_district'],
                        'postcode' => $data['store_postcode'],
                        'telephone' => $data['store_telephone'],
                        'lat' => $data['lat'],
                        'lng' => $data['lng'],
                        'bank_name' => $data['bank_name'],
                        'bank_branch' => $data['bank_branch'],
                        'bank_account_number' => $data['bank_account_number'],
                        'bank_account_name' => $data['bank_account_name'],
                        'shipping' => $data['shipping'],
                        'status' => 0,
                        'parent' => $this->data['user']->merchant,
                    ));
                    $this->data['name'] = $data['owner_name'];
                    $message = $this->load->view('email/register_confirmation', $this->data, TRUE);
                    $cronjob = array(
                        'from' => settings('send_email_from'),
                        'from_name' => settings('store_name'),
                        'to' => $data['email'],
                        'subject' => 'Konfirmasi Pendaftaran Merchant ' . settings('store_name'),
                        'message' => $message
                    );
                    $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
//                    send_mail('noreply@tokomanamana.com', $data['email'], 'Konfirmasi Pendaftaran Merchant ' . settings('store_name'), $message);
                    $return = array('message' => lang('register_successful'), 'status' => 'success', 'redirect' => site_url('login'));
                } while (0);
            } else {
                $return = array('message' => validation_errors(), 'status' => 'error');
            }
            echo json_encode($return);
        } else {
            $this->data['page'] = 'register';
            $this->data['provincies'] = $this->main->gets('provincies', [], 'name asc');
            $this->data['widget'] = $this->recaptcha->getWidget();
            $this->data['script'] = $this->recaptcha->getScriptTag();

            $this->load->library('googlemaps');
            $config['center'] = '-6.137325741920823,106.83344066563723';
            $config['loadAsynchronously'] = TRUE;
            $config['map_height'] = '500px';
            $config['onboundschanged'] = 'if (!centreGot) {
                var mapCentre = map.getCenter();
                marker_0.setOptions({
                        position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng()) 
                });
                setLocation(mapCentre.lat(), mapCentre.lng());
            }centreGot = true;';
            $config['disableFullscreenControl'] = TRUE;
            $config['disableMapTypeControl'] = TRUE;
            $config['disableStreetViewControl'] = TRUE;
            $config['places'] = TRUE;
            $config['placesAutocompleteInputID'] = 'search-location';
            $config['placesAutocompleteBoundsMap'] = TRUE; // set results biased towards the maps viewport
            $config['placesAutocompleteOnChange'] = 'map.setCenter(this.getPlace().geometry.location); 
            marker_0.setOptions({
                        position: new google.maps.LatLng(this.getPlace().geometry.location.lat(), this.getPlace().geometry.location.lng()) 
                });
                setLocation(this.getPlace().geometry.location.lat(), this.getPlace().geometry.location.lng());';
            $this->googlemaps->initialize($config);
            $marker = array();
            $marker['draggable'] = true;
            $marker['ondragend'] = 'setLocation(event.latLng.lat(), event.latLng.lng());';
            $this->googlemaps->add_marker($marker);
            $this->data['map'] = $this->googlemaps->create_map();

            $this->template->_init();
            $this->template->form();
            $this->output->set_title('Merchant Application');
            $this->load->css('../assets/frontend_1/css/libs/bootstrap-datepicker3.min.css');
            $this->load->js('../assets/frontend_1/js/bootstrap-datepicker.min.js');
            //$this->load->js('https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js');
            $this->load->js(base_url('../').'assets/backend/js/merchant/register.js');
            $this->load->view('form', $this->data);
        }
    }

    public function view($id = '') {
        $id = decode($id);
        $this->data['merchant'] = $this->downline->merchant($id);
        $this->data['data'] = array();

        $this->breadcrumbs->push(lang('downline'), '/downline');
        $this->breadcrumbs->push(lang('view_heading'), '/');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->form();
        //$this->load->js('../assets/backend/js/modules/merchants/form.js');
        $this->output->set_title(lang('view_heading'));
        $this->load->view('view', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->downline->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    '<a href="' . site_url('downline/view/' . encode($data->id)) . '">' . $data->id . '</a>',
                    $data->name,
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                    '<li><a href="' . site_url('downline/view/' . encode($data->id)) . '">' . lang('button_view') . '</a></li>
                    </ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->downline->count_all();
        $output['recordsFiltered'] = $this->downline->count_all($search);
        echo json_encode($output);
    }

    public function check_email($email) {
        if ($this->main->get('merchant_users', ['email' => $email])) {
            $this->form_validation->set_message('check_email', sprintf(lang('email_registered'), $email));
            return false;
        } else {
            return true;
        }
    }

    public function check_recaptcha() {
        $recaptcha = $this->input->post('g-recaptcha-response');
        $response = $this->recaptcha->verifyResponse($recaptcha);
        if (isset($response['success']) && $response['success'] == true) {
            return true;
        } else {
            $this->form_validation->set_message('check_recaptcha', lang('capctcha_failed'));
        }
    }

}
