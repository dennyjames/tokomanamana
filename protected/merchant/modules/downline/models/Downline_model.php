<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Downline_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('m.*')
                ->where('m.parent', $this->data['user']->merchant)
                ->limit($length, $start);

        return $this->db->get('merchants m');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'm.id';
                break;
            case 1: $key = 'm.name';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->_where_like($search);
        $this->db->where('m.parent', $this->data['user']->merchant);
        return $this->db->count_all_results('merchants m');
    }

    private function _where_like($search = '') {
        $columns = array('m.id', 'm.name');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    function get($id) {
        $this->db->select('m.*')
        ->where('m.parent', $this->data['user']->merchant);
        $query = $this->db->get('merchants m');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }
    
    function merchant($id) {
        $this->db->select('m.*, p.name province_name, c.name city_name, d.name district_name')
                ->join('provincies p', 'p.id = m.province', 'left')
                ->join('cities c', 'c.id = m.city', 'left')
                ->join('districts d', 'd.id = m.district', 'left')
                ->where('m.id', $id);
        $query = $this->db->get('merchants m');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

}
