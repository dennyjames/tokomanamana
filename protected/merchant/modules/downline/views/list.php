<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2>Downline</h2>
            </div>

           <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('downline/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span>Tambah</span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table table-hover" id="table" data-url="<?php echo site_url('downline/get_list'); ?>" data-state-save="false">
                <thead>
                    <tr>
                        <th><?php echo lang('downline_code_th'); ?></th>
                        <th><?php echo lang('downline_customer_th'); ?></th>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions_th'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>