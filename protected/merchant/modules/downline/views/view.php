<div class="content-wrapper">
    <!-- Page Header -->
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('view_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('downline'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('view_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
     <!-- END Page Header -->
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#data" data-toggle="tab"><?php echo lang('data_tabs'); ?></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="data">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="name"><?php echo lang('name'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" required="" id="name" name="name" placeholder="<?php echo lang('name_placeholder'); ?>" value="<?php echo ($merchant) ? $merchant->name : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="name"><?php echo lang('phone'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" required="" id="phone" name="phone" placeholder="<?php echo lang('phone_placeholder'); ?>" value="<?php echo ($merchant) ? $merchant->telephone : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('address'); ?></label>
                                            <div class="col-md-9">
                                                <textarea cols="30" rows="2" class="form-control" id="address" name="address"><?php echo ($merchant) ? $merchant->address : ''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>