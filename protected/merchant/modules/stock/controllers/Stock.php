<?php

defined('BASEPATH') or exit('No direct script access allowed!');
require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Stock extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('stock_model', 'stock');
        $this->data['menu'] = 'stock';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();
        $this->template->form();
        $this->load->js('../assets/principle/js/merchant/modules/stock/stock.js');

        $this->breadcrumbs->unshift('Stok', '/products');
        $this->breadcrumbs->push('Daftar Stok Produk', '/stock');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title('Stok Produk');
        $this->load->view('list', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));
        $branch_id = $this->data['user']->merchant;

        $output['data'] = array();
        $datas = $this->stock->get_all($branch_id, $start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = [
                    $data->code, 
                    $data->name,
                    ($data->variation_status) ? $this->get_variation($data->variation) : '-',
                    ($data->old_quantity == null ? 0 : $data->old_quantity),
                    $data->new_quantity,
                    ($data->status == 1 ? '<button class="btn btn-primary" onclick="change_confirm(\'' . encode($data->id) . '\', ' . $data->new_quantity . ')">Konfirmasi!</button>' : '<i>Telah Terkonfirmasi</i>'),
                    ($data->type == 'in' ? 'Masuk' : 'Keluar'),
                    $data->date_added
                ];
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->stock->count_all();
        $output['recordsFiltered'] = $this->stock->count_all($search);
        echo json_encode($output);
    }

    private function get_variation($id) {
        $variations = $this->stock->get_variations($id);
        $str = '';
        foreach($variations->result() as $variation) {
            $str .= $variation->type . ' ' . $variation->value . ' ';
        }
        return $str;
    }

    public function confirm() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $branch_id = $this->data['user']->merchant;
        
        $stock_history = $this->stock->get_history(decode($data['id']), $branch_id);
        
        if($stock_history) {
            $update_confirm = $this->main->update('products_principal_history', ['status' => 2], ['id' => decode($data['id'])]);
            if($update_confirm) {
                $product_id = $stock_history->product_id;
                $product = $this->main->get('products', ['id' => $product_id, 'store_type' => 'principal']);
                $category = $this->main->get('categories', ['id' => $product->category]);
                $branch = $this->main->get('merchants', ['id' => $branch_id, 'type' => 'principle_branch']);
                $new_quantity = $stock_history->quantity;
                if($product) {
                    if($product->variation) {
                        $product_branch_variation = $this->main->get('products_principal_stock', ['branch_id' => $branch_id, 'product_id' => $product_id, 'id_option' => $stock_history->id_option]);
                        $product_option = $this->main->get('product_option', ['id' => $stock_history->id_option]);

                        // create seo url
                        if(!$this->main->get('products_principal_stock', ['branch_id' => $branch_id, 'product_id' => $product_id])) {
                            $branch_profile = $this->main->get('merchant_users', ['id' => $branch->auth]);
                            $branch_name = strtolower($branch_profile->username);
                            $branch_name = str_replace(' ', '-', $branch_name);
                            $category_name = strtolower($category->name);
                            $category_name = str_replace(' ', '-', $category_name);
                            $product_name = strtolower($product->name);
                            $product_name = str_replace(' ', '-', $product_name);
                            $seo_url = $branch_name . '/' . $category_name . '/' . $product_name;
                            $data_url = [
                                'query' => 'catalog/products/view/' . $product_id . '/' . $branch_id,
                                'keyword' => $seo_url
                            ];
                            $this->main->insert('seo_url', $data_url);
                        }

                        if($product_branch_variation) {
                            $old_quantity = $product_branch_variation->quantity;
                            $quantity = $old_quantity + $new_quantity;
                            $this->main->update('products_principal_stock', ['quantity' => $quantity], ['product_id' => $product_id, 'branch_id' => $branch_id, 'id_option' => $stock_history->id_option]);
                        } else {
                            $data = [
                                'branch_id' => $branch_id,
                                'product_id' => $product_id,
                                'quantity' => $new_quantity,
                                'id_option' => $stock_history->id_option
                            ];
                            $this->main->insert('products_principal_stock', $data);
                        }

                        $quantity_product = $product_option->quantity;
                        $new_quantity_product = $quantity_product - $new_quantity;
                        $this->main->update('product_option', ['quantity' => $new_quantity_product], ['id' => $product_option->id]);
                        $return = [
                            'status' => 'success',
                            'message' => 'Stok Produk Bertambah!'
                        ];

                    } else {
                        $product_branch = $this->stock->get_product_branch($branch_id, $product_id);
                        if($product_branch) {
                            $old_quantity = $product_branch->quantity;
                            $quantity = $old_quantity + $new_quantity;
                            $this->main->update('products_principal_stock', ['quantity' => $quantity], ['product_id' => $product_id, 'branch_id' => $branch_id]);
                        } else {
                            $branch_profile = $this->main->get('merchant_users', ['id' => $branch->auth]);
                            $branch_name = strtolower($branch_profile->username);
                            $branch_name = str_replace(' ', '-', $branch_name);
                            $category_name = strtolower($category->name);
                            $category_name = str_replace(' ', '-', $category_name);
                            $product_name = strtolower($product->name);
                            $product_name = str_replace(' ', '-', $product_name);
                            $seo_url = $branch_name . '/' . $category_name . '/' . $product_name;
                            $data_url = [
                                'query' => 'catalog/products/view/' . $product_id . '/' . $branch_id,
                                'keyword' => $seo_url
                            ];
                            $data = [
                                'branch_id' => $branch_id,
                                'product_id' => $product_id,
                                'quantity' => $new_quantity
                            ];
                            $this->main->insert('products_principal_stock', $data);
                            $this->main->insert('seo_url', $data_url);
                        }
                        $quantity_product = $product->quantity;
                        $new_quantity_product = $quantity_product - $new_quantity;
                        $this->main->update('products', ['quantity' => $new_quantity_product], ['id' => $product_id, 'store_type' => 'principal']);
                        $return = [
                            'status' => 'success',
                            'message' => 'Stok Produk Bertambah!'
                        ];
                    }
                } else {
                    $return = [
                        'status' => 'error',
                        'message' => 'Terjadi Suatu Kesalahan'
                    ];
                }
            } else {
                $return = [
                    'status' => 'error',
                    'message' => 'Terjadi Suatu Kesalahan'
                ];
            }
        } else {
            $return = [
                'status' => 'error',
                'message' => 'Terjadi Suatu Kesalahan'
            ];
        }
        
        echo json_encode($return);
    }

    public function not_corresponding() {
        $new_stock = $this->input->post('new_stock', true);
        $id = decode($this->input->post('id'));
        $update = $this->main->update('products_principal_history', ['quantity_revisi' => $new_stock], ['id' => $id]);
        if($update) {
            $return = [
                'status' => 'success',
                'message' => 'Stok baru berhasil dikirim'
            ];
        } else {
            $return = [
                'status' => 'error',
                'message' => 'Terjadi suatu kesalahan!'
            ];
        }

        echo json_encode($return);
    }

}
