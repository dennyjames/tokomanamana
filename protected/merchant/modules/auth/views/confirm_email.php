
<style>
    .wrapper {
        position: relative;
        width: 350px;
        height: 200px;
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    body {
        background-image: url('<?php echo site_url("../files/images/bg_register.jpeg") ?>');
        background-size: cover;
        background-attachment: fixed;
    }

    .btn-success{
        background-color: #97C23C;
        border-color: #97C23C;
    }
    .btn-success:hover{
        background-color: #84a936;
        border-color: #84a936;
    }


    @media screen and (max-width: 480px) {
        .wrapper {
            width: 230px;
            height: 150px;
        }

</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<div class="content">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <br><br>
            <div class="panel panel-body">
                <div class="login_page_wrapper">
                    <div class="md-card" id="login_card">
                        <div class="md-card-content large-padding">
                            <img src="<?php echo site_url('../assets/frontend/images/logo.png'); ?>" alt="" style="margin:10px 0; height: 50px;">
                            <hr>
                            <center><h1 class="heading_a uk-margin-medium-bottom uk-text-center"><?=$header?></h1></center>
                            <div id="message"></div>

                            <p><?=$message?></p>    

                        </div>
                    </div>
                    
                </div>
                <div><a href="<?php echo site_url('login'); ?>">Kembali ke halaman Login</a></div>
            </div>
        </div>
    </div>

</div>


