
<style>
    .wrapper {
        position: relative;
        width: 350px;
        height: 200px;
        -moz-user-select: none;
        -webkit-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

     .btn-success{
        background-color: #97C23C;
        border-color: #97C23C;
    }
    .btn-success:hover{
        background-color: #84a936;
        border-color: #84a936;
    }

    body {
        background-image: url('<?php echo site_url("../files/images/bg_register.jpeg") ?>');
        background-size: cover;
        background-attachment: fixed;
    }


    @media screen and (max-width: 480px) {
        .wrapper {
            width: 230px;
            height: 150px;
        }

</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<div class="content">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <br><br>
            <div class="panel panel-body">
                <div class="login_page_wrapper">
                    <div class="md-card" id="login_card">
                        <div class="md-card-content large-padding">
                            <img src="<?php echo site_url('../assets/frontend/images/logo.png'); ?>" alt="" style="margin:10px 0; height: 50px;">
                            <hr>
                            <center><h1 class="heading_a uk-margin-medium-bottom uk-text-center">Berhasil Reset Password Tokomanamana</h1></center>
                            <div id="message"></div>
                            <form id="forgot-password_success" method="post">
                                <p>Silahkan masukkan password lama dan baru anda.</p>                    
                               <!--  <div class="form-group">
                                    <label class="col-md-3 control-label" style="margin: 0px;padding: 10px">Password Lama</label>
                                    <div class="col-md-9">
                                        <input type="password" id="old_password" class="form-control" required="" name="old_password" placeholder="Masukkan Password Lama" aria-required="true">
                                    </div>
                                </div>
                                <br><br><br> -->
                                <div class="form-group">
                                    <label class="col-md-3 control-label" style="margin: 0px;padding: 10px">Password Baru</label>
                                    <div class="col-md-9">
                                        <input type="password" id="new_password" class="form-control" required="" name="new_password" placeholder="Masukkan Password Baru" aria-required="true">
                                    </div>
                                </div>
                                <br><br>
                                <div class="form-group">
                                    <label class="col-md-3 control-label" style="margin: 0px;padding: 10px">Confirm Password Baru</label>
                                    <div class="col-md-9">
                                        <input type="password" id="new_password_confirm" class="form-control" required="" name="new_password_confirm" placeholder="Ulangi Password Baru" aria-required="true">
                                    </div>
                                </div>
                                <br><br>
                                <div class="uk-margin-medium-top" style="text-align: right;margin-right: 10px">
                                    <button class="button btn btn-success" type="submit">Ganti Kata Sandi</button>
                                </div>
                            </form>


                                                  
                                
                            
                        </div>
                    </div>
                    
                </div>
                <div>Sudah punya akun?? <a href="<?php echo site_url('login'); ?>">Login disini!</a></div>
            </div>
        </div>
    </div>

</div>


