<style>
.wrapper {
  position: relative;
  width: 250px;
  height: 180px;
  -moz-user-select: none;
  -webkit-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

.signature-pad {
  position: absolute;
  left: 0;
  top: 0;
  width:250px;
  height:180px;
  background-color: #ccc;
}
.btn-clear-canvas {
  position: relative;
  width:250px;
  text-align:right;  
}
@media screen and (max-width: 480px) {
    .wrapper {
        width: 250px;
        height: 150px;
    }
    .signature-pad {
        width: 250px;
        height: 150px;
    }
    .btn-clear-canvas {
        width: 250px;
    }
}
</style>
<div class="content pb-20">
    <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
        <div class="panel panel-body login-form">
            <div class="text-center">
                <h5 class="content-group-lg">Verifikasi Signature</h5>
            </div>
            <?php
            if(isset($message_error)){
            ?>
            <div class="alert alert-danger" role="alert">
                <?php echo $message_error;?>
            </div>
            <?php } ?>
            <div class="form-group">
                <label class="col-md-12 control-label">Signature</label>
                <div class="col-md-12">
                    <div class="wrapper">
                        <canvas id="signature-pad" class="signature-pad"></canvas>
                        <input type="hidden" id="base64_img" name="base64_img" required="">
                    </div>
                    <div class="btn-clear-canvas">
                        <a href="javascript:void(0)" class="btn btn-default" id="clear-canvas">Clear</a>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="checkbox">
                    <label>
                        <a href="#terms" data-toggle="modal" data-target="#terms">
                            <input id="agreeSnk" type="checkbox" name="agree" required="">
                        <?php echo lang('terms_prefix'); ?> <?php echo lang('terms'); ?></a>
                    </label>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" id="verif_submit" class="btn bg-blue btn-block">Verifikasi</button>
            </div>
        </div>
    </form>
</div>
<div id="terms" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo lang('terms'); ?></h5>
            </div>
            <div class="modal-body">
                <?php echo settings('term_merchant_register'); ?>
                <div class="row">
                    <div class="pull-right">
                        <button type="submit" onclick="agreeSnk()" class="btn btn-success">Setuju <i class="icon-check position-right"></i></button>
                        <button type="submit" onclick="closeSnKmodal()" class="btn btn-danger">Tidak Setuju <i class="icon-cross position-right"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>