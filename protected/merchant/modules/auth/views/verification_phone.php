<div class="content pb-20">
    <form action="<?php echo current_url(); ?>" method="post">
        <div class="panel panel-body login-form">
            <div class="text-center">
                <h5 class="content-group-lg">Verifikasi No. Handphone</h5>
            </div>
            <h6 class="content-group text-center text-semibold no-margin-top"><?php echo $user->phone; ?></h6>

            <div class="form-group has-feedback has-feedback-left">
                <input type="text" name="token" class="form-control" placeholder="Masukkan kode verifikasi" autocomplete="off" >
                <div class="form-control-feedback">
                    <i class="icon-lock2 text-muted"></i>
                </div>
            </div>
            <div class="form-group">
                <button type="submit" class="btn bg-blue btn-block">Verifikasi</button>
            </div>
            <div class="form-group login-options">
                <p>Belum mendapatkan kode? <span id="wait" style="<?php echo (!$token_time_left) ? 'display:none;' : ''; ?>">Tunggu <span></span> detik</span><a id="request-token" href="javascript:void(0)" style="<?php echo ($token_time_left) ? 'display:none;' : ''; ?>">Kirim ulang</a></p>
            </div>
        </div>
    </form>
</div>
<script>
    var time_left_verification = <?php echo $token_time_left; ?>;
</script>