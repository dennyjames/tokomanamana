<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

    function __construct() {
        parent::__construct();
        if (!$this->ion_auth->logged_in())
        //redirect('member/login?back=' . uri_string());
        redirect('auth/guest?back=' . uri_string());
        $this->load->library('form_validation');
        $this->load->library('rajaongkir');
        $this->lang->load('shop/checkout', settings('language'));
//        $this->load->model('catalog/product_model', 'product');
        $this->load->model('cart_model');
        $this->load->model('checkout/checkout_model', 'checkout');
//        $this->load->model('payment_methods');
    }

    public function index() {
        redirect('shop/checkout/address');
    }

    public function address() {
        if (!$this->cart->contents())
            redirect('shop/cart');
        $merchant_user = $this->session->userdata('merchant_user');
        //var_dump($merchant_user);exit();
        $this->form_validation->set_rules('address', 'Alamat', 'trim|required', array('required' => 'Alamat harus dipilih'));

        if ($this->form_validation->run() == true) {
            $data = $this->input->post(null, true);
            $this->session->set_userdata('checkout_address', $data['address']);
            redirect('shop/checkout/shipping');
        } else {
            $this->data['messages'] = validation_errors();
        }
        $this->data['provincies'] = $this->main->gets('provincies', array(), 'name ASC');
        $this->data['list_address'] = $this->checkout->get_merchant_data($merchant_user->merchant);
        $this->data['shippings'] = array();
        $this->data['cities'] = array();

        $this->output->set_title(lang('checkout_address_text') . ' - ' . settings('meta_title'));
        $this->template->shop();
        $this->load->js('../assets/backend/js/merchant/modules/shop/checkout.js');
        $this->load->view('checkout/address', $this->data);
    }

    public function shipping() {
        if (!$this->cart->contents())
            redirect('shop/cart');
        if (!$this->session->userdata('checkout_address'))
            redirect('shop/checkout/address');
        $merchant_user = $this->session->userdata('merchant_user');
        $this->form_validation->set_rules('shipping[]', 'lang:checkout_shipping_field_shipping', 'required');

        if ($this->form_validation->run() == true) {
            $data = $this->input->post(null, true);
            foreach ($data['shipping'] as $key => $shipping) {
                $x = $this->main->get('merchants',array('id'=>$shipping['merchant']));
                $cost = $data['shipping_cost'][$key];
                $item = array(
                    'rowid' => $key,
                    'shipping_merchant' => $shipping['merchant'],
                    'shipping' => $shipping['service'],
                    'shipping_cost' => $cost,
                    'merchant_type' => $x->type
                );
                $this->cart->update($item);
            }
            redirect('shop/checkout/payment');
        } else {
            $this->data['messages'] = validation_errors();
            $this->data['merchant_user'] = $merchant_user;
            $this->data['address'] = $this->checkout->get_merchant_data($merchant_user->merchant)->row();
            $this->output->set_title(lang('checkout_shipping_text') . ' - ' . settings('meta_title'));
            $this->template->shop();
            $this->load->js('../assets/backend/js/merchant/modules/shop/checkout.js');
            $this->load->view('checkout/shipping', $this->data);
        }
    }

    public function payment() {
        if (!$this->cart->contents())
            redirect('shop/cart');
        if (!$this->session->userdata('checkout_address'))
            redirect('shop/checkout/address');
        $list_shipping = array();
        foreach ($this->cart->contents() as $key => $item) {
            array_push($list_shipping,$item['shipping']);
        }
        //var_dump($this->cart->contents());

        $merchant_user = $this->session->userdata('merchant_user');
        $counts = array_count_values($list_shipping);
        $count_total_shipping = 0;
        $this->data['pickup_count'] = isset($counts['pickup']) ? $counts['pickup'] : 0;
        $this->form_validation->set_rules('payment', 'lang:checkout_shipping_field_payment', 'required');
        $shipping_group= array();
        foreach ($this->cart->contents() as $key => $item) {
            $id = $item['shipping_merchant'] . '-' . $item['shipping'];
            $shipping_group[$id]['merchant_id'] = $item['shipping_merchant'];
            $shipping_group[$id]['shipping_type'] = $item['shipping'];
            $shipping_group[$id]['shipping_cost'] = $item['shipping_cost'];
            $shipping_group[$id]['weight'] = isset($shipping_group[$id]['weight']) ? ($shipping_group[$id]['weight'] + $item['weight']) / 1000 : $item['weight'] / 1000;
            $shipping_group[$id]['shipping_total'] = $shipping_group[$id]['shipping_cost'];
        }
        foreach ($shipping_group as $key => $item){
            $count_total_shipping = $count_total_shipping + $item['shipping_total'];
        }
        $this->data['count_total_shipping'] = $count_total_shipping;
        if ($this->form_validation->run() == true) {
            $address = $this->checkout->get_merchant_data($merchant_user->merchant)->row();
            $payment_method = $this->input->post('payment');
            $coupon = $this->session->userdata('coupon');

           $data_order = array(
                'code' => order_code(),
                'customer' => $merchant_user->merchant,
                'payment_method' => $payment_method,
                'due_date' => date('Y-m-d H:i:s', strtotime(' +1 day')),
//                'due_date' => date('Y-m-d H:i:s', strtotime(' +2 minutes')),
                'shipping_name' => $address->name,
                'shipping_address' => $address->address,
                'shipping_phone' => $merchant_user->phone,
                'shipping_province' => $address->province,
                'shipping_city' => $address->city,
                'shipping_district' => $address->district,
                'shipping_postcode' => $address->postcode,
            );
            $order = $this->main->insert('orders_merchant', $data_order);
            if($coupon) {
                $data_coupon = array(
                    'coupon' => $coupon->id,
                    'order' => $this->db->insert_id(),
                    'customer' => $merchant_user->merchant,
                    'amount' => $coupon->total_disc,
                );
                $coupon_history = $this->main->insert('coupon_history', $data_coupon);  
            }
            $data_invoice = array(
                'customer' => $merchant_user->merchant,
                'order_status' => settings('order_new_status'),
                'order' => $order
            );
            $cart = array();
            foreach ($this->cart->contents() as $key => $item) {
                $id = $item['shipping_merchant'] . '-' . $item['shipping'];
                $cart[$id]['merchant'] = $item['shipping_merchant'];
                $cart[$id]['shipping'] = $item['shipping'];
                $cart[$id]['shipping_cost'] = isset($cart[$id]['shipping_cost']) ? $cart[$id]['shipping_cost'] + $item['shipping_cost'] : $item['shipping_cost'];
                //$cart[$id]['shipping_cost'] = isset($cart[$id]['shipping_cost']) && isset($cart[$id]['weight']) ? $cart[$id]['shipping_cost'] * ceil($cart[$id]['weight']) : 0;
                $cart[$id]['subtotal'] = isset($cart[$id]['subtotal']) ? $cart[$id]['subtotal'] + $item['subtotal'] : $item['subtotal'];
                $cart[$id]['weight'] = isset($cart[$id]['weight']) ? $cart[$id]['weight'] + $item['weight'] : $item['weight'];
                $cart[$id]['product'][$key] = $item;
            }
            //var_dump($cart);
            ksort($cart, SORT_NUMERIC);

            $order_total = 0;
            $order_shipping = 0;
            foreach ($cart as $merchant => $data) {
                $data_invoice['code'] = invoice_code();
                $data_invoice['merchant'] = $data['merchant'];
                $data_invoice['shipping_courier'] = $data['shipping'];
                $data_invoice['shipping_cost'] = $data['shipping_cost'];
                $data_invoice['shipping_weight'] = $data['weight'];
                $data_invoice['subtotal'] = $data['subtotal'];
                $data_invoice['total'] = $data['subtotal'] + $data['shipping_cost'];
                $order_total += $data_invoice['subtotal'];
                $order_shipping += $data['shipping_cost'];
                //var_dump($data['product']);
                //exit();
                $invoice = $this->main->insert('order_invoice_merchant', $data_invoice);
                foreach ($data['product'] as $item) {
                    $data_product = array(
                        'order' => $order,
                        'invoice' => $invoice,
                        'product' => $item['id'],
                        'name' => $item['name'],
                        'code' => $item['code'],
                        'price' => $item['price'],
                        'quantity' => $item['qty'],
                        'weight' => $item['weight'],
                        'total' => $item['subtotal'],
                        'options' => json_encode($item['options']),
                        'promo_data' => json_encode($item['promo_data'])
                    );
                    if($item['inPromo']){
                        $data_product['price_old'] = $item['old_price'];
                        $data_product['discount'] = $item['qty'] * $item['disc_price'];
                    }
                    $order_product = $this->main->insert('order_product_merchant', $data_product);
                }
                $this->main->insert('order_history_merchant', array('order' => $order, 'invoice' => $invoice, 'order_status' => settings('order_new_status')));
            }
            if($coupon) {
                $order_total = $order_total -  $coupon->total_disc;
            }
            $this->main->update('orders_merchant', array('total' => $order_total + $order_shipping, 'subtotal' => $order_total, 'shipping_cost' => $order_shipping), array('id' => $order));
            $this->cart->destroy();

            $this->load->model('orders');
            $data = $this->orders->get($order);
            $this->data['order'] = $data;
            $this->data['customer_merchant'] = $this->main->get('merchants', array('id' => $this->data['order']->customer));
            $this->data['customer'] = $this->main->get('merchant_users', array('id' => $this->data['customer_merchant']->auth));
            $this->data['invoices'] = $this->main->gets('order_invoice_merchant', array('order' => $this->data['order']->id));

            $payment_method = $this->main->get('payment_methods', array('name' => $payment_method));
            if ($payment_method->vendor == 'xendit') {
                if ($payment_method->name == 'credit_card') {
                    redirect('shop/checkout/credit_card?id=' . $data_order['code']);
                } else {
                    $this->load->library('xendit');
                    $xendit_response = $this->xendit->createInvoice($data_order['code'], $order_total + $order_shipping, $this->data['user']->email, 'Order #' . $order);
                    $payment = $xendit_response;
                    if (in_array($data->payment_method, array('mandiri_va', 'bni_va', 'bri_va'))) {
                        switch ($data->payment_method) {
                            case 'mandiri_va':
                                $va_name = 'Mandiri Virtual Account';
                                $va_code = 'MANDIRI';
                                break;
                            case 'bni_va':
                                $va_name = 'BNI Virtual Account';
                                $va_code = 'BNI';
                                break;
                            case 'bri_va':
                                $va_name = 'BRI Virtual Account';
                                $va_code = 'BRI';
                                break;
                        }
                        foreach ($payment['available_banks'] as $account) {
                            if ($account['bank_code'] == $va_code) {
                                $payment_detail = array(
                                    'vendor' => 'xendit',
                                    'account_code' => $account['bank_code'],
                                    'account_name' => $va_name,
                                    'account_holder_name' => $account['account_holder_name'],
                                    'account_branch' => $account['bank_branch'],
                                    'account_number' => $account['bank_account_number'],
                                    'amount' => $account['transfer_amount'],
                                );
                                break;
                            }
                        }
                    } elseif ($data->payment_method == 'alfamart') {
                        foreach ($payment['available_retail_outlets'] as $account) {
                            if ($account['retail_outlet_name'] == 'ALFAMART') {
                                $payment_detail = array(
                                    'vendor' => 'xendit',
                                    'account_code' => $account['retail_outlet_name'],
                                    'account_name' => $account['retail_outlet_name'],
                                    'account_holder_name' => 'XENDIT',
                                    'account_branch' => '',
                                    'account_number' => $account['payment_code'],
                                    'amount' => $account['transfer_amount'],
                                );
                                break;
                            }
                        }
                    }
                    $this->main->insert('order_meta_merchant', [
                        'order_id' => $order,
                        'meta_name' => 'xendit_response',
                        'meta_value' => json_encode($payment)
                    ]);
                }
            } elseif ($payment_method->vendor == 'sprint') {
                $this->load->library('sprint');
                $transaction = $this->data['order'];
                $response = $this->sprint->process($transaction);
                $this->main->insert('order_meta_merchant', [
                    'order_id' => $order,
                    'meta_name' => 'sprint_request',
                    'meta_value' => json_encode($response['request'])
                ]);
                $this->main->insert('order_meta_merchant', [
                    'order_id' => $order,
                    'meta_name' => 'sprint_response',
                    'meta_value' => json_encode($response['response'])
                ]);
                $payment_detail = array(
                    'vendor' => 'sprint',
                    'account_code' => $response['request']['channelId'],
                    'account_name' => $payment_method->title,
                    'account_holder_name' => isset($payment_method->merchant_name) ? $payment_method->merchant_name : '',
                    'account_branch' => '',
                    'account_number' => ($data->payment_method == 'bca_va') ? $response['request']['customerAccount'] : '',
                    'amount' => $response['request']['transactionAmount'],
                );
                if (in_array($data->payment_method, ['kredivo', 'bca_sakuku', 'creditcard','creditcard01','creditcard03','creditcard06','creditcard12'])) {
                    $payment_detail['payment_url'] = $response['response']['redirectURL'];
                } elseif ($data->payment_method == 'bca_klikpay') {
                    $payment_detail['payment_url'] = $response['response']['redirectURL'];
                    $payment_detail['payment_data'] = $response['response']['redirectData'];
                }
            } elseif ($payment_method->vendor == 'ipay88') {
                $this->load->library('ipay');
                $this->load->config('ipay88', TRUE);
                $transaction = $this->data['order'];
                $cust = $this->data['customer'];
                $merchant_code = $this->config->item('merchant_code', 'ipay88');
                $merchant_key = $this->config->item('merchant_key', 'ipay88');
                $responseUrl = $this->config->item('responseURL', 'ipay88');
                $backendUrl = $this->config->item('backendURL', 'ipay88');
                $payment_detail = array(
                    'MerchantCode' => $merchant_code,
                    'MerchantKey' => $merchant_key,
                    'RefNo' => $transaction->code,
                    'Amount' => $transaction->total,
                    'Currency' => 'IDR',
                    'ProdDesc' => $transaction->code,
                    'UserName' => $cust->fullname,
                    'UserEmail' => $cust->email,
                    'UserContact' => $cust->phone,
                    'Remark' => '',
                    'Lang' => 'UTF-8',
                    'ResponseURL' => $responseUrl,
                    'BackendURL' => $backendUrl,
                );
                if($data->payment_method == 'ovo'){
                    $payment_detail['PaymentId'] = 1;
                } elseif($data->payment_method == 'tcash'){
                    $payment_detail['PaymentId'] = 15;
                } else {
                    $payment_detail['PaymentId'] = 1;
                }
                //$payment_detail['xfield1'] = '||IPP:3||';
                $responseIpay = $this->ipay->setMerchantCode($payment_detail['MerchantCode'])
                ->setMerchantKey($payment_detail['MerchantKey'])
                ->setPaymentId($payment_detail['PaymentId'])
                ->setRefNo($payment_detail['RefNo'])
                ->setAmount($payment_detail['Amount'])
                ->setCurrency($payment_detail['Currency'])
                ->setProdDesc($payment_detail['ProdDesc'])
                ->setUsername($payment_detail['UserName'])
                ->setUserEmail($payment_detail['UserEmail'])
                ->setUserContact($payment_detail['UserContact'])
                ->setRemark($payment_detail['Remark'])
                ->setLang($payment_detail['Lang'])
                ->setResponseUrl($payment_detail['ResponseURL'])
                ->setBackendUrl($payment_detail['BackendURL'])
                ->prepare()
                ->process();
                // var_dump($response);
                // exit();
            } else { //is transfer bank
                $payment_detail = json_decode($payment_method->setting);
            }
            $this->main->update('orders_merchant', array('payment_to' => json_encode($payment_detail)), array('id' => $order));
            $message = $this->load->view('email/transaction/invoice', $this->data, TRUE);
            $cronjob = array(
                'from' => settings('send_email_from'),
                'from_name' => settings('store_name'),
                'to' => $merchant_user->email,
                'subject' => 'Menunggu Pembayaran Invoice ' . $this->data['order']->code,
                'message' => $message
            );
            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
            if ($merchant_user->verification_phone) {
                $sms = json_decode(settings('sprint_sms'), true);
                $sms['m'] = 'Segera lakukan pembayaran melalui ' . strtoupper($this->data['order']->payment_method) . ' dengan nominal ' . rupiah($this->data['order']->total) . ' sebelum ' . get_date_indo_full($this->data['order']->due_date) . ' WIB';
                if($this->data['order']->payment_method=='alfamart'){
                    $sms['m'] .= PHP_EOL.'Kode Bayar: '.$payment_detail['account_number'];
                }elseif(in_array($this->data['order']->payment_method, ['mandiri_va','bni_va','bri_va','bca_va'])){
                    $sms['m'] .= PHP_EOL.'Rek. Virtual: '.$payment_detail['account_number'];
                }
                $sms['d'] = $merchant_user->phone;
                $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
            }

//            if ($transaction->payment_type == 'bca_klikpay') {
//                $this->bca_klikpay($payment_request['redirectURL'], $payment_request['redirectData']);
//            } elseif ($transaction->payment_type == 'kredivo') {
//                $this->kredivo($payment_request['redirectURL'], $payment_request['redirectData']);
//            } else {
//                $this->virtual_account($transaction, $payment_rezquest);
//            }
            if (in_array($data->payment_method, ['kredivo', 'bca_sakuku', 'creditcard','creditcard01','creditcard03','creditcard06','creditcard12'])) {
                redirect($response['response']['redirectURL']);
            } elseif ($data->payment_method == 'bca_klikpay') {
                $this->sprint->bca_klikpay($response['response']['redirectURL'], $response['response']['redirectData']);
            } else if (in_array($data->payment_method, ['ovo'])) {
                echo $responseIpay;
            } else {
                redirect('shop/checkout/finish?id=' . $data_order['code']);
            }
        } else {
            $this->data['messages'] = validation_errors();
            $shipping_cost = 0;
            foreach ($this->cart->contents() as $item) {
                $shipping_cost += $item['shipping_cost'];
            }
            $this->data['address'] = $this->checkout->get_merchant_data($merchant_user->merchant)->row();
            $this->data['shipping_cost'] = $shipping_cost;
            $this->data['payment_methods'] = $this->main->gets('payment_methods', ['status' => 1], 'sort_order asc');
            $this->output->set_title(lang('checkout_payment_text') . ' - ' . settings('meta_title'));
            $this->template->shop();
            $this->load->js('../assets/backend/js/merchant/modules/shop/checkout.js');
            $this->load->view('checkout/payment', $this->data);
        }
    }

    public function credit_card() {
//        $order = $this->session->flashdata('order_id');
        $order = $this->input->get('id');
        if (!$order)
            redirect('shop/cart');
//        $this->data['order'] = $this->checkout->get_orders($order);
        $this->data['order'] = $this->main->get('orders', array('code' => $order));
        if (!$this->data['order'])
            redirect('shop/cart');

        $this->data['invoices'] = $this->main->gets('order_invoice', array('order' => $this->data['order']->id));
        $this->data['customer_merchant'] = $this->main->get('merchants', array('id' => $this->data['order']->customer));
        $this->data['customer'] = $this->main->get('merchant_users', array('id' => $this->data['customer_merchant']->auth));

        if ($this->data['order']->payment_method != 'transfer') {
            $this->load->model('order_meta_merchant');
            $this->data['order_meta_merchant'] = $this->order_meta_merchant->get(['order_id' => $this->data['order']->id, 'meta_name' => 'xendit_response']);
        }

        $this->output->set_title(lang('checkout_finish_text') . ' - ' . settings('meta_title'));
        $this->template->_init();
        $this->load->view('credit_card', $this->data);
    }

    public function finish() {
        $this->session->unset_userdata('coupon');
        
        $order = $this->input->get('id');
        if (!$order)
            redirect('shop/cart');
        $merchant_user = $this->session->userdata('merchant_user');
        $order = $this->main->get('orders_merchant', array('code' => $order, 'customer' => $merchant_user->merchant));
        if (!$order)
            redirect('shop/cart');

        if ($order->payment_status == 'Pending' && in_array($order->payment_method, ['bca_sakuku', 'bca_klikpay', 'kredivo', 'creditcard','creditcard01','creditcard03','creditcard06','creditcard12'])) {
            $order->payment_status = 'Failed';
            $this->main->update('orders_merchant', array('payment_status' => 'Failed'), array('id' => $order->id));
            $this->main->update('order_invoice_merchant', array('order_status' => settings('order_cancel_status')), array('order' => $order->id));
            $invoices = $this->main->gets('order_invoice_merchant', array('order' => $order->id));
            if ($invoices) {
                foreach ($invoices->result() as $invoice) {
                    $this->main->insert('order_history_merchant', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => settings('order_cancel_status')));
                }
            }
            $this->data['order'] = $order;
            $this->data['customer_merchant'] = $this->main->get('merchants', array('id' => $order->customer));
            $this->data['customer'] = $this->main->get('merchant_users', array('id' => $this->data['customer_merchant']->auth));
            $this->data['payment_method'] = $this->main->get('payment_methods', array('name' => $order->payment_method))->title;
            $message = $this->load->view('email/transaction/payment_failed', $this->data, TRUE);
            $cronjob = array(
                'from' => settings('send_email_from'),
                'from_name' => settings('store_name'),
                'to' => $this->data['customer']->email,
                'subject' => 'Pembayaran Tagihan ' . $this->data['order']->code.' Gagal',
                'message' => $message
            );
            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
        }
        $this->data['order'] = $order;
        $this->data['invoices'] = $this->main->gets('order_invoice_merchant', array('order' => $this->data['order']->id));
        $this->data['customer_merchant'] = $this->main->get('merchants', array('id' => $order->customer));
        $this->data['customer'] = $this->main->get('merchant_users', array('id' => $this->data['customer_merchant']->auth));

        $this->output->set_title(lang('checkout_finish_text') . ' - ' . settings('meta_title'));
        $this->template->shop();
        $this->load->js('../assets/backend/js/merchant/modules/shop/checkout_finish.js');
        $this->load->view('checkout/finish', $this->data);
    }

    public function getCities($province) {
        $cities = $this->main->gets('cities', array('province' => $province), 'name ASC');
        $output = '<option value="">Pilih kota</option>';
        if ($cities) {
            foreach ($cities->result() as $city) {
                $output .= '<option value="' . $city->id . '">' . $city->type . ' ' . $city->name . '</option>';
            }
        }
        echo $output;
    }

    public function getDistricts($city) {
        $districts = $this->main->gets('districts', array('city' => $city), 'name ASC');
        $output = '<option value="">Pilih kecamatan</option>';
        if ($districts) {
            foreach ($districts->result() as $district) {
                $output .= '<option value="' . $district->id . '">' . $district->name . '</option>';
            }
        }
        echo $output;
    }

    public function getShippings() {
        $data = $this->input->post(null, TRUE);
//        $couriers = json_decode($data['courier']);
        $output = '';
        foreach ($data['courier'] as $courier) {
            if($courier == 'pickup') {
                $output .= '<li>
                <input id="' . $data['id'] . '-pickup" name="shipping[' . $data['id'] . '][service]" type="radio" value="pickup" class="shipping" required="" data-id="' . $data['id'] . '" data-value="0">
                <label for="' . $data['id'] . '-pickup">Ambil Sendiri <span style="color:red;font-size:10px">* Tidak support kredivo</span></label>
                </li>';
            } else {
                $services = $this->rajaongkir->cost($data['from'], 'subdistrict', $data['to'], 'subdistrict', $data['weight'], $courier);
                if ($services) {
                    $services = json_decode($services);
                    foreach ($services->rajaongkir->results as $service) {
                        foreach ($service->costs as $cost) {
                            if($cost->service == "CTC"){
                                $label_service = "REG";
                            } else if ($cost->service == "CTCYES"){
                                $label_service = "YES";
                            } else {
                                $label_service = $cost->service;
                            }
                            $output .= '<li>';
                            $output .= '<input name="shipping[' . $data['id'] . '][service]" type="radio" value="' . $service->code . '-' . $cost->service . '" class="shipping" id="' . $data['id'] . $service->code . '-' . $cost->service . '" required="" data-id="' . $data['id'] . '" data-value="' . $cost->cost[0]->value . '">';
                            $output .= '<label for="' . $data['id'] . $service->code . '-' . $cost->service . '">' . strtoupper($service->code) . ' ' . $label_service . ' <span>(' . rupiah($cost->cost[0]->value) . ')</span></label>';
                            $output .= '</li>';
                        }
                    }
                }
            }
        }
        echo $output;
    }

    public function add_address() {
        $this->form_validation->set_rules('title', 'Nama Alamat', 'trim|required');
        $this->form_validation->set_rules('name', 'lang:checkout_shipping_field_name', 'trim|required');
        $this->form_validation->set_rules('phone', 'lang:checkout_shipping_field_phone', 'trim|required');
        $this->form_validation->set_rules('address', 'lang:checkout_shipping_field_address', 'trim|required');
        $this->form_validation->set_rules('province', 'lang:checkout_shipping_field_province', 'required');
        $this->form_validation->set_rules('city', 'lang:checkout_shipping_field_city', 'required');
        $this->form_validation->set_rules('district', 'lang:checkout_shipping_field_district', 'required');
        $this->form_validation->set_rules('postcode', 'lang:checkout_shipping_field_postcode', 'trim|required');

        $return['status'] = 'error';
        if ($this->form_validation->run() == true) {
            $data = $this->input->post(null, true);
            $data['customer'] = $this->data['user']->id;
            $address = $this->main->insert('customer_address', $data);
            $province = $this->main->get('provincies', array('id' => $data['province']));
            $city = $this->main->get('cities', array('id' => $data['city']));
            $district = $this->main->get('districts', array('id' => $data['district']));
            $return['status'] = 'success';
            $return['html'] = '<div class="payment-method-block">
                               <div class="payment-method-detail">
                               <input type="radio" value="' . $address . '" onclick="selectPayment(this);" name="address" required id="address-' . $address . '">
                               <label for="address-' . $address . '">
                               <div class="payment-method-title">' . $data['name'] . ' - ' . $data['title'] . '</div>
                               <div class="payment-method-subtitle">' . $data['address'] . '<br>' . $district->name . ', ' . $city->name . ', ' . $data['postcode'] . '<br>' . $province->name . '</div>
                               </label>
                               </div>
                               </div>';
        } else {
            $return['message'] = '<div class="alert alert-danger fade in">' . validation_errors() . '</div>';
        }

        echo json_encode($return);
    }

    public function add_coupon() {
        $this->form_validation->set_rules('couponCode', 'Kode Kupon', 'trim|required');
        $return['status'] = 'error';
        $return['data'] = NULL;
        if ($this->form_validation->run() == true) {
            $coupon = $this->main->get('coupons', array('status' => 1,'code' => $this->input->post('couponCode')));
            if($coupon) {
                $today = strtotime(date("Y-m-d H:i:s"));
                $couponStart= strtotime($coupon->date_start);
                $couponEnd = strtotime($coupon->date_end);
                //$sess_user = $this->session->userdata('user');
                $merchant_user = $this->session->userdata('merchant_user');
                $this->db->where('merchant',$merchant_user->merchant);
                $this->db->where('coupon',$coupon->id);
                $countCc = $this->db->get('coupon_history_merchant');
                $this->db->where('coupon',$coupon->id);
                $countC = $this->db->get('coupon_history_merchant');
                if($coupon->type =='P') {
                    $coupon->total_disc = $this->cart->total() * ($coupon->discount/100);
                } else {
                    $coupon->total_disc = $coupon->discount;
                }
                $list_shipping = array();
                $list_categories_invalid = array();
                $list_zona_invalid = array();
                $invalid_type = array();
                $branch_type = array();
                $merchant_type = array();
                $exp_ids = array();
                $exp_idsx = array();
                if($coupon->catIds){
                    $exp_ids = explode(',', $coupon->catIds);
                }
                if($coupon->zonaIds){
                    $exp_idsx = explode(',', $coupon->zonaIds);
                }
                foreach ($this->cart->contents() as $key => $item) {
                    array_push($list_shipping,$item['shipping']);
                    if (!in_array($item['category'], $exp_ids)){
                         array_push($list_categories_invalid,$item['category']);
                    }
                    if (!in_array($item['merchant_group'], $exp_idsx)){
                        array_push($list_zona_invalid,$item['merchant_group']);
                    }
                    if($item['merchant_type'] == 'branch') {
                        array_push($branch_type,$item['merchant_type']);
                    } else {
                        array_push($merchant_type,$item['merchant_type']);
                    }
                }
                
                if($coupon->coupon_type == 'branch'){
                    $invalid_type = $merchant_type;
                } else if($coupon->coupon_type == 'merchant'){
                    $invalid_type = $branch_type;
                }
                //var_dump($this->cart->contents());exit();
                $counts = array_count_values($list_shipping);
                $count_total_shipping = 0;
                $this->data['pickup_count'] = isset($counts['pickup']) ? $counts['pickup'] : 0;
                $shipping_group= array();
                foreach ($this->cart->contents() as $key => $item) {
                    $id = $item['shipping_merchant'] . '-' . $item['shipping'];
                    $shipping_group[$id]['merchant_id'] = $item['shipping_merchant'];
                    $shipping_group[$id]['shipping_type'] = $item['shipping'];
                    $shipping_group[$id]['shipping_cost'] = $item['shipping_cost'];
                    $shipping_group[$id]['weight'] = isset($shipping_group[$id]['weight']) ? ($shipping_group[$id]['weight'] + $item['weight']) / 1000 : $item['weight'] / 1000;
                    $shipping_group[$id]['shipping_total'] = $shipping_group[$id]['shipping_cost'];
                }
                foreach ($shipping_group as $key => $item){
                    $count_total_shipping = $count_total_shipping + $item['shipping_total'];
                }
                $coupon->total_shopping = $this->cart->total() + $count_total_shipping - ($this->session->userdata('coupon')?$coupon->total_disc:0);
                if(count($list_categories_invalid) > 0){
                    $return['message'] = '<div class="alert alert-danger fade in"> Kupon tidak berlaku !</div>';
                }
                if($today <= $couponStart || $today >= $couponEnd){
                    $return['message'] = '<div class="alert alert-danger fade in"> Kupon sudah tidak berlaku !</div>';
                } else if($this->cart->total() < $coupon->min_order){
                    $return['message'] = '<div class="alert alert-danger fade in">Minimal transaksi dibawah '.$coupon->min_order.'!</div>';
                } else if($coupon->uses_customer <= $countCc->num_rows() && $coupon->uses_customer != 0){
                    $return['message'] = '<div class="alert alert-danger fade in">Kupon sudah pernah digunakan !</div>';
                } else if($coupon->uses_total <= $countC->num_rows() && $coupon->uses_total != 0){
                    $return['message'] = '<div class="alert alert-danger fade in">Kupon sudah tidak berlaku !</div>';
                } else if(count($list_categories_invalid) > 0){
                    $return['message'] = '<div class="alert alert-danger fade in"> Kupon tidak berlaku di kategori ini!</div>';
                } else if(count($list_zona_invalid) > 0){
                    $return['message'] = '<div class="alert alert-danger fade in"> Kupon tidak berlaku di zona ini!</div>';
                } else if(count($invalid_type) > 0){
                    $return['message'] = '<div class="alert alert-danger fade in"> Kupon tipe salah!</div>';
                }
                else {
                    $this->session->set_userdata('coupon', $coupon);
                    $return['message'] = '<div class="alert alert-success fade in">Kode Kupon valid!</div>';
                    $return['data'] = $coupon;
                    $return['status'] = 'success';
                }
            } else {
                $return['message'] = '<div class="alert alert-danger fade in">Kode Kupon invalid!</div>';
            }
        } else {
            $return['message'] = '<div class="alert alert-danger fade in"> Kode Kupon tidak boleh kosong</div>';
        }
        echo json_encode($return);
    }

    public function reset_coupon() {
        $this->session->unset_userdata('coupon');
        $list_shipping = array();
        foreach ($this->cart->contents() as $key => $item) {
            array_push($list_shipping,$item['shipping']);
        }
        //var_dump($this->cart->contents());
        $counts = array_count_values($list_shipping);
        $count_total_shipping = 0;
        $this->data['pickup_count'] = isset($counts['pickup']) ? $counts['pickup'] : 0;
        $shipping_group= array();
        foreach ($this->cart->contents() as $key => $item) {
            $id = $item['shipping_merchant'] . '-' . $item['shipping'];
            $shipping_group[$id]['merchant_id'] = $item['shipping_merchant'];
            $shipping_group[$id]['shipping_type'] = $item['shipping'];
            $shipping_group[$id]['shipping_cost'] = $item['shipping_cost'];
            $shipping_group[$id]['weight'] = isset($shipping_group[$id]['weight']) ? ($shipping_group[$id]['weight'] + $item['weight']) / 1000 : $item['weight'] / 1000;
            $shipping_group[$id]['shipping_total'] = $shipping_group[$id]['shipping_cost'];
        }
        foreach ($shipping_group as $key => $item){
            $count_total_shipping = $count_total_shipping + $item['shipping_total'];
        }
        $return['totalShopping']= $this->cart->total() + $count_total_shipping;
        $return['status'] = 'success';
        $return['message'] = 'Sukses menghapus kupon';
        echo json_encode($return);
    }
//    public function email($id) {
//        $this->data['data'] = $this->main->get('order_payment', array('code' => $id));
//        $this->data['orders'] = $this->main->gets('orders', array('payment' => $this->data['data']->id));
//        $this->load->view('email/payment_order', $this->data);
//    }
    private function _payment_email($id) {
        $this->data['data'] = $this->main->get('order_payment', array('id' => $id));
        $this->data['orders'] = $this->main->gets('orders', array('payment' => $this->data['data']->id));
        $message = $this->load->view('email/payment_order', $this->data, TRUE);
        if (!send_mail('noreply@tokomanamana.com', $this->data['user']->email, 'Menunggu Pembayaran Transfer Bank untuk pembayaran ' . $this->data['data']->code, $message)) {
            log_message('error', 'send email failed');
        }
    }

    private function _invoice_email($id) {
//    public function invoice_email($id) {
        $this->load->library('email');
        $config['mailtype'] = 'html';
        $config['wordwrap'] = FALSE;
        $config['priority'] = 1;
        $config['protocol'] = 'sendmail';
        $this->email->initialize($config);
//        $this->data['order'] = $this->checkout->get_order_by_id($id);
//        $data['address'] = $this->main->get('address', array('auth' => $this->data['user']->id, 'primary' => 1));
//        $data['customer'] = $this->checkout->getProfile($data['order']->customer);
//        $this->data['products'] = $this->checkout->get_order_products($id);
        $this->data['order'] = $this->main->get('orders', array('id' => $id));
        $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['order']->customer));
        $this->data['invoices'] = $this->main->gets('order_invoice', array('order' => $this->data['order']->id));
        $message = $this->load->view('email/invoice', $this->data, TRUE);
        // send_mail('noreply@tokomanamana.com', $this->data['user']->email, 'Menunggu Pembayaran Invoice ' . $this->data['order']->code, $message);
        $this->email->from('noreply@tokomanamana.com', 'TokoManaMana.com');
        $this->email->to($this->data['user']->email);
        $this->email->subject('Menunggu Pembayaran Invoice ' . $this->data['order']->code);
        $this->email->message($message);
        $this->email->send();
    }

    public function info_store($id) {
        $merchant = $this->checkout->get_merchant($id);
        $output = '<dl class="dl-horizontal">
                <p class="lead topbottom">' . $merchant->name . '</p>
                <hr class="topbottom">';
        if ($merchant->username) {
            $output .= '<dt class="listHeading">ID:</dt>
                <dd class="listText">' . $merchant->username . '</dd>';
        }
        $output .= '<dt class="listHeading">Kota:</dt>
                <dd class="listText">' . $merchant->city . '</dd>';
        $output .= '<dt class="listHeading">Kecamatan:</dt>
                <dd class="listText">' . $merchant->district . '</dd>';
        if ($merchant->distance) {
            $output .= '<dt class="listHeading">Jarak:</dt>
                <dd class="listText">' . number($merchant->distance) . ' km</dd>';
        }
        $merchant->rating = round($merchant->rating);
        $rating = '<span class="spr-starrating spr-badge-starrating">';
        for ($i = 1; $i <= $merchant->rating; $i++) {
            $rating .= '<i class="spr-icon spr-icon-star"></i>';
        }
        for ($i = $merchant->rating + 1; $i <= 5; $i++) {
            $rating .= '<i class="spr-icon spr-icon-star-empty"></i>';
        }
        $rating .= '</span>';
        $output .= '<dt class="listHeading">Rating:</dt>
                <dd class="listText">' . $rating . '</dd>
            </dl>';
        echo $output;
    }
    public function check_stock() {
        $this->load->model('checkout_model', 'checkout');
        $merchant_user = $this->session->userdata('merchant_user');
        // $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        if (!$this->cart->contents()){
            $return = array('message' => 'Tidak ada barang di dalam cart! ', 'status' => 'failed');
        } else {
            $not_avail = array();
            $merchant_stock = array();
            $count_valid = 0;
            foreach ($this->cart->contents() as $item) {
                if($item['type'] == 'p') {
                    $package_items = $item['package_items'];
                    $count_packages = count($package_items);
                    
                    $merchant_stock = array();
                    $merchant = $this->checkout->get_branch($merchant_user->merchant_group);
                    foreach ($package_items as $package) {
                        $stock = $this->checkout->check_branch_stock($merchant ? $merchant->id : 0,$package['product_id'], $item['qty'] * $package['qty'], $item['price_level']); 
                        if ($stock->num_rows() > 0) {
                            foreach ($stock->result() as $merchantx) {
                                $merchant_stock[$merchantx->id][] = array('prod_id' => $package['product_id']);
                            }
                        }  
                    }
                    if (count($merchant_stock) > 0) {
                        //foreach ($merchant_stock as $mcn) {
                            if(count($merchant_stock) == $count_packages) {
                                $count_valid++;
                            }
                        //}
                    }
                    if ($count_valid == 0){
                        $not_avail[] = $item;
                    }

                } else {
                    $merchant = $this->checkout->get_branch($merchant_user->merchant_group);
                    // $stock = $this->checkout->check_branch_stock($merchant ? $merchant->id : 0,$item['id'], $item['qty'], $item['price_level']);
                    // if($stock->num_rows() == 0){
                    // $not_avail[] = $item;
                    // }
                   
                }
                if(count($not_avail) > 0) {
                    $return = array('message' => 'Stok barang tidak tersedia! ', 'status' => 'failed','data' => $not_avail);
                } else {
                    $return = array('message' => 'Stok barang tersedia! ', 'status' => 'success');
                }
                
            }
            echo json_encode($return);
        }
        
    }
}
