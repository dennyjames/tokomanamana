<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->lang->load('ion_auth', settings('language'));
        $this->lang->load('auth', settings('language'));
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->model('home_model', 'home');
        //$this->load->library('recaptcha');
    }

    public function index() {
        $this->load->library('session');
        $this->load->helper(array('captcha','url'));
        // if ($this->ion_auth->logged_in())
        //     redirect();
 
            // store image html code in a variable
            $this->data['page'] = 'register';
            $merchant_user = $this->session->userdata('merchant_user');
            $branch = $this->home->get_branch($merchant_user->merchant_group);
            $this->data['promotions'] = $this->home->get_promotions(18);
            $this->data['merchant_group'] = $merchant_user->merchant_group;
            $this->data['merchant_list'][] = $branch->id;

            $this->template->shop();
            $this->template->form();
            $this->output->set_title('Tokomanamana Shop');
            $this->data['meta_description'] = 'Daftar menjadi penjual ditokomanamana, Tanpa ribet, cukup update stok, dan terima order disekitar lokasi anda.';
            $this->load->css('../assets/frontend_1/css/libs/bootstrap-datepicker3.min.css');
            $this->load->js('../assets/frontend_1/js/bootstrap-datepicker.min.js');
            // $this->load->js('https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js');
            // $this->load->js(base_url('../').'assets/backend/js/merchant/modules/auth/register.js');
            $this->load->view('home', $this->data);
    }
}
