<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('shop/catalog', settings('language'));
        $this->load->model('catalog/category_model', 'category');
        $this->load->model('catalog/product_model', 'product');
        $this->load->library('breadcrumb');
    }

    public function view($id) {
        $product = $this->product->get_product($id);
        if (!$product)
            show_404();
            
        $merchant_user = $this->session->userdata('merchant_user');
        $merchant_group = $merchant_user->merchant_group;
        $this->product->add_viewed($product->id);
        if($this->ion_auth->logged_in()) {
            $this->data['mywish'] = $this->main->get('wishlist', array('customer' => $this->data['user']->id, 'product' => $product->id));
        } else {
            $this->data['mywish'] = $this->main->get('wishlist', array('customer' => 0, 'product' => $product->id));
        }

        $this->data['data'] = $product;
        $this->data['old_price'] = $product->price;
        $this->data['related_product'] =  $this->product->get_related_product($product->id,$product->category);
        $this->data['show_last_seen_product'] =  FALSE;
        $this->data['last_seen_product'] =  $this->product->get_lastseen_product();
        if($product->promo == 1){
            $promo_data = json_decode($product->promo_data,true);
            if($promo_data['type'] == 'P') {
                $disc_price = round(($product->price * $promo_data['discount']) / 100);
                $label_disc = $promo_data['discount'].'% off';
            } else {
                $disc_price = $promo_data['discount'];
                $label_disc = 'SALE';
            }
            $end_price= $product->price - $disc_price;
            $today = date('Y-m-d');
            $today=date('Y-m-d', strtotime($today));
            $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
            $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

            if (($today >= $DateBegin) && ($today <= $DateEnd)){
                $this->data['price'] = $end_price;
                $this->data['inPromo'] = true;
                $this->data['label_disc'] =  $label_disc;
            } else {
                $this->data['price'] = $product->price;
                $this->data['inPromo'] = false;
                $this->data['label_disc'] =  '';
            }
        } else {
            $this->data['price'] = $product->price;
            $this->data['inPromo'] = false;
            $this->data['label_disc'] =  '';
        }
//        $this->data['price_group'] = 0;
        if ($product->merchant == 0) {
            //$merchant = $this->session->userdata('list_merchant')[0];
            $price_group = $this->main->get('product_price', array('product' => $product->id, 'merchant_group' => $merchant_group));
            if ($price_group) {
                $this->data['old_price'] = $price_group->price;
                if($product->promo == 1){
                    if($promo_data['type'] == 'P') {
                        $disc_price_group = round(($price_group->price * $promo_data['discount']) / 100);
                    } else {
                        $disc_price_group = $promo_data['discount'];
                    }
                    $end_price_group= $price_group->price - $disc_price_group;
        
                    if (($today >= $DateBegin) && ($today <= $DateEnd)){
                        $this->data['price'] = $end_price_group;
                    } else {
                        $this->data['price'] = $price_group->price;
                    }
                } else {
                    $this->data['price'] = $price_group->price;
                }
                //$this->data['price'] = $inPromo ? round(($price_group->price * $promo_data['discount'])) : $price_group->price;
//                $this->data['price_group'] = $price_group;
            }
            $this->data['price_level'] = $this->product->price_level_group($product->id, $merchant_group);
//        }else{
//            $this->data['price_level'] = $this->main->gets('product_price_level',array('product'=>$product->id),'min_qty asc');
        }
        $this->data['images'] = $this->product->get_images($id);
        if($this->data['images']){
            $this->data['meta_image'] = $this->data['images']->row(0)->image;
        }
//        $this->data['options'] = $this->product->get_product_options($product->id);
//        $this->data['option_groups'] = array();
        $this->data['is_option'] = false;
        if ($this->main->gets('product_option', array('product' => $product->id))) {
            $this->data['is_option'] = true;
            $options = $this->option($product->id);
            $this->data['options'] = $options['options'];
            $this->data['images'] = $options['images'];
//            $this->data['options'] = $this->option($product->id);
        }

        $this->data['features'] = $this->product->get_product_features($product->id);
        $this->data['rating'] = $this->product->get_rating($product->id);
        $this->data['rating']->rating = round($this->data['rating']->rating);
        $this->data['reviews'] = $this->product->get_reviews($product->id);

        //breadcrumb
        $categoryPaths = $this->category->get_category_path($product->category);
        $this->breadcrumb->add('<i class="fa fa-home"></i>', site_url());
        if ($categoryPaths)
            foreach ($categoryPaths->result() as $categoryPath)
                $this->breadcrumb->add($categoryPath->name, seo_url('catalog/categories/view/' . $categoryPath->id));
        $this->breadcrumb->add($product->name, seo_url('catalog/products/view/' . $product->id));
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['meta_description'] = ($product->meta_description) ? $product->meta_description : $product->short_description;
        $this->data['meta_keyword'] = ($product->meta_keyword) ? $product->meta_keyword : settings('meta_keyword');
        $this->data['page'] = 'product';
        $this->data['merchants'] = $this->product->get_merchants($product->id);
        $this->data['product_relations'] = $this->product->get_relations($product->category, $product->id, 12, $categoryPaths->row(0)->id);
        $this->data['also_boughts'] = $this->product->get_also_boughts($categoryPaths->row(0)->id, $product->id, 6);

        $this->output->set_title((($product->meta_title) ? $product->meta_title : $product->name) . ' - ' . settings('meta_title'));
        $this->template->shop();
        $this->load->css('../assets/frontend/css/jquery.fancybox.min.css');
        $this->load->css('https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.2/flexslider.min.css');
        $this->load->js('../assets/frontend/js/jquery.fancybox.min.js');
        $this->load->js('https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.7.2/jquery.flexslider-min.js');
        $this->load->js(base_url().'../assets/backend/js/merchant/modules/shop/product.js');
        $this->load->view('products/view', $this->data);
    }

    private function option($product, $selected = array()) {
        $res = array();
        $options = $this->product->get_option_groups($product);
//        $current_product_option = 0;
//        if ($options->num_rows() > 0) {
//            $this->data['is_option'] = true;
//            $colors = array();
        $groups = array();
//            $combinations = array();
//            $combination_prices = array();
//            $combination_images = $this->product->get_combination_images($product);
        foreach ($options->result() as $option) {
//                print_r($option);
            if ($option->type == 'c' && $option->color) {
                $colors[$option->option]['value'] = $option->color;
                $colors[$option->option]['name'] = $option->option_name;
            }
            if (!isset($groups[$option->group])) {
                $groups[$option->group] = array(
                    'name' => $option->group_name,
                    'type' => $option->type,
                    'default' => -1
                );
            }
            $groups[$option->group]['options'][$option->option] = array(
                'name' => $option->option_name,
                'color' => $option->color,
                'selected' => (isset($selected[$option->group]) && $selected[$option->group] == $option->option) ? true : false,
            );
            if (!$selected) {
                if ($option->default && $groups[$option->group]['default'] == -1) {
                    $groups[$option->group]['default'] = $option->option;
                    $current_product_option = $option->product_option;
                }
            }
        }
        if (!$selected) {
            foreach ($groups as $key => $group) {
                foreach ($group['options'] as $id_option => $option) {
                    if ($group['default'] == $id_option) {
                        $groups[$key]['options'][$id_option]['selected'] = true;
                        continue;
                    }
                }
            }
        } else {
            $current_product_option = $this->product->get_product_option_id($product, $selected);
        }
        $data['option_groups'] = $groups;
        $res['options'] = $this->load->view('products/option', $data, true);
//            $this->data['option_groups'] = $groups;
        //get images group by product option
//            $image_groups = $this->db->select("po.id, GROUP_CONCAT(o.id ORDER BY og.sort_order ASC, o.sort_order ASC, og.name ASC SEPARATOR '-') path, po.default", false)
//                            ->join('product_option_combination poc', 'poc.product_option = po.id', 'left')
//                            ->join('options o', 'poc.option = o.id', 'left')
//                            ->join('option_group og', 'o.group = og.id', 'left')
//                            ->where('po.product', $product)
//                            ->group_by('po.id')
//                            ->get('product_option po')->result_array();
//            foreach ($image_groups as $key => $image_group) {
//                //get image by product option
//                $images = $this->db->select('pi.id, pi.image')
//                        ->join('product_image pi', 'pi.id = poi.product_image', 'left')
//                        ->where('poi.product_option', $image_group['id'])
//                        ->get('product_option_image poi');
//                if ($images->num_rows() > 0) {
//                    $image_groups[$key]['images'] = $images->result_array();
//                } else {
//                    $image_groups[$key]['images'] = $this->data['images']->result_array();
//                }
//            }
        if ($this->main->gets('product_option_image', array('product_option' => $current_product_option))) {
            $images = $this->product->get_product_option_images($current_product_option);
        } else {
            $images = $this->product->get_images($product);
        }
        $res['images'] = '';
        if ($images) {
            foreach ($images->result() as $image) {
                $res['images'] .= '<div class="image-item">' .
                        '<a href="' . get_image($image->image) . '" class="thumbnail fancybox" data-fancybox="gallery">' .
                        '<img src="' . get_image($image->image) . '" data-item="' . $image->id . '" />' .
                        '</a>' .
                        '</div>';
            }
        }
//        $this->data['images'] = $image_groups;
//        }
        return $res;
    }

    public function change_option() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $selected = $this->input->post('option');
        $product = decode($this->input->post('product'));
        $options = $this->option($product, $selected);
        echo json_encode($options);
    }

    public function add_to_cart_ajax() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $id = decode($data['product']);
        $qty = $data['qty'];
        $output = array('status' => 'error', 'message' => 'Qty minimum 1','items' => array());
        $merchant_user = $this->session->userdata('merchant_user');
        $merchant_group = $merchant_user->merchant_group;
        if ($qty > 0) {
            $product = $this->product->get_product($id);
            if ($product) {
                do {
                    $product_option = 0;
                    if (isset($data['option'])) {
                        $product_option = $this->product->get_product_option_id($product->id, $data['option']);
                        if (!$product_option) {
                            $output['message'] = 'Kesalahan memilih variasi produk.';
                            break;
                        }
//                    $product_option = $this->main->get('product_option', array('id' => $current_product_option));
                        $options = $this->product->get_option_groups($product->id, $product_option);
                        $option = $options->row();
//                    print_r($option);
//                    $product_option = $product_options->row();
                        $image = $this->product->get_product_option_images($product_option);
                        if ($image->num_rows() > 0) {
                            $product->image = $image->row()->image;
                        }
                    }
                    if ($product->length && $product->height && $product->width) {
                        $weight_volume = ($product->length * $product->height * $product->width) / 6;
                        if ($weight_volume > $product->weight)
                            $product->weight = round($weight_volume);
                    }

                    $old_price = $product->price;
                    if($product->promo == 1){
                        $promo_data = json_decode($product->promo_data,true);
                        if($promo_data['type'] == 'P') {
                            $disc_value = $promo_data['discount'];
                            $disc_price = round(($product->price * $promo_data['discount']) / 100);
                        } else {
                            $disc_value = NULL;
                            $disc_price = $promo_data['discount'];
                        }
                        $end_price= $product->price - $disc_price;
                        $today = date('Y-m-d');
                        $today=date('Y-m-d', strtotime($today));
                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
            
                        if (($today >= $DateBegin) && ($today <= $DateEnd)){
                            $price = $end_price;
                            $inPromo = true;
                        } else {
                            $price = $product->price;
                            $inPromo = false;
                        }
                    } else {
                        $price = $product->price;
                        $inPromo = false;
                    }

                    $rowid = md5($id . ($product_option ? $product_option : 0));
                    $item = $this->cart->get_item($rowid);
                    $merchant = $this->session->userdata('list_merchant')[0];
                    //$price = $product->price;
                    //jika produk tanaka
                    if ($product->merchant == 0) {
                        $price_level = $this->product->price_in_level($product->id, $merchant_group, ($item ? $item['qty'] + $qty : $qty));
                        if ($price_level) { //jika price level ada, maka produk tsb masuk ke harga grosir
                            $price = $price_level;
                            $price_level = 1;
                        } else {
                            $price_group = $this->main->get('product_price', array('product' => $product->id, 'merchant_group' => $merchant_group));
                            if ($price_group) {
                                $old_price = $price_group->price;
                                if($product->promo == 1){
                                    if($promo_data['type'] == 'P') {
                                        $disc_price_group = round(($price_group->price * $promo_data['discount']) / 100);
                                    } else {
                                        $disc_price_group = $promo_data['discount'];
                                    }
                                    $end_price_group= $price_group->price - $disc_price_group;
                        
                                    if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                        $price = $end_price_group;
                                    } else {
                                        $price = $price_group->price;
                                    }
                                } else {
                                    $price = $price_group->price;
                                }
                            }
                        }
                    } else {
                        $price_level = 0;
                    }
                    $price += ($product_option) ? $option->price : 0;
                    $weight = $product->weight + ($product_option ? $option->weight : 0);
                    if ($item) {
                        $item['qty'] += $qty;
                        $item['price'] = $price;
                        $item['price_level'] = $price_level;
                        $item['weight'] = $weight * $item['qty'];
                        $this->cart->update($item);
                    } else {
                        $item = array(
                            'id' => $id,
                            'name' => $product->name,
                            'type' => $product->type,
                            'image' => $product->image,
                            'code' => $product->code,
                            'description' => $product->short_description,
                            'qty' => $qty,
                            'price' => $price,
                            'price_level' => $price_level,
                            'merchant' => $product->merchant,
                            'merchant_name' => $product->merchant_name,
                            'merchant_group' => ($product->merchant) ? 0 : $merchant_group,
                            'weight' => $weight * $qty,
                            'shipping' => '',
                            'shipping_merchant' => $product->merchant,
                            'shipping_cost' => 0,
                            'options' => ($product_option) ? $options->result_array() : array()
                        );
                        if($product->type == 'p'){
                            $item['package_items'] = json_decode($product->package_items, true);
                        } else {
                            $item['package_items'] = array();
                        }
                        if($inPromo){
                            $item['inPromo'] = true;
                            $item['promo_data'] = $promo_data;
                            $item['disc_value'] = $disc_value;
                            $item['disc_price'] = $disc_price;
                            $item['old_price'] = $old_price;
                        } else {
                            $item['inPromo'] = false;
                            $item['promo_data'] = array();
                            $item['disc_value'] = NULL;
                            $item['disc_price'] = NULL;
                            $item['old_price'] = $price;
                        }
                        $this->cart->insert($item);
                    }
                    $output['status'] = 'success';
                } while (0);
            }
            $output['items'] = $this->cart->contents() ? $this->cart->contents() : array();
        }
        echo json_encode($output);
    }


    public function submit_review() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('rating', 'Rating', 'trim|required');
        $this->form_validation->set_rules('title', 'Title', 'trim|required');
        $this->form_validation->set_rules('description', 'Description', 'trim|required|min_length[10]');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            do {
                $this->main->insert('product_review', array('product' => $data['product'], 'customer' => $this->data['user']->id, 'title' => $data['title'], 'rating' => $data['rating'], 'description' => $data['description']));
                $return = array('message' => $this->template->alert('success', 'Review telah diterima dan akan di review oleh Administrator.'), 'status' => 'success');
            } while (0);
        } else {
            $return = array('message' => $this->template->alert('danger', validation_errors()), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function submitLikeDislike() {
        if ($this->ion_auth->logged_in()) {
            $id = $this->input->post('id', TRUE);
            $type = $this->input->post('type', TRUE);
            if ($likeDislike = $this->main->get('product_review_likedislike', array('product_review' => $id, 'customer' => $this->data['user']->id))) {
                $type = $likeDislike->likedislike;
            } else {
                $this->main->insert('product_review_likedislike', array('product_review' => $id, 'customer' => $this->data['user']->id, 'likedislike' => $type));
                $typeName = ($type == 1) ? "like" : "dislike";
                $this->db->query("UPDATE product_review SET `$typeName`=`$typeName`+1 WHERE id=$id");
            }
            echo ($type == 1) ? lang('product_text_review_like_this') : lang('product_text_review_dislike_this');
        }
    }

}
