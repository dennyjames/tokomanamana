<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Xendit extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('orders');
        $this->load->model('order_meta');
    }

    public function index() {
        $data = file_get_contents("php://input");
        $data = json_decode($data);

        if (is_null($data)) {
            echo 'Error. Data return NULL';
            exit;
        }

        $invoice = $data->external_id;
        $order = $this->main->get('orders_merchant', ['code' => $invoice]);
        if ($order) {
            if (strtoupper($data->status) == 'PAID') {
                $this->main->update('orders_merchant', [
                    'payment_status' => 'Paid',
                    'payment_to' => 'Xendit',
                    'payment_date' => date('Y-m-d'),
                    'payment_total' => $data->amount
                        ], ['id' => $order->id]);

                //$this->main->update('order_invoice', array('order_status' => settings('order_payment_received_status')), array('order' => $order->id));
                $this->data['order'] = $this->orders->get($order->id);
                $this->data['customer_merchant'] = $this->main->get('merchants', array('id' => $this->data['order']->customer));
                $this->data['customer'] = $this->main->get('merchant_users', array('id' => $this->data['customer_merchant']->auth));
                $this->data['invoices'] = $this->main->gets('order_invoice_merchant', array('order' => $this->data['order']->id));

                if ($this->data['invoices']) {
                    foreach ($this->data['invoices']->result() as $invoice) {
                        if($invoice->shipping_courier == 'pickup') {
                            $due_date_inv = date('Y-m-d H:i:s', strtotime('+1 days'));
                        } else {
                            $due_date_inv = date('Y-m-d H:i:s', strtotime('+2 days'));
                        }
                        $this->main->update('order_invoice_merchant', array('order_status' => settings('order_payment_received_status'), 'due_date' => $due_date_inv), array('id' => $invoice->id));
                        $this->main->insert('order_history_merchant', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => settings('order_payment_received_status')));
                        $this->data['invoice'] = $invoice;
                        $this->data['merchant'] = $this->main->get('merchants', array('id' => $invoice->merchant));

                        //update stock merchant
                        if ($this->data['merchant']->type == 'merchant' || $this->data['merchant']->type == 'branch') {
                            $products = $this->main->gets('order_product_merchant', array('invoice' => $invoice->id));
                            if ($products) {
                                foreach ($products->result() as $product) {
                                    $product_detail = $this->db->query("select type, package_items from products where id = $product->product ");
                                    $row = $product_detail->row();
                                    if($row->type == 'p') {
                                        $package_items = json_decode($row->package_items, true);
                                        foreach ($package_items as $package) {
                                            $this->db->query("UPDATE product_merchant SET quantity = (quantity - $product->quantity) WHERE product = ".$package['product_id']." AND merchant = " . $this->data['merchant']->id); 
                                        }
                                    } else {
                                        $this->db->query("UPDATE product_merchant SET quantity = (quantity - $product->quantity) WHERE product = $product->product AND merchant = " . $this->data['merchant']->id);
                                    }
                                }
                            }
                        }

                        $merchant_user = $this->main->get('merchant_users', array('id' => $this->data['merchant']->auth));
                        $message = $this->load->view('email/merchant/new_order', $this->data, true);
                        $cronjob = array(
                            'from' => settings('send_email_from'),
                            'from_name' => settings('store_name'),
                            'to' => $merchant_user->email,
                            'subject' => 'Pesanan baru dari ' . $this->data['customer']->fullname,
                            'message' => $message
                        );
                        $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                        if ($merchant_user->verification_phone) {
                            $sms = json_decode(settings('sprint_sms'), true);
                            $sms['m'] = 'Pesanan baru dari ' . $customer->fullname . '. Segera proses sebelum ' . get_date_indo_full($invoice->due_date) . ' WIB.';
                            $sms['d'] = $merchant_user->phone;
                            $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                        }
                    }
                }

                $message = $this->load->view('email/transaction/payment_success', $this->data, true);
                $cronjob = array(
                    'from' => settings('send_email_from'),
                    'from_name' => settings('store_name'),
                    'to' => $this->data['customer']->email,
                    'subject' => 'Pembayaran Sukses #' . $this->data['order']->code,
                    'message' => $message
                );
                $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                if ($this->data['customer']->verification_phone) {
                    $sms = json_decode(settings('sprint_sms'), true);
                    $sms['m'] = 'Pembayaran sebesar ' . rupiah($this->data['order']->total) . ' telah berhasil pada ' . get_date_indo_full(date('Y-m-d H:i:s')) . ' WIB. Pesanan Anda akan diteruskan ke penjual.';
                    $sms['d'] = $this->data['customer']->phone;
                    $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                }
            } else {
//                $this->main->update('orders',[
//                    'payment_status' => 'Failed',
//                    'payment_date' => date('Y-m-d')
//                        ], ['id' => $order->id]);
            }

            $this->order_meta->insert([
                'order_id' => $order->id,
                'meta_name' => 'xendit_response_callback',
                'meta_value' => json_encode($data)
            ]);
        }
    }

    public function send_email() {
        $this->load->library('email');
        $config['mailtype'] = 'html';
        $config['wordwrap'] = FALSE;
        $config['priority'] = 1;
        $config['protocol'] = 'sendmail';
        $this->email->initialize($config);
        $this->email->from('noreply@tokomanamana.com', 'TokoManaMana.com');
        $this->email->to('sandi4code@gmail.com');
        $this->email->subject('Menunggu Pembayaran Invoice ');
        $this->email->message('helooooo haaaiiii');
        $this->email->send();
        echo $this->email->print_debugger();
    }

}
