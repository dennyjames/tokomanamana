<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<!-- Add the slick-theme.css if you want default styling -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
<style>
.slick-prev:before, .slick-next:before {
    color:#97C23C
}
</style>
<?php
    if ($promotions) {
?>
<div id="hot-promo" class="shopify-section index-section index-section-proban">
    <div>
        <section class="home_proban_layout">
            <div class="home_proban_wrapper">
                <div class="container">
                    <div class="row">
                        <div class="home_proban_inner">
                            <div class="page-title" style="border-color: #4C4A3F">
                                <div class="group_title">
                                    <h2>Promo</h2>
                                </div>
                                <!-- <a href="">Lihat semua</a> -->
                            </div>
                            <div class="home_proban_content" id="promo-product">
                                    <?php
                                        foreach ($promotions->result() as $promotion) {

                                            if ($promotion->merchant == 0) {
                                                $merchant = $this->session->userdata('list_merchant')[0];
                                                $price_group = $this->main->get('product_price', array('product' => $promotion->id, 'merchant_group' => $merchant['group']));
                                                if ($price_group) {
                                                    $promotion->price = $price_group->price;
                                                }
                                            }

                                            $promo_data = json_decode($promotion->promo_data,true);
                                            if($promo_data['type'] == 'P') {
                                                $disc_price = round(($promotion->price * $promo_data['discount']) / 100);
                                                $label_disc = $promo_data['discount'].'% off';
                                            } else {
                                                $disc_price = $promo_data['discount'];
                                                $label_disc = 'SALE';
                                            }
                                            $end_price= $promotion->price - $disc_price;
                                            $today = date('Y-m-d');
                                            $today=date('Y-m-d', strtotime($today));
                                            $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                            $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                            if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                    ?>
                                    <div class="col-sm-2 proban_product">
                                        <div class="row-container product list-unstyled clearfix">
                                        <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;"><?php echo $label_disc;?></span>
                                            <div class="row-left">
                                                <a href="<?php echo seo_url('catalog/products/view/' . $promotion->id); ?>" class="hoverBorder container_item">
                                                    <div class="hoverBorderWrapper">
                                                        <img src="<?php echo get_image($promotion->image); ?>" class="img-responsive front" alt="<?php echo $promotion->name;?>">
                                                    </div>
                                                </a>
                                            </div>

                                            <div class="row-right animMix">
                                                <div class="product-title"><a class="title-5 ma-shop" href="<?php echo seo_url('catalog/products/view/' . $promotion->id); ?>"><?php echo $promotion->name;?></a></div>
                                                <div class="rating-star">
                                                    <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                    </span>
                                                </div>
                                                <div class="product-price">
                                                    <span class="price_sale">
                                                        <span class="money" style="text-decoration: line-through;font-size:9px;color:#8b8f8b"><?php echo rupiah($promotion->price); ?></span>
                                                        <span class="money"><?php echo rupiah($end_price); ?></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                            }
                                        }
                                    ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<?php
    }
?>
<?php
if ($categories = $this->main->gets('categories', array('home_show' => 1, 'active' => 1), 'home_sort_order asc')) {
    $i = 0;
    foreach ($categories->result() as $category) {
        ?>
        <div id="category-<?php echo $category->id; ?>" class="shopify-section index-section index-section-proban">
            <div>
                <section class="home_proban_layout">
                    <div class="home_proban_wrapper">
                        <div class="container">
                            <div class="row">
                                <div class="home_proban_inner">
                                    <div class="page-title" style="border-color: <?php echo $category->home_border; ?>">
                                        <div class="group_title">
                                            <h2><?php echo $category->name; ?></h2>
                                            <ul class="link_menu hidden-xs">
                                                <li class="nav-item">
                                                    <a href="#">Terlaris</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#">Spesial</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#">Ulasan Terbanyak</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#">Terbaru</a>
                                                </li>
                                            </ul>
                                        </div>
                                        <a href="<?php echo seo_url('shop/catalog/categories/view/' . $category->id); ?>">Lihat semua</a>
                                    </div>
                                    <div class="home_proban_content" id="list-product-<?php echo $category->id; ?>">
                                        <?php
                                        //if ($this->session->has_userdata('list_merchant')) {
                                            $products = $this->home->products($category->id, $merchant_list, $merchant_group, 12);
                                            if ($products) {
                                                foreach ($products->result() as $product) {
                                                    ?>
                                                    <div class="col-sm-2 proban_product">
                                                        <div class="row-container product list-unstyled clearfix">
                                                            <span class="product-discount-label" style="position:absolute;bottom:100px;left:10px;z-index:1;background-color:blue;color:white;padding:5px 10px;">Grosir</span>
                                                            <div class="row-left">
                                                                <a href="<?php echo seo_url('shop/catalog/products/view/' . $product->id); ?>" class="hoverBorder container_item">
                                                                    <div class="hoverBorderWrapper">
                                                                        <img src="<?php echo get_image($product->image); ?>" class="img-responsive front" alt="<?php echo $product->name; ?>">
                                                                    </div>
                                                                </a>
                                                            </div>

                                                            <div class="row-right animMix">
                                                                <div class="product-title"><a class="title-5" href="<?php echo seo_url('shop/catalog/products/view/' . $product->id); ?>"><?php echo $product->name; ?></a></div>
                                                                <div class="rating-star">
                                                                    <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>
                                                                    </span>
                                                                </div>
                                                                <div class="product-price">
                                                                    <span class="price_sale"><span class="money"><?php echo rupiah($product->price); ?></span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                        //}
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <?php
    }
}
?>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script>
 $(window).on('resize orientationchange', function() {
    $('.responsive').slick('resize');
    });
$(document).ready(function(){
    $('.responsive').slick({
        autoplay: true,
        autoplaySpeed: 3000,
        centerMode: true,
        variableWidth: true,
        centerPadding: '60px',
        slidesToShow: 3,
        responsive: [
            {
            breakpoint: 768,
            settings: {
                centerMode: true,
                centerPadding: '10px',
                slidesToShow: 3
            }
            },
            {
            breakpoint: 480,
            settings: {
                centerMode: true,
                centerPadding: '10px',
                slidesToShow: 1
            }
            }
        ]
    });
});
</script>
