<section class="collection-heading heading-content ">
    <div class="container">
        <div class="row">
            <div class="col-sm-9 col-sm-push-3" style="padding-left: 8px; padding-right: 0;">
                <div class="collection-wrapper">
                    <h1 class="collection-title"><span><?php echo $data->name; ?></span></h1>
                    <?php echo $breadcrumb; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="collection-content">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div id="shopify-section-collection-template" class="shopify-section">
                    <div class="collection-inner">
                        <div id="tags-load" style="display:none;"><i class="fa fa-spinner fa-pulse fa-2x"></i></div>
                        <div id="collection">
                            <div class="collection_inner">
                                <div class="collection-leftsidebar sidebar col-sm-3 clearfix">
                                    <div class="sidebar-block collection-block">
                                        <div class="sidebar-title">
                                            <span>Kategori</span>
                                            <i class="fa fa-caret-down show_sidebar_content" aria-hidden="true"></i>
                                        </div>
                                        <div class="sidebar-content">
                                            <ul class="list-cat">
                                                <?php
                                                $pathCategories = explode(',', $data->path);
                                                $categories = $this->category->get_categories(0);
                                                foreach ($categories->result() as $category) {
                                                    ?>
                                                    <li class="<?php echo (in_array($category->id, $pathCategories)) ? 'active' : ''; ?>">
                                                        <a href="<?php echo seo_url('shop/catalog/categories/view/' . $category->id); ?>"><?php echo $category->name; ?></a>
                                                        <?php if (in_array($category->id, $pathCategories) && $subcategories = $this->category->get_categories($category->id)) { ?>
                                                            <ul>
                                                                <?php foreach ($subcategories->result() as $subcategory) { ?>
                                                                    <li class="<?php echo (in_array($subcategory->id, $pathCategories)) ? 'active' : ''; ?>">
                                                                        <a href="<?php echo seo_url('shop/catalog/categories/view/' . $subcategory->id); ?>"><?php echo $subcategory->name; ?></a>
                                                                        <?php if (in_array($subcategory->id, $pathCategories) && $subcategories2 = $this->category->get_categories($subcategory->id)) { ?>
                                                                            <ul>
                                                                                <?php foreach ($subcategories2->result() as $subcategory2) { ?>
                                                                                    <li class="<?php echo (in_array($subcategory2->id, $pathCategories)) ? 'active' : ''; ?>"><a href="<?php echo seo_url('shop/catalog/categories/view/' . $subcategory2->id); ?>"><?php echo $subcategory2->name; ?></a></li>
                                                                                <?php } ?>
                                                                            </ul>
                                                                        <?php } ?>
                                                                    </li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php } ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="collection-mainarea col-sm-9 clearfix">
                                <?php
                                if($data->id == 3 || $data->id == 105) { ?>
                                <div><img class="img-responsive" src="<?php echo base_url('assets/frontend/images/mytech-banner.jpeg'); ?>"></div>
                                <?php } ?>
                                    <?php if ($products->num_rows() > 0) { ?>
                                        <div class="collection_toolbar">
                                            <div class="toolbar_left">
                                                Menampilan <?php echo number($start); ?> sampai <?php echo number($to); ?> produk dari total <?php echo number($total_products); ?> produk
                                            </div>
                                            <div class="toolbar_right">
                                                <div class="group_toolbar">
                                                    <form id="form-filter">
                                                        <div class="sortBy">
                                                            <span class="toolbar_title">Urutkan:</span>
                                                            <div class="control-container">
                                                                <select name="sort" tabindex="-1">
                                                                    <option value="popular" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'popular') ? 'selected' : ''; ?>>Terpopuler</option>
                                                                    <option value="nameAZ" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'nameAZ') ? 'selected' : ''; ?>>Nama (A-Z)</option>
                                                                    <option value="nameZA" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'nameZA') ? 'selected' : ''; ?>>Nama (Z-A)</option>
                                                                    <option value="lowprice" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'lowprice') ? 'selected' : ''; ?>>Termurah</option>
                                                                    <option value="highprice" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'highprice') ? 'selected' : ''; ?>>Termahal</option>
                                                                    <option value="newest" <?php echo ($this->input->get('sort') && $this->input->get('sort') == 'newest') ? 'selected' : ''; ?>>Terbaru</option>
                                                                </select>
                                                            </div>
                                                            <span class="toolbar_title">Tampilkan:</span>
                                                            <div class="control-container">
                                                                <select name="show" tabindex="-1">
                                                                    <option value="20" <?php echo ($this->input->get('show') && $this->input->get('show') == '20') ? 'selected' : ''; ?>>20</option>
                                                                    <option value="40" <?php echo ($this->input->get('show') && $this->input->get('show') == '40') ? 'selected' : ''; ?>>40</option>
                                                                    <option value="80" <?php echo ($this->input->get('show') && $this->input->get('show') == '80') ? 'selected' : ''; ?>>80</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="collection-items clearfix">
                                            <div class="products">
                                                <?php foreach ($products->result() as $product) { 
                                                    $sale_on = FALSE;
                                                    if($product->promo == 1) {
                                                        if ($product->merchant == 0) {
                                                            $merchant_user = $this->session->userdata('merchant_user');
                                                            $merchant_group = $merchant_user->merchant_group;
                                                            $price_group = $this->main->get('product_price', array('product' => $product->id, 'merchant_group' => $merchant_group));
                                                            if ($price_group) {
                                                                $product->price = $price_group->price;
                                                            }
                                                        }
                                                        $promo_data = json_decode($product->promo_data,true);
                                                        if($promo_data['type'] == 'P') {
                                                            $disc_price = round(($product->price * $promo_data['discount']) / 100);
                                                            $label_disc = $promo_data['discount'].'% off';
                                                        } else {
                                                            $disc_price = $promo_data['discount'];
                                                            $label_disc = 'SALE';
                                                        }
                                                        $end_price= $product->price - $disc_price;
                                                        $today = date('Y-m-d');
                                                        $today=date('Y-m-d', strtotime($today));
                                                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                                                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));

                                                        if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                                            $sale_on = TRUE;
                                                        }
                                                    }
                                                    
                                                    ?>
                                                    <div class="product-item col-sm-3">
                                                        <div class="product">
                                                            <?php if($sale_on) {?>
                                                            <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;"><?php echo $label_disc;?></span>
                                                            <?php } ?>
                                                            <div class="row-left">
                                                                <a href="<?php echo seo_url('shop/catalog/products/view/' . $product->id); ?>" class="hoverBorder container_item">
                                                                    <img src="<?php echo ($product->image) ? site_url('../files/images/' . $product->image) : site_url('../assets/frontend/images/noimage.jpg'); ?>" class="img-responsive" alt="<?php echo $product->name; ?>">
                                                                </a>
                                                                <div class="hover-mask">
                                                                    <div class="group-mask">
                                                                        <div class="inner-mask">
                                                                            <div class="group-actionbutton">
                                                                                <div class="effect-ajax-cart">
                                                                                    <?php if ($this->main->gets('product_option', array('product' => $product->id))) { ?>
                                                                                        <a class="btn add-to-cart" href="<?php echo seo_url('shop/catalog/products/view/' . $product->id); ?>">Beli Sekarang</a>
                                                                                    <?php } else { ?>
                                                                                        <?php if ($product->merchant != 0 && $product->quantity == 0) { ?>
                                                                                            <a class="btn add-to-cart" href="<?php echo seo_url('shop/catalog/products/view/' . $product->id); ?>">Beli Sekarang</a>
                                                                                        <?php } else { ?>
                                                                                            <button class="btn add-to-cart" type="button" data-id="<?php echo encode($product->id); ?>">Beli Sekarang</button>
                                                                                        <?php } ?>
                                                                                    <?php } ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="row-right animMix">
                                                                <div class="grid-mode">
                                                                    <div class="product-title"><a class="title-5" href="<?php echo seo_url('shop/catalog/products/view/' . $product->id); ?>"><?php echo $product->name; ?></a></div>
                                                                    <div class="rating-star">
                                                                        <span class="spr-badge" data-rating="5.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i><i class="spr-icon spr-icon-star" style=""></i></span><span class="spr-badge-caption">1 review</span>
                                                                        </span>
                                                                    </div>
                                                                    <div class="product-price">
                                                                        <?php if($sale_on) {?>
                                                                        <span class="money" style="text-decoration: line-through;font-size:9px;color:#8b8f8b"><?php echo rupiah($product->price); ?></span>
                                                                        <span class="money"><?php echo rupiah($end_price); ?></span>
                                                                        <?php } else { ?>
                                                                        <span class="price_sale"><span class="money"><?php echo rupiah($product->price); ?></span></span>
                                                                        <?php } ?>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php if ($pagination) { ?>
                                            <div class="collection-bottom-toolbar">
                                                <div class="product-pagination">
                                                    <div class="pagination_group">
                                                        <?php echo $pagination; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <p>Tidak ada produk di kategori ini.</p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
