<section class="collection-heading heading-content">
    <div class="container">
        <div class="cart-step">
            <ul>
                <li class="active">1. Belanjaan Saya</li>
                <li>2. Alamat</li>
                <li>3. Pengiriman</li>
                <li>4. Pembayaran</li>
                <li>5. Selesai</li>
            </ul>
        </div>
        <div class="cart-title">1. Belanjaan Saya</div>
    </div>
</section>
<section class="cart-content checkout">
    <div class="container">
        <?php if ($this->cart->contents()) { ?>
            <div class="cart-product">
                <?php foreach ($this->cart->contents() as $item) { ?>
                    <div class="cart-item" id="cart-<?php echo $item['rowid']; ?>">
                        <div class="cart-item-left">
                            <div class="item-image">
                                <a href="<?php echo seo_url('catalog/products/view/' . $item['id']); ?>">
                                    <img src="<?php echo get_image($item['image']); ?>">
                                </a>
                            </div>
                            <div class="item-desc">
                                <div class="item-name"><a href="<?php echo seo_url('catalog/products/view/' . $item['id']); ?>"><?php echo $item['name']; ?></a></div>
                                <div class="cart-line">
                                    <?php if ($item['options']) { ?>
                                        <?php foreach ($item['options'] as $option) { ?>
                                            <div class="option"><?php echo $option['option_name']; ?></div>
                                        <?php } ?>
                                        <div class="divider"></div>
                                    <?php } ?>
                                    <div class="info"><?php echo $item['code']; ?></div>
                                    <div class="info"><?php echo $item['weight']; ?> gram</div>
                                    <div class="info">Min Qty : <?php echo $item['min_qty']; ?></div>
                                </div>                                                
                                <?php if ($item['merchant']) { ?>
                                    <div class="cart-line">
                                        <span class="merchant">Sold by: <?php echo $item['merchant_name']; ?></span>
                                    </div>
                                <?php } ?>
                                <div class="alertEmptyStock" id="alert-<?php echo $item['rowid']; ?>" style="color:red;"></div>
                            </div>
                        </div>
                        <div class="cart-item-right">
                            <div class="cart-price">
                                <span class="price"><?php echo rupiah($item['price']); ?></span>
                                <span>
                                    <input type="number" size="3" min="<?php echo $item['min_qty']; ?>" max="999" value="<?php echo $item['qty']; ?>" onchange="javascript:window.location.href = '<?php echo site_url('shop/update_cart/' . $item['rowid'] . '/'); ?>' + this.value">
                                </span>
                                <span class="total"><?php echo rupiah($item['subtotal']); ?></span>
                                <span class="delete"><a href="<?php echo site_url('shop/delete_cart/' . $item['rowid']); ?>">Hapus</a></span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="cart-footer">
                <div class="total">
                    <span>Total Belanja</span>
                    <span><?php echo rupiah($this->cart->total()); ?></span>
                </div>
                <div class="action">
                    <a href="<?php echo site_url('shop'); ?>"><i class="fa fa-arrow-left"></i> Lanjut Belanja</a>
                    <a href="javascript:void(0)" onclick="checkStock()" class="btn">Beli Sekarang <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
        <?php } else { ?>
            <h3>Keranjang belanja kosong.</h3>
        <?php } ?>
    </div>
</section>
