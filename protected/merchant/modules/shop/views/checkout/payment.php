<section class="collection-heading heading-content ">
    <div class="container">
        <div class="cart-step">
            <ul>
            <li class="active">1. Belanjaan Saya</li>
                <li class="active">2. Alamat</li>
                <li class="active">3. Pengiriman</li>
                <li class="active">4. Pembayaran</li>
                <li>5. Selesai</li>
            </ul>
        </div>
        <div class="cart-title">4. Pembayaran</div>
    </div>
</section>
<section class="cart-content checkout">
    <div class="container">
        <?php if (isset($message)) { ?>
            <div class="alert alert-danger fade in alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <?php echo $message; ?>
            </div>
        <?php } ?>
        <form class="form" action="" method="post">
            <div class="payment">
                <div class="left">
                    <div class="header">
                        <div class="title">Pilih Metode Pembayaran</div>
                    </div>
                    <div class="body">
                        <div id="payment-list">
                            <?php
                            if ($payment_methods) {
                                foreach ($payment_methods->result() as $payment_method) {
                                    if ($payment_method->name == 'alfamart' && ($this->cart->total() + $shipping_cost) > 5000000) {
                                        
                                    } else if($payment_method->name == 'kredivo' && $pickup_count > 0){

                                    } else {

                                        ?>
                                        <div class="payment-method-block">
                                            <div class="payment-method-detail">
                                                <input type="radio" value="<?php echo $payment_method->name; ?>" onclick="selectPayment(this);" name="payment" required id="payment-<?php echo $payment_method->name; ?>">
                                                <label for="payment-<?php echo $payment_method->name; ?>">
                                                    <div class="payment-method-title"><?php echo $payment_method->title; ?></div>
                                                    <div class="payment-method-subtitle"><?php echo $payment_method->description; ?></div>
                                                </label>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="right">
                    <div class="header">
                        <div class="title">Ringkasan Pemesanan</div>
                    </div>
                    <div class="body">
                        <div class="cart-summary">
                            <?php
                            foreach ($this->cart->contents() as $item) {
                                $id = explode('-', $item['id']);
                                ?>
                                <div class="cart-item">
                                    <div class="cart-item-left">
                                        <div class="item-image">
                                            <a href="<?php echo seo_url('catalog/products/view/' . $id[0]); ?>">
                                                <img src="<?php echo get_image($item['image']); ?>">
                                            </a>
                                        </div>
                                        <div class="item-desc">
                                            <div class="item-name"><a href="<?php echo seo_url('catalog/products/view/' . $item['id']); ?>"><?php echo $item['name']; ?></a></div>
                                            <div class="cart-line">
                                                <?php if ($item['options']) { ?>
                                                    <?php foreach ($item['options'] as $option) { ?>
                                                        <div class="option"><?php echo $option['option_name']; ?></div>
                                                    <?php } ?>
                                                    <div class="divider"></div>
                                                <?php } ?>
                                                <div class="info"><?php echo $item['code']; ?></div>
                                                <div class="info"><?php echo $item['weight']; ?> gram</div>
                                            </div>
                                            <div class="cart-line">
                                                <span class="price"><?php echo rupiah($item['subtotal']); ?> <div class="info">(<?php echo $item['qty'] . ' x ' . rupiah($item['price']); ?>)</div></span>
                                            </div>
                                            <div class="cart-line">
                                                <span class="info">Dikirim oleh: <strong><?php echo $this->main->get('merchants', array('id' => $item['shipping_merchant']))->name; ?></strong></span>
                                            </div>
                                            <div class="cart-line">
                                                <span class="info">
                                                    <?php
                                                    $shipping = $item['shipping'];
                                                    if ($shipping == 'pickup') {
                                                        echo '<strong>Diambil Sendiri</strong>';
                                                    } else {
                                                        echo 'Jasa Pengiriman: ';
                                                        $shipping = explode('-', $shipping);
                                                        echo '<strong>' . strtoupper($shipping[0]) . ' - ' . $shipping[1] . '</strong>';
                                                    }
                                                    ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <hr>
                        <ul class="purchase-summary">
                            <li>
                                <span class="summary-title">Alamat Pengiriman</span>
                                <span class="summary-amount">
                                    <div style="display: table-cell;vertical-align: top;padding-left: 5px;">
                                        <p><?php echo $address->address; ?><br>
                                            <?php echo $this->main->get('districts', array('id' => $address->district))->name . ', ' . $this->main->get('cities', array('id' => $address->city))->name; ?><br>
                                            <?php echo $this->main->get('provincies', array('id' => $address->province))->name; ?>
                                        </p>
                                    </div>
                                </span>
                            </li>
                            <li style="background-color:#97C23C;padding:10px">
                                <span style="margin-bottom:10px">Kode Kupon</span>
                                <?php if($this->session->userdata('coupon')) {
                                    if($this->session->userdata('coupon')->type =='P') {
                                        $total_disc = $this->cart->total() * ($this->session->userdata('coupon')->discount/100);
                                    } else {
                                        $total_disc = $this->session->userdata('coupon')->discount;
                                    }
                                }
                                ?>
                                <div id="coupon-input">
                                    <?php if($this->session->userdata('coupon')) {
                                    ?>
                                    <div class="alert alert-success  alert-dismissable">
                                        <a href="javascript:void(0)" onclick="resetCoupon()" class="close" aria-label="close">×</a>
                                        <strong>Kupon "<?php echo $this->session->userdata('coupon')->code ?>" sedang digunakan</strong>
                                    </div>
                                    <?php } else {?>
                                    <input type="text" class="couponCode" name="couponCode" style="float:left;height:40px;width:60%;" value="<?php echo $this->session->userdata('coupon') ? $this->session->userdata('coupon')->code:''?>">
                                    <a href="javascript:void(0)" onclick="submitCoupon()" class="btn btn-default btn-sm">Submit</a>
                                    <?php } ?>
                                </div>
                                    <div id="coupon-alert">
                                    </div>
                            </li>
                            <li>
                                <span class="summary-title">Sub total (<b><?php echo $this->cart->total_items(); ?></b> produk)</span>
                                <span class="summary-amount"><?php echo rupiah($this->cart->total()); ?></span>
                            </li>
                            <li>
                                <span class="summary-title">Biaya pengiriman</span>
                                <span id="shippingFee" class="summary-amount"><?php echo rupiah($count_total_shipping); ?></span>
                            </li>
                            <li id="showDisc" style="<?php echo $this->session->userdata('coupon') ? '':'display:none;'?>">
                                <span class="summary-title">Potongan</span>
                                <span id="coupon_disc" class="summary-amount"><?php echo $this->session->userdata('coupon') ? rupiah($total_disc) :''?></span>
                            </li>
                            <li>
                                <span class="summary-title"><b>Total pembayaran</b></span>
                                <span class="summary-amount"><b id="totalShopping"><?php echo rupiah($this->cart->total() + $count_total_shipping - ($this->session->userdata('coupon')?$total_disc:0)); ?></b></span>
                            </li>
                            <hr>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="cart-footer">
                <div class="action">
                    <a href="javascript:void(0);" class="btn" style="width: 100%; font-size: 18px;" onclick="submitPaymentForm()">BAYAR SEKARANG</a>
                </div>
            </div>
        </form>
            <div class="modal fade" id="KredivoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" 
                        data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">
                            Kredivo Agreement
                        </h4>
                    </div>
                    
                    <!-- Modal Body -->
                    <div class="modal-body">
                        <div>
                            <div>
                                <p>Anda akan diarahkan ke halaman kredivo</p>
                                <ol style="list-style-type: lower-alpha;">  
                                    <li style="list-style: inherit;margin-left:15px;"> Anda harus mempunyai akun Kredivo, atau registrasi ekspres pada halaman Kredivo setelah klik bayar </li>
                                    <li style="list-style: inherit;margin-left:15px;"> Siapkan dokumen berikut untuk diunggah (khusus pengguna baru Kredivo) </li>
                                    <li style="list-style: inherit;margin-left:15px;"> Kartu Identitas (KTP) </li>
                                    <li style="list-style: inherit;margin-left:15px;"> Bukti Tempat Tinggal (Akun E-commerce) </li>
                                    <li style="list-style: inherit;margin-left:15px;"> Bukti Penghasilan (Akun Internet Banking) </li>
                                    <li style="list-style: inherit;margin-left:15px;"> Berdomisili di Jabodetabek, Kota Bandung, Medan, Surabaya, Palembang, Semarang atau Denpasar </li>
                                    <li style="list-style: inherit;margin-left:15px;"> Berpenghasilan min. Rp 3.000.000 per bulan </li>
                                    <li style="list-style: inherit;margin-left:15px;"> Metode Tunda Pembayaran hanya untuk pembayaran maks. Rp 3.000.000 </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    
                    <!-- Modal Footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default"
                                data-dismiss="modal">
                                    Close
                        </button>
                        <a href="javascript:void(0)" class="btn btn-primary" onclick="agreeKredivo()">
                           Lanjutkan
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
