<section class="collection-heading heading-content ">
    <div class="container">
        <div class="cart-step">
            <ul>
                <li class="active">1. Belanjaan Saya</li>
                <li class="active">2. Alamat</li>
                <li class="active">3. Pengiriman</li>
                <li>4. Pembayaran</li>
                <li>5. Selesai</li>
            </ul>
        </div>
        <div class="cart-title">3. Pengiriman</div>
    </div>
</section>
<section class="cart-content checkout">
    <div class="container">
        <?php if (isset($message)) { ?>
            <div class="alert alert-danger fade in alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                <?php echo $message; ?>
            </div>
        <?php } ?>
        <form class="form" action="" method="post">
            <input type="hidden" id="total" value="<?php echo $this->cart->total(); ?>">
            <div class="shipping">
                <?php foreach ($this->cart->contents() as $item) { ?>
                <input type="hidden" id="shipping-cost-<?php echo $item['rowid']; ?>" name="shipping_cost[<?php echo $item['rowid']; ?>]" value="">
                    <div class="cart-item">
                        <div class="cart-item-left">
                            <div class="item-image">
                                <a href="<?php echo seo_url('catalog/products/view/' . $item['id']); ?>">
                                    <img src="<?php echo get_image($item['image']); ?>">
                                </a>
                            </div>
                            <div class="item-desc">
                                <div class="item-name"><a href="<?php echo seo_url('catalog/products/view/' . $item['id']); ?>"><?php echo $item['name']; ?></a></div>
                                <div class="cart-line">
                                    <?php if ($item['options']) { ?>
                                        <?php foreach ($item['options'] as $option) { ?>
                                            <div class="option"><?php echo $option['option_name']; ?></div>
                                        <?php } ?>
                                        <div class="divider"></div>
                                    <?php } ?>
                                    <div class="info"><?php echo $item['code']; ?></div>
                                    <div class="info"><?php echo $item['weight']; ?> gram</div>
                                </div>
                                <div class="cart-line">
                                    <span class="price"><?php echo rupiah($item['subtotal']); ?> <div class="info">(<?php echo $item['qty'] . ' x ' . rupiah($item['price']); ?>)</div></span>
                                </div>
                                <?php if ($item['merchant']) { ?>
                                    <div class="cart-line">
                                        <span class="merchant">Sold by: <?php echo $item['merchant_name']; ?></span>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="cart-item-right">
                            <div class="shipment">
                                <?php
                                $pickup = true;
                                if ($item['merchant']) {
                                    $merchant = $this->main->get('merchants', array('id' => $item['merchant']));
                                    ?>
                                    <span class="product-merchant">Dikirim oleh: <?php echo $item['merchant_name']; ?></span> <a href="javascript:void(0);" class="info-store" data-id="<?php echo $item['rowid']; ?>" data-toggle="popover" data-trigger="focus" data-html="true">Info Toko</a>
                                    <input type="hidden" id="shipping-<?php echo $item['rowid']; ?>" name="shipping[<?php echo $item['rowid']; ?>][merchant]" value="<?php echo $item['merchant']; ?>">
                                    <?php
                                } else {
                                    $merchant_stock = array();
                                    $merchant_valid = array();
                                    $echo_merchant = "";
                                    $count_valid = 0;
                                    if($item['type'] == 'p') {
                                        $package_items = $item['package_items'];
                                        $count_packages = count($package_items);
                                        
                                            $pickup = true;
                                            $merchant = $this->checkout->get_branch($merchant_user->merchant_group);
                                        ?>
                                        <span class="product-merchant">Dikirim Oleh: <?php echo $merchant->name; ?> <a href="javascript:void(0);" class="info-store" data-id="<?php echo $item['rowid']; ?>" data-toggle="popover" data-trigger="focus" data-html="true">Info Toko</a></span>
                                            <input type="hidden" id="shipping-<?php echo $item['rowid']; ?>" name="shipping[<?php echo $item['rowid']; ?>][merchant]" value="<?php echo $merchant->id; ?>">                        
                                        <?php
                                        
                                    } else {
                                            $pickup = true;
                                            $merchant = $this->checkout->get_branch($merchant_user->merchant_group);
                                            ?>
                                            <span class="product-merchant">Dikirim Oleh: <?php echo $merchant->name; ?> <a href="javascript:void(0);" class="info-store" data-id="<?php echo $item['rowid']; ?>" data-toggle="popover" data-trigger="focus" data-html="true">Info Toko</a></span>
                                            <input type="hidden" id="shipping-<?php echo $item['rowid']; ?>" name="shipping[<?php echo $item['rowid']; ?>][merchant]" value="<?php echo $merchant->id; ?>">                               
                                            <?php
                                    }
                                    
                                }
                                //var_dump($merchant->district);exit();
                                ?>
                                <?php if ($this->agent->is_mobile()) { ?>
                                    <select class="form-control choose-shipping" name="shipping[<?php echo $item['rowid']; ?>][service]" required>
                                        <option value="" disabled="" selected="">-- Pilih jasa pengiriman --</option>
                                        <?php if ($pickup) { ?>
                                            <option value="pickup" data-id="<?php echo $item['rowid']; ?>" data-value="0">Ambil Sendiri <span style="color:red;font-size:10px">* Tidak support kredivo</span></option>
                                        <?php } ?>
                                        <?php
                                        $couriers = json_decode($merchant->shipping);
                                        if ($couriers) {
                                            foreach ($couriers as $courier) {
                                                $services = $this->rajaongkir->cost($merchant->district, 'subdistrict', $address->district, 'subdistrict', ($item['weight']), $courier);
                                                if ($services) {
                                                    $services = json_decode($services);
                                                    if ($services->rajaongkir->status->code == 200) {
                                                        foreach ($services->rajaongkir->results as $service) {
                                                            foreach ($service->costs as $cost) {
                                                                ?>
                                                                <option value="<?php echo $service->code . '-' . $cost->service; ?>" data-id="<?php echo $item['rowid']; ?>" data-value="<?php echo $cost->cost[0]->value; ?>"><?php echo strtoupper($service->code) . ' ' . $cost->service; ?> <span>(<?php echo rupiah($cost->cost[0]->value); ?>)</option>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                    </select>
                                <?php } else { ?>
                                    <ul id="shipping-courier-<?php echo $item['rowid']; ?>">
                                        <?php
                                        if ($pickup) {
                                            ?>
                                            <li>
                                                <input id="<?php echo $item['rowid']; ?>-pickup" name="shipping[<?php echo $item['rowid']; ?>][service]" type="radio" value="pickup" class="choose-shipping" required="" data-id="<?php echo $item['rowid']; ?>" data-value="0">
                                                <label for="<?php echo $item['rowid']; ?>-pickup">Ambil Sendiri <span style="color:red;font-size:10px">* Tidak support kredivo</span></label>
                                            </li>
                                            <?php
                                        }
                                        $couriers = json_decode($merchant->shipping);
                                        if ($couriers) {
                                            foreach ($couriers as $courier) {
                                                $services = $this->rajaongkir->cost($merchant->district, 'subdistrict', $address->district, 'subdistrict', ($item['weight']), $courier);
                                                if ($services) {
                                                    $services = json_decode($services);
                                                    if ($services->rajaongkir->status->code == 200) {
                                                        foreach ($services->rajaongkir->results as $service) {
                                                            foreach ($service->costs as $cost) {
                                                                if($cost->service == "CTC"){
                                                                    $label_service = "REG";
                                                                } else if ($cost->service == "CTCYES"){
                                                                    $label_service = "YES";
                                                                } else {
                                                                    $label_service = $cost->service;
                                                                }
                                                                ?>
                                                                <li>
                                                                    <input name="shipping[<?php echo $item['rowid']; ?>][service]" type="radio" value="<?php echo $service->code . '-' . $cost->service; ?>" class="choose-shipping" id="<?php echo $item['rowid'] . $service->code . '-' . $cost->service; ?>" required="" data-id="<?php echo $item['rowid']; ?>" data-value="<?php echo $cost->cost[0]->value; ?>">
                                                                    <label for="<?php echo $item['rowid'] . $service->code . '-' . $cost->service; ?>"><?php echo strtoupper($service->code) . ' ' . $label_service; ?> <span>(<?php echo rupiah($cost->cost[0]->value); ?>)</span></label>
                                                                </li>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                    </ul>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="cart-footer">
                <div class="total">
                    <span>Estimasi Biaya Kirim</span>
                    <span id="shipping-cost-text" style="color: #000; font-size: 14px;"><?php echo rupiah(0); ?></span>
                </div>
                <div class="total">
                    <span>Total</span>
                    <span id="total-text"><?php echo rupiah($this->cart->total()); ?></span>
                </div>
                <div class="action">
                    <a href="<?php echo site_url('checkout/address'); ?>"><i class="fa fa-arrow-left"></i> Kembali ke Alamat</a>
                    <button type="submit" class="btn">LANJUTKAN <i class="fa fa-arrow-right"></i></button>
                </div>
            </div>
        </form>
    </div>
</section>
