<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home_model extends CI_Model {

    function bestsellers($limit = 6) {
        $this->db->select('COUNT(op.quantity) total, p.id, p.name, p.price, pi.image')
                ->join('products p', 'p.id = op.product', 'left')
                ->join('product_image pi', 'op.product = pi.product AND primary = 1', 'left')
                ->where('p.status', 1)
                ->group_by('op.product')
                ->order_by('RAND()')
                ->limit($limit);
        $query = $this->db->get('order_product op');
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function promotions($limit = 6) {
        $this->db->select('p.id, pd.name, p.price, p.image')
                ->join('product_description pd', 'p.id = pd.id', 'left')
                ->where('p.brand', 11)
                ->where('p.status !=', 0)
                ->order_by('RAND()')
                ->limit($limit);
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function get_promotions($limit = 6) {
        $this->db->select('p.id, p.name, p.merchant, p.price,p.promo_data, pi.image')
                ->join('product_image pi', 'pi.product = p.id AND pi.primary = 1', 'left')
                ->where('p.promo', 1)
                ->where('p.status !=', 0)
                ->order_by('RAND()')
                ->limit($limit);
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function get_sub_categories($parent = 0, $limit = 4) {
        $this->db->select('c.id, c.viewed, c.name')
                ->join('categories c', 'cp.category = c.id', 'left')
                ->where('cp.path', $parent)
                ->where('cp.category !=', $parent)
                ->where('active', 1)
                ->order_by('c.viewed', 'DESC')
                ->limit($limit);
        $query = $this->db->get('category_path cp');
        return ($query->num_rows() > 0) ? $query : FALSE;
    }
    function get_branch($group) {
        $this->db->join('merchants m', 'mg.branch = m.id', 'left')
                ->where('mg.id', $group)
                ->select('m.*');
        return $this->db->get('merchant_groups mg')->row();
    }
    function get_branchx($location) {
        //if ($CI->session->has_userdata('location')) {
            $merchant = $this->db->query("SELECT id, `name`, `group`, (6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) AS distance FROM merchants WHERE status = 1 HAVING distance < 80 ORDER BY distance");
            if ($merchant->num_rows() > 0) {
                $this->session->set_userdata('list_merchant', $merchant->result_array());
            } else {
                $merchant = $this->db->query("SELECT id, `name`, `group`, (6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) AS distance FROM merchants WHERE status = 1 ORDER BY distance LIMIT 5");
                $this->session->set_userdata('list_merchant', $merchant->result_array());
            }
        //}
        // $this->db->select('c.id, c.viewed, c.name')
        //         ->join('categories c', 'cp.category = c.id', 'left')
        //         ->where('cp.path', $parent)
        //         ->where('cp.category !=', $parent)
        //         ->where('active', 1)
        //         ->order_by('c.viewed', 'DESC')
        //         ->limit($limit);
        // $query = $this->db->get('category_path cp');
        return ($query->num_rows() > 0) ? $query : FALSE;
    }

    function products($category, $merchant, $merchant_group, $limit = 9) {
        $merchant = implode(',', $merchant);
        return $this->db->query("SELECT * FROM (
                    SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, (CASE WHEN pp.price IS NULL THEN p.price ELSE pp.price END) price, pi.image, pp.id ppid
                    FROM products p
                    LEFT JOIN product_price pp ON pp.product = p.id AND pp.merchant_group = $merchant_group
                    LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
                    WHERE p.merchant = 0 AND p.status = 1
                    UNION
                    SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, p.price, pi.image, 0
                    FROM products p
                    LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
                    WHERE merchant IN ($merchant) AND p.status = 1
                    ) p
                 WHERE p.category IN (SELECT category FROM category_path WHERE path = $category) LIMIT $limit");
    }

}
