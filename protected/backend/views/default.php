<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo "{$title}"; ?></title>
    <?php
    foreach ($css as $file) {
        echo "\n";
        echo '<link href="' . $file . '?timestamp=' . time() . '" rel="stylesheet" type="text/css" />';
    }
    echo "\n";
    ?>
    <script type="text/javascript">
        var base_url = '<?php echo base_url(); ?>';
        var site_url = '<?php echo site_url(); ?>';
        var current_url = '<?php echo current_url(); ?>';
        var decimal_digit = '<?php echo settings('number_of_decimal'); ?>';
        var decimal_separator = '<?php echo settings('number_separator_decimal'); ?>';
        var thousand_separator = '<?php echo settings('number_separator_thousand'); ?>';
        var datatable_data = {};
    </script>
</head>

<body class="<?php echo (isset($body_class)) ? $body_class : ''; ?>">
    <div class="navbar navbar-inverse">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo site_url(); ?>">
                <!--<img src="<?php echo site_url('../assets/backend/images/logo_light.png'); ?>" alt="">-->
                <?php echo settings('store_name'); ?>
            </a>

            <ul class="nav navbar-nav visible-xs-block">
                <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
                <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
            </ul>
        </div>

        <div class="navbar-collapse collapse" id="navbar-mobile">
            <ul class="nav navbar-nav">
                <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-notification2"></i>
                        <span class="visible-xs-inline-block position-right">Notifikasi</span>
                        <span class="badge bg-warning-400" id="notification-delivery-late-count"><?php echo count($notifications); ?></span>
                    </a>

                    <div class="dropdown-menu dropdown-content">
                        <div class="dropdown-content-heading">
                            Pemberitahuan Pesanan
                        </div>
                        <?php if ($notifications) { ?>
                            <ul class="media-list dropdown-content-body width-350">
                                <?php foreach ($notifications as $notif) { ?>
                                    <li class="media" onclick="window.location.href = '<?php echo $notif['url']; ?>'">
                                        <div class="media-body">
                                            <?php echo $notif['msg']; ?>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>

                            <div class="dropdown-content-footer">
                                <a href="<?php echo site_url('orders'); ?>" target="_blank" data-popup="tooltip" title="Pesanan"><i class="icon-menu display-block"></i></a>
                            </div>
                        <?php } else { ?>
                            <p class="pl-20 pr-20">Tidak ada pemberitahuan.</p>
                        <?php } ?>
                    </div>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-coins"></i>
                        <span class="visible-xs-inline-block position-right">Permintaan Tarik Dana</span>
                        <span class="badge bg-warning-400" id="notification-delivery-late-count"><?php echo count($withdrawals); ?></span>
                    </a>

                    <div class="dropdown-menu dropdown-content">
                        <div class="dropdown-content-heading">
                            Permintaan Tarik Dana
                        </div>
                        <?php if ($withdrawals) { ?>
                            <ul class="media-list dropdown-content-body width-350">
                                <?php foreach ($withdrawals as $wd) { ?>
                                    <li class="media">
                                        <div class="media-body">
                                            <?php echo $wd; ?>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>

                            <div class="dropdown-content-footer">
                                <a href="<?php echo site_url('merchants/withdrawals'); ?>" target="_blank" data-popup="tooltip" title="Permintaan Tarik Dana"><i class="icon-menu display-block"></i></a>
                            </div>
                        <?php } else { ?>
                            <p class="pl-20 pr-20 width-350">Tidak ada pemberitahuan.</p>
                        <?php } ?>
                    </div>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-store2"></i>
                        <span class="visible-xs-inline-block position-right">Merchant</span>
                        <span class="badge bg-warning-400" id="notification-delivery-late-count"><?php echo count($merchant_notifs); ?></span>
                    </a>

                    <div class="dropdown-menu dropdown-content">
                        <div class="dropdown-content-heading">
                            Merchant
                        </div>
                        <?php if ($merchant_notifs) { ?>
                            <ul class="media-list dropdown-content-body width-350">
                                <?php foreach ($merchant_notifs as $mn) { ?>
                                    <li class="media">
                                        <a href="<?php echo site_url('merchants?status=0'); ?>">
                                            <div class="media-body">
                                                Merchant "<?php echo $mn['name']; ?>" belum aktif
                                            </div>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } else { ?>
                            <p class="pl-20 pr-20 width-350">Tidak ada Merchant yang belum aktif.</p>
                        <?php } ?>
                    </div>
                </li>
                <li class="dropdown">
                    <?php $principle_notifications = $this->main->gets('principle_register', ['status' => 0]) ?>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-store"></i>
                        <span class="visible-xs-inline-block position-right">Principal</span>
                        <span class="badge bg-warning-400" id="notification-delivery-late-count"><?php echo ($principle_notifications) ? count($principle_notifications->result()) : 0 ?></span>
                    </a>

                    <div class="dropdown-menu dropdown-content">
                        <div class="dropdown-content-heading">
                            Principal
                        </div>
                        <?php if ($principle_notifications) { ?>
                            <ul class="media-list dropdown-content-body width-350">
                                <?php foreach ($principle_notifications->result() as $principle) { ?>
                                    <li class="media">
                                        <a href="<?php echo site_url('principle/request?status=0'); ?>">
                                            <div class="media-body">
                                                Principal "<?php echo $principle->company_name; ?>" ingin bergabung
                                            </div>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        <?php } else { ?>
                            <p class="pl-20 pr-20 width-350">Tidak ada Principal yang ingin bergabung.</p>
                        <?php } ?>
                    </div>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <span><?php echo $user->username; ?></span>
                        <i class="caret"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <!--                            <li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
                            <li><a href="#"><i class="icon-coins"></i> My balance</a></li>
                            <li><a href="#"><span class="badge bg-teal-400 pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>-->
                        <li><a href="<?php echo site_url('auth/logout'); ?>"><i class="icon-switch2"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="page-container">
        <div class="page-content">
            <div class="sidebar sidebar-main">
                <div class="sidebar-content">
                    <div class="sidebar-category sidebar-category-visible">
                        <div class="category-content no-padding">
                            <ul class="navigation navigation-main navigation-accordion">

                                <li class="<?php echo menu_active($menu, 'dashboard'); ?>">
                                    <a href="<?php echo site_url(); ?>"><i class="icon-home4"></i> <span><?php echo lang('dashboard'); ?></span></a>
                                </li>

                                <?php if ($this->aauth->is_allowed('catalog')) { ?>
                                    <li class="<?php echo menu_active($menu, 'catalog'); ?>">
                                        <a href="#"><i class="icon-archive"></i> <span><?php echo lang('catalog'); ?></span></a>
                                        <ul>
                                            <?php if ($this->aauth->is_allowed('catalog/product')) { ?>
                                                <li class="<?php echo menu_active($menu, 'catalog_product'); ?>">
                                                    <a href="<?php echo site_url('catalog/products'); ?>"><?php echo lang('product'); ?></a>
                                                    <!-- <ul>
                                                            <li class="<?php //echo menu_active($menu, 'catalog_product_tanaka'); 
                                                                        ?>"><a href="<?php //echo site_url('catalog/products'); 
                                                                                        ?>"><?php //echo lang('product_tanaka'); 
                                                                                            ?></a></li>
                                                            <li class="<?php //echo menu_active($menu, 'catalog_product_other'); 
                                                                        ?>"><a href="<?php //echo site_url('catalog/products/other'); 
                                                                                        ?>"><?php //echo lang('product_other'); 
                                                                                            ?></a></li>
                                                        </ul> -->
                                                </li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('catalog/category')) { ?>
                                                <li class="<?php echo menu_active($menu, 'catalog_category'); ?>"><a href="<?php echo site_url('catalog/categories'); ?>"><?php echo lang('category'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('catalog/deskripsi')) { ?>
                                                <li class="<?php echo menu_active($menu, 'catalog_deskripsi'); ?>"><a href="<?php echo site_url('catalog/deskripsi'); ?>"><?php echo lang('deskripsi'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('catalog/deskripsi_relasi')) { ?>
                                                <li class="<?php echo menu_active($menu, 'deskripsi_relasi'); ?>"><a href="<?php echo site_url('catalog/deskripsi_relasi'); ?>"><?php echo lang('deskripsi_relasi'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('catalog/brand')) { ?>
                                                <li class="<?php echo menu_active($menu, 'catalog_brand'); ?>"><a href="<?php echo site_url('catalog/brands'); ?>"><?php echo lang('brand'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('catalog/variation')) { ?>
                                                <li class="<?php echo menu_active($menu, 'catalog_option'); ?>"><a href="<?php echo site_url('catalog/options'); ?>"><?php echo lang('option'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('catalog/feature')) { ?>
                                                <li class="<?php echo menu_active($menu, 'catalog_feature'); ?>"><a href="<?php echo site_url('catalog/features'); ?>"><?php echo lang('feature'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('catalog/review')) { ?>
                                                <li class="<?php echo menu_active($menu, 'catalog_review'); ?>"><a href="<?php echo site_url('catalog/reviews'); ?>"><?php echo lang('review'); ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <!-- <?php if ($this->aauth->is_allowed('order')) { ?>
                                        <li class="<?php echo menu_active($menu, 'order'); ?>">
                                            <a href="#"><i class="icon-display4"></i> <span><?php echo lang('order'); ?></span></a>
                                            <ul>
                                                <?php if ($this->aauth->is_allowed('order')) { ?>
                                                    <li class="<?php echo menu_active($menu, 'order'); ?>"><a href="<?php echo site_url('orders'); ?>"><?php echo lang('order'); ?></a></li>
                                                <?php } ?>
                                                <?php if ($this->aauth->is_allowed('order/order_merchant')) { ?>
                                                    <li class="<?php echo menu_active($menu, 'order_merchant'); ?>"><a href="<?php echo site_url('orders/orders_merchant'); ?>">Pesanan Merchant</a></li>
                                                <?php } ?>
                                            </ul>
                                        </li>
                                    <?php } ?> -->
                                <?php if ($this->aauth->is_allowed('order')) : ?>
                                    <li class="<?php echo menu_active($menu, 'order'); ?>">
                                        <a href="<?php echo site_url('orders'); ?>"><i class="icon-display4"></i> <span><?php echo lang('order'); ?></span></a>
                                    </li>
                                <?php endif; ?>
                                <?php if ($this->aauth->is_allowed('order_reward')) { ?>
                                    <li class="<?php echo menu_active($menu, 'reward'); ?>"><a href="<?php echo site_url('orders/reward'); ?>"><i class="icon-stars"></i> <span>Reward</span></a></li>
                                <?php } ?>
                                <?php if ($this->aauth->is_allowed('design')) { ?>
                                    <li class="<?php echo menu_active($menu, 'design'); ?>">
                                        <a href="#"><i class="icon-display4"></i> <span><?php echo lang('design'); ?></span></a>
                                        <ul>
                                            <?php if ($this->aauth->is_allowed('design/page')) { ?>
                                                <li class="<?php echo menu_active($menu, 'design_page'); ?>"><a href="<?php echo site_url('design/pages'); ?>"><?php echo lang('page'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('design/banner')) { ?>
                                                <li class="<?php echo menu_active($menu, 'design_banner'); ?>"><a href="<?php echo site_url('design/banners'); ?>"><?php echo lang('banner'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('design/brands')) { ?>
                                                <li class="<?php echo menu_active($menu, 'design_brands'); ?>"><a href="<?php echo site_url('design/brands'); ?>"><?php echo lang('brand'); ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($this->aauth->is_allowed('marketing')) { ?>
                                    <li class="<?php echo menu_active($menu, 'marketing'); ?>">
                                        <a href="#"><i class="icon-share3"></i> <span><?php echo lang('marketing'); ?></span></a>
                                        <ul>
                                            <?php if ($this->aauth->is_allowed('marketing/coupon')) { ?>
                                                <li class="<?php echo menu_active($menu, 'marketing_coupon'); ?>"><a href="<?php echo site_url('marketing/coupons'); ?>"><?php echo lang('coupon'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('marketing/reward_points')) { ?>
                                                <li class="<?php echo menu_active($menu, 'marketing_reward_points'); ?>"><a href="<?php echo site_url('marketing/reward_points'); ?>">Reward Points</a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('marketing/subscriber')) { ?>
                                                <li class="<?php echo menu_active($menu, 'marketing_subscriber'); ?>"><a href="<?php echo site_url('marketing/subscribers'); ?>"><?php echo lang('subscriber'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('marketing/send_email')) { ?>
                                                <li class="<?php echo menu_active($menu, 'marketing_send_email'); ?>"><a href="<?php echo site_url('marketing/send_email'); ?>">Send Email</a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($this->aauth->is_allowed('merchant')) { ?>
                                    <li class="<?php echo menu_active($menu, 'merchant'); ?>">
                                        <a href="#"><i class="icon-store2"></i> <span><?php echo lang('merchant'); ?></span></a>
                                        <ul>
                                            <li class="<?php echo menu_active($menu, 'merchant_list'); ?>"><a href="<?php echo site_url('merchants'); ?>"><?php echo lang('merchant'); ?></a></li>
                                            <?php if ($this->aauth->is_allowed('merchant/distributor')) { ?>
                                                <li class="<?php echo menu_active($menu, 'merchant_distributor'); ?>"><a href="<?php echo site_url('merchants/distributor'); ?>">Distributor</a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('merchant/branch')) { ?>
                                                <li class="<?php echo menu_active($menu, 'merchant_branch'); ?>"><a href="<?php echo site_url('merchants/branches'); ?>"><?php echo lang('merchant_branch'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('merchant/group')) { ?>
                                                <li class="<?php echo menu_active($menu, 'merchant_group'); ?>"><a href="<?php echo site_url('merchants/groups'); ?>"><?php echo lang('merchant_group'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('merchant/withdrawal')) { ?>
                                                <li class="navigation-divider"></li>
                                                <li class="<?php echo menu_active($menu, 'merchant_withdrawal'); ?>"><a href="<?php echo site_url('merchants/withdrawals'); ?>"><?php echo lang('merchant_withdrawal'); ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($this->aauth->is_allowed('principle')) { ?>
                                    <li class="<?php echo menu_active($menu, 'principle'); ?>">
                                        <a href="#"><i class="icon-store"></i> <span>Principal</span></a>
                                        <ul>
                                            <li class="<?php echo menu_active($menu, 'principle_list'); ?>"><a href="<?php echo site_url('principle'); ?>">Principal</a></li>
                                            <?php if ($this->aauth->is_allowed('principle/request')) { ?>
                                                <li class="<?php echo menu_active($menu, 'principle_request'); ?>"><a href="<?php echo site_url('principle/request'); ?>">Permintaan Principal</a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($this->aauth->is_allowed('blog')) { ?>
                                    <li class="<?php echo menu_active($menu, 'blog'); ?>">
                                        <a href="#"><i class="icon-magazine"></i> <span><?php echo lang('blog'); ?></span></a>
                                        <ul>
                                            <li class="<?php echo menu_active($menu, 'blog_post'); ?>"><a href="<?php echo site_url('blogs/posts'); ?>"><?php echo lang('blog_post'); ?></a></li>
                                            <?php if ($this->aauth->is_allowed('blog/category')) { ?>
                                                <li class="<?php echo menu_active($menu, 'blog_category'); ?>"><a href="<?php echo site_url('blogs/categories'); ?>"><?php echo lang('blog_category'); ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($this->aauth->is_allowed('report')) { ?>
                                    <li class="<?php echo menu_active($menu, 'report'); ?>">
                                        <a href="#"><i class="icon-chart"></i> <span><?php echo lang('report'); ?></span></a>
                                        <ul>
                                            <?php if ($this->aauth->is_allowed('report/sales')) { ?>
                                                <li class="<?php echo menu_active($menu, 'report_sale'); ?>"><a href="<?php echo site_url('reports/sales'); ?>"><?php echo lang('report_sales'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('report/merchant')) { ?>
                                                <li class="<?php echo menu_active($menu, 'report_merchant'); ?>"><a href="<?php echo site_url('reports/merchant'); ?>"><?php echo lang('report_merchant'); ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                <?php } ?>
                                <?php if ($this->aauth->is_allowed('setting')) { ?>
                                    <li class="<?php echo menu_active($menu, 'setting'); ?>">
                                        <a href="#"><i class="icon-cog4"></i> <span><?php echo lang('setting'); ?></span></a>
                                        <ul>
                                            <li class="<?php echo menu_active($menu, 'setting_base'); ?>"><a href="<?php echo site_url('settings'); ?>"><?php echo lang('setting'); ?></a></li>
                                            <!--<li class="<?php // echo menu_active($menu, 'setting_payment_method');    
                                                            ?>"><a href="<?php // echo site_url('settings/payment_methods');    
                                                                            ?>"><?php // echo lang('payment_method');    
                                                                                ?></a></li>-->
                                            <?php if ($this->aauth->is_allowed('setting/user')) { ?>
                                                <li class="<?php echo menu_active($menu, 'setting_user'); ?>">
                                                    <a href="#"><?php echo lang('user'); ?></a>
                                                    <ul>
                                                        <li class="<?php echo menu_active($menu, 'setting_user_user'); ?>"><a href="<?php echo site_url('settings/users'); ?>"><?php echo lang('user'); ?></a></li>
                                                        <?php if ($this->aauth->is_allowed('setting/user/group')) { ?>
                                                            <li class="<?php echo menu_active($menu, 'setting_user_group'); ?>"><a href="<?php echo site_url('settings/users/groups'); ?>"><?php echo lang('user_group'); ?></a></li>
                                                        <?php } ?>
                                                        <?php if ($this->aauth->is_allowed('setting/user/permission')) { ?>
                                                            <li class="<?php echo menu_active($menu, 'setting_user_permission'); ?>"><a href="<?php echo site_url('settings/users/permissions'); ?>"><?php echo lang('user_permission'); ?></a></li>
                                                        <?php } ?>
                                                    </ul>
                                                </li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('setting/principle_user')) { ?>
                                                <li class="<?php echo menu_active($menu, 'setting_principle_user'); ?>">
                                                    <a href="#"><?php echo lang('principle_user'); ?></a>
                                                    <ul>
                                                        <li class="<?php echo menu_active($menu, 'setting_principle_user_user'); ?>"><a href="<?php echo site_url('settings/principle_users'); ?>"><?php echo lang('principle_user'); ?></a></li>
                                                        <?php if ($this->aauth->is_allowed('setting/principle_user/group')) { ?>
                                                            <li class="<?php echo menu_active($menu, 'setting_principle_user_group'); ?>"><a href="<?php echo site_url('settings/principle_users/groups'); ?>"><?php echo lang('principle_user_group'); ?></a></li>
                                                        <?php } ?>
                                                        <?php if ($this->aauth->is_allowed('setting/principle_user/permission')) { ?>
                                                            <li class="<?php echo menu_active($menu, 'setting_principle_user_permission'); ?>"><a href="<?php echo site_url('settings/principle_users/permissions'); ?>"><?php echo lang('principal_user_permission'); ?></a></li>
                                                        <?php } ?>
                                                    </ul>
                                                </li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('setting/merchant_user')) { ?>
                                                <li class="<?php echo menu_active($menu, 'setting_merchant_user'); ?>">
                                                    <a href="#"><?php echo lang('merchant_user'); ?></a>
                                                    <ul>
                                                        <li class="<?php echo menu_active($menu, 'setting_merchant_user_user'); ?>"><a href="<?php echo site_url('settings/merchant_users'); ?>"><?php echo lang('merchant_user'); ?></a></li>
                                                        <?php if ($this->aauth->is_allowed('setting/merchant_user/group')) { ?>
                                                            <li class="<?php echo menu_active($menu, 'setting_merchant_user_group'); ?>"><a href="<?php echo site_url('settings/merchant_users/groups'); ?>"><?php echo lang('merchant_user_group'); ?></a></li>
                                                        <?php } ?>
                                                        <?php if ($this->aauth->is_allowed('setting/merchant_user/permission')) { ?>
                                                            <li class="<?php echo menu_active($menu, 'setting_merchant_user_permission'); ?>"><a href="<?php echo site_url('settings/merchant_users/permissions'); ?>"><?php echo lang('merchant_user_permission'); ?></a></li>
                                                        <?php } ?>
                                                    </ul>
                                                </li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('setting/point')) { ?>
                                                <li class="<?php echo menu_active($menu, 'setting_point'); ?>"><a href="<?php echo site_url('settings/point'); ?>"><?php echo lang('point'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('setting/filemanager')) { ?>
                                                <li class="<?php echo menu_active($menu, 'setting_filemanager'); ?>"><a href="<?php echo site_url('settings/filemanager'); ?>"><?php echo lang('filemanager'); ?></a></li>
                                            <?php } ?>
                                            <?php if ($this->aauth->is_allowed('setting/seo_url')) { ?>
                                                <li class="<?php echo menu_active($menu, 'setting_seo_url'); ?>"><a href="<?php echo site_url('settings/seo_url'); ?>"><?php echo lang('seo_url'); ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
            <?php echo $output; ?>
        </div>
    </div>
    <?php
    foreach ($js as $file) {
        echo "\n    ";
        echo '<script src="' . $file . '?timestamp=' . time() . '"></script>';
    }
    echo "\n";
    ?>
    <script src="<?php echo site_url('../assets/backend/js/idle.js'); ?>"></script>
    <script>
        var awayCallback = function() {
            //console.log(new Date().toTimeString() + ": away");
            swal({
                title: 'Anda sudah idle terlalu lama!',
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#EF5350",
                confirmButtonText: 'Login',
                cancelButtonText: lang.button.cancel,
                closeOnConfirm: false
            }, function() {
                window.location = "<?php echo site_url('auth/logout'); ?>";
            });
        };

        var awayBackCallback = function() {
            //console.log(new Date().toTimeString() + ": back");
        };
        var onVisibleCallback = function() {
            //console.log(new Date().toTimeString() + ": now looking at page");
        };

        var onHiddenCallback = function() {
            //console.log(new Date().toTimeString() + ": not looking at page");
        };
        //this is one way of using it.
        /*
        var idle = new Idle();
        idle.onAway = awayCallback;
        idle.onAwayBack = awayBackCallback;
        idle.setAwayTimeout(2000);
        idle.start();
        */
        //this is another way of using it
        var idle = new Idle({
            onHidden: onHiddenCallback,
            onVisible: onVisibleCallback,
            onAway: awayCallback,
            onAwayBack: awayBackCallback,
            awayTimeout: 7200000 //away with 2 hours of inactivity
        }).start();
    </script>
</body>

</html>