<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Banners extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->aauth->control('design/banner');

        $this->lang->load('design/banners', settings('language'));
        $this->load->model('banners_model', 'banners');
        $this->data['menu'] = 'design_banner';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('design'), '/');
        $this->breadcrumbs->push(lang('banner'), '/design/banners');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('banner_heading'));
        $this->load->view('banners/list', $this->data);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->data['data'] = array();

        $this->breadcrumbs->unshift(lang('design'), '/');
        $this->breadcrumbs->push(lang('banner'), '/design/banners');

        if ($id) {
            $this->aauth->control('design/banner/edit');
            $id = decode($id);
            $this->data['data'] = $this->main->get('banners', array('id' => $id));

            $this->breadcrumbs->push(lang('banner_edit_heading'), '/design/banners/form/' . encode($id));
            $this->breadcrumbs->push($this->data['data']->name, '/');
        } else {
            $this->aauth->control('design/banner/add');
            $this->breadcrumbs->push(lang('banner_add_heading'), '/design/banners/form');
        }

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(($this->data['data']) ? lang('banner_edit_heading') : lang('banner_add_heading'));
        $this->load->view('banners/form', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->banners->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->name,
                    $data->type,
                    ($data->status == 1) ? '<i class="icon-checkmark3 text-success"></i>' : '<i class="icon-cross2 text-danger-400"></i>',
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">'.
                    ($this->aauth->is_allowed('design/banner/edit')?'<li><a href="' . site_url('design/banners/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>':'').
                    ($this->aauth->is_allowed('design/banner/delete')?'<li><a href="' . site_url('design/banners/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>':'').
                    '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->banners->count_all();
        $output['recordsFiltered'] = $this->banners->count_all($search);
        echo json_encode($output);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:banner_form_name_label', 'trim|required');
        $this->form_validation->set_rules('type', 'lang:banner_form_type_label', 'trim|required');
        $this->form_validation->set_rules('image', 'lang:banner_form_image_label', 'trim|required');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            do {
                if ($data['id'])
                    $data['id'] = decode($data['id']);

                $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;

                if ($data['id']) {
                    $this->main->update('banners', $data, array('id' => $data['id']));
                } else {
                    $id = $this->main->insert('banners', $data);
                }

                $return = array('message' => sprintf(lang('banner_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('design/banners'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('design/banner/delete');
        $id = decode($id);
        $data = $this->main->get('banners', array('id' => $id));
        $delete = $this->main->delete('banners', array('id' => $id));
        if ($delete) {
            $this->main->delete('seo_url', array('query' => 'banners/view/' . $id));
            $return = array('message' => sprintf(lang('banner_delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => sprintf(lang('banner_delete_error_message'), $data->name), 'status' => 'danger');
        }
        echo json_encode($return);
    }

}
