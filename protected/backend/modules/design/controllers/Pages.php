<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Pages extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('design/page');
        $this->lang->load('design/pages', settings('language'));
        $this->load->model('pages_model', 'pages');
        $this->data['menu'] = 'design_page';
    }

    public function index() {
        $this->breadcrumbs->unshift(lang('design'), '/');
        $this->breadcrumbs->push(lang('page'), '/design/pages');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->table();
        $this->output->set_title(lang('heading'));
        $this->load->view('pages/list', $this->data);
    }

    public function form($id = '') {
        $this->data['data'] = array();

        $this->breadcrumbs->unshift(lang('design'), '/');
        $this->breadcrumbs->push(lang('page'), '/design/pages');

        if ($id) {
            $this->aauth->control('design/page/edit');
            $id = decode($id);
            $this->data['data'] = $this->pages->get($id);
            $this->data['data']->content = tinymce_get_url_files($this->data['data']->content);
            $this->data['data']->options = json_decode($this->data['data']->options);
            $this->breadcrumbs->push(lang('edit_heading'), '/design/pages/form/' . encode($id));
            $this->breadcrumbs->push($this->data['data']->title, '/');
        } else {
            $this->aauth->control('design/page/add');
            $this->breadcrumbs->push(lang('add_heading'), '/design/pages/form');
        }

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->form();
        $this->output->set_title(($this->data['data']) ? lang('edit_heading') : lang('add_heading'));
        $this->load->view('pages/form', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->pages->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->title,
                    ($data->keyword) ? $data->keyword : '',
                    ($data->status == 1) ? '<i class="icon-checkmark3 text-success"></i>' : '<i class="icon-cross2 text-danger-400"></i>',
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                    ($this->aauth->is_allowed('design/page/edit') ? '<li><a href="' . site_url('design/pages/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>' : '') .
                    ($this->aauth->is_allowed('design/page/delete') ? '<li><a href="' . site_url('design/pages/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>' : '') .
                    '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->pages->count_all();
        $output['recordsFiltered'] = $this->pages->count_all($search);
        echo json_encode($output);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('title', 'lang:title', 'trim|required');
        $this->form_validation->set_rules('seo_url', 'lang:seo_url', 'trim|seo_url');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            $html_content = html_escape($this->input->post('content', false));
            do {
                if ($data['id'])
                    $data['id'] = decode($data['id']);

                $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;
                $data['content'] = tinymce_parse_url_files($html_content);
                if(!isset($data['options']['hidden_title'])){
                    $data['options']['hidden_title'] = 0;
                }
                $data['options'] = json_encode($data['options']);
                $url = 'pages/view/' . $data['id'];
                if ($seo_url = $data['seo_url']) {
                    if (check_url($seo_url, $url)) {
                        $return = array('message' => lang('friendly_url_exist'), 'status' => 'error');
                        break;
                    }
                } else {
                    $seo_url = $url;
                }
                unset($data['seo_url']);

                if ($data['id']) {
                    $data['user_modified'] = $this->data['user']->id;
                    $this->main->update('pages', $data, array('id' => $data['id']));
                    $this->main->delete('seo_url', array('query' => $url));
                } else {
                    $data['user_added'] = $this->data['user']->id;
                    $id = $this->main->insert('pages', $data);
                    $url .= $id;
                }

                $this->main->insert('seo_url', array('query' => $url, 'keyword' => $seo_url));
                $return = array('message' => sprintf(lang('save_success'), $data['title']), 'status' => 'success', 'redirect' => site_url('design/pages'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('design/page/delete');
        $id = decode($id);
        $data = $this->main->get('pages', array('id' => $id));
        $delete = $this->main->delete('pages', array('id' => $id));
        if ($delete) {
            $this->main->delete('seo_url', array('query' => 'pages/view/' . $id));
            $return = array('message' => sprintf(lang('delete_success'), $data->title), 'status' => 'success');
        } else {
            $return = array('message' => sprintf(lang('delete_error'), $data->title), 'status' => 'danger');
        }
        echo json_encode($return);
    }

}
