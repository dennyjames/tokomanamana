<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Orders_merchant_model extends CI_Model {

    function get($id) {
        $this->db->select('o.*, m.name customer_name, p.name shipping_province_name, c.name shipping_city_name, d.name shipping_district_name')
                ->join('merchants m', 'm.id = o.customer','left')
                ->join('provincies p', 'o.shipping_province = p.id', 'left')
                ->join('cities c', 'o.shipping_city = c.id', 'left')
                ->join('districts d', 'o.shipping_district = d.id', 'left')
                ->where('o.id', $id);
        $query = $this->db->get('orders_merchant o');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->limit($length, $start);

        return $this->db->get('orders_merchant o');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'o.code';
                break;
            case 1: $key = 'o.date_added';
                break;
            case 2: $key = 'o.shipping_name';
                break;
            case 3: $key = 'o.total';
                break;
            case 4: $key = 'o.payment_method';
                break;
            case 5: $key = 'o.payment_status';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->_where_like($search);
        return $this->db->count_all_results('orders_merchant o');
    }

    private function _where_like($search = '') {
        $columns = array('o.code', 'o.shipping_name', 'o.date_added', 'o.total', 'o.payment_method', 'o.payment_status');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    function invoices($order) {
        $this->db->select('oi.*, sos.name order_status_name, sos.color order_status_color, m.name merchant_name')
                ->join('setting_order_status sos', 'oi.order_status = sos.id', 'left')
                ->join('merchants m', 'oi.merchant = m.id', 'left')
                ->where('oi.order', $order);
        return $this->db->get('order_invoice_merchant oi');
    }

    function invoice_histories($invoice) {
        $this->db->select('oh.*, sos.name order_status_name, sos.color order_status_color')
                ->join('setting_order_status sos', 'oh.order_status = sos.id', 'left')
                ->where('oh.invoice', $invoice)
                ->order_by('oh.id', 'desc');
        return $this->db->get('order_history_merchant oh');
    }

}
