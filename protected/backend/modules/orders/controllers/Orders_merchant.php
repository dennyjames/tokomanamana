<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Orders_merchant extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->aauth->control('order');

        $this->lang->load('orders', settings('language'));
        $this->load->model('orders_merchant_model', 'orders');
        $this->data['menu'] = 'order_merchant';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();
        $this->load->js('../assets/backend/js/modules/orders.js');

        $this->breadcrumbs->push(lang('order'), '/orders');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('order_heading'));
        $this->load->view('list_merchant', $this->data);
    }

    public function view($id) {
        $this->template->_init();

        $id = decode($id);
        $this->data['order'] = $this->orders->get($id);
        $this->data['invoices'] = $this->orders->invoices($id);

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('order'), '/orders');
        $this->breadcrumbs->push($this->data['order']->code, '/orders/view/' . encode($id));
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('order_heading') . ' ' . $this->data['order']->code);
        $this->load->view('view_merchant', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->orders->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    '<a href="' . site_url('orders/orders_merchant/view/' . encode($data->id)) . '">' . $data->code . '</a>',
                    get_date_time($data->date_added),
                    $data->shipping_name,
                    number($data->total),
                    $data->payment_method,
                    $data->payment_status,
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                    ($this->aauth->is_allowed('order/confirm_payment') && ($data->payment_status == 'Confirmed' || $data->payment_status == 'Pending') ? '<li><a href="' . site_url('orders/orders_merchant/paid/' . encode($data->id)) . '" class="paid">' . lang('order_button_paid') . '</a></li>' : '') .
                    '<li><a href="' . site_url('orders/orders_merchant/view/' . encode($data->id)) . '">' . lang('button_view') . '</a></li>' .
                    ($this->aauth->is_allowed('order/delete') ? '<li><a href="' . site_url('orders/orders_merchant/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>' : '') .
                    '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->orders->count_all();
        $output['recordsFiltered'] = $this->orders->count_all($search);
        echo json_encode($output);
    }

    public function paid($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->aauth->control('order/confirm_payment');

        $id = decode($id);
        $data = $this->orders->get($id);
        $update = $this->main->update('orders_merchant', array('payment_status' => 'Paid'), array('id' => $id));
        if ($update) {
            $this->data['order'] = $data;
            $this->data['customer_merchant'] = $this->main->get('merchants', array('id' => $this->data['order']->customer));
            $this->data['customer'] = $this->main->get('merchant_users', array('id' => $this->data['customer_merchant']->auth));
            $customer = $this->data['customer'];
            $invoices = $this->main->gets('order_invoice_merchant', array('order' => $id));
            if ($invoices) {
                foreach ($invoices->result() as $invoice) {
                    if($invoice->shipping_courier == 'pickup') {
                        $due_date_inv = date('Y-m-d H:i:s', strtotime('+1 days'));
                    } else {
                        $due_date_inv = date('Y-m-d H:i:s', strtotime('+2 days'));
                    }
                    $this->main->update('order_invoice_merchant', array('order_status' => settings('order_payment_received_status'), 'due_date' => $due_date_inv), array('id' => $invoice->id));
                    $this->main->insert('order_history_merchant', array('order' => $id, 'invoice' => $invoice->id, 'order_status' => settings('order_payment_received_status')));

                    $this->data['invoice'] = $invoice;
                    $this->data['merchant'] = $this->main->get('merchants', array('id' => $invoice->merchant));
                    

                    //update stock merchant
                    // if ($this->data['merchant']->type == 'merchant' || $this->data['merchant']->type == 'branch') {
                    //     $products = $this->main->gets('order_product_merchant', array('invoice' => $invoice->id));
                    //     if ($products) {
                    //         foreach ($products->result() as $product) {
                    //             $product_detail = $this->db->query("select type, package_items from products where id = $product->product ");
                    //             $row = $product_detail->row();
                    //             if($row->type == 'p') {
                    //                 $package_items = json_decode($row->package_items, true);
                    //                 foreach ($package_items as $package) {
                    //                     $this->db->query("UPDATE product_merchant SET quantity = (quantity - $product->quantity) WHERE product = ".$package['product_id']." AND merchant = " . $this->data['merchant']->id); 
                    //                 }
                    //             } else {
                    //                 $this->db->query("UPDATE product_merchant SET quantity = (quantity - $product->quantity) WHERE product = $product->product AND merchant = " . $this->data['merchant']->id);
                    //             }
                    //         }
                    //     }
                    // }

                    $merchant_user = $this->main->get('merchant_users', array('id' => $this->data['merchant']->auth));
                    $message = $this->load->view('email/merchant/new_order', $this->data, true);
                    $cronjob = array(
                        'from' => settings('send_email_from'),
                        'from_name' => settings('store_name'),
                        'to' => $merchant_user->email,
                        'subject' => 'Pesanan baru dari ' . $customer->fullname,
                        'message' => $message
                    );
                    $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                    if ($merchant_user->verification_phone) {
                        $sms = json_decode(settings('sprint_sms'), true);
                        $sms['m'] = 'Pesanan baru dari ' . $customer->fullname . '. Segera proses sebelum ' . get_date_indo_full($invoice->due_date) . ' WIB.';
                        $sms['d'] = $merchant_user->phone;
                        $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                    }
                }
            }
            $this->data['invoices'] = $invoices;
            $message = $this->load->view('email/transaction/payment_success', $this->data, true);
            $cronjob = array(
                'from' => settings('send_email_from'),
                'from_name' => settings('store_name'),
                'to' => $customer->email,
                'subject' => 'Pembayaran Sukses #' . $data->code,
                'message' => $message
            );
            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
            if ($customer->verification_phone) {
                $sms = json_decode(settings('sprint_sms'), true);
                $sms['m'] = 'Pembayaran sebesar ' . rupiah($data->total) . ' telah berhasil. Pesanan Anda akan diteruskan ke penjual.';
                $sms['d'] = $this->data['customer']->phone;
                $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
            }
            $return = array('message' => 'Pembayaran pesanan ' . $data->code . ' berhasil dikonfirmasi.', 'status' => 'success');
        } else {
            $return = array('message' => 'Pembayaran pesanan ' . $data->code . ' gagal dikonfirmasi.', 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('order/delete');
        $id = decode($id);
        $data = $this->main->get('orders', array('id' => $id));
        $this->main->delete('orders', array('id' => $data->id));
        $this->main->delete('order_history', array('order' => $data->id));
        $this->main->delete('order_invoice', array('order' => $data->id));
        $this->main->delete('order_meta', array('order_id' => $data->id));
        $this->main->delete('order_product', array('order' => $data->id));
        $return = array('message' => 'Pesanan ' . $data->code . ' berhasil dihapus.', 'status' => 'success');
        echo json_encode($return);
    }

}
