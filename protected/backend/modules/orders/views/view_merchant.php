<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('order_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <!--<a href="<?php echo site_url('orders/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span><?php echo lang('order_add_heading'); ?></span></a>-->
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="2">Order Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo lang('order_id'); ?></td>
                                    <td><?php echo $order->code; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('order_customer'); ?></td>
                                    <td><?php echo $order->customer_name; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('order_total'); ?></td>
                                    <td style="font-size: 15px; font-weight: bold;"><?php echo rupiah($order->total); ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('order_date'); ?></td>
                                    <td><?php echo get_date_time($order->date_added); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-4">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="2"><?php echo lang('order_payment_detail'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><?php echo lang('order_payment_method'); ?></td>
                                    <td><?php echo $order->payment_method; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('order_payment_status'); ?></td>
                                    <td><?php echo $order->payment_status; ?></td>
                                </tr>
                                <tr>
                                    <td><?php echo lang('order_payment_date'); ?></td>
                                    <td><?php echo ($order->payment_date && $order->payment_date != '0000-00-00') ? get_date($order->payment_date) : ''; ?></td>
                                </tr>
                                <?php if ($order->payment_method == 'transfer' && $order->payment_to) { ?>
                                    <tr>
                                        <td>Detail Transfer</td>
                                        <td><?php
                                            $payment = json_decode($order->payment_to);
                                            if (isset($payment->to)) {
                                                echo 'Ke: ' . $payment->to->name . '<br>Jumlah: ' . rupiah($payment->amount) . '<br>Dari: ' . $payment->from . '<br>Catatan: ' . $payment->note;
                                            }
                                            ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-4">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th><?php echo lang('order_shipping_address'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <?php echo $order->shipping_name . '<br>' . $order->shipping_address . '<br>' . $order->shipping_district_name . ' - ' . $order->shipping_city_name . '<br>' . $order->shipping_province_name . '<br>' . $order->shipping_postcode; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row mt-15">
                    <div class="col-sm-12">
                        <h6 class="text-semibold">Invoice:</h6>
                        <div class="tabbable nav-tabs-vertical nav-tabs-left">
                            <ul class="nav nav-tabs nav-tabs-highlight">
                                <?php if ($invoices) foreach ($invoices->result() as $key => $invoice) { ?>
                                        <li class="<?php echo ($key == 0) ? 'active' : ''; ?>"><a href="<?php echo '#inv-' . $invoice->id; ?>" data-toggle="tab"><?php echo $invoice->code; ?></a></li>
                                    <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <?php if ($invoices) foreach ($invoices->result() as $key => $invoice) { ?>
                                        <div class="tab-pane <?php echo ($key == 0) ? 'active' : ''; ?>" id="<?php echo 'inv-' . $invoice->id; ?>">
                                            <h6 class="text-semibold">Merchant: <?php echo $invoice->merchant_name; ?></h6>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="2" class="text-center">Detail</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><?php echo lang('order_invoice_code'); ?></td>
                                                                <td><?php echo $invoice->code; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><?php echo lang('order_invoice_status'); ?></td>
                                                                <td><?php echo '<label class="label no-margin label-' . $invoice->order_status_color . '">' . $invoice->order_status_name . '</label>'; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><?php echo lang('order_invoice_subtotal'); ?></td>
                                                                <td style="font-size: 15px; font-weight: bold;"><?php echo rupiah($invoice->subtotal); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><?php echo lang('order_invoice_total'); ?></td>
                                                                <td style="font-size: 15px; font-weight: bold;"><?php echo rupiah($invoice->total); ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="col-sm-6">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="2" class="text-center"><?php echo lang('order_shipping_tabs'); ?></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr>
                                                                <td><?php echo lang('order_invoice_shipping_courier'); ?></td>
                                                                <td><?php
                                                                    $shipping = explode('-', $invoice->shipping_courier);
                                                                    echo strtoupper($shipping[0]) . (isset($shipping[1]) ? ' ' . $shipping[1] : '');
                                                                    ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><?php echo lang('order_invoice_shipping_weight'); ?></td>
                                                                <td><?php echo number($invoice->shipping_weight) . ' gram'; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><?php echo lang('order_invoice_shipping_cost'); ?></td>
                                                                <td><?php echo rupiah($invoice->shipping_cost); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td><?php echo lang('order_invoice_shipping_tracking'); ?></td>
                                                                <td><?php echo $invoice->tracking_number; ?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <table class="table table-bordered mt-15">
                                                <thead>
                                                    <tr>
                                                        <th><?php echo lang('order_product_code'); ?></th>
                                                        <th><?php echo lang('order_product_name'); ?></th>
                                                        <th><?php echo lang('order_product_weight'); ?></th>
                                                        <th><?php echo lang('order_product_quantity'); ?></th>
                                                        <th><?php echo lang('order_product_price'); ?></th>
                                                        <th><?php echo lang('order_product_subtotal'); ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if ($products = $this->main->gets('order_product_merchant', array('order' => $order->id, 'invoice' => $invoice->id))) foreach ($products->result() as $product) { ?>
                                                            <tr>
                                                                <td><?php echo $product->code; ?></td>
                                                                <td><?php echo $product->name; ?></td>
                                                                <td><?php echo number($product->weight) . ' gram'; ?></td>
                                                                <td><?php echo number($product->quantity); ?></td>
                                                                <td><?php echo rupiah($product->price); ?></td>
                                                                <td><?php echo rupiah($product->total); ?></td>
                                                            </tr>
                                                        <?php } ?>
                                                </tbody>
                                            </table>
                                            <table class="table table-bordered mt-15">
                                                <thead>
                                                    <tr>
                                                        <th><?php echo lang('order_history_time'); ?></th>
                                                        <th><?php echo lang('order_history_status'); ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php if ($histories = $this->orders->invoice_histories($invoice->id)) foreach ($histories->result() as $history) { ?>
                                                            <tr>
                                                                <td><?php echo get_date_time($history->date_added); ?></td>
                                                                <td><?php echo $history->order_status_name; ?></td>
                                                            </tr>
                                                        <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>