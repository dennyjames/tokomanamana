<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Withdrawals extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('merchant/withdrawal');
        $this->lang->load('withdrawals', settings('language'));
        $this->load->model('withdrawals_model', 'withdrawals');

        $this->data['menu'] = 'merchant_withdrawal';
    }

    public function index() {
        $this->breadcrumbs->unshift(lang('merchant'), '/');
        $this->breadcrumbs->push(lang('merchant_withdrawal'), '/merchants/withdrawals');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->table();
        $this->load->js('../assets/backend/js/modules/merchants/withdrawals.js');
        $this->output->set_title(lang('heading'));
        $this->load->view('withdrawals/list', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->withdrawals->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $to = json_decode($data->to);
                $output['data'][] = array(
                    $data->date_added,
                    $data->merchant_name,
                    rupiah($data->amount),
                    $to->name . ' ' . $to->branch . '<br>' . $to->account_name . ' ' . $to->account_number,
                    $data->status,
                    ($data->status == 'Pending') ? '<button type="button" class="btn btn-primary pay" data-id="' . encode($data->id) . '" data-bank="'.$to->name.'" data-amount="'.$data->amount.'" data-accountnumber="'.$to->account_number.'">Bayar</button>' : '',
                    ($data->status == 'Pending') ? '<button type="button" class="btn btn-primary cancel" data-id="' . encode($data->id) . '" data-bank="'.$to->name.'" data-amount="'.$data->amount.'" data-accountnumber="'.$to->account_number.'">Cancel</button>' : '',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->withdrawals->count_all();
        $output['recordsFiltered'] = $this->withdrawals->count_all($search);
        echo json_encode($output);
    }

    public function pay($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('merchant_balance_withdrawal', array('id' => $id));
        if ($data) {
            $this->main->update('merchant_balance_withdrawal', array('status' => 'Paid'), array('id' => $id));

            $merchant = $this->main->get('merchants', array('id' => $data->merchant));
            $balance = $merchant->cash_balance - $data->amount;
            $this->main->insert('merchant_balances', array('merchant' => $data->merchant, 'type' => 'out', 'amount' => $data->amount, 'balance' => $balance,'note'=>'Penarikan Dana'));
            $this->main->update('merchants', array('cash_balance' => $balance), array('id' => $merchant->id));

            $to = json_decode($data->to);
            
            $user = $this->main->get('merchant_users', array('id' => $merchant->auth));
            $message = $this->load->view('email/merchant/withdrawal', $this->data, true);
            $cronjob = array(
                'from' => settings('send_email_from'),
                'from_name' => settings('store_name'),
                'to' => $user->email,
                'subject' => 'Penarikan Dana ' . $to->name . ' - ' . $to->account_number . ' a/n ' . $to->account_name . ' telah berhasil',
                'message' => $message
            );
            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
            $return = array('message' => 'Berhasil disimpan!', 'status' => 'success');
        } else {
            $return = array('message' => 'Data tidak ditemukan!', 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function pay_cancel() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($this->input->post('id'));
        $bank = $this->input->post('bank');
        $amount = $this->input->post('amount');
        $account_number = $this->input->post('account_number');
        $data = $this->main->get('merchant_balance_withdrawal', array('id' => $id));
        if ($data) {
            $this->main->update('merchant_balance_withdrawal', array('status' => 'Cancel'), array('id' => $id));

            $merchant = $this->main->get('merchants', array('id' => $data->merchant));
            $balance = $merchant->cash_balance + $data->amount;
            $this->main->insert('merchant_balances', array('merchant' => $data->merchant, 'type' => 'in', 'amount' => $data->amount, 'balance' => $balance,'note'=>'Pembatalan Penarikan Dana'));
            $this->main->update('merchants', array('cash_balance' => $balance), array('id' => $merchant->id));

            
            $return = array('message' => 'Berhasil dicancel!', 'status' => 'success');
        } else {
            $return = array('message' => 'Data tidak ditemukan!', 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function pay_bca() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($this->input->post('id'));
        $bank = $this->input->post('bank');
        $amount = $this->input->post('amount');
        $account_number = $this->input->post('account_number');
        //$account_number = '0611112504';
        //$account_number = '';
        $this->db->select_max('trans_id');
        $result= $this->db->get('bca_payment')->row_array();
        if($result['trans_id']) {
            $next_id = $result['trans_id'] + 1;
        } else {
            $next_id = 1;
        }

        $this->load->library('Bca');
        $trans_id = sprintf("%08d", $next_id);
        $data = $this->main->get('merchant_balance_withdrawal', array('id' => $id));
        if ($data) {
            $bca_login_api = Bca::login();
            $bca_login_decode =  json_decode($bca_login_api, true);
            if($bca_login_decode['code'] == 200) {
                $bca_transfer_api = Bca::transfer($bca_login_decode['body']['access_token'],$trans_id,$amount,$account_number);
                $bca_transfer_decode =  json_decode($bca_transfer_api, true);
                $dataa = array(
                    'trans_date' => date('Y-m-d H:i:s'),
                    'amount' => $amount,
                    'dest_account_no' => $account_number,
                    'merchant_id' => $data->merchant,
                    'response' => $bca_transfer_api
                );
                $this->main->insert('bca_response', $dataa);
                // $bca_balance_api = Bca::get_balance($bca_login_decode['body']['access_token']);
                // $bca_balance_decode =  json_decode($bca_balance_api, true);
                //var_dump($bca_transfer_decode);exit();
                if($bca_transfer_decode['code'] == 200) {
                    $status = $bca_transfer_decode['body']['Status'];
                    $datax = array(
                        'trans_id' => $trans_id,
                        'trans_date' => date('Y-m-d H:i:s'),
                        'ref_id' => 'PO'.date('Y').'-'.$trans_id,
                        'amount' => $amount,
                        'dest_account_no' => $account_number,
                        'merchant_id' => $data->merchant,
                        'status' => $status,
                    );
                    $this->main->insert('bca_payment', $datax);
                    if($status == 'Success') {
                        $data = $this->main->get('merchant_balance_withdrawal', array('id' => $id));
                        if ($data) {
                            $this->main->update('merchant_balance_withdrawal', array('status' => 'Paid'), array('id' => $id));

                            $merchant = $this->main->get('merchants', array('id' => $data->merchant));
                            $balance = $merchant->cash_balance - $data->amount;
                            $this->main->insert('merchant_balances', array('merchant' => $data->merchant, 'type' => 'out', 'amount' => $data->amount, 'balance' => $balance,'note'=>'Penarikan Dana'));
                            $this->main->update('merchants', array('cash_balance' => $balance), array('id' => $merchant->id));

                            $to = json_decode($data->to);
                            
                            $user = $this->main->get('merchant_users', array('id' => $merchant->auth));
                            $message = $this->load->view('email/merchant/withdrawal', $this->data, true);
                            $cronjob = array(
                                'from' => settings('send_email_from'),
                                'from_name' => settings('store_name'),
                                'to' => $user->email,
                                'subject' => 'Penarikan Dana ' . $to->name . ' - ' . $to->account_number . ' a/n ' . $to->account_name . ' telah berhasil',
                                'message' => $message
                            );
                            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                            $return = array('message' => 'Berhasil disimpan!', 'status' => 'success');
                        } else {
                            $return = array('message' => 'Data tidak ditemukan!', 'status' => 'error');
                        }
                    } else {
                        $return = array('message' => 'Status pembayaran dihold!', 'status' => 'error');
                    }
                } else {
                    $return = array('message' => $bca_transfer_decode['body']['ErrorMessage']['Indonesian'], 'status' => 'error');
                }
            } else {
                $return = array('message' => $bca_login_decode['body']['ErrorMessage']['Indonesian'], 'status' => 'error','code' => $bca_login_decode['code']);
            }
         } else {
            $return = array('message' => 'Data tidak ditemukan!', 'status' => 'error');
        }
       
        echo json_encode($return);
    }

}
