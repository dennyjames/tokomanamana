<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Branches extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('merchant/branch');
        $this->lang->load('merchants', settings('language'));
        $this->lang->load('branches', settings('language'));
//        $this->load->model('branches_model', 'branches');

        $this->load->model('merchants_model', 'merchants');

        $this->data['menu'] = 'merchant_branch';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('merchant'), '/');
        $this->breadcrumbs->push(lang('merchant_branch'), '/merchants/branches');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('branch_heading'));
        $this->load->view('branches/list', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->merchants->get_all($start, $length, $search, $order, 'branch');
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->name,
                    $data->city,
                    '<td class="text-center">
                    <ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                    ($this->aauth->is_allowed('merchant/branch/edit') ? '<li><a href="' . site_url('merchants/branches/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>' : '') .
                    ($this->aauth->is_allowed('merchant/branch/delete') ? '<li><a href="' . site_url('merchants/branches/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>' : '') .
                    '</ul>
                    </li>
                    </ul>
                    </td>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->merchants->count_all('', 'branch');
        $output['recordsFiltered'] = $this->merchants->count_all($search, 'branch');
        echo json_encode($output);
    }

    public function form($id = '') {
        $this->load->library('googlemaps');

        $this->data['provinces'] = $this->main->gets('provincies', array(), 'name asc');
        $this->data['cities'] = array();
        $this->data['districts'] = array();
        $this->data['data'] = array();
        $config['center'] = 'auto';
        if ($id) {
            $this->aauth->control('merchant/branch/edit');
            $id = decode($id);
            $this->data['data'] = $this->merchants->get($id, 'branch');
            if ($this->data['data']->province)
                $this->data['cities'] = $this->main->gets('cities', array('province' => $this->data['data']->province), 'name asc');
            if ($this->data['data']->city)
                $this->data['districts'] = $this->main->gets('districts', array('city' => $this->data['data']->city), 'name asc');
            if ($this->data['data']->lat && $this->data['data']->lng) {
                $config['center'] = $this->data['data']->lat . ',' . $this->data['data']->lng;
            }
        }else{
            $this->aauth->control('merchant/branch/add');
        }
        
        $config['loadAsynchronously'] = TRUE;
        $config['map_height'] = '500px';
        $config['onboundschanged'] = 'if (!centreGot) {
                var mapCentre = map.getCenter();
                marker_0.setOptions({
                        position: new google.maps.LatLng(mapCentre.lat(), mapCentre.lng()) 
                });
                setLocation(mapCentre.lat(), mapCentre.lng());
            }centreGot = true;';
        $config['disableFullscreenControl'] = TRUE;
        $config['disableMapTypeControl'] = TRUE;
        $config['disableStreetViewControl'] = TRUE;
        $config['places'] = TRUE;
        $config['placesAutocompleteInputID'] = 'search-location';
        $config['placesAutocompleteBoundsMap'] = TRUE; // set results biased towards the maps viewport
        $config['placesAutocompleteOnChange'] = 'map.setCenter(this.getPlace().geometry.location); 
            marker_0.setOptions({
                        position: new google.maps.LatLng(this.getPlace().geometry.location.lat(), this.getPlace().geometry.location.lng()) 
                });
                setLocation(this.getPlace().geometry.location.lat(), this.getPlace().geometry.location.lng());';
        $this->googlemaps->initialize($config);
        $marker = array();
        $marker['draggable'] = true;
        $marker['ondragend'] = 'setLocation(event.latLng.lat(), event.latLng.lng());';
        $this->googlemaps->add_marker($marker);
        $this->data['map'] = $this->googlemaps->create_map();

        $this->breadcrumbs->push(lang('merchant'), '/merchants');
        $this->breadcrumbs->push(lang('branch'), '/merchants/branches');
        $this->breadcrumbs->push(($this->data['data']) ? lang('branch_edit_heading') : lang('branch_add_heading'), '/');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->form();
        $this->load->js('../assets/backend/js/modules/merchants/form.js');
        $this->output->set_title(($this->data['data']) ? lang('branch_edit_heading') : lang('branch_add_heading'));
        $this->load->view('branches/form', $this->data);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:branch_form_name_label', 'trim|required');
        $this->form_validation->set_rules('province', 'lang:branch_form_province_label', 'trim|required');
        $this->form_validation->set_rules('city', 'lang:branch_form_city_label', 'trim|required');
        $this->form_validation->set_rules('district', 'lang:branch_form_district_label', 'trim|required');
        $this->form_validation->set_rules('fullname', 'lang:branch_form_fullname_label', 'trim|required');
        if (!$this->input->post('id')) {
            $this->form_validation->set_rules('email', 'lang:branch_form_email_label', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'lang:branch_form_password_label', 'trim|required');
        }

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            do {
                $this->load->library('ion_auth');
                if (!$data['id']) {
                    $user = $this->ion_auth->register($data['email'], $data['password'], $data['email'],$data['phone'], array('fullname' => $data['fullname'], 'telephone' => $data['telephone']), array(2));
                    if ($user) {
                        unset($data['email']);
                        unset($data['password']);
                        unset($data['fullname']);
                        unset($data['phone']);
                        $data['auth'] = $user;
                        $data['shipping'] = json_encode(array('jne', 'tiki', 'pos'));
                        $data['type'] = 'branch';
                        $save = $this->main->insert('merchants', $data);
                    } else {
                        $return = array('message' => $this->ion_auth->errors(), 'status' => 'error');
                        break;
                    }
                } else {
                    $data['id'] = decode($data['id']);
                    $branch = $this->main->get('merchants', array('id' => $data['id']));
                    $auth = $branch->auth;
                    $data_auth = array('fullname' => $data['fullname'],'phone' => $data['phone']);
                    if (isset($data['password'])) {
                        $data_auth['password'] = $data['password'];
                        unset($data['password']);
                    }
                    $this->ion_auth->update($auth, $data_auth);
                    unset($data['fullname']);
                    unset($data['phone']);
                    $save = $this->main->update('merchants', $data, array('id' => $data['id']));
                }
                $return = array('message' => sprintf(lang('branch_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('merchants/branches'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('merchant/branch/delete');
        $id = decode($id);
        $data = $this->main->get('merchants', array('id' => $id));

        $this->main->delete('merchants', array('id' => $id));
        $this->main->delete('merchant_users', array('id' => $data->auth));
        $this->main->delete('merchant_user_to_group', array('user' => $data->auth));
        $this->main->delete('product_merchant', array('merchant' => $data->id));
        $products = $this->main->gets('products', array('merchant' => $data->id));
        if ($products) {
            foreach ($products->result() as $product) {
                $this->main->delete('product_price', array('product' => $product->id));
                $this->main->delete('product_price_level', array('product' => $product->id));
                $this->main->delete('product_feature', array('product' => $product->id));
                $this->main->delete('product_image', array('product' => $product->id));
                $reviews = $this->main->gets('product_review', array('product' => $product->id));
                if ($reviews) {
                    foreach ($reviews->result() as $review) {
                        $this->main->delete('product_review_likedislike', array('product_review' => $review->id));
                    }
                }
                $this->main->delete('product_review', array('product' => $product->id));
                $options = $this->main->gets('product_option', array('product' => $product->id));
                if ($options) {
                    foreach ($options->result() as $option) {
                        $this->main->delete('product_option_combination', array('product_option' => $option->id));
                        $this->main->delete('product_option_image', array('product_option' => $option->id));
                    }
                }
                $this->main->delete('product_option', array('product' => $product->id));
            }
        }
        $this->main->delete('products', array('merchant' => $data->id));

        $return = array('message' => sprintf(lang('branch_delete_success_message'), $data->name), 'status' => 'success');

        echo json_encode($return);
        // $delete = $this->main->delete('merchants', array('id' => $id));
        // $delete_users = $this->main->delete('merchant_users', array('id' => $data->auth));
        // if ($delete && $delete_users) {
        //     $return = array('message' => sprintf(lang('branch_delete_success_message'), $data->name), 'status' => 'success');
        // } else {
        //     $return = array('message' => lang('branch_delete_error_message'), 'status' => 'error');
        // }

        // echo json_encode($return);
    }

}
