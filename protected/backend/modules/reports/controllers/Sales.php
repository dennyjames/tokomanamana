<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Sales extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('report/sales');
        $this->lang->load('sales', settings('language'));
        $this->load->model('reports_model', 'reports');
        $this->data['menu'] = 'report_sales';
        $this->data['body_class'] = 'has-detached-left';
    }

    public function index() {
        $this->breadcrumbs->unshift(lang('report'), '/');
        $this->breadcrumbs->push(lang('report_sales'), '/reports/sales');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->template->_init();
        $this->template->form();
        $this->template->table();
        $this->load->js('../assets/backend/js/modules/reports.js');
        $this->output->set_title(lang('heading'));
        $this->load->view('sales/index', $this->data);
    }
    
    public function data() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $input = $this->input->post(null, true);
        $filter = $input['filter'];
        $output['data'] = array();
        // $datas = $this->reports->sales($input['start'], $input['length'], $input['order'][0], $filter['from'], $filter['to'],$filter['order_status'],$filter['payment_status'],$filter['shipping'],$filter['zone'],isset($filter['city'])?$filter['city']:'');
        $datas = $this->reports->sales($input['start'], $input['length'], $input['order'][0], $filter['from'], $filter['to'],$filter['order_status'],$filter['payment_status'],$filter['shipping'],isset($filter['city'])?$filter['city']:'',$filter['merchants']);
        if ($datas) {
            foreach ($datas->result() as $data) {
//                $shipping = explode('-', $data->shipping_courier);
                $output['data'][] = array(
                    '<a href="'. site_url('orders/view/'. encode($data->order)).'" target="_blank">' . $data->invoice . '</a>',
                    get_date($data->date),
                    $data->customer,
                    number($data->total),
                    $data->payment_status,
                    $data->order_status,
                    strtoupper($data->shipping_courier),
                    $data->merchant_name,
                );
            }
        }
        $output['recordsTotal'] = $this->main->count('order_invoice');
        // $output['recordsFiltered'] = $this->reports->sales_count($filter['from'], $filter['to'],$filter['order_status'],$filter['payment_status'],$filter['shipping'],$filter['zone'],isset($filter['city'])?$filter['city']:'');
        $output['recordsFiltered'] = $this->reports->sales_count($filter['from'], $filter['to'],$filter['order_status'],$filter['payment_status'],$filter['shipping'],isset($filter['city'])?$filter['city']:'',$filter['merchants']);
        echo json_encode($output);
    }

}
