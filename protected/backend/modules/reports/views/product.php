<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <h2><?php echo lang('product_heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <?php $this->load->view('sidebar'); ?>
        <div class="container-detached">
            <div class="content-detached">
                <div class="panel panel-flat">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-8">
                                <form id="filter-form" action="<?php echo site_url('reports/sales/product_data'); ?>" method="post">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <div class="input-group">
                                                        <label class="input-group-addon">Dari:</label>
                                                        <input type="text" class="form-control date" name="from" value="<?php echo date('Y-m-d'); ?>">
                                                    </div>
                                                </div>
                                                <div class="col-xs-6">
                                                    <div class="input-group">
                                                        <label class="input-group-addon">Sampai:</label>
                                                        <input type="text" class="form-control date" name="to" value="<?php echo date('Y-m-d'); ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <button class="btn btn-default" type="button" id="filter">Filter</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-flat">
                    <table class="table table-hover" id="table">
                        <thead>
                            <tr>
                                <th>Nama Produk</th>
                                <th>Jml. Transaksi</th>
                                <th>Jml. Invoice</th>
                                <th>Jml. Terjual</th>
                                <th>Total Penjualan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if ($datas->num_rows() > 0) { ?> <?php foreach ($datas->result() as $data) { ?>
                                    <tr>
                                        <td><?php echo $data->name; ?></td>
                                        <td><?php echo number($data->transaction); ?></td>
                                        <td><?php echo number($data->invoice); ?></td>
                                        <td><?php echo number($data->quantity); ?></td>
                                        <td><?php echo number($data->sales); ?></td>
                                    </tr>
                                <?php } ?>
                            <?php } else { ?>
                                <tr>
                                    <td colspan="4">Tidak ada transaksi</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>