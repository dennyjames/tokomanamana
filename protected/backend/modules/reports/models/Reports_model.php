<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_model extends CI_Model {

    function sales($start = 0, $length, $order = array(), $from, $to, $order_status = '', $payment_status = '', $shipping = '', $city = '', $merchant = '') {
        $this->db->select('o.id order, oi.id, oi.code invoice, o.code, cst.fullname customer, oi.total, oi.shipping_courier, o.date_added date, c.name city, sos.name order_status, o.payment_status, m.name AS merchant_name')
                ->join('orders o', 'o.id = oi.order', 'left')
                ->join('customers cst', 'cst.id = o.customer', 'left')
                ->join('setting_order_status sos', 'sos.id = oi.order_status', 'left')
                ->join('cities c', 'c.id = o.shipping_city', 'left')
                ->join('merchants m', 'm.id = oi.merchant', 'left')
                ->where('DATE(o.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'')
                ->limit($length, $start);
        if ($order_status)
            $this->db->where('oi.order_status', $order_status);
        if ($payment_status)
            $this->db->where('o.payment_status', $payment_status);
        if ($shipping)
            $this->db->like('oi.shipping_courier', $shipping);
        if ($merchant)
            $this->db->where('oi.merchant', $merchant);
        if ($city) {
            $city = array_filter(explode(',', $city));
            $this->db->where_in('o.shipping_city', $city);
        }
        if ($order) {
            $col = $order['column'];
            switch ($order['column']) {
                case 0: $col = 'invoice';
                    break;
                case 1: $col = 'date';
                    break;
                case 2: $col = 'customer';
                    break;
                case 3: $col = 'total';
                    break;
                case 4: $col = 'payment_status';
                    break;
                case 5: $col = 'order_process';
                    break;
                case 6: $col = 'shipping_courier';
                    break;
                case 7: $col = 'merchant_name';
                    break;
            }
            $this->db->order_by($col, $order['dir']);
        }
        return $this->db->get('order_invoice oi');
    }
    function sales_count($from, $to, $order_status = '', $payment_status = '', $shipping = '', $city = '', $merchant = '') {
        $this->db->join('orders o', 'o.id = oi.order', 'left')
                ->join('merchants m', 'm.id = oi.merchant', 'left')
                ->where('DATE(o.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'');
        if ($order_status)
            $this->db->where('oi.order_status', $order_status);
        if ($payment_status)
            $this->db->where('o.payment_status', $payment_status);
        if ($shipping)
            $this->db->like('oi.shipping_courier', $shipping);
        if ($merchant)
            $this->db->where('m.name', $merchant);
        if ($city) {
            $city = array_filter(explode(',', $city));
            $this->db->where_in('o.shipping_city', $city);
        }
        return $this->db->count_all_results('order_invoice oi');
    }

    // function sales_merchant($from, $to, $start = 0, $length, $order = array(), $merchant = '') {
    //     $this->db->select('m.id, m.name, m.type, oi.*, CASE WHEN `m`.`type` = \'merchant\' THEN `pm`.`product_merchant` ELSE `pps`.`product_branch` END `stock_product`')
    //             ->join('(SELECT 
    //                     oi.merchant,
    //                     SUM(CASE WHEN order_status IN (1,2) THEN 1 ELSE 0 END) pending_payment,
    //                     SUM(CASE WHEN order_status = 3 THEN 1 ELSE 0 END) pending_process,
    //                     SUM(CASE WHEN order_status = 4 THEN 1 ELSE 0 END) on_process,
    //                     SUM(CASE WHEN order_status IN (5, 6, 10, 11) THEN 1 ELSE 0 END) on_shipment,
    //                     SUM(CASE WHEN order_status = 7 THEN 1 ELSE 0 END) finish,
    //                     SUM(CASE WHEN order_status = 8 THEN 1 ELSE 0 END) cancel,
    //                     SUM(CASE WHEN order_status = 9 THEN 1 ELSE 0 END) reject
    //                     FROM order_invoice oi
    //                     WHERE DATE(oi.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'
    //                     GROUP BY oi.merchant) oi', 'oi.merchant = m.id', 'left')
    //             ->join('(SELECT merchant, SUM(quantity) product_merchant FROM product_merchant GROUP BY merchant) pm', 'pm.merchant = m.id', 'left')
    //             ->join('(SELECT branch_id, SUM(quantity) product_branch FROM products_principal_stock GROUP BY branch_id) pps', 'pps.branch_id = m.id', 'left')
    //             ->limit($length, $start);
    //     if($merchant){
    //         $this->db->where('m.id',$merchant);
    //     }
    //     if ($order) {
    //         $col = $order['column'];
    //         switch ($order['column']) {
    //             default:
    //                 $col = 'm.name';
    //                 break;
    //             case 1: $col = 'stock_product';
    //                 break;
    //             case 2: $col = 'pending_payment';
    //                 break;
    //             case 3: $col = 'pending_process';
    //                 break;
    //             case 4: $col = 'on_process';
    //                 break;
    //             case 5: $col = 'on_shipment';
    //                 break;
    //             case 6: $col = 'finish';
    //                 break;
    //             case 7: $col = 'cancel';
    //                 break;
    //             case 8: $col = 'reject';
    //                 break;
    //         }
    //         $this->db->order_by($col, $order['dir']);
    //     }
    //     return $this->db->get('merchants m');
    // }

    function sales_merchant($from, $to, $start = 0, $length, $order = array(), $merchant = '') {
        $this->db->select('m.id, m.name, m.type, oi.*, CASE WHEN `m`.`type` = \'merchant\' THEN `p`.`product_merchant` ELSE `pps`.`product_branch` END `stock_product`')
                ->join('(SELECT 
                        oi.merchant,
                        SUM(CASE WHEN order_status IN (1,2) THEN 1 ELSE 0 END) pending_payment,
                        SUM(CASE WHEN order_status = 3 THEN 1 ELSE 0 END) pending_process,
                        SUM(CASE WHEN order_status = 4 THEN 1 ELSE 0 END) on_process,
                        SUM(CASE WHEN order_status IN (5, 6, 10, 11) THEN 1 ELSE 0 END) on_shipment,
                        SUM(CASE WHEN order_status = 7 THEN 1 ELSE 0 END) finish,
                        SUM(CASE WHEN order_status = 8 THEN 1 ELSE 0 END) cancel,
                        SUM(CASE WHEN order_status = 9 THEN 1 ELSE 0 END) reject
                        FROM order_invoice oi
                        WHERE DATE(oi.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'
                        GROUP BY oi.merchant) oi', 'oi.merchant = m.id', 'left')
                ->join('(SELECT store_id, SUM(quantity) product_merchant FROM products WHERE store_type = \'merchant\' GROUP BY store_id) p', 'p.store_id = m.id', 'left')
                ->join('(SELECT branch_id, SUM(quantity) product_branch FROM products_principal_stock GROUP BY branch_id) pps', 'pps.branch_id = m.id', 'left')
                ->limit($length, $start);
        if($merchant){
            $this->db->where('m.id',$merchant);
        }
        if ($order) {
            $col = $order['column'];
            switch ($order['column']) {
                default:
                    $col = 'm.name';
                    break;
                case 1: $col = 'stock_product';
                    break;
                case 2: $col = 'm.type';
                    break;
                case 3: $col = 'pending_payment';
                    break;
                case 4: $col = 'pending_process';
                    break;
                case 5: $col = 'on_process';
                    break;
                case 6: $col = 'on_shipment';
                    break;
                case 7: $col = 'finish';
                    break;
                case 8: $col = 'cancel';
                    break;
                case 9: $col = 'reject';
                    break;
            }
            $this->db->order_by($col, $order['dir']);
        }
        return $this->db->get('merchants m');
    }

    function get_all_merchant(){
        return $this->db->query("(SELECT id, name FROM merchants) UNION (SELECT id, name FROM principles) ORDER BY name ASC");
    }

//
//    function monthly($year) {
//        $status = settings('order_finish_status');
//        $this->db->select('COUNT(id) invoice, SUM(oi.total) sales, MONTH(oi.date_added) month, o.total transaction')
//                ->join('(SELECT COUNT(o.id) total, MONTH(o.date_added) month FROM orders o
//                        LEFT JOIN order_invoice oi ON oi.order = o.id
//                        WHERE YEAR(o.date_added) = ' . $year . '
//                        AND oi.order_status = ' . $status . '
//                        GROUP BY MONTH(o.date_added)) o', 'o.month = MONTH(oi.date_added)', 'left')
//                ->where('oi.order_status', $status)
//                ->where('YEAR(date_added)',$year)
//                ->group_by('MONTH(date_added)')
//                ->order_by('month ASC');
//        return $this->db->get('order_invoice oi');
//    }
//
//    function product($from, $to) {
//        $status = settings('order_finish_status');
//        $this->db->select('op.name, op.code, SUM(op.quantity) quantity, SUM(op.total) sales, opi.total invoice, opo.total transaction')
//                ->join('order_invoice oi', 'oi.id = op.invoice', 'left')
//                ->join('(SELECT COUNT(invoice) total, op.product FROM order_product op
//                        LEFT JOIN order_invoice oi ON op.invoice = oi.id 
//                        WHERE oi.order_status = 7 AND DATE(oi.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'
//                        GROUP BY op.product) opi', 'opi.product = op.product', 'left')
//                ->join('(SELECT COUNT(op.order) total, op.product FROM order_product op
//                        LEFT JOIN order_invoice oi ON oi.id = op.invoice
//                        WHERE oi.order_status = ' . $status . ' AND DATE(oi.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'
//                        GROUP BY op.product) opo', 'opo.product = op.product', 'left')
//                ->where('oi.order_status', $status)
//                ->where('DATE(oi.date_added) BETWEEN \'' . $from . '\' AND \'' . $to . '\'')
//                ->group_by('op.product');
//        return $this->db->get('order_product op');
//    }
}
