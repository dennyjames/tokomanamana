<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['heading'] = 'Laporan Merchant';

$lang['name'] = 'Nama Merchant';
$lang['zone'] = 'Zona';
$lang['stock'] = 'Stok';
$lang['product_tanaka'] = 'Produk Tanaka';
$lang['product_other'] = 'Produk Lain';
$lang['transaction'] = 'Transaksi';
$lang['pending_payment'] = 'Belum Dibayar';
$lang['pending_process'] = 'Belum Diproses';
$lang['on_process'] = 'Sedang Diproses';
$lang['on_shipment'] = 'Dalam Pengiriman';
$lang['finish'] = 'Selesai';
$lang['cancel'] = 'Batal';