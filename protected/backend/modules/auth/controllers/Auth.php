<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function login() {
        if ($this->input->is_ajax_request()) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('identity', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('otp_code', 'Kode Verifikasi', 'required');
            if ($this->form_validation->run() == true) {
                $data = $this->input->post(null, true);
                $remember = (bool) $this->input->post('remember');
                do {
                    $return = ['status' => 'error', 'message' => 'Login gagal, silahkan coba lagi'];
                    $merchant = $this->main->get('aauth_users', array('email' => $data['identity']));
                    if (!$merchant) {
                        break;
                    }
                    if ($merchant->verification_code != $data['otp_code']) {
                        break;
                    }
                    if ($this->aauth->login($this->input->post('identity'), $this->input->post('password'), $remember)) {
                        $return = array('message' => '', 'status' => 'success', 'redirect' => site_url($this->input->post('back')));
                    } else {
                        $return = array('message' => $this->aauth->print_errors(), 'status' => 'error');
                    }
                } while (0);
            } else {
                $return = array('message' => validation_errors(), 'status' => 'error');
            }
            echo json_encode($return);
        } else {
            $this->output->set_title('Login Admin Application');
            $this->template->_init('auth');
            $this->template->form();
            $this->load->js(site_url().'../assets/backend/js/modules/auth/login.js');
        }
    }
    
    public function logout() {
        $this->aauth->logout();
        redirect('auth/login', 'refresh');
    }

    public function forgot_password() {
        if ($this->aauth->logged_in())
            redirect();

        if ($this->input->is_ajax_request()) {
            $this->form_validation->set_rules('email', 'Alamat Email', 'required|valid_email');
            if ($this->form_validation->run() == true) {
                $forgotten = $this->aauth->forgotten_password($this->input->post('email'));
                if ($forgotten) {
                    $return = array('message' => 'Permintaan reset password telah diterima. Silahkan cek email Anda untuk langkah selanjutnya.', 'status' => 'success');
                } else {
                    $return = array('message' => 'Email yang Anda masukkan tidak terdaftar.', 'status' => 'error');
                }
            } else {
                $return = array('message' => validation_errors(), 'status' => 'error');
            }
            echo json_encode($return);
        } else {
            $this->template->_auth();
            $this->load->js('assets/js/modules/auth/forgot_password.min.js');

            $this->output->set_title('Lupa Password Bogatoko');
            $this->load->view('forgot_password');
        }
    }

    public function reset_password($code) {
        if ($this->aauth->logged_in())
            redirect();

        $reset = $this->aauth->forgotten_password_complete($code);
        $this->template->_auth();

        $this->output->set_title('Reset Password Bogatoko');
        if ($reset) {
            $this->load->view('forgot_password_complete');
        } else {
            $this->load->view('forgot_password_failed');
        }
    }

    public function request_otp_login() {
        $email = $this->input->post('email');
        $return = ['status' => 'error'];
        if ($admin = $this->main->get('aauth_users', array('email' => $email))) {
            if($admin->phone == ''){
                $return['msg'] = 'No HP kosong / tidak valid';
            } else {
                $this->load->library('sprint');
                $sms = json_decode(settings('sprint_sms'), true);
                $code = rand(1000, 9999);
                $sms['m'] = 'Hai, berikut kode login TokoManaMana Anda ' . $code;
                $sms['d'] = $admin->phone;
                $url = $sms['url'];
                unset($sms['url']);
                //$sprint_response = $this->sprint->sms($url, $sms);
                send_mail('noreply@tokomanamana.com','Tokomanamana',$email, 'Kode verifikasi login admin', $sms['m']);
                $this->main->update('aauth_users', array('verification_code' => $code, 'verification_sent_time' => date('Y-m-d H:i:s')), array('id' => $admin->id));
                $return['msg'] = 'Kode OTP berhasil dikirim ke email '.$email;
                $return['status'] = 'success';
            }
        }
        echo json_encode($return);
    }
}
