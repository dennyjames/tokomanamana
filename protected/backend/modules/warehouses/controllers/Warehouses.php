<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Warehouses extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('warehouse');
        $this->lang->load('warehouses', settings('language'));
        $this->load->model('warehouses_model', 'warehouses');

        $this->data['menu'] = 'warehouse_list';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('warehouse'), '/');
        $this->breadcrumbs->push(lang('warehouse'), '/warehouses');
        
        $this->data['url_ajax'] = site_url('warehouses/get_list');
        
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->output->set_title(lang('heading'));
        $this->load->view('list', $this->data);
    }

    public function view($id = '') {
        if ($id) {
            $this->aauth->control('warehouse/view');
            $id = decode($id);
            $this->breadcrumbs->push(lang('warehouse'), '/warehouses');
            $this->breadcrumbs->push(lang('edit_heading'), '/');

            $this->data['breadcrumbs'] = $this->breadcrumbs->show();

            $this->template->_init();
            $this->template->form();
            $this->load->css('../assets/backend/css/seatLayout.css');
            $this->load->js('../assets/backend/js/modules/warehouses/view.js');
            $this->load->js('../assets/backend/js/seatLayout.min.js');
            $this->output->set_title(lang('edit_heading'));
            $this->load->view('view', $this->data);
        }
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        
        $datas = $this->warehouses->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->code,
                    $data->date_added,
                    $data->principle_name,
                    $data->name,
                    lang('status_' . $data->status),
                    '<td class="text-center">
                    <ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                    ($this->aauth->is_allowed('warehouse/payment_confirm') ? '<li><a href="' . site_url('warehouses/payment_confirm/' . encode($data->id)) . '"> Konfirmasi Pembayaran </a></li>' : '') .
                    ($this->aauth->is_allowed('warehouse/edit') ? '<li><a href="' . site_url('warehouses/view/' . encode($data->id)) . '">Lihat</a></li>' : '') .
                    '</ul>
                    </li>
                    </ul>
                    </td>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->warehouses->count_all();
        $output['recordsFiltered'] = $this->warehouses->count_all($search);
        echo json_encode($output);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:name', 'trim|required');
        $this->form_validation->set_rules('province', 'lang:province', 'trim|required');
        $this->form_validation->set_rules('city', 'lang:city', 'trim|required');
        $this->form_validation->set_rules('district', 'lang:district', 'trim|required');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            do {
                if ($data['id'])
                    $data['id'] = decode($data['id']);

                $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;

                if ($data['id']) {
                    $this->main->update('warehouse', $data, array('id' => $data['id']));
                } else {
                    $id = $this->main->insert('warehouse', $data);
                }

                $return = array('message' => sprintf(lang('warehouse_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('warehouses'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('merchant/delete');
        $id = decode($id);
        $data = $this->main->get('merchants', array('id' => $id));
        $this->main->delete('merchants', array('id' => $id));
        $this->main->delete('merchant_users', array('id' => $data->auth));
        $this->main->delete('merchant_user_to_group', array('user' => $data->auth));
        $this->main->delete('product_merchant', array('merchant' => $data->id));
        $products = $this->main->gets('products', array('merchant' => $data->id));
        if ($products) {
            foreach ($products->result() as $product) {
                $this->main->delete('product_price', array('product' => $product->id));
                $this->main->delete('product_price_level', array('product' => $product->id));
                $this->main->delete('product_feature', array('product' => $product->id));
                $this->main->delete('product_image', array('product' => $product->id));
                $reviews = $this->main->gets('product_review', array('product' => $product->id));
                if ($reviews) {
                    foreach ($reviews->result() as $review) {
                        $this->main->delete('product_review_likedislike', array('product_review' => $review->id));
                    }
                }
                $this->main->delete('product_review', array('product' => $product->id));
                $options = $this->main->gets('product_option', array('product' => $product->id));
                if ($options) {
                    foreach ($options->result() as $option) {
                        $this->main->delete('product_option_combination', array('product_option' => $option->id));
                        $this->main->delete('product_option_image', array('product_option' => $option->id));
                    }
                }
                $this->main->delete('product_option', array('product' => $product->id));
            }
        }
        $this->main->delete('products', array('merchant' => $data->id));

        $return = array('message' => sprintf(lang('delete_success_message'), $data->name), 'status' => 'success');

        echo json_encode($return);
    }

    public function check_username($str) {
        if ($this->main->get('merchants', array('username' => $str)) && $this->main->get('seo_url', array('keyword' => $str))) {
            $this->form_validation->set_message('check_username', sprintf(lang('username_exist_message'), $str));
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function get_cities() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $output = '<option value=""></option>';
        if ($province = $this->input->post('id')) {
            $cities = $this->main->gets('cities', array('province' => $province), 'name asc');
            if ($cities)
                foreach ($cities->result() as $city) {
                    $output .= '<option value="' . $city->id . '">' . $city->name . '</option>';
                }
        }
        echo $output;
    }

    public function get_districts() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $output = '<option value=""></option>';
        if ($city = $this->input->post('id')) {
            $districts = $this->main->gets('districts', array('city' => $city), 'name asc');
            if ($districts)
                foreach ($districts->result() as $district) {
                    $output .= '<option value="' . $district->id . '">' . $district->name . '</option>';
                }
        }
        echo $output;
    }

}
