<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['heading'] = 'Gudang';
$lang['sub_heading'] = 'Kelola Data Gudang';
$lang['list_heading'] = 'Daftar Gudang';
$lang['add_heading'] = 'Tambah Gudang';
$lang['edit_heading'] = 'Edit Gudang';

$lang['general_tabs'] = 'Umum';
$lang['data_tabs'] = 'Data';
$lang['address_tabs'] = 'Alamat';
$lang['image_tabs'] = 'Gambar';
$lang['owner_tabs'] = 'Pemilik';

$lang['name'] = 'Nama';
$lang['price'] = 'Harga';
$lang['size'] = 'Ukuran';
$lang['description'] = 'Deskripsi';
$lang['province'] = 'Provinsi';
$lang['city'] = 'Kota';
$lang['district'] = 'Kecamatan';
$lang['address'] = 'Alamat';
$lang['postcode'] = 'Kode Pos';
$lang['status'] = 'Status';
$lang['password'] = 'Password';
$lang['vehicle'] = 'Kendaraan';
$lang['maps'] = 'Lokasi Maps';
$lang['status_0'] = '<span class="label label-danger">Tidak aktif</span>';
$lang['status_1'] = '<span class="label label-success">Aktif</span>';

$lang['name_placeholder'] = 'Masukkan nama gudang';
$lang['price_placeholder'] = 'Masukkan harga gudang / m3';
$lang['size_placeholder'] = 'Masukkan ukuran gudang dalam m3';

$lang['warehouse_save_success_message'] = "Gudang '%s' berhasil disimpan.";
$lang['warehouse_save_error_message'] = "Gudang '%s' gagal disimpan.";
$lang['warehouse_delete_success_message'] = "Gudang '%s' berhasil dihapus.";
$lang['warehouse_delete_error_message'] = "Gudang '%s' gagal dihapus.";