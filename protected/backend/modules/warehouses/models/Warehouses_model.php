<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouses_model extends CI_Model {

    function get($id) {
        $this->db->select('w.*')
        ->join('cities c', 'c.id = w.city', 'left')
        ->where('w.id', $id);
        $query = $this->db->get('warehouse w');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('m.name,p.name as principle_name,m.warehouse_price as price,wr.*')
                ->join('merchants m', 'm.id = wr.warehouse_id', 'left')
                ->join('principles p', 'p.id = wr.principle_id', 'left')
                ->limit($length, $start); 

        return $this->db->get('warehouse_rent wr');
    }

    function get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'm.name';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->where_like($search);
        $this->db->join('merchants m', 'm.id = wr.warehouse_id', 'left');
        return $this->db->count_all_results('warehouse_rent wr');
    }

    function where_like($search = '') {
        $columns = array('w.name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

    function get_admins() {
        $this->ion_auth_model->_ion_select = array('auth_users.id, fullname');
        $this->ion_auth_model->_ion_where = array('auth_users.id NOT IN (SELECT auth FROM merchants)');
        return $this->ion_auth->users(array(3));
    }

}
