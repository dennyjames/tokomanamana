<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table" id="table" data-url="<?php echo $url_ajax; ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="asc">Code</th>
                        <th>Tanggal</th>
                        <th>Principal</th>
                        <th>Gudang</th>
                        <th><?php echo lang('status'); ?></th>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>