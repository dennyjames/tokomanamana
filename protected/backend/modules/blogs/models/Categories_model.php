<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categories_model extends CI_Model {

    function get($id) {
        $this->db->select('c.*, keyword seo_url')
                ->join('seo_url su', "su.query = CONCAT('blogs/categories/view/',c.id)",'left')
                ->where('c.id', $id);
        $query = $this->db->get('blog_categories c');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('c.*, keyword')
                ->join('seo_url su', "su.query = CONCAT('blogs/categories/view/',c.id)",'left')
                ->limit($length, $start);

        return $this->db->get('blog_categories c');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'name';
                break;
            case 1: $key = 'keyword';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->_where_like($search);
        $this->db->join('seo_url su', "su.query = CONCAT('blogs/categories/view/',c.id)",'left');
        return $this->db->count_all_results('blog_categories c');
    }

    private function _where_like($search = '') {
        $columns = array('name', 'keyword');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

}
