<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Posts_model extends CI_Model {

    function get($id) {
        $this->db->select('p.*, keyword seo_url')
                ->join('blog_categories c','c.id = p.category','left')
                ->join('seo_url su', "su.query = CONCAT('blogs/posts/view/',p.id)",'left')
                ->where('p.id', $id);
        $query = $this->db->get('blogs p');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('p.id, p.title, p.status, c.name category, keyword')
                ->join('blog_categories c','c.id = p.category','left')
                ->join('seo_url su', "su.query = CONCAT('blogs/posts/view/',p.id)",'left')
                ->limit($length, $start);

        return $this->db->get('blogs p');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'title';
                break;
            case 1: $key = 'c.name';
                break;
            case 2: $key = 'keyword';
                break;
            case 3: $key = 'status';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->_where_like($search);
        $this->db->join('blog_categories c','c.id = p.category','left')->join('seo_url su', "su.query = CONCAT('blogs/posts/view/',p.id)",'left');
        return $this->db->count_all_results('blogs p');
    }

    private function _where_like($search = '') {
        $columns = array('title', 'keyword','c.name','status');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

}
