<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['category_heading'] = 'Kategori Blog';
$lang['category_list_heading'] = 'Daftar Kategori Blog';
$lang['category_add_heading'] = 'Tambah Kategori Blog';
$lang['category_edit_heading'] = 'Edit Kategori Blog';

$lang['category_name_th'] = 'Nama';
$lang['category_seo_url_th'] = 'Friendly URL';

$lang['category_form_name_label'] = 'Nama';
$lang['category_form_seo_url_label'] = 'Friendly URL';

$lang['category_save_success_message'] = "Kategori Blog '%s' berhasil disimpan.";
$lang['category_save_error_message'] = "Kategori Blog '%s' gagal disimpan.";
$lang['category_delete_success_message'] = "Kategori Blog '%s' berhasil dihapus.";
$lang['category_delete_error_message'] = "Kategori Blog '%s' gagal dihapus.";
$lang['category_friendly_url_exist_message'] = "Friendly URL sudah digunakan.";