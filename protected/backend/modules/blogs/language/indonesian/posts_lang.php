<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['post_heading'] = 'Blog Post';
$lang['post_list_heading'] = 'Daftar Blog Post';
$lang['post_add_heading'] = 'Tambah Blog Post';
$lang['post_edit_heading'] = 'Edit Blog Post';

$lang['post_title_th'] = 'Judul';
$lang['post_category_th'] = 'Kategori';
$lang['post_seo_url_th'] = 'URL';
$lang['post_status_th'] = 'Tampilkan';

$lang['post_form_general_tabs'] = 'Umum';
$lang['post_form_seo_tabs'] = 'SEO';

$lang['post_form_title_label'] = 'Judul';
$lang['post_form_content_label'] = 'Konten';
$lang['post_form_category_label'] = 'Kategori';
$lang['post_form_seo_url_label'] = 'Friendly URL';
$lang['post_form_status_label'] = 'Tampilkan';
$lang['post_form_meta_title_label'] = 'Meta Judul';
$lang['post_form_meta_description_label'] = 'Meta Deskripsi';
$lang['post_form_meta_keyword_label'] = 'Meta Kata Kunci';

$lang['post_save_success_message'] = "Blog Post '%s' berhasil disimpan.";
$lang['post_save_error_message'] = "Blog Post '%s' gagal disimpan.";
$lang['post_delete_success_message'] = "Blog Post '%s' berhasil dihapus.";
$lang['post_delete_error_message'] = "Blog Post '%s' gagal dihapus.";
$lang['post_friendly_url_exist_message'] = "Friendly URL sudah digunakan.";