<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Posts extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->aauth->control('blog');

        $this->lang->load('blogs/posts', settings('language'));
        $this->load->model('posts_model', 'posts');
        $this->data['menu'] = 'blog_post';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('blog'), '/');
        $this->breadcrumbs->push(lang('blog_post'), '/blogs/posts');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('post_heading'));
        $this->load->view('posts/list', $this->data);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->data['data'] = array();

        $this->breadcrumbs->unshift(lang('blog'), '/');
        $this->breadcrumbs->push(lang('post'), '/blogs/posts');

        if ($id) {
            $this->aauth->control('blog/edit');
            $id = decode($id);
            $this->data['data'] = $this->posts->get($id);
            $this->data['data']->content = tinymce_get_url_files($this->data['data']->content);

            $this->breadcrumbs->push(lang('post_edit_heading'), '/blogs/posts/form/' . encode($id));
            $this->breadcrumbs->push($this->data['data']->title, '/');
        } else {
            $this->aauth->control('blog/add');
            $this->breadcrumbs->push(lang('post_add_heading'), '/blogs/posts/form');
        }
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->data['categories'] = $this->main->gets('blog_categories', array(), 'name asc');

        $this->output->set_title(($this->data['data']) ? lang('post_edit_heading') : lang('post_add_heading'));
        $this->load->view('posts/form', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->posts->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->title,
                    $data->category,
                    ($data->keyword) ? $data->keyword : '',
                    ($data->status == 1) ? '<i class="icon-checkmark3 text-success"></i>' : '<i class="icon-cross2 text-danger-400"></i>',
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                    ($this->aauth->is_allowed('blog/edit') ? '<li><a href="' . site_url('blogs/posts/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>' : '') .
                    ($this->aauth->is_allowed('blog/delete') ? '<li><a href="' . site_url('blogs/posts/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>' : '') .
                    '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->posts->count_all();
        $output['recordsFiltered'] = $this->posts->count_all($search);
        echo json_encode($output);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('title', 'lang:post_form_title_label', 'trim|required');
        $this->form_validation->set_rules('seo_url', 'lang:post_form_seo_url_label', 'trim|seo_url');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            $html_content = html_escape($this->input->post('content', false));
            do {
                if ($data['id'])
                    $data['id'] = decode($data['id']);

                $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;
                $data['content'] = tinymce_parse_url_files($html_content);

                $url = 'blogs/posts/view/' . $data['id'];
                if ($seo_url = $data['seo_url']) {
                    if (check_url($seo_url, $url)) {
                        $return = array('message' => lang('post_friendly_url_exist_message'), 'status' => 'error');
                        break;
                    }
                } else {
                    $seo_url = $url;
                }
                unset($data['seo_url']);

                if ($data['id']) {
                    $this->main->update('blogs', $data, array('id' => $data['id']));
                    $this->main->delete('seo_url', array('query' => $url));
                } else {
                    $id = $this->main->insert('blogs', $data);
                    $url .= $id;
                }

                $this->main->insert('seo_url', array('query' => $url, 'keyword' => $seo_url));

                $return = array('message' => sprintf(lang('post_save_success_message'), $data['title']), 'status' => 'success', 'redirect' => site_url('blogs/posts'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->aauth->control('blog/delete');

        $id = decode($id);
        $data = $this->main->get('blogs', array('id' => $id));
        $delete = $this->main->delete('blogs', array('id' => $id));
        if ($delete) {
            $this->main->delete('seo_url', array('query' => 'blogs/posts/view/' . $id));
            $return = array('message' => sprintf(lang('post_delete_success_message'), $data->title), 'status' => 'success');
        } else {
            $return = array('message' => sprintf(lang('post_delete_error_message'), $data->title), 'status' => 'danger');
        }
        echo json_encode($return);
    }

}
