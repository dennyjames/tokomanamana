<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Categories extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->aauth->control('blog/category');
        
        $this->lang->load('blogs/categories', settings('language'));
        $this->load->model('categories_model', 'categories');
        $this->data['menu'] = 'blog_category';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('blog'), '/');
        $this->breadcrumbs->push(lang('blog_category'), '/blogs/categories');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('category_heading'));
        $this->load->view('categories/list', $this->data);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->data['data'] = array();

        $this->breadcrumbs->unshift(lang('blogs'), '/');
        $this->breadcrumbs->push(lang('category'), '/blogs/categories');

        if ($id) {
            $this->aauth->control('blog/category/edit');
            $id = decode($id);
            $this->data['data'] = $this->categories->get($id);

            $this->breadcrumbs->push(lang('category_edit_heading'), '/blogs/categories/form/' . encode($id));
            $this->breadcrumbs->push($this->data['data']->name, '/');
        } else {
            $this->aauth->control('blog/category/add');
            $this->breadcrumbs->push(lang('category_add_heading'), '/blogs/categories/form');
        }

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(($this->data['data']) ? lang('category_edit_heading') : lang('category_add_heading'));
        $this->load->view('categories/form', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->categories->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->name,
                    ($data->keyword) ? $data->keyword : '',
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">'.
                    ($this->aauth->is_allowed('blog/category/edit')?'<li><a href="' . site_url('blogs/categories/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>':'').
                    ($this->aauth->is_allowed('blog/category/delete')?'<li><a href="' . site_url('blogs/categories/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>':'').
                    '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->categories->count_all();
        $output['recordsFiltered'] = $this->categories->count_all($search);
        echo json_encode($output);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:category_form_name_label', 'trim|required');
        $this->form_validation->set_rules('seo_url', 'lang:category_form_seo_url_label', 'trim|seo_url');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            do {
                if ($data['id'])
                    $data['id'] = decode($data['id']);

                $url = 'blogs/categories/view/' . $data['id'];
                if ($seo_url = $data['seo_url']) {
                    if (check_url($seo_url, $url)) {
                        $return = array('message' => lang('category_friendly_url_exist_message'), 'status' => 'error');
                        break;
                    }
                } else {
                    $seo_url = $url;
                }
                unset($data['seo_url']);

                if ($data['id']) {
                    $this->main->update('blog_categories', $data, array('id' => $data['id']));
                    $this->main->delete('seo_url', array('query' => $url));
                } else {
                    $id = $this->main->insert('blog_categories', $data);
                    $url .= $id;
                }

                $this->main->insert('seo_url', array('query' => $url, 'keyword' => $seo_url));

                $return = array('message' => sprintf(lang('category_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('blogs/categories'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('blog/category/delete');
        $id = decode($id);
        $data = $this->main->get('blog_categories', array('id' => $id));
        $delete = $this->main->delete('blog_categories', array('id' => $id));
        if ($delete) {
            $this->main->delete('seo_url', array('query' => 'blogs/categories/view/' . $id));
            $return = array('message' => sprintf(lang('category_delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => sprintf(lang('category_delete_error_message'), $data->name), 'status' => 'danger');
        }
        echo json_encode($return);
    }

}
