<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('feature_variant_edit_heading') : lang('feature_variant_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('catalog/features'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('feature_list_heading'); ?></span></a>
                    <?php if ($feature) { ?>
                        <a href="<?php echo site_url('catalog/features/variants/' . encode($feature->id)); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('feature_variant_list_heading'); ?></span></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('catalog/features/variant_save'); ?>" method="post" class="form-horizontal" id="form">
                    <input type="hidden" name="id" value="<?php echo set_value('id', ($data) ? encode($data->id) : ''); ?>">
                    <!--<input type="hidden" id="submit" name="submit" value="list">-->
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('feature_variant_form_feature_label'); ?></label>
                                <div class="col-md-9">
                                    <select class="form-control" name="feature" required="" id="feature">
                                        <?php
                                        if ($features) {
                                            foreach ($features->result() as $f) {
                                                $selected = '';
                                                if ($data) {
                                                    if ($f->id == $data->feature)
                                                        $selected = 'selected';
                                                }elseif ($feature) {
                                                    if ($feature->id == $f->id)
                                                        $selected = 'selected';
                                                }
                                                echo '<option value="' . $f->id . '" ' . $selected . '>' . $f->name . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="value"><?php echo lang('feature_variant_form_value_label'); ?></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" required="" id="value" name="value" placeholder="<?php echo lang('feature_variant_form_value_placeholder'); ?>" value="<?php echo ($data) ? $data->value : ''; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('catalog/features'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary" name="submit_new" value="1"><?php echo lang('button_save_new'); ?></button>
                                    <button type="submit" class="btn btn-primary" ><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var rules_form = {
        feature: {
            required: true
        },
        value: {
            required: true
        },
    };
</script>