<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('review_edit_heading') : lang('review_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('catalog/reviews'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('review_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('catalog/reviews/save'); ?>" class="form-horizontal" method="post" id="form">
                    <input type="hidden" name="id" value="<?php echo ($data) ? encode($data->id) : ''; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#general" data-toggle="tab"><?php echo lang('review_form_general_tabs'); ?></a></li>
                                    <li><a href="#data" data-toggle="tab"><?php echo lang('review_form_data_tabs'); ?></a></li>
                                    <li><a href="#image" data-toggle="tab"><?php echo lang('review_form_image_tabs'); ?></a></li>
                                    <li class=""><a href="#feature" data-toggle="tab"><?php echo lang('review_form_feature_tabs'); ?></a></li>
                                    <li class=""><a href="#seo" data-toggle="tab"><?php echo lang('review_form_seo_tabs'); ?></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('review_form_name_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control"  name="name" placeholder="<?php echo lang('review_form_name_placeholder'); ?>" value="<?php echo ($data) ? $data->name : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('review_form_short_description_label'); ?></label>
                                            <div class="col-md-9">
                                                <textarea cols="30" rows="2" class="form-control" id="short_description" name="short_description" placeholder="<?php echo lang('review_form_short_description_placeholder'); ?>"><?php echo ($data) ? $data->short_description : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('review_form_description_label'); ?></label>
                                            <div class="col-md-9">
                                                <textarea cols="30" rows="2" class="form-control tinymce" id="description" name="description" placeholder="<?php echo lang('review_form_description_placeholder'); ?>"><?php echo ($data) ? $data->description : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('review_form_status_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="checkbox" name="status" value="1" data-on-text="<?php echo lang('enabled'); ?>" data-off-text="<?php echo lang('disabled'); ?>" class="switch" <?php echo ($data) ? (($data->status == 1) ? 'checked' : '') : 'checked'; ?>>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane " id="data">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('review_form_code_label'); ?></label>
                                            <div class="col-md-5">
                                                <input type="text" name="code" id="code"  class="form-control" value="<?php echo ($data) ? $data->code : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('review_form_category_label'); ?></label>
                                            <div class="col-md-9">
                                                <select class="bootstrap-select"  name="category" id="category" data-live-search="true" data-width="100%">
                                                    <option value=""></option>
                                                    <?php if ($categories) { ?>
                                                        <?php foreach ($categories->result() as $category) { ?>
                                                            <option value="<?php echo $category->id; ?>" <?php echo ($data) ? ($data->category == $category->id) ? 'selected' : '' : ''; ?>><?php echo $category->name; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('review_form_brand_label'); ?></label>
                                            <div class="col-md-6">
                                                <select class="bootstrap-select" name="brand" id="brand" data-live-search="true" data-width="100%">
                                                    <option value=""></option>
                                                    <?php if ($brands) { ?>
                                                        <?php foreach ($brands->result() as $brand) { ?>
                                                            <option value="<?php echo $brand->id; ?>" <?php echo ($data) ? ($data->brand == $brand->id) ? 'selected' : '' : ''; ?>><?php echo $brand->name; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('review_form_price_label'); ?></label>
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <span class="input-group-addon">Rp</span>
                                                    <input type="text" id="price" name="price" class="form-control number" value="<?php echo ($data) ? $data->price : 0; ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('review_form_weight_label'); ?></label>
                                            <div class="col-md-2">
                                                <div class="input-group">
                                                    <input type="number" name="weight" class="form-control" value="<?php echo ($data) ? $data->weight : 100; ?>">
                                                    <span class="input-group-addon">gram</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane " id="image">
                                        <div class="row" id="images">
                                            <?php
                                            if ($data) {
                                                if ($data_images) {
                                                    foreach ($data_images->result() as $image) {
                                                        echo '<div class="col-lg-3 col-sm-4" id="' . $image->id . '"><div class="thumbnail"><div class="thumb"><img src="' . site_url('../files/images/' . $image->image) . '">'
                                                        . '<input type="hidden" name="images[' . $image->id . ']" value="' . $image->image . '">'
                                                        . '</div>'
                                                        . '<div class="row">'
                                                        . '<div class="col-md-8">'
                                                        . '<div class="radio"><label><input type="radio" value="' . $image->id . '" ' . (($image->primary) ? 'checked' : '') . ' name="image_primary" id="image_primary' . $image->id . '">Gambar Utama</label></div>'
                                                        . '</div>'
                                                        . '<div class="col-md-4">'
                                                        . '<div class="radio"><a href="javascript:delete_image(\'' . $image->id . '\')" class="text-muted pull-right"><i class="icon-trash"></i></a></div>'
                                                        . '</div>'
                                                        . '</div>'
                                                        . '</div>'
                                                        . '</div>';
                                                    }
                                                }
                                            }
                                            ?>
                                            <div class="col-md-3 col-sm-4" id="add-image">
                                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#filemanager"><i class="icon-plus3 position-left"></i> <?php echo lang('review_form_add_image_button'); ?></button>
                                                <input type="hidden" id="add-image-value">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="feature">
                                        <div id="feature-form">
                                            <?php echo ($data) ? $this->load->view('feature_form') : lang('review_form_feature_hidden_info'); ?>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="seo">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('review_form_seo_url_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="seo_url" name="seo_url" value="<?php echo ($data) ? $data->seo_url : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('review_form_meta_title_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="meta_title" placeholder="<?php echo lang('review_form_meta_title_placeholder'); ?>" value="<?php echo ($data) ? $data->meta_title : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('review_form_meta_description_label'); ?></label>
                                            <div class="col-md-9">
                                                <textarea cols="30" rows="2" class="form-control" name="meta_description" placeholder="<?php echo lang('review_form_meta_description_placeholder'); ?>"><?php echo ($data) ? $data->meta_description : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('review_form_meta_keyword_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="meta_keyword" class="tags-input" value="<?php echo ($data) ? $data->meta_keyword : ''; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('catalog/reviews'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="filemanager" class="modal">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">File Manager</h5>
            </div>

            <div class="modal-body">
                <iframe  width="100%" height="550" frameborder="0" src="<?php echo site_url('filemanager/dialog.php?type=1&editor=false&field_id=add-image-value&relative_url=1'); ?>"></iframe>
            </div>
        </div>
    </div>
</div>