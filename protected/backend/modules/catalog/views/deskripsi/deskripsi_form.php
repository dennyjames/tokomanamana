<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('deskripsi_edit_heading') : lang('deskripsi_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('catalog/deskripsi'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('deskripsi_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('catalog/deskripsi/save'); ?>" method="post" class="form-horizontal" id="form">
                    <input type="hidden" name="id_deskripsi" value="<?php echo set_value('id_deskripsi', ($data) ? encode($data->id_deskripsi) : ''); ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="name"><?php echo lang('deskripsi_form_name_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" required="" id="nama" name="nama" placeholder="<?php echo lang('deskripsi_form_name_placeholder'); ?>" value="<?php echo ($data) ? $data->nama : ''; ?>">
                                            </div>
                                        </div>
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('catalog/deskripsi'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var rules_form = {};
</script>