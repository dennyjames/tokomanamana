<?php
if ($features->num_rows() > 0) {
    foreach ($features->result() as $feature) {
        ?>
        <div class="form-group">
            <label class="col-md-3 control-label"><?php echo $feature->name; ?></label>
            <div class="col-md-9">
                <?php
                if ($feature->type == 'i') {
                    echo '<input type="text" name="feature[' . $feature->id . '][value]" class="form-control" value="' . $feature->value . '">';
                } elseif ($feature->type == 't') {
                    echo '<textarea name="feature[' . $feature->id . '][value]" class="form-control">' . $feature->value . '</textarea>';
                } else {
                    $variants = $this->main->gets('feature_variant', array('feature' => $feature->id));
                    if ($feature->type == 's') {
                        echo '<select name="feature[' . $feature->id . '][value]" class="form-control">';
                        echo '<option value=""></option>';
                        foreach ($variants->result() as $variant) {
                            echo '<option value="' . $variant->id . '"' . (($variant->id == $feature->value) ? 'selected' : '') . '>' . $variant->value . '</option>';
                        }
                        echo '</select>';
                    } elseif ($feature->type == 'c') {
                        $values = array();
                        if ($product) {
                            $feature_values = $this->main->gets('product_feature', array('product' => $product, 'feature' => $feature->id));
                            if ($feature_values) {
                                foreach ($feature_values->result() as $value) {
                                    $values[] = $value->value;
                                }
                            }
                        }
                        foreach ($variants->result() as $variant) {
                            echo '<div class="checkbox"><label><input name="feature[' . $feature->id . '][value][]" type="checkbox" value="' . $variant->id . '" ' . ((in_array($variant->id, $values)) ? 'checked' : '') . '> ' . $variant->value . ' </label></div>';
                        }
                    }
                }
                ?>
            </div>
        </div>
        <?php
    }
}
?>