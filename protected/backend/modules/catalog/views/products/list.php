<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <?php //if ($this->aauth->is_allowed('catalog/product/add')) { ?>
                        <!-- <a href="<?php //echo site_url('catalog/products/add'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span><?php //echo lang('add_heading'); ?></span></a> -->
                    <?php //} ?>
                    <?php if ($this->aauth->is_allowed('catalog/product/edit')) { ?>
                        <a href="javascript:void(0);" class="btn btn-link btn-float has-text" id="btn-export"><i class="icon-download7 text-primary"></i><span>Export</span></a>
                        <!-- <a href="javascript:void(0);" class="btn btn-link btn-float has-text" id="btn-import"><i class="icon-upload7 text-primary"></i><span>Import</span></a> -->
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table table-hover" id="table" data-url="<?php echo site_url('catalog/products/get_list'); ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="asc"><?php echo lang('code'); ?></th>
                        <th><?php echo lang('name'); ?></th>
                        <th><?php echo lang('price'); ?></th>
                        <th><?php echo lang('category'); ?></th>
                        <th><?php echo lang('status'); ?></th>
                        <th><?php echo lang('store_type'); ?></th>
                        <th><?php echo lang('store'); ?></th>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
<?php //if ($this->aauth->is_allowed('catalog/product/edit')) { ?>
    <!-- <div id="modal-import" class="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h5 class="modal-title">Import</h5>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" action="<?php //echo site_url('catalog/products/import'); ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="control-label col-sm-3">File</label>
                            <div class="col-sm-9">
                                <input type="file" id="file" name="file" class="form-control" required="">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    <button type="button" id="submit-import" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div> -->
<?php //} ?>