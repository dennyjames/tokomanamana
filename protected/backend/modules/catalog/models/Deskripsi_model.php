<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Deskripsi_model extends CI_Model {

    function get($id) {
        $this->db->select("*", FALSE);
        return $this->db->get_where('deskripsi', array('id_deskripsi' => $id))->row();
    }

    function get_all($start = 0, $length = 0, $search = '', $order = array()) {
        $this->where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        if ($length) {
            $this->db->limit($length, $start);
        }
        $this->db->select('*');

        return $this->db->get('deskripsi');
    }

    function get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'nama';
                break;
            case 1: $key = 'c1.home_show';
                break;
            case 2: $key = 'c1.home_sort_order';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->where_like($search);
        $this->db->select("*", FALSE);
        return $this->db->count_all_results('deskripsi');
    }

    function where_like($search = '') {
        $columns = array('nama');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    function get_all_parents($id = '') {
        $this->db->select("*", FALSE);

        if ($id) {
            $this->db->where('id_deskripsi !=', $id);
        }

        return $this->db->get('deskripsi');
    }

}
