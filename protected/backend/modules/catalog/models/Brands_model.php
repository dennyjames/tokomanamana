<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Brands_model extends CI_Model {

    function get($id) {
        $this->db->select('b.*, keyword seo_url')
                ->join('seo_url su', "su.query = CONCAT('catalog/brands/view/',b.id)",'left')
                ->where('b.id', $id);
        $query = $this->db->get('brands b');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }
    
    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('b.*, su.keyword seo_url')
                ->join('seo_url su', "su.query = CONCAT('catalog/brands/view/',b.id)",'left')
                ->limit($length, $start);

        return $this->db->get('brands b');
    }

    function get_alias_key($key) {
        switch ($key) {
            case 1: $key = 'name';
                break;
            case 2: $key = 'seo_url';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->where_like($search);
        return $this->db->join('seo_url su', "su.query = CONCAT('catalog/brands/view/',b.id)",'left')->count_all_results('brands b');
    }

    function where_like($search = '') {
        $columns = array('name', 'su.keyword');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

}
