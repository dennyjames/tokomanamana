<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Categories_model extends CI_Model {

    function get($id) {
        $this->db->select("cp.category id, GROUP_CONCAT(c2.name ORDER BY cp.level SEPARATOR ' > ') name, c1.image, c1.active, c1.sort_order, c1.home_show, c1.home_sort_order", FALSE)
                ->join('categories c1', 'cp.category = c1.id', 'left')
                ->join('categories c2', 'cp.path = c2.id', 'left')
                ->group_by('cp.category');
        return $this->db->get_where('category_path cp', array('cp.category' => $id))->row();
    }

    function get_all($start = 0, $length = 0, $search = '', $order = array()) {
        $this->where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        if ($length) {
            $this->db->limit($length, $start);
        }
        $this->db->select("cp.category id, GROUP_CONCAT(c2.name ORDER BY cp.level SEPARATOR ' > ') as name, c1.image, c1.active, c1.sort_order, c1.home_show, c1.home_sort_order", FALSE)
                ->join('categories c1', 'cp.category = c1.id', 'left')
                ->join('categories c2', 'cp.path = c2.id', 'left')
                ->group_by('cp.category');

        return $this->db->get('category_path cp');
    }

    function get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'name';
                break;
            case 1: $key = 'c1.home_show';
                break;
            case 2: $key = 'c1.home_sort_order';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->where_like($search);
        $this->db->select("cp.category id, GROUP_CONCAT(c2.name ORDER BY cp.level SEPARATOR ' > ') name", FALSE)
                ->join('categories c1', 'cp.category = c1.id', 'left')
                ->join('categories c2', 'cp.path = c2.id', 'left')
                ->group_by('cp.category');
        return $this->db->count_all_results('category_path cp');
    }

    function where_like($search = '') {
        $columns = array('c1.name');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    function get_all_parents($id = '') {
        $this->db->select("cp.category id, GROUP_CONCAT(c2.name ORDER BY cp.level SEPARATOR ' > ') name", FALSE)
                ->join('categories c1', 'cp.category = c1.id', 'left')
                ->join('categories c2', 'cp.path = c2.id', 'left')
                ->where_not_in('cp.category', 'SELECT category FROM category_path WHERE level = 2', false)
                ->order_by('c1.name')
                ->group_by('cp.category');

        if ($id) {
            $this->db->where('cp.category !=', $id);
        }

        return $this->db->get('category_path cp');
    }

}
