<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Deskripsi_relasi_model extends CI_Model {

    function get($id) {
        $this->db->select("*", FALSE);
        return $this->db->get_where('deskripsi_relasi', array('id_deskripsi_relasi' => $id))->row();
    }

    function get_all($start = 0, $length = 0, $search = '', $order = array()) {
        $this->where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        if ($length) {
            $this->db->limit($length, $start);
        }
        $this->db->select('dr.*, c.name, d.nama')
             ->join('categories c', 'dr.id_kategori = c.id', 'left')
             ->join('deskripsi d', 'dr.id_deskripsi = d.id_deskripsi', 'left')
             ->group_by('dr.id_kategori');
        return $this->db->get('deskripsi_relasi dr');
    }

    function get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'name';
                break;
            case 1: $key = 'c1.home_show';
                break;
            case 2: $key = 'c1.home_sort_order';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->where_like($search);
        $this->db->select("*", FALSE);
        return $this->db->count_all_results('deskripsi_relasi');
    }

    function where_like($search = '') {
        $columns = array('nama');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }

    function get_all_parents($id = '') {
        $this->db->select('dr.*, c.name, c.id')
             ->join('deskripsi_relasi dr', 'dr.id_kategori = c.id', 'left')
             ->order_by('c.name', 'ASC')
             ->where('dr.id_deskripsi_relasi IS NULL');

        if ($id) {
            $this->db->where('c.id !=', $id);
        }

        return $this->db->get('categories c');
    }

    function get_kategori($id) {
        $this->db->select('id, name')
             ->where('id', $id);
        return $this->db->get('categories'); 
    }

    function get_param_lama($id){
        $this->db->select('*')
             ->where('id_kategori', $id);
        return $this->db->get('deskripsi_relasi');
    }

    function get_param_kosong($id){
        return $this->db->query("select a.* , if(b.id_deskripsi is null, 'No', 'YES') as hasil from deskripsi a left join (select * from deskripsi_relasi where id_kategori='$id') b ON a.id_deskripsi=b.id_deskripsi order by a.nama asc");
    }

}
