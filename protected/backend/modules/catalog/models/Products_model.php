<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model {

    // function get($id, $store) {
    //     $this->db->select('products_principal_stock.product_id,
    //                         products_principal_stock.branch_id,
    //                         products_principal_stock.quantity,
    //                         products.*,
    //                         seo_url.keyword AS seo_url')
    //                 ->join('products', 'products_principal_stock.product_id = products.id', 'LEFT')
    //                 ->join('seo_url', "seo_url.query = CONCAT('catalog/products/view/', products_principal_stock.product_id, '/', products_principal_stock.branch_id)", 'LEFT')
    //                 ->where('products_principal_stock.product_id', $id)
    //                 ->where('products_principal_stock.branch_id', $store);
    //     $query = $this->db->get('products_principal_stock');

    //     return ($query->num_rows() > 0) ? $query->row() : false;
    //     // $this->db->select('p.*, keyword seo_url')
    //     //         ->join('seo_url su', "su.query = CONCAT('catalog/products/view/',p.id)", 'left')
    //     //         ->where('p.id', $id);
    //     // $query = $this->db->get('products p');
    //     // return ($query->num_rows() > 0) ? $query->row() : false;
    // }

    function get($id, $store) {
        $this->db->select('products.*,
                            seo_url.keyword AS seo_url
                            ')
                        ->join('seo_url', "seo_url.query = CONCAT('catalog/products/view', products.id, '/', products.store_id)", 'left')
                        ->where('products.id', $id)
                        ->where('products.store_id', $store);
        $query = $this->db->get('products');

        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    // function get_all($merchant = 0, $start = 0, $length, $search = '', $order = array(), $category = null) {
    //     $this->_where_like($search);
        
    //     if ($order) {
    //         $order['column'] = $this->_get_alias_key($order['column']);
    //         $this->db->order_by($order['column'], $order['dir']);
    //     }

    //     $this->db->select('products.code,
    //                         products_principal_stock.product_id AS id,
    //                         products_principal_stock.branch_id,
    //                         products.name,
    //                         products.price,
    //                         products.price_old,
    //                         products.discount,
    //                         products_principal_stock.quantity,
    //                         products.category,
    //                         categories.name AS category,
    //                         products.status,
    //                         merchants.type AS store_type,
    //                         merchants.name AS store_name')
    //                 ->join('products', 'products_principal_stock.product_id = products.id', 'LEFT')
    //                 ->join('merchants', 'products_principal_stock.branch_id = merchants.id', 'LEFT')
    //                 ->join('categories', 'products.category = categories.id', 'LEFT')
    //                 ->limit($length, $start);
        
    //     if ($merchant == -1) {
    //         $this->db->where('products_principal_stock.branch_id != ', 0);
    //     }
    //     elseif ($merchant > 0) {
    //         $this->db->where('products_principal_stock.branch_id', $merchant);
    //     }

    //     if($category) {
    //         $this->db->where('categories.id', $category);
    //     }
    //     else {
    //         $this->db->where('products.merchant', 0);
    //     }

    //     return $this->db->get('products_principal_stock');



//        // $this->db->select('p.id, code, p.name, price_old, discount, quantity, price, p.category, p.status, c.name category')
//        //        ->join("(SELECT `cp`.`category`, GROUP_CONCAT(c2.name ORDER BY cp.level SEPARATOR ' > ') name FROM `category_path` `cp` LEFT JOIN `categories` `c1` ON `cp`.`category` = `c1`.`id` LEFT JOIN `categories` `c2` ON `cp`.`path` = `c2`.`id` GROUP BY `cp`.`category`) c", 'c.category = p.category')
//        //         ->join('categories c', 'c.id = p.category', 'left')
//        //         ->limit($length, $start);
//        // if ($merchant == -1) {
//        //     $this->db->select('m.name merchant_name')
//        //             ->join('merchants m', 'm.id = p.merchant', 'left')
//        //             ->where('p.merchant !=', 0);
//        // } elseif ($merchant > 0) {
//        //     $this->db->where('p.merchant', $merchant);
//        // } else {
//        //     $this->db->select('(SELECT COUNT(id) FROM product_merchant pm WHERE pm.product = p.id) total_merchant')
//        //             ->where('p.merchant', 0);
//        // }

//        // return $this->db->get('products p');
    // }

    function get_all($merchant = 0, $start = 0, $length, $search = '', $order = array(), $category = null) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('products.code,
                            products.id AS id,
                            products.store_id AS store_id,
                            products.name,
                            products.price,
                            products.price_old,
                            products.discount,
                            products.quantity,
                            products.store_type,
                            categories.name AS category,
                            products.status,
                            CASE WHEN products.store_type = \'principal\' THEN principles.name ELSE merchants.name END store_name
                            ')
                    ->join('merchants', 'products.store_id = merchants.id', 'LEFT')
                    ->join('categories', 'products.category = categories.id', 'LEFT')
                    ->join('principles', 'products.store_id = principles.id', 'LEFT')
                    ->join('products_principal_stock', 'products.id = products_principal_stock.product_id', 'left')
                    ->limit($length, $start)
                    ->where('products.store_id !=', 0);
        if ($merchant > 0) {
            $this->db->group_start()
                        ->where('merchants.id', $merchant)
                        ->or_where('principles.id', $merchant)
                        ->group_end();
        }

        if($category) {
            $this->db->where('categories.id', $category);
        }

        return $this->db->get('products');
    }

    private function _get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'products.code';
                break;
            case 1: $key = 'products.name';
                break;
            case 2: $key = 'products.price';
                break;
            case 3: $key = 'categories.name';
                break;
            case 4: $key = 'products.status';
                break;
            case 5: $key = 'products.store_type';
                break;
            case 6: $key = 'CASE WHEN products.store_type = \'principal\' THEN principles.name ELSE merchants.name END store_name';
                break;
        }
        return $key;
    }

    function count_all($search = '', $merchant = null, $category = null) {
        $this->_where_like($search);

        if($merchant) {
            $this->db->group_start()
                        ->where('merchants.id', $merchant)
                        ->or_where('principles.id', $merchant)
                        ->group_end();
        }

        if($category) {
            $this->db->where('categories.id', $category);
        }

        $this->db->select('products.code,
                            products.id AS id,
                            products.store_id AS store_id,
                            products.name,
                            products.price,
                            products.price_old,
                            products.discount,
                            products.quantity,
                            products.store_type,
                            categories.name AS category,
                            products.status,
                            CASE WHEN products.store_type = \'principal\' THEN principles.name ELSE merchants.name END store_name
                            ')
                    ->join('merchants', 'products.store_id = merchants.id', 'LEFT')
                    ->join('categories', 'products.category = categories.id', 'LEFT')
                    ->join('principles', 'products.store_id = principles.id', 'LEFT')
                    ->join('products_principal_stock', 'products.id = products_principal_stock.product_id', 'left');
        
        return $this->db->count_all_results('products');


        // $this->db->select('p.id, code, p.name, price, p.category, p.status, c.name category, (SELECT COUNT(id) FROM product_merchant pm WHERE pm.product = p.id) total_merchant')
        //         ->join("(SELECT `cp`.`category`, GROUP_CONCAT(c2.name ORDER BY cp.level SEPARATOR ' > ') name FROM `category_path` `cp` LEFT JOIN `categories` `c1` ON `cp`.`category` = `c1`.`id` LEFT JOIN `categories` `c2` ON `cp`.`path` = `c2`.`id` GROUP BY `cp`.`category`) c", 'c.category = p.category');
        // return $this->db->count_all_results('products p');
    }

    private function _where_like($search = '') {
        $columns = array('products.code', 'products.name', 'products.price', 'categories.name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

    function get_categories() {
        $this->db->select("pcp.category id, GROUP_CONCAT(pc2.name ORDER BY pcp.level SEPARATOR ' > ') name", FALSE)
                ->join('categories pc1', 'pcp.category = pc1.id', 'left')
                ->join('categories pc2', 'pcp.path = pc2.id', 'left')
                ->order_by('pc2.name')
                ->group_by('pcp.category');

        return $this->db->get('category_path pcp');
    }

    function get_all_category() {
        return $this->db->query("SELECT id, name FROM categories ORDER BY name ASC");
    }

    function get_all_merchants() {
        return $this->db->query("(SELECT id, name FROM merchants WHERE type = 'merchant') UNION (SELECT id, name FROM principles) ORDER BY name ASC");
    }

    function get_package_item($q) {
        $this->db->select("p.id , p.name as value");
        $this->db->limit(6);
        $this->db->like('name',$q);
        $query = $this->db->get('products p');
        return $query->result();
    }

    function get_product_features($product) {
        $this->db->select('pf.product, pf.value, f.*')
                ->join('features f', 'pf.feature = f.id', 'left')
                ->where('pf.product', $product)
                ->group_by('pf.feature');
        return $this->db->get('product_feature pf');
    }

    function get_product_feature_variants($product, $feature) {
        $this->db->select('fv.*')
                ->join('feature_variant fv', 'pf.feature = fv.feature AND pf.value = fv.id', 'left')
                ->where('pf.product', $product)
                ->where('pf.feature', $feature);
        $query = $this->db->get('product_feature pf');
        return($query->num_rows() > 0) ? $query : false;
    }

    function get_product_feature_by_category($category, $product = 0) {
        $this->db->select('cf.category, f.*, fv.value variant')
                ->join('features f', 'f.id = cf.feature', 'left')
                ->join('feature_variant fv', 'fv.id = cf.feature', 'left')
                ->where('cf.category', $category)
                ->group_by('cf.feature')
                ->order_by('f.sort_order ASC');

        if ($product) {
            $this->db->select('pf.product, pf.value')
                    ->join('product_feature pf', 'pf.feature = cf.feature AND pf.product = ' . $product, 'left');
        }
        return $this->db->get('category_feature cf');
    }

    function get_features_by_category($category, $product = 0) {
        $this->db->select('f.*, pf.value')
                ->join('features f', 'cf.feature = f.id', 'left')
                ->join('product_feature pf', 'pf.feature = cf.feature AND pf.product = ' . $product, 'left')
                ->where('cf.category', $category)
                ->group_by('cf.feature')
                ->order_by('f.sort_order ASC');
        return $this->db->get('category_feature cf');
    }

    function get_prices($product = 0) {
        $this->db->select('pp.*, mg.id merchant_group, mg.name')
                ->join('product_price pp', 'mg.id = pp.merchant_group AND pp.product = ' . $product, 'left')
                ->order_by('mg.name');
        return $this->db->get('merchant_groups mg');
    }

    function price_level_groups($price_level = 0) {
        $this->db->select('pp.*, mg.id merchant_group, mg.name')
                ->join('product_price_level_groups pp', 'mg.id = pp.merchant_group AND pp.product_price_level = ' . $price_level, 'left')
                ->order_by('mg.name');
        return $this->db->get('merchant_groups mg');
    }

    function get_product_options($product, $ids = array()) {
        $this->db->select("po.*, GROUP_CONCAT(og.name, ': ', o.value ORDER BY og.sort_order ASC SEPARATOR ', ') option_combination")
                ->join('product_option_combination poc', 'po.id = poc.product_option', 'left')
                ->join('options o', 'poc.option = o.id', 'left')
                ->join('option_group og', 'og.id = o.group', 'left')
                ->group_by('po.id')
                ->where('po.product', $product);
        if($ids)
            $this->db->where_in('po.id',$ids);
        return $this->db->get('product_option po');
    }

    function get_product_option_image($product, $product_option) {
        $this->db->select('id, product, image, product_option')
                ->join('product_option_image poi', 'pi.id = poi.product_image AND poi.product_option = ' . $product_option, 'left')
                ->where('pi.product', $product);

        return $this->db->get('product_image pi');
    }

    // function get_product_option($product) {
    //     $this->db->select('po.*, o.name')
    //             ->join('options o', 'o.id = po.option', 'left')
    //             ->where('po.product', $product);
    //     return $this->db->get('product_option po');
    // }

    // function get_product_option_variant($product_option) {
    //     $this->db->select('pov.*, ov.value')
    //             ->join('option_variant ov', 'pov.option_variant = ov.id', 'left')
    //             ->where('pov.product_option', $product_option);
    //     return $this->db->get('product_option_variant pov');
    // }

    // function get_available_options() {
    //     $this->db->having('(SELECT COUNT(ov.id) FROM option_variant ov WHERE ov.option = o.id) >', 0)
    //             ->order_by('o.name', 'asc');
    //     return $this->db->get('options o');
    // }

    // function export($start = 0, $length, $search = '', $order = array()) {
    //     $this->_where_like($search);

    //     if ($order) {
    //         $order['column'] = $this->_get_alias_key($order['column']);
    //         $this->db->order_by($order['column'], $order['dir']);
    //     }

    //     $this->db->select('products_principal_stock.product_id AS id,
    //                         products_principal_stock.branch_id,
    //                         products.name,
    //                         products.short_description,
    //                         products.code,
    //                         products.length,
    //                         products.width,
    //                         products.height,
    //                         products.weight,
    //                         products.price,
    //                         categories.name AS category')
    //                 ->join('products', 'products_principal_stock.product_id = products.id', 'LEFT')
    //                 ->join('categories', 'categories.id = products.category', 'LEFT')
    //                 ->where('products.merchant', 0)
    //                 ->limit($length, $start);

    //     return $this->db->get('products_principal_stock');

    //     // $this->db->select('products.*')
    //     //         ->join('categories', 'categories.id = products.category', 'left')
    //     //         ->limit($length, $start)
    //     //         ->select('(SELECT COUNT(id) FROM product_merchant WHERE product_merchant.product = products.id) total_merchant')
    //     //         ->where('products.merchant', 0);

    //     // return $this->db->get('products');
    // }
    function export($start = 0, $length, $search = '', $order = array(), $category, $merchant) {
        $this->_where_like($search);
        if ($order) {
            $order['column'] = $this->_get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        if ($merchant > 0) {
            $this->db->group_start()
                        ->where('merchants.id', $merchant)
                        ->or_where('principles.id', $merchant)
                        ->group_end();
        }

        if($category) {
            $this->db->where('categories.id', $category);
        }
        $this->db->select('products.id AS id,
                        products.store_id,
                        products.store_type,
                        products.name,
                        products.short_description,
                        products.code,
                        products.length,
                        products.width,
                        products.height,
                        products.weight,
                        products.price,
                        CASE WHEN products.store_type = \'principal\' THEN principles.name ELSE merchants.name END store_name,
                        categories.name AS category')
                ->join('categories', 'categories.id = products.category', 'LEFT')
                ->join('merchants', 'merchants.id = products.store_id', 'LEFT')
                ->join('principles', 'principles.id = products.store_id', 'LEFT')
                ->limit($length, $start);
        return $this->db->get('products');
    }

    function get_table_deskripsi($id){
        $this->db->select('dr.*, d.nama')
             ->join('deskripsi d', 'dr.id_deskripsi = d.id_deskripsi', 'left')
             ->where('dr.id_kategori', $id);
        return $this->db->get('deskripsi_relasi dr');
    }

}
