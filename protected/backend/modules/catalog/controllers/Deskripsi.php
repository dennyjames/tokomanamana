<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Deskripsi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->aauth->control('catalog/deskripsi');

        $this->lang->load('deskripsi', settings('language'));
        $this->load->model('deskripsi_model', 'desk');
        $this->data['menu'] = 'catalog_deskripsi';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('deskripsi'), '/');
        $this->breadcrumbs->push(lang('deskripsi'), '/catalog/deskripsi');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('deskripsi_heading'));
        $this->load->view('deskripsi/deskripsi', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->desk->get_all($start, $length, $search, $order);
//        log_message('debug', $this->db->last_query());
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->nama,
                    '<td class="text-center">
                    <ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="' . site_url('catalog/deskripsi/form/' . encode($data->id_deskripsi)) . '">' . lang('button_edit') . '</a></li>
                    <li><a href="' . site_url('catalog/deskripsi/delete/' . encode($data->id_deskripsi)) . '" class="delete">' . lang('button_delete') . '</a></li>
                    </ul>
                    </li>
                    </ul>
                    </td>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->desk->count_all();
        $output['recordsFiltered'] = $this->desk->count_all($search);
        echo json_encode($output);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->load->js('../assets/backend/js/plugins/forms/duallistbox.min.js');
        $this->load->js('../assets/backend/js/modules/catalog/deskripsi_form.js');

        $this->data['data'] = array();
        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('deskripsi'), '/catalog/deskripsi');

        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->main->get('deskripsi', array('id_deskripsi' => $id));
            $this->breadcrumbs->push(lang('deskripsi_edit_heading'), '/catalog/deskripsi/form/' . encode($id));
            $this->breadcrumbs->push($this->data['data']->nama, '/');
        } else {
            $this->breadcrumbs->push(lang('deskripsi_add_heading'), '/catalog/deskripsi/form');
        }

        $this->data['deskripsi'] = $this->desk->get_all_parents($id);

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(($this->data['data']) ? lang('deskripsi_edit_heading') : lang('deskripsi_add_heading'));
        $this->load->view('deskripsi/deskripsi_form', $this->data);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('nama', 'lang:nama deksripsi', 'trim|required');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            $data['nama'] = $data['nama'];
            if (!$data['id_deskripsi']) {
                $save = $this->main->insert('deskripsi', $data);
                $data['id_deskripsi'] = $save;
            } else {
                $data['id_deskripsi'] = decode($data['id_deskripsi']);
                $save = $this->main->update('deskripsi', $data, array('id_deskripsi' => $data['id_deskripsi']));
            }


            if ($save !== false) {
                $return = array('message' => sprintf(lang('deskripsi_save_success_message'), $data['nama']), 'status' => 'success', 'redirect' => site_url('catalog/deskripsi'));
            } else {
                $return = array('message' => sprintf(lang('deskripsi_save_error_message'), $data['nama']), 'status' => 'error');
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('deskripsi', array('id_deskripsi' => $id));

        $delete = $this->main->delete('deskripsi', array('id_deskripsi' => $id));
        if ($delete) {
            $return = array('message' => sprintf(lang('deskripsi_delete_success_message'), $data->nama), 'status' => 'success');
        } else {
            $return = array('message' => lang('deskripsi_delete_error_message'), 'status' => 'error');
        }
        echo json_encode($return);
    }
}
