<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Brands extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->aauth->control('catalog/brand');

        $this->lang->load('brands', settings('language'));
        $this->load->model('brands_model', 'brands');

        $this->data['menu'] = 'catalog_brand';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('brand'), '/catalog/brands');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('brand_heading'));
        $this->load->view('brands/brands', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->brands->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    ($data->image) ? '<a href="#"><img src="' . site_url('../files/images/' . $data->image) . '" class="img-circle img-xs"></a>' : '',
                    $data->name,
                    $data->seo_url,
                    '<td class="text-center">
                    <ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="' . site_url('catalog/brands/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>
                    <li><a href="' . site_url('catalog/brands/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>
                    </ul>
                    </li>
                    </ul>
                    </td>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->brands->count_all();
        $output['recordsFiltered'] = $this->brands->count_all($search);
        echo json_encode($output);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->load->js('../assets/backend/js/modules/catalog/brand_form.js');

        $this->data['data'] = array();
        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('brand'), '/catalog/brands');

        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->brands->get($id);
            $this->data['data']->description = tinymce_get_url_files($this->data['data']->description);
            $this->breadcrumbs->push(lang('brand_edit_heading'), '/catalog/brands/form/' . encode($id));
            $this->breadcrumbs->push($this->data['data']->name, '/');
        } else {
            $this->breadcrumbs->push(lang('brand_add_heading'), '/catalog/brands/form');
        }
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(($this->data['data']) ? lang('brand_edit_heading') : lang('brand_add_heading'));
        $this->load->view('brands/brand_form', $this->data);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:brand_form_name_label', 'trim|required');
        $this->form_validation->set_rules('seo_url', 'lang:brand_form_seo_url_label', 'trim|alpha_dash');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            do {
                if ($data['id'])
                    $data['id'] = decode($data['id']);

                $data['description'] = tinymce_parse_url_files($data['description']);
                
                $url = 'catalog/brands/view/' . $data['id'];
                if ($seo_url = $data['seo_url']) {
                    if (check_url($seo_url, $url)) {
                        $return = array('message' => lang('brand_friendly_url_exist_message'), 'status' => 'error');
                        break;
                    }
                } else {
                    $seo_url = $url;
                }
                unset($data['seo_url']);

                if (!$data['id']) {
                    $id = $this->main->insert('brands', $data);
                    $url .= $id;
                } else {
                    $save = $this->main->update('brands', $data, array('id' => $data['id']));
                    $this->main->delete('seo_url', array('query' => $url));
                }
                
                $this->main->insert('seo_url', array('query' => $url, 'keyword' => $seo_url));

                $return = array('message' => sprintf(lang('brand_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('catalog/brands'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('brands', array('id' => $id));

        $delete = $this->main->delete('brands', array('id' => $id));
        if ($delete) {
            $this->main->delete('seo_url', array('query' => 'catalog/brands/view/' . $data->id));
            $return = array('message' => sprintf(lang('brand_delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => lang('brand_delete_error_message'), 'status' => 'error');
        }

        echo json_encode($return);
    }

}
