<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Features extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('catalog/feature');
        $this->lang->load('catalog/features', settings('language'));
        $this->load->model('features_model', 'features');

        $this->data['menu'] = 'catalog_feature';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();
        $this->load->js('../assets/backend/js/modules/catalog/features.js');

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('feature'), '/catalog/features');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('feature_heading'));
        $this->load->view('features/features', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->features->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->name,
                    '<input type="number" class="form-control input-xs" onchange="update_sort_order(this);" data-id="' . encode($data->id) . '" value="' . $data->sort_order . '">',
//                    $data->count_variant,
                    $data->count_variant,
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                    (($data->type != 'i' && $data->type != 't') ? '<li><a href="' . site_url('catalog/features/variants/' . encode($data->id)) . '">' . lang('button_view') . '</a></li>' : '') .
                    ($this->aauth->is_allowed('catalog/feature/edit') ? '<li><a href="' . site_url('catalog/features/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>' : '') .
                    ($this->aauth->is_allowed('catalog/feature/delete') ? '<li><a href="' . site_url('catalog/features/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>' : '') .
                    '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->features->count_all();
        $output['recordsFiltered'] = $this->features->count_all($search);
        echo json_encode($output);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->data['data'] = array();
        if ($id) {
            $this->aauth->control('catalog/feature/edit');
            $id = decode($id);
            $this->data['data'] = $this->main->get('features', array('id' => $id));
        } else {
            $this->aauth->control('catalog/feature/add');
        }

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('feature'), '/catalog/features');
        $this->breadcrumbs->push(($this->data['data']) ? lang('feature_edit_heading') : lang('feature_add_heading'), '/');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(($this->data['data']) ? lang('feature_edit_heading') : lang('feature_add_heading'));
        $this->load->view('features/feature_form', $this->data);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:feature_form_name_label', 'trim|required');
        if (!$this->input->post('id')) {
            $this->form_validation->set_rules('type', 'lang:feature_form_type_label', 'trim|required');
        }

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            if (!$data['id']) {
                $save = $this->main->insert('features', $data);
            } else {
                $data['id'] = decode($data['id']);
                $save = $this->main->update('features', $data, array('id' => $data['id']));
            }

            if ($save !== false) {
                $return = array('message' => sprintf(lang('feature_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('catalog/features'));
            } else {
                $return = array('message' => sprintf(lang('feature_save_error_message'), $data['name']), 'status' => 'error');
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('catalog/feature/delete');
        $id = decode($id);
        $data = $this->main->get('features', array('id' => $id));

        $delete = $this->main->delete('features', array('id' => $id));
        if ($delete) {
            $return = array('message' => sprintf(lang('feature_delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => sprintf(lang('feature_delete_error_message'), $data->name), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function update_sort_order() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        if ($this->main->update('features', array('sort_order' => $this->input->post('value')), array('id' => decode($this->input->post('id'))))) {
            $return = array('message' => 'Urutan fitur berhasil diperbaharui', 'status' => 'success');
        } else {
            $return = array('message' => 'Urutan fitur gagal diperbaharui', 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function variants($feature) {
        $this->template->_init();
        $this->template->table();

        $feature = decode($feature);

        $this->data['data'] = $this->main->get('features', array('id' => $feature));

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('feature'), '/catalog/features');
        $this->breadcrumbs->push(lang('feature_variant_heading'), '/catalog/features/variants/' . encode($feature));
        $this->breadcrumbs->push($this->data['data']->name, '/');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('feature_variant_add_heading'));
        $this->load->view('features/feature_variants', $this->data);
    }

    public function get_variant_list($feature) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $feature = decode($feature);

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->features->get_feature_variants($feature, $start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->value,
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="' . site_url('catalog/features/variant_form/' . $data->feature . '/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>
                    <li><a href="' . site_url('catalog/features/variant_delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>
                    </ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->features->count_feature_variants($feature);
        $output['recordsFiltered'] = $this->features->count_feature_variants($feature, $search);
        echo json_encode($output);
    }

    public function variant_form($feature = '', $id = '') {
        $this->template->_init();
        $this->template->form();

        $this->data['data'] = array();
        $this->data['feature'] = array();
        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->main->get('feature_variant', array('id' => $id));
        }
        if ($feature) {
            $this->data['feature'] = $this->main->get('features', array('id' => $feature));
        }

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('feature'), '/catalog/features');
        if ($feature)
            $this->breadcrumbs->push($this->data['feature']->name, '/catalog/features/variants/' . encode($feature));
        $this->breadcrumbs->push(($this->data['data']) ? lang('feature_variant_edit_heading') : lang('feature_variant_add_heading'), '/');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->data['features'] = $this->features->parent_variants();
        $this->output->set_title(($this->data['data']) ? lang('feature_variant_edit_heading') : lang('feature_variant_add_heading'));
        $this->load->view('features/feature_variant_form', $this->data);
    }

    public function variant_save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('feature', 'lang:feature_variant_form_feature_label', 'trim|required');
        $this->form_validation->set_rules('value', 'lang:feature_variant_form_value_label', 'trim|required');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            $new = 0;
            if (isset($data['submit_new'])) {
                $new = $data['submit_new'];
                unset($data['submit_new']);
            }

            if (!$data['id']) {
                $save = $this->main->insert('feature_variant', $data);
            } else {
                $data['id'] = decode($data['id']);
                $save = $this->main->update('feature_variant', $data, array('id' => $data['id']));
            }

            if ($save !== false) {
                if ($new) {
                    $redirect = 'catalog/features/variant_form/' . $data['feature'];
                } else {
                    $redirect = 'catalog/features/variants/' . encode($data['feature']);
                }
                $return = array('message' => lang('message_save_success'), 'status' => 'success', 'redirect' => site_url($redirect));
            } else {
                $return = array('message' => lang('message_save_error'), 'status' => 'error');
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function variant_delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $delete = $this->main->delete('feature_variant', array('id' => $id));
        if ($delete) {
            $return = array('message' => lang('message_delete_success'), 'status' => 'success');
        } else {
            $return = array('message' => lang('message_delete_error'), 'status' => 'error');
        }

        echo json_encode($return);
    }

}
