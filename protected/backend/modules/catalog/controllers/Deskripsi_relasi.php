<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Deskripsi_relasi extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->aauth->control('catalog/deskripsi_relasi');

        $this->lang->load('deskripsi_relasi', settings('language'));
        $this->load->model('deskripsi_relasi_model', 'desk');
        $this->data['menu'] = 'deskripsi_relasi';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('deskripsi_relasi'), '/');
        $this->breadcrumbs->push(lang('deskripsi_relasi'), '/catalog/deskripsi_relasi');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('deskripsi_relasi_heading'));
        $this->load->view('deskripsi_relasi/deskripsi_relasi', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->desk->get_all($start, $length, $search, $order);
//        log_message('debug', $this->db->last_query());
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->name,
                    '<td class="text-center">
                    <ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="' . site_url('catalog/deskripsi_relasi/form/' . encode($data->id_kategori)) . '">' . lang('button_edit') . '</a></li>
                    <li><a href="' . site_url('catalog/deskripsi_relasi/delete/' . encode($data->id_kategori)) . '" class="delete">' . lang('button_delete') . '</a></li>
                    </ul>
                    </li>
                    </ul>
                    </td>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->desk->count_all();
        $output['recordsFiltered'] = $this->desk->count_all($search);
        echo json_encode($output);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->load->js('../assets/backend/js/plugins/forms/duallistbox.min.js');
        $this->load->js('../assets/backend/js/modules/catalog/deskripsi_relasi_form.js');

        $this->data['data'] = array();
        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('deskripsi_relasi'), '/catalog/deskripsi_relasi');
        $this->data['categories'] = $this->desk->get_all_parents($id);
        $this->data['categories_lama'] = '';
        $this->data['param_lama'] = '';
        $this->data['param_kosong'] = '';

        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->main->get('categories', array('id' => $id));
            $this->breadcrumbs->push(lang('deskripsi_relasi_edit_heading'), '/catalog/deskripsi_relasi/form/' . encode($id));
            $this->breadcrumbs->push($this->data['data']->name, '/');

            $this->data['param_lama'] = $this->desk->get_param_lama($id);
            $this->data['param_kosong'] = $this->desk->get_param_kosong($id);
            $this->data['categories_lama'] = $this->desk->get_kategori($id);
            $tes = $this->desk->get_kategori($id)->result();
            /*var_dump($tes);
            exit();*/
        } else {
            $this->breadcrumbs->push(lang('deskripsi_relasi_add_heading'), '/catalog/deskripsi_relasi/form');
        }

        $this->data['deskripsi_relasi'] = $this->desk->get_all_parents($id);

        $this->data['param'] = $this->main->gets('deskripsi', array(), 'nama asc');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(($this->data['data']) ? lang('deskripsi_relasi_edit_heading') : lang('deskripsi_relasi_add_heading'));
        $this->load->view('deskripsi_relasi/deskripsi_relasi_form', $this->data);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('param', 'lang:param', 'trim');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            if ($data['idkat']) {
                
                $id_kat = $data['idkat'];
                $save = $data['idkat'];
                $param = array_filter($data['param']);
                $this->main->delete('deskripsi_relasi', array('id_kategori' => $id_kat));
                if ($param) {
                    $data_perm = array();
                    foreach ($param as $perm) {
                        array_push($data_perm, array('id_deskripsi' => $perm, 'id_kategori' => $id_kat));
                    }
                    $this->db->insert_batch('deskripsi_relasi', $data_perm);
                }

            } else {
                $id_kat = $data['parent'];
                $save = $data['parent'];
                $kat = $this->main->get('categories', array('id' => $id_kat));
                $param = array_filter($data['param']);
                if ($param) {
                    $data_perm = array();
                    foreach ($param as $perm) {
                        array_push($data_perm, array('id_deskripsi' => $perm, 'id_kategori' => $id_kat));
                    }
                    $this->db->insert_batch('deskripsi_relasi', $data_perm);
                }
            }

            $kat = $this->main->get('categories', array('id' => $id_kat));

            if ($save !== false) {
                $return = array('message' => sprintf(lang('deskripsi_relasi_save_success_message'), $kat->name), 'status' => 'success', 'redirect' => site_url('catalog/deskripsi_relasi'));
            } else {
                $return = array('message' => sprintf(lang('deskripsi_relasi_save_error_message'), $kat->name), 'status' => 'error');
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('categories', array('id' => $id));

        $delete = $this->main->delete('deskripsi_relasi', array('id_kategori' => $id));
        if ($delete) {
            $return = array('message' => sprintf(lang('deskripsi_relasi_delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => lang('deskripsi_relasi_delete_error_message'), 'status' => 'error');
        }
        echo json_encode($return);
    }
}
