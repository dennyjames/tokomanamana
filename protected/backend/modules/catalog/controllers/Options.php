<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Options extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('catalog/variation');
        $this->lang->load('catalog/options', settings('language'));
        $this->load->model('options_model', 'options');

        $this->data['menu'] = 'catalog_option';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();
        $this->load->js('../assets/backend/js/modules/catalog/options.js');

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('option'), '/catalog/options');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('option_heading'));
        $this->load->view('options/list', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->options->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->name,
                    '<input type="number" class="form-control input-xs" onchange="update_sort_order(this);" data-id="' . encode($data->id) . '" value="' . $data->sort_order . '">',
                    $data->count_option,
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                    '<li><a href="' . site_url('catalog/options/variants/' . encode($data->id)) . '">' . lang('button_view') . '</a></li>' .
                    ($this->aauth->is_allowed('catalog/variation/edit') ? '<li><a href="' . site_url('catalog/options/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>' : '') .
                    ($this->aauth->is_allowed('catalog/variation/delete') ? '<li><a href="' . site_url('catalog/options/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>' : '') .
                    '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->options->count_all();
        $output['recordsFiltered'] = $this->options->count_all($search);
        echo json_encode($output);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->data['data'] = array();
        if ($id) {
            $this->aauth->control('catalog/variation/edit');
            $id = decode($id);
            $this->data['data'] = $this->main->get('option_group', array('id' => $id));
        } else {
            $this->aauth->control('catalog/variation/add');
        }

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('option'), '/catalog/options');
        $this->breadcrumbs->push(($this->data['data']) ? lang('option_edit_heading') : lang('option_add_heading'), '/');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(($this->data['data']) ? lang('option_edit_heading') : lang('option_add_heading'));
        $this->load->view('options/form', $this->data);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:option_form_name_label', 'trim|required');
        if (!$this->input->post('id')) {
            $this->form_validation->set_rules('type', 'lang:option_form_type_label', 'trim|required');
        }

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            if (!$data['id']) {
                $save = $this->main->insert('option_group', $data);
            } else {
                $data['id'] = decode($data['id']);
                $save = $this->main->update('option_group', $data, array('id' => $data['id']));
            }

            if ($save !== false) {
                $return = array('message' => sprintf(lang('option_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('catalog/options'));
            } else {
                $return = array('message' => sprintf(lang('option_save_error_message'), $data['name']), 'status' => 'error');
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('catalog/variation/delete');
        $id = decode($id);
        $data = $this->main->get('option_group', array('id' => $id));

        $delete = $this->main->delete('option_group', array('id' => $id));
        if ($delete) {
            $return = array('message' => sprintf(lang('option_delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => sprintf(lang('option_delete_error_message'), $data->name), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function update_sort_order() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        if ($this->main->update('option_group', array('sort_order' => $this->input->post('value')), array('id' => decode($this->input->post('id'))))) {
            $return = array('message' => 'Urutan fitur berhasil diperbaharui', 'status' => 'success');
        } else {
            $return = array('message' => 'Urutan fitur gagal diperbaharui', 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function variants($option) {
        $this->template->_init();
        $this->template->table();

        $option = decode($option);

        $this->data['data'] = $this->main->get('option_group', array('id' => $option));

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('option'), '/catalog/options');
        $this->breadcrumbs->push(lang('option_variant_heading'), '/catalog/options/variants/' . encode($option));
        $this->breadcrumbs->push($this->data['data']->name, '/');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('option_variant_add_heading'));
        $this->load->view('options/variant_list', $this->data);
    }

    public function get_variant_list($option) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $option = decode($option);

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $group = $this->main->get('option_group', array('id' => $option));
        $datas = $this->options->get_option_variants($group->id, $start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $row = array(
                    $data->value,
                    $data->color,
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="' . site_url('catalog/options/variant_form/' . $data->group . '/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>
                    <li><a href="' . site_url('catalog/options/variant_delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>
                    </ul>
                    </li>
                    </ul>',
                );
                if ($group->type != 'c') {
                    $row[1] = $row[2];
                    unset($row[2]);
                }
                $output['data'][] = $row;
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->options->count_option_variants($group->id);
        $output['recordsFiltered'] = $this->options->count_option_variants($group->id, $search);
        echo json_encode($output);
    }

    public function variant_form($option, $id = '') {
        $this->template->_init();
        $this->template->form();

        $this->data['data'] = array();
        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->main->get('options', array('id' => $id));
        }
        $this->data['option'] = $this->main->get('option_group', array('id' => $option));

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('option'), '/catalog/options');
        $this->breadcrumbs->push($this->data['option']->name, '/catalog/options/variants/' . encode($option));
        $this->breadcrumbs->push(($this->data['data']) ? lang('option_variant_edit_heading') : lang('option_variant_add_heading'), '/');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->output->set_title(($this->data['data']) ? lang('option_variant_edit_heading') : lang('option_variant_add_heading'));
        $this->load->view('options/variant_form', $this->data);
    }

    public function variant_save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('group', 'lang:option_variant_form_option_label', 'trim|required');
        $this->form_validation->set_rules('value', 'lang:option_variant_form_value_label', 'trim|required');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            $new = 0;
            if (isset($data['submit_new'])) {
                $new = $data['submit_new'];
                unset($data['submit_new']);
            }

            if (!$data['id']) {
                $save = $this->main->insert('options', $data);
            } else {
                $data['id'] = decode($data['id']);
                $save = $this->main->update('options', $data, array('id' => $data['id']));
            }

            if ($save !== false) {
                if ($new) {
                    $redirect = 'catalog/options/variant_form/' . $data['group'];
                } else {
                    $redirect = 'catalog/options/variants/' . encode($data['group']);
                }
                $return = array('message' => lang('message_save_success'), 'status' => 'success', 'redirect' => site_url($redirect));
            } else {
                $return = array('message' => lang('message_save_error'), 'status' => 'error');
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function variant_delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $delete = $this->main->delete('options', array('id' => $id));
        if ($delete) {
            $return = array('message' => lang('message_delete_success'), 'status' => 'success');
        } else {
            $return = array('message' => lang('message_delete_error'), 'status' => 'error');
        }

        echo json_encode($return);
    }

}
