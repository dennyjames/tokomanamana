<?php

defined('BASEPATH') or exit('No direct script access allowed!');

require '../vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Products extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->aauth->control('catalog/product');

        $this->lang->load('catalog/products', settings('language'));
        $this->load->model('products_model', 'products');
        $this->load->model('product_price_model', 'product_prices');
        $this->data['menu'] = 'catalog_product_tanaka';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();
        $this->load->js('../assets/backend/js/modules/catalog/products.js');

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('product'), '/catalog/products');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('heading'));
        $this->load->view('products/list', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));
        $category = intval($this->input->post('category'));
        $merchant = intval($this->input->post('merchant'));


        $output['data'] = array();
        if($merchant != '' && $category != '') {
            $datas = $this->products->get_all($merchant, $start, $length, $search, $order, $category);
        } elseif($category != '') {
            $datas = $this->products->get_all(0, $start, $length, $search, $order, $category);
        } elseif($merchant != '') {
            $datas = $this->products->get_all($merchant, $start, $length, $search, $order);
        } else {
            $datas = $this->products->get_all(0, $start, $length, $search, $order);
        }
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    // '<a href="' . site_url('catalog/products/form/' . encode($data->id)) . '/' . encode($data->store_id) .'">' . $data->code . '</a>',
                    $data->code,
                    $data->name,
                    number($data->price),
                    $data->category,
                    $this->_status_product($data->status),
                    (($data->store_type == 'merchant') ? lang('merchants') : 'Principal'),
                    $data->store_name,
                    '<ul class="icons-list">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                            <ul class="dropdown-menu dropdown-menu-right">' . 
                                // ($this->aauth->is_allowed('catalog/product/edit') ? '<li><a href="' . site_url('catalog/products/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>' : '') .
                                ($this->aauth->is_allowed('catalog/product/view') ? '<li><a href="' . site_url('catalog/products/view/' . encode($data->id)) . '/' . encode($data->store_id) . '">' . lang('button_view') . '</a></li>' : '') .
                                ($this->aauth->is_allowed('catalog/product/delete') ? '<li><a href="' . site_url('catalog/products/delete/' . encode($data->id)) . '/' . encode($data->store_id) . '" class="delete">' . lang('button_delete') . '</a></li>' : '') .
                            '</ul>
                        </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->products->count_all();
        if($category != '' && $merchant != '') {
            $output['recordsFiltered'] = $this->products->count_all($search, $merchant, $category);
        } elseif($category != '') {
            $output['recordsFiltered'] = $this->products->count_all($search, null, $category);
        } elseif($merchant != '') {
            $output['recordsFiltered'] = $this->products->count_all($search, $merchant, null);
        } else {
            $output['recordsFiltered'] = $this->products->count_all($search);
        }
        echo json_encode($output);
    }

    public function get_category() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $categories = $this->products->get_all_category()->result();

        $output = '';
        $output .= '<div class="datatable_category" style="display: inline-block;margin: 0 0 20px 20px;">';
        $output .= '<select name="table_category" id="table_category" style="height: 36px;padding: 7px 12px;font-size: 13px;line-height: 1.5384616;color: #333;background-color: #fff;border: 1px solid #ddd;outline: 0;" onchange="set_datatable()">';
        $output .= '<option value="" selected>Kategori</option>';
        foreach($categories as $category) {
            if($category->name == 'Promo') {
                $output .= '';
            } else {
                $output .= '<option value="' . $category->id . '">' . $category->name . '</option>';
            }
        }
        $output .= '</select>';
        $output .= '</div>';

        echo $output;
        
    }

    public function get_merchant() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $merchants = $this->products->get_all_merchants()->result();

        $output = '';
        $output .= '<div class="datatable_merchant" style="display: inline-block;margin: 0 0 20px 20px;">';
        $output .= '<select name="table_merchant" id="table_merchant" style="height: 36px;padding: 7px 12px;font-size: 13px;line-height: 1.5384616;color: #333;background-color: #fff;border: 1px solid #ddd;outline: 0;" onchange="set_datatable()">';
        $output .= '<option value="" selected>Toko</option>';
        foreach($merchants as $merchant) {
            $output .= '<option value="' . $merchant->id . '">' . $merchant->name . '</option>';
        }
        $output .= '</select>';
        $output .= '</div>';

        echo $output;
    }

    /* UNUSED */
    public function add() {
        $product = $this->main->insert('products', array('is_temp' => 1, 'user_added' => $this->data['user']->id));
        // redirect('catalog/products/form/' . encode($product));
        redirect('catalog/products/form/'. encode($product) .'/'. encode('false'));
    }

    public function form($id, $store) {
        // $id ? $this->aauth->control('catalog/product/edit') : $this->aauth->control('catalog/product/add');
        $id = decode($id);
        $store = decode($store);
        // $this->data['data'] = $this->main->get('products',array('id'=>$id));
        $this->data['brands'] = $this->main->gets('brands', array(), 'name ASC');
        $this->data['categories'] = $this->products->get_categories();
        $this->data['option_groups'] = $this->main->gets('option_group', array(), 'sort_order asc');
        $this->data['merchant_groups'] = $this->main->gets('merchant_groups', array(), 'name asc');
        $this->data['price_levels'] = array();
        // $this->breadcrumbs->unshift(lang('catalog'), '/');
        // $this->breadcrumbs->push(lang('product'), '/catalog/products');

        // if ($id) {
        //     $id = decode($id);
        $this->data['data'] = $this->products->get($id, $store);
        $this->data['data']->package_items = json_decode($this->data['data']->package_items);
        $this->data['data']->promo_data = json_decode($this->data['data']->promo_data);
        $this->data['data']->table_description = json_decode($this->data['data']->table_description);
        $this->data['images'] = $this->main->gets('product_image', array('product' => $id));
        $this->data['features'] = $this->products->get_features_by_category($this->data['data']->category, $this->data['data']->id);
        $this->data['data']->description = tinymce_get_url_files($this->data['data']->description);
        $this->data['product_options'] = $this->products->get_product_options($this->data['data']->id);
        $this->data['price_levels'] = $this->main->gets('product_price_level', array('product' => $id), 'min_qty asc');

        //     $this->breadcrumbs->push(lang('edit_heading'), '/catalog/products/form/' . encode($id));
        //     $this->breadcrumbs->push($this->data['data']->name, '/');
        // } else {
        //     $this->breadcrumbs->push(lang('add_heading'), '/catalog/products/form');
        // }
        $this->data['prices'] = $this->products->get_prices(($id) ? $id : 0);
        // $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->template->_init();
        $this->template->form();
        $this->load->js('https://code.jquery.com/ui/1.10.4/jquery-ui.js');
        $this->load->js(site_url().'../assets/backend/js/modules/catalog/product_form.js');
        $this->output->set_title(lang('heading'));
        $this->load->view('products/form', $this->data);
    }

    public function copy() {
        $this->data['data'] = array();
        $this->data['zonas'] = $this->main->gets('merchant_groups', array());
        $this->template->_init();
        $this->template->form();
        $this->load->js('https://code.jquery.com/ui/1.10.4/jquery-ui.js');
        $this->load->js('../assets/backend/js/modules/catalog/product_form.js');
        $this->output->set_title(lang('heading'));
        $this->load->view('products/product_copy', $this->data);
    }

    public function image() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $data = $this->input->post(null, true);
        switch ($data['act']) {
            case 'upload':
                $id = $this->main->insert('product_image', array('product' => $data['product'], 'image' => $data['image'], 'primary' => $data['primary']));
                echo $id;
                break;
            case 'delete':
                $this->main->delete('product_image', array('id' => $data['id']));
                $this->main->delete('product_option_image', array('product_image' => $data['id']));
                break;
            default:break;
        }
    }

    public function option($act) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        switch ($act) {
            case 'generate':
                $options = $this->input->post('option');
                $product = $this->input->post('id');
                $options = array_filter($options);
                if ($options) {
                    $opt = array();
                    $options = array_values($options);
                    $options = $this->_create_combination($options);
                    $ids = array();
                    foreach ($options as $option) {
                        if (!$this->_check_combination($product, $option)) {
                            $product_option = $this->main->insert('product_option', array('product' => $product));
                            array_push($ids, $product_option);
                            $product_option_combination = array();
                            foreach ($option as $opt) {
                                array_push($product_option_combination, array('product_option' => $product_option, 'option' => $opt));
                            }
                            $this->db->insert_batch('product_option_combination', $product_option_combination);
                        }
                    }
                    if ($ids) {
                        $options = $this->products->get_product_options($product, $ids);
                        echo json_encode(array('status' => 'success', 'data' => $options->result()));
                    }
                } else {
                    $return = array('message' => 'Variasi tidak ada.', 'status' => 'error');
                }
                break;
            case 'get_image':
                $product_option = $this->input->post('product_option');
                $product = $this->input->post('product');
                $images = $this->products->get_product_option_image($product, $product_option);
                echo json_encode($images->result());
                break;
            case 'save_image':
                $product_option = $this->input->post('product_option');
                $this->main->delete('product_option_image', array('product_option' => $product_option));
                $images = $this->input->post('images');
                $images = array_filter($images);
                foreach ($images as $image) {
                    $this->main->insert('product_option_image', array('product_option' => $product_option, 'product_image' => $image));
                }
                break;
            case 'delete':
                $product_option = $this->input->post('product_option');
                $this->main->delete('product_option', array('id' => $product_option));
                $this->main->delete('product_option_combination', array('product_option' => $product_option));
                $this->main->delete('product_option_image', array('product_option' => $product_option));
                break;
            default:break;
        }
    }

    private function _create_combination($options) {
        if (count($options) <= 1) {
            return count($options) ? array_map(create_function('$v', 'return (array($v));'), $options[0]) : $options;
        }
        $res = array();
        $first = array_pop($options);
        foreach ($first as $option) {
            $tab = $this->_create_combination($options);
            foreach ($tab as $to_add) {
                $res[] = is_array($to_add) ? array_merge($to_add, array($option)) : array($to_add, $option);
            }
        }
        return $res;
    }

    private function _check_combination($product, $options) {
        $exists = array();
        $options_exist = $this->db->select('poc.*')
                ->join('product_option po', 'po.id = poc.product_option', 'left')
                ->where('po.product', $product)
                ->get('product_option_combination poc');
        if ($options_exist->num_rows() > 0) {
            foreach ($options_exist->result() as $oe) {
                $exists[$oe->product_option][] = $oe->option;
            }
        } else {
            return false;
        }
        foreach ($exists as $key => $oe) {
            if (count($oe) == count($options)) {
                $diff = false;
                for ($i = 0; isset($oe[$i]); $i++) {
                    if (!in_array($oe[$i], $options) || $key == false) {
                        $diff = true;
                    }
                }
                if (!$diff) {
                    return true;
                }
            }
        }
        return false;
    }

    public function get_feature_values($feature, $id) {
        $feature = $this->main->get('features', array('id' => $feature));
        $products = $this->main->gets('feature_variant', array('feature' => $feature->id));
        if ($feature->type == 'i') {
            $output = '<input type="text" name="feature[' . $id . '][value]" class="form-control">';
        } else {
            if ($products) {
                if ($feature->type == 't') {
                    $output = '<textarea name="feature[' . $id . '][value]" class="form-control">';
                } elseif ($feature->type == 's') {
                    $output = '<select name="feature[' . $id . '][value]" class="form-control">';
                    foreach ($products->result() as $feature) {
                        $output .= '<option value="' . $feature->id . '">' . $feature->value . '</option>';
                    }
                    $output .= '</select>';
                } elseif ($feature->type == 'c') {
                    $output = '';
                    foreach ($products->result() as $feature) {
                        $output .= '<div class="checkbox"><label><input type="checkbox" name="feature[' . $id . '][value][]" value="' . $feature->id . '">' . $feature->value . '</label></div>';
                    }
                }
            } else {
                header('HTTP/1.1 500 Internal Server Error');
                // header('Content-Type: application/json; charset=UTF-8');
                // die(json_encode(array('message' => 'ERROR', 'code' => 1337)));
            }
        }
        echo $output;
    }

    public function get_package_item() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $items = $this->products->get_package_item($this->input->get('query'));
        echo json_encode($items);
    }

    public function get_feature_form() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        if ($category = $this->input->post('category')) {
            $data['product'] = ($this->input->post('product')) ? $this->input->post('product') : 0;
            $data['features'] = $this->products->get_features_by_category($category, $data['product']);
            $this->load->view('products/feature_form', $data);
        } else {
            echo "";
        }
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');

        $code_check = '';
        if ($this->input->post('id')) {
            $code = $this->main->get('products', array('id' => $this->input->post('id')))->code;
            if ($code != $this->input->post('code')) {
                $code_check = '|is_unique[products.code]';
            }
        }
        $this->form_validation->set_rules('code', 'lang:code', 'trim|required|alpha_dash' . $code_check);
        $this->form_validation->set_rules('price', 'lang:price', 'trim|required|numeric');
        $this->form_validation->set_rules('weight', 'lang:weight', 'trim|required|numeric');
        $this->form_validation->set_rules('name', 'lang:name', 'trim|required');
        $this->form_validation->set_rules('short_description', 'lang:short_description', 'trim|required');
        $this->form_validation->set_rules('category', 'lang:category', 'trim|required');
        $this->form_validation->set_rules('seo_url', 'lang:seo_url', 'trim|seo_url');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);
            do {
                $features = array();
                $prices = array();
                $level_prices = array();
                $options = array();
                $package_items = array();
                $table_description = array();
                $promo_data = array();

                if (isset($data['prices'])) {
                    $prices = $data['prices'];
                    unset($data['prices']);
                }
                if (isset($data['price_level'])) {
                    $level_prices = $data['price_level'];
                    unset($data['price_level']);
                }
                if (isset($data['feature'])) {
                    $features = array_filter($data['feature']);
                    unset($data['feature']);
                }
                if (isset($data['options'])) {
                    $options = $data['options'];
                    unset($data['options']);
                }
                if (isset($data['package_items'])) {
                    $data['package_items'] = json_encode($data['package_items']);
                }
                if (isset($data['table_description'])) {
                    $data['table_description'] = json_encode($data['table_description']);
                }
                if (isset($data['promo_data'])) {
                    $data['promo_data']['date_start'] = date('Y-m-d',strtotime($data['promo_data']['date_start']));
                    $data['promo_data']['date_end'] = date('Y-m-d',strtotime($data['promo_data']['date_end']));
                    $data['promo_data'] = json_encode($data['promo_data']);
                }
                if (!$product_images = $this->main->gets('product_image', array('product' => $data['id']))) {
                    $return = array('message' => lang('empty_image_message'), 'status' => 'error');
                    break;
                }
                if (!isset($data['image_primary'])) {
                    $return = array('message' => lang('image_primary_not_set_message'), 'status' => 'error');
                    break;
                }

                if ($this->main->gets('product_option', array('product' => $data['id']))) {
                    if (!isset($data['default_option'])) {
                        $return = array('message' => lang('default_option_not_set_message'), 'status' => 'error');
                        break;
                    }
                    $this->main->update('product_option', array('default' => 0), array('product' => $data['id']));
                    $this->main->update('product_option', array('default' => 1), array('product' => $data['id'], 'id' => $data['default_option']));
                    unset($data['default_option']);
                }

                $url = 'catalog/products/view/' . $data['id'];
                if ($seo_url = $data['seo_url']) {
                    if (check_url($seo_url, $url)) {
                        $return = array('message' => lang('friendly_url_exist_message'), 'status' => 'error');
                        break;
                    }
                } else {
                    $seo_url = $url;
                }
                unset($data['seo_url']);

                $this->main->update('product_image', array('primary' => 0), array('product' => $data['id']));
                $this->main->update('product_image', array('primary' => 1), array('product' => $data['id'], 'id' => $data['image_primary']));
                unset($data['image_primary']);

                $this->main->delete('seo_url', array('query' => $url));
                $this->main->insert('seo_url', array('query' => $url, 'keyword' => $seo_url));

                $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;
                $data['description'] = tinymce_parse_url_files($data['description']);
                $data['price'] = parse_number($data['price']);

                $this->main->delete('product_feature', array('product' => $data['id']));
                $this->main->delete('product_price', array('product' => $data['id']));
                $this->db->where_in('product_price_level', '(SELECT id FROM product_price_level WHERE product = ' . $data['id'] . ')', false)
                        ->delete('product_price_level_groups');
                $this->main->delete('product_price_level', array('product' => $data['id']));
                
                $this->main->update('products', $data, array('id' => $data['id']));

                if (count($options) > 0) {
                    foreach ($options as $id => $option) {
                        $this->main->update('product_option', array('price' => $option['price'], 'weight' => $option['weight']), array('id' => $id));
                    }
                }

                $data_products = array();
                if (count($features) > 0) {
                    foreach ($features as $feature_id => $feature) {
                        if ($feature['value']) {
                            if (is_array($feature['value'])) {
                                if (count($feature['value']) > 0) {
                                    foreach ($feature['value'] as $variant) {
                                        $data_products[] = array(
                                            'product' => $data['id'],
                                            'feature' => $feature['id'],
                                            'value' => $variant
                                        );
                                    }
                                }
                            } else {
                                $data_products[] = array(
                                    'product' => $data['id'],
                                    'feature' => $feature_id,
                                    'value' => $feature['value']
                                );
                            }
                        }
                    }
                    if ($data_products)
                        $this->db->insert_batch('product_feature', $data_products);
                }
                $data_prices = array();
                if (count($prices) > 0) {
                    foreach ($prices as $merchant_group => $price) {
                        if (isset($price['price'])) {
                            $data_prices[] = array(
                                'product' => $data['id'],
                                'merchant_group' => $merchant_group,
                                'value' => 0,
                                'percent' => 0,
                                'percent_value' => 0,
                                'price' => $price['price']
                            );
                        }
                    }
                    if ($data_prices)
                        $this->db->insert_batch('product_price', $data_prices);
                }
                if (count($level_prices) > 0) {
                    foreach ($level_prices as $price) {
                        if (isset($price['qty']) && isset($price['price'])) {
                            $price_level_parent = $price['price'];
                            $price_level = $this->main->insert('product_price_level', array('product' => $data['id'], 'min_qty' => $price['qty'], 'price' => $price['price']));
                            $price_level_groups = array_filter($price['group']);
                            $data_price_level_groups = array();
                            if ($price_level_groups) {
                                foreach ($price_level_groups as $group => $price) {
                                    if (isset($price['price'])) {
                                        $data_price_level_groups[] = array(
                                            'product_price_level' => $price_level,
                                            'merchant_group' => $group,
                                            'value' => 0,
                                            'percent' => 0,
                                            'percent_value' => 0,
                                            'price' => $price['price']
                                        );
                                    }
                                }
                            }
                            if ($data_price_level_groups)
                                $this->db->insert_batch('product_price_level_groups', $data_price_level_groups);
                        }
                    }
                }

                $return = array('message' => sprintf(lang('save_success_message'), $data['name']), 'status' => 'success');
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function copy_save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $check_zona = $this->main->gets('product_price', array('merchant_group' => $data['from_zona']));
        $this->db->select('id');
        $this->db->where('merchant_group',$data['to_zona']);
        $check_zona_to = $this->db->get('product_price');
        $check_price_level = $this->main->gets('product_price_level_groups', array('merchant_group' => $data['from_zona']));
        $this->db->select('id');
        $this->db->where('merchant_group',$data['to_zona']);
        $check_price_level_to = $this->db->get('product_price_level_groups');
        if($check_zona->num_rows() > 0) {
            foreach ($check_zona->result_array() as $price) {
                if (isset($price['value']) || isset($price['percent'])) {
                    $data_prices[] = array(
                        'product' => $price['product'],
                        'merchant_group' => $data['to_zona'],
                        'value' => $price['value'],
                        'percent' => $price['percent'],
                        'percent_value' => $price['percent_value'],
                        'price' => $price['price']
                    );
                }
            }
            if ($check_zona_to->num_rows() > 0) {
                foreach ($check_zona_to->result_array() as $key => $value) {
                    $data_prices[$key]['id'] = $value['id'];
                }
                $this->db->update_batch('product_price', $data_prices, 'id');
            } else {
                $this->db->insert_batch('product_price', $data_prices);
            }
        } 
        if($check_price_level->num_rows() > 0) {
            foreach ($check_price_level->result_array() as $price) {
                if (isset($price['value']) || isset($price['percent'])) {
                    $data_price_level_groups[] = array(
                        'product_price_level' => $price['product_price_level'],
                        'merchant_group' => $data['to_zona'],
                        'value' => $price['value'],
                        'percent' => $price['percent'],
                        'percent_value' => $price['percent_value'],
                        'price' => $price['price']
                    );
                }
            }
            if ($check_price_level_to->num_rows() > 0) {
                foreach ($check_price_level_to->result_array() as $key => $value) {
                    $data_price_level_groups[$key]['id'] = $value['id'];
                }
                $this->db->update_batch('product_price_level_groups', $data_price_level_groups, 'id');
            } else {
                $this->db->insert_batch('product_price_level_groups', $data_price_level_groups);
            }
        } 
        $return = array('message' => 'Copy Zona Produk Berhasil', 'status' => 'success');

        echo json_encode($return);
    }

    public function view($id, $store) {
        $this->load->model('catalog/categories_model', 'categories');

        $this->template->_init();
        $this->load->js('../assets/backend/js/modules/catalog/product_view.js');

        $id = decode($id);
        $store = decode($store);
        $this->data['product'] = $this->products->get($id, $store);
        if($this->data['product']->store_type == 'principal') {
            $seo_url = '(Produk Principal)';
        } else {
            $query = 'catalog/products/view/' . $id . '/' . $store;
            $get_seo_url = $this->main->get('seo_url', ['query' => $query]);
            // var_dump($this->db->last_query());die;
            if($get_seo_url) {
                $seo_url = $get_seo_url->keyword;
            } else {
                $seo_url = '-';
            }
        }
        $this->data['product']->seo_url = $seo_url;
        $this->data['product_images'] = $this->main->gets('product_image', array('product' => $id));
        $this->data['product_features'] = $this->products->get_product_feature_by_category($this->data['product']->category, $id);
        $this->data['brand'] = $this->main->get('brands', array('id' => $this->data['product']->brand));
        $this->data['category'] = $this->categories->get($this->data['product']->category);
        $this->data['merchant'] = $this->main->get('merchants', array('id' => $store));
        // $this->data['merchant'] = $this->main->get('merchants', array('id' => $this->data['product']->merchant));

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('product'), '/catalog/products');
        $this->breadcrumbs->push($this->data['product']->name, '/catalog/products/view/' . encode($id));

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title('Produk ' . $this->data['product']->name);
        $this->load->view('products/product_view', $this->data);
    }

    /* UNUSED */
    public function other() {
        $this->data['menu'] = 'catalog_product_other';

        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('product'), '/catalog/products');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('heading'));
        $this->load->view('products/other/list', $this->data);
    }

    /* UNUSED */
    public function get_list_other() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->products->get_all(-1, $start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    '<a href="' . site_url('catalog/products/view/' . encode($data->id)) . '">' . $data->code . '</a>',
                    $data->name,
                    number($data->price_old),
                    number($data->price) . '<small class="display-block text-muted">Diskon ' . number($data->discount) . '%</small>',
                    number($data->quantity),
                    $data->category,
                    $this->_status_product($data->status),
                    $data->merchant_name,
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">' .
                    (($data->status == 2) ? '<li><a href="' . site_url('catalog/products/activate/' . encode($data->id)) . '" class="activate">' . lang('button_activate') . '</a></li>' : '') .
                    ($this->aauth->is_allowed('catalog/product/edit') ? '<li><a href="' . site_url('catalog/products/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>' : '') .
                    ($this->aauth->is_allowed('catalog/product/delete') ? '<li><a href="' . site_url('catalog/products/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>' : '') .
                    '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->products->count_all();
        $output['recordsFiltered'] = $this->products->count_all($search);
        echo json_encode($output);
    }

    // public function delete($id, $store) {
    //     // $this->input->is_ajax_request() or exit('No direct post submit allowed!');

    //     $id = decode($id);
    //     $store = decode($store);

    //     $data = $this->main->get('products', array('id' => $id));
    //     $delete = $this->main->delete('products_principal_stock', array('branch_id' => $store, 'product_id' => $id));
    //     if ($delete) {
    //         $this->main->delete('seo_url', array('query' => 'catalog/products/view/' . $id . '/' . $store));
    //         $this->db->where_in('product_review', "SELECT id FROM product_review WHERE product = $id AND merchant = $store")->delete('product_review_likedislike');
    //         $this->main->delete('product_review', array('product' => $id, 'merchant' => $store));
    //         $count = $this->main->count('products_principal_stock', array('product_id' => $id, 'branch_id' => $store));
            
    //         if ($count > 0) {
                
    //         } else {
    //             $this->main->delete('product_image', array('product' => $id));
    //             $this->main->delete('product_merchant', array('product' => $id, 'merchant' => $store));
    //             // $this->db->where_in('product_option', 'SELECT id FROM product_option WHERE product = ' . $id, false)->delete('product_option_variant');
    //             $this->main->delete('product_option', array('product' => $id));
    //             $this->main->delete('product_price', array('product' => $id));
    //             $this->main->delete('product_price_level', array('product' => $id));
    //             $this->main->delete('products', array('id' => $id));
    //         }
    //         $return = array('message' => sprintf(lang('delete_success_message'), $data->name), 'status' => 'success');
            
    //     } else {
    //         $return = array('message' => sprintf(lang('delete_error_message'), $data->name), 'status' => 'danger');
    //     }
    //     echo json_encode($return);
    // }

    public function delete($id, $store) {
        // $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $store = decode($store);

        $data = $this->main->get('products', array('id' => $id));
        $delete = $this->main->delete('products', array('store_id' => $store, 'id' => $id));
        if ($delete) {
            $product_branch = $this->main->gets('products_principal_stock', array('product_id' => $id));
            
            if ($product_branch) {
                foreach($product_branch->result() as $product) {
                    $this->main->delete('seo_url', ['query' => 'catalog/products/view/' . $id . '/' . $product->branch_id]);
                    $this->db->where_in('product_review', "SELECT id FROM product_review WHERE product = $id AND merchant = $product->branch_id")->delete('product_review_likedislike');
                    $this->main->delete('product_review', array('product' => $id, 'merchant' => $product->branch_id));
                }
                $this->main->delete('product_image', array('product' => $id));
                $this->main->delete('product_merchant', array('product' => $id, 'merchant' => $store));
                // $this->db->where_in('product_option', 'SELECT id FROM product_option WHERE product = ' . $id, false)->delete('product_option_variant');
                $this->main->delete('product_option', array('product' => $id));
                $this->main->delete('product_price', array('product' => $id));
                $this->main->delete('product_price_level', array('product' => $id));
            } else {
                $this->main->delete('seo_url', array('query' => 'catalog/products/view/' . $id . '/' . $store));
                $this->db->where_in('product_review', "SELECT id FROM product_review WHERE product = $id AND merchant = $store")->delete('product_review_likedislike');
                $this->main->delete('product_review', array('product' => $id, 'merchant' => $store));
                $this->main->delete('product_image', array('product' => $id));
                $this->main->delete('product_merchant', array('product' => $id, 'merchant' => $store));
                // $this->db->where_in('product_option', 'SELECT id FROM product_option WHERE product = ' . $id, false)->delete('product_option_variant');
                $this->main->delete('product_option', array('product' => $id));
                $this->main->delete('product_price', array('product' => $id));
                $this->main->delete('product_price_level', array('product' => $id));
                $this->main->delete('products', array('id' => $id));
            }
            $return = array('message' => sprintf(lang('delete_success_message'), $data->name), 'status' => 'success');
            
        } else {
            $return = array('message' => sprintf(lang('delete_error_message'), $data->name), 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function activate($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('products', array('id' => $id));
        $update = $this->main->update('products', array('status' => 1), array('id' => $id));
        if ($update) {
            $return = array('message' => 'Produk ' . $data->name . ' berhasil di aktifasi.', 'status' => 'success');
        } else {
            $return = array('message' => 'Produk ' . $data->name . ' gagal di aktifasi.', 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function disabled($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('products', array('id' => $id));
        $update = $this->main->update('products', array('status' => 2), array('id' => $id));
        if ($update) {
            $return = array('message' => 'Produk ' . $data->name . ' berhasil di non-aktifkan.', 'status' => 'success');
        } else {
            $return = array('message' => 'Produk ' . $data->name . ' gagal di non-aktifkan.', 'status' => 'danger');
        }
        echo json_encode($return);
    }

    public function get_product_code() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $merchant = $this->main->get('merchants', array('id' => $this->input->post('merchant')));
        echo product_code($merchant->name, $merchant->id);
    }

    private function _status_product($status) {
        switch ($status) {
            case 0: $text = '<i class="icon-cross2 text-danger-400"></i>';
                break;
            case 1: $text = '<i class="icon-checkmark3 text-success"></i>';
                break;
            case 2: $text = '<i class="icon-cross2 text-danger-400"></i>';
        }
        return $text;
    }

    public function get_option($id) {
        $data = $this->main->get('options', array('id' => $id));
        $variants = $this->main->gets('option_variant', array('option' => $data->id))->result();
        echo json_encode(array('option' => $data, 'variants' => $variants));
    }

    // public function export() {
    //     $this->aauth->control('catalog/product/edit');
    //     $spreadsheet = new Spreadsheet();
    //     $spreadsheet->setActiveSheetIndex(0);
    //     $sheet = $spreadsheet->getActiveSheet();
    //     $sheet->fromArray(array('ID', 'Nama', 'Desk. Singkat', 'Kode', 'Dimensi (PxLxT)', 'Berat (gram)', 'Harga Normal', 'Harga Zona', 'Harga Level'), NULL, 'A1');
    //     $data = $this->input->get(null, true);
    //     $order = explode(',', $data['order']);
    //     $products = $this->products->export($data['start'], $data['length'], $data['search'], array('column' => $order[0], 'dir' => $order[1]));
    //     $i = 2;
    //     if ($products->num_rows() > 0) {
    //         foreach ($products->result() as $product) {
    //             $row_data = array(
    //                 $product->id,
    //                 $product->name,
    //                 $product->short_description,
    //                 $product->code,
    //                 $product->length . 'x' . $product->width . 'x' . $product->height,
    //                 $product->weight,
    //                 $product->price,
    //             );
    //             $row_price_zone = '';
    //             $price_zone = $this->db->select('mg.id, mg.name, pp.value, pp.percent')->join('product_price pp', 'pp.merchant_group = mg.id AND pp.product = ' . $product->id, 'left')->get('merchant_groups mg');
    //             if ($price_zone->num_rows() > 0) {
    //                 foreach ($price_zone->result() as $pz) {
    //                     $row_price_zone .= $pz->id . '-' . $pz->name . '=F:' . $pz->value . ',P:' . $pz->percent . ';';
    //                 }
    //             }
    //             $row_data[] = $row_price_zone;
    //             $row_price_level = '';
    //             $price_level = $this->main->gets('product_price_level', array('product' => $product->id), 'min_qty asc');
    //             if ($price_level) {
    //                 foreach ($price_level->result() as $pl) {
    //                     $row_price_level .= $pl->min_qty . '=' . $pl->price;
    //                     $groups = $this->db->select('mg.id, mg.name, pplg.product_price_level, IFNULL(pplg.value,0) value, IFNULL(pplg.percent,0) percent')->join('product_price_level_groups pplg', 'pplg.merchant_group = mg.id AND pplg.product_price_level = ' . $pl->id, 'left')->order_by('mg.name')->get('merchant_groups mg');
    //                     if ($groups->num_rows() > 0) {
    //                         $row_price_level .= '=>';
    //                         foreach ($groups->result() as $group) {
    //                             $row_price_level .= $group->id . '-' . $group->name . '=F:' . number_format($group->value, 0, '', '') . ',P:' . (($this->is_decimal($group->percent)) ? number_format($group->percent, 1, '.', '') : 0) . ';';
    //                         }
    //                     }
    //                     $row_price_level .= ';';
    //                 }
    //             }
    //             $row_data[] = $row_price_level;
    //             $sheet->fromArray($row_data, null, 'A' . $i);
    //             $i++;
    //         }
    //     }
    //     header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    //     header('Content-Disposition: attachment;filename="Produk Tokomanamana.xlsx"');
    //     header('Cache-Control: max-age=0');
    //     header('Cache-Control: max-age=1');
    //     header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    //     header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    //     header('Cache-Control: cache, must-revalidate');
    //     header('Pragma: public');
    //     $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
    //     $writer->save('php://output');
    //     exit;
    // }

    public function export() {
        $this->aauth->control('catalog/product/edit');
        $filename = "product_stock";
        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0);
        $sheet = $spreadsheet->getActiveSheet();
        \PhpOffice\PhpSpreadsheet\Shared\File::setUseUploadTempDirectory(true);
        $sheet->fromArray(array('ID', 'Nama', 'Kategori', 'Desk. Singkat', 'Kode', 'Dimensi (PxLxT)', 'Berat (gram)', 'Toko', 'Tipe Toko', 'Harga Normal', 'Harga Level'), NULL, 'A1');
        $data = $this->input->get(null, true);
        $order = explode(',', $data['order']);
        $fullFileName = "${filename}_".date('Y-m-d');
        $products = $this->products->export($data['start'], $data['length'], $data['search'], array('column' => $order[0], 'dir' => $order[1]), $data['category'], $data['merchant']);
        $i = 2;
        if ($products->num_rows() > 0) {
            foreach ($products->result() as $product) {
                $row_data = array(
                    $product->id,
                    $product->name,
                    $product->category,
                    $product->short_description,
                    $product->code,
                    $product->length . 'x' . $product->width . 'x' . $product->height,
                    $product->weight,
                    $product->store_name,
                    $product->store_type,
                    $product->price,
                );
                $row_price_level = '';
                $price_level = $this->main->gets('product_price_level', array('product' => $product->id), 'min_qty asc');
                if ($price_level) {
                    foreach ($price_level->result() as $pl) {
                        $row_price_level .= $pl->min_qty . '=' . $pl->price;
                        $row_price_level .= ';';
                    }
                }
                $row_data[] = $row_price_level;
                $sheet->fromArray($row_data, null, 'A' . $i);
                $i++;
            }
        };
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fullFileName . '.xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }

    /* UNUSED */
    public function import() {
        $this->aauth->control('catalog/product/edit');
        $res = array('status' => 'error', 'message' => 'File tidak ada');
        if (isset($_FILES['file']['name'])) {
            $this->load->library('upload');
            $config['upload_path'] = '../files/import/';
            $config['allowed_types'] = 'xlsx';
            $config['overwrite'] = TRUE;

            $this->upload->initialize($config);
            if ($this->upload->do_upload('file')) {
                $file = $this->upload->data();
                $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load('../files/import/' . $file['file_name']);
                $sheet = $spreadsheet->getSheet(0);
                $datas = array();
                for ($row = 2; $row <= $sheet->getHighestRow(); $row++) {
                    $data = $sheet->rangeToArray('A' . $row . ':' . $sheet->getHighestColumn() . $row, NULL, TRUE, FALSE);
                    // array_push($datas, $data);
                    $dimention = array_filter(explode('x', $data[0][4]));
                    $id = $data[0][0];
                    $product = array(
                        'name' => $data[0][1],
                        'short_description' => $data[0][2],
                        'code' => $data[0][3],
                        'length' => isset($dimention[0]) ? $dimention[0] : 0,
                        'width' => isset($dimention[1]) ? $dimention[1] : 0,
                        'height' => isset($dimention[2]) ? $dimention[2] : 0,
                        'weight' => $data[0][5],
                        'price' => $data[0][6]
                    );
                    $this->main->update('products', $product, array('id' => $id));
                    $price_zone = $data[0][7];
                    $price_zone = ($price_zone) ? array_filter(explode(';', $price_zone)) : array();
                    $price_level = $data[0][8];
                    $price_levels = ($price_level) ? array_filter(explode(';;', $price_level)) : array();
                    $this->db->delete('product_price', array('product' => $id));
                    $this->db->where_in('product_price_level', '(SELECT id FROM product_price_level WHERE product = ' . $id . ')', false)
                            ->delete('product_price_level_groups');
                    $this->db->delete('product_price_level', array('product' => $id));

                    $data_price_zone = array();
                    if (count($price_zone) > 0) {
                        foreach ($price_zone as $price) {
                            $price = array_filter(explode('=', $price));
                            if ($price) {
                                $group = array_filter(explode('-', $price[0]));
                                $price = array_filter(explode(',', isset($price[1]) ? $price[1] : ''));
                                if ($group && isset($price)) {
                                    $data_price = array(
                                        'product' => $id,
                                        'merchant_group' => $group[0],
                                        'value' => 0,
                                        'percent' => 0,
                                        'percent_value' => 0,
                                    );
                                    foreach ($price as $p) {
                                        $p = explode(':', $p);
                                        if ($p[0] == 'F') {
                                            $data_price['value'] = ($p[1]) ? $p[1] : 0;
                                        }
                                        if ($p[0] == 'P') {
                                            $data_price['percent'] = ($p[1]) ? $p[1] : 0;
                                            $data_price['percent_value'] = ($p[1]) ? $product['price'] * $p[1] / 100 : 0;
                                        }
                                    }
                                    $data_price['price'] = $product['price'] + $data_price['value'] + $data_price['percent_value'];
                                    $data_price_zone[] = $data_price;
                                }
                            }
                        }
                        if ($data_price_zone)
                            $this->db->insert_batch('product_price', $data_price_zone);
                    }

                    $data_price_level = array();
                    if (count($price_levels) > 0) {
                        foreach ($price_levels as $price) {
                            $price = explode('=>', $price);
                            $qty = explode('=', $price[0]);
                            $price_parent = $qty[1];
                            $qty = $qty[0];
                            $price_level = $this->main->insert('product_price_level', array('product' => $id, 'min_qty' => $qty, 'price' => $price_parent));
                            $group_price = array_filter(explode(';', $price[1]));
                            // print_r($group_price);
                            $data_price = array();
                            if ($group_price) {
                                foreach ($group_price as $price) {
                                    // print_r($price);
                                    // echo '<br>';
                                    $price = explode('=', $price);
                                    $merchant_group = explode('-', $price[0]);
                                    $price = explode(',', $price[1]);
                                    $data_price = array(
                                        'product_price_level' => $price_level,
                                        'merchant_group' => $merchant_group[0],
                                        'value' => 0,
                                        'percent' => 0,
                                        'percent_value' => 0,
                                        'price' => $price_parent
                                    );
                                    foreach ($price as $p) {
                                        $p = explode(':', $p);
                                        if ($p[0] == 'F') {
                                            $data_price['value'] = ($p[1]) ? $p[1] : 0;
                                        }
                                        if ($p[0] == 'P') {
                                            $data_price['percent'] = ($p[1]) ? $p[1] : 0;
                                            $data_price['percent_value'] = ($p[1]) ? $price_parent * $p[1] / 100 : 0;
                                        }
                                    }
                                    $data_price['price'] = $price_parent + $data_price['value'] + $data_price['percent_value'];
                                    $this->main->insert('product_price_level_groups', $data_price);
                                }
                            }
                        }
                    }
                }
                $res = array('status' => 'success', 'message' => 'Import berhasil disimpan');
            } else {
                $res['message'] = $this->upload->display_errors();
            }
        }
        echo json_encode($res);
    }

    private function is_decimal($val) {
        return is_numeric($val) && floor($val) != $val;
    }

    public function get_table_description(){
        $id = $this->input->post('id');
        $data = $this->products->get_table_deskripsi($id)->result();
        echo json_encode($data);
    }

}
