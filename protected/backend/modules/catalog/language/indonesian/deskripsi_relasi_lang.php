<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['deskripsi_relasi_heading'] = 'Deskripsi Relasi';
$lang['deskripsi_relasi_sub_heading'] = 'Kelola Data Deskripsi Relasi';
$lang['deskripsi_relasi_list_heading'] = 'Daftar Deskripsi Relasi';
$lang['deskripsi_relasi_add_heading'] = 'Tambah Deskripsi Relasi';
$lang['deskripsi_relasi_edit_heading'] = 'Edit Deskripsi Relasi';

$lang['deskripsi_relasi_image_th'] = 'Gambar';
$lang['deskripsi_relasi_name_th'] = 'Param Deskripsi';
$lang['deskripsi_relasi_kategori_th'] = 'Kategori';
$lang['deskripsi_relasi_count_product_th'] = 'Jumlah Produk';
$lang['deskripsi_relasi_sort_order_th'] = 'Urutan';

$lang['deskripsi_relasi_form_general_tabs'] = 'Umum';
$lang['deskripsi_relasi_form_feature_tabs'] = 'Fitur';
$lang['deskripsi_relasi_form_seo_tabs'] = 'SEO';
$lang['deskripsi_relasi_form_home_tabs'] = 'Home';

$lang['deskripsi_relasi_form_name_label'] = 'Kategori';
$lang['deskripsi_relasi_form_image_label'] = 'Gambar';
$lang['deskripsi_relasi_form_icon_label'] = 'Ikon';
$lang['deskripsi_relasi_form_description_label'] = 'Deskripsi Relasi';
$lang['deskripsi_relasi_form_parent_label'] = 'Kategori';
$lang['deskripsi_relasi_form_param_label'] = 'Param Deskripsi';
$lang['deskripsi_relasi_form_meta_title_label'] = 'Meta Judul';
$lang['deskripsi_relasi_form_meta_description_label'] = 'Meta Deskripsi Relasi';
$lang['deskripsi_relasi_form_meta_keyword_label'] = 'Meta Keyword';

$lang['deskripsi_relasi_form_name_placeholder'] = 'Masukkan nama Deskripsi Relasi';
$lang['deskripsi_relasi_form_description_placeholder'] = 'Masukkan Deskripsi Relasi dari Deskripsi Relasi ini. Deskripsi Relasi akan ditampilkan di halaman Deskripsi Relasi';

$lang['deskripsi_relasi_form_add_image_button'] = 'Tambah Gambar';
$lang['deskripsi_relasi_form_edit_image_button'] = 'Ubah Gambar';
$lang['deskripsi_relasi_form_delete_image_button'] = 'Hapus Gambar';

$lang['deskripsi_relasi_save_success_message'] = "Deskripsi Relasi '%s' berhasil disimpan.";
$lang['deskripsi_relasi_save_error_message'] = "Deskripsi Relasi '%s' gagal disimpan.";
$lang['deskripsi_relasi_delete_success_message'] = "Deskripsi Relasi '%s' berhasil dihapus.";
$lang['deskripsi_relasi_delete_error_message'] = "Deskripsi Relasi '%s' gagal dihapus.";
$lang['deskripsi_relasi_delete_exist_child_message'] = "Deskripsi Relasi '%s' tidak dapat dihapus. Silahkan hapus dahulu Deskripsi Relasi yang berada di Deskripsi Relasi ini.";
$lang['deskripsi_relasi_delete_exist_product_message'] = "Deskripsi Relasi '%s' tidak dapat dihapus. Silahkan hapus dahulu produk yang berada di Deskripsi Relasi ini.";
$lang['deskripsi_relasi_list_nothing_message'] = "Deskripsi Relasi belum ada.";