<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['deskripsi_heading'] = 'Deskripsi';
$lang['deskripsi_sub_heading'] = 'Kelola Data Deskripsi';
$lang['deskripsi_list_heading'] = 'Daftar Deskripsi';
$lang['deskripsi_add_heading'] = 'Tambah Deskripsi';
$lang['deskripsi_edit_heading'] = 'Edit Deskripsi';

$lang['deskripsi_image_th'] = 'Gambar';
$lang['deskripsi_name_th'] = 'Nama';
$lang['deskripsi_count_product_th'] = 'Jumlah Produk';
$lang['deskripsi_sort_order_th'] = 'Urutan';

$lang['deskripsi_form_general_tabs'] = 'Umum';
$lang['deskripsi_form_feature_tabs'] = 'Fitur';
$lang['deskripsi_form_seo_tabs'] = 'SEO';
$lang['deskripsi_form_home_tabs'] = 'Home';

$lang['deskripsi_form_name_label'] = 'Nama';
$lang['deskripsi_form_image_label'] = 'Gambar';
$lang['deskripsi_form_icon_label'] = 'Ikon';
$lang['deskripsi_form_description_label'] = 'Deskripsi';
$lang['deskripsi_form_parent_label'] = 'Deskripsi Utama';
$lang['deskripsi_form_meta_title_label'] = 'Meta Judul';
$lang['deskripsi_form_meta_description_label'] = 'Meta Deskripsi';
$lang['deskripsi_form_meta_keyword_label'] = 'Meta Keyword';

$lang['deskripsi_form_name_placeholder'] = 'Masukkan nama Deskripsi';
$lang['deskripsi_form_description_placeholder'] = 'Masukkan deskripsi dari Deskripsi ini. Deskripsi akan ditampilkan di halaman Deskripsi';

$lang['deskripsi_form_add_image_button'] = 'Tambah Gambar';
$lang['deskripsi_form_edit_image_button'] = 'Ubah Gambar';
$lang['deskripsi_form_delete_image_button'] = 'Hapus Gambar';

$lang['deskripsi_save_success_message'] = "Deskripsi '%s' berhasil disimpan.";
$lang['deskripsi_save_error_message'] = "Deskripsi '%s' gagal disimpan.";
$lang['deskripsi_delete_success_message'] = "Deskripsi '%s' berhasil dihapus.";
$lang['deskripsi_delete_error_message'] = "Deskripsi '%s' gagal dihapus.";
$lang['deskripsi_delete_exist_child_message'] = "Deskripsi '%s' tidak dapat dihapus. Silahkan hapus dahulu Deskripsi yang berada di Deskripsi ini.";
$lang['deskripsi_delete_exist_product_message'] = "Deskripsi '%s' tidak dapat dihapus. Silahkan hapus dahulu produk yang berada di Deskripsi ini.";
$lang['deskripsi_list_nothing_message'] = "Deskripsi belum ada.";