<div bgcolor="#FFFFFF" style="font-family:'Helvetica Neue','Helvetica',Helvetica,Arial,sans-serif;width:100%!important;height:100%;font-size:14px;color:#404040;margin:0;padding:0">
    <table style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0" bgcolor="transparent">
        <tbody>
            <tr style="margin:0;padding:0">
                <td style="margin:0;padding:0"></td>
                <td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0" bgcolor="#FFFFFF">
                    <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;border:1px solid #e7e7e7">
                        <table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:20px;" bgcolor="transparent">
                            <tbody>
                                <tr style="margin:0;padding:0">
                                    <td>
                                        <img src="<?php echo site_url('../assets/frontend/images/logo.png'); ?>" alt="" style="margin:10px 0; height: 50px;">
                                    </td>
                                </tr>

                                <tr style="margin:0;padding:0">
                                    <td style="margin:0;padding:0">
                                        <h5 style="line-height:32px;color:#666;font-weight:700;font-size:20px;margin:0 0 20px;padding:0">Konfirmasi Pendaftaran Principal</h5>
                                        <p style="font-weight:normal;color:#999;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0">
                                            Hai <?php echo $name; ?>,<br>
                                            Terimakasih atas kepercayaannya mendaftarkan diri menjadi principal di <?php echo settings('store_name'); ?>.<br>
                                            <!-- Anda kini telah terdaftar di sistem kami, namun akun Anda <b>belum aktif</b> dan akan segera di verifikasi oleh tim kami. -->
                                            Akun anda telah terdaftar di sistem kami, namun anda harus mengkonfirmasi dengan mengklik tombol dibawah agar akun anda aktif.
                                            <br>
                                            <br />
                                            <br>
                                            Harap konfirmasi sebelum <b><?php echo $due_date_confirmation ?></b>
                                            <br>
                                            Terimakasih.
                                            <br />
                                            <br />
                                            <br />
                                            <span style="display: block; text-align: center"><a href="<?php echo site_url('../principal/auth/email_confirm/' . $token); ?>" target="_blank" style="background-color: #97C23C; padding: 20px; text-decoration: none; color: #fff; font-weight: bold;border-radius:5px;">Konfirmasi</a></span>
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div style="padding:0 20px">
                            <p style="font-size:12px;color:#999;padding:24px 0 10px;margin:0;border-top: 1px solid #E0E0E0;">
                                Email dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.
                            </p>
                        </div>
                    </div>
                </td>
                <td style="margin:0;padding:0"></td>
            </tr>
        </tbody>
    </table>
</div>