<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('view_principle') ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('principle'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('principle_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('principle/save'); ?>" method="post" class="form-horizontal" id="form">
                    <input type="hidden" name="id" value="<?php echo set_value('id', ($data) ? encode($data->id) : ''); ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#general" data-toggle="tab"><?php echo lang('principle_form_general_tabs'); ?></a></li>
                                    <li><a href="#seo" data-toggle="tab"><?php echo lang('principle_form_seo_tabs'); ?></a></li>
                                    <li><a href="#auth" data-toggle="tab"><?php echo lang('principle_form_auth_tabs'); ?></a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="general">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="name"><?php echo lang('principle_form_name_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" required="" name="name" value="<?php echo ($data) ? $data->name : ''; ?>" onkeyup="convertToSlug(this.value);">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('principle_form_image_label'); ?></label>
                                            <?php if ($data && ($data->image)) { ?>
                                                <div class="col-md-3" id="image-preview">
                                                    <div class="thumbnail">
                                                        <div class="thumb">
                                                            <img src="<?php echo site_url('../files/images/' . $data->image); ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="col-md-6" id="add-image">
                                                <button type="button" id="add-image-btn" class="btn btn-default" data-toggle="modal" data-target="#filemanager" <?php echo ($data && $data->image) ? 'style="display:none;"' : ''; ?>><?php echo lang('button_add_image'); ?></button>
                                                <button type="button" id="edit-image-btn" class="btn btn-default" data-toggle="modal" data-target="#filemanager" <?php echo (!$data || ($data && !$data->image)) ? 'style="display:none;"' : ''; ?>><?php echo lang('button_edit_image'); ?></button>
                                                <button type="button" id="delete-image-btn" class="btn btn-default" <?php echo (!$data || ($data && !$data->image)) ? 'style="display:none;"' : ''; ?>><?php echo lang('button_delete_image'); ?></button>
                                                <input type="hidden" id="image" name="image" value="<?php echo ($data) ? $data->image : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('principle_form_description_label'); ?></label>
                                            <div class="col-md-9">
                                                <textarea cols="30" rows="2" class="form-control tinymce" name="description"><?php echo ($data) ? $data->description : ''; ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane " id="seo">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('principle_form_seo_url_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" id="seo_url" name="seo_url" value="<?php echo ($data) ? $data->seo_url : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('principle_form_meta_title_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="meta_title" class="form-control" value="<?php echo ($data) ? $data->meta_title : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('principle_form_meta_description_label'); ?></label>
                                            <div class="col-md-9">
                                                <textarea type="text" name="meta_description" class="form-control"><?php echo ($data) ? $data->meta_description : ''; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('principle_form_meta_keyword_label'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" name="meta_keyword" class="tags-input" value="<?php echo ($data) ? $data->meta_keyword : ''; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane " id="auth">

                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="fullname"><?php echo lang('owner_name'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" required="" name="fullname" value="<?php echo ($data) ? $data->fullname : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="email"><?php echo lang('email'); ?></label>
                                            <div class="col-md-9">
                                                <input type="email" class="form-control" <?php echo (!$data) ? 'required' : 'disabled' ?> name="email" value="<?php echo ($data) ? $data->email : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label"><?php echo lang('phone'); ?></label>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="phone" value="<?php echo ($data) ? $data->phone : ''; ?>">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label" for="password"><?php echo lang('password'); ?></label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" <?php echo (!$data) ? 'required' : '' ?> name="password">
                                                <?php if ($data) { ?>
                                                    <span class="help-block"><?php echo lang('password_update_help'); ?></span>
                                                <?php } ?>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('principle'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="filemanager" class="modal" data-backdrop="false">
    <div class="modal-dialog modal-full">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">File Manager</h5>
            </div>

            <div class="modal-body">
                <iframe width="100%" height="550" frameborder="0" src="<?php echo base_url('filemanager/dialog.php?type=1&editor=false&field_id=image&relative_url=1'); ?>"></iframe>
            </div>
        </div>
    </div>
</div>