<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Request_model extends CI_Model
{

    function get($id)
    {
        $this->db->select('b.*,pv.label as sku_label,pv1.label as revenue_label,pv2.label as price_label,pv3.label as from_label')
            ->join('principle_variable pv', 'pv.id = b.sku_total', 'left')
            ->join('principle_variable pv1', 'pv1.id = b.revenue', 'left')
            ->join('principle_variable pv2', 'pv2.id = b.range_price', 'left')
            ->join('principle_variable pv3', 'pv3.id = b.know_from', 'left')
            ->where('b.id', $id);
        $query = $this->db->get('principle_register b');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_all($start = 0, $length, $search = '', $order = array())
    {
        $this->where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('b.*')
            ->limit($length, $start);

        return $this->db->get('principle_register b');
    }

    function get_alias_key($key)
    {
        switch ($key) {
            case 0:
                $key = 'company_name';
                break;
            case 1:
                $key = 'owner_name';
                break;
            case 2:
                $key = 'owner_phone';
                break;
            case 3:
                $key = 'status';
                break;
        }
        return $key;
    }

    function count_all($search = '')
    {
        $this->where_like($search);
        return $this->db->count_all_results('principle_register b');
    }

    function where_like($search = '')
    {
        $columns = array('company_name', 'owner_name', 'owner_phone');
        if ($search) {
            foreach ($columns as $column) {
                $this->db->like('IFNULL(' . $column . ',"")', $search);
            }
        }
    }
}
