<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Principle extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->aauth->control('principle');

        $this->lang->load('principle', settings('language'));
        $this->load->model('principle_model', 'principle');
        
        $this->load->library('aauth');
        $this->config->load('aauth', TRUE);
        $v = $this->config->item("aauth");
        $v['users'] = 'aauthp_users';
        $v['groups'] = 'aauthp_groups';
        $v['group_to_group'] = 'aauthp_group_to_group';
        $v['user_to_group'] = 'aauthp_user_to_group';
        $v['perms'] = 'aauthp_perms';
        $v['perm_to_group'] = 'aauthp_perm_to_group';
        $v['perm_to_user'] = 'aauthp_perm_to_user';
        $v['pms'] = 'aauthp_pms';
        $v['user_variables'] = 'aauthp_user_variables';
        $v['login_attempts'] = 'aauthp_login_attempts';
        //$this->config->set_item("aauth", $v);
        $this->data['v'] = $v;
        $this->aauth->change_config($this->data['v']);
        $this->data['menu'] = 'principle_list';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('principle'), '/');
        $this->breadcrumbs->push(lang('principle'), '/');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('principle_heading'));
        $this->load->view('principle/list', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->principle->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    ($data->image) ? '<a href="#"><img src="' . site_url('../files/images/' . $data->image) . '" class="img-circle img-xs"></a>' : '',
                    $data->name,
                    $data->seo_url,
                    '<td class="text-center">
                    <ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="' . site_url('principle/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>
                    <li><a href="' . site_url('principle/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>
                    </ul>
                    </li>
                    </ul>
                    </td>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->principle->count_all();
        $output['recordsFiltered'] = $this->principle->count_all($search);
        echo json_encode($output);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->load->js('../assets/backend/js/modules/principle/form.js');

        $this->data['data'] = array();
        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('principle'), '/principle');

        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->principle->get($id);
            $this->data['data']->description = tinymce_get_url_files($this->data['data']->description);
            $this->breadcrumbs->push(lang('principle_edit_heading'), '/principle/form/' . encode($id));
            $this->breadcrumbs->push($this->data['data']->name, '/');
        } else {
            $this->breadcrumbs->push(lang('principle_add_heading'), '/principle/form');
        }
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(($this->data['data']) ? lang('principle_edit_heading') : lang('principle_add_heading'));
        $this->load->view('principle/form', $this->data);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'lang:principle_form_name_label', 'trim|required');
        $this->form_validation->set_rules('seo_url', 'lang:principle_form_seo_url_label', 'trim|alpha_dash');
        
        if (!$this->input->post('id')) {
            $this->form_validation->set_rules('email', 'lang:email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'lang:password', 'trim|required');
        }

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            do {
                if ($data['id'])
                    $data['id'] = decode($data['id']);
                
                $data['description'] = tinymce_parse_url_files($data['description']);
                
                $url = 'principle/view/' . $data['id'];
                if ($seo_url = $data['seo_url']) {
                    if (check_url($seo_url, $url)) {
                        $return = array('message' => lang('principle_friendly_url_exist_message'), 'status' => 'error');
                        break;
                    }
                } else {
                    $seo_url = $url;
                }
                unset($data['seo_url']);

                if (!$data['id']) {

                    $user = $this->aauth->create_user($data['email'], $data['password'], $data['fullname'], 1 ,$data['phone']);
                    //var_dump($user);exit();
                    if ($user) {
                        unset($data['email']);
                        unset($data['password']);
                        unset($data['fullname']);
                        unset($data['phone']);
                        $data['auth'] = $user;
                        //$save = $this->main->insert('merchants', $data);
//                        $this->main->insert('seo_url', array('keyword' => $data['username'], 'query' => 'merchants/view/' . $this->db->insert_id()));
                    } else {
                        $return = array('message' => $this->aauth->print_errors(), 'status' => 'error');
                        break;
                    }
                    $id = $this->main->insert('principles', $data);
                    $url .= $id;
                } else {
                    // $principle = $this->main->get('principles', array('id' => $data['id']));
                    // $auth = $principle->auth;
                    // $data_auth = array('fullname' => $data['fullname'],'phone' => $data['phone']);
                    // if (isset($data['password'])) {
                    //     $data_auth['password'] = $data['password'];
                    //     unset($data['password']);
                    // }
                    // $this->aauth->update_user($data['id'], FALSE, FALSE, $data['fullname'], 1 ,$data['phone']);
                    // //$this->ion_auth->update($auth, $data_auth);
                    // unset($data['fullname']);
                    // unset($data['phone']);
                    // $save = $this->main->update('principles', $data, array('id' => $data['id']));
                    // //$save = $this->main->update('principle', $data, array('id' => $data['id']));
                    // $this->main->delete('seo_url', array('query' => $url));
                }
                
                $this->main->insert('seo_url', array('query' => $url, 'keyword' => $seo_url));

                $return = array('message' => sprintf(lang('principle_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('principle'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('principles', array('id' => $id));
        $delete = $this->main->delete('principles', array('id' => $id));
        $this->main->delete($this->data['v']['users'], array('id' => $data->auth));
        $this->main->delete($this->data['v']['user_to_group'], array('user_id' => $data->auth));
        if ($delete) {
            $this->main->delete('seo_url', array('query' => 'principle/view/' . $data->id));
            $return = array('message' => sprintf(lang('principle_delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => lang('principle_delete_error_message'), 'status' => 'error');
        }

        echo json_encode($return);
    }

}
