<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Request extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->aauth->control('principle/request');

        $this->lang->load('principle', settings('language'));
        $this->load->model('request_model', 'principle');

        $this->data['menu'] = 'principle_request';
    }

    public function index()
    {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('principle'), '/');
        $this->breadcrumbs->push(lang('principle'), '/');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $status = isset($this->input->get()['status']) ? $this->input->get()['status'] : NULL;
        if ($status != null) {
            $this->data['url_ajax'] = 'request/get_list?status=' . $status;
        } else {
            $this->data['url_ajax'] = 'request/get_list';
        }

        $this->output->set_title(lang('principle_heading'));
        $this->load->view('request/list', $this->data);
    }

    public function get_list()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $status = isset($this->input->get()['status']) ? $this->input->get()['status'] : NULL;
        if ($status != NULL) {
            $this->db->where('b.status', $status);
        }
        $datas = $this->principle->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->company_name,
                    $data->owner_name,
                    $data->owner_phone,
                    $this->status_($data->status),
                    '<td class="text-center">
                    <ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="' . site_url('principle/request/view/' . encode($data->id)) . '">' . lang('button_view') . '</a></li>
                    </ul>
                    </li>
                    </ul>
                    </td>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->principle->count_all();
        $output['recordsFiltered'] = $this->principle->count_all($search);
        echo json_encode($output);
    }

    // public function view($id = '') {
    //     $this->template->_init();
    //     $this->template->form();

    //     $this->load->js('../assets/backend/js/modules/principle/form.js');

    //     $this->data['data'] = array();
    //     $this->breadcrumbs->unshift(lang('catalog'), '/');
    //     $this->breadcrumbs->push(lang('principle'), '/principle');

    //     if ($id) {
    //         $id = decode($id);
    //         $this->data['data'] = $this->principle->get($id);
    //         //$this->data['data']->description = tinymce_get_url_files($this->data['data']->description);
    //         //$this->breadcrumbs->push(lang('principle_edit_heading'), '/principle/form/' . encode($id));
    //         //$this->breadcrumbs->push($this->data['data']->name, '/');
    //     } else {
    //         //$this->breadcrumbs->push(lang('principle_add_heading'), '/principle/form');
    //     }
    //     $this->data['breadcrumbs'] = $this->breadcrumbs->show();

    //     $this->output->set_title(($this->data['data']) ? lang('principle_edit_heading') : lang('principle_add_heading'));
    //     $this->load->view('request/view', $this->data);
    // }

    private function status_($status)
    {
        if ($status == 0) {
            $return = '<span class="label label-info">Mengajukan</span>';
        } elseif ($status == 1) {
            $return = '<span class="label label-success">Diterima</span>';
        } elseif ($status == 3) {
            $return = '<span class="label label-warning">Menunggu Konfirmasi</span>';
        } else {
            $return = '<span class="label label-danger">Ditolak</span>';
        }
        return $return;
    }

    // NEWMETHOD
    public function view($id = '')
    {
        $this->template->_init();
        $this->template->form();

        $this->load->js('../assets/backend/js/modules/principle/form.js');
        $this->load->js('../assets/backend/js/lightbox.min.js');
        $this->load->css('../assets/backend/css/lightbox.min.css');

        $this->data['data'] = array();
        $this->breadcrumbs->unshift(lang('catalog'), '/');
        $this->breadcrumbs->push(lang('principle'), '/principle');

        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->principle->get($id);
            if (!$this->data['data']) {
                redirect('principle/request');
            }
            //$this->data['data']->description = tinymce_get_url_files($this->data['data']->description);
            //$this->breadcrumbs->push(lang('principle_edit_heading'), '/principle/form/' . encode($id));
            //$this->breadcrumbs->push($this->data['data']->name, '/');
        } else {
            redirect('principle/request');
        }
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('view_principle'));
        $this->load->view('request/view', $this->data);
    }

    private function send_email($id)
    {
        $email = $this->main->get('cron_job', ['id' => $id, 'type' => 'email']);
        if ($email) {
            $email = json_decode($email->content);
            send_mail($email->from, $email->from_name, $email->to, $email->subject, $email->message);
        }
    }

    public function approved()
    {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $id = $this->input->post('id');
        $check = $this->main->get('principle_register', ['id' => $id]);

        if ($check) {

            $principle_email = $check->owner_email;
            $principle_company_name = $check->company_name;
            $owner_name = $check->owner_name;
            $token = bin2hex(random_bytes(32));
            $due_date_confirmation = date('Y-m-d H:i:s', strtotime('+1 day'));
            $this->data = [
                'site_name' => 'Tokomanamana',
                'company_name' => $principle_company_name,
                'token' => $token,
                'name' => $owner_name,
                'email' => $principle_email,
                'due_date_confirmation' => $due_date_confirmation
            ];
            $message = $this->load->view('email/register_confirmation', $this->data, true);

            $cronjob = [
                'from' => settings('send_email_from'),
                'from_name' => settings('store_name'),
                'to' => $principle_email,
                'subject' => 'Konfirmasi Pendaftaran Principal ' . settings('store_name'),
                'message' => $message
            ];
            $success = $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));

            if ($success) {
                // $this->send_email($success);
                $this->main->update('principle_register', ['token_confirm' => $token, 'token_confirm_status' => 1, 'status' => 3, 'due_date_confirmation' => $due_date_confirmation], ['id' => $id]);
                $return = [
                    'status' => 'success',
                    'message' => 'Email konfirmasi berhasil dikirim!'
                ];
            } else {
                $return = [
                    'status' => 'error',
                    'message' => 'Terjadi suatu kesalahan!'
                ];
            }
        } else {
            $return = [
                'status' => 'error',
                'message' => 'Principal tersebut tidak ada!'
            ];
        }

        echo json_encode($return);
    }

    public function rejected_principal() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $data = $this->input->post(null, true);
        $id = decode($data['id']);

        $principle_register = $this->main->get('principle_register', ['id' => $id]);
        if($principle_register) {
            if(isset($data['documents_not_complete']) || isset($data['documents_not_valid'])) {
                $not_complete = false;
                $not_valid = false;
                $documents_not_valid = [];
                $documents_not_complete = [];
                $checking = false;
                if(isset($data['documents_not_complete']) && isset($data['documents_not_valid'])) {
                    $checking = true;
                    if(!isset($data['document_not_complete']) && !isset($data['document_not_valid'])) {
                        $checking = false;
                        $not_complete = false;
                        $documents_not_complete = [];
                        $not_valid = false;
                        $documents_not_valid = [];
                    } else {
                        $not_complete = true;
                        $not_valid = true;
                        $documents_not_valid = [];
                        $documents_not_complete = [];
                        foreach($data['document_not_complete'] as $key => $document) {
                            $documents_not_complete[$key] = $document;
                        }
                        foreach($data['document_not_valid'] as $key => $document) {
                            $documents_not_valid[$key] = $document;
                        }
                    }       
                } elseif(isset($data['documents_not_complete'])) {
                    $checking = true;
                    if(!isset($data['document_not_complete'])) {
                        $checking = false;
                        $not_complete = false;
                        $documents_not_complete = [];
                    } else {
                        $not_complete = true;
                        $documents_not_complete = [];
                        foreach($data['document_not_complete'] as $key => $document) {
                            $documents_not_complete[$key] = $document;
                        }
                    }       
                } elseif(isset($data['documents_not_valid'])) {
                    $checking = true;
                    if(!isset($data['document_not_valid'])) {
                        $checking = false;
                        $not_valid = false;
                        $documents_not_valid = [];
                        $return = [
                            'status' => 'error',
                            'message' => 'Kolom dokumen harus diisi!'
                        ];
                    } else {
                        $not_valid = true;
                        $documents_not_valid = [];
                        foreach($data['document_not_valid'] as $key => $document) {
                            $documents_not_valid[$key] = $document;
                        }
                    }
                } else {
                    $checking = false;
                    $return = [
                        'status' => 'error',
                        'message' => 'Tidak ada field yang diisi!'
                    ];
                }
                
                if($checking == true) {
                    $this->data = [
                        'site_name' => 'Tokomanamana',
                        'company_name' => $principle_register->company_name,
                        'name' => $principle_register->owner_name,
                        'email' => $principle_register->owner_email,
                        'not_valid' => $not_valid,
                        'not_complete' => $not_complete,
                        'documents_not_valid' => $documents_not_valid,
                        'documents_not_complete' => $documents_not_complete
                    ];

                    $message = $this->load->view('email/rejected_email', $this->data, true);

                    $cronjob = [
                        'from' => settings('send_email_from'),
                        'from_name' => settings('store_name'),
                        'to' => $principle_register->owner_email,
                        'subject' => 'Pendaftaran Principal ' . settings('store_name') . ' Ditolak',
                        'message' => $message
                    ];

                    $success = $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));

                    if($success) {
                        $this->main->delete('phone_verification', ['phone' => $principle_register->owner_phone]);
                        $this->main->update('principle_register', ['status' => 2], ['id' => $id]);
                        // $this->send_email($success);
                        $return = [
                            'status' => 'success',
                            'message' => 'Email Penolakan Principal Berhasil Dikirim!',
                            'redirect' => site_url('principle/request')
                        ];
                    } else {
                        $return = [
                            'status' => 'error',
                            'message' => 'Email Penolakan Principal Gagal Dikirim!'
                        ];
                    }
                } else {
                    $return = [
                        'status' => 'error',
                        'message' => 'Tidak ada field yang diisi!'
                    ];
                }
            } else {
                $return = [
                    'status' => 'error',
                    'message' => 'Tidak ada field yang diisi!'
                ];
            }

        } else {
            $return = [
                'status' => 'error',
                'message' => 'Principal tidak ada!'
            ];
        }

        echo json_encode($return);
    }
}
