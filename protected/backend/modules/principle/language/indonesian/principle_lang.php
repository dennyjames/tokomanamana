<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['principle_heading'] = 'Principle';
$lang['principle_sub_heading'] = 'Kelola Data Principle';
$lang['principle_list_heading'] = 'Daftar Principle';
$lang['principle_add_heading'] = 'Tambah Principle';
$lang['principle_edit_heading'] = 'Edit Principle';

$lang['principle_image_th'] = 'Gambar';
$lang['principle_name_th'] = 'Nama';
$lang['principle_seo_url_th'] = 'Friendly URL';

$lang['principle_company_th'] = 'Nama Perusahaan';
$lang['principle_owner_th'] = 'Nama PIC';
$lang['principle_phone_th'] = 'No Telp';

$lang['principle_form_general_tabs'] = 'Umum';
$lang['principle_form_seo_tabs'] = 'SEO';
$lang['principle_form_auth_tabs'] = 'Auth';

$lang['principle_form_name_label'] = 'Nama';
$lang['principle_form_image_label'] = 'Gambar';
$lang['principle_form_description_label'] = 'Deskripsi';
$lang['principle_form_seo_url_label'] = 'Friendly URL';
$lang['principle_form_meta_title_label'] = 'Meta Judul';
$lang['principle_form_meta_description_label'] = 'Meta Deskripsi';
$lang['principle_form_meta_keyword_label'] = 'Meta Keyword';
$lang['owner_name'] = 'Nama Pemilik';
$lang['email'] = 'Email';
$lang['phone'] = 'Phone';
$lang['password'] = 'Password';

$lang['req_brand_name'] = 'Nama Merek';
$lang['req_company_name'] = 'Nama Perusahaan';
$lang['req_owner_name'] = 'Nama Penanggung Jawab';
$lang['req_owner_email'] = 'Email Penanggung Jawab';
$lang['req_owner_phone'] = 'Telepon Penanggung Jawab';
$lang['req_date_added'] = 'Tanggal Pendaftaran';

$lang['req_company_type'] = 'Jenis Perusahaan';
$lang['req_main_category'] = 'Kategori utama Produk';
$lang['req_sku'] = 'SKU yang ingin dijual';
$lang['req_revenue'] = 'Penghasilan';
$lang['req_document'] = 'Dokumen';
$lang['req_instagram'] = 'Akun Instagram';
$lang['req_ecommerce'] = 'E-Commerce Lainnya';
$lang['req_price'] = 'Range Harga Produk';
$lang['req_know_from'] = 'Tahu Tokomanamana dari';
$lang['req_custom_from'] = 'Lainnya';

$lang['principle_form_name_placeholder'] = 'Masukkan nama principle';
$lang['principle_form_description_placeholder'] = 'Masukkan deskripsi dari principle ini. Deskripsi akan ditampilkan di halaman yang menampilkan semua produk dari Principle ini';

$lang['principle_save_success_message'] = "Principle '%s' berhasil disimpan.";
$lang['principle_save_error_message'] = "Principle '%s' gagal disimpan.";
$lang['principle_delete_success_message'] = "Principle '%s' berhasil dihapus.";
$lang['principle_delete_error_message'] = "Principle '%s' gagal dihapus.";
$lang['page_friendly_url_exist_message'] = "Friendly URL sudah digunakan.";

$lang['view_principle'] = 'Principal';
$lang['actions_th'] = 'Opsi';
