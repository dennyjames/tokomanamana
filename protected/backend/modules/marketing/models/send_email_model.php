<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Send_email_model extends CI_Model {

    function get_email_customer() {
        return $this->db->query("select email from customers");
    }

    function get_email_merchant() {
        return $this->db->query("select email from merchant_users");
    }

    function get_email_principal() {
        return $this->db->query("select email from aauthp_users");
    }
    
    function get_email_subscriber() {
        return $this->db->query("select email from subscriptions");
    }
}
