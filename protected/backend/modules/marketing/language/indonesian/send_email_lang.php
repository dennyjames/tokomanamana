<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['heading'] = 'Broadcast Email Newsletter';

$lang['email'] = 'Email';

$lang['save_success'] = "Email '%s' berhasil disimpan.";
$lang['save_error'] = "Email '%s' gagal disimpan.";
$lang['delete_success'] = "Email '%s' berhasil dihapus.";
$lang['delete_error'] = "Email '%s' gagal dihapus.";
