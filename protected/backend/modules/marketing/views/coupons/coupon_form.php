<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('coupon_edit_heading') : lang('coupon_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('marketing/coupons'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('coupon_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('marketing/coupons/save'); ?>" class="form-horizontal" method="post" id="form">
                    <input type="hidden" name="id" value="<?php echo ($data) ? encode($data->id) : ''; ?>">
                    <input type="hidden" id="decodeid" value="<?php echo ($data) ? $data->id : ''; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_name_label'); ?></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" required="" name="name" value="<?php echo ($data) ? $data->name : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Deskripsi</label>
                                <div class="col-md-9">
                                    
                                    <textarea class="form-control" id="deskripsi" required name="deskripsi" placeholder="Deskripsi Kupon"><?=($data ? $data->deskripsi : '')?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_code_label'); ?></label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" required="" name="code" value="<?php echo ($data) ? $data->code : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_type_label'); ?></label>
                                <div class="col-md-3">
                                    <select class="form-control" name="type" required="">
                                        <option value="F" <?php if(($data ? $data->type : '') == 'F'){ echo "selected";}?>>Fixed Amount</option>
                                        <option value="P"  <?php if(($data ? $data->type : '') == 'P'){ echo "selected";}?>>Percentage</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Tipe Kupon</label>
                                <div class="col-md-3">
                                    <select class="form-control" name="coupon_type" required="">
                                        <option value="all" <?php if(($data ? $data->coupon_type : '') == 'all'){ echo "selected";}?>>All</option>
                                        <option value="branch" <?php if(($data ? $data->coupon_type : '') == 'branch'){ echo "selected";}?>>Branch</option>
                                        <option value="merchant" <?php if(($data ? $data->coupon_type : '') == 'merchant'){ echo "selected";}?>>Merchant</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Kategori</label>
                                <div class="col-md-3">
                                    <div id="treeSelectorCat"></div>
                                    <input id="catIds" name="catIds" type="hidden" value="<?php echo ($data) ? $data->catIds : NULL; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Zona</label>
                                <div class="col-md-3">
                                <select class="form-control" name="zonaIds[]" required="" multiple="">
                                    <?php 
                                        $exp_zona = $data ? explode(',',$data->zonaIds) : array();
                                        foreach ($zonas as $zona) {?>
                                        <option value="<?php echo $zona->id?>" <?php if (in_array($zona->id, $exp_zona)) { echo "selected";}?>><?php echo $zona->name?></option>
                                    <?php } ?>
                                </select>
                                    <!-- <div id="treeSelectorZona"></div>
                                    <input id="zonaIds" name="zonaIds" type="hidden" value="<?php echo ($data) ? $data->zonaIds : []; ?>"> -->
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_discount_label'); ?></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control number" required="" name="discount" value="<?php echo ($data) ? $data->discount : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_min_order_label'); ?></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control number" name="min_order" value="<?php echo ($data) ? $data->min_order : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_available_date_label'); ?></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control date" name="date_start" value="<?php echo ($data) ? $data->date_start : ''; ?>" placeholder="<?php echo lang('coupon_form_date_start_placeholder'); ?>">
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control date" name="date_end" value="<?php echo ($data) ? $data->date_end : ''; ?>" placeholder="<?php echo lang('coupon_form_date_end_placeholder'); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_use_per_coupon_label'); ?></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control number" name="uses_total" value="<?php echo ($data) ? $data->uses_total : 1; ?>">
                                </div>
                                <span class="help-block"><?php echo lang('coupon_form_use_per_coupon_help'); ?></span>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_use_per_customer_label'); ?></label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control number" name="uses_customer" value="<?php echo ($data) ? $data->uses_customer : 1; ?>">
                                </div>
                                <span class="help-block"><?php echo lang('coupon_form_use_per_customer_help'); ?></span>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Gambar</label>
                                <div class="col-md-9">
                                    <?php
                                        $img_style = 'display: none';
                                        $img_src = '';
                                        if($data){
                                            $img_src = $data->gambar;
                                            if($data->gambar != ''){
                                                $img_style = '';
                                            }
                                        }
                                    ?>
                                    <img src="<?=$img_src?>" class="thumbnail" style="width: 240px; <?=$img_style?>" id="img">
                                    <button type="button" class="btn btn-info" onclick="chose_image()">Pilih Gambar</button>
                                    <input type="file" accept="image/*" id="gambar_input" placeholder="Point" class="form-control" style="display: none" onchange="change_image(this)">
                                    <input type="hidden" name="gambar" id="base64_image" value="<?=$img_src?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('coupon_form_status_label'); ?></label>
                                <div class="col-md-9">
                                    <input type="checkbox" name="status" value="1" data-on-text="<?php echo lang('yes'); ?>" data-off-text="<?php echo lang('no'); ?>" class="switch" <?php echo ($data) ? (($data->status == 1) ? 'checked' : '') : 'checked'; ?>>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('marketing/coupons'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
