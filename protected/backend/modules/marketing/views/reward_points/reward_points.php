<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2>Reward Points</h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('marketing/reward_points/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span>Tambah Reward</span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="container" style="margin-top: 24px">
                <div class="row">
                    <div class="col-xs-12">
                        <?php
                            $q_tukar_point = $this->Model->get_data('value', 'settings', null, array('key' => 'periode_tukar_point'));
                            $ptp = '(Belum Ada)';
                            $periode_tukar_point = '';

                            if($q_tukar_point->num_rows() > 0){
                                $exp = explode(';', $q_tukar_point->row()->value);
                                $periode_tukar_point = '('.date('d M Y', strtotime($exp[0].'/'.$exp[1].'/'.$exp[2])).' S/D '.date('d M Y', strtotime($exp[3].'/'.$exp[4].'/'.$exp[5])).') ';

                                if($exp[6] == '1'){
                                    $periode_tukar_point .= '<i class="icon-checkmark3 text-success" ></i> Aktif';

                                    $ptp = '<b style="color: green">'.$periode_tukar_point.'</b>';
                                }else{
                                    $periode_tukar_point .= '<i class="icon-cross2 text-danger-400"></i> Tidak Aktif';

                                    $ptp = '<b style="color: red">'.$periode_tukar_point.'</b>';
                                }
                            }
                        ?>
                        <p><b>Periode Tukar Poin <?=$ptp?></b></p>
                    </div>
                    <div class="col-xs-12 col-lg-3">
                        <label class="control-label">Tahun</label>
                        <select class="form-control" style="width: 240px" id="tahun_mulai" required name="tahun_mulai" onchange="change_tahun_awal(this.value)">
                            <option value="">Pilih Tahun</option>
                            <?php
                                $tahun = date('Y');

                                for($a=0; $a<5; $a++){
                                    ?>
                            <option value="<?=$tahun+$a?>"><?=$tahun+$a?></option>
                                    <?php
                                }
                            ?>
                            
                        </select>
                    </div>
                    <div class="col-xs-12 col-lg-3">
                        <label class="control-label">Bulan</label>
                        <select class="form-control tahun_mulai" style="width: 240px" id="bulan_mulai" required name="bulan_mulai" disabled="" onchange="change_bulan_awal(this.value)">
                            <option value="">Pilih Bulan</option>
                            <?php
                                for($a=1; $a<=12; $a++){
                                    ?>
                            <option value="<?=(date('m', strtotime('2019/'.$a.'/1')))?>"><?=(date('M', strtotime('2019/'.$a.'/1')))?></option>
                                    <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-xs-12 col-lg-3">
                        <label class="control-label">Tanggal</label>
                        <select class="form-control tahun_mulai bulan_mulai" style="width: 240px" id="tanggal_mulai" required name="tanggal_mulai" disabled="" onchange="change_tanggal_awal(this.value)">
                            <option value="">Pilih Tanggal</option>
                        </select>
                    </div>


                    <div class="col-xs-12" style="margin-top: 12px">
                        <p><b>Sampai</b></p>
                    </div>

                    <div class="col-xs-12 col-lg-3">
                        <label class="control-label">Tahun</label>
                        <select class="form-control tahun_mulai bulan_mulai tanggal_mulai" style="width: 240px" id="tahun_selesai" required name="tahun_selesai" disabled="" onchange="change_tahun_akhir(this.value)">
                            <option value="">Pilih Tahun</option>
                            <?php
                                $tahun = date('Y');

                                for($a=0; $a<5; $a++){
                                    ?>
                            <option value="<?=$tahun+$a?>"><?=$tahun+$a?></option>
                                    <?php
                                }
                            ?>
                            
                        </select>
                    </div>
                    <div class="col-xs-12 col-lg-3">
                        <label class="control-label">Bulan</label>
                        <select class="form-control tahun_mulai bulan_mulai tanggal_mulai tahun_selesai" style="width: 240px" id="bulan_selesai" required name="bulan_selesai" disabled="" onchange="change_bulan_akhir(this.value)">
                            <option value="">Pilih Bulan</option>
                            <?php
                                for($a=1; $a<=12; $a++){
                                    ?>
                            <option value="<?=(date('m', strtotime('2019/'.$a.'/1')))?>"><?=(date('M', strtotime('2019/'.$a.'/1')))?></option>
                                    <?php
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-xs-12 col-lg-3">
                        <label class="control-label">Tanggal</label>
                        <select class="form-control tahun_mulai bulan_mulai tanggal_mulai tahun_selesai bulan_selesai" style="width: 240px" id="tanggal_selesai" required name="tanggal_selesai" disabled="">
                            <option value="">Pilih Tanggal</option>
                        </select>
                    </div>

                    <div class="col-xs-12" style="text-align: center; margin: 24px">
                        <a class="btn btn-success" onclick="submit_periode()"><i class="fa fa-save"></i> Save</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-flat">
            <table class="table table-hover" id="table" data-url="<?php echo site_url('marketing/reward_points/get_list'); ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="asc">Nama</th>
                        <th>Reward</th>
                        <th>Point</th>
                        <th>QTY</th>
                        <th style="width: 45px;" class="text-center">Status</th>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions_th'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<script type="text/javascript">
    var current_url = '<?=base_url()?>marketing/reward_points';

    function submit_periode() {
        let error = '';
        let tahun_mulai = $('#tahun_mulai').val();
        let bulan_mulai = $('#bulan_mulai').val();
        let tanggal_mulai = $('#tanggal_mulai').val();
        let tahun_selesai = $('#tahun_selesai').val();
        let bulan_selesai = $('#bulan_selesai').val();
        let tanggal_selesai = $('#tanggal_selesai').val();
      if(tahun_mulai != ''){
        if(bulan_mulai != ''){
          if(tanggal_mulai != ''){
            if(tahun_selesai != ''){
              if(bulan_selesai != ''){
                if(tanggal_selesai == ''){
                  error = 'Tanggal Selesai Belum Dipilih';
                }
              }else{
                error = 'Bulan Selesai Belum Dipilih';
              }
            }else{
              error = 'Tahun Selesai Belum Dipilih';
            }
          }else{
            error = 'Tanggal Mulai Belum Dipilih';
          }
        }else{
          error = 'Bulan Mulai Belum Dipilih';
        }
      }else{
        error = 'Tahun Mulai Belum Dipilih';
      }

      if(error != ''){
        swal({
            title: "Error !",
              text: error,
              type: 'error'
          }).then((result) => {
          
        });
      }else{
        $.ajax({
          type: 'POST',
          url: current_url + "/update_periode_tukar_point",
          data: {
            tahun_mulai: tahun_mulai,
            bulan_mulai: bulan_mulai,
            tanggal_mulai: tanggal_mulai,
            tahun_selesai: tahun_selesai,
            bulan_selesai: bulan_selesai,
            tanggal_selesai: tanggal_selesai
          },
          dataType: "json",
          success: function (data) {
            if (data.success == 1) {
              window.location.replace(current_url);
            }else if(data.success == 0){
              swal({
                  title: "Error !",
                    text: 'Data Sudah Ada',
                    type: 'error'
                }).then((result) => {
                
              });
            }
          }
        });
      }
    }
</script>