<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? 'Edit Reward Points' : 'Tambah Reward Points'; ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('marketing/reward_points'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span>Daftar Reward Points</span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('marketing/reward_points/save'); ?>" class="form-horizontal" method="post" id="form">
                    <input type="hidden" name="id" value="<?php echo ($data) ? encode($data->id) : ''; ?>">
                    <input type="hidden" id="decodeid" value="<?php echo ($data) ? $data->id : ''; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Reward Name</label>
                                <div class="col-md-9">
                                    
                                    <input type="text" id="nama" placeholder="Reward Name" class="form-control" value="<?=(($data) ? $data->nama : '')?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Deskripsi</label>
                                <div class="col-md-9">
                                    
                                    <textarea class="form-control" id="deskripsi" placeholder="Deskripsi Reward"><?=($data ? $data->deskripsi : '')?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">QTY Reward</label>
                                <div class="col-md-9">
                                    
                                    <input type="number" id="qty_reward" placeholder="QTY Reward" class="form-control" value="<?=(($data) ? $data->qty : '')?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Tipe</label>
                                <?php
                                    $checkbox_ = array(
                                        0 => '',
                                        1 => '',
                                        2 => ''
                                    );

                                    $reward = array(
                                        0 => '',
                                        1 => '',
                                        2 => ''
                                    );

                                    $reward_style = array(
                                        0 => 'display: none',
                                        1 => 'display: none',
                                        2 => 'display: none'
                                    );

                                    $reward_name = array(
                                        0 => 'Type Something',
                                        1 => 'Type Something',
                                        2 => 'Type Something'
                                    );

                                    $reward_id = array(
                                        0 => '',
                                        1 => '',
                                        2 => ''
                                    );

                                    $tipe = '--';

                                    $is_active = array(
                                        0 => 'checked',
                                        1 => ''
                                    );

                                    $data_is_active = '1';

                                    $group = array(
                                        0 => 'checked',
                                        1 => '',
                                        2 => ''
                                    );

                                    $data_group = 'g';

                                    $data_point = '';

                                    $img_style = 'display: none';
                                    $img_src = '';

                                    $qty0 = 0;
                                    $qty1 = 0;
                                    $qty2 = 0;

                                    if($data){
                                        $tipe = $data->tipe;
                                        $exp = explode('-', $data->tipe);
                                        if($exp[0] != ''){
                                            $checkbox_[0] = 'checked';
                                        }
                                        if($exp[1] != ''){
                                            $checkbox_[1] = 'checked';
                                        }
                                        if($exp[2] != ''){
                                            $checkbox_[2] = 'checked';
                                        }

                                        $reward = array(
                                            0 => $data->id_product,
                                            1 => $data->id_product_paket,
                                            2 => $data->id_coupon
                                        );

                                        $reward_style = array(
                                            0 => $data->id_product == 0 ? 'display: none' : '',
                                            1 => $data->id_product_paket == 0 ? 'display: none' : '',
                                            2 => $data->id_coupon == 0 ? 'display: none' : ''
                                        );

                                        $reward_name = array(
                                            0 => $data->p0_name == '' ? $reward_name[0] : $data->p0_name,
                                            1 => $data->p1_name == '' ? $reward_name[1] : $data->p1_name,
                                            2 => $data->c_name == '' ? $reward_name[2] : $data->c_name
                                        );

                                        $reward_id = array(
                                            0 => $data->id_product == 0 ? '' :  $data->id_product,
                                            1 => $data->id_product_paket == 0 ? '' :  $data->id_product_paket,
                                            2 => $data->id_coupon == 0 ? '' : $data->id_coupon
                                        );

                                        $is_active = array(
                                            0 => $data->is_active == '1' ? 'checked' : '',
                                            1 => $data->is_active == '0' ? 'checked' : ''
                                        );

                                        $data_is_active = $data->is_active;

                                        $group = array(
                                            0 => $data->customer_merchant == 'g' ? 'checked' : '',
                                            1 => $data->customer_merchant == 'c' ? 'checked' : '',
                                            2 => $data->customer_merchant == 'm' ? 'checked' : ''
                                        );

                                        $data_group = $data->customer_merchant;

                                        $data_point = $data->point;

                                        $img_style = $data->gambar != '' ? '' : $img_style;
                                        $img_src = $data->gambar != '' ? $data->gambar : '';

                                        $qty0 = $data->qty_product;
                                        $qty1 = $data->qty_paket;
                                        $qty2 = $data->qty_coupon;
                                    }
                                    ?>
                                <div class="col-md-9">
                                    <p><input type="checkbox" name="tipe" value="0" <?=$checkbox_[0]?> onclick="tipe_check(this.value)"> Produk Standar</p>
                                    <p><input type="checkbox" name="tipe" value="1" <?=$checkbox_[1]?> onclick="tipe_check(this.value)"> Produk Paket</p>
                                    <p><input type="checkbox" name="tipe" value="2" <?=$checkbox_[2]?> onclick="tipe_check(this.value)"> Coupons</p>
                                    <input type="hidden" id="checkbox_" value="<?=$tipe?>">
                                </div>
                            </div>
                            <div class="form-group" id="tipe0" style="<?=$reward_style[0]?>">
                                <label class="col-md-3 control-label">Produk Standar</label>
                                <input type="hidden" id="inp_tipe0" value="<?=$reward_id[0]?>"/>
                                <div class="col-xs-12 col-md-4" style="">
                                    <div class="btn-group">
                                        <button type="button" class="btn border-slate text-slate-800 btn-flat dropdown-toggle" data-toggle="dropdown" id="filter-0"><?=$reward_name[0]?> <span class="caret"></span></button>
                                        <ul class="dropdown-menu dropdown-menu-right">

                                            <li id="item_choice0">
                                                <input type="text" id="search0" onkeyup="change_0(this.value, 0)" placeholder="Type Something" class="form-control">
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="btn-group">
                                        <input type="number" name="" id="qty_product" placeholder="QTY Produk" class="form-control" value="<?=$qty0?>">
                                    </div>
                                </div>

                            </div>
                            <div class="form-group" id="tipe1" style="<?=$reward_style[1]?>">
                                <label class="col-md-3 control-label">Produk Paket</label>
                                <input type="hidden" id="inp_tipe1" value="<?=$reward_id[1]?>"/>
                                <div class="col-xs-12 col-md-4" style="">
                                    <div class="btn-group">
                                        <button type="button" class="btn border-slate text-slate-800 btn-flat dropdown-toggle" data-toggle="dropdown" id="filter-1"><?=$reward_name[1]?> <span class="caret"></span></button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li id="item_choice1">
                                                <input type="text" id="search1" onkeyup="change_0(this.value, 1)" placeholder="Type Something" class="form-control">
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="btn-group">
                                        <input type="number" name="" id="qty_paket" placeholder="QTY Paket" class="form-control" value="<?=$qty1?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="tipe2" style="<?=$reward_style[2]?>">
                                <label class="col-md-3 control-label">Coupons</label>
                                <input type="hidden" id="inp_tipe2" value="<?=$reward_id[2]?>"/>
                                <div class="col-xs-12 col-md-4" style="">
                                    <div class="btn-group">
                                        <button type="button" class="btn border-slate text-slate-800 btn-flat dropdown-toggle" data-toggle="dropdown" id="filter-2"><?=$reward_name[2]?> <span class="caret"></span></button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li id="item_choice2">
                                                <input type="text" id="search2" onkeyup="change_0(this.value, 2)" placeholder="Type Something" class="form-control">
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="btn-group">
                                        <input type="number" name="" id="qty_coupon" placeholder="QTY Coupon" class="form-control" value="<?=$qty2?>">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Status</label>
                                <div class="col-md-9">
                                    <p><input type="radio" name="status" value="1" <?=$is_active[0]?> onclick="is_active_(1)"> Aktif</p>
                                    <p><input type="radio" name="status" value="0" <?=$is_active[1]?> onclick="is_active_(0)"> Tidak Aktif</p>
                                    <input type="hidden" id="is_active" value="<?=$data_is_active?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Customer/Merchant</label>
                                <div class="col-md-9">
                                    <p><input type="radio" name="cm" value="g" <?=$group[0]?> onclick="cm_('g')"> All</p>
                                    <p><input type="radio" name="cm" value="c" <?=$group[1]?> onclick="cm_('c')"> Customer</p>
                                    <p><input type="radio" name="cm" value="m" <?=$group[2]?> onclick="cm_('m')"> Merchant</p>
                                    <input type="hidden" id="customer_merchant" value="<?=$data_group?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Point</label>
                                <div class="col-md-9">
                                    <input type="number" id="point" placeholder="Point" class="form-control" value="<?=$data_point?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Gambar</label>
                                <div class="col-md-9">
                                    <img src="<?=$img_src?>" class="thumbnail" style="width: 240px; <?=$img_style?>" id="img">
                                    <button type="button" class="btn btn-info" onclick="chose_image()">Pilih Gambar</button>
                                    <input type="file" accept="image/*" id="gambar_input" placeholder="Point" class="form-control" style="display: none" onchange="change_image(this)">
                                    <input type="hidden" id="base64_image" value="<?=$img_src?>">
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('marketing/coupons'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <?php
                                        if($data){
                                            ?>
                                    <button type="button" onclick="submit_data(1)" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                            <?php
                                        }else{
                                            ?>
                                    <button type="button" onclick="submit_data(0)" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                            <?php
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var current_url = '<?=base_url()?>marketing/reward_points';
    
</script>