<style>
/* width */
::-webkit-scrollbar {
  width: 10px;
}

/* Track */
::-webkit-scrollbar-track {
  background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
  background: #888; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
  background: #555; 
}

.imgTest img {
    width:50px;
}

th, td {
  padding: 15px;
}
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">                
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('heading'); ?></h2>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <div class="container" style="margin-top: 24px">
                <div class="row">
                    <div class="form-group col-xs-12 col-lg-12 col-md-12" >
                        <!-- <div class="col-md-12" id="divmaxfields">
                            <table cellpadding="1">
                                <tr>
                                    <td><label class="control-label"><b>Jumlah Gambar</b></label></td>
                                    <td> : </td>
                                    <td><input style="width:200px" type="number" id="inputmaxgbr" class="form-control" name="" ></td>
                                    <td></td>
                                    <td></td>
                                    <td><label class="control-label"><b>Jumlah Teks</b></label></td>
                                    <td> : </td>
                                    <td><input style="width:200px" type="number" id="inputmaxteks" class="form-control" name="" ></td>
                                    <td><button href="" type="button" class="btn btn-primary" onclick="max_fields()">Submit</button></td>
                                </tr>
                            </table>
                            <input type="hidden" value="0" id="maxfieldsgbr">
                            <input type="hidden" value="0" id="maxfieldsteks">
                        </div> -->
                        <div class="col-md-10" style="margin-bottom:20px;">
                            <label class="control-label"><b>Penerima :</b></label>
                                <label class="checkbox-inline">
                                  <input id="customer" type="checkbox" name="customer" value="1">Customer
                                </label>
                                <label class="checkbox-inline">
                                  <input id="merchant" type="checkbox" name="merchant" value="2">Merchant
                                </label>
                                <label class="checkbox-inline">
                                  <input id="principal" type="checkbox" name="principal" value="3">Principal
                                </label>
                                <label class="checkbox-inline">
                                  <input id="subscriber" type="checkbox" name="subscriber" value="4">Subscribers
                                </label>
                                <div id="err_penerima" style="display:none">Silahkan pilih penerima terlebih dahulu !</div>
                        </div>                        
                        <div class="col-md-12">
                            <label class="control-label"><b>Pesan :</b></label>
                        </div>
                        <div class="input_fields_wrap col-md-5" id="input_gbr_form" >
                            <button class="btn btn-primary add_field_button"><i class="fa fa-plus"></i> Gambar <i class="fa fa-photo fa-lg"></i></button>
                            <br><br>
                            <input type="hidden"  id="countgbr" value="0">
                        </div>
                        <div class="col-md-5" id="input_teks_form" >
                            <button class="btn btn-primary add_field_teks"><i class="fa fa-plus"></i> Teks <i class="fa fa-clone fa-lg"></i></button>
                            <br><br>
                            <input type="hidden"  id="countteks" value="0">
                        </div>
                        <div id="divtemplate" class="col-md-11" style="margin-top:20px">
                            <label class="control-label"><b>Template :</b></label><br>
                            <?php //echo $versi;
                            $i=0;
                            foreach ($tempp as $tot) { ?>
                                <div style="display:inline;text-transform: capitalize;" class="col-md-2 col-xs-6 col-lg-2">
                                    <center>
                                    <input type="radio" name="template" value="<?=$tot->id?>" <?php if($i==0){ echo "checked";} ?>> <?=str_replace("_"," ",$tot->name);?><br><br>
                                    <img style="width:120px" src="<?=base_url()?>filemanager/img/template_email/<?=$tot->name?>.jpg"><br><br>
                                    
                                    <?php 
                                        foreach ($versi[$i] as $ver) {
                                           echo "(".$ver->subject.")";
                                        }
                                    ?>
                                    </center>
                                </div>
                            <?php $i++;}
                            ?>
                        </div>
                        <div id="divsubmit" class="col-lg-12" >
                            <br><br>
                            <input type="submit" style="margin-bottom:12px;" class="btn btn-success pull-left" onclick="kirim()" value="Kirim">
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>