<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Subscribers extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('marketing/subscriber');
        $this->lang->load('subscribers', settings('language'));
        $this->load->model('subscribers_model', 'subscribers');
        $this->data['menu'] = 'marketing_subscriber';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('marketing'), '/');
        $this->breadcrumbs->push(lang('subscriber'), '/marketing/subscribers');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('heading'));
        $this->load->view('subscribers/list', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->subscribers->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->email,
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">'.
                    ($this->aauth->is_allowed('marketing/subscriber/delete')?'<li><a href="' . site_url('marketing/subscribers/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>':'').
                    '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->subscribers->count_all();
        $output['recordsFiltered'] = $this->subscribers->count_all($search);
        echo json_encode($output);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('marketing/subscriber/delete');
        $id = decode($id);
        $data = $this->main->get('subscriptions', array('id' => $id));
        $delete = $this->main->delete('subscriptions', array('id' => $id));
        if ($delete) {
            $return = array('message' => sprintf(lang('delete_success'), $data->email), 'status' => 'success');
        } else {
            $return = array('message' => sprintf(lang('delete_error'), $data->email), 'status' => 'danger');
        }
        echo json_encode($return);
    }

}
