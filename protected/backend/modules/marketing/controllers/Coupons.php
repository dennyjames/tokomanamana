<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Coupons extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('marketing/coupon');
        $this->lang->load('coupons', settings('language'));
        $this->load->model('coupons_model', 'coupons');
        $this->data['menu'] = 'marketing_coupon';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('marketing'), '/');
        $this->breadcrumbs->push(lang('coupon'), '/marketing/coupons');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        $this->output->set_title(lang('coupon_heading'));
        $this->load->view('coupons/coupons', $this->data);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->data['data'] = array();

        $this->breadcrumbs->unshift(lang('marketing'), '/');
        $this->breadcrumbs->push(lang('coupon'), '/marketing/coupons');
        $this->db->order_by('name','asc');
        $this->data['zonas'] = $this->db->get('merchant_groups')->result();
        if ($id) {
            $this->aauth->control('marketing/coupon/edit');
            $id = decode($id);
            $this->data['data'] = $this->main->get('coupons', array('id' => $id));

            $this->breadcrumbs->push(lang('coupon_edit_heading'), '/marketing/coupons/form/' . encode($id));
            $this->breadcrumbs->push($this->data['data']->name, '/');
        } else {
            $this->aauth->control('marketing/coupon/add');
            $this->breadcrumbs->push(lang('coupon_add_heading'), '/marketing/coupons/form');
        }

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->load->css('../assets/backend/css/jquery.treeSelector.css');
        $this->load->js('../assets/backend/js/jquery.treeSelector.js');
        $this->load->js('../assets/backend/js/modules/design/coupons.js');
        $this->output->set_title(($this->data['data']) ? lang('coupon_edit_heading') : lang('coupon_add_heading'));
        $this->load->view('coupons/coupon_form', $this->data);
    }
    public function getCategories() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $cats = array();
        $this->db->select('id,name,parent');
        $this->db->where('parent',0);
        $this->db->where('active',1);
        $q = $this->db->get('categories');
        $res = $q->result();
        foreach($res as $resx){
            $this->db->select('id,name as title,parent,id as value');
            $this->db->where('parent',$resx->id);
            $this->db->where('active',1);
            $qc = $this->db->get('categories');
            $resc = $qc->result();
            $resx->children = $resc;
            $cats[] = $resx;
        }
        $response['data'] = $cats;
        echo json_encode($response);
    }
    public function getZona() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->db->select('id,name,id as value');
        $q = $this->db->get('merchant_groups');
        $res = $q->result();
        $response['data'] = $res;
        echo json_encode($response);
    }
    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->coupons->get_all($start, $length, $search, $order);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->name,
                    $data->code,
                    number($data->discount),
                    ($data->date_start != '0000-00-00') ? $data->date_start : '',
                    ($data->date_end != '0000-00-00') ? $data->date_end : '',
                    ($data->status == 1) ? '<i class="icon-checkmark3 text-success"></i>' : '<i class="icon-cross2 text-danger-400"></i>',
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">'.
                    ($this->aauth->is_allowed('marketing/coupon/edit')?'<li><a href="' . site_url('marketing/coupons/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>':'').
                    ($this->aauth->is_allowed('marketing/coupon/delete')?'<li><a href="' . site_url('marketing/coupons/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>':'').
                    '</ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->coupons->count_all();
        $output['recordsFiltered'] = $this->coupons->count_all($search);
        echo json_encode($output);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'lang:coupon_form_name_label', 'trim|required');
        $this->form_validation->set_rules('code', 'lang:coupon_form_code_label', 'trim|required|alpha_dash');
        $this->form_validation->set_rules('type', 'lang:coupon_form_type_label', 'trim|required');
        $this->form_validation->set_rules('discount', 'lang:coupon_form_discount_label', 'trim|numeric');
        $this->form_validation->set_rules('min_order', 'lang:coupon_form_min_order_label', 'trim|numeric');
        $this->form_validation->set_rules('uses_total', 'lang:coupon_form_use_per_coupon_label', 'trim|numeric');
        $this->form_validation->set_rules('uses_customer', 'lang:coupon_form_use_per_customer_label', 'trim|numeric');
        $this->form_validation->set_rules('status', 'lang:coupon_form_status_label', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Bidang Deskripsi Dibutuhkan', 'trim|required');
        $this->form_validation->set_rules('gambar', 'Gambar Dibutuhkan', 'trim|required');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            do {
                if ($data['id'])
                    $data['id'] = decode($data['id']);

                $data['status'] = (isset($data['status']) && $data['status'] == 1) ? $data['status'] : 0;
                $data['zonaIds'] = isset($data['zonaIds']) ? implode(",",$data['zonaIds']) : '';
                $data['gambar'] = str_replace('[removed]', 'data:image/jpeg;base64,', $data['gambar']);
//                $data['date_start'] = $data['date_start_submit'];
//                $data['date_end'] = $data['date_end_submit'];
//                unset($data['date_start_submit']);
//                unset($data['date_end_submit']);

                if ($data['id']) {
                    $this->main->update('coupons', $data, array('id' => $data['id']));
                } else {
                    $id = $this->main->insert('coupons', $data);
                }

                $return = array('message' => sprintf(lang('coupon_save_success_message'), $data['name']), 'status' => 'success', 'redirect' => site_url('marketing/coupons'));
            } while (0);
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->aauth->control('marketing/coupon/delete');
        $id = decode($id);
        $data = $this->main->get('coupons', array('id' => $id));
        $delete = $this->main->delete('coupons', array('id' => $id));
        if ($delete) {
            $this->main->delete('coupon_history', array('coupon' => $id));
            $return = array('message' => sprintf(lang('coupon_delete_success_message'), $data->name), 'status' => 'success');
        } else {
            $return = array('message' => sprintf(lang('coupon_delete_error_message'), $data->name), 'status' => 'danger');
        }
        echo json_encode($return);
    }

}
