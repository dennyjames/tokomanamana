<?php

defined('BASEPATH') or exit('No direct script access allowed!');
class Send_email extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('marketing/send_email');
        $this->lang->load('send_email', settings('language'));
        $this->load->library('email');
        $this->load->model('send_email_model', 'sendemail');
        $this->data['menu'] = 'marketing_send_email';
        $this->load->library('curl');
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->load->js('../assets/backend/js/modules/marketing/send_email.js');

        $this->breadcrumbs->unshift(lang('marketing'), '/');
        $this->breadcrumbs->push(lang('send_email'), '/marketing/send_email');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('heading'));

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sendgrid.com/v3/templates?generations=dynamic",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_SSL_VERIFYPEER => false,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_POSTFIELDS => "{}",
          CURLOPT_HTTPHEADER => array(
            "authorization: Bearer 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"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $response_ = json_decode($response);
        $this->data['tempp']= $response_->templates;
        $tes = array();
        foreach ($response_->templates as $key) {
            $tes[]=$key->versions;
        }
        $this->data['versi'] = $tes;
        $this->load->view('send_email/send_email', $this->data);
    }

    public function kirim_email(){
        $email_customer = $this->sendemail->get_email_customer()->result();
        $email_merchant = $this->sendemail->get_email_merchant()->result();
        $email_principal = $this->sendemail->get_email_principal()->result();
        $email_subscriber = $this->sendemail->get_email_subscriber()->result();
        $template_id = $this->input->post('template');
        $jmlgbr = $this->input->post('jmlgbr');
        $jmlteks = $this->input->post('jmlteks');
        $customer = $this->input->post('customer');
        $merchant = $this->input->post('merchant');
        $principal = $this->input->post('principal');
        $subscriber = $this->input->post('subscriber');

        if($subscriber > 0){
            foreach ($email_subscriber as $mail){
                $to = $mail->email;
                //untuk teks
                $param_dynamic = array();
                if($jmlteks){
                  for($i=1; $i <= $jmlteks; $i++){
                    $teks = $this->input->post('teks'.$i);
                    $loopteks = 'teks'.$i;
                    $param_dynamic[$loopteks] = $teks;
                  }
                }
                //untuk gambar
                $param_attachment = array();
                if($jmlgbr){
                  for($a=1; $a <= $jmlgbr; $a++){
                    $img = $this->input->post('image'.$a);
                    $namaimg = $this->input->post('namagbr'.$a);
                    $exsatu = explode('.', $namaimg);
                    $typesatu = end($exsatu);
                    
                    if($typesatu=='png' || $typesatu=='PNG'){
                      $image = str_replace('data:image/png;base64,',"",$img);
                    } else if ($typesatu=='jpg' || $typesatu=='JPG' || $typesatu=='JPEG' || $typesatu=='jpeg'){
                      $image = str_replace('data:image/jpeg;base64,',"",$img);
                    }
                    $loopimg = 'image'.$a;
                    $param_attachment[] = array('filename' => $namaimg, 'type' => 'image/'.$typesatu, 'content' => $image, 'content_id' => $loopimg, 'disposition' => 'inline');
                    $linkgbr = $this->input->post('linkgbr'.$a);
                    $looplinkgbr = 'linkgbr'.$a;
                    $param_dynamic[$looplinkgbr] = $linkgbr;
                  }
                }
                $param_to = array(array('email'=>$to,'name' => ' '));
                $param_from = array('email'=>'marketing@tokomanamana.com', 'name'=>'TokoManamana');
                
                $params = array(array('to' => $param_to,'dynamic_template_data' => $param_dynamic));
                $params_all = array(array('personalizations'=>$params,'attachments' => $param_attachment,'from'=>$param_from,'template_id' => $template_id));
                $new_array = array();
                foreach($params_all as $value) { $new_array[] = $value; }
                $tesss = json_encode($new_array);
                $r = substr_replace($tesss, '', 0, 1);
                $r_ = substr_replace($r, '',-1);
                $curl = curl_init();
                  curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $r_,
                    CURLOPT_HTTPHEADER => array(
                      "authorization: Bearer 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",
                      "content-type: application/json"
                    ),
                  ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                /*var_dump($response);
                exit();*/
            }
        }

        if($merchant > 0){
            foreach ($email_merchant as $mail){
                $to = $mail->email;
                //untuk teks
                $param_dynamic = array();
                if($jmlteks){
                  for($i=1; $i <= $jmlteks; $i++){
                    $teks = $this->input->post('teks'.$i);
                    $loopteks = 'teks'.$i;
                    $param_dynamic[$loopteks] = $teks;
                  }
                }
                //untuk gambar
                $param_attachment = array();
                if($jmlgbr){
                  for($a=1; $a <= $jmlgbr; $a++){
                    $img = $this->input->post('image'.$a);
                    $namaimg = $this->input->post('namagbr'.$a);
                    $exsatu = explode('.', $namaimg);
                    $typesatu = end($exsatu);
                    
                    if($typesatu=='png' || $typesatu=='PNG'){
                      $image = str_replace('data:image/png;base64,',"",$img);
                    } else if ($typesatu=='jpg' || $typesatu=='JPG' || $typesatu=='JPEG' || $typesatu=='jpeg'){
                      $image = str_replace('data:image/jpeg;base64,',"",$img);
                    }
                    $loopimg = 'image'.$a;
                    $param_attachment[] = array('filename' => $namaimg, 'type' => 'image/'.$typesatu, 'content' => $image, 'content_id' => $loopimg, 'disposition' => 'inline');
                    $linkgbr = $this->input->post('linkgbr'.$a);
                    $looplinkgbr = 'linkgbr'.$a;
                    $param_dynamic[$looplinkgbr] = $linkgbr;
                  }
                }
                $param_to = array(array('email'=>$to,'name' => ' '));
                $param_from = array('email'=>'marketing@tokomanamana.com', 'name'=>'TokoManamana');
                
                $params = array(array('to' => $param_to,'dynamic_template_data' => $param_dynamic));
                $params_all = array(array('personalizations'=>$params,'attachments' => $param_attachment,'from'=>$param_from,'template_id' => $template_id));
                $new_array = array();
                foreach($params_all as $value) { $new_array[] = $value; }
                $tesss = json_encode($new_array);
                $r = substr_replace($tesss, '', 0, 1);
                $r_ = substr_replace($r, '',-1);
                $curl = curl_init();
                  curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $r_,
                    CURLOPT_HTTPHEADER => array(
                      "authorization: Bearer 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",
                      "content-type: application/json"
                    ),
                  ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                /*var_dump($response);
                exit();*/
            }
        }

        if($principal > 0){
            foreach ($email_principal as $mail){
                $to = $mail->email;
                //untuk teks
                $param_dynamic = array();
                if($jmlteks){
                  for($i=1; $i <= $jmlteks; $i++){
                    $teks = $this->input->post('teks'.$i);
                    $loopteks = 'teks'.$i;
                    $param_dynamic[$loopteks] = $teks;
                  }
                }
                //untuk gambar
                $param_attachment = array();
                if($jmlgbr){
                  for($a=1; $a <= $jmlgbr; $a++){
                    $img = $this->input->post('image'.$a);
                    $namaimg = $this->input->post('namagbr'.$a);
                    $exsatu = explode('.', $namaimg);
                    $typesatu = end($exsatu);
                    
                    if($typesatu=='png' || $typesatu=='PNG'){
                      $image = str_replace('data:image/png;base64,',"",$img);
                    } else if ($typesatu=='jpg' || $typesatu=='JPG' || $typesatu=='JPEG' || $typesatu=='jpeg'){
                      $image = str_replace('data:image/jpeg;base64,',"",$img);
                    }
                    $loopimg = 'image'.$a;
                    $param_attachment[] = array('filename' => $namaimg, 'type' => 'image/'.$typesatu, 'content' => $image, 'content_id' => $loopimg, 'disposition' => 'inline');
                    $linkgbr = $this->input->post('linkgbr'.$a);
                    $looplinkgbr = 'linkgbr'.$a;
                    $param_dynamic[$looplinkgbr] = $linkgbr;
                  }
                }
                $param_to = array(array('email'=>$to,'name' => ' '));
                $param_from = array('email'=>'marketing@tokomanamana.com', 'name'=>'TokoManamana');
                
                $params = array(array('to' => $param_to,'dynamic_template_data' => $param_dynamic));
                $params_all = array(array('personalizations'=>$params,'attachments' => $param_attachment,'from'=>$param_from,'template_id' => $template_id));
                $new_array = array();
                foreach($params_all as $value) { $new_array[] = $value; }
                $tesss = json_encode($new_array);
                $r = substr_replace($tesss, '', 0, 1);
                $r_ = substr_replace($r, '',-1);
                $curl = curl_init();
                  curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $r_,
                    CURLOPT_HTTPHEADER => array(
                      "authorization: Bearer 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",
                      "content-type: application/json"
                    ),
                  ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                /*var_dump($response);
                exit();*/
            }
        }

        if($customer > 0){
            foreach ($email_customer as $mail){
                $to = $mail->email;
                //untuk teks
                $param_dynamic = array();
                if($jmlteks){
                  for($i=1; $i <= $jmlteks; $i++){
                    $teks = $this->input->post('teks'.$i);
                    $loopteks = 'teks'.$i;
                    $param_dynamic[$loopteks] = $teks;
                  }
                }
                //untuk gambar
                $param_attachment = array();
                if($jmlgbr){
                  for($a=1; $a <= $jmlgbr; $a++){
                    $img = $this->input->post('image'.$a);
                    $namaimg = $this->input->post('namagbr'.$a);
                    $exsatu = explode('.', $namaimg);
                    $typesatu = end($exsatu);
                    
                    if($typesatu=='png' || $typesatu=='PNG'){
                      $image = str_replace('data:image/png;base64,',"",$img);
                    } else if ($typesatu=='jpg' || $typesatu=='JPG' || $typesatu=='JPEG' || $typesatu=='jpeg'){
                      $image = str_replace('data:image/jpeg;base64,',"",$img);
                    }
                    $loopimg = 'image'.$a;
                    $param_attachment[] = array('filename' => $namaimg, 'type' => 'image/'.$typesatu, 'content' => $image, 'content_id' => $loopimg, 'disposition' => 'inline');
                    $linkgbr = $this->input->post('linkgbr'.$a);
                    $looplinkgbr = 'linkgbr'.$a;
                    $param_dynamic[$looplinkgbr] = $linkgbr;
                  }
                }
                $param_to = array(array('email'=>$to,'name' => ' '));
                $param_from = array('email'=>'marketing@tokomanamana.com', 'name'=>'TokoManamana');
                
                $params = array(array('to' => $param_to,'dynamic_template_data' => $param_dynamic));
                $params_all = array(array('personalizations'=>$params,'attachments' => $param_attachment,'from'=>$param_from,'template_id' => $template_id));
                $new_array = array();
                foreach($params_all as $value) { $new_array[] = $value; }
                $tesss = json_encode($new_array);
                $r = substr_replace($tesss, '', 0, 1);
                $r_ = substr_replace($r, '',-1);
                $curl = curl_init();
                  curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 30,
                    CURLOPT_SSL_VERIFYPEER => false,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $r_,
                    CURLOPT_HTTPHEADER => array(
                      "authorization: Bearer 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",
                      "content-type: application/json"
                    ),
                  ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                /*var_dump($response);
                exit();*/
            }
        }
        

        curl_close($curl);

            if ($err) {
                die(json_encode(array('success' => 0)));
                echo "cURL Error #:" . $err;
            } else {
                die(json_encode(array('success' => 1)));
                echo $response;
            }
    }

}
