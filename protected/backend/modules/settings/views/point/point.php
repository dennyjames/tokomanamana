<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2>POINT1 &nbsp;</h2><br>
                <h3>FILTER</h3><br><hr>
                <div class="container" style="padding-left: 48px">
                    <div class="row">
                        <div class="col-xs-12 col-md-4" style="margin-top: 18px">
                            <p>.. </p>
                            <div class="btn-group">
                                <button type="button" class="btn border-slate text-slate-800 btn-flat dropdown-toggle" data-toggle="dropdown" id="filter-category">Default Semua Item <span class="caret"></span></button>
                                
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4" style="margin-top: 18px">
                            <p>KATEGORI</p>
                            <div class="btn-group">
                                <button type="button" class="btn border-slate text-slate-800 btn-flat dropdown-toggle" data-toggle="dropdown" id="filter-category0">Semua Kategori <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li class="">
                                        <a href="javascript:void(0);" onclick="set_filter_item(this, 1, '', 0);">Semua Kategori</a>
                                    </li>
                                    <li id="item_choice0">
                                        <input type="text" id="search0" onkeyup="change_kategori(this.value)" placeholder="Type Something" class="form-control">
                                        <input type="hidden" id="val0">
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4" style="margin-top: 18px">
                            <p>SUB KATEGORI</p>
                            <div class="btn-group">
                                <button type="button" class="btn border-slate text-slate-800 btn-flat dropdown-toggle" data-toggle="dropdown" id="filter-category1">Semua Sub Kategori <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li class="">
                                        <a href="javascript:void(0);" onclick="set_filter_item(this, 2, '', 1);">Semua Sub Kategori</a>
                                    </li>
                                    <li id="item_choice1">
                                        <input type="text" id="search1" onkeyup="change_sub_kategori(this.value)" placeholder="Type Something" class="form-control">
                                        <input type="hidden" id="val1">
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4" style="margin-top: 18px">
                            <p>ITEM</p>
                            <div class="btn-group" id="row_item1">
                                <button type="button" class="btn border-slate text-slate-800 btn-flat dropdown-toggle" data-toggle="dropdown" id="filter-category2">Semua Item <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li class="">
                                        <a href="javascript:void(0);" onclick="set_filter_item(this, 3, '', 2);">Semua Item</a>
                                    </li>
                                    <li id="item_choice2">
                                        <input type="text" id="search2" onkeyup="change_item(this.value)" placeholder="Type Something" class="form-control">
                                        <input type="hidden" id="val2">
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-4" style="margin-top: 18px">
                            <p>MERCHANT/DOWNLINE/CUSTOMER</p>
                            <div class="btn-group" id="row_item1">
                                <button type="button" class="btn border-slate text-slate-800 btn-flat dropdown-toggle" data-toggle="dropdown" id="filter-merchant">Merchant <span class="caret"></span></button>
                                <input type="hidden" id="val3" value="0">
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="javascript:void(0);" onclick="set_merchant(this, 0);" data-category="g">Merchant</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" onclick="set_merchant(this, 1);" data-category="g">Downline</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);" onclick="set_merchant(this, 2);" data-category="g">Customer</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div><br>
                </div>
                
                <!-- <div class="row" style="padding-left: 48px">
                    <div class="col-xs-4">
                        <button type="button" class="btn btn-info" style="width: 100%" onclick="filter()">Filter</button>

                    </div>
                </div> -->
            </div>

            <!-- <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('settings/seo_url/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span>Tambah Seo Url</span></a>
                </div>
            </div> -->
        </div>
    </div>
    <div class="content">
        
        <div class="panel panel-flat">
            <center>
                <button type="button" id="btn_add" onclick="add_point()" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span>Tambah Seetting Point</span></button>
            </center>

            <div class="panel panel-flat" id="form_setting_point" style="display: none">
                <center><h4>Tambah Setting Point</h4></center>
                <div class="panel-body">
                    <div class="form-group form_edit">
                        <label class="col-md-2 control-label">ID General</label>
                        <div class="col-md-10">
                            <input type="" class="form-control" placeholder="Point" id="point_form" readonly>
                            <input type="hidden" class="form-control" placeholder="Point" id="id_setting_poin" readonly>
                        </div><br>
                    </div>
                    <div class="form-group form_edit">
                        <label class="col-md-2 control-label">Name</label>
                        <div class="col-md-10">
                            <input type="" class="form-control" placeholder="Point" id="name_form" readonly>
                            
                        </div><br>
                    </div>
                    <div class="form-group form_edit">
                        <label class="col-md-2 control-label">Type</label>
                        <div class="col-md-10">
                            <input type="" class="form-control" placeholder="Point" id="type_form" readonly>
                            
                        </div><br>
                    </div>
                    <div class="form-group form_edit">
                        <label class="col-md-2 control-label">Merchant</label>
                        <div class="col-md-10">
                            <input type="" class="form-control" placeholder="Point" id="merchant_form" readonly>
                            
                        </div><br>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Point</label>
                        <div class="col-md-10">
                            <input type="number" class="form-control" placeholder="Point" id="point">
                            
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="heading-elements action-left">
                        <button type="button" class="btn btn-default" onclick="cancel()"><?php echo lang('button_cancel'); ?></a>
                        <button type="button" onclick="save()" class="btn btn-primary form_add"><?php echo lang('button_save'); ?></button>

                        <button type="button" onclick="edit()" class="btn btn-primary form_edit"><?php echo lang('button_save'); ?></button>
                        <div class="">
                            
                        </div>
                    </div>
                </div>
                
            </div>
            
            <table class="table" id="table" data-url="<?php echo site_url('settings/point/get_list'); ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="asc">ID GENERAL</th>
                        <th>NAME</th>
                        <th>TYPE</th>
                        <th>MERCHANT TYPE</th>
                        <th>POINT</th>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions_th'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>