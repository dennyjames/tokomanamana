<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo ($data) ? lang('user_edit_heading') : lang('user_add_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                    <a href="<?php echo site_url('settings/users'); ?>" class="btn btn-link btn-float has-text"><i class="icon-list3 text-primary"></i><span><?php echo lang('user_list_heading'); ?></span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('settings/users/save'); ?>" method="post" class="form-horizontal" id="form">
                    <input type="hidden" name="id" value="<?php echo ($data) ? encode($data->id) : ''; ?>">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('user_email'); ?></label>
                                <div class="col-md-4">
                                    <input type="email" class="form-control" required="" name="email" value="<?php echo ($data) ? $data->email : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('user_username'); ?></label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" required="" name="username" value="<?php echo ($data) ? $data->username : ''; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('user_group'); ?></label>
                                <div class="col-md-4">
                                    <select class="form-control" name="group">
                                        <?php if ($groups) foreach ($groups->result() as $group) { ?>
                                        <option value="<?php echo $group->name; ?>" <?php echo ($data&& $data->group==$group->id)?'selected':''; ?>><?php echo $group->definition; ?></option>
                                            <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label"><?php echo lang('user_password'); ?></label>
                                <div class="col-md-4">
                                    <input type="password" class="form-control" <?php echo ($data) ? '' : 'required'; ?> id="password" name="password">
                                    <?php if ($data) { ?>
                                        <span class="help-block"><?php echo lang('leave_blank'); ?></span>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Phone</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" required="" name="phone" value="<?php echo ($data) ? $data->phone : ''; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <div class="heading-elements action-left">
                                <a class="btn btn-default" href="<?php echo site_url('settings/users'); ?>"><?php echo lang('button_cancel'); ?></a>
                                <div class="pull-right">
                                    <button type="submit" class="btn btn-primary"><?php echo lang('button_save'); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>