<div class="content-wrapper">
    <div class="page-header page-header-default">
        <div class="page-header-content">
            <div class="page-title">
                <?php echo $breadcrumbs; ?>
                <h2><?php echo lang('user_list_merchant_heading'); ?></h2>
            </div>

            <div class="heading-elements">
                <div class="heading-btn-group">
                 <!--    <a href="<?php echo site_url('settings/merchant_users/form'); ?>" class="btn btn-link btn-float has-text"><i class="icon-plus-circle2 text-primary"></i><span><?php echo lang('user_add_heading'); ?></span></a> -->
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="panel panel-flat">
            <table class="table" id="table" data-url="<?php echo site_url('settings/merchant_users/get_list'); ?>">
                <thead>
                    <tr>
                        <th class="default-sort" data-sort="asc"><?php echo lang('user_username'); ?></th>
                        <th ><?php echo lang('user_email'); ?></th>
                        <th ><?php echo lang('user_group'); ?></th>
                        <th>Merchant</th>
                        <th class="no-sort text-center" style="width: 20px;"><?php echo lang('actions'); ?></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>