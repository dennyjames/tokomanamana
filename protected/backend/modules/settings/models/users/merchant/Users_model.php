<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

	function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->select('aum.id, aum.username, aum.email, agm.definition group_name, m.name AS merchant_name')
                ->join('aauthm_user_to_group augm', 'augm.user_id = aum.id', 'left')
                ->join('aauthm_groups agm', 'agm.id = augm.group_id', 'left')
                ->join('merchants m', 'm.id = aum.merchant_id', 'left')
                ->limit($length, $start);

        return $this->db->get('aauthm_users aum');
    }

    function get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'aum.username';
                break;
            case 1: $key = 'aum.email';
                break;
            case 2: $key = 'agm.definition';
                break;
            case 3: $key = 'merchant_name';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->where_like($search);
        $this->db->join('aauthm_user_to_group augm', 'augm.user_id = aum.id', 'inner')
                ->join('aauthm_groups agm', 'agm.id = augm.group_id', 'inner')
                ->join('merchants m', 'm.id = aum.merchant_id', 'left');
        return $this->db->count_all_results('aauthm_users aum');
    }

    function where_like($search = '') {
        $columns = array('aum.username', 'aum.email', 'agm.definition', 'm.name');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

}