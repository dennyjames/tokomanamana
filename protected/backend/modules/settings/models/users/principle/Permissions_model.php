<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions_model extends CI_Model {

    function get_all($start = 0, $length, $search = '', $order = array()) {
        $this->where_like($search);
        if ($order) {
            $order['column'] = $this->get_alias_key($order['column']);
            $this->db->order_by($order['column'], $order['dir']);
        }
        $this->db->limit($length, $start);

        return $this->db->get('aauthp_perms');
    }

    function get_alias_key($key) {
        switch ($key) {
            case 0: $key = 'name';
                break;
            case 1: $key = 'definition';
                break;
        }
        return $key;
    }

    function count_all($search = '') {
        $this->where_like($search);
        return $this->db->count_all_results('aauthp_perms');
    }

    function where_like($search = '') {
        $columns = array('name', 'definition');
        if ($search) {
            $this->db->group_start();
            foreach ($columns as $column) {
                $this->db->or_like('IFNULL(' . $column . ',"")', $search);
            }
            $this->db->group_end();
        }
    }

    function create_perm($perm, $definition) {
        $check = $this->main->get('aauthp_perms', ['name' => $perm]);
        if($check) {
            return false;
        } else {
            $data = [
                'name' => $perm,
                'definition' => $definition
            ];
            $this->db->insert('aauthp_perms', $data);
            return $this->db->insert_id();
        }
    }

    function update_perm($id, $name = null, $definition = null) {
        $data = [];
        if($name != null) {
            $data['name'] = $name;
        }
        if($definition != null) {
            $data['definition'] = $definition;
        }
        $this->db->where('id', $id);
        $this->db->set($data);
        $this->db->update('aauthp_perms');
    }

    function delete_perms($id) {
        $check = $this->main->get('aauthp_perms', ['id' => $id]);
        if($check) {

            $this->db->where('perm_id', $id);
            $this->db->delete('aauthp_perm_to_group');

            $this->db->where('perm_id', $id);
            $this->db->delete('aauthp_perm_to_user');

            $this->db->where('id', $id);
            $this->db->delete('aauthp_perms');

            return true;

        } else {
            return false;
        }
    }

}
