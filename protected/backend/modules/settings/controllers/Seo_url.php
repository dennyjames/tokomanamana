<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Seo_url extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('setting/seo_url');
        $this->lang->load('seo_url', settings('language'));
        $this->load->model('seo_url_model', 'seo_url');

        $this->data['menu'] = 'setting_seo_url';
    }

    public function index() {
        $this->template->_init();
        $this->template->table();

        $this->breadcrumbs->unshift(lang('setting'), '/');
        $this->breadcrumbs->push(lang('seo_url'), '/settings/seo_url');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(lang('seo_url_heading'));
        $this->load->js('../assets/backend/js/modules/settings/seo_url.js');
        $this->load->view('seo_url/seo_url', $this->data);
    }

    public function get_list() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $start = $this->input->post('start');
        $length = $this->input->post('length');
        $order = $this->input->post('order')[0];
        $search = $this->input->post('search')['value'];
        $additional = $this->input->post('additional_data');
        $draw = intval($this->input->post('draw'));

        $output['data'] = array();
        $datas = $this->seo_url->get_all($start, $length, $search, $order, $additional);
        if ($datas) {
            foreach ($datas->result() as $data) {
                $output['data'][] = array(
                    $data->query,
                    $data->keyword,
                    '<ul class="icons-list">
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-menu7"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="' . site_url('settings/seo_url/form/' . encode($data->id)) . '">' . lang('button_edit') . '</a></li>
                    <li><a href="' . site_url('settings/seo_url/delete/' . encode($data->id)) . '" class="delete">' . lang('button_delete') . '</a></li>
                    </ul>
                    </li>
                    </ul>',
                );
            }
        }
        $output['draw'] = $draw++;
        $output['recordsTotal'] = $this->seo_url->count_all();
        $output['recordsFiltered'] = $this->seo_url->count_all($search, $additional);
        echo json_encode($output);
    }

    public function form($id = '') {
        $this->template->_init();
        $this->template->form();

        $this->data['data'] = array();
        if ($id) {
            $id = decode($id);
            $this->data['data'] = $this->main->get('seo_url', array('id' => $id));
        }

        $this->breadcrumbs->unshift(lang('setting'), '/');
        $this->breadcrumbs->push(lang('seo_url'), 'settings/seo_url');
        $this->breadcrumbs->push(($this->data['data']) ? lang('seo_url_edit_heading') : lang('seo_url_add_heading'), '/');

        $this->data['breadcrumbs'] = $this->breadcrumbs->show();

        $this->output->set_title(($this->data['data']) ? lang('seo_url_edit_heading') : lang('seo_url_add_heading'));
        $this->load->view('seo_url/seo_url_form', $this->data);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('query', 'lang:seo_url_form_query_label', 'trim|required|callback_check_query');
        $this->form_validation->set_rules('keyword', 'lang:seo_url_form_keyword_label', 'trim|required|callback_check_keyword');

        if ($this->form_validation->run() === true) {
            $data = $this->input->post(null, true);

            if (!$data['id']) {
                $save = $this->main->insert('seo_url', $data);
            } else {
                $data['id'] = decode($data['id']);
                $save = $this->main->update('seo_url', $data, array('id' => $data['id']));
            }

            if ($save !== false) {
                $return = array('message' => sprintf(lang('seo_url_save_success_message'), $data['query']), 'status' => 'success', 'redirect' => site_url('settings/seo_url'));
            } else {
                $return = array('message' => sprintf(lang('seo_url_save_error_message'), $data['query']), 'status' => 'error');
            }
        } else {
            $return = array('message' => validation_errors(), 'status' => 'error');
        }
        echo json_encode($return);
    }

    public function delete($id) {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');

        $id = decode($id);
        $data = $this->main->get('seo_url', array('id' => $id));

        $delete = $this->main->delete('seo_url', array('id' => $id));
        if ($delete) {
            $return = array('message' => sprintf(lang('seo_url_delete_success_message'), $data->query), 'status' => 'success');
        } else {
            $return = array('message' => lang('seo_url_delete_error_message'), 'status' => 'error');
        }

        echo json_encode($return);
    }

    public function check_query($str) {
        if ($this->input->post('id')) {
            $check = $this->main->get('seo_url', array('query' => $str, 'id !=' => decode($this->input->post('id'))));
        } else {
            $check = $this->main->get('seo_url', array('query' => $str));
        }
        if ($check) {
            $this->form_validation->set_message('check_query', sprintf(lang('seo_url_query_exist_message'), $str));
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function check_keyword($str) {
        if ($this->input->post('id')) {
            $check = $this->main->get('seo_url', array('keyword' => $str, 'id !=' => decode($this->input->post('id'))));
        } else {
            $check = $this->main->get('seo_url', array('keyword' => $str));
        }
        if ($check) {
            $this->form_validation->set_message('check_keyword', sprintf(lang('seo_url_keyword_exist_message'), $str));
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
