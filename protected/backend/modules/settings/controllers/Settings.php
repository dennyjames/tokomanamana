<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Settings extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->aauth->control('setting');
        $this->data['menu'] = 'setting_base';
        
        $this->lang->load('settings', settings('language'));
    }

    public function index() {
//        $this->session->unset_userdata('tnkSettings');
        
        $this->breadcrumbs->push(lang('setting'), '/');
        $this->data['breadcrumbs'] = $this->breadcrumbs->show();
        
        $this->template->_init();
        $this->template->form();
        $this->load->js('../assets/backend/js/modules/settings/settings.js');
        $this->output->set_title(lang('heading'));
        $this->load->view('settings', $this->data);
    }

    public function save() {
        $this->input->is_ajax_request() or exit('No direct post submit allowed!');
        
        $data = $this->input->post(null, true);
        
//        print_r($data['footer']);
        
        foreach ($data as $key => $value) {
            if($key=='footer'){
                $value = json_encode($value);
            }
            $this->main->update('settings', array('value' => $value), array('key' => $key));
        }
        $this->main->delete('sessions',array('id !='=> session_id()));
        $this->session->unset_userdata('tnkSettings');
        
        $return = array('message' => "Pengaturan berhasil disimpan.", 'status' => 'success', 'redirect' => '');
        echo json_encode($return);
    }

}
