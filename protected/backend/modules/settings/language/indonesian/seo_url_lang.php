<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['seo_url_heading'] = 'SEO Url';
$lang['seo_url_list_heading'] = 'Daftar SEO Url';
$lang['seo_url_add_heading'] = 'Tambah SEO Url';
$lang['seo_url_edit_heading'] = 'Edit SEO Url';

$lang['seo_url_form_query_label'] = 'Query';
$lang['seo_url_form_keyword_label'] = 'Keyword';

$lang['seo_url_query_th'] = 'Query';
$lang['seo_url_keyword_th'] = 'Keyword';

$lang['seo_url_save_success_message'] = "SEO Url '%s' berhasil disimpan.";
$lang['seo_url_save_error_message'] = "SEO Url '%s' gagal disimpan.";
$lang['seo_url_delete_success_message'] = "SEO Url '%s' berhasil dihapus.";
$lang['seo_url_delete_error_message'] = "SEO Url '%s' gagal dihapus.";
$lang['seo_url_query_exist_message'] = "Query '%s' sudah terdaftar.";
$lang['seo_url_keyword_exist_message'] = "Keyword '%s' sudah terdaftar untuk query lain.";