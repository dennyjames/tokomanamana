<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$lang['heading'] = 'Pengaturan';

$lang['general'] = 'Umum';
$lang['store'] = 'Toko';
$lang['terms'] = 'Syarat dan Ketentuan';
$lang['footer'] = 'Footer';

$lang['meta_title'] = 'Meta Judul';
$lang['meta_description'] = 'Meta Deskripsi';
$lang['meta_keyword'] = 'Meta Keyword';
$lang['layout_banner'] = 'Layout Banner';
$lang['store_name'] = 'Nama Toko';
$lang['store_description'] = 'Deskripsi';
$lang['store_address'] = 'Alamat';
$lang['email'] = 'Email';
$lang['telephone'] = 'No. Telepon';
$lang['logo'] = 'Logo';
$lang['term_customer_register'] = 'Agreement Customer';
$lang['term_merchant_register'] = 'Agreement Merchant';

$lang['save_success'] = "Pengaturan berhasil disimpan.";
$lang['save_error'] = "Pengaturan gagal disimpan.";