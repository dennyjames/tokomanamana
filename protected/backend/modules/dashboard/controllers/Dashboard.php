<?php

defined('BASEPATH') or exit('No direct script access allowed!');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('dashboard_model','dashboard');
        $this->data['menu'] = 'dashboard';
    }

    public function index() {
        $this->data['summary'] = $this->dashboard->sales_summary();
        $this->data['last_orders'] = $this->dashboard->last_orders();
//        $this->data['last_search'] = $this->dashboard->last_search();
//        $this->data['top_search'] = $this->dashboard->top_search();
        $this->data['bestsellers'] = $this->dashboard->bestsellers();
        $this->data['mostviewed'] = $this->dashboard->mostviewed();
        
        $this->template->_init();
//        $this->load->js('assets/js/plugins/visualization/d3/d3.min.js');
//        $this->load->js('assets/js/plugins/visualization/c3/c3.min.js');
//        $this->load->js('assets/js/modules/dashboard.js');
        $this->output->set_title('Dasbor');
        $this->load->view('dashboard', $this->data);
    }

}
