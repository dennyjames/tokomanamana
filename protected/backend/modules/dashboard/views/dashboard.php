<div class="content-wrapper">
    <div class="content">
        <div class="row">
            <div class="col-sm-6">
                <div class="panel">
                    <div class="panel-heading">
                        <h6 class="panel-title">Penjualan Bulan Ini</h6>
                    </div>
                    <div class="panel-body">
                        <div class="row text-center">
                            <div class="col-xs-5">
                                <h5 class="text-semibold no-margin"><?php echo number($summary->transaction); ?></h5>
                                <span class="text-muted text-size-small">Transaksi</span>
                            </div>
                            <div class="col-xs-7">
                                <h5 class="text-semibold no-margin"><?php echo rupiah($summary->sales); ?></h5>
                                <span class="text-muted text-size-small">Penjualan</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading">
                        <h6 class="panel-title">10 Pesanan Terakhir</h6>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Pelanggan</th>
                                    <th>Merchant</th>
                                    <th>Item</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($last_orders->num_rows() > 0) { ?>
                                    <?php foreach ($last_orders->result() as $order) { ?>
                                        <tr>
                                            <td><?php echo $order->code; ?></td>
                                            <td><?php echo $order->customer_name; ?></td>
                                            <td><?php echo $order->merchant_name; ?></td>
                                            <td><?php echo number($order->quantity); ?></td>
                                            <td><?php echo rupiah($order->total); ?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } else { ?>
                                    <tr>
                                        <td class="text-center" colspan="4">Tidak ada transaksi</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
<!--                <div class="panel">
                    <div class="panel-heading">
                        <h6 class="panel-title">Penjualan Bulan Ini</h6>
                    </div>
                    <div class="panel-body">
                        <div class="chart-container">
                            <div class="chart" id="sales"></div>
                        </div>
                    </div>
                </div>-->
                <div class="tab-content-bordered content-group">
                    <ul class="nav nav-tabs nav-tabs-highlight nav-lg nav-justified">
                        <li class="active"><a href="#bestsellers" data-toggle="tab">Bestsellers</a></li>
                        <li><a href="#most-viewed" data-toggle="tab">Banyak Dilihat</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane has-padding active" id="bestsellers">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Desain</th>
                                            <th>Harga</th>
                                            <th>Terjual</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if ($bestsellers->num_rows() > 0) { ?>
                                            <?php foreach ($bestsellers->result() as $bestseller) { ?>
                                                <tr>
                                                    <td><?php echo $bestseller->name; ?></td>
                                                    <td><?php echo rupiah($bestseller->price); ?></td>
                                                    <td><?php echo number($bestseller->quantity); ?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td class="text-center" colspan="3">Tidak ada data</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane has-padding" id="most-viewed">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Desain</th>
                                            <th>Harga</th>
                                            <th>Dilihat</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if ($mostviewed->num_rows() > 0) { ?>
                                            <?php foreach ($mostviewed->result() as $viewed) { ?>
                                                <tr>
                                                    <td><?php echo $viewed->name; ?></td>
                                                    <td><?php echo rupiah($viewed->price); ?></td>
                                                    <td><?php echo number($viewed->viewed); ?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td class="text-center" colspan="3">Tidak ada data</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>