<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    function sales_summary() {
        $status = settings('order_finish_status');
        $this->db->select('IFNULL(COUNT(id),0) transaction, IFNULL(SUM(total),0) sales')
                ->where(array('order_status' => $status, 'YEAR(date_added)' => date('Y'), 'MONTH(date_added)' => date('m')));
        return $this->db->get('order_invoice')->row();
    }

    function last_orders() {
        $this->db->select('oi.code, oi.order, oi.total, c.fullname customer_name, oi.customer, m.name merchant_name, oi.merchant, op.quantity')
                ->join('customers c', 'oi.customer = c.id', 'left')
                ->join('merchants m', 'oi.merchant = m.id', 'left')
                ->join('(SELECT SUM(quantity) quantity, invoice FROM order_product op GROUP BY invoice) op', 'op.invoice = oi.id', 'left')
                ->where(array('YEAR(oi.date_added)' => date('Y'), 'MONTH(oi.date_added)' => date('m')))
                ->limit(10)
                ->order_by('oi.id DESC');
        return $this->db->get('order_invoice oi');
    }

    function last_search() {
        $this->db->select('*, COUNT(keyword) uses')
                ->limit(5)
                ->group_by('keyword')
                ->order_by('time DESC');
        return $this->db->get('search');
    }

    function top_search() {
        $this->db->select('*, COUNT(keyword) uses')
                ->limit(5)
                ->group_by('keyword')
                ->order_by('uses DESC');
        return $this->db->get('search');
    }

    function bestsellers() {
        $status = settings('order_finish_status');
        $this->db->select('op.name, SUM(quantity) quantity, op.price')
                ->join('order_invoice oi', 'op.invoice = oi.id', 'left')
                ->limit(15)
                ->group_by('op.product')
                ->where('oi.order_status', $status)
                ->order_by('quantity DESC');
        return $this->db->get('order_product op');
    }

    function mostviewed() {
        $this->db->select('name, price, viewed')
                ->where('status', 1)
                ->order_by('viewed desc')
                ->limit(15);
        return $this->db->get('products');
    }

}
