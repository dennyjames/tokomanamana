<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');


if (!function_exists('menu_active')) {

    function menu_active($menu, $active) {
        if (substr($menu, 0, strlen($active)) == $active) {
            return 'active';
        } else {
            return FALSE;
        }
    }

}

function check_url($keyword, $query = '') {
    $CI = get_instance();
    $CI->load->database();

    if ($query)
        $CI->db->where('query !=', $query);
    $query = $CI->db->where('keyword', $keyword)->get('seo_url');
    return($query->num_rows() > 0) ? true : false;
}

function tinymce_get_url_files($text) {
    return str_replace('files/', base_url('../files/'), $text);
}

function tinymce_parse_url_files($text) {
    return str_replace(base_url('../files/'), 'files/', $text);
}
