<?php

function get_user() {
    $CI = & get_instance();
    if ($CI->aauth->is_loggedin()) {
        if (uri_string() == 'auth/login') {
            redirect('');
        }
        if (!$CI->session->has_userdata('user')) {
            $user = $CI->aauth->get_user();
            $CI->session->set_userdata('user', $user, 3600);
        }
        $CI->data['user'] = $CI->session->userdata('user');
        $CI->data['notifications'] = array();
        $orders = $CI->db->select('o.id, o.code, o.payment_status, c.fullname customer_name')
                ->where_in('o.payment_status', ['Pending', 'Confirmed'])
                ->join('customers c', 'c.id = o.customer')
                ->order_by('o.date_modified desc')
                ->get('orders o');
        if ($orders->num_rows() > 0) {
            foreach ($orders->result() as $order) {
                $notif['url'] = site_url('orders/view/' . encode($order->id));
                if ($order->payment_status == 'Pending') {
                    $notif['msg'] = 'Pesanan baru "' . $order->code . '" dari ' . $order->customer_name . '.';
                } else {
                    $notif['msg'] = 'Konfirmasi pembayaran pesanan "' . $order->code . '" telah dilakukan.';
                }
                array_push($CI->data['notifications'], $notif);
            }
        }
        $CI->data['withdrawals'] = array();
        $withdrawals = $CI->db->select('bw.*, m.name merchant_name')
                ->join('merchants m', 'bw.merchant = m.id', 'left')
                ->where('bw.status', 'Pending')
                ->get('merchant_balance_withdrawal bw');
        if ($withdrawals->num_rows() > 0) {
            foreach ($withdrawals->result() as $wd) {
                array_push($CI->data['withdrawals'], 'Permintaan Tarik Dana dari Merchant "'.$wd->merchant_name.'" sebesar '. rupiah($wd->amount));
            }
        }
        $CI->data['merchant_notifs'] = array();
        $merchant_notifs = $CI->db->select('m.id,m.name')
                ->where('status', 0)
                ->order_by('id','desc')
                ->get('merchants m');
        if ($merchant_notifs->num_rows() > 0) {
            foreach ($merchant_notifs->result() as $mn) {
                array_push($CI->data['merchant_notifs'], array('id'=>$mn->id,'name'=>$mn->name));
            }
        }
    } else {
        if (uri_string() !== 'auth/login' && uri_string() !== 'auth/request_otp_login') {
            if($CI->uri->segment(1) != 'crons'){
                redirect('auth/login?back=' . uri_string());
            }
        }
    }
}
