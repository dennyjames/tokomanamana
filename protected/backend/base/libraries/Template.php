<?php

(defined('BASEPATH')) or exit('No direct script access allowed');

class Template {

    public function __construct() {
        $this->ci = & get_instance();
    }

    public function _init($template = 'default') {
        $this->ci->output->set_template($template);

        $this->ci->load->css('https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900');
        $this->ci->load->css('../assets/backend/css/icons/icomoon/styles.css');
        $this->ci->load->css('../assets/backend/css/bootstrap.css');
        $this->ci->load->css('../assets/backend/css/core.css');
        $this->ci->load->css('../assets/backend/css/components.css');
        $this->ci->load->css('../assets/backend/css/colors.css');
        $this->ci->load->css('../assets/backend/css/custom.css');

        $this->ci->load->js('../assets/backend/js/plugins/loaders/pace.min.js');
        $this->ci->load->js('../assets/backend/js/core/libraries/jquery.min.js');
        $this->ci->load->js('../assets/backend/js/core/libraries/bootstrap.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/loaders/blockui.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/sweet_alert.min.js');
        $this->ci->load->js('../assets/backend/js/language/' . settings('language') . '.js');
        $this->ci->load->js('../assets/backend/js/core/app.js');
    }

    public function table() {
        $this->ci->load->js('../assets/backend/js/plugins/tables/datatables/datatables.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/tables/table.js');
    }

    public function form() {
        $this->ci->load->css('../assets/backend/css/plugins/datepicker.min.css');
        
        $this->ci->load->js('../assets/backend/js/plugins/forms/styling/uniform.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/jquery.number.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/tinymce/tinymce.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/bootstrap_select.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/select2.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/tagsinput.min.js');
//        $this->ci->load->js('../assets/backend/js/plugins/forms/bootstrap-typeahead.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/styling/switch.min.js');
//        $this->ci->load->js('../assets/backend/js/plugins/forms/pickadate/picker.js');
//        $this->ci->load->js('../assets/backend/js/plugins/forms/pickadate/picker.date.js');
//        $this->ci->load->js('../assets/backend/js/plugins/forms/pickadate/legacy.js');
//        $this->ci->load->js('../assets/backend/js/plugins/forms/pickadate/translations/' . settings('language') . '.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/validation/validate.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/validation/localization/messages_' . settings('language') . '.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/jquery.form.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/datepicker.min.js');
        $this->ci->load->js('../assets/backend/js/plugins/forms/form.js');
    }

}
