<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Crons extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function confirm_bca_pay(){
        $this->load->library('Bca');
        $this->load->model('model');
        $this->load->model('orders');

        $bca_login_api = Bca::login();
            $bca_login_decode =  json_decode($bca_login_api, true);
            if($bca_login_decode['code'] == 200) {
                $today = date("Y-m-d");
                $yesterday = date('Y-m-d',strtotime("-1 days"));
                $bca_transfer_api = Bca::get_statement($bca_login_decode['body']['access_token'],$yesterday,$today);
                //$bca_transfer_api = Bca::get_balance($bca_login_decode['body']['access_token']);
                
                $bca_transfer_decode =  json_decode($bca_transfer_api, true);

                foreach ($bca_transfer_decode['body']['Data'] as $bca_response) { //TransactionAmount
                    $amount_bca = $bca_response['TransactionAmount'];
                    $get = $this->model->check_kode_unik($amount_bca)->result();
                    if($get){
                        foreach ($get as $item) {
                            $id = $item->id;
                            $this->main->update('orders', array('payment_status' => 'Paid'), array('id' => $id));
                            $data = $this->orders->get($id);

                            $customer = $this->main->get('customers', array('id' => $data->customer));
                            $this->data['order'] = $data;
                            $this->data['customer'] = $customer;
                            $invoices = $this->main->gets('order_invoice', array('order' => $id));

                            if ($invoices) {
                                foreach ($invoices->result() as $invoice) {
                                    if($invoice->shipping_courier == 'pickup') {
                                        $due_date_inv = date('Y-m-d H:i:s', strtotime('+1 days'));
                                    } else {
                                        $due_date_inv = date('Y-m-d H:i:s', strtotime('+2 days'));
                                    }
                                    $this->main->update('order_invoice', array('order_status' => settings('order_payment_received_status'), 'due_date' => $due_date_inv), array('id' => $invoice->id));
                                    $this->main->insert('order_history', array('order' => $id, 'invoice' => $invoice->id, 'order_status' => settings('order_payment_received_status')));

                                    $this->data['invoice'] = $invoice;
                                    $this->data['merchant'] = $this->main->get('merchants', array('id' => $invoice->merchant));

                                    //update stock merchant
                                    if ($this->data['merchant']->type == 'merchant' || $this->data['merchant']->type == 'branch') {
                                        $products = $this->main->gets('order_product', array('invoice' => $invoice->id));
                                        if ($products) {
                                            foreach ($products->result() as $product) {
                                                $product_detail = $this->db->query("select type, package_items from products where id = $product->product ");
                                                $row = $product_detail->row();
                                                if($row->type == 'p') {
                                                    $package_items = json_decode($row->package_items, true);
                                                    foreach ($package_items as $package) {
                                                        $this->db->query("UPDATE product_merchant SET quantity = (quantity - $product->quantity) WHERE product = ".$package['product_id']." AND merchant = " . $this->data['merchant']->id); 
                                                    }
                                                } else {
                                                    $this->db->query("UPDATE product_merchant SET quantity = (quantity - $product->quantity) WHERE product = $product->product AND merchant = " . $this->data['merchant']->id);
                                                }
                                            }
                                        }
                                    }

                                    $merchant_user = $this->main->get('merchant_users', array('id' => $this->data['merchant']->auth));
                                    $message = $this->load->view('email/merchant/new_order', $this->data, true);
                                    $cronjob = array(
                                        'from' => settings('send_email_from'),
                                        'from_name' => settings('store_name'),
                                        'to' => $merchant_user->email,
                                        'subject' => 'Pesanan baru dari ' . $customer->fullname,
                                        'message' => $message
                                    );
                                    $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                                    if ($merchant_user->verification_phone) {
                                        $sms = json_decode(settings('sprint_sms'), true);
                                        $sms['m'] = 'Pesanan baru dari ' . $customer->fullname . '. Segera proses sebelum ' . get_date_indo_full($invoice->due_date) . ' WIB.';
                                        $sms['d'] = $merchant_user->phone;
                                        $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                                    }
                                }
                            }
                            $this->data['invoices'] = $invoices;
                            $message = $this->load->view('email/transaction/payment_success', $this->data, true);
                            $cronjob = array(
                                'from' => settings('send_email_from'),
                                'from_name' => settings('store_name'),
                                'to' => $customer->email,
                                'subject' => 'Pembayaran Sukses #' . $data->code,
                                'message' => $message
                            );
                            $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
                            if ($customer->verification_phone) {
                                $sms = json_decode(settings('sprint_sms'), true);
                                $sms['m'] = 'Pembayaran sebesar ' . rupiah($data->total) . ' telah berhasil. Pesanan Anda akan diteruskan ke penjual.';
                                $sms['d'] = $this->data['customer']->phone;
                                $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                            }
                            $return = array('message' => 'Pembayaran pesanan ' . $data->code . ' berhasil dikonfirmasi.', 'status' => 'success');
                        }
                    }
                    
                }
                //var_dump($bca_transfer_decode);
            }
    }

    public function cancel_order() {
        $orders = $this->db->where('due_date <= NOW()')->where('payment_status', 'Pending')->get('orders');
        if ($orders->num_rows() > 0) {
            $status = settings('order_cancel_status');
            foreach ($orders->result() as $order) {
                $this->main->update('orders', array('payment_status' => 'Expired'), array('id' => $order->id));
                $this->main->update('order_invoice', array('order_status' => $status), array('order' => $order->id));
                $invoices = $this->main->gets('order_invoice', array('order' => $order->id));
                if ($invoices) {
                    foreach ($invoices->result() as $invoice) {
                        $this->main->insert('order_history', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => $status));
                    }
                }
            }
        }
    }

}
