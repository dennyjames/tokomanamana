<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Checkout_model extends CI_Model {

    function getShippings($district) {
        $this->db->select('s.*, sc.cost, sc.district, sc.estimate')
                ->join('shippings s', 's.id = sc.shipping', 'left');
        $query = $this->db->get_where('shipping_cost sc', array('district' => $district));
        return ($query->num_rows() > 0) ? $query : false;
    }

    function getOrderByCode($code) {
        $this->db->select("o.*, os.name order_status_name, os.color order_status_color, sd.name shipping_district_name, sc.name shipping_city_name, sp.name shipping_province_name, s.name shipping_courier_name", FALSE)
                ->join('order_status os', 'o.order_status = os.id', 'left')
                ->join('shipping_cities sc', 'sc.id = o.shipping_city', 'left')
                ->join('shipping_districts sd', 'sd.id = o.shipping_district', 'left')
                ->join('shipping_provincies sp', 'sp.id = o.shipping_province', 'left')
                ->join('shippings s', 's.id = o.shipping_courier', 'left')
                ->where('o.code', $code);
        $query = $this->db->get('orders o');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_order_by_id($id) {
        $this->db->select("o.*, os.name order_status_name, os.color order_status_color, sd.name shipping_district_name, sc.name shipping_city_name, sp.name shipping_province_name, s.name shipping_courier_name", FALSE)
                ->join('order_status os', 'o.order_status = os.id', 'left')
                ->join('shipping_cities sc', 'sc.id = o.shipping_city', 'left')
                ->join('shipping_districts sd', 'sd.id = o.shipping_district', 'left')
                ->join('shipping_provincies sp', 'sp.id = o.shipping_province', 'left')
                ->join('shippings s', 's.id = o.shipping_courier', 'left')
                ->where('o.id', $id);
        $query = $this->db->get('orders o');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_merchants($product, $quantity, $pricing_level = 1) {
        $location = array('lat' => '-6.186486399999999','lng' =>'106.83409110000002');
        return $this->db->query("SELECT *, (6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) AS distance FROM merchants WHERE status = 1 AND (type = 'merchant' OR type = 'distributor') AND id IN (SELECT merchant FROM product_merchant WHERE quantity >= $quantity AND product = $product " . ($pricing_level ? "AND pricing_level = 1" : "") . ") HAVING distance < (CASE
        WHEN type='merchant' THEN 40
        ELSE 200
        END ) ORDER BY distance LIMIT 5");
//        return $this->db->get('orders o');
    }

    function get_merchant($id) {
        $location = $this->session->userdata('location');
        $this->db->select("m.id, m.name, c.name city, d.name district, IFNULL(r.rating,0) rating, m.username,(CASE WHEN lat != '' AND lng != '' THEN(6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) ELSE 0 END) AS distance")
                ->join('cities c', 'm.city = c.id', 'left')
                ->join('districts d', 'm.district = d.id', 'left')
                ->join('(SELECT merchant, ((SUM(rating_speed)+SUM(rating_service)+SUM(rating_accuracy))/(COUNT(id)*3)) rating FROM product_review
	WHERE merchant = ' . $id . ') r', 'r.merchant = m.id', 'left')
                ->where('m.id', $id)
                ->where('m.status', 1);
        return $this->db->get('merchants m')->row();
    }

    function get_branch($group) {
        $this->db->join('merchants m', 'mg.branch = m.id', 'left')
                ->where('mg.id', $group)
                ->select('m.*');
        return $this->db->get('merchant_groups mg')->row();
    }
    function check_branch_stock($merchant,$product, $quantity, $pricing_level = 1){
        $q = $this->db->query("SELECT * FROM product_merchant WHERE merchant = '$merchant' AND quantity >= '$quantity' AND product = '$product' ");
        return $q;
    }
    function customer_address($customer) {
        $this->db->select('ca.*, p.name province_name, c.name city_name, d.name district_name')
                ->join('provincies p', 'p.id = ca.province')
                ->join('cities c', 'c.id = ca.city')
                ->join('districts d', 'd.id = ca.district')
                ->where('ca.customer', $customer);
        return $this->db->get('customer_address ca');
    }

}
