<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Product_model extends CI_Model {

    public $table = 'products';
    public $primary_key = 'id';

    function get_product($id) {
        $this->db->select("p.*, pi.image, b.name brand_name, m.name merchant_name, p.quantity", FALSE)
                ->join('product_image pi', 'p.id = pi.product AND primary = 1', 'left')
                ->join('brands b', 'b.id = p.brand', 'left')
                ->join('merchants m', 'm.id = p.merchant', 'left')
                ->where('p.status', 1)
                ->where('p.id', $id);
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query->row() : false;
    }

    function get_rating($product) {
        $this->db->select('IFNULL((SUM(pr.rating_speed)+SUM(pr.rating_service)+SUM(pr.rating_accuracy))/(COUNT(pr.id)*3),0) rating, IFNULL(COUNT(pr.id),0) review')
                ->where('pr.product', $product)
                ->where('pr.status', 1);
        return $this->db->get('product_review pr')->row();
    }

    function get_images($id) {
        $this->db->where('product', $id);
//                ->where('primary', 0);
        $query = $this->db->get('product_image');
        return ($query->num_rows() > 0) ? $query : false;
    }

    function get_option_groups($product, $product_option = 0) {
        $this->db->select('og.id group, og.type, og.name group_name, o.id option, o.value option_name, o.color, po.id product_option, po.price, po.weight, po.default')
                ->join('product_option_combination poc', 'po.id = poc.product_option', 'left')
                ->join('options o', 'poc.option = o.id', 'left')
                ->join('option_group og', 'o.group = og.id', 'left')
                ->where('po.product', $product)
                ->group_by('o.group, po.id')
                ->order_by('og.sort_order ASC, o.sort_order ASC, og.name ASC');
        if ($product_option) {
            $this->db->where('po.id', $product_option);
        }
        return $this->db->get('product_option po');
    }

//    function get_combination_images($product) {
//        $images = array();
//        $this->db->select("poi.product_image, poi.product_option, pi.image, pi.primary")
//                ->join('product_image pi', 'pi.id = poi.product_image', 'left')
//                ->where_in('poi.product_option', 'SELECT id FROM product_option	WHERE product = ' . $product);
//        $query = $this->db->get('product_option_image poi');
//        if ($query->num_rows() > 0) {
//            foreach ($query->result_array() as $image) {
//                $images[$image['product_option']][] = $image;
//            }
//        }
//        return $images;
//    }

    function get_product_option_id($product, $options) {
        $this->db->select('poc.product_option')
                ->join('product_option po', 'po.id = poc.product_option', 'left')
                ->where('po.product', $product)
                ->where_in('option', $options)
                ->group_by('poc.product_option')
                ->having('COUNT(poc.product_option)', count($options));
        $query = $this->db->get('product_option_combination poc');
        return ($query->num_rows() > 0) ? $query->row()->product_option : false;
    }

    function get_product_option_images($product_option) {
        $this->db->select("pi.id, pi.image")
                ->join('product_image pi', 'pi.id = poi.product_image', 'left')
                ->where('poi.product_option', $product_option)
                ->order_by('pi.sort_order asc, pi.primary desc');
        $query = $this->db->get('product_option_image poi');
        return ($query->num_rows() > 0) ? $query : false;
    }

    function get_relations($category, $product, $limit = 10, $categoryParent = 0) {
        $this->db->select("p.id, pi.image, price, price_old, discount, name", FALSE)
                ->join('product_image pi', 'p.id = pi.product AND primary = 1', 'left')
                ->where("category", $category)
                ->where('p.id !=', $product)
                ->where('p.status', 1)
                ->order_by('RAND()')
                ->limit($limit);
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query : $this->get_also_boughts($categoryParent, $product, $limit);
    }

    function get_also_boughts($category, $product, $limit = 6) {
        $this->db->select("p.id, pi.image, price_old, price, discount, name", FALSE)
                ->join('product_image pi', 'p.id = pi.product AND primary = 1', 'left')
                ->where("category IN (SELECT category FROM category_path WHERE path = $category)")
                ->where('p.id !=', $product)
                ->where('p.status =', 1)
                ->order_by('RAND()')
                ->limit($limit);
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query : false;
    }

    function get_products_by_id($products = array()) {
        $this->db->select("p.id, pi.image, price_old, price, discount, name", FALSE)
                ->join('product_image pi', 'p.id = pi.product AND primary = 1', 'left')
                ->where_in('p.id', $products)
                ->where('p.status', 1);
        $query = $this->db->get('products p');
        return ($query->num_rows() > 0) ? $query : false;
    }

    function get_product_features($product) {
        $this->db->select('pf.product, f.id, type, name, pf.value')
                ->join('features f', 'pf.feature = f.id', 'left')
                ->group_by('pf.feature')
                ->order_by('f.sort_order ASC')
                ->where('pf.product', $product);
        $query = $this->db->get('product_feature pf');
        return($query->num_rows() > 0) ? $query : false;
    }

    function get_product_feature_variants($product, $feature) {
        $this->db->select('pfv.*')
                ->join('product_feature_variant pfv', 'ptf.feature = pfv.feature AND ptf.variant = pfv.id', 'left')
                ->where('ptf.product', $product)
                ->where('ptf.feature', $feature);
        $query = $this->db->get('product_to_feature ptf');
        return($query->num_rows() > 0) ? $query : false;
    }

//    function get_product_options($product) {
//        $this->db->select('po.*, o.name, o.type')
//                ->join('options o', 'po.option = o.id')
//                ->where('po.product', $product);
//        $query = $this->db->get('product_option po');
//        return($query->num_rows() > 0) ? $query : false;
//    }
//    function get_product_option_variants($product,$option) {
//        $this->db->select('pov.*, ov.value, ov.color')
//                ->join('option_variant ov', 'pov.option_variant = ov.id')
//                ->where('pov.product', $product)
//                ->where('pov.product_option', $option);
//        $query = $this->db->get('product_option_variant pov');
//        return($query->num_rows() > 0) ? $query : false;
//    }
//    
//    function get_product_option_variant_full($option, $variant){
//        $this->db->select('pov.id, pov.product_option, pov.option_variant, pov.price, pov.weight, po.option, o.type, o.name, ov.value, ov.color')
//                ->join('product_option po','po.id = pov.product_option','left')
//                ->join('options o','o.id = po.option','left')
//                ->join('option_variant ov','ov.id = pov.option_variant','left')
//                ->where('pov.product_option',$option)
//                ->where('pov.option_variant',$variant);
//        $query = $this->db->get('product_option_variant pov');
//        return($query->num_rows() > 0) ? $query->row_array() : false;
//    }

    function get_reviews($product) {
        $customer = ($this->ion_auth->logged_in()) ? ' AND prl.customer = ' . $this->data['user']->id . ' ' : '';
        $this->db->select('pr.id, pr.rating, pr.title, pr.description, pr.like, pr.dislike, pr.date_added, c.fullname name, prl.likedislike')
                ->join('customers c', 'pr.customer = c.id', 'left')
                ->join('product_review_likedislike prl', 'pr.id = prl.product_review' . $customer, 'left')
                ->where('pr.product', $product)
                ->where('pr.status', 1)
                ->order_by('pr.date_added DESC');
        $query = $this->db->get('product_review pr');
        return($query->num_rows() > 0) ? $query : FALSE;
    }

    function get_review_headers($product) {
        $query = $this->db->query("SELECT 5 rating, COUNT(id) total FROM product_review WHERE rating = 5 AND product = $product AND status = 1 UNION
SELECT 4 rating, COUNT(id) total FROM product_review WHERE rating = 4 AND product = $product AND status = 1 UNION
SELECT 3 rating, COUNT(id) total FROM product_review WHERE rating = 3 AND product = $product AND status = 1 UNION
SELECT 2 rating, COUNT(id) total FROM product_review WHERE rating = 2 AND product = $product AND status = 1 UNION
SELECT 1 rating, COUNT(id) total FROM product_review WHERE rating = 1 AND product = $product AND status = 1");
        return $query;
    }

    function add_viewed($product) {
        $this->db->set('viewed', 'viewed + ' . 1, FALSE);
        $this->db->where('id', $product);
        $this->db->update('products');
        return true;
    }

    function get_merchants($product) {
        $this->db->select('m.id, pm.quantity, pm.price_old, pm.price, pm.discount, m.name name, c.name city')
                ->join('merchants m', 'm.id = pm.merchant AND m.status = 1', 'left')
                ->join('cities c', 'c.id = m.city', 'left')
                ->where('pm.product', $product)
                ->where('pm.status', 1);
        return $this->db->get('product_merchant pm');
    }

    function price_in_level($product, $group, $qty) {
        $this->db->select('pplg.price')
                ->join('product_price_level ppl', 'ppl.id = pplg.product_price_level', 'left')
                ->where('merchant_group', $group)
                ->where('product', $product)
                ->where('min_qty <=', $qty)
                ->order_by('min_qty', 'desc')
                ->limit(1);
        $query = $this->db->get('product_price_level_groups pplg');
        return ($query->num_rows() > 0) ? $query->row()->price : false;
    }

    function price_level_group($product, $group = 0) {
        $this->db->select('pclg.*, pcl.min_qty, pcl.product')
                ->join('product_price_level pcl', 'pcl.id = pclg.product_price_level', 'left')
                ->where_in('product_price_level', '(SELECT id FROM product_price_level WHERE product = ' . $product . ')', false)
                ->where('merchant_group', $group)
                ->order_by('pcl.min_qty asc');
        return $this->db->get('product_price_level_groups pclg');
    }

}
