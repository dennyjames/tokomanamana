<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Category extends REST_Controller {
    
    public $table = "categories";
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        
    }
    public function total_get()
    {

        $this->db->where('active', '1');
        $album = $this->db->count_all($this->table);
        $this->response($album, 200);
    }

    public function all_get()
    {
        $limit = $this->get('limit');
        if($limit){
            $this->db->limit($limit);
        }
        $this->db->select('id,name');
        $this->db->where('active', '1');
        $this->db->where('status_global', '0');
        $this->db->order_by('name','asc');
        $q = $this->db->get($this->table)->result_array();
        $this->response($q, 200);
    }

    public function all_product_get()
    {
        $limit = $this->get('limit');
        $cat_id = $this->get('cat_id');
        
        if($limit){
            $this->db->limit($limit);
        }
        $this->db->select('id,name');
        $this->db->where('active', '1');
        if($cat_id){
            $this->db->where('id', $cat_id);
        }
        $this->db->order_by('name','asc');
        $q = $this->db->get($this->table)->result_array();
        if(count($q) > 0){
            foreach ($q as $key => $val) {
                $this->db->select('a.id,a.name,a.code,a.barcode,a.category as category_id,a.brand,a.weight,
                a.length,a.width,a.height,a.price_old,a.price,a.discount,a.quantity,a.package_items,a.viewed,
                a.date_added');
                $this->db->where('a.status', '1');
                $this->db->where('a.status_global', '0');
                $this->db->where('a.category', $val['id']);
                $this->db->order_by('a.name','ASC');
                $q[$key]['products']= $this->db->get('products as a')->result_array();
            }
            $this->response($q, 200);
        } else {
            $resp = array('status' => 'failed','msg'=>'Category not found!');
            $this->response($resp, 400);
        }
       
    }

    public function all_stock_get()
    {
        $limit = $this->get('limit');
        $cat_id = $this->get('cat_id');
        $prod_id = $this->get('prod_id');
        if($limit){
            $this->db->limit($limit);
        }
        $this->db->select('id,name');
        $this->db->where('active', '1');
        $this->db->where('status_global', '0');
        if($cat_id){
            $this->db->where('id', $cat_id);
        }
        $this->db->order_by('name','asc');
        $q = $this->db->get($this->table)->result_array();
        foreach ($q as $key => $val) {
            $this->db->select('a.id,a.name,a.code,a.barcode,a.category as category_id,a.brand,a.weight,
            a.length,a.width,a.height,a.price_old,a.price,a.discount,a.quantity,a.package_items,a.viewed,
            a.date_added');
            $this->db->where('a.status', '1');
            if($prod_id){
                $this->db->where('a.id', $prod_id);
            }
            $this->db->where('a.category', $val['id']);
            $this->db->order_by('a.name','ASC');
            $q[$key]['products']= $this->db->get('products as a')->result_array();
            foreach ($q[$key]['products'] as $key2 => $val2) {
                $this->db->select('a.id,a.merchant,b.name,a.code,a.quantity,a.price_old,a.price,a.discount,a.weight,a.pricing_level');
                $this->db->join('merchants as b','b.id = a.merchant','left');
                $this->db->where('a.status', '1');
                $this->db->where('a.product', $val['id']);
                $this->db->order_by('a.id','ASC');
                $q[$key]['products'][$key2]['stock']= $this->db->get('product_merchant as a')->result_array();
            }
        }
        $this->response($q, 200);
    }
    public function total_product_get()
    {
        $limit = $this->post('limit');
        if($limit){
            $this->db->limit($limit);
        }
        $this->db->select('id,name');
        $this->db->where('active', '1');
        $this->db->order_by('name','asc');
        $q = $this->db->get($this->table)->result_array();
        $qnotEmpty = array();
        foreach ($q as $key => $val) {
            $this->db->select('id');
            $this->db->where('a.status', '1');
            $this->db->where('a.status_global', '0');
            $this->db->where('a.category', $val['id']);
            $this->db->order_by('a.name','ASC');
            $q[$key]['total_products']= $this->db->get('products as a')->num_rows();
            if($q[$key]['total_products'] > 0) {
                $qnotEmpty[] = $q[$key];
            }
        }
        $this->response($qnotEmpty, 200);
    }
    public function update_product_global1_post()
    {
        $prod_ids = $this->post('id_prods');
        $this->db->where_in('id',$prod_ids);
        $this->db->where('status_global', 0);
        if(count($prod_ids) > 0){
            $q = $this->db->update('products', array('status_global' => 1));
        } else {
            $q = false;
        }
        if ($q) {
            $resp = array('status'=>'success','message'=>'Status Global Produk berhasil diubah');
            $status = 200;
        } else {
            $resp = array('status'=>'failed','message'=>'Status Global Produk gagal diubah');
            $status = 400;
        }
        $this->response($resp, $status);
    }
    
    public function search_autocomplete_post()
    {
        $keyword = $this->post('search');
        $limit = $this->post('limit');
        if($limit){
            $this->db->limit($limit);
        }
        $this->db->select('id,name');
        $this->db->where('active', '1');
        $this->db->where('status_global', '0');
        if($keyword) {
            $this->db->like("name", $keyword);
        }
        $this->db->order_by('id','asc');
        $q = $this->db->get($this->table)->result_array();
        $q2 = array();
        if(count($q) > 0) {
            foreach($q as $key => $val) {
                $q2[] = array(
                        'id' => $val['id'],
                        'name' => $val['name'].'<br>'. $val['id'],
                        );
            }
        }
        $this->response($q2, 200);
    }

    public function search_post()
    {
        $keyword = $this->post('search');
        $limit = $this->post('limit');
        if($limit){
            $this->db->limit($limit);
        }
        $this->db->select('id,name');
        $this->db->where('active', '1');
        $this->db->where('status_global', '0');
        if($keyword) {
            $this->db->like("name", $keyword);
        }
        $this->db->order_by('id','asc');
        $q = $this->db->get($this->table)->result_array();
        $this->response($q, 200);
    }
    
    public function update_category_global1_post()
    {
        $cat_ids = $this->post('id_cats');
        $this->db->where_in('id',$cat_ids);
        $this->db->where('status_global', 0);
        if(count($cat_ids) > 0){
            $q = $this->db->update($this->table, array('status_global' => 1));
        } else {
            $q = false;
        }
        if ($q) {
            $resp = array('status'=>'success','message'=>'Status Global berhasil diubah');
            $status = 200;
        } else {
            $resp = array('status'=>'failed','message'=>'Status Global gagal diubah');
            $status = 400;
        }
        $this->response($resp, $status);
    }

    public function update_category_global_to1_get()
    {
        $this->db->where('status_global', 1);
        $q = $this->db->update($this->table, array('status_global' => 0));
        if ($q) {
            $resp = array('status'=>'success','message'=>'Status Global berhasil diubah');
            $status = 200;
        } else {
            $resp = array('status'=>'failed','message'=>'Status Global gagal diubah');
            $status = 400;
        }
        $this->response($resp, $status);
    }

    public function search_stock_post()
    {
        $limit = $this->post('limit');
        $cat_id = $this->post('cat_id');
        $keyword = $this->post('search');
        $prod_id = $this->post('prod_id');
        $type = $this->post('type');
        if($limit){
            $this->db->limit($limit);
        }
        $this->db->select('id,name');
        $this->db->where('active', '1');
        $this->db->where('status_global', '0');
        if($cat_id){
            $this->db->where('id', $cat_id);
        }
        if($keyword) {
            $this->db->like("name", $keyword);
        }
        $this->db->order_by('id','asc');
        $q = $this->db->get($this->table)->result_array();
        foreach ($q as $key => $val) {
            $this->db->select('a.id,a.name,a.code,a.barcode,a.category as category_id,a.brand,a.weight,
            a.length,a.width,a.height,a.price_old,a.price,a.discount,a.quantity,a.package_items,a.viewed,
            a.date_added');
            $this->db->where('a.status', '1');
            if($prod_id){
                $this->db->where('a.id', $prod_id);
            }
            $this->db->where('a.category', $val['id']);
            $this->db->order_by('a.name','ASC');
            $q[$key]['products']= $this->db->get('products as a')->result_array();
            foreach ($q[$key]['products'] as $key2 => $val2) {
                $this->db->select('a.id,a.merchant,b.name,a.code,a.quantity,a.price_old,a.price,a.discount,a.weight,a.pricing_level');
                $this->db->join('merchants as b','b.id = a.merchant','left');
                if($type){
                    $this->db->where('b.type', $type);
                }
                $this->db->where('a.status', '1');
                $this->db->where('a.product', $val['id']);
                $this->db->order_by('a.id','ASC');
                $q[$key]['products'][$key2]['stock']= $this->db->get('product_merchant as a')->result_array();
            }
        }
        $this->response($q, 200);
    }

}
