<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Product extends REST_Controller {
    
    public $table = "products";
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        
    }
    public function total_get()
    {

        $this->db->where('status', '1');
        $album = $this->db->count_all($this->table);
        $this->response($album, 200);
    }
    
    public function all_get()
    {
        $limit = $this->get('limit');
        if($limit){
            $this->db->limit($limit);
        }
        $this->db->select('a.id,a.name,a.code,a.barcode,a.category as category_id,b.name as category_name,a.brand,a.weight,
        a.length,a.width,a.height,a.price_old,a.price,a.discount,a.quantity,a.package_items,a.viewed,
        a.date_added');
        $this->db->join('categories as b','b.id = a.category','left');
        $this->db->where('b.status_global', '0');
        $this->db->where('a.status', '1');
        $this->db->order_by('a.name','ASC');
        $data = $this->db->get($this->table.' as a')->result();
        $this->response($data, 200);
    }

    public function update_product_global_to0_get()
    {
        $this->db->where('status_global', 1);
        $q = $this->db->update($this->table, array('status_global' => 0));
        if ($q) {
            $resp = array('status'=>'success','message'=>'Status Global berhasil diubah');
            $status = 200;
        } else {
            $resp = array('status'=>'failed','message'=>'Status Global gagal diubah');
            $status = 400;
        }
        $this->response($resp, $status);
    }

    public function merchant_stock_get() {
            $prod_id = $this->get('prod_id');
            $merchant = $this->get('merchant');
            $this->db->select('a.id,a.product,c.name as product_name,a.merchant,b.name,a.code,a.quantity,a.price_old,a.price,a.discount,a.weight,a.pricing_level');
            $this->db->join('merchants as b','b.id = a.merchant','left');
            $this->db->join('products as c','c.id = a.product','left');
            $this->db->where('a.status', '1');
            $this->db->where('b.status_global', '0');
            if($prod_id){
                $this->db->where('a.product', $prod_id);
            }
            if($merchant){
                $this->db->where('a.merchant', $merchant);
            }
            $this->db->order_by('a.product','ASC');
            $data= $this->db->get('product_merchant as a')->result_array();
            $this->response($data, 200);
    }

    public function insert_product_stock_post()
    {
        $post_data = $this->post('data');
        
        foreach($post_data as $dataz){
            $prod_id = $dataz['id_product'];
            $zona = $dataz['zona'];
            $price_level = $dataz['total'];

            $this->db->select('id');
            $this->db->where('name',$zona);
            $zona_data= $this->db->get('merchant_groups');
            if($zona_data->num_rows() > 0) {
                $zona_data_all = $zona_data->result_array();
                $zona_id = $zona_data_all[0]['id'];
            } else {
                $zona_id = NULL;
            }

            if(count($price_level) > 0) {
                foreach($price_level as $key => $val){
                    if($key == 0) {
                            $this->db->select('a.id');
                            $this->db->where('a.product', $prod_id);
                            $this->db->where('a.merchant_group', $zona_id);
                            $q = $this->db->get('product_price as a');
                            if($q->num_rows() > 0) {
                                foreach($q->result_array() as $row){
                                    $this->db->where('id', $row['id']);
                                    $upd = $this->db->update('product_price', array('price' => $price_level[0]['total']));
                                }
                            } else {
                                $data = array(
                                    'product'=>$prod_id,
                                    'merchant_group'=>$zona_id,
                                    'price'=>$price_level[0]['total']
                                );
                                $this->db->insert('product_price',$data);
                            }
                    } else if ($key <= 2){
                            $this->db->select('a.id');
                            $this->db->where('a.product', $prod_id);
                            $this->db->order_by('a.min_qty','asc');
                            $q = $this->db->get('product_price_level as a');
                            if($q->num_rows() > 0) {
                                    $nums = 0;
                                    foreach($q->result_array() as $row){
                                        if($nums == ($key-1)){
                                            if($zona == 'Zona 1'){
                                                $this->db->where('id', $row['id']);
                                                $upd = $this->db->update('product_price_level', array('price' => $price_level[$key]['total']));
                                            }
                                            $this->db->where('merchant_group', $zona_id);
                                            $this->db->where('product_price_level', $row['id']);
                                            $upd2 = $this->db->update('product_price_level_groups', array('price' => $price_level[$key]['total']));
                                        }
                                        $nums++;
                                    }
                                    if($q->num_rows() == 1 && $nums == 1) {
                                        $data = array(
                                            'product'=>$prod_id,
                                            'min_qty'=>$price_level[$key]['min_qty'],
                                            'price'=>$price_level[$key]['total']
                                        );
                                        $this->db->insert('product_price_level',$data);
                                        $datax = array(
                                            'product_price_level'=>$this->db->insert_id(),
                                            'merchant_group'=>$zona_id,
                                            'price'=>$price_level[$key]['total']
                                        );
                                        $this->db->insert('product_price_level_groups',$datax);
                                    }
                            } 
                            else {
                                    $data = array(
                                        'product'=>$prod_id,
                                        'min_qty'=>$price_level[$key]['min_qty'],
                                        'price'=>$price_level[$key]['total']
                                    );
                                    $this->db->insert('product_price_level',$data);
                                    $datax = array(
                                        'product_price_level'=>$this->db->insert_id(),
                                        'merchant_group'=>$zona_id,
                                        'price'=>$price_level[$key]['total']
                                    );
                                    $this->db->insert('product_price_level_groups',$datax);
                            }
                    } 
                }
                $resp = array('status'=>'success','message'=>'change price success');
                $status = 200;
            }
            else {
                $resp = array('status'=>'failed','message'=>'Zona Price kosong');
                $status = 400;
            }

        }
       
        $this->response($resp, $status);
    }
    

}
