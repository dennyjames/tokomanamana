<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Banner extends REST_Controller {
    
    public $table = "banners";
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $params = array('rounds' => 8);
        $params['salt_prefix'] = version_compare(PHP_VERSION, '5.3.7', '<') ? '$2a$' : '$2y$';
        $this->load->library('bcrypt', $params);
    }

    public function slider_get()
    {
        $limit = $this->get('limit');
        if($limit){
            $this->db->limit($limit);
        }
        $this->db->select('a.*');
        $this->db->where('a.status', '1');
        $this->db->where('a.type', 'slider');
        $data = $this->db->get($this->table.' as a')->result();
        $this->response($data, 200);
    }

    public function block_get()
    {
        $limit = $this->get('limit');
        if($limit){
            $this->db->limit($limit);
        }
        $this->db->select('a.*');
        $this->db->where('a.status', '1');
        $this->db->where('a.type', 'block_slider');
        $data = $this->db->get($this->table.' as a')->result();
        $this->response($data, 200);
    }

    public function brand_get()
    {
        $limit = $this->get('limit');
        if($limit){
            $this->db->limit($limit);
        }
        $this->db->select('a.*');
        $this->db->where('a.status', '1');
        $data = $this->db->get('brands as a')->result();
        $this->response($data, 200);
    }

}
