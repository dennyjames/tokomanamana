<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Info extends REST_Controller {
    
    public $table = "customers";
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $params = array('rounds' => 8);
        $params['salt_prefix'] = version_compare(PHP_VERSION, '5.3.7', '<') ? '$2a$' : '$2y$';
        $this->load->library('bcrypt', $params);
        $this->load->model('Model', 'm');
    }

    function view_post() {
        $cust_id = $this->post('cust_id');
        $this->db->select('c.*')
                ->where('c.id', $cust_id);
        $data = $this->db->get('customers c')->result();
        if(count($data) > 0) {
            $res = array('msg' => 'Get Customer Data sukses!','data'=>$data);
            $this->response($res, 200);
        } else {
            $res = array('msg' => 'Get Customer Data gagal','data'=>NULL);
            $this->response($res, 400);
        }
    }

    function customer_address_post() {
        $cust_id = $this->post('cust_id');
        $this->db->select('ca.*, p.name province_name, c.name city_name, d.name district_name')
                ->join('provincies p', 'p.id = ca.province')
                ->join('cities c', 'c.id = ca.city')
                ->join('districts d', 'd.id = ca.district')
                ->where('ca.customer', $cust_id);
        $data = $this->db->get('customer_address ca')->result();
        if(count($data) > 0) {
            $res = array('msg' => 'Get Alamat Customer sukses!','data'=>$data);
            $this->response($res, 200);
        } else {
            $res = array('msg' => 'Tidak ada alamat untuk user ini','data'=>NULL);
            $this->response($res, 400);
        }
    }

    function customer_address_byid_post() {
        $id = $this->post('id');
        $cust_id = $this->post('cust_id');
        $this->db->select('ca.*, p.name province_name, c.name city_name, d.name district_name')
                ->join('provincies p', 'p.id = ca.province')
                ->join('cities c', 'c.id = ca.city')
                ->join('districts d', 'd.id = ca.district')
                ->where('ca.customer', $cust_id)
                ->where('ca.id', $id);
        $data = $this->db->get('customer_address ca')->result();
        if(count($data) > 0) {
            $this->m->update_data('cart', array('id_address' => $id, 'status' => '1'), array('id_customer' => $cust_id));
            $res = array('msg' => 'Get Alamat Customer sukses!','data'=>$data[0]);
            $this->response($res, 200);
        } else {
            $res = array('msg' => 'Tidak ada alamat untuk user ini','data'=>NULL);
            $this->response($res, 400);
        }
    }

    public function setting_customer_address_post(){
        $m = $this->m;

        $data_ = NULL;
        $prov = NULL;

        $q = $m->get_data('', 'provincies', null, null, '', 'name asc');
        $data_ops = array();
        foreach($q->result() as $key){
            $data_ops[count($data_ops)] = array(
                'value' => $key->id,
                'label' => $key->name
            );
        }

        $prov = array(
            'defaultValue' => array('value' => '0', 'label' => 'Pilih Provinsi'),
            'label' => '',
            'animationType' => 'slide',
            'options' => $data_ops
        );

        $cust_id = $this->post('cust_id');
        $this->db->select('ca.*, p.name province_name, c.name city_name, d.name district_name')
                ->join('provincies p', 'p.id = ca.province')
                ->join('cities c', 'c.id = ca.city')
                ->join('districts d', 'd.id = ca.district')
                ->where('ca.customer', $cust_id)
                ->order_by('ca.id desc');
        $data = $this->db->get('customer_address ca')->result();
        if(count($data) > 0) {
            $data_ = $data;
            $msg = 'Get Alamat Customer sukses!';
        } else {
            $msg = 'Tidak ada alamat untuk user ini';
        }

        $res = array('msg' => $msg,'data'=>$data_, 'prov' => $prov);

        $this->response($res, 200);
    }

    public function get_kota_post(){
        $m = $this->m;

        $data_ = NULL;
        $id_prov = $this->post('id_prov');

        $q = $m->get_data('', 'cities', null, array('province' => $id_prov), '', 'name asc');

        $data_ops = array();
        foreach($q->result() as $key){
            $data_ops[count($data_ops)] = array(
                'value' => $key->id,
                'label' => $key->name
            );
        }

        $data_ = array(
            'defaultValue' => array('value' => '0', 'label' => 'Pilih Kota'),
            'label' => '',
            'animationType' => 'slide',
            'options' => $data_ops
        );

        $res = array('data' => $data_);
        $this->response($res, 200);
    }

    public function get_kec_post(){
        $m = $this->m;

        $data_ = NULL;
        $id_kota = $this->post('id_kota');

        $q = $m->get_data('', 'districts', null, array('city' => $id_kota), '', 'name asc');

        $data_ops = array();
        foreach($q->result() as $key){
            $data_ops[count($data_ops)] = array(
                'value' => $key->id,
                'label' => $key->name
            );
        }

        $data_ = array(
            'defaultValue' => array('value' => '0', 'label' => 'Pilih Kota'),
            'label' => '',
            'animationType' => 'slide',
            'options' => $data_ops
        );

        $res = array('data' => $data_);
        $this->response($res, 200);
    }

    public function add_address_post(){
        $m = $this->m;
        $save_as = $this->post('save_as');
        $nama = $this->post('nama');
        $no_hp = $this->post('no_hp');
        $kode_pos = $this->post('kode_pos');
        $alamat = $this->post('alamat');
        $prov = $this->post('prov');
        $kota = $this->post('kota');
        $kec = $this->post('kec');
        $id_cust = $this->post('id_cust');

        $cek_cust = $m->get_data('', 'customers', null, array('id' => $id_cust));
        if($cek_cust->num_rows() > 0){
            $data = array(
                'customer' => $id_cust,
                'title' => $save_as,
                'name' => $nama,
                'phone' => $no_hp,
                'address' => $alamat,
                'city' => $kota,
                'postcode' => $kode_pos,
                'province' => $prov,
                'district' => $kec,
                'primary' => 1
            );

            $m->insert_data('customer_address', $data);

            $res = array('success' => 1, 'msg' => 'Tambah Alamat Berhasil !');
        }else{
            $res = array('success' => 0, 'msg' => 'Permintaan Tidak Dikenali');
        }

        $this->response($res, 200);
    }

    public function edit_address_post(){
        $m = $this->m;
        $save_as = $this->post('save_as');
        $nama = $this->post('nama');
        $no_hp = $this->post('no_hp');
        $kode_pos = $this->post('kode_pos');
        $alamat = $this->post('alamat');
        $prov = $this->post('prov');
        $kota = $this->post('kota');
        $kec = $this->post('kec');
        $idEdit = $this->post('idEdit');

        $cek_cust = $m->get_data('', 'customer_address', null, array('id' => $idEdit));
        if($cek_cust->num_rows() > 0){
            $data = array(
                'title' => $save_as,
                'name' => $nama,
                'phone' => $no_hp,
                'address' => $alamat,
                'city' => $kota,
                'postcode' => $kode_pos,
                'province' => $prov,
                'district' => $kec,
                'primary' => 1
            );

            $m->update_data('customer_address', $data, array('id' => $idEdit));

            $res = array('success' => 1, 'msg' => 'Ubah Alamat Berhasil !');
        }else{
            $res = array('success' => 0, 'msg' => 'Permintaan Tidak Dikenali');
        }

        $this->response($res, 200);
    }

    public function delete_address_post(){
        $m = $this->m;

        $idDelete = $this->post('idDelete');

        $q = $m->get_data('', 'customer_address', null, array('id' => $idDelete));

        if($q->num_rows() > 0){
            $m->delete_data('customer_address', array('id' => $idDelete));
            $res = array('success' => 1, 'msg' => 'Delete Alamat Berhasil !');
        }else{
            $res = array('success' => 0, 'msg' => 'Permintaan Tidak Dikenali');
        }

        $this->response($res, 200);
    }

    public function get_kota_kec_post(){
        $m = $this->m;
        $id_prov = $this->post('id_prov');
        $id_kota = $this->post('id_kota');
        $id_kec = $this->post('id_kec');

        //provincies
        $q_prov = $m->get_data('', 'provincies', null, null, '', 'name asc');
        $data_prov = array();

        $default_prov = array('value' => '0', 'label' => 'Pilih Provinsi');

        foreach($q_prov->result() as $key){
            $data_prov[count($data_prov)] = array(
                'value' => $key->id,
                'label' => $key->name
            );

            if($key->id == $id_prov){
                $default_prov = array('value' => $key->id, 'label' => $key->name);
            }
        }

        $prov = array(
            'defaultValue' => $default_prov,
            'label' => '',
            'animationType' => 'slide',
            'options' => $data_prov
        );

        //cities
        $q_kota = $m->get_data('', 'cities', null, array('province' => $id_prov), '', 'name asc');
        $data_kota = array();

        $default_kota = array('value' => '0', 'label' => 'Pilih Kota');

        foreach($q_kota->result() as $key){
            $data_kota[count($data_kota)] = array(
                'value' => $key->id,
                'label' => $key->name
            );

            if($key->id == $id_kota){
                $default_kota = array('value' => $key->id, 'label' => $key->name);
            }
        }

        $city = array(
            'defaultValue' => $default_kota,
            'label' => '',
            'animationType' => 'slide',
            'options' => $data_kota
        );

        //districts
        $q_kec = $m->get_data('', 'districts', null, array('city' => $id_kota), '', 'name asc');
        $data_kec = array();

        $default_kec = array('value' => '0', 'label' => 'Pilih Kecamatan');

        foreach($q_kec->result() as $key){
            $data_kec[count($data_kec)] = array(
                'value' => $key->id,
                'label' => $key->name
            );

            if($key->id == $id_kec){
                $default_kec = array('value' => $key->id, 'label' => $key->name);
            }
        }

        $kec = array(
            'defaultValue' => $default_kec,
            'label' => '',
            'animationType' => 'slide',
            'options' => $data_kec
        );

        $res = array('success' => 1, 'prov' => $prov, 'kota' => $city, 'kec' => $kec);
        $this->response($res, 200);
    }

    public function get_id_cust_post(){
        $email = $this->post('email');
        $m = $this->m;

        $q = $m->get_data('id, email', 'customers', null, null, '', '', 0, 0, array(0 => "email LIKE '%$email%'"));

        $res = array('data' => $q->result());

        $this->response($res, 200);
    }
}

