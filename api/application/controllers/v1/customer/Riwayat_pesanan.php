<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Riwayat_pesanan extends REST_Controller {
    
    public $table = "customers";
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $params = array('rounds' => 8);
        $params['salt_prefix'] = version_compare(PHP_VERSION, '5.3.7', '<') ? '$2a$' : '$2y$';
        $this->load->library('bcrypt', $params);
        $this->load->model('Model', 'm');
    }

    function index_post(){
        $m = $this->m;
        $id_cust = $this->post('id_customer');

        $cek_cust = $m->get_data('', 'customers', null, array('id' => $id_cust));
        if($cek_cust->num_rows() > 0){
            $join = array(
                0 => 'orders o-o.id=oh.order'
            );

            $where = array(
                'o.customer' => $id_cust
            );
            $qorder = $m->get_data(
                'o.code, o.payment_status, sum(o.subtotal) as total_tagihan, o.due_date, o.id,
                oh.date_added, oh.order, oh.invoice',
                'order_history oh', $join, $where, 'oh.order, oh.invoice', 'oh.order_status desc');

            $order = array();

            foreach($qorder->result() as $key){
                $datenow = new DateTime();
                $due_date = new DateTime($key->due_date);
                $diff = $datenow->diff($due_date);
                $hari = $diff->d ? $diff->d.' Hari ' : '';
                $jam = $diff->h ? $diff->h.' Jam ' : '';
                $menit = $diff->i ? $diff->i.' Menit ' : '';
                $due_time = $hari.$jam.$menit;

                $batas_waktu = '';
                if($diff->invert == 0 && $key->payment_status == 'Pending'){
                    $batas_waktu = $due_time;
                }

                $joininv = array(
                    0 => 'merchants m-m.id=oi.merchant',
                    1 => 'setting_order_status sos-sos.id=oi.order_status'
                );
                $whereinv = array(
                    'oi.order' => $key->order,
                    'oi.id' => $key->invoice
                );
                $qorderinv = $m->get_data(
                    'm.name,
                    sos.name as order_status_name,
                    oi.due_date',
                    'order_invoice oi', $joininv, $whereinv, '', 'oi.code asc');
                
                $inv = array();

                foreach($qorderinv->result() as $key0){
                    
                    $due_datex = new DateTime($key0->due_date);
                    $diffx = $datenow->diff($due_datex);
                    $harix = $diffx->d ? $diffx->d.' Hari ' : '';
                    $jamx = $diffx->h ? $diffx->h.' Jam ' : '';
                    $menitx = $diffx->i ? $diffx->i.' Menit ' : '';
                    $due_timex = $harix.$jamx.$menitx;

                    $batas_waktuinv = '';
                    if($diffx->invert == 0 && $key->payment_status == 'Paid' && $key0->order_status != 8){
                        $batas_waktuinv = $due_timex;
                    }

                    $joinprod = array(
                        0 => 'products p-p.id=op.product'
                    );

                    $whereprod = array(
                        'op.order' => $key->order,
                        'op.invoice' => $key->invoice
                    );

                    $qprod = $m->get_data('p.name', 'order_product op', $joinprod, $whereprod, '', 'p.name asc');
                    
                    $product = array();

                    foreach($qprod->result() as $key1){
                        $qimg = $m->get_data('image', 'product_image', null, array('primary' => 1));
                        
                        $img = '';

                        if($qimg->num_rows() > 0){
                            $img = $qimg->row()->image;
                        }

                        $product[] = array(
                            'nama_product' => $key1->name,
                            'gambar' => $img
                        );
                    }
                    $inv[] = array(
                        'penjual' => $key0->name,
                        'status_pembelian' => $key0->order_status_name,
                        'batas_waktu' => $batas_waktuinv,
                        'products' => $product
                    );
                }

                $order[] = array(
                    'order_id' => $key->id,
                    'no_tagihan' => $key->code,
                    'tgl' => date('d/m/Y H:i', strtotime($key->date_added)),
                    'total_tagihan' => number_format($key->total_tagihan),
                    'status_tagihan' => $key->payment_status,
                    'batas_waktu' => $batas_waktu,
                    'invoice' => $inv
                );
            }
            
            $res = array('success' => 1, 'data_pembelian' => $order);
        }else{
            $res = array('success' => 0, 'msg' => 'Invalid Request');
        }

        $this->response($res, 200);
    }

    function detail_post() {
        $order = $this->post('order');
        $m = $this->m;

        $select = "o.*, sd.name shipping_district_name, sc.name shipping_city_name, sp.name shipping_province_name";
        $join = array(
            0 => 'cities sc-sc.id = o.shipping_city',
            1 => 'districts sd-sd.id = o.shipping_district',
            2 => 'provincies sp-sp.id = o.shipping_province'
        );
        $where = array(
            'o.id' => $order
        );
        $q = $m->get_data($select, 'orders o', $join, $where);
        if($q->num_rows() > 0){
            $data = $q->row();
            $data->payment_method_name = $m->get_data('', 'payment_methods', null, array('name' => $data->payment_method))->row()->title;
            $payment = json_decode($data->payment_to);
            
            $select_inv = 'oi.*, m.name merchant_name, os.name order_status_name';
            $join_inv = array(
                0 => 'merchants m-m.id = oi.merchant',
                1 => 'setting_order_status os-os.id = oi.order_status'
            );
            $where_inv = array(
                'oi.order' => $order
            );
            $q_invoice = $m->get_data($select_inv, 'order_invoice oi', $join_inv, $where_inv);
            
            $data_inv = array();

            foreach($q_invoice->result() as $key){
                $q_prod = $m->get_data('op.*, pi.image', 'order_product op', array(0 => 'product_image pi-pi.product = op.product AND pi.primary = 1'), array('op.invoice' => $key->id));

                $key->product = $q_prod->result();
                $data_inv[] = $key;
            }

            $res = array('success' => 1, 'data' => $data, 'payment' => $payment, 'inv' => $data_inv);
        }else{
            $res = array('success' => 0, 'msg' => 'Invalid Request');
        }

        $this->response($res, 200);
    }

    function invoice_status_post() {
        $cust_id = $this->post('cust_id');
        $status_id = $this->post('status_id');
        $this->db->select('oi.*,o.payment_method')
                ->join('orders o', 'o.id = oi.order')
                ->where('oi.customer', $cust_id)
                ->where('oi.order_status', $status_id);
        $data = $this->db->get('order_invoice oi')->result();
        if(count($data) > 0) {
            $res = array('msg' => 'Get invoice sukses!','data'=>$data);
            $this->response($res, 200);
        } else {
            $res = array('msg' => 'Tidak ada invoice untuk user ini','data'=>NULL);
            $this->response($res, 400);
        }
    }

    function customer_address_post() {
        $cust_id = $this->post('cust_id');
        $this->db->select('ca.*, p.name province_name, c.name city_name, d.name district_name')
                ->join('provincies p', 'p.id = ca.province')
                ->join('cities c', 'c.id = ca.city')
                ->join('districts d', 'd.id = ca.district')
                ->where('ca.customer', $cust_id);
        $data = $this->db->get('customer_address ca')->result();
        if(count($data) > 0) {
            $res = array('msg' => 'Get Alamat Customer sukses!','data'=>$data);
            $this->response($res, 200);
        } else {
            $res = array('msg' => 'Tidak ada alamat untuk user ini','data'=>NULL);
            $this->response($res, 400);
        }
    }

    function customer_address_byid_post() {
        $id = $this->post('id');
        $cust_id = $this->post('cust_id');
        $this->db->select('ca.*, p.name province_name, c.name city_name, d.name district_name')
                ->join('provincies p', 'p.id = ca.province')
                ->join('cities c', 'c.id = ca.city')
                ->join('districts d', 'd.id = ca.district')
                ->where('ca.customer', $cust_id)
                ->where('ca.id', $id);
        $data = $this->db->get('customer_address ca')->result();
        if(count($data) > 0) {
            $this->m->update_data('cart', array('id_address' => $id, 'status' => '1'), array('id_customer' => $cust_id));
            $res = array('msg' => 'Get Alamat Customer sukses!','data'=>$data[0]);
            $this->response($res, 200);
        } else {
            $res = array('msg' => 'Tidak ada alamat untuk user ini','data'=>NULL);
            $this->response($res, 400);
        }
    }

    public function setting_customer_address_post(){
        $m = $this->m;

        $data_ = NULL;
        $prov = NULL;

        $q = $m->get_data('', 'provincies', null, null, '', 'name asc');
        $data_ops = array();
        foreach($q->result() as $key){
            $data_ops[count($data_ops)] = array(
                'value' => $key->id,
                'label' => $key->name
            );
        }

        $prov = array(
            'defaultValue' => array('value' => '0', 'label' => 'Pilih Provinsi'),
            'label' => '',
            'animationType' => 'slide',
            'options' => $data_ops
        );

        $cust_id = $this->post('cust_id');
        $this->db->select('ca.*, p.name province_name, c.name city_name, d.name district_name')
                ->join('provincies p', 'p.id = ca.province')
                ->join('cities c', 'c.id = ca.city')
                ->join('districts d', 'd.id = ca.district')
                ->where('ca.customer', $cust_id)
                ->order_by('ca.id desc');
        $data = $this->db->get('customer_address ca')->result();
        if(count($data) > 0) {
            $data_ = $data;
            $msg = 'Get Alamat Customer sukses!';
        } else {
            $msg = 'Tidak ada alamat untuk user ini';
        }

        $res = array('msg' => $msg,'data'=>$data_, 'prov' => $prov);

        $this->response($res, 200);
    }

    public function get_kota_post(){
        $m = $this->m;

        $data_ = NULL;
        $id_prov = $this->post('id_prov');

        $q = $m->get_data('', 'cities', null, array('province' => $id_prov), '', 'name asc');

        $data_ops = array();
        foreach($q->result() as $key){
            $data_ops[count($data_ops)] = array(
                'value' => $key->id,
                'label' => $key->name
            );
        }

        $data_ = array(
            'defaultValue' => array('value' => '0', 'label' => 'Pilih Kota'),
            'label' => '',
            'animationType' => 'slide',
            'options' => $data_ops
        );

        $res = array('data' => $data_);
        $this->response($res, 200);
    }

    public function get_kec_post(){
        $m = $this->m;

        $data_ = NULL;
        $id_kota = $this->post('id_kota');

        $q = $m->get_data('', 'districts', null, array('city' => $id_kota), '', 'name asc');

        $data_ops = array();
        foreach($q->result() as $key){
            $data_ops[count($data_ops)] = array(
                'value' => $key->id,
                'label' => $key->name
            );
        }

        $data_ = array(
            'defaultValue' => array('value' => '0', 'label' => 'Pilih Kota'),
            'label' => '',
            'animationType' => 'slide',
            'options' => $data_ops
        );

        $res = array('data' => $data_);
        $this->response($res, 200);
    }

    public function add_address_post(){
        $m = $this->m;
        $save_as = $this->post('save_as');
        $nama = $this->post('nama');
        $no_hp = $this->post('no_hp');
        $kode_pos = $this->post('kode_pos');
        $alamat = $this->post('alamat');
        $prov = $this->post('prov');
        $kota = $this->post('kota');
        $kec = $this->post('kec');
        $id_cust = $this->post('id_cust');

        $cek_cust = $m->get_data('', 'customers', null, array('id' => $id_cust));
        if($cek_cust->num_rows() > 0){
            $data = array(
                'customer' => $id_cust,
                'title' => $save_as,
                'name' => $nama,
                'phone' => $no_hp,
                'address' => $alamat,
                'city' => $kota,
                'postcode' => $kode_pos,
                'province' => $prov,
                'district' => $kec,
                'primary' => 1
            );

            $m->insert_data('customer_address', $data);

            $res = array('success' => 1, 'msg' => 'Tambah Alamat Berhasil !');
        }else{
            $res = array('success' => 0, 'msg' => 'Permintaan Tidak Dikenali');
        }

        $this->response($res, 200);
    }

    public function edit_address_post(){
        $m = $this->m;
        $save_as = $this->post('save_as');
        $nama = $this->post('nama');
        $no_hp = $this->post('no_hp');
        $kode_pos = $this->post('kode_pos');
        $alamat = $this->post('alamat');
        $prov = $this->post('prov');
        $kota = $this->post('kota');
        $kec = $this->post('kec');
        $idEdit = $this->post('idEdit');

        $cek_cust = $m->get_data('', 'customer_address', null, array('id' => $idEdit));
        if($cek_cust->num_rows() > 0){
            $data = array(
                'title' => $save_as,
                'name' => $nama,
                'phone' => $no_hp,
                'address' => $alamat,
                'city' => $kota,
                'postcode' => $kode_pos,
                'province' => $prov,
                'district' => $kec,
                'primary' => 1
            );

            $m->update_data('customer_address', $data, array('id' => $idEdit));

            $res = array('success' => 1, 'msg' => 'Ubah Alamat Berhasil !');
        }else{
            $res = array('success' => 0, 'msg' => 'Permintaan Tidak Dikenali');
        }

        $this->response($res, 200);
    }

    public function delete_address_post(){
        $m = $this->m;

        $idDelete = $this->post('idDelete');

        $q = $m->get_data('', 'customer_address', null, array('id' => $idDelete));

        if($q->num_rows() > 0){
            $m->delete_data('customer_address', array('id' => $idDelete));
            $res = array('success' => 1, 'msg' => 'Delete Alamat Berhasil !');
        }else{
            $res = array('success' => 0, 'msg' => 'Permintaan Tidak Dikenali');
        }

        $this->response($res, 200);
    }

    public function get_kota_kec_post(){
        $m = $this->m;
        $id_prov = $this->post('id_prov');
        $id_kota = $this->post('id_kota');
        $id_kec = $this->post('id_kec');

        //provincies
        $q_prov = $m->get_data('', 'provincies', null, null, '', 'name asc');
        $data_prov = array();

        $default_prov = array('value' => '0', 'label' => 'Pilih Provinsi');

        foreach($q_prov->result() as $key){
            $data_prov[count($data_prov)] = array(
                'value' => $key->id,
                'label' => $key->name
            );

            if($key->id == $id_prov){
                $default_prov = array('value' => $key->id, 'label' => $key->name);
            }
        }

        $prov = array(
            'defaultValue' => $default_prov,
            'label' => '',
            'animationType' => 'slide',
            'options' => $data_prov
        );

        //cities
        $q_kota = $m->get_data('', 'cities', null, array('province' => $id_prov), '', 'name asc');
        $data_kota = array();

        $default_kota = array('value' => '0', 'label' => 'Pilih Kota');

        foreach($q_kota->result() as $key){
            $data_kota[count($data_kota)] = array(
                'value' => $key->id,
                'label' => $key->name
            );

            if($key->id == $id_kota){
                $default_kota = array('value' => $key->id, 'label' => $key->name);
            }
        }

        $city = array(
            'defaultValue' => $default_kota,
            'label' => '',
            'animationType' => 'slide',
            'options' => $data_kota
        );

        //districts
        $q_kec = $m->get_data('', 'districts', null, array('city' => $id_kota), '', 'name asc');
        $data_kec = array();

        $default_kec = array('value' => '0', 'label' => 'Pilih Kecamatan');

        foreach($q_kec->result() as $key){
            $data_kec[count($data_kec)] = array(
                'value' => $key->id,
                'label' => $key->name
            );

            if($key->id == $id_kec){
                $default_kec = array('value' => $key->id, 'label' => $key->name);
            }
        }

        $kec = array(
            'defaultValue' => $default_kec,
            'label' => '',
            'animationType' => 'slide',
            'options' => $data_kec
        );

        $res = array('success' => 1, 'prov' => $prov, 'kota' => $city, 'kec' => $kec);
        $this->response($res, 200);
    }
}

