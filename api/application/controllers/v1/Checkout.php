<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Checkout extends REST_Controller {
    
    public $table = "customers";
    public $url_base = 'http://157.230.36.113/site';
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $params = array('rounds' => 8);
        $params['salt_prefix'] = version_compare(PHP_VERSION, '5.3.7', '<') ? '$2a$' : '$2y$';
        $this->load->model('Checkout_model', 'checkout');
        $this->load->library('bcrypt', $params);
        $this->load->model('Model', 'm');
        
    }

    public function payment_post()
    {

        $this->load->library('sprint');
        $now = date("Y-m-d H:i:s");
        $due_date= date("Y-m-d H:i:s", strtotime("+ 2 day"));
        $transaction = array(
                'id'=>'10001',
                'total'=>'10000',
                'date_added'=> $now,
                'due_date'=>$due_date,
                'code'=>'12144',
                'customer_name'=>'Test Doank',
                'customer_email'=>'testing@mail.com',
                'customer_phone'=>'081123455432',
                'shipping_address'=>'Jalan Dulu no 12',
                'shipping_city_name'=>'Jakarta',
                'shipping_province_name'=>'DKI Jakarta',
                'shipping_post_code'=>'11720',
                'payment_method'=>'bca_va'
                
        );
        $transaction = (object) $transaction;
        $response = $this->sprint->process($transaction);
        $this->response($response, 200);
    }
    
    public function payment_process_post(){
        $data = $this->post(null, true);
        $cart_items = json_decode($data['item'],true);
        $this->data['user'] = json_decode($data['user'],false);
        $address_id = $data['address_id'];
        $payment_method = $data['payment_method'];
        $coupon= $data['coupon'] ?  $data['coupon'] : '';
        $list_shipping = array();

        foreach ( $cart_items as $key => $item) {
            array_push($list_shipping,$item['shipping']);
        }
        //var_dump(order_code());exit();
        $counts = array_count_values($list_shipping);
        $count_total_shipping = 0;
        $this->data['pickup_count'] = isset($counts['pickup']) ? $counts['pickup'] : 0;
        //var_dump($this->data['pickup_count']);exit();
        //$this->form_validation->set_rules('payment', 'lang:checkout_shipping_field_payment', 'required');
        $shipping_group= array();
        foreach ( $cart_items as $key => $item) {
            $id = $item['shipping_merchant'] . '-' . $item['shipping'];
            $shipping_group[$id]['merchant_id'] = $item['shipping_merchant'];
            $shipping_group[$id]['shipping_type'] = $item['shipping'];
            $shipping_group[$id]['shipping_cost'] = $item['shipping_cost'];
            $shipping_group[$id]['weight'] = isset($shipping_group[$id]['weight']) ? ($shipping_group[$id]['weight'] + $item['weight']) / 1000 : $item['weight'] / 1000;
            $shipping_group[$id]['shipping_total'] = $shipping_group[$id]['shipping_cost'];
        }
        foreach ($shipping_group as $key => $item){
            $count_total_shipping = $count_total_shipping + $item['shipping_total'];
        }
        $this->data['count_total_shipping'] = $count_total_shipping;
        //var_dump(count($cart_items));exit();
        if (count($cart_items) > 0) {
            $address = $this->main->get('customer_address', array('id' => $address_id));
            //$address = $this->main->get('customer_address', array('id' => $address_id, 'customer' => $this->data['user']->id));
            //var_dump($address);exit();
            // $payment_method = $payment_method;
            // $coupon = $this->session->userdata('coupon');
           $data_order = array(
                'code' => order_code(),
                'customer' => $this->data['user']->id,
                'payment_method' => $payment_method,
                'due_date' => date('Y-m-d H:i:s', strtotime(' +1 day')),
//                'due_date' => date('Y-m-d H:i:s', strtotime(' +2 minutes')),
                'shipping_name' => $address->name,
                'shipping_address' => $address->address,
                'shipping_phone' => $address->phone,
                'shipping_province' => $address->province,
                'shipping_city' => $address->city,
                'shipping_district' => $address->district,
                'shipping_postcode' => $address->postcode,
            );
            $order = $this->main->insert('orders', $data_order);
            if($coupon) {
                $data_coupon = array(
                    'coupon' => $coupon->id,
                    'order' => $this->db->insert_id(),
                    'customer' => $this->data['user']->id,
                    'amount' => $coupon->total_disc,
                );
                $coupon_history = $this->main->insert('coupon_history', $data_coupon);  
            }
            $data_invoice = array(
                'customer' => $this->data['user']->id,
                'order_status' => settings('order_new_status'),
                'order' => $order
            );
            $cart = array();
            foreach ( $cart_items as $key => $item) {
                $id = $item['shipping_merchant'] . '-' . $item['shipping'];
                $cart[$id]['merchant'] = $item['shipping_merchant'];
                $cart[$id]['shipping'] = $item['shipping'];
                $cart[$id]['shipping_cost'] = isset($cart[$id]['shipping_cost']) ? $cart[$id]['shipping_cost'] + $item['shipping_cost'] : $item['shipping_cost'];
                //$cart[$id]['shipping_cost'] = isset($cart[$id]['shipping_cost']) && isset($cart[$id]['weight']) ? $cart[$id]['shipping_cost'] * ceil($cart[$id]['weight']) : 0;
                $cart[$id]['subtotal'] = isset($cart[$id]['subtotal']) ? $cart[$id]['subtotal'] + $item['subtotal'] : $item['subtotal'];
                $cart[$id]['weight'] = isset($cart[$id]['weight']) ? $cart[$id]['weight'] + $item['weight'] : $item['weight'];
                $cart[$id]['product'][$key] = $item;
            }
            //var_dump($cart);exit();
            ksort($cart, SORT_NUMERIC);

            $order_total = 0;
            $order_shipping = 0;
            foreach ($cart as $merchant => $data) {
                $data_invoice['code'] = invoice_code();
                $data_invoice['merchant'] = $data['merchant'];
                $data_invoice['shipping_courier'] = $data['shipping'];
                $data_invoice['shipping_cost'] = $data['shipping_cost'];
                $data_invoice['shipping_weight'] = $data['weight'];
                $data_invoice['subtotal'] = $data['subtotal'];
                $data_invoice['total'] = $data['subtotal'] + $data['shipping_cost'];
                $order_total += $data_invoice['subtotal'];
                $order_shipping += $data['shipping_cost'];
                //var_dump($data['product']);
                //exit();
                $invoice = $this->main->insert('order_invoice', $data_invoice);
                foreach ($data['product'] as $item) {
                    $data_product = array(
                        'order' => $order,
                        'invoice' => $invoice,
                        'product' => $item['id'],
                        'name' => $item['name'],
                        'code' => $item['code'],
                        'price' => $item['price'],
                        'quantity' => $item['qty'],
                        'weight' => $item['weight'],
                        'total' => $item['subtotal'],
                        'options' => json_encode($item['options']),
                        'promo_data' => json_encode($item['promo_data'])
                    );
                    if($item['inPromo']){
                        $data_product['price_old'] = $item['old_price'];
                        $data_product['discount'] = $item['qty'] * $item['disc_price'];
                    }
                    $order_product = $this->main->insert('order_product', $data_product);
                }
                $this->main->insert('order_history', array('order' => $order, 'invoice' => $invoice, 'order_status' => settings('order_new_status')));
            }
            if($coupon) {
                $order_total = $order_total -  $coupon->total_disc;
            }
            $this->main->update('orders', array('total' => $order_total + $order_shipping, 'subtotal' => $order_total, 'shipping_cost' => $order_shipping), array('id' => $order));
            //$this->cart->destroy();

            $this->load->model('orders');
            $data = $this->orders->get($order);
            $this->data['order'] = $data;
            $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['order']->customer));
            $this->data['invoices'] = $this->main->gets('order_invoice', array('order' => $this->data['order']->id));

            $payment_method = $this->main->get('payment_methods', array('name' => $payment_method));
            if ($payment_method->vendor == 'xendit') {
                if ($payment_method->name == 'credit_card') {
                    redirect('checkout/credit_card?id=' . $data_order['code']);
                } else {
                    $this->load->library('xendit');
                    $xendit_response = $this->xendit->createInvoice($data_order['code'], $order_total + $order_shipping, $this->data['user']->email, 'Order #' . $order);
                    $payment = $xendit_response;
                    if (in_array($data->payment_method, array('mandiri_va', 'bni_va', 'bri_va'))) {
                        switch ($data->payment_method) {
                            case 'mandiri_va':
                                $va_name = 'Mandiri Virtual Account';
                                $va_code = 'MANDIRI';
                                break;
                            case 'bni_va':
                                $va_name = 'BNI Virtual Account';
                                $va_code = 'BNI';
                                break;
                            case 'bri_va':
                                $va_name = 'BRI Virtual Account';
                                $va_code = 'BRI';
                                break;
                        }
                        foreach ($payment['available_banks'] as $account) {
                            if ($account['bank_code'] == $va_code) {
                                $payment_detail = array(
                                    'vendor' => 'xendit',
                                    'account_code' => $account['bank_code'],
                                    'account_name' => $va_name,
                                    'account_holder_name' => $account['account_holder_name'],
                                    'account_branch' => $account['bank_branch'],
                                    'account_number' => $account['bank_account_number'],
                                    'amount' => $account['transfer_amount'],
                                );
                                break;
                            }
                        }
                    } elseif ($data->payment_method == 'alfamart') {
                        foreach ($payment['available_retail_outlets'] as $account) {
                            if ($account['retail_outlet_name'] == 'ALFAMART') {
                                $payment_detail = array(
                                    'vendor' => 'xendit',
                                    'account_code' => $account['retail_outlet_name'],
                                    'account_name' => $account['retail_outlet_name'],
                                    'account_holder_name' => 'XENDIT',
                                    'account_branch' => '',
                                    'account_number' => $account['payment_code'],
                                    'amount' => $account['transfer_amount'],
                                );
                                break;
                            }
                        }
                    }
                    $this->main->insert('order_meta', [
                        'order_id' => $order,
                        'meta_name' => 'xendit_response',
                        'meta_value' => json_encode($payment)
                    ]);
                }
            } elseif ($payment_method->vendor == 'sprint') {
                $this->load->library('sprint');
                $transaction = $this->data['order'];
                $response = $this->sprint->process($transaction);
                $this->main->insert('order_meta', [
                    'order_id' => $order,
                    'meta_name' => 'sprint_request',
                    'meta_value' => json_encode($response['request'])
                ]);
                $this->main->insert('order_meta', [
                    'order_id' => $order,
                    'meta_name' => 'sprint_response',
                    'meta_value' => json_encode($response['response'])
                ]);
                $payment_detail = array(
                    'vendor' => 'sprint',
                    'account_code' => $response['request']['channelId'],
                    'account_name' => $payment_method->title,
                    'account_holder_name' => isset($payment_method->merchant_name) ? $payment_method->merchant_name : '',
                    'account_branch' => '',
                    'account_number' => ($data->payment_method == 'bca_va') ? $response['request']['customerAccount'] : '',
                    'amount' => $response['request']['transactionAmount'],
                );
                if (in_array($data->payment_method, ['kredivo', 'bca_sakuku', 'creditcard','creditcard01','creditcard03','creditcard06','creditcard12'])) {
                    $payment_detail['payment_url'] = $response['response']['redirectURL'];
                } elseif ($data->payment_method == 'bca_klikpay') {
                    $payment_detail['payment_url'] = $response['response']['redirectURL'];
                    $payment_detail['payment_data'] = $response['response']['redirectData'];
                }
            } elseif ($payment_method->vendor == 'ipay88') {
                $this->load->library('ipay');
                $this->load->config('ipay88', TRUE);
                $transaction = $this->data['order'];
                $cust = $this->data['customer'];
                $merchant_code = $this->config->item('merchant_code', 'ipay88');
                $merchant_key = $this->config->item('merchant_key', 'ipay88');
                $responseUrl = $this->config->item('responseURL', 'ipay88');
                $backendUrl = $this->config->item('backendURL', 'ipay88');
                $payment_detail = array(
                    'MerchantCode' => $merchant_code,
                    'MerchantKey' => $merchant_key,
                    'RefNo' => $transaction->code,
                    'Amount' => $transaction->total,
                    'Currency' => 'IDR',
                    'ProdDesc' => $transaction->code,
                    'UserName' => $cust->fullname,
                    'UserEmail' => $cust->email,
                    'UserContact' => $cust->phone,
                    'Remark' => '',
                    'Lang' => 'UTF-8',
                    'ResponseURL' => $responseUrl,
                    'BackendURL' => $backendUrl,
                );
                if($data->payment_method == 'ovo'){
                    $payment_detail['PaymentId'] = 1;
                } elseif($data->payment_method == 'tcash'){
                    $payment_detail['PaymentId'] = 15;
                } else {
                    $payment_detail['PaymentId'] = 1;
                }
                //$payment_detail['xfield1'] = '||IPP:3||';
                $responseIpay = $this->ipay->setMerchantCode($payment_detail['MerchantCode'])
                ->setMerchantKey($payment_detail['MerchantKey'])
                ->setPaymentId($payment_detail['PaymentId'])
                ->setRefNo($payment_detail['RefNo'])
                ->setAmount($payment_detail['Amount'])
                ->setCurrency($payment_detail['Currency'])
                ->setProdDesc($payment_detail['ProdDesc'])
                ->setUsername($payment_detail['UserName'])
                ->setUserEmail($payment_detail['UserEmail'])
                ->setUserContact($payment_detail['UserContact'])
                ->setRemark($payment_detail['Remark'])
                ->setLang($payment_detail['Lang'])
                ->setResponseUrl($payment_detail['ResponseURL'])
                ->setBackendUrl($payment_detail['BackendURL'])
                ->prepare()
                ->process();
                // var_dump($response);
                // exit();
            } else { //is transfer bank
                $payment_detail = json_decode($payment_method->setting);
            }
            $this->main->update('orders', array('payment_to' => json_encode($payment_detail)), array('id' => $order));
            // $message = $this->load->view('email/transaction/invoice', $this->data, TRUE);
            // $cronjob = array(
            //     'from' => settings('send_email_from'),
            //     'from_name' => settings('store_name'),
            //     'to' => $this->data['user']->email,
            //     'subject' => 'Menunggu Pembayaran Invoice ' . $this->data['order']->code,
            //     'message' => $message
            // );
            // $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
            
            // if ($this->data['user']->verification_phone) {
            //     $sms = json_decode(settings('sprint_sms'), true);
            //     $sms['m'] = 'Segera lakukan pembayaran melalui ' . strtoupper($this->data['order']->payment_method) . ' dengan nominal ' . rupiah($this->data['order']->total) . ' sebelum ' . get_date_indo_full($this->data['order']->due_date) . ' WIB';
            //     if($this->data['order']->payment_method=='alfamart'){
            //         $sms['m'] .= PHP_EOL.'Kode Bayar: '.$payment_detail['account_number'];
            //     }elseif(in_array($this->data['order']->payment_method, ['mandiri_va','bni_va','bri_va','bca_va'])){
            //         $sms['m'] .= PHP_EOL.'Rek. Virtual: '.$payment_detail['account_number'];
            //     }
            //     $sms['d'] = $this->data['user']->phone;
            //     $this->main->insert('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
            // }

            if (in_array($data->payment_method, ['kredivo', 'bca_sakuku', 'creditcard','creditcard01','creditcard03','creditcard06','creditcard12'])) {
                redirect($response['response']['redirectURL']);
            } elseif ($data->payment_method == 'bca_klikpay') {
                $this->sprint->bca_klikpay($response['response']['redirectURL'], $response['response']['redirectData']);
            } else if (in_array($data->payment_method, ['ovo'])) {
                echo $responseIpay;
            } else {
                $res = array('msg' => 'Transfer Payment Created!','order_code' => $data_order['code']);
                $this->response($res, 200);
                //redirect('checkout/finish?id=' . $data_order['code']);
            }
        }
    }

    public function get_merchant_post()
    {
        $product = $this->post('prod_id');
        $quantity = $this->post('qty');

        $exp = explode(';', $product);
        $exp1 = explode(';', $quantity);
        if(count($exp) > 0){
            $data_res = array();
            for($a=0; $a<count($exp); $a++){
                if($exp[$a] != ''){
                    $pricing_level = $this->post('price_lvl');
                    $response = $this->checkout->get_merchants($exp[$a], $exp1[$a], $pricing_level);
                    $data_res[count($data_res)] = $response->result();
                    
                }
            }

            $this->response($data_res, 200);
        }else{
            $pricing_level = $this->post('price_lvl');
            $response = $this->checkout->get_merchants($product, $quantity, $pricing_level);
            $this->response($response->result(), 200);
        }
        
    }

    public function get_branch_post()
    {
        $group = $this->post('group');
        $response = $this->checkout->get_branch($group);
        $this->response($response, 200);
    }

    public function get_ongkir_post() {
        $this->load->library('rajaongkir');
        // $m_district = $this->post('m_district');
        // $c_district = $this->post('c_district');
        $id_cust = $this->post('id_cust');
        //$id_cust = '156';
        $id_merchant = $this->post('id_merchant');
        //$id_merchant = '132';
        if(count(explode(';', $id_merchant)) > 1){
            $id_merchant = explode(';', $id_merchant)[1];
        }
        $m_district = $this->m->get_data('district', 'merchants', null, array('id' => $id_merchant))->row()->district;
        $c_district = $this->m->get_data('district', 'customer_address', null, array('customer' => $id_cust))->row()->district;
        $qty = $this->post('qty');
        //$qty = '4';
        $weight = $this->post('weight');
        //$weight = '100';
        $courier = $this->post('courier');
        $courier = strtolower($courier);
        //$courier = 'jne';
        $services = $this->rajaongkir->cost($m_district, 'subdistrict', $c_district, 'subdistrict', ($qty * $weight), $courier);
        $this->response($services, 200);
    }

    public function get_payment_methods_post(){
        $this->db->select('pm.*,pmt.name as type_name,pmt.code as type_code');
        $this->db->order_by('pm.sort_order','asc');
        $this->db->where('pm.status',1);
        $this->db->join('payment_method_type pmt','pmt.id = pm.type','left');
        $datax = $this->db->get('payment_methods pm');
        $this->response($datax->result(), 200);
    }

    public function finished_post(){
        $data = $this->post(null, true);
        $this->data['user'] = json_decode($data['user'],false);
        $order = $data['order_id'];
        
        if (!$order){
            $res = array('msg' => 'Tidak ada data untuk no order ini!','status' => 'failed');
            $this->response($res, 400);
        }
            //exit;
            //redirect('cart');

        $order = $this->main->get('orders', array('code' => $order, 'customer' => $this->data['user']->id));
        if (!$order){
            $res = array('msg' => 'Tidak ada data untuk no order ini!','status' => 'failed');
            $this->response($res, 400);
        }
            //exit;
            //redirect('cart');
        if ($order->payment_status == 'Pending' && in_array($order->payment_method, ['bca_sakuku', 'bca_klikpay', 'kredivo', 'creditcard','creditcard01','creditcard03','creditcard06','creditcard12'])) {
            $order->payment_status = 'Failed';
            $this->main->update('orders', array('payment_status' => 'Failed'), array('id' => $order->id));
            $this->main->update('order_invoice', array('order_status' => settings('order_cancel_status')), array('order' => $order->id));
            $invoices = $this->main->gets('order_invoice', array('order' => $order->id));
            if ($invoices) {
                foreach ($invoices->result() as $invoice) {
                    $this->main->insert('order_history', array('order' => $order->id, 'invoice' => $invoice->id, 'order_status' => settings('order_cancel_status')));
                }
            }
            $this->data['order'] = $order;
            $this->data['customer'] = $this->main->get('customers', array('id' => $order->customer));
            $this->data['payment_method'] = $this->main->get('payment_methods', array('name' => $order->payment_method))->title;
            // $message = $this->load->view('email/transaction/payment_failed', $this->data, TRUE);
            // $cronjob = array(
            //     'from' => settings('send_email_from'),
            //     'from_name' => settings('store_name'),
            //     'to' => $this->data['customer']->email,
            //     'subject' => 'Pembayaran Tagihan ' . $this->data['order']->code.' Gagal',
            //     'message' => $message
            // );
            // $this->main->insert('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
        }
        $this->data['order'] = $order;
        $this->data['invoices'] = $this->main->gets('order_invoice', array('order' => $this->data['order']->id))->result();
        $this->data['customer'] = $this->main->get('customers', array('id' => $this->data['order']->customer));
        $res = array('msg' => 'View Order berhasil!','status' => 'success','data' => $this->data);
        $this->response($res, 200);
    }

    public function get_data_post(){
        $m = $this->m;
        $id_cust = $this->post('id_cust');

        $join = array(
            0 => 'cart_detail cd-cd.id_cart=c.id',
            1 => 'products p-p.id=cd.id_product',
            2 => 'customers cust-cust.id=c.id_customer'
        );
        $qcart = $m->get_data('c.*, p.name, p.id as p_id, cd.qty, cd.harga, p.weight, cd.price, cust.fullname as customer_name', 'cart c', $join, array('c.id_customer' => $id_cust));
        if($qcart->num_rows() > 0){
            $product = array();
            $ongkir = 0;
            //$grand_total = $qcart->row()->grand_total;
            $grand_total = 0;
            foreach($qcart->result() as $key){
                $qprod = $m->get_data('', 'product_image', null, array('product' => $key->p_id, 'primary' => '1'));
                $img = '';
                if($qprod->num_rows() > 0){
                    $img = $qprod->row()->image;
                }
                $product[] = array(
                    'name' => $key->name,
                    'weight' => number_format($key->weight),
                    'img' => $img,
                    'harga' => number_format($key->harga),
                    'qty' => $key->qty,
                    'sub_total' => number_format($key->harga * $key->qty),
                    'key' => $k->p_id
                );
                $ongkir += $key->price;
                $grand_total += $key->qty * $key->harga;
            }

            $grand_total += $ongkir;

            $res = array(
                'product' => $product,
                'ongkir' => number_format($ongkir),
                'grand_total' => number_format($grand_total),
                'cust_name' => $qcart->row()->customer_name,
                'success' => 1
            );
        }else{
            $res = array('success' => 0, 'msg' => 'Data Pembelian Tidak Ada');
        }

        $this->response($res, 200);
    }

    public function update_ongkir_post(){
        $m = $this->m;
        $this->load->library('rajaongkir');
        
        $id_cust = $this->post('id_cust');
        //$data_by_merchant = json_decode($this->post('data_by_merchant'), true);
        $data_by_merchant = $this->post('data_by_merchant');

        $join = array(
            0 => 'customer_address ca-ca.id=c.id_address'
        );
        $qcart = $m->get_data('c.*, ca.district', 'cart c', $join, array('c.id_customer' => $id_cust));
        if($qcart->num_rows() > 0){
            $grand_total = 0;
            for($a=0; $a<count($data_by_merchant); $a++){
                $shipping = $data_by_merchant[$a]['shipping'];
                //die(json_encode(array('g' => $shipping)));
                for($b=0; $b<count($shipping); $b++){
                    $service = $data_by_merchant[$a]['shipping'][$b]['service'];
                    //die(json_encode(array('gg' => $service)));
                    for($c=0; $c<count($service); $c++){
                        $product = $data_by_merchant[$a]['shipping'][$b]['service'][$c]['product'];
                        $cnt_product = count($product);
                        $total_ekspedisi = 0;
                        
                        if($cnt_product > 1){
                            $weight = 0;
                            for($e=0; $e<count($product); $e++){
                                $weight += $product[$e]['weight'] * $product[$e]['qty'];
                            }
                            
                            $m_district = $m->get_data('district', 'merchants', null, array('id' => $data_by_merchant[$a]['id_merchant']))->row()->district;                            
                            $c_district = $qcart->row()->district;
                            // $m_district = 2100;
                            // $c_district = 2100;
                            $courier = $shipping[$b]['id'];
                            $courier = strtolower($courier);
                            
                            $services = $this->rajaongkir->cost($m_district, 'subdistrict', $c_district, 'subdistrict', $weight, $courier);

                            $services = json_decode($services);

                            //cek service
                            $price = $service[$c]['price'];
                            for($s=0; $s<count($services->rajaongkir->results[0]->costs); $s++){
                                if($service[$c]['id'] == $services->rajaongkir->results[0]->costs[$s]->service){
                                    $price = $services->rajaongkir->results[0]->costs[$s]->cost[0]->value;
                                }
                            }

                            for($d=0; $d<count($product); $d++){
                                $data_update = array(
                                    'qty' => $product[$d]['qty'],
                                    'subtotal' => $product[$d]['qty'] * $product[$d]['price'],
                                    'shipping' => $shipping[$b]['id'],
                                    'shipping_service' => $service[$c]['id'],
                                    'shipping_cost' => $service[$c]['price'],
                                    'price' => $price/$cnt_product,
                                    'id_merchant' => $data_by_merchant[$a]['id_merchant']
                                );
        
                                $where = array(
                                    'id_cart' => $qcart->row()->id,
                                    'id_product' => $product[$d]['id']
                                );
    
                                $m->update_data('cart_detail', $data_update, $where);

                                $harga = $m->get_data('harga', 'cart_detail', null, array('id_cart' => $qcart->row()->id, 'id_product' => $product[$d]['id']))->row()->harga;

                                $grand_total += ($product[$d]['qty'] * $harga) + ($price / $cnt_product);
                                //die(json_encode(array('d' => $product[$d]['id'])));
                            }
                        }else{
                            $data_update = array(
                                'qty' => $product[0]['qty'],
                                'subtotal' => $product[0]['qty'] * $product[0]['price'],
                                'shipping' => $shipping[$b]['id'],
                                'shipping_service' => $service[$c]['id'],
                                'shipping_cost' => $service[$c]['price'],
                                'price' => $service[$c]['price'],
                                'id_merchant' => $data_by_merchant[$a]['id_merchant']
                            );
    
                            $where = array(
                                'id_cart' => $qcart->row()->id,
                                'id_product' => $product[0]['id']
                            );

                            $m->update_data('cart_detail', $data_update, $where);

                            $harga = $m->get_data('harga', 'cart_detail', null, array('id_cart' => $qcart->row()->id, 'id_product' => $product[$d]['id']))->row()->harga;

                            $grand_total += ($product[0]['qty'] * $harga) + $service[$c]['price'];
                            //die(json_encode(array('d1' => $product[0]['id'])));
                        }
                    }
                }
            }

            $m->update_data('cart', array('grand_total' => $grand_total, 'status' => '2'), array('id' => $qcart->row()->id));

            $res = array('success' => 1, 'msg' => 'Data Berhasil Diproses', 'a' => $qcart->row()->id);
        }else{
            $res = array('success' => 0, 'msg' => 'Data Keranjang Tidak Ada');
        }
        $this->response($res, 200);
    }

    public function add_order_post(){
        $m = $this->m;
        $id_cust = $this->post('id_cust');
        $payment_method = $this->post('payment_method');

        $join = array(
            0 => 'customer_address ca-ca.id=c.id_address',
            1 => 'customers cust-cust.id=c.id_customer'
        );
        $cek = $m->get_data('*, c.id as c_id, cust.email, cust.verification_phone, cust.phone, cust.fullname', 'cart c', $join, array('c.id_customer' => $id_cust));

        if($cek->num_rows() > 0){
            
            $address = $cek->row();
            $code_order = $m->order_code();
            $due_date = date('Y-m-d H:i:s', strtotime(' +1 day'));
            $data_order = array(
                'code' => $code_order,
                'customer' => $id_cust,
                'payment_method' => $payment_method,
                'due_date' => $due_date,
    //                'due_date' => date('Y-m-d H:i:s', strtotime(' +2 minutes')),
                'shipping_name' => $address->name,
                'shipping_address' => $address->address,
                'shipping_phone' => $address->phone,
                'shipping_province' => $address->province,
                'shipping_city' => $address->city,
                'shipping_district' => $address->district,
                'shipping_postcode' => $address->postcode,
            );

            $m->insert_data('orders', $data_order);
            $order = $m->get_data('id', 'orders', null, array('code' => $code_order), '', 'id desc', 0, 1)->row()->id;
            //echo 'order '.$order;
            //if($coupon) {
            if(false){
                $data_coupon = array(
                    'coupon' => $coupon->id,
                    'order' => $this->db->insert_id(),
                    'customer' => $this->data['user']->id,
                    'amount' => $coupon->total_disc,
                );
                $coupon_history = $this->main->insert('coupon_history', $data_coupon);  
            }
            $data_invoice = array(
                'customer' => $id_cust,
                'order_status' => settings('order_new_status'),
                'order' => $order
            );
            //echo 'ee';
            $cart = array();

            $join = array(
                0 => 'products p-p.id=cd.id_product'
            );
            $qcart_detail = $m->get_data(
                'cd.*,
                p.id as p_id, p.*',
                'cart_detail cd', $join, array('cd.id_cart' => $cek->row()->c_id)
            );
            //echo 'dd';
            $cnt = 0;
            foreach($qcart_detail->result() as $key){
                $id = $key->id_merchant.'-'.$key->shipping;
                
                $cart[$id]['merchant'] = $key->id_merchant;
                $cart[$id]['shipping'] = $key->shipping.'-'.$key->shipping_service;
                $cart[$id]['shipping_cost'] = isset($cart[$id]['shipping_cost']) ? $cart[$id]['shipping_cost'] + $key->shipping_cost : $key->shipping_cost;
                $cart[$id]['subtotal'] = isset($cart[$id]['subtotal']) ? $cart[$id]['subtotal'] + $key->subtotal : $key->subtotal;
                $cart[$id]['weight'] = isset($cart[$id]['weight']) ? $cart[$id]['weight'] + $key->weight : $key->weight;
                $key->description = '';
                $cart[$id]['product'][$cnt] = $key;
                $cnt++;
            }
            ksort($cart, SORT_NUMERIC);

            $order_total = 0;
            $order_shipping = 0;
            foreach ($cart as $merchant => $data) {
                $code_inv = $m->invoice_code();
                $data_invoice['code'] = $code_inv;
                $data_invoice['merchant'] = $data['merchant'];
                $data_invoice['shipping_courier'] = $data['shipping'];
                $data_invoice['shipping_cost'] = $data['shipping_cost'];
                $data_invoice['shipping_weight'] = $data['weight'];
                $data_invoice['subtotal'] = $data['subtotal'];
                $data_invoice['total'] = $data['subtotal'] + $data['shipping_cost'];
                $order_total += $data_invoice['subtotal'];
                $order_shipping += $data['shipping_cost'];
                //var_dump($data['product']);
                //exit();
                $m->insert_data('order_invoice', $data_invoice);
                $invoice = $m->get_data('id', 'order_invoice', null, array('code' => $code_inv), '', 'id desc', 0, 1)->row()->id;
                //echo 'inv '.$invoice;
                //print_r($data['product']);
                foreach ($data['product'] as $item) {
                    $data_product = array(
                        'order' => $order,
                        'invoice' => $invoice,
                        'product' => $item->p_id,
                        'name' => $item->name,
                        'code' => $item->code,
                        'price' => $item->price,
                        'quantity' => $item->qty,
                        'weight' => $item->weight,
                        'total' => $item->subtotal,
                        //'options' => json_encode($item->options),
                        'promo_data' => json_encode($item->promo_data)
                    );

                    $inPromo = false;
                    if($item->promo == '1'){
                        $datapromo = json_decode($item->promo_data);
                        $datestart = (int) str_replace('-', '', $datapromo->date_start);
                        $dateend = (int) str_replace('-', '', $datapromo->date_end);
                        $datenow = (int) date('Ymd');
                        if($datapromo->type != 'F' && $datestart <= $datenow && $dateend >= $datenow){
                            $inPromo = true;
                            $disc_ = $datapromo->discount;
                        }
                    }
                    
                    if($inPromo){
                        
                        $data_product['price_old'] = $item->price_old;
                        $data_product['discount'] = $item->qty * $item->discount;
                    }
                    $m->insert_data('order_product', $data_product);
                    $order_product = $m->get_data('id', 'order_product', null, array('order' => $order, 'invoice' => $invoice, 'product' => $item->p_id), '', 'id desc', 0, 1)->row()->id;

                    //echo 'op '.$order_product;
                }
                $m->insert_data('order_history', array('order' => $order, 'invoice' => $invoice, 'order_status' => settings('order_new_status')));
                //echo 'oh';
            }
            if(false) {
            //if($coupon) {
                $order_total = $order_total -  $coupon->total_disc;
            }
            $m->update_data('orders', array('total' => $order_total + $order_shipping, 'subtotal' => $order_total, 'shipping_cost' => $order_shipping), array('id' => $order));
            
            
            



            //$this->load->model('orders');
            $data = $m->get_data(
                'o.*, cst.fullname customer_name, cst.email customer_email, cst.phone customer_phone, p.name shipping_province_name, c.name shipping_city_name, d.name shipping_district_name',
                'orders o',
                array(
                    0 => 'customers cst-cst.id=o.customer',
                    1 => 'provincies p-p.id=o.shipping_province',
                    2 => 'cities c-c.id=o.shipping_city',
                    3 => 'districts d-d.id=o.shipping_district'
                ),
                array('o.id' => $order)
            )->row();
            $data_['order'] = $data;
            $data_['customer'] = $m->get_data('', 'customers', null, array('id' => $data_['order']->customer))->row();
            $data_['invoices'] = $m->get_data('', 'order_invoice', null, array('order' => $data_['order']->id));

            $payment_method = $m->get_data('', 'payment_methods', null, array('name' => $payment_method))->row();
            if ($payment_method->vendor == 'xendit') {
                if ($payment_method->name == 'credit_card') {
                    $res = array('success' => 0, 'msg' => 'Credit Card belum tersedia', 'redirect' => 'credit_card', 'order' => $data_order['code']);
                } else {

                    $res = array('success' => 1, 'msg' => 'Xendit Error Response', 'redirect' => 'xendit', 'data' => array(
                        'code' => $data_order['code'],
                        'grand_total' => $order_total+$order_shipping,
                        'email' => $address->email,
                        'data_order' => $data->payment_method,
                        'order' => $order,
                        'payment_method' => $payment_method
                    ));
                }
            } elseif ($payment_method->vendor == 'sprint') {
                $res = array('success' => 1, 'msg' => 'Sprint Error Response', 'redirect' => 'sprint', 'data' => array(
                    'transaction' => $data,
                    'order' => $order,
                    'payment_method' => $payment_method,
                    'data' => $data
                ));
            } elseif ($payment_method->vendor == 'ipay88') {
                $this->load->library('ipay');
                $this->load->config('ipay88', TRUE);
                $transaction = $this->data['order'];
                $cust = $this->data['customer'];
                $merchant_code = $this->config->item('merchant_code', 'ipay88');
                $merchant_key = $this->config->item('merchant_key', 'ipay88');
                $responseUrl = $this->config->item('responseURL', 'ipay88');
                $backendUrl = $this->config->item('backendURL', 'ipay88');
                $payment_detail = array(
                    'MerchantCode' => $merchant_code,
                    'MerchantKey' => $merchant_key,
                    'RefNo' => $transaction->code,
                    'Amount' => $transaction->total,
                    'Currency' => 'IDR',
                    'ProdDesc' => $transaction->code,
                    'UserName' => $cust->fullname,
                    'UserEmail' => $cust->email,
                    'UserContact' => $cust->phone,
                    'Remark' => '',
                    'Lang' => 'UTF-8',
                    'ResponseURL' => $responseUrl,
                    'BackendURL' => $backendUrl,
                );
                if($data->payment_method == 'ovo'){
                    $payment_detail['PaymentId'] = 1;
                } elseif($data->payment_method == 'tcash'){
                    $payment_detail['PaymentId'] = 15;
                } else {
                    $payment_detail['PaymentId'] = 1;
                }
                //$payment_detail['xfield1'] = '||IPP:3||';
                $responseIpay = $this->ipay->setMerchantCode($payment_detail['MerchantCode'])
                ->setMerchantKey($payment_detail['MerchantKey'])
                ->setPaymentId($payment_detail['PaymentId'])
                ->setRefNo($payment_detail['RefNo'])
                ->setAmount($payment_detail['Amount'])
                ->setCurrency($payment_detail['Currency'])
                ->setProdDesc($payment_detail['ProdDesc'])
                ->setUsername($payment_detail['UserName'])
                ->setUserEmail($payment_detail['UserEmail'])
                ->setUserContact($payment_detail['UserContact'])
                ->setRemark($payment_detail['Remark'])
                ->setLang($payment_detail['Lang'])
                ->setResponseUrl($payment_detail['ResponseURL'])
                ->setBackendUrl($payment_detail['BackendURL'])
                ->prepare()
                ->process();
                // var_dump($response);
                // exit();
            } else { //is transfer bank
                $payment_detail = json_decode($payment_method->setting);
                $res = array('success' => 1, 'redirect' => 'transfer', 'payment_detail' => $payment_detail,'order_code' => $data_order['code']);
            }

            $m->update_data('orders', array('payment_to' => json_encode($payment_detail)), array('id' => $order));
            
            $message = $this->view_inv($data_['order'], $address, $data_['invoices']);
            //echo $message;
            $cronjob = array(
                'from' => settings('send_email_from'),
                'from_name' => settings('store_name'),
                'to' => $address->email,
                'subject' => 'Menunggu Pembayaran Invoice ' . $data_['order']->code,
                'message' => $message
            );

            $m->insert_data('cron_job', array('type' => 'email', 'content' => json_encode($cronjob)));
            if($address->verification_phone){
            //if ($this->data['user']->verification_phone) {
//                $sms = json_decode(settings('sprint_sms'), true);
//                $sms['m'] = 'Segera lakukan pembayaran melalui ' . strtoupper($data_['order']->payment_method) . ' dengan nominal ' . rupiah($data_['order']->total) . ' sebelum ' . get_date_indo_full($data_['order']->due_date) . ' WIB';
//                if($data_['order']->payment_method=='alfamart'){
//                    $sms['m'] .= PHP_EOL.'Kode Bayar: '.$payment_detail['account_number'];
//                }elseif(in_array($data_['order']->payment_method, ['mandiri_va','bni_va','bri_va','bca_va'])){
//                    $sms['m'] .= PHP_EOL.'Rek. Virtual: '.$payment_detail['account_number'];
//                }
//                $sms['d'] = $address->phone;
//                $m->insert_data('cron_job', array('type' => 'sms', 'content' => json_encode($sms)));
                //print_r($sms);
            }


            $res['due_date'] = date('d/m/Y', strtotime(' +1 day'));
            $res['due_date'] .= ' pukul '.date('H').':00 WIB';

            //delete cart

            $qcart = $m->get_data('id', 'cart', null, array('id_customer' => $id_cust));
            foreach($qcart->result() as $key){
                $m->delete_data('cart_detail', array('id_cart' => $key->id));
            }
            $m->delete_data('cart', array('id_customer' => $id_cust));



            
        }else{
            $res = array(
                'success' => 0,
                'msg' => 'Invalid Request'
            );
        }

        $this->response($res, 200);
    }

    public function delete_post(){
        $id = $this->post('id');
        $tbl = $this->post('tbl');
        $m = $this->m;
        $c = $m->get_data('', 'cart', null, array('id_customer' => 0));
        foreach($c->result() as $key){
            $m->delete_data('cart_detail', array('id_cart' => $key->id));
        }
        $m->delete_data('cart', array('id_customer' => 0));
    }

    public function get_tbl_post(){
        $tbl = $this->post('table');
        $id = $this->post('id');
        $name = $this->post('name');
        if($id && $name){
            $where = array($name => $id);
        }else{
            $where = null;
        }
        $q = $this->m->get_data('', $tbl, null, $where, '', 'id desc');

        die(json_encode(array('s' => $q->result())));
    }

    public function FunctionName(){
        // [
        //     {
        //         "id_merchant":"132",
        //         "shipping":[
        //             {
        //                 "id":"Jne",
        //                 "service":[
        //                     {
        //                         "id":"OKE",
        //                         "price":"19.000",
        //                         "product":[
        //                             {
        //                                 "id":"150",
        //                                 "qty":"2",
        //                                 "price":"17900",
        //                                 "weight":"50"
        //                             },
        //                             {
        //                                 "id":"274",
        //                                 "qty":"3",
        //                                 "price":"17200",
        //                                 "weight":"100"
        //                             }
        //                         ]
        //                     }
        //                 ]
        //             }
        //         ]
        //     }
        // ]
    }

    public function sprint_req_post(){
        $this->load->library('sprint');
        $transaction = $this->post('transaction');
        $order = $this->post('order');
        $payment_method = $this->post('payment_method');
        $data = $this->post('data');

        $response = $this->sprint->process($transaction);
        die(json_encode(array('res' => 'asd')));
        
        $this->m->insert_data('order_meta', array('order_id' => $order, 'meta_name' => 'sprint_request', 'meta_value' => json_encode($response['request'])));
        $payment_detail = array(
            'vendor' => 'sprint',
            'account_code' => $response['request']['channelId'],
            'account_name' => $payment_method->title,
            'account_holder_name' => isset($payment_method->merchant_name) ? $payment_method->merchant_name : '',
            'account_branch' => '',
            'account_number' => ($data->payment_method == 'bca_va') ? $response['request']['customerAccount'] : '',
            'amount' => $response['request']['transactionAmount'],
        );
        if (in_array($data->payment_method, ['kredivo', 'bca_sakuku', 'creditcard','creditcard01','creditcard03','creditcard06','creditcard12'])) {
            $payment_detail['payment_url'] = $response['response']['redirectURL'];
            $res = array('success' => 1, 'payment_detail' => $payment_detail);
        } elseif ($data->payment_method == 'bca_klikpay') {
            $payment_detail['payment_url'] = $response['response']['redirectURL'];
            $payment_detail['payment_data'] = $response['response']['redirectData'];
            $res = array('success' => 2, 'payment_detail' => $payment_detail);
        }
        $this->response($res, 200);
    }

    public function view_inv($order, $customer, $invoices){
        $m = $this->m;
        $fulldate = $order->date_added;
        
        $fulldate = date('Y/m/d H:i:s', strtotime($fulldate));
        $date_ = explode(' ', $fulldate)[0];
        $time_ = explode(' ', $fulldate)[1];

        $date = explode('/', $date_)[2];
        //$date = substr($fulldate, 8, 2);
        $month = explode('/', $date_)[1];
        //$month = get_month(substr($fulldate, 5, 2));
        $year = explode('/', $date_)[0];
        //$year = substr($fulldate, 0, 4);
        $time = explode(':', $time_)[0];
        //$time = substr($fulldate, 10);
        $date_full_indo = $date . ' ' . $month . ' ' . $year . ', ' . $time;

        $datanya = '
        <div bgcolor="#FFFFFF" style="font-family:\'Helvetica Neue\',\'Helvetica\',Helvetica,Arial,sans-serif;width:100%!important;height:100%;font-size:14px;color:#404040;margin:0;padding:0">
            <table style="max-width:100%;border-collapse:collapse;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:0" bgcolor="transparent">
                <tbody>
                    <tr style="margin:0;padding:0">
                        <td style="margin:0;padding:0"></td>
                        <td style="display:block!important;max-width:600px!important;clear:both!important;margin:0 auto;padding:0" bgcolor="#FFFFFF">
                            <div style="max-width:600px;display:block;border-collapse:collapse;margin:0 auto;border:1px solid #e7e7e7">
                                <table style="max-width:100%;border-spacing:0;width:100%;background-color:transparent;margin:0;padding:20px;" bgcolor="transparent">
                                    <tbody>
                                        <tr style="margin:0;padding:0">
                                            <td>
                                                <img src="'.$this->url_base.'assets/frontend/images/logo.png" alt="" style="margin:10px 0">
                                            </td>
                                        </tr>
        
                                        <tr style="margin:0;padding:0">
                                            <td style="margin:0;padding:0">
                                                <h5 style="line-height:32px;color:#666;font-weight:700;font-size:24px;margin:0 0 20px;padding:0">Segera selesaikan pembayaran Anda</h5>
                                                <p style="font-weight:normal;color:#999;font-size:14px;line-height:1.6;margin:0 0 20px;padding:0">
                                                    Hai '.$customer->fullname.',<br>
                                                    Terimakasih atas kepercayaanmu berbelanja di '.settings('store_name').' pada tanggal '.$date_full_indo.'. Berikut adalah detail tagihanmu:
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table style="width:100%;margin-bottom:24px;padding:0 20px;">
                                    <thead>
                                        <tr><td style="width:50%">
                                                <span style="font-size:12px;color:#666;font-weight:600">Total Pembayaran</span>
                                            </td>
                                            <td style="width:50%">
                                                <span style="font-size:12px;color:#666;font-weight:600">Batas Waktu Pembayaran</span>
                                            </td>
                                        </tr></thead>
                                    <tbody>
                                        <tr>
                                            <td style="vertical-align:top;">
        
                                                <p style="font-size:16px;color:#999;margin:0 0 5px 0">'.number_format($order->total, 2).'</p>
        
                                            </td>
                                            <td style="vertical-align:top;">
                                                <p style="font-size:16px;color:#999;margin:0 0 5px 0">'.$date_full_indo.'</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table style="width:100%;margin-bottom:24px;padding:0 20px; ">
                                    <thead>
                                        <tr><td style="width:50% ">
                                                <span style="font-size:12px;color:#666;font-weight:600 ">Metode Pembayaran</span>
                                            </td>
                                            <td style="width:50% ">
                                                <span style="font-size:12px;color:#666;font-weight:600 ">Referensi Pembayaran</span>
                                            </td>
                                        </tr></thead>
                                    <tbody>
                                        <tr>
                                            <td style="vertical-align:top">
                                                <p style="font-size:16px;color:#999;margin:5px 0">'.strtoupper($order->payment_method).'</p>
                                            </td>
                                            <td style="vertical-align:top">
                                                <p style="font-size:16px;color:#999;margin:5px 0">'.$order->code.'</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table style="width:100%;margin-bottom:24px;padding:0 20px;">
                                    <thead>
                                        <tr><td style="width:50%">
                                                <span style="font-size:12px;color:#666;font-weight:600">Status Pembayaran</span>
                                            </td>
                                        </tr></thead>
                                    <tbody>
                                        <tr>
                                            <td style="vertical-align:top">
                                                <span style="font-size:16px;color:#999;">Menunggu Pembayaran</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table style="width:100%;margin-bottom:24px;padding:0 20px;">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div style="border: 1px solid #e7e7e7;border-radius: 5px;padding: 0px 20px;">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <h2 style="font-size:14px;font-weight:600;color:#666;margin:50px 0 16px 0;padding:0 20px;">Detail Pemesanan</h2>
                                <span style="width:30px;border:2px solid #FF5722;display:inline-block;margin-left:20px;"></span>
                                <table style="width:100%;margin-top:16px;margin-bottom:18px;border-bottom:1px solid #E0E0E0;padding: 0 20px;">
                                    <tbody>';
        foreach ($invoices->result() as $invoice) {
            $datanya .= '
                                            <tr>
                                                <td style="padding-top: 16px;" colspan="2">
                                                    <p style="color:#97C23C;text-decoration:none;">'.$invoice->code.'</p>
                                                </td>
                                            </tr>';
            $qorder_product = $m->get_data('', 'order_product', null, array('invoice' => $invoice->id))->result();
            foreach ($qorder_product as $product) {
                $datanya .= '
                                                <tr>
                                                    <td style="vertical-align:top">
                                                        <p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;">'.$product->name.'</p>
                                                        <p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:8px;">'.$product->quantity.' X '.number_format($product->price, 2).'</p>
                                                    </td>
                                                    <td style="vertical-align:top;text-align:right;width:120px;">
                                                        <span style="float:left;font-size:14px;color:#999;margin-top:5px;">Rp</span>
                                                        <p style="float:right;font-size:14px;color:#999;margin-top:5px;">'.number_format($product->total).'</p>
                                                    </td>
                                                </tr>';
            }
            $datanya .= '
                                            <tr>
                                                <td style="vertical-align:top">
                                                    <p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;">';
            if ($invoice->shipping_courier != 'pickup') {
                $invoice->shipping_courier = explode('-', $invoice->shipping_courier);
                $invoice->shipping_courier = strtoupper($invoice->shipping_courier[0]) . ' ' . $invoice->shipping_courier[1];
            } else {
                $invoice->shipping_courier = 'Ambil Sendiri';
            }
            $datanya .=  $invoice->shipping_courier.'
                                                    </p>
                                                </td>
                                                <td style="vertical-align:top;text-align:right;width:120px;">
                                                    <span style="float:left;font-size:14px;color:#999;margin-top:5px;">Rp</span>
                                                    <p style="float:right;font-size:14px;color:#999;margin-top:5px;">'.number_format($invoice->shipping_cost).'</p>
                                                </td>
                                            </tr>';
        }
        //echo '0<br>';

        $datanya .= '
                                    </tbody>
                                </table>
                                <table style="width:100%;margin-bottom:20px;padding:0 20px;">
                                    <tbody>
                                        <tr style="margin-bottom:16px;">
                                            <td style="vertical-align:top">
                                                <p style="font-size:14px;color:#999;margin-top:5px;margin-bottom:5px;font-weight:bold;">Total Pembayaran</p>
                                            </td>
                                            <td style="vertical-align:top;text-align:right;width:120px;">
                                                <span style="float:left;font-size:14px;color:#999;margin-top:5px;font-weight:bold;">Rp</span>
                                                <p style="float:right;font-size:14px;color:#999;margin-top:5px;font-weight:bold;">'.number_format($order->total).'</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div style="padding:0 20px">
                                    <p style="font-size:14px;color:#999;padding:24px 0 10px;margin:0;border-top: 1px solid #E0E0E0;">
                                        Email dibuat secara otomatis. Mohon tidak mengirimkan balasan ke email ini.
                                    </p>
                                </div>
                            </div>
                        </td>
                        <td style="margin:0;padding:0"></td>
                    </tr>
                </tbody>
            </table>
        </div>
        ';
        //echo $datanya;
        return $datanya;
    }
}

