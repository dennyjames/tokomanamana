<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Search extends REST_Controller {
    
    public $table = "products";
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $params = array('rounds' => 8);
        $params['salt_prefix'] = version_compare(PHP_VERSION, '5.3.7', '<') ? '$2a$' : '$2y$';
        $this->load->library('bcrypt', $params);
    }

    public function view() {
        $limit = $this->get('limit');
        $cat_id = $this->get('id');
        if($limit){
            $this->db->limit($limit);
        }
        if($cat_id){
            $this->db->where('a.category', $cat_id);
        }
        $this->db->select('a.id,a.name,a.code,c.image,a.barcode,a.category as category_id,b.name as category_name,a.brand,a.weight,
        a.length,a.width,a.height,a.price_old,a.price,a.discount,a.quantity,a.package_items,a.viewed,
        a.date_added');
        $this->db->join('categories as b','b.id = a.category','left');
        $this->db->join('product_image as c','c.product = a.id AND primary = 1','left');
        $this->db->where('a.status', '1');
        $this->db->order_by('a.name','ASC');
        $data = $this->db->get($this->table.' as a')->result();
        $this->response($data, 200);
    }

    public function get_merchant_post(){
        //$location = "'lat':'-6.186486399999999','lng':'106.83409110000002'";
        $location = json_decode($this->post('location'),true);
        //var_dump($location);exit();
        $merchant = $this->db->query("SELECT id, `name`, `group`, (6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) AS distance FROM merchants HAVING distance < 80 ORDER BY distance");
        if ($merchant->num_rows() > 0) {
            $data = $merchant->result();
        } else {
            $merchant = $this->db->query("SELECT id, `name`, `group`, (6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) AS distance FROM merchants ORDER BY distance LIMIT 5");
            $data = $merchant->result();
        }
    //$data = "'lat':'-6.186486399999999','lng':'106.83409110000002'";
    //$data = json_decode($this->post('location'),true);
    //$data = $this->post('location'), true); 

        $this->response($data, 200); //200 parameter sukses x 400 = GAGAL
    }

    public function search_post() {
        $word = $this->post('word');
        $merchant_ = json_decode($this->post('merchant'), true);
        $merchant_group = $merchant_[0]['group'];
        foreach($merchant_ as $m){
            $merchant[] = $m['id'];
        }
        $sort = 'popular';
        $limit = 20;
        $start = 0;
        $query_order = 'ORDER BY ';
        switch ($sort) {
            default:
            case 'popular':
                $query_order .= 'p.viewed DESC';
                break;
            case 'newest' :
                $query_order .= 'p.date_added DESC';
                break;
            case 'lowprice' :
                $query_order .= 'p.price ASC';
                break;
            case 'highprice' :
                $query_order .= 'p.price DESC';
                break;
            case 'nameAZ' :
                $query_order .= 'p.name ASC';
                break;
            case 'nameZA' :
                $query_order .= 'p.name DESC';
                break;
        }
        $query_limit = '';
        if ($limit) {
            $query_limit = 'LIMIT ' . $start . ',' . $limit;
        }
        $merchant = implode(',', $merchant);
        $where = '';
        $data_ = $this->db->query("SELECT * FROM (
                    SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, IFNULL(pp.price, p.price) price, pi.image, pp.id ppid, p.viewed, p.date_added, p.meta_keyword, c.name cat_name
                    FROM products p
                    LEFT JOIN product_price pp ON pp.product = p.id AND pp.merchant_group = $merchant_group
                    LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
                    LEFT JOIN categories c ON c.id = p.category
                    WHERE p.merchant = 0 AND p.status = 1
                    UNION
                    SELECT p.id, p.merchant, p.name, p.short_description, p.code, p.category, p.brand, p.price, pi.image, 0, p.viewed, p.date_added, p.meta_keyword, c.name cat_name
                    FROM products p
                    LEFT JOIN product_image pi ON pi.product = p.id AND pi.primary = 1
                    LEFT JOIN categories c ON c.id = p.category
                    WHERE merchant IN (SELECT id FROM merchants WHERE id IN ($merchant) AND status = 1)  AND p.status = 1
                    ) p
                 WHERE (p.code LIKE '%$word%' OR p.name LIKE '%$word%' OR p.meta_keyword REGEXP '$word' OR cat_name LIKE '%$word%') $where $query_order $query_limit");
        $data = $data_->result();
        $this->response($data, 200);
    }

    /*public function test_post(){
        var_dump(json_decode($this->post('merchant'),true));
         //var_dump($this->post('merchant'));
    }*/
}
