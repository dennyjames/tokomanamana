<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Product extends REST_Controller {
    
    public $table = "products";
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $params = array('rounds' => 8);
        $params['salt_prefix'] = version_compare(PHP_VERSION, '5.3.7', '<') ? '$2a$' : '$2y$';
        $this->load->library('bcrypt', $params);
    }
    
    public function all_get()
    {
        $limit = $this->get('limit');
        $cat_id = $this->get('id');
        if($limit){
            $this->db->limit($limit);
        }
        if($cat_id){
            $this->db->where('a.category', $cat_id);
        }
        $this->db->select('a.id,a.name,a.code,c.image,a.barcode,a.category as category_id,b.name as category_name,a.brand,a.weight,
        a.length,a.width,a.height,a.price_old,a.price,a.discount,a.quantity,a.package_items,a.viewed,
        a.date_added');
        $this->db->join('categories as b','b.id = a.category','left');
        $this->db->join('product_image as c','c.product = a.id AND primary = 1','left');
        $this->db->where('a.status', '1');
        $this->db->order_by('a.name','ASC');
        $data = $this->db->get($this->table.' as a')->result();
        $this->response($data, 200);
    }

    public function best_seller_get()
    {
        $limit = $this->get('limit');
        if(!$limit){
            $limit = 30;
        }

        $this->db->limit($limit);
        $this->db->select('b.id,b.name,b.code,c.image,b.barcode,b.category as category_id,b.brand,b.weight,
        b.length,b.width,b.height,b.price_old,b.price,b.discount,b.quantity,b.package_items,b.promo,b.promo_data,b.viewed,
        b.date_added,count(a.product) AS total');
        $this->db->join($this->table.' as b','b.id = a.product','left');
        $this->db->join('product_image as c','c.product = b.id AND primary = 1','left');
        $this->db->where('b.status', '1');
        $this->db->order_by('total DESC');
        $this->db->group_by('a.product');
        $data = $this->db->get('order_product as a')->result();
        foreach($data as $key){
            $key->price = 'Rp.'.number_format($key->price);
        }
        $this->response($data, 200);
    }

    public function promo_get()
    {
        $limit = $this->get('limit');
        if(!$limit){
            $limit = 100;
            
        }
        $this->db->limit($limit);
        $this->db->select('a.id,a.name,a.code,b.image,a.barcode,a.category as category_id,a.brand,a.weight,
        a.length,a.width,a.height,a.price_old,a.price,a.discount,a.quantity,a.package_items,a.promo_data,a.viewed,
        a.date_added');
        $this->db->join('product_image as b','b.product = a.id AND primary = 1','left');
        $this->db->where('a.status', '1');
        $this->db->where('a.promo', '1');
        $data = $this->db->get($this->table.' as a')->result();

        foreach($data as $key){
            $key->price = 'Rp.'.number_format($key->price);
        }
        $this->response($data, 200);
    }

    public function view_post() {
        $id = $this->post('id');
        $this->db->select("p.*, pi.image, b.name brand_name, m.name merchant_name, p.quantity", FALSE)
                ->join('product_image pi', 'p.id = pi.product AND primary = 1', 'left')
                ->join('brands b', 'b.id = p.brand', 'left')
                ->join('merchants m', 'm.id = p.merchant', 'left')
                ->where('p.status', 1)
                ->where('p.id', $id);
        $data = $this->db->get('products p')->result();
        $this->response($data, 200);
    }

    public function related_product_post() {
        $id = $this->post('id');
        $cat_id = $this->post('category_id');
        $limit = 6;
        $this->db->select('p.id, p.name, p.merchant, p.price,p.promo,p.promo_data, pi.image')
                ->join('product_image pi', 'pi.product = p.id AND pi.primary = 1', 'left')
                ->where('p.status !=', 0)
                ->where('p.category',$cat_id)
                ->where('p.id !=',$id)
                ->order_by('RAND()')
                ->limit($limit);
        $data = $this->db->get('products p')->result();
        $this->response($data, 200);  
    }

}
