<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Cart extends REST_Controller {
    
    public $table = "products";
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $params = array('rounds' => 8);
        $this->load->model('Product_model', 'product');
        $params['salt_prefix'] = version_compare(PHP_VERSION, '5.3.7', '<') ? '$2a$' : '$2y$';
        $this->load->library('bcrypt', $params);
        $this->load->model('Model', 'm');
    }

    public function get_data_cart_post(){
        $m = $this->m;
        $id_cust = $this->post('id_cust');
        // $id_cust = '156';

        $cek = $m->get_data('id, status', 'cart', null, array('id_customer' => $id_cust));
            

        if($cek->num_rows() > 0){
            $join = array(
                0 => 'products p-p.id=cd.id_product',
                1 => 'merchants m-m.id=p.merchant'
            );

            $cek_cart = $m->get_data(
                'cd.id_product, p.*, cd.qty, cd.harga, cd.level_harga, m.name as m_name, m.group, m.type, m.shipping',
                'cart_detail cd', $join, array('cd.id_cart' => $cek->row()->id));

            $data_cart = array();
            foreach($cek_cart->result() as $key){
                $q_product_image = $m->get_data('', 'product_image', null, array('product' => $key->id_product, 'primary' => '1'));
                $prod_img = '';
                if($q_product_image->num_rows() > 0){
                    $prod_img = $q_product_image->row()->image;
                }
                $promo_data = array();
                $inPromo = false;
                $disc_value = null;
                $disc_price = null;
                $old_price = 0;
                if($key->promo == 1){
                    $promo_data = json_decode($key->promo_data,true);
                    $today = date('Y-m-d');
                    $today=date('Y-m-d', strtotime($today));
                    $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                    $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
        
                    if (($today >= $DateBegin) && ($today <= $DateEnd)){
                        $inPromo = true;
                        if($promo_data['type'] == 'P') {
                            $disc_value = $promo_data['discount'];
                            $disc_price = round(($key->price * $promo_data['discount']) / 100);
                        } else {
                            $disc_value = NULL;
                            $disc_price = $promo_data['discount'];
                        }
                    }else{
                        $promo_data = array();
                    }
                }

                $data_cart[count($data_cart)] = array(
                    'id' => $key->id_product,
                    'name' => $key->name,
                    'type' => $key->type,
                    'image' => $prod_img,
                    'code' => $key->code,
                    'category' => $key->category,
                    'description' => $key->description,
                    'qty' => $key->qty,
                    'price' => $key->harga,
                    'price_level' => $key->level_harga == 0 ? false : $key->level_harga,
                    'merchant' => $key->merchant,
                    'merchant_name' => $key->m_name,
                    'merchant_group' => $key->group,
                    'merchant_type' => $key->type,
                    'weight' => $key->weight,
                    'shipping' => $key->shipping,
                    'shipping_merchant' => $key->merchant,
                    'shipping_cost' => 0,
                    'options' => array(),
                    'package_items' => ($key->type == 'p' ? json_decode($key->package_items, true) : array()),
                    'inPromo' => $inPromo,
                    'promo_data' => $promo_data,
                    'disc_value' => $disc_value,
                    'disc_price' => $disc_price,
                    'old_price' => $key->harga
                );
                
            }

            $res = array('success' => 1, 'data_cart' => $data_cart, 'status_cart' => $cek->row()->status);
        }else{
            $res = array('success' => 0);
        }

        $this->response($res, 200);
    }

    public function delete_cart1_post(){
        $m = $this->m;
        $id_prod = $this->post('id_prod');
        $id_cust = $this->post('id_cust');

        $cart = $m->get_data('', 'cart', null, array('id_customer' => $id_cust));
        if($cart->num_rows() > 0) {
            $m->delete_data('cart_detail', array('id_product' => $id_prod, 'id_cart' => $cart->row()->id));
            $m->update_data('cart', array('status' => '0'), array('id_customer' => $id_cust));
            $res = array('success' => 1, 'msg' => 'Hapus Data Cart Berhasil!');
        }else{
            $res = array('success' => 0, 'msg' => 'Data Cart Tidak Ada!');
        }
        $this->response($res, 200);
    }

    public function delete_cart_all_post(){
        $m = $this->m;
        $id_cust = $this->post('id_cust');

        $cart = $m->get_data('', 'cart', null, array('id_customer' => $id_cust));
        if($cart->num_rows() > 0) {
            $m->delete_data('cart_detail', array('id_cart' => $cart->row()->id));
            $m->update_data('cart', array('status' => '0'), array('id_customer' => $id_cust));
            $res = array('success' => 1, 'msg' => 'Hapus Data Cart Berhasil!');
        }else{
            $res = array('success' => 0, 'msg' => 'Data Cart Tidak Ada!');
        }
        $this->response($res, 200);
    }

    public function add_cart_post() {
        $m = $this->m;
        $data = $this->post(null, true);
        $id = $data['prod_id'];
        $id_cust = $data['id_cust'];
        $qty = $data['qty'];
        $guest = $data['guest'];
        // $id = '150';
        // $id_cust = '156';
        // $qty = 1;
        $data_item = array();
        $output = array('status' => 'error', 'message' => 'Qty minimum 1');
        
        if ($qty > 0) {
            $product = $this->product->get_product($id);
            
            if ($product) {
                do {
                    $product_option = 0;
                    if (isset($data['option'])) {
                        $product_option = $this->product->get_product_option_id($product->id, $data['option']);
                        if (!$product_option) {
                            $output['message'] = 'Kesalahan memilih variasi produk.';
                            break;
                        }
//                    $product_option = $this->main->get('product_option', array('id' => $current_product_option));
                        $options = $this->product->get_option_groups($product->id, $product_option);
                        $option = $options->row();
//                    print_r($option);
//                    $product_option = $product_options->row();
                        $image = $this->product->get_product_option_images($product_option);
                        if ($image->num_rows() > 0) { 
                            $product->image = $image->row()->image;
                        }
                    }
                    if ($product->length && $product->height && $product->width) {
                        $weight_volume = ($product->length * $product->height * $product->width) / 6;
                        if ($weight_volume > $product->weight)
                            $product->weight = round($weight_volume);
                    }
                    $old_price = $product->price;
                    $cart_jenis_diskon = '';
                    if($product->promo == 1){
                        $promo_data = json_decode($product->promo_data,true);
                        
                        if($promo_data['type'] == 'P') {
                            $disc_value = $promo_data['discount'];
                            $disc_price = round(($product->price * $promo_data['discount']) / 100);
                        } else {
                            $disc_value = NULL;
                            $disc_price = $promo_data['discount'];
                        }
                        $end_price= $product->price - $disc_price;
                        $today = date('Y-m-d');
                        $today=date('Y-m-d', strtotime($today));
                        $DateBegin = date('Y-m-d', strtotime($promo_data['date_start']));
                        $DateEnd = date('Y-m-d', strtotime($promo_data['date_end']));
            
                        if (($today >= $DateBegin) && ($today <= $DateEnd)){
                            $price = $end_price;
                            $inPromo = true;
                            $cart_jenis_diskon = $promo_data['type'];
                        } else {
                            $price = $product->price;
                            $inPromo = false;
                        }
                    } else {
                        $price = $product->price;
                        $inPromo = false;
                    }

                    $rowid = md5($id . ($product_option ? $product_option : 0));
                    $item = $data_item[$rowid];
                    $location = array('lat' => '-6.186486399999999','lng' =>'106.83409110000002');
                    $merchantx = $this->db->query("SELECT id, `name`, `group`, (6971 * acos(cos(radians($location[lat])) * cos(radians(lat)) * cos(radians(lng) - radians($location[lng])) + sin(radians($location[lat])) * sin(radians(lat)))) AS distance FROM merchants WHERE status = 1 ORDER BY distance LIMIT 5")->result_array();
                    $merchant = $merchantx[0];
                    //$price = $product->price;
                    //jika produk tanaka
                    if ($product->merchant == 0) {
                        $price_level = $this->product->price_in_level($product->id, $merchant['group'], ($item ? $item['qty'] + $qty : $qty));
                        if ($price_level) { //jika price level ada, maka produk tsb masuk ke harga grosir
                            $price = $price_level;
                            $price_level = 1;
                        } else {
                            $this->db->where('product',$product->id);
                            $this->db->where('merchant_group',$merchant['group']);
                            $price_group = $this->db->get('product_price');
                            if ($price_group->num_rows()) {
                                $price_group = $price_group->result()[0];
                                $old_price = $price_group->price;
                                if($product->promo == 1){
                                    if($promo_data['type'] == 'P') {
                                        $disc_price_group = round(($price_group->price * $promo_data['discount']) / 100);
                                    } else {
                                        $disc_price_group = $promo_data['discount'];
                                    }
                                    $end_price_group= $price_group->price - $disc_price_group;
                        
                                    if (($today >= $DateBegin) && ($today <= $DateEnd)){
                                        $price = $end_price_group;
                                    } else {
                                        $price = $price_group->price;
                                    }
                                } else {
                                    $price = $price_group->price;
                                }
                            }
                        }
                    } else {
                        $price_level = 0;
                    }
                    $price += ($product_option) ? $option->price : 0;
                    $weight = $product->weight + ($product_option ? $option->weight : 0);
                    if ($item) {
                        $item['qty'] += $qty;
                        $item['price'] = $price;
                        $item['price_level'] = $price_level;
                        $item['weight'] = $weight * $item['qty'];
                        $cart_price = $price;
                        //$this->cart->update($item);
                    } else {
                        $item = array(
                            'id' => $id,
                            'name' => $product->name,
                            'type' => $product->type,
                            'image' => $product->image,
                            'code' => $product->code,
                            'category' => $product->category,
                            'description' => $product->short_description,
                            'qty' => $qty,
                            'price' => $price,
                            'price_level' => $price_level,
                            'merchant' => $product->merchant,
                            'merchant_name' => $product->merchant_name,
                            'merchant_group' => ($product->merchant) ? 0 : $merchant['group'],
                            'merchant_type' => '',
                            'weight' => $weight * $qty,
                            'shipping' => '',
                            'shipping_merchant' => $product->merchant,
                            'shipping_cost' => 0,
                            'options' => ($product_option) ? $options->result_array() : array()
                        );
                        
                        if($product->type == 'p'){
                            $item['package_items'] = json_decode($product->package_items, true);
                        } else {
                            $item['package_items'] = array();
                        }
                        if($inPromo){
                            $item['inPromo'] = true;
                            $item['promo_data'] = $promo_data;
                            $item['disc_value'] = $disc_value;
                            $item['disc_price'] = $disc_price;
                            $item['old_price'] = $old_price;
                            $cart_price = $old_price;
                            $cart_diskon = $cart_price - $price;
                        } else {
                            $item['inPromo'] = false;
                            $item['promo_data'] = array();
                            $item['disc_value'] = NULL;
                            $item['disc_price'] = NULL;
                            $item['old_price'] = $price;
                            $cart_price = $price;
                            $cart_diskon = 0;
                        }
                        //$this->cart->insert($item);
                    }
                    $output['message'] = 'Berhasil menambahkan produk';
                    $output['status'] = 'success';

                    if($id_cust){
                        $cek_cart = $m->get_data('', 'cart', null, array('id_customer' => $id_cust));
                    
                        if($cek_cart->num_rows() > 0){
                            $cek_cart_detail = $m->get_data('', 'cart_detail', null, array('id_cart' => $cek_cart->row()->id, 'id_product' => $id));
                            if($cek_cart_detail->num_rows() > 0){
                                $output['message'] = 'Produk Sudah Ada';
                                $output['status'] = 'error';
                            }else{
                                $data_edit = array(
                                    'tgl_edit' => date('Y/m/d H:i:s'),
                                    'grand_total' => ($price * $qty) + $cek_cart->row()->grand_total,
                                    'status' => '0'
                                );

                                $m->update_data('cart', $data_edit, array('id' => $cek_cart->row()->id));

                                $data_add1 = array(
                                    'id_cart' => $cek_cart->row()->id,
                                    'id_product' => $id,
                                    'qty' => $qty,
                                    'harga' => $cart_price,
                                    'level_harga' => $price_level ? $price_level : 0,
                                    'diskon' => $cart_diskon,
                                    'jenis_diskon' => $cart_jenis_diskon,
                                    'subtotal' => $price * $qty
                                );
        
                                $m->insert_data('cart_detail', $data_add1);
                            }
                        }else{
                            $data_add = array(
                                'id_customer' => $id_cust,
                                'id_address' => 0,
                                'status' => 0,
                                'tgl_add' => date('Y/m/d H:i:s'),
                                'tgl_edit' => date('Y/m/d H:i:s'),
                                'grand_total' => ($price * $qty),
                                'id_guest' => 0
                            );

                            $m->insert_data('cart', $data_add);

                            $id_cart = $m->get_data('id', 'cart', null, array('id_customer' => $id_cust))->row()->id;

                            $data_add1 = array(
                                'id_cart' => $id_cart,
                                'id_product' => $id,
                                'qty' => $qty,
                                'harga' => $cart_price,
                                'level_harga' => $price_level ? $price_level : 0,
                                'diskon' => $cart_diskon,
                                'jenis_diskon' => $cart_jenis_diskon,
                                'subtotal' => $price * $qty
                            );

                            $m->insert_data('cart_detail', $data_add1);
                        }
                    }else if($guest == '0'){
                        $id_guest = $m->get_data('id_guest', 'cart', null, null, '', 'id_guest desc', 0, 1);
                        if($id_guest->num_rows() > 0){
                            if($id_guest->row()->id_guest == '' || $id_guest->row()->id_guest == null){
                                $id_guest = 1;
                            }else{
                                $id_guest = $id_guest->row()->id_guest + 1;
                            }
                        }else{
                            $id_guest = 1;
                        }
                        $data_add = array(
                            'id_customer' => 0,
                            'id_address' => 0,
                            'status' => 0,
                            'tgl_add' => date('Y/m/d H:i:s'),
                            'tgl_edit' => date('Y/m/d H:i:s'),
                            'grand_total' => ($price * $qty),
                            'id_guest' => $id_guest
                        );
                        $m->insert_data('cart', $data_add);

                        $id_cart = $m->get_data('id', 'cart', null, array('id_guest' => $id_guest))->row()->id;

                        $data_add1 = array(
                            'id_cart' => $id_cart,
                            'id_product' => $id,
                            'qty' => $qty,
                            'harga' => $cart_price,
                            'level_harga' => $price_level ? $price_level : 0,
                            'diskon' => $cart_diskon,
                            'jenis_diskon' => $cart_jenis_diskon,
                            'subtotal' => $price * $qty
                        );

                        $m->insert_data('cart_detail', $data_add1);

                        $output['id_guest'] = $id_guest;
                    }else{
                        $output['id_guest'] = $guest;
                        $cek_cart = $m->get_data('', 'cart', null, array('id_guest' => $guest));
                    
                        if($cek_cart->num_rows() > 0){
                            $cek_cart_detail = $m->get_data('', 'cart_detail', null, array('id_cart' => $cek_cart->row()->id, 'id_product' => $id));
                            if($cek_cart_detail->num_rows() > 0){
                                $output['message'] = 'Produk Sudah Ada';
                                $output['status'] = 'error';
                            }else{
                                $data_edit = array(
                                    'tgl_edit' => date('Y/m/d H:i:s'),
                                    'grand_total' => ($price * $qty) + $cek_cart->row()->grand_total,
                                    'status' => '0'
                                );

                                $m->update_data('cart', $data_edit, array('id' => $cek_cart->row()->id));

                                $data_add1 = array(
                                    'id_cart' => $cek_cart->row()->id,
                                    'id_product' => $id,
                                    'qty' => $qty,
                                    'harga' => $cart_price,
                                    'level_harga' => $price_level ? $price_level : 0,
                                    'diskon' => $cart_diskon,
                                    'jenis_diskon' => $cart_jenis_diskon,
                                    'subtotal' => $price * $qty
                                );
        
                                $m->insert_data('cart_detail', $data_add1);
                            }
                        }else{
                            $data_add = array(
                                'id_customer' => 0,
                                'id_address' => 0,
                                'status' => 0,
                                'tgl_add' => date('Y/m/d H:i:s'),
                                'tgl_edit' => date('Y/m/d H:i:s'),
                                'grand_total' => ($price * $qty),
                                'id_guest' => $guest
                            );

                            $m->insert_data('cart', $data_add);

                            $id_cart = $m->get_data('id', 'cart', null, array('id_customer' => $id_cust))->row()->id;

                            $data_add1 = array(
                                'id_cart' => $id_cart,
                                'id_product' => $id,
                                'qty' => $qty,
                                'harga' => $cart_price,
                                'level_harga' => $price_level ? $price_level : 0,
                                'diskon' => $cart_diskon,
                                'jenis_diskon' => $cart_jenis_diskon,
                                'subtotal' => $price * $qty
                            );

                            $m->insert_data('cart_detail', $data_add1);
                        }
                    }
                    
                    $output['data'] = $item;
                    
                    
                } while (0);
            }
        }
        $this->response($output, 200);
    }

    public function cek_cart_detail_post(){
        $m = $this->m;
        $join = array(0 => 'cart_detail cd-cd.id_cart=c.id');
        $cek = $m->get_data('', 'cart_detail', null, array('id_cart' => 5));
        print_r($cek->result());
    }

    public function cek_cart_post(){
        $cek = $this->m->get_data('', 'cart');
        print_r($cek->result());
    }

    public function delete_cart_post(){
        $m = $this->m;
        $m->delete_data('cart', array('id !=' => 0));
        $m->delete_data('cart_detail', array('id !=' => 0));
    }

    public function check_stock_post() {
        $data = $this->post(null, true);
        $m = $this->m;
        $cart_items = json_decode($data['item'],true);
        $id_cust = $data['id_cust'];
        $guest = $data['guest'];
        $this->load->model('Checkout_model', 'checkout');

        if($guest != 0){
            $cek = $m->get_data('', 'cart', null, array('id_customer' => $id_cust));
            if($cek->num_rows() > 0){
                $m->delete_data('cart_detail', array('id_cart' => $cek->row()->id));
            }
            $m->delete_data('cart', array('id_customer' => $id_cust));

            $m->update_data('cart', array('id_customer' => $id_cust, 'id_guest' => 0), array('id_guest' => $guest));
        }
        if (count($cart_items) == 0){
            $cek = $m->get_data('', 'cart', null, array('id_customer' => $id_cust));
            if($cek->num_rows() > 0){
                $m->delete_data('cart_detail', array('id_cart' => $cek->row()->id));
            }
            $return = array('message' => 'Tidak ada barang di dalam keranjang! ', 'status' => 'failed');
        } else {
            $not_avail = array();
            $merchant_stock = array();
            $count_valid = 0;
            $total_avail_ = array();
            $data_debug = array();
            foreach ($cart_items as $item) {
                if($item['type'] == 'p') {
                    $package_items = $item['package_items'];
                    $count_packages = count($package_items);
                    foreach ($package_items as $package) {
                        $merchants = $this->checkout->get_merchants($package['product_id'], $item['qty'] * $package['qty'], $item['price_level']);
                        if ($merchants->num_rows() > 0) {
                            foreach ($merchants->result() as $merchantx) {
                                //var_dump($merchant);
                                $merchant_stock[$merchantx->id][] = array('prod_id' => $package['product_id']);
                            }
                        }  
                    }
                    //exit();
                    if (count($merchant_stock) > 0) {
                        foreach ($merchant_stock as $mcn) {
                            if(count($mcn) == $count_packages) {
                                $count_valid++;
                            }
                        }
                    } 
                    if ($count_valid > 0){
                        $cart_detail = $m->get_data('cd.*, c.grand_total', 'cart_detail cd', array(0 => 'cart c-c.id=cd.id_cart'), array('c.id_customer' => $id_cust, 'cd.id_product' => $item['id']))->row();
                        $harga = $cart_detail->harga;
                        $qty = $cart_detail->qty;
                        $grand_total = $cek_cart_detail->grand_total;

                        $grand_total -= ($qty * $harga);
                        $grand_total += ($harga * $item['qty']);

                        $m->update_data('cart', array('grand_total' => $grand_total), array('id' => $cart_detail->id_cart));
                        $m->update_data('cart_detail', array('qty' => $item['qty'], 'subtotal' => $harga * $item['qty']), array('id' => $cart_detail->id));
                    } else {
                        $merchant_stock = array();
                        //$list_merchant = $this->session->userdata('list_merchant')[0];
                        $list_merchant = array("id"=> "67","name"=> "Monic Shop","group"=> "3","distance"=> "4.054714708788247"); 
                        $merchant = $this->checkout->get_branch($list_merchant['group']);
                        foreach ($package_items as $package) {
                            $stock = $this->checkout->check_branch_stock($merchant ? $merchant->id : 0,$package['product_id'], $item['qty'] * $package['qty'], $item['price_level']); 
                            if ($stock->num_rows() > 0) {
                                foreach ($stock->result() as $merchantx) {
                                    $merchant_stock[$merchantx->id][] = array('prod_id' => $package['product_id']);
                                }
                            }  
                        }
                        if (count($merchant_stock) > 0) {
                            //foreach ($merchant_stock as $mcn) {
                                if(count($merchant_stock) == $count_packages) {
                                    $count_valid++;
                                }
                            //}
                        }
                        if ($count_valid == 0){
                            $not_avail[] = $item;
                        }else{
                            $cart_detail = $m->get_data('cd.*, c.grand_total', 'cart_detail cd', array(0 => 'cart c-c.id=cd.id_cart'), array('c.id_customer' => $id_cust, 'cd.id_product' => $item['id']))->row();
                            $harga = $cart_detail->harga;
                            $qty = $cart_detail->qty;
                            $grand_total = $cek_cart_detail->grand_total;

                            $grand_total -= ($qty * $harga);
                            $grand_total += ($harga * $item['qty']);

                            $m->update_data('cart', array('grand_total' => $grand_total), array('id' => $cart_detail->id_cart));
                            $m->update_data('cart_detail', array('qty' => $item['qty'], 'subtotal' => $harga * $item['qty']), array('id' => $cart_detail->id));
                        }
                    }
                } else {
                    //$merchants = $this->checkout->get_merchants($item['id'], $item['qty'], $item['price_level']);
                    $merchants = $this->checkout->get_merchants($item['id'], $item['id'] == '0' ? 1000000 : $item['qty'], $item['price_level']);
                    $data_debug[] = array(
                        'id' => $item['id'],
                        'qty' => $item['qty'],
                        'price' => $item['price_level']
                    );
                    if ($merchants->num_rows() > 0) {
                        $cart_detail = $m->get_data('cd.*, c.grand_total', 'cart_detail cd', array(0 => 'cart c-c.id=cd.id_cart'), array('c.id_customer' => $id_cust, 'cd.id_product' => $item['id']))->row();
                        $harga = $cart_detail->harga;
                        $qty = $cart_detail->qty;
                        $grand_total = $cek_cart_detail->grand_total;

                        $grand_total -= ($qty * $harga);
                        $grand_total += ($harga * $item['qty']);

                        $m->update_data('cart', array('grand_total' => $grand_total), array('id' => $cart_detail->id_cart));
                        $m->update_data('cart_detail', array('qty' => $item['qty'], 'subtotal' => $harga * $item['qty']), array('id' => $cart_detail->id));

                        //$return = array('message' => 'Stok barang tersedia! ', 'status' => 'success'); 
                    } else { 
                        //$list_merchant = $this->session->userdata('list_merchant')[0];
                        $list_merchant = array("id"=> "67","name"=> "Monic Shop","group"=> "3","distance"=> "4.054714708788247"); 
                        $merchant = $this->checkout->get_branch($list_merchant['group']);
                        $stock = $this->checkout->check_branch_stock($merchant ? $merchant->id : 0,$item['id'], $item['qty'], $item['price_level']);
                        
                        if($stock->num_rows() == 0){
                            $not_avail[] = $item;
                        }
                    }
                }
            }   

            if(count($not_avail) > 0) {
                $return = array('message' => count($not_avail).' barang yang ingin Anda beli tidak tersedia! ', 'status' => 'failed','data' => $not_avail, 'data_debug' => $data_debug);
            } else {
                $return = array('message' => 'Stok barang tersedia! ', 'status' => 'success', 'data_debug' => $data_debug);
            }
        }
        $this->response($return, 200);
        
    }
}
