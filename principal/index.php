<?php

date_default_timezone_set('Asia/Jakarta');

$system_path = '../protected/system';
$application_folder = '../protected/principle/base';
$view_folder = '../protected/principle/views';

require_once '../protected/db.php';
require_once '../protected/system.php';
