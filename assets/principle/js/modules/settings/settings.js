$(function () {
    $('#province').on('change', function () {
        $('#district').html('<option value="">Pilih Kecamatan</option>');
        if ($(this).val() == "") {
            $('#city').html('<option value="">Pilih Kota</option>');
        } else {
            $.ajax({
                type: "POST",
                url: site_url + 'settings/get_cities',
                data: {id: $(this).val()},
                success: function (data) {
                    $('#city').html(data);
                }
            });
        }
    });
    $('#city').on('change', function () {
        if ($(this).val() == "") {
            $('#district').html('<option value="">Pilih Kecamatan</option>');
        } else {
            $.ajax({
                type: "POST",
                url: site_url + 'settings/get_districts',
                data: {id: $(this).val()},
                success: function (data) {
                    $('#district').html(data);
                }
            });
        }
    });
});