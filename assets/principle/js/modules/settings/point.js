let has_parent = new Array();
let kategori_parent = new Array();

let has_kategori = new Array();
let kategorinya = new Array();
let has_sub_kategori = new Array();
let sub_kategorinya = new Array();
$(function () {


});
function filter() {
	let val0 = $('#val0').val();
	let val1 = $('#val1').val();
	let val2 = $('#val2').val();
	let val3 = $('#val3').val();

	datatable_data.kategori = val0;
	datatable_data.sub_kategori = val1;
	datatable_data.item = val2;
	datatable_data.merchant = val3;
    // $('#filter-category').html($(e).text() + ' <span class="caret"></span>');
    // var category = $(e).data('category');

    
    var table = $('#table').DataTable();

//    table.data(datatable_data).draw();
//    table.ajax.data = datatable_data;
    table.ajax.reload();
//    var id = $(e).data('id');
//    var value = $(e).val();
//    $.ajax({
//        type: 'POST',
//        url: current_url + "/update_sort_order",
//        data: {id: id, value: value},
//        success: function (data) {
//            if (data.status == 'error') {
//                error_message(data.message);
//            }
//        }
//    });
}

function set_merchant(e, merchant) {
	$('#filter-merchant').html($(e).text() + ' <span class="caret"></span>');

	$('#val3').val(merchant);
	$('#form-merchant').html($(e).text() + ' <span class="caret"></span>');

	$('#form-val3').val(merchant);

	filter();
}

function reset_global() {
	$('#filter-category').html('Default Semua Item');
	$('#form-category').html('Default Semua Item');
	$('#filter-category-global').remove();
	$('#form-category-global').remove();
	$('#filter-category0').html('Semua Kategori');
	$('#form-category0').html('Semua Kategori');
	$('#filter-category1').html('Semua Sub Kategori');
	$('#form-category1').html('Semua Sub Kategori');
	$('#filter-category2').html('Semua Item');
	$('#form-category2').html('Semua Item');
	for(let a=0; a<3; a++){
		$('#search'+a).val('');
		$('#val'+a).val('');
		$('.li_baru'+a).remove();
		$('#form-search'+a).val('');
		$('#form-val'+a).val('');
		$('.form-li_baru'+a).remove();
	}
	filter();
}

function set_filter_item(e, tipe, id, param) {
	if(tipe == '1'){
		var tipe1 = 'Per Kategori';
		$('#filter-category1').html('Semua Sub Kategori <span class="caret"></span>');
		$('#val1').val('');
		$('#search1').val('');
		$('.li_baru1').remove();
		$('#form-category1').html('Semua Sub Kategori <span class="caret"></span>');
		$('#form-val1').val('');
		$('#form-search1').val('');
		$('.form-li_baru1').remove();
	}else if(tipe == '2'){
		if(id != ''){
			if(has_parent[id] == 1){
				$('#filter-category0').html(kategori_parent[id].name+' <span class="caret"></span>');
				$('#val0').val(kategori_parent[id].id);
				$('#form-category0').html(kategori_parent[id].name+' <span class="caret"></span>');
				$('#form-val0').val(kategori_parent[id].id);
			}else{
				$('#filter-category0').html('Semua Kategori <span class="caret"></span>');
				$('#val0').val('');
				$('#form-category0').html('Semua Kategori <span class="caret"></span>');
				$('#form-val0').val('');
			}
			
			$('#search0').val('');
			$('.li_baru0').remove();
			$('#form-search0').val('');
			$('.form-li_baru0').remove();
		}
		var tipe1 = 'Per Sub Kategori';
		$('#filter-category2').html('Semua Item <span class="caret"></span>');
		$('#val2').val('');
		$('#search2').val('');
		$('.li_baru2').remove();
		$('#form-category2').html('Semua Item <span class="caret"></span>');
		$('#form-val2').val('');
		$('#form-search2').val('');
		$('.form-li_baru2').remove();
	}else{
		if(id != ''){
			if(has_sub_kategori[id] == 1){
				$('#filter-category1').html(sub_kategorinya[id].name+' <span class="caret"></span>');
				$('#val1').val(sub_kategorinya[id].id);
				$('#form-category1').html(sub_kategorinya[id].name+' <span class="caret"></span>');
				$('#form-val1').val(sub_kategorinya[id].id);
			}
			if(has_kategori[id] == 1){
				$('#filter-category0').html(kategorinya[id].name+' <span class="caret"></span>');
				$('#val0').val(kategorinya[id].id);
				$('#form-category0').html(kategorinya[id].name+' <span class="caret"></span>');
				$('#form-val0').val(kategorinya[id].id);
			}
		}
		var tipe1 = 'Per Item';
	}
	$('#filter-category'+param).html($(e).text() + ' <span class="caret"></span>');
	$('#filter-category').html(tipe1 + ' <span class="caret"></span>');
	$('#val'+param).val(id);
	$('#filter-category').after('\n\
		<ul id="filter-category-global" class="dropdown-menu dropdown-menu-right">\n\
            <li><a href="javascript:void(0);" onclick="reset_global();" data-category="g">Default Semua Item</a></li>\n\
        </ul>');

	$('#form-category'+param).html($(e).text() + ' <span class="caret"></span>');
	$('#form-category').html(tipe1 + ' <span class="caret"></span>');
	$('#form-val'+param).val(id);
	$('#form-category').after('\n\
		<ul id="form-category-global" class="dropdown-menu dropdown-menu-right">\n\
            <li><a href="javascript:void(0);" onclick="reset_global();" data-category="g">Default Semua Item</a></li>\n\
        </ul>');

	filter();
}

function change_kategori(val) {
	$('.li_baru0').remove();
	$('.form-li_baru0').remove();
	$.ajax({
       type: 'POST',
       url: current_url + "/filter_kategori",
       data: {search: val},
       dataType: "json",
       success: function (data) {
           if (data.success == '1') {
           		for(let a=0; a<data.data.length; a++){
           			$('#item_choice0').after('<li class="li_baru0"><a href="javascript:void(0);" onclick="set_filter_item(this, '+data.data[a].tipe+', '+data.data[a].id+', 0);">'+data.data[a].name+'</a></li>');
           			$('#form-item_choice0').after('<li class="form-li_baru0"><a href="javascript:void(0);" onclick="set_filter_item(this, '+data.data[a].tipe+', '+data.data[a].id+', 0);">'+data.data[a].name+'</a></li>');
           		}
               
           }
       }
   	});
}

function change_sub_kategori(val) {
	$('.li_baru1').remove();
	$('.form-li_baru1').remove();
	let parent = $('#val0').val();
	$.ajax({
       type: 'POST',
       url: current_url + "/filter_sub_kategori",
       data: {search: val, parent: parent},
       dataType: "json",
       success: function (data) {
           if (data.success == '1') {
           		for(let a=0; a<data.data.length; a++){
           			has_parent[data.data[a].id] = data.data[a].has_parent;
           			kategori_parent[data.data[a].id] = data.data[a].parent;
           			$('#item_choice1').after('<li class="li_baru1"><a href="javascript:void(0);" onclick="set_filter_item(this, '+data.data[a].tipe+', '+data.data[a].id+', 1);">'+data.data[a].name+'</a></li>');
           			$('#form-item_choice1').after('<li class="form-li_baru1"><a href="javascript:void(0);" onclick="set_filter_item(this, '+data.data[a].tipe+', '+data.data[a].id+', 1);">'+data.data[a].name+'</a></li>');
           		}
               
           }
       }
   	});
}

function change_item(val) {
	$('.li_baru2').remove();
	$('.form-li_baru2').remove();
	let kategori = $('#val0').val();
	let sub_kategori = $('#val1').val();
	$.ajax({
       type: 'POST',
       url: current_url + "/filter_item",
       data: {search: val, kategori: kategori, sub_kategori: sub_kategori},
       dataType: "json",
       success: function (data) {
           if (data.success == '1') {
           		for(let a=0; a<data.data.length; a++){
           			has_kategori[data.data[a].id] = data.data[a].has_category;
           			kategorinya[data.data[a].id] = data.data[a].kategorinya;
           			has_sub_kategori[data.data[a].id] = data.data[a].has_sub_category;
           			sub_kategorinya[data.data[a].id] = data.data[a].sub_kategorinya;
           			$('#item_choice2').after('<li class="li_baru2"><a href="javascript:void(0);" onclick="set_filter_item(this, '+data.data[a].tipe+', '+data.data[a].id+', 2);">'+data.data[a].name+'</a></li>');
           			$('#form-item_choice2').after('<li class="form-li_baru2"><a href="javascript:void(0);" onclick="set_filter_item(this, '+data.data[a].tipe+', '+data.data[a].id+', 2);">'+data.data[a].name+'</a></li>');
           		}
               
           }
       }
   });
}

function add_point() {
	$('#btn_add').hide();
	$('#form_setting_point').show();
	$('.form_edit').hide();
	$('.form_add').show();
}

function cancel() {
	$('#btn_add').show();
	$('#form_setting_point').hide();
}

function save() {
	let point = $('#point').val();

	if(point != ''){
		let val0 = $('#val0').val();
		let val1 = $('#val1').val();
		let val2 = $('#val2').val();
		let val3 = $('#val3').val();

		datatable_data.kategori = val0;
		datatable_data.sub_kategori = val1;
		datatable_data.item = val2;
		datatable_data.merchant = val3;

		$.ajax({
			type: 'POST',
			url: current_url + "/save",
			data: {
				kategori: val0,
				sub_kategori: val1,
				item: val2,
				merchant: val3,
				point: point
			},
			dataType: "json",
			success: function (data) {
				if (data.success == '1') {
					var table = $('#table').DataTable();
				    table.ajax.reload();
					swal({
				    	title: "Great !",
				       	text: 'Point Berhasil Diinput',
				       	type: 'success'
				   	}).then((result) => {
						
					});
				}else if(data.success == '0'){
					var table = $('#table').DataTable();
				    table.ajax.reload();
					swal({
				    	title: "Error !",
				       	text: 'Data Sudah Ada',
				       	type: 'error'
				   	}).then((result) => {
						
					});
				}
			}
		});
	}else{
		swal({
	    	title: "Ooops!",
	       	text: 'Field "Point" masih kosong',
	       	type: 'error'
	   	});
	}
	
}

function edit_form(cnt) {
	$('#btn_add').hide();
	$('#form_setting_point').show();
	$('.form_edit').show();
	$('.form_add').hide();

	$('#id_setting_poin').val($('#id_setting_poin'+cnt).val());
	$('#point_form').val($('#id_general'+cnt).val());
	$('#name_form').val($('#name'+cnt).val());
	$('#type_form').val($('#tipe_item'+cnt).val());
	$('#merchant_form').val($('#tipe_merchant'+cnt).val());
	$('#point').val($('#point'+cnt).val());
}

function edit() {
	let id_setting_poin = $('#id_setting_poin').val();
	let point = $('#point').val();

	$.ajax({
		type: 'POST',
		url: current_url + "/save_edit",
		data: {
			id_setting_poin: id_setting_poin,
			point: point
		},
		dataType: "json",
		success: function (data) {
			if (data.success == '1') {
				var table = $('#table').DataTable();
			    table.ajax.reload();

			    $('#btn_add').show();
				$('#form_setting_point').hide();
				$('.form_edit').hide();
				$('.form_add').show();

				swal({
			    	title: "Great !",
			       	text: 'Point Berhasil Diupdate',
			       	type: 'success'
			   	}).then((result) => {
					
				});
			}
		}
	});
}