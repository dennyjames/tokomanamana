$(function () {
    var table = $('#table').DataTable();
    table.on('click', '.paid', function (e) {
        var url = $(this).attr('href');
        e.preventDefault();
        swal({
            title: lang.message.confirm.paid,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: lang.button.yes,
            cancelButtonText: lang.button.cancel,
            closeOnConfirm: false
        }, function () {
            $.ajax({
                url: url,
                success: function (data) {
                    data = JSON.parse(data);
                    table.ajax.reload();
                    swal(data.message, '', data.status);
                }
            });
        });
    })
})