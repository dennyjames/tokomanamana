function delete_etalase(id) {
	table = $('#table').DataTable();
	swal({
		title: 'Yakin ingin dihapus?',
		type: 'info',
		showCancelButton: true,
		confirmButtonText: 'Ya',
		cancelButtonText: 'Batal',
		confirmButtonColor: 'red'
	},function(isConfirm){
		if(isConfirm) {
			$.ajax({
				type: 'post',
				url: current_url + '/delete',
				data: {
					id: id
				},
				dataType: 'JSON',
				success: function (data) { 
					swal({
		                title: data.message,
		                type: data.status,
		                timer: 1000,
		                showConfirmButton: false
		            });
		            table.draw();
				}
			});
		} else {

		}
	});
}