$(function () {
//    $('.sort-order').on('change', function (e) {
//        
//    })
});
function update_sort_order(e) {
    var id = $(e).data('id');
    var value = $(e).val();
    $.ajax({
        type: 'POST',
        url: current_url + "/update_sort_order",
        data: {id: id, value: value},
        success: function (data) {
            if (data.status == 'error') {
                error_message(data.message);
            }
        }
    });
}