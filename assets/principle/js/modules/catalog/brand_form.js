$(function () {
    $('body').on('click', '#edit-image-btn', function () {
        $('#image-preview').remove();
    });
    $('body').on('click', '#delete-image-btn', function () {
        $('#image').val('');
        $('#image-preview').remove();
        $('#add-image-btn').show();
        $('#edit-image-btn').hide();
        $('#delete-image-btn').hide();
    });
});
function responsive_filemanager_callback(field_id) {
    if (field_id) {
        var url = $('#' + field_id).val();
        var html = '<div class="col-md-3" id="image-preview">';
        html += '<div class="thumbnail"><div class="thumb">';
        html += '<img src="' + site_url + '../files/images/' + url + '">';
        html += '</div></div></div>';
        $(html).insertBefore('#add-image');
        $('#add-image-btn').hide();
        $('#edit-image-btn').show();
        $('#delete-image-btn').show();
    }
}