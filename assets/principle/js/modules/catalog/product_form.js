$(function () {
    var prod_type = $('#type').val();
    var promo = $('#promo-slc').val();
    let option = $('#option-slc').val();
    var preorder = $('#preorder').val();
    let grosir = $('#grosir-slc').val();
    let reseller = $('#reseller-slc').val();
    var preorder_time = $('#preorder-time-id').val();
    let count_checkbox_colour = 0
    let count_checkbox_size = 0
    let colour_status = 0;
    let size_status = 0;
    let colour_size_on = 0;

    // variation panels
    $('.variation-body-colour').addClass('inactive');
    $('.variation-body-size').addClass('inactive');

    $('.variation-swich-colour').on('switchChange.bootstrapSwitch', function (e) {
        if(e.target.checked){
             $('.variation-body-colour').removeClass('inactive'); 
             $('.checkbox-colour').prop('disabled', false);
             $('.checkbox-colour').css('cursor','pointer');
             colour_status = 1;
             $('#option-generate').prop('disabled',false); 
             if(size_status === 1){
                colour_size_on = 1;
             }

        }
        else{
            $('.variation-body-colour').addClass('inactive'); 
            // $('.checkbox-colour').prop('checked', false);
            $('.checkbox-colour').prop('disabled', true);
            $('.checkbox-colour').css('cursor','not-allowed');
            colour_status = 0;
            colour_size_on = 0;
            $('#option-generate').prop('disabled',true); 
            $('.checkbox-colour').prop('checked', false);
        }
       
    });

    $('.variation-swich-size').on('switchChange.bootstrapSwitch', function (e) {
        if(e.target.checked){
             $('.variation-body-size').removeClass('inactive'); 
             $('.checkbox-size').prop('disabled', false);
             $('.checkbox-size').css('cursor','pointer');
             size_status = 1;
             $('#option-generate').prop('disabled',false);
             if(colour_status === 1){
                colour_size_on = 1;
             }

        }
        else{
            $('.variation-body-size').addClass('inactive'); 
            $('.checkbox-size').prop('disabled', true);
            $('.checkbox-size').css('cursor','not-allowed');
            size_status = 0;
            colour_size_on = 0;
            $('#option-generate').prop('disabled',true); 
            $('.checkbox-size').prop('checked', false);
        }
       
    });

    $('.checkbox-colour-count').on('change',function(){
        count_checkbox_colour = $(".checkbox-colour-count input:checkbox:checked").length;
        if(colour_status === 1){
            if(count_checkbox_colour > 0){
               $('#option-generate').prop('disabled',false); 
            }
        }
      

    })
    $('.checkbox-size-count').on('change',function(){
        count_checkbox_size = $(".checkbox-size-count input:checkbox:checked").length;
        if(size_status === 1){
            if(count_checkbox_size > 0){
               $('#option-generate').prop('disabled',false); 
            }
        }
    
    })

    if(prod_type == 's') {
        $("#package-tab").hide();
    } else {
        $("#package-tab").show();
    }
    if(promo == '0') {
        $("#promo-tab").hide();
    } else {
        $("#promo-tab").show();
    }
    if(option == '0') {
        $("#option-tab").hide();
    } else {
        $("#option-tab").show();
        $('#form-group-price').hide();
        $('.form-group-stock').hide();
    }
    if(brand == 'other'){
        var input = '<input type="text" name="brand_other" id="brand_other"  class="form-control" value="">';
        $('#brand_other_div').html(input);
    }
    if(reseller == '0'){
        $('#price-reseller-list').hide();
        $('.td-reseller-qty').prop('required',false);
        $('.td-reseller-qty').removeAttr('min');
        $('.number-reseller').prop('required',false);
    } else{
       $('#price-reseller-list').show();
    }
    if(grosir == '0'){
        $('#price-list').hide();
        $('.td-grosir-qty').prop('required',false);
        $('.td-grosir-qty').removeAttr('min');
        $('.number-grosir').prop('required',false);
    } else{
       $('#price-list').show();
    }
    if(reseller == '0'){
        $('#price-reseller-list').hide();
        $('.td-reseller-qty').prop('required',false);
        $('.td-reseller-qty').removeAttr('min');
        $('.number-reseller').prop('required',false);
    } else{
       $('#price-reseller-list').show();
    }
    if((grosir === '1' && option === '1') || (reseller === '1' && option == '1')){
    	$('#form-group-price').show();
        $('.variant_price').prop({ readOnly: true });
        $('#price-input').on('change',function(){
        	$('.variant_price').val($('#price-input').val());
        });
    } else if((grosir === '0' && option === '1') || (reseller === '0' && option === '1')){
        $('#form-group-price').hide();
    }

    if(preorder == '0') {
        $("#preorder-time").hide();
        $("#coming-soon-time").hide();
    } else if(preorder == '1') {
        $("#preorder-time").show();
        $("#label-time").html('Waktu Pre-Order');
        $("#coming-soon-time").hide();
    } else {
        $("#preorder-time").hide();
        $("#coming-soon-time").show();
        $("#label-time").html('Waktu Coming Soon');
    }
    $('#percent_tag').hide();
    
    var input_type_promo = $('#input_type_promo').children(":selected").attr("id");
    if(input_type_promo == 'percentage'){
        $("#discount_addon").hide();
        $("#discount_addon_percent").show();
        $('#discount').prop('type', 'number');
        $("#discount").prop('min',0);
        $("#discount").prop('max',100);
    }
    else{
         $('#discount_addon_percent').hide();
    }
    $('#input_type_promo').change(function(){
        if(this.options[this.selectedIndex].id == 'percentage'){
            $("#discount_addon").hide();
             $("#discount_addon_percent").show();
             $('#discount').val(0);
             $('#discount').prop('type', 'number');
             $("#discount").prop('min',0);
             $("#discount").prop('max',100);
        }
        else{
            $('#discount').val(0);
            $('#discount_addon').show();
            $("#discount_addon_percent").hide();
            $('#discount').prop('type', 'text');
            $("#discount").prop('max',false);
            $("#discount").prop('min',false);

        }
    });

    //Datepicker
    $( "#coming-soon-time-id" ).datepicker();

    $('.page-header .page-title').css({width: $('.page-header .page-title').width() - $('.page-header .heading-elements').width() - 50});
    $('#category').on('change', function () {
        $.ajax({
            type: "POST",
            url: site_url + 'catalog/products/get_feature_form',
            data: {category: $(this).val(), product: $('#id').val()},
            success: function (data) {
                $('#feature-form').html(data);
            }
        });
    });
    $('#type').on('change', function() {
        if ( this.value == 's'){
            $("#package-tab").hide();
        } else {
            $("#package-tab").show();
        } 
    });
    $('#promo-slc').on('change', function() {
        if ( this.value == '0'){
            $("#promo-tab").hide();
        } else {
            $("#promo-tab").show();
        } 
    });
    $('#a_option').on('click', function() {
        $('#a_option > .circle').remove();  
    });
    $('#preorder').on('change', function() {
        if ( this.value == '0'){
            $("#preorder-time").hide();
            $("#coming-soon-time").hide();
        } else if(this.value == '1'){
            $("#preorder-time").show();
            $("#coming-soon-time").hide();
            $("#label-time").html('Waktu Pre-Order');
            $("#preorder-time-id").prop('min',3);
            if(preorder_time == 0){
                $("#preorder-time-id").prop("value", 3);
            }
        } else {
            $("#preorder-time").hide();
            $("#coming-soon-time").show();
            $("#label-time").html('Waktu Coming Soon');
        }
    });
    $('#option-slc').on('change', function() {
        if ( this.value == '0'){
            option = this.value;
            $("#option-tab").hide();
            $('#form-group-price').show();
            $('.form-group-stock').show();
     
        } else {
            option = this.value;
            $("#option-tab").show();
            $('#a_option > .circle').remove();
            $('<div class="circle"></div>').appendTo('#a_option');
            $('#form-group-price').hide();
            $('.form-group-stock').hide();
            $('#stock_normal').val(0);
            if(grosir === '0' && reseller === '0'){
                $('.variant_price').prop({ readOnly: false });
            }
            else if((grosir === '1' && reseller === '1') || (grosir === '0' && reseller === '1') || (grosir === '1' && reseller === '0')){
                $('#form-group-price').show();
                $('.variant_price').prop({ readOnly: true });
            }
           
        } 
    });
    $('#grosir-slc').on('change', function() {
        if ( this.value == '0'){
            grosir = this.value;
            $('#price-list').hide();
            $('.td-grosir-qty').prop('required',false);
            $('.td-grosir-qty').removeAttr('min');
            $('.number-grosir').prop('required',false);
            
            if(option === '1' && reseller === '0'){
                $('#form-group-price').hide();
                $('.variant_price').prop({ readOnly: false });
            }

   
        } else {
            grosir = this.value;
            $('.td-grosir-qty').prop('required',true);
            $('.td-grosir-qty').prop('min',2);
            $('.number-grosir').prop('required',true);
         
            if(option === '1'){
                $('.variant_price').prop({ readOnly: true });
                $('#price-input').on('change',function(){
                    $('.variant_price').val($('#price-input').val());
                });
                 swal({
                    title: 'Peringatan',
                    text: 'Menambah harga grosir akan mengubah semua harga varian menjadi harga utama! Lanjut?',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Ok',
                    cancelButtonText: 'Batal',
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (inputValue) {
                    if(inputValue === true){

                       $('#price-list').show();

                       $('#form-group-price').show();
                       $('.variant_price').prop({ readOnly: true });
                       $('.variant_price').val($('#price-input').val());

                       if($('.price-qty').length <= 0){
                        add_new_grosir_row();
                        }
                    }else{
                        grosir = 0;
                        $('#grosir-slc').val(0);
                    }
                    
                });
            }
            else{
                $('#price-list').show();
            }
            
        } 
    });
    $('#reseller-slc').on('change', function() {

        if ( this.value == '0'){
            reseller = this.value;
            $('#price-reseller-list').hide();
            $('.td-reseller-qty').prop('required',false);
            $('.td-reseller-qty').removeAttr('min');
            $('.number-reseller').prop('required',false);
            
            if(option === '1' && grosir === '0'){
                $('#form-group-price').hide();
                $('.variant_price').prop({ readOnly: false });
            }
   
        } else {
            reseller = this.value;
            $('.td-reseller-qty').prop('required',true);
            $('.td-reseller-qty').attr('min',2);
            $('.number-reseller').prop('required',true);

            if(option === '1'){
                $('.variant_price').prop({ readOnly: true });
                $('#price-input').on('change',function(){
                    $('.variant_price').val($('#price-input').val());
                });
                 swal({
                    title: 'Peringatan',
                    text: 'Menambah harga reseller akan mengubah semua harga varian menjadi harga utama! Lanjut?',
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: 'Ok',
                    cancelButtonText: 'Batal',
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (inputValue) {
                    if(inputValue === true){
                        $('#price-reseller-list').show();
                        $('#form-group-price').show();
                        $('.variant_price').prop({ readOnly: true });
                        $('.variant_price').val($('#price-input').val());

                        if($('.price-qty-reseller').length <= 0){
                            add_new_reseller_row();
                        }

                    }else{

                        reseller = 0;
                        $('#reseller-slc').val(0);
                    }
                    
   
                });
            }
            else{
                $('#price-reseller-list').show();
            }

        } 
    });
        
    $(document).on("keydown.autocomplete",".autocompleteTxt",function(e){
        $(this).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: site_url +'/catalog/products/get_package_item',
                    data: {
                        query: request.term
                    },
                    success: function( data ) {
                        data = $.parseJSON(data);
                        response( data );
                    }
                });
                },
            minLength: 1,
            matchContains: true,
            selectFirst: false,
            select: function( event, ui ) {
                currentId = $(this).attr('id').replace('packageItem', '');
                $( '#packageItem'+currentId ).val( ui.item.label );
                $( '#packageItemId'+currentId ).val( ui.item.id );
            },
            change: function( event, ui ) {
                currentId = $(this).attr('id').replace('packageItem', '');
                $( "#packageItemId"+currentId ).val( ui.item? ui.item.id : 0 );
            } 
        });
    });
    $(document).on('click', '#add-package-item',function () {
        var id = Math.random().toString(36).slice(2);
        var html = '<div class="row" id="rowID-' + id + '">';
        html += '<div class="col-md-6">';
        html += '<div class="form-group">';
        html += '<label class="col-md-3 control-label">Item Name</label>';
        html += '<div class="col-md-9">';
        html += '<input id="packageItem' + id + '" class="autocompleteTxt form-control" type="text" name="package_items[' + id + '][product_name]">';
        html += '<input id="packageRowId' + id + '" class="form-control" type="hidden" name="package_items[' + id + '][row_id]" value="'+id+'">';
        html += '<input id="packageItemId' + id + '" class="form-control" type="hidden" name="package_items[' + id + '][product_id]">';
        html += '</div></div></div>';
        html += '<div class="col-md-2">';
        html += '<div class="form-group">';
        html += '<label class="col-md-3 control-label">Qty</label>';
        html += '<div class="col-md-9">';
        html += '<input class="form-control" type="number" name="package_items[' + id + '][qty]">';
        html += '</div></div></div>';
        html += '<div class="col-md-2">'
        html += '<button type="button" class="btn btn-danger btn-xs remove-item" data-id="' + id + '">x</button>';
        html += '</div></div>';
        $('#package-lists').append(html);
    });
    $('#add-price').on('click', function () {
        var count = $('#price-count').val();
        var id = Math.random().toString(36).slice(2);
        var html = '<tr>';
        html += '<td class="price-qty" style="width:10%;"><input type="number" name="price_level[' + id + '][qty]" class="form-control"></td>';
        html += '<td style="width:30%;"><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" name="price_level[' + id + '][price]" class="form-control number"></div></td>';
        // html += '<td><div class="row p-10" style="max-height: 176px; overflow: auto;">';
        // $.each(groups, function (key, group) {
        //     html += '<div class="col-sm-6 well p-10"><label>' + group.name + '</label>';
        //     html += '<div class="row">';
        //     html += '<div class="col-xs-12">';
        //     html += '<div class="has-feedback has-feedback-left">';
        //     html += '<input type="text" class="form-control input-sm number" name="price_level[' + id + '][group][' + group.merchant_group + '][price]" value="0">';
        //     html += '<div class="form-control-feedback">Rp</div>';
        //     html += '</div>';
        //     html += '</div>';
        //     html += '</div>';
        //     html += '</div>';
        // });
        // html += '</div></td>';
        html += '<td><button type="button" class="btn btn-danger btn-xs remove-price">x</button></td></tr>';
        count = parseInt(count) + 1;
        $('#price-count').val(count);
        $('#price-list tbody').append(html);
        $('.number').number(true, decimal_digit, decimal_separator, thousand_separator);
        $('.number').attr('autocomplete', 'off');
    });
    // $('#add-price-reseller').on('click', function () {
    //     var count = $('#price-count-reseller').val();
    //     var id = Math.random().toString(36).slice(2);
    //     var html = '<tr>';
    //     html += '<td class="price-qty" style="width:10%;"><input type="number" name="price_reseller[' + id + '][qty]" class="form-control"></td>';
    //     html += '<td style="width:30%;"><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" name="price_reseller[' + id + '][price]" class="form-control number"></div></td>';
    //     // html += '<td><div class="row p-10" style="max-height: 176px; overflow: auto;">';
    //     // $.each(groups, function (key, group) {
    //     //     html += '<div class="col-sm-6 well p-10"><label>' + group.name + '</label>';
    //     //     html += '<div class="row">';
    //     //     html += '<div class="col-xs-12">';
    //     //     html += '<div class="has-feedback has-feedback-left">';
    //     //     html += '<input type="text" class="form-control input-sm number" name="price_level[' + id + '][group][' + group.merchant_group + '][price]" value="0">';
    //     //     html += '<div class="form-control-feedback">Rp</div>';
    //     //     html += '</div>';
    //     //     html += '</div>';
    //     //     html += '</div>';
    //     //     html += '</div>';
    //     // });
    //     // html += '</div></td>';
    //     html += '<td><button type="button" class="btn btn-danger btn-xs remove-price">x</button></td></tr>';
    //     count = parseInt(count) + 1;
    //     $('#price-count-reseller').val(count);
    //     $('#price-list-reseller tbody').append(html);
    //     $('.number').number(true, decimal_digit, decimal_separator, thousand_separator);
    //     $('.number').attr('autocomplete', 'off');
    // });
    $('body').on('click', '.remove-price', function () {
        $(this).parents('tr').remove();
    });
    $('body').on('click', '.remove-item', function () {
        var id = $(this).attr("data-id");
        $('#rowID-'+id).remove();
    });
    $('#add-image').on('click', function () {
        $('#filemanager').modal('show');
    });
    // $('#option-generate').on('click', function () {
    //     if ($('.options:checked').length > 0) {
    //         $.ajax({
    //             url: site_url + 'catalog/products/option/generate',
    //             type: 'post',
    //             data: $('.options:checked, #id'),
    //             success: function (data) {
    //                 if (data) {
    //                     data = JSON.parse(data);
    //                     if (data.status == 'error') {
    //                         error_message(data.message);
    //                     } else {
    //                         var html = '';
    //                         var id = $('#id').val();
    //                         $.each(data.data, function (i, opt) {
    //                             html += '<tr id="option-' + opt.id + '">' +
    //                                     '<td><input type="radio" class="form-control" name="default_option" value="' + opt.id + '"></td>' +
    //                                     '<td>' + opt.option_combination + '</td>' +
    //                                     '<td><input type="text" class="number form-control" name="options[' + opt.id + '][price]" value="' + opt.price + '"></td>' +
    //                                     '<td><input type="text" class="number form-control" name="options[' + opt.id + '][weight]" value="' + opt.weight + '"></td>' +
    //                                     '<td>' +
    //                                     '<button type="button" class="btn btn-default btn-xs" onclick="set_image_option(' + id + ',' + opt.id + ')"><i class="icon-image2"></i></button>' +
    //                                     '<button type="button" class="btn btn-danger btn-xs" onclick="delete_option(' + opt.id + ')"><i class="icon-x"></i></button>' +
    //                                     '</td>' +
    //                                     '</tr>';
    //                         });
    //                         $('#combination-list tbody').append(html);
    //                         $('.number').number(true, decimal_digit, decimal_separator, thousand_separator);
    //                         $('.number').attr('autocomplete', 'off');
    //                     }
    //                 }
    //                 $('.options').prop('checked',false);
    //                 $.uniform.update('.options');
    //             }
    //         })
    //     }
    // });


    $('#option-generate').on('click', function () {
        if ($('.options:checked').length > 0) {
            var html = '';
            var id = $('#id').val();
            var grosir_aktif = $('#grosir-slc').val();
            if(colour_size_on === 1){
            
                if(count_checkbox_colour <= 0 || count_checkbox_size <= 0){
                     
                    swal({
                        title: 'Jika variasi warna dan ukuran aktif anda perlu mengisi dua-duanya!',
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: 'Ok',
                        closeOnConfirm: false,
                    });
                     return false;
                }
            }
            $.ajax({
                url: site_url + 'catalog/products/option/generate',
                type: 'post',
                data: $('.options:checked, #id'),
                success: function (data) {
                    count_checkbox_size = 0
                    count_checkbox_colour = 0
                    $('#combination-list tbody').empty();
                    if (data) {
                        data = JSON.parse(data);
                        if (data.status == 'error') {
                            error_message(data.message);
                        } else {
                            var variant_status = '';
                            if(grosir_aktif === '1'){
                                 $.each(data.data, function (i, opt) {
                                    if(opt.status==='1'){variant_status = '<td><input type="hidden" name="options[' + opt.id + '][status]" value="0"><input type="checkbox" class="checkbox-status-variant" name="options[' + opt.id + '][status]" id="variant_status' + opt.id + '" value="1" checked><label class="label-status-variant" for="variant_status' + opt.id + '">Toggle</label></td>'}
                                    else{variant_status = '<td><input type="hidden" name="options[' + opt.id + '][status]" value="0" ><input type="checkbox" class="checkbox-status-variant" name="options[' + opt.id + '][status]" id="variant_status' + opt.id + '" value="1"><label class="label-status-variant" for="variant_status' + opt.id + '">Toggle</label></td>'}
                                    html += '<tr id="option-' + opt.id + '">' +
                                    '<td><input type="radio" class="form-control" checked="" name="default_option" value="' + opt.id + '"></td>' +
                                    '<td>' + opt.option_combination + '</td>' +
                                    '<td><input type="text" class="number form-control  variant_price" required="" name="options[' + opt.id + '][price]" value="' + $('#price-input').val() + '"readonly></td>' +
                                    '<td><input type="text" class="number form-control" required="" name="options[' + opt.id + '][stock]" value="' + opt.quantity + '"></td>' +
                                    variant_status +
                                    '<td>' +
                                    '<button type="button" class="btn btn-default btn-xs" onclick="set_image_option(' + id + ',' + opt.id + ')"><i class="icon-image2"></i></button>' +
                                    '<button type="button" class="btn btn-danger btn-xs" onclick="delete_option(' + opt.id + ')"><i class="icon-x"></i></button>' +
                                    '</td>' +
                                    '</tr>';
                                });
                                $('#combination-list tbody').append(html);
                                $('.number').number(true, decimal_digit, decimal_separator, thousand_separator);
                                $('.number-grosir').number(true, decimal_digit, decimal_separator, thousand_separator);
                                $('.number-grosir').attr('autocomplete', 'off');

                            } else{
                                $.each(data.data, function (i, opt) {
                                    let quantity = 0;
                                    if(opt.quantity) {
                                        quantity = opt.quantity;
                                    } else {
                                        quantity = 0;
                                    }
                                    if(opt.status==='1'){variant_status = '<td><input type="hidden" name="options[' + opt.id + '][status]" value="0"><input type="checkbox" class="checkbox-status-variant" name="options[' + opt.id + '][status]" id="variant_status' + opt.id + '" value="1" checked><label class="label-status-variant" for="variant_status' + opt.id + '">Toggle</label></td>'}
                                    else{variant_status = '<td><input type="hidden" name="options[' + opt.id + '][status]" value="0" ><input type="checkbox" class="checkbox-status-variant" name="options[' + opt.id + '][status]" id="variant_status' + opt.id + '" value="1"><label class="label-status-variant" for="variant_status' + opt.id + '">Toggle</label></td>'}
                                    html += '<tr id="option-' + opt.id + '">' +
                                    '<td><input type="radio" class="form-control" checked="" name="default_option" value="' + opt.id + '"></td>' +
                                    '<td>' + opt.option_combination + '</td>' +
                                    '<td><input type="text" class="number form-control variant_price" required="" name="options[' + opt.id + '][price]" value="' + opt.price + '"></td>' +
                                    '<td><input type="text" class="number form-control" required="" name="options[' + opt.id + '][stock]" value="' + quantity + '"></td>' +
                                    variant_status +
                                    '<td>' +
                                    '<button type="button" class="btn btn-default btn-xs" onclick="set_image_option(' + id + ',' + opt.id + ')"><i class="icon-image2"></i></button>' +
                                    '<button type="button" class="btn btn-danger btn-xs" onclick="delete_option(' + opt.id + ')"><i class="icon-x"></i></button>' +
                                    '</td>' +
                                    '</tr>';
                                });
                                $('#combination-list tbody').append(html);
                                $('.number').number(true, decimal_digit, decimal_separator, thousand_separator);
                                $('.number-grosir').number(true, decimal_digit, decimal_separator, thousand_separator);
                                $('.number-grosir').attr('autocomplete', 'off');
                            }
                        }
                    }
                    $('.options').prop('checked',false);
                    $.uniform.update('.options');
                }
            });
        }
    });
    option_select();
    $('#preorder-time-id').on('change', function(e) {
        let value = $(this).val();
        if(value < 3) {
            $(this).val(3);
            swal('Tidak boleh kurang dari 3 hari!.', '', 'error');
        }
    })
});
function responsive_filemanager_callback(field_id) {
    if (field_id) {
        var url = $('#' + field_id).val();
        var final_url = 'principal/' + url;
        var username = $('#username').val();
        var primary = 0;
        if (!$("input:radio[name='image_primary']").is(":checked")) {
            primary = 1;
        }
        $.ajax({
            url: site_url + 'catalog/products/image',
            type: 'post',
            data: {act: 'upload', image: final_url, username: username, product: $('input[name=id]').val(), primary: primary},
            success: function (id) {
//                var id = Math.random().toString(36).slice(2);
                var html = '<div class="image" id="img' + id + '">';
                html += '<div class="image-bg" style="background-image: url(\'' + base_url + '../files/images/' + final_url + '\')"></div>';
                html += '<div class="option">';
                html += '<div class="radio"><label><input type="radio" value="' + id + '" name="image_primary" id="image_primary' + id + '">Default</label></div>';
                html += '<div><a href="javascript:delete_image(\'' + id + '\')" class="text-muted pull-right"><i class="icon-trash"></i></a></div>';
                html += '</div>';
                html += '</div>';
                $(html).insertBefore('#add-image');
                if (primary)
                    $('#image_primary' + id).prop('checked', true);
            }
        });
    }
}

function delete_image(id) {
    swal({
        title: lang.message.confirm.delete_image,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: lang.button.delete,
        cancelButtonText: lang.button.cancel,
        closeOnConfirm: false,
        closeOnCancel: false
    }, function () {
        $.ajax({
            url: site_url + 'catalog/products/image',
            type: 'post',
            data: {act: 'delete', id: id},
            success: function () {
                $('#img' + id).remove();
                if (!$("input:radio[name='image_primary']").is(":checked")) {
                    $("input:radio[name='image_primary']").prop('checked', true);
                }
                swal.close()
            }
        });
    });
}

function option_select() {
    $('#option-image .image').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).children('input').prop('checked', false);
        } else {
            $(this).addClass('active');
            $(this).children('input').prop('checked', true);
        }
    });
}

function set_image_option(product, product_option) {
    $.ajax({
        url: site_url + 'catalog/products/option/get_image',
        type: 'post',
        data: {product: product, product_option: product_option},
        success: function (res) {
            res = JSON.parse(res);
            console.log(res);
            if (res.length > 0) {
                var html = '';
                $.each(res, function (i, img) {
                    html += '<div class="image ' + (img.product_option ? 'active' : '') + '"><input type="checkbox" name="images[]" value="' + img.id + '" ' + (img.product_option ? 'checked' : '') + '><img src="' + base_url + '../files/images/' + img.image + '"></div>';
                });
                $('#option-image .modal-body').html(html);
                $('input[name=product_option]').val(product_option);
                $('#option-image').modal('show');
                option_select();
            } else {
                swal('Tidak ada gambar produk.', '', 'error');
            }
        }
    });

}

function save_image_option() {
    $.ajax({
        url: site_url + 'catalog/products/option/save_image',
        type: 'post',
        data: $('#option-image input:checked, input[name=product_option], #id'),
        success: function () {
            $('#option-image').modal('hide');
        }
    });
}

function delete_option(id) {
    swal({
        title: lang.message.confirm.delete,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: lang.button.delete,
        cancelButtonText: lang.button.cancel,
        closeOnConfirm: false,
        closeOnCancel: false
    }, function () {
        $.ajax({
            url: site_url + 'catalog/products/option/delete',
            type: 'post',
            data: {product_option: id},
            success: function () {
                $('#option-' + id).remove();
                swal.close()
            }
        });
    });
}

function get_table_deskripsi($idkat){
    var id = $('#category').val();
    var tr = '';
    var div = '';
    $('#table_desk tbody').html('');
    $('#add_param_table_desk').html('');
    $.ajax({
        method: 'post',
        url: site_url + 'catalog/products/get_table_description',
        data: {
            id: id
        },
        success: function(data) {
            data = JSON.parse(data);
            var i;
            for(i in data){
                var key = Math.random().toString(36).slice(2);
                tr += '<tr>';
                tr += '<td style="width:200px">'+ data[i].nama +'</td>';
                tr += '<input type="hidden" name="table_description['+key+'][row_id]" id="tabledeskrowid'+key+'"  class="form-control" value="'+key+'">';
                tr += '<input type="hidden" name="table_description['+key+'][id_deskripsi]" id="tabledescriptionid'+key+'"  class="form-control" value="'+data[i].id_deskripsi+'">';
                tr += '<input type="hidden" name="table_description['+key+'][name]" id="tabledescriptionname'+key+'"  class="form-control" value="'+data[i].nama+'">';
                tr += '<td><input type="text" name="table_description['+key+'][value]" id="tabledescription'+key+'"  class="form-control" placeholder="Silahkan input '+data[i].nama+' produk Anda" required>';
                tr += '</td>';
                tr += '</tr>';
            }
            div += '<br><a onclick="add_param()" class="btn btn-success">Add Param</a><br><br>';
            $('#table_desk tbody').append(tr);
            $('#add_param_table_desk').append(div);
        }
    });
}

function add_param(){
    var tr = '';
    var i = $('#count_btn').val();
    var key = Math.random().toString(36).slice(2);
    i++;
    tr += '<tr id="tr'+i+'">';
    tr += '<td style="width:200px">';
    tr += '<input type="hidden" value="'+key+'" name="table_description['+key+'][row_id]" id="param'+i+'"  class="form-control" placeholder="Parameter" required>';
    tr += '<input type="hidden" name="table_description['+key+'][id_deskripsi]" id="tabledescriptionid'+key+'"  class="form-control" value="0">';
    tr += '<input type="text" name="table_description['+key+'][name]" id="param'+i+'"  class="form-control" placeholder="Parameter" required>';
    tr += '</td>'; 
    tr += '<td><input type="text" name="table_description['+key+'][value]" id="value'+i+'"  class="form-control" placeholder="Value" required></td>';
    tr += '<td style="width:60px"><a onclick="delete_param('+i+')" class="btn btn-danger"><i class="icon-trash"></></a></td>';
    tr += '</tr>';
    
    $('#param_other_table_desk').append(tr);
    $('#count_btn').val(i);
}

function delete_param(id){
    $('#tr'+id).remove();
    var i = $('#count_param').val();
    i--;
    $('#count_param').val(i);
}

function add_new_grosir_row(){
    var count = $('#price-count').val();
    var id = Math.random().toString(36).slice(2);
    var html = '<tr>';
    html += '<td class="price-qty" width="10%"><input type="number" min="2" required name="price_level[' + id + '][qty]" class="form-control td-grosir-qty"></td>';
    html += '<td width="30%"><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" required name="price_level[' + id + '][price]" class="form-control number-grosir"></div></td>';
    html += '<td><button type="button" class="btn btn-danger btn-xs remove-price">x</button></td></tr>';
    count = parseInt(count) + 1;
    $('#price-count').val(count);
    $('#price-list tbody').append(html);
    $('.number-grosir').number(true, decimal_digit, decimal_separator, thousand_separator);
    $('.number-grosir').attr('autocomplete', 'off');
}

function add_new_reseller_row(){
    var count = $('#price-count-reseller').val();
    var id = Math.random().toString(36).slice(2);
    var html = '<tr>';
    html += '<td class="price-qty-reseller" width="10%"><input type="number" required min="2" name="price_reseller_list[' + id + '][qty]" class="form-control td-reseller-qty"></td>';
    html += '<td width="30%"><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" required name="price_reseller_list[' + id + '][price]" class="form-control number-reseller"></div></td>';
    html += '<td><button type="button" class="btn btn-danger btn-xs remove-price-reseller">x</button></td></tr>';
    count = parseInt(count) + 1;
    $('#price-count-reseller').val(count);
    $('#price-reseller-list tbody').append(html);
    $('.number-reseller').number(true, decimal_digit, decimal_separator, thousand_separator);
    $('.number-reseller').attr('autocomplete', 'off');
}
//function add_option() {
//    var id = $('#list-option').val();
//    if (id == '') {
//        error_message('Variasi belum dipilih!');
//        return;
//    }
//    $.ajax({
//        type: 'post',
//        url: site_url + 'catalog/products/get_option/' + id,
//        success: function (data) {
//            data = JSON.parse(data);
//            id = Math.random().toString(36).slice(2);
////            console.log(data);
//            var option = data.option;
//            var variants = data.variants;
//            var html = '<div class="tab-pane" id="option-' + id + '">';
//            html += '<input type="hidden" name="options[' + id + '][product_option]" value="">';
//            html += '<input type="hidden" name="options[' + id + '][option]" value="' + option.id + '">';
//            html += '<div class="form-group">';
//            html += '<label class="col-md-3 control-label">Required?</label>';
//            html += '<div class="col-md-9"><input type="checkbox" name="options[' + id + '][required]" value="1" data-on-text="Yes" data-off-text="No" class="switch" checked="checked"></div>';
//            html += '</div>';
//            html += '  <div class="table-responsive">';
//            html += '   <table id="option-variant-' + id + '" class="table">';
//            html += '     <thead>';
//            html += '       <tr>';
//            html += '         <th style="width: 50%">Variasi</th>';
//            html += '         <th style="width: 25%">Harga</th>';
//            html += '         <th style="width: 20%">Berat</th>';
//            html += '         <th style="width: 5%"></th>';
//            html += '       </tr>';
//            html += '     </thead>';
//            html += '     <tbody></tbody>';
//            html += '     <tfoot>';
//            html += '       <tr>';
//            html += '         <th colspan="4" class="text-center"><button type="button" onclick="add_option_variant(\'' + id + '\');" class="btn btn-default">Tambah Variasi</button></th>';
//            html += '       </tr>';
//            html += '     </tfoot>';
//            html += '   </table>';
//            html += '  </div>';
//
//            html += '<select id="option-variants-' + id + '" style="display: none;">'
//            $.each(variants, function (i, d) {
//                html += '<option value="' + d.id + '">' + d.value + '</option>';
//            });
//            html += '</select>';
//            html += '</div>';
//
//            $('#option .tab-content').append(html);
//            $(".switch").bootstrapSwitch();
//            $('#option .nav').append('<li><a href="#option-' + id + '" data-toggle="tab"><span class="label label-danger pull-right" onclick="$(\'a[href=\\\'#option-' + id + '\\\']\').parent().remove(); $(\'#option-' + id + '\').remove(); $(\'#option .nav a:first\').tab(\'show\')"><i class="icon-x"></i></span> ' + option.name + '</a></li>');
//            $('#option .nav a[href=\'#option-' + id + '\']').tab('show');
//        }
//    });
//}
//
//function add_option_variant(id) {
//    var variant_id = Math.random().toString(36).slice(2);
//    var html = '<tr id="option-variant-row-' + variant_id + '">';
//    html += ' <td><select name="options[' + id + '][variant][' + variant_id + '][option_variant]" class="form-control">';
//    html += $('#option-variants-' + id).html();
//    html += '</select><input type="hidden" name="options[' + id + '][variant][' + variant_id + '][product_option_variant]" value=""></td>';
//    html += '<td><input type="text" class="form-control number" name="options[' + id + '][variant][' + variant_id + '][price]" value="0"></td>';
//    html += '<td><div class="input-group"><input type="number" name="options[' + id + '][variant][' + variant_id + '][weight]" class="form-control" value="0"><span class="input-group-addon">gram</span></div></td>';
//    html += '<td><button type="button" class="btn btn-danger" onclick="$(\'#option-variant-row-' + variant_id + '\').remove();"><i class="icon-x"></i></td>';
//    html += '</tr>';
//    $('#option-' + id + ' tbody').append(html);
//    $('.number').number(true, decimal_digit, decimal_separator, thousand_separator);
//    $('.number').attr('autocomplete', 'off');
//}
