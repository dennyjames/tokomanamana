$(function () {
    
    var prod_type = $('#type').val();
    var promo = $('#promo-slc').val();
    if(prod_type == 's') {
        $("#package-tab").hide();
    } else {
        $("#package-tab").show();
    }
    if(promo == '0') {
        $("#promo-tab").hide();
    } else {
        $("#promo-tab").show();
    }

    $('.page-header .page-title').css({width: $('.page-header .page-title').width() - $('.page-header .heading-elements').width() - 50});
    $('#category').on('change', function () {
        $.ajax({
            type: "POST",
            url: site_url + 'catalog/products/get_feature_form',
            data: {category: $(this).val(), product: $('#id').val()},
            success: function (data) {
                $('#feature-form').html(data);
            }
        });
    });
    $('#type').on('change', function() {
        if ( this.value == 's'){
            $("#package-tab").hide();
        } else {
            $("#package-tab").show();
        } 
    });
    $('#promo-slc').on('change', function() {
        if ( this.value == '0'){
            $("#promo-tab").hide();
        } else {
            $("#promo-tab").show();
        } 
    });
        
    $(document).on("keydown.autocomplete",".autocompleteTxt",function(e){
        $(this).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: site_url +'/catalog/products/get_package_item',
                    data: {
                        query: request.term
                    },
                    success: function( data ) {
                        data = $.parseJSON(data);
                        response( data );
                    }
                });
                },
            minLength: 1,
            matchContains: true,
            selectFirst: false,
            select: function( event, ui ) {
                currentId = $(this).attr('id').replace('packageItem', '');
                $( '#packageItem'+currentId ).val( ui.item.label );
                $( '#packageItemId'+currentId ).val( ui.item.id );
            },
            change: function( event, ui ) {
                currentId = $(this).attr('id').replace('packageItem', '');
                $( "#packageItemId"+currentId ).val( ui.item? ui.item.id : 0 );
            } 
        });
    });
    $(document).on('click', '#add-package-item',function () {
        var id = Math.random().toString(36).slice(2);
        var html = '<div class="row" id="rowID-' + id + '">';
        html += '<div class="col-md-6">';
        html += '<div class="form-group">';
        html += '<label class="col-md-3 control-label">Item Name</label>';
        html += '<div class="col-md-9">';
        html += '<input id="packageItem' + id + '" class="autocompleteTxt form-control" type="text" name="package_items[' + id + '][product_name]">';
        html += '<input id="packageRowId' + id + '" class="form-control" type="hidden" name="package_items[' + id + '][row_id]" value="'+id+'">';
        html += '<input id="packageItemId' + id + '" class="form-control" type="hidden" name="package_items[' + id + '][product_id]">';
        html += '</div></div></div>';
        html += '<div class="col-md-2">';
        html += '<div class="form-group">';
        html += '<label class="col-md-3 control-label">Qty</label>';
        html += '<div class="col-md-9">';
        html += '<input class="form-control" type="number" name="package_items[' + id + '][qty]">';
        html += '</div></div></div>';
        html += '<div class="col-md-2">'
        html += '<button type="button" class="btn btn-danger btn-xs remove-item" data-id="' + id + '">x</button>';
        html += '</div></div>';
        $('#package-lists').append(html);
    });
    $('#add-price').on('click', function () {
        var count = $('#price-count').val();
        var id = Math.random().toString(36).slice(2);
        var html = '<tr>';
        html += '<td class="price-qty"><input type="number" name="price_level[' + id + '][qty]" class="form-control"></td>';
        html += '<td><div class="input-group"><span class="input-group-addon">Rp</span><input type="text" name="price_level[' + id + '][price]" class="form-control number"></div></td>';
        html += '<td><div class="row p-10" style="max-height: 176px; overflow: auto;">';
        $.each(groups, function (key, group) {
            html += '<div class="col-sm-6 well p-10"><label>' + group.name + '</label>';
            html += '<div class="row">';
            html += '<div class="col-xs-12">';
            html += '<div class="has-feedback has-feedback-left">';
            html += '<input type="text" class="form-control input-sm number" name="price_level[' + id + '][group][' + group.merchant_group + '][price]" value="0">';
            html += '<div class="form-control-feedback">Rp</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            html += '</div>';
        });
        html += '</div></td>';
        html += '<td><button type="button" class="btn btn-danger btn-xs remove-price">x</button></td></tr>';
        count = parseInt(count) + 1;
        $('#price-count').val(count);
        $('#price-list tbody').append(html);
        $('.number').number(true, decimal_digit, decimal_separator, thousand_separator);
        $('.number').attr('autocomplete', 'off');
    });
    $('body').on('click', '.remove-price', function () {
        $(this).parents('tr').remove();
    });
    $('body').on('click', '.remove-item', function () {
        var id = $(this).attr("data-id");
        $('#rowID-'+id).remove();
    });
    $('#add-image').on('click', function () {
        $('#filemanager').modal('show');
    });
    $('#option-generate').on('click', function () {
        if ($('.options:checked').length > 0) {
            $.ajax({
                url: site_url + 'catalog/products/option/generate',
                type: 'post',
                data: $('.options:checked, #id'),
                success: function (data) {
                    if (data) {
                        data = JSON.parse(data);
                        if (data.status == 'error') {
                            error_message(data.message);
                        } else {
                            var html = '';
                            var id = $('#id').val();
                            $.each(data.data, function (i, opt) {
                                html += '<tr id="option-' + opt.id + '">' +
                                        '<td><input type="radio" class="form-control" name="default_option" value="' + opt.id + '"></td>' +
                                        '<td>' + opt.option_combination + '</td>' +
                                        '<td><input type="text" class="number form-control" name="options[' + opt.id + '][price]" value="' + opt.price + '"></td>' +
                                        '<td><input type="text" class="number form-control" name="options[' + opt.id + '][weight]" value="' + opt.weight + '"></td>' +
                                        '<td>' +
                                        '<button type="button" class="btn btn-default btn-xs" onclick="set_image_option(' + id + ',' + opt.id + ')"><i class="icon-image2"></i></button>' +
                                        '<button type="button" class="btn btn-danger btn-xs" onclick="delete_option(' + opt.id + ')"><i class="icon-x"></i></button>' +
                                        '</td>' +
                                        '</tr>';
                            });
                            $('#combination-list tbody').append(html);
                            $('.number').number(true, decimal_digit, decimal_separator, thousand_separator);
                            $('.number').attr('autocomplete', 'off');
                        }
                    }
                    $('.options').prop('checked',false);
                    $.uniform.update('.options');
                }
            })
        }
    });
    option_select();
});
function responsive_filemanager_callback(field_id) {
    if (field_id) {
        var url = $('#' + field_id).val();
        var primary = 0;
        if (!$("input:radio[name='image_primary']").is(":checked")) {
            primary = 1;
        }
        $.ajax({
            url: site_url + 'catalog/products/image',
            type: 'post',
            data: {act: 'upload', image: url, product: $('input[name=id]').val(), primary: primary},
            success: function (id) {
//                var id = Math.random().toString(36).slice(2);
                var html = '<div class="image" id="img' + id + '">';
                html += '<div class="image-bg" style="background-image: url(\'' + base_url + '../files/images/' + url + '\')"></div>';
                html += '<div class="option">';
                html += '<div class="radio"><label><input type="radio" value="' + id + '" name="image_primary" id="image_primary' + id + '">Default</label></div>';
                html += '<div><a href="javascript:delete_image(\'' + id + '\')" class="text-muted pull-right"><i class="icon-trash"></i></a></div>';
                html += '</div>';
                html += '</div>';
                $(html).insertBefore('#add-image');
                if (primary)
                    $('#image_primary' + id).prop('checked', true);
            }
        });
    }
}

function delete_image(id) {
    swal({
        title: lang.message.confirm.delete_image,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: lang.button.delete,
        cancelButtonText: lang.button.cancel,
        closeOnConfirm: false,
        closeOnCancel: false
    }, function () {
        $.ajax({
            url: site_url + 'catalog/products/image',
            type: 'post',
            data: {act: 'delete', id: id},
            success: function () {
                $('#img' + id).remove();
                if (!$("input:radio[name='image_primary']").is(":checked")) {
                    $("input:radio[name='image_primary']").prop('checked', true);
                }
                swal.close()
            }
        });
    });
}

function option_select() {
    $('#option-image .image').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).children('input').prop('checked', false);
        } else {
            $(this).addClass('active');
            $(this).children('input').prop('checked', true);
        }
    });
}

function set_image_option(product, product_option) {
    $.ajax({
        url: site_url + 'catalog/products/option/get_image',
        type: 'post',
        data: {product: product, product_option: product_option},
        success: function (res) {
            res = JSON.parse(res);
            console.log(res);
            if (res.length > 0) {
                var html = '';
                $.each(res, function (i, img) {
                    html += '<div class="image ' + (img.product_option ? 'active' : '') + '"><input type="checkbox" name="images[]" value="' + img.id + '" ' + (img.product_option ? 'checked' : '') + '><img src="' + base_url + '../files/images/' + img.image + '"></div>';
                });
                $('#option-image .modal-body').html(html);
                $('input[name=product_option]').val(product_option);
                $('#option-image').modal('show');
                option_select();
            } else {
                swal('Tidak ada gambar produk.', '', 'error');
            }
        }
    });

}

function save_image_option() {
    $.ajax({
        url: site_url + 'catalog/products/option/save_image',
        type: 'post',
        data: $('#option-image input:checked, input[name=product_option], #id'),
        success: function () {
            $('#option-image').modal('hide');
        }
    });
}

function delete_option(id) {
    swal({
        title: lang.message.confirm.delete,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: lang.button.delete,
        cancelButtonText: lang.button.cancel,
        closeOnConfirm: false,
        closeOnCancel: false
    }, function () {
        $.ajax({
            url: site_url + 'catalog/products/option/delete',
            type: 'post',
            data: {product_option: id},
            success: function () {
                $('#option-' + id).remove();
                swal.close()
            }
        });
    });
}

function get_table_deskripsi($idkat){
    var id = $('#category').val();
    var tr = '';
    var div = '';
    $('#table_desk tbody').html('');
    $.ajax({
        method: 'post',
        url: site_url + 'catalog/products/get_table_description',
        data: {
            id: id
        },
        success: function(data) {
            data = JSON.parse(data);
            var i;
            for(i in data){
                var key = Math.random().toString(36).slice(2);
                tr += '<tr>';
                tr += '<td style="width:200px">'+ data[i].nama +'</td>';
                tr += '<input type="hidden" name="table_description['+key+'][row_id]" id="tabledeskrowid'+key+'"  class="form-control" value="'+key+'">';
                tr += '<input type="hidden" name="table_description['+key+'][id_deskripsi]" id="tabledescriptionid'+key+'"  class="form-control" value="'+data[i].id_deskripsi+'">';
                tr += '<input type="hidden" name="table_description['+key+'][name]" id="tabledescriptionname'+key+'"  class="form-control" value="'+data[i].nama+'">';
                tr += '<td><input type="text" name="table_description['+key+'][value]" id="tabledescription'+key+'"  class="form-control" placeholder="Silahkan input '+data[i].nama+' produk Anda" required>';
                tr += '</td>';
                tr += '</tr>';
            }
            div += '<br><a onclick="add_param()" class="btn btn-success">Add Param</a><br><br>';
            $('#table_desk tbody').append(tr);
            $('#add_param_table_desk').append(div);
        }
    });
}

function add_param(){
    var tr = '';
    var i = $('#count_btn').val();
    var key = Math.random().toString(36).slice(2);
    i++;
    tr += '<tr id="tr'+i+'">';
    tr += '<td style="width:200px">';
    tr += '<input type="hidden" value="'+key+'" name="table_description['+key+'][row_id]" id="param'+i+'"  class="form-control" placeholder="Parameter" required>';
    tr += '<input type="hidden" name="table_description['+key+'][id_deskripsi]" id="tabledescriptionid'+key+'"  class="form-control" value="0">';
    tr += '<input type="text" name="table_description['+key+'][name]" id="param'+i+'"  class="form-control" placeholder="Parameter" required>';
    tr += '</td>'; 
    tr += '<td><input type="text" name="table_description['+key+'][value]" id="value'+i+'"  class="form-control" placeholder="Value" required></td>';
    tr += '<td style="width:60px"><a onclick="delete_param('+i+')" class="btn btn-danger"><i class="icon-trash"></></a></td>';
    tr += '</tr>';
    
    $('#param_other_table_desk').append(tr);
    $('#count_btn').val(i);
}

function delete_param(id){
    $('#tr'+id).remove();
    var i = $('#count_param').val();
    i--;
    $('#count_param').val(i);
}
function formatNumber(num) {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
}
function new_stock(id){
    //console.log('bekerja');
    var old_stk = $('#stk_old'+id).html();
    var new_stk = $('#stk_new'+id).val();

    if(new_stk==''){
        var total_stk = parseInt(old_stk) + 0;
    } else {
        var total_stk = parseInt(old_stk) + parseInt(new_stk);
    }
    $('#stk_total'+id).val(formatNumber(total_stk));
    if(total_stk == 0){
        $('#stk_total'+id).attr('style', 'color:red');
    } else {
        $('#stk_total'+id).attr('style', 'color:#000');
    }
}

function tipe(id){
    var tipe = $('#tipe_'+id).val();
    $('#tipe'+id).val(tipe);
}
