function confirm(id, stock) {
    swal({
        title: "Konfirmasi Stok?",
        type: "info",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Ya, konfirmasi",
        cancelButtonText: "Tidak!",
        closeOnConfirm: true,
        closeOnCancel: true,
        cancelButtonColor: 'red'
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
              url: site_url + 'catalog/refund_stock/confirm',
              type: 'post',
              data: {id: id, stock: stock},
              success: function(data) {
                  let table = $('#table').DataTable();
                  table.draw();
                  swal({
                      title: data.message,
                      type: data.status,
                      showConfirmButton: true
                  });
              }
          });
        } else {
          $.ajax({
              url: site_url + 'catalog/refund_stock/cancel',
              type: 'post',
              data: {id: id},
              success: function(data) {
                  let table = $('#table').DataTable();
                  table.draw();
                  swal({
                      title: data.message,
                      type: data.status,
                      showConfirmButton: true
                  });
              }
          });
        }
    });
}