$('body').on('click', '#btn-export', function (e) {
    e.preventDefault();
    var table = $('#table').DataTable();
    var page = table.page.info();
    var search = table.search();
    var order = table.order();
    window.open(site_url + 'catalog/products/export?start=' + page.start + '&length=' + page.length + '&search=' + search + '&order=' + order[0], '_blank');
});
$('#btn-import').on('click', function () {
    $('#modal-import').modal('show');
    $('#modal-import form')[0].reset();
});
$('#submit-import').on('click', function () {
    var data = new FormData();
    var file = $('#file')[0].files[0];
    data.append('file', file);
    $.ajax({
        url: site_url+'catalog/products/import',
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (res) {
            responseForm(res);
        }
    });
});
$('body').on('click', '.status', function(e) {
    e.preventDefault();
    let el = $(this);
    let id = el.data('id');
    swal({
        title: 'Ubah Status Produk?',
        type: 'info',
        showCancelButton: true,
        confirmButtonText: 'Ubah',
        cancelButtonText: 'Batal'
    },function(isConfirm){
        if(isConfirm) {
            if (el.data('status') == 0) {
                data = {id: el.data('id'), status: 1 };
            } else if (el.data('status') == 1)  {
                data = {id: el.data('id'), status: 0 };
            }
            $.ajax({
                type: 'post',
                url: current_url + '/update_status_product',
                data: data,
                success: function (data) {      
                    if(data==1){
                        el.data('status',1);
                        el.html('<i class="status" style="color:#5CB85C;text-decoration:underline;">Aktif</i>');
                    }
                    else if(data==0){
                        el.data('status',0);
                        el.html('<i class="status" style="color:#D9534F;text-decoration:underline;">Aktifkan produk ini</i>');
                    }
                    swal({
                        title: 'Status Berhasil Diubah!',
                        type: 'success',
                        timer: 1000,
                        showConfirmButton: false
                    });
                }
            });
        } else {

        }
    });
});
$('body').on('change', '.quantity', function (e) {
    e.preventDefault();
    var el = $(this);
    el.attr('disabled', 'disabled');
    if(el.val() == "") {
        swal({
            title: 'Stok tidak boleh kosong!',
            type: 'error',
            timer: 1000,
            showConfirmButton: false
        });
        el.removeAttr('disabled');
        el.val(0);
    } else {
        $.ajax({
            type: "POST",
            url: current_url + '/update_quantity_product',
            data: {quantity: el.val(), id: el.data('id')},
            success: function (data) {
                el.removeAttr('disabled');
                el.data('id', data);
                swal({
                    title: 'Stok Berhasil Diubah!',
                    type: 'success',
                    timer: 1000,
                    showConfirmButton: false
                });
            }
        });
    }
});