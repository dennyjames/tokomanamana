function tipe_check(val) {
    var checkbox_ = $('#checkbox_').val();
    var split = checkbox_.split('-');

    if(val == '0'){
        if(split[0] == ''){
            $('#checkbox_').val('1-'+split[1]+'-'+split[2]);
            $('#tipe0').show();
        }else{
            $('#checkbox_').val('-'+split[1]+'-'+split[2]);
            $('#tipe0').hide();
        }
    }else if(val == '1'){
        if(split[1] == ''){
            $('#checkbox_').val(split[0]+'-1-'+split[2]);
            $('#tipe1').show();
        }else{
            $('#checkbox_').val(split[0]+'--'+split[2]);
            $('#tipe1').hide();
        }
    }else{
        if(split[2] == ''){
            $('#checkbox_').val(split[0]+'-'+split[1]+'-1');
            $('#tipe2').show();
        }else{
            $('#checkbox_').val(split[0]+'-'+split[1]+'-');
            $('#tipe2').hide();
        }
    }
}

function change_0(val, param) {
	$('.li_baru'+param).remove();
	$.ajax({
    type: 'POST',
    url: current_url + "/filter_" + param,
    data: {search: val},
    dataType: "json",
    success: function (data) {
      if (data.success == '1') {
      	for(let a=0; a<data.data.length; a++){
          let nama = data.data[a].name;
          let id = data.data[a].id;
      		$('#item_choice'+param).after('<li class="li_baru'+param+'"><a href="javascript:void(0);" onclick="set_filter_0(\''+nama+'\', \''+id+'\', '+param+')">'+data.data[a].name+'</a></li>');
      	}
      }
    }
 	});
}

function set_filter_0(nama, id, param) {
  $('#filter-'+param).html(nama + '<span class="caret"></span>');
  $('#inp_tipe'+param).val(id);
}

function is_active_(val) {
  $('#is_active').val(val);
}

function cm_(val) {
  $('#customer_merchant').val(val);
}

function submit_data(param) {
  let checkbox_ = $('#checkbox_').val();
  let error = '';
  let head_error = 'Input Error';
  let nama = $('#nama').val();
  let desc = $('#deskripsi').val();
  let qty_reward = $('#qty_reward').val();
  let id = $('#decodeid').val();
  let base64_image = $('#base64_image').val();

  if(param == 0){
    var link = 'save';
  }else{
    var link = 'save_edit';
  }

  let s0 = '';
  let p0 = '';
  let c0 = '';

  let qty0 = 0;
  let qty1 = 0;
  let qty2 = 0;

  if(desc == ''){
    error = 'Deskripsi Reward Belum Diisi';
  }

  if(nama == ''){
    error = 'Nama Belum Diisi';
  }

  if(qty_reward == ''){
    error = 'QTY Belum Diisi';
  }

  if(checkbox_ == '--'){
    error = 'Jenis Reward Belum Ditentukan';
  }else{
    let split = checkbox_.split('-');
    if(split[0] != ''){
      let s = $('#inp_tipe0').val();
      if(s != ''){
        let qty_p = $('#qty_product').val();
        if(qty_p != ''){
          s0 = s;
          qty0 = qty_p;
        }else{
          error = 'QTY Produk Belum Diisi';
        }
      }else{
        error = 'Produk Standar Belum Dipilih';
      }
    }
    if(split[1] != ''){
      let p = $('#inp_tipe1').val();
      if(p != ''){
        let qty_pk = $('#qty_paket').val();
        if(qty_pk != ''){
          p0 = p;
          qty1 = qty_pk;
        }else{
          error = 'QTY Paket Belum Diisi';
        }
      }else{
        error = 'Produk Paket Belum Dipilih';
      }
    }
    if(split[2] != ''){
      let c = $('#inp_tipe2').val();
      if(c != ''){
        let qty_c = $('#qty_coupon').val();
        if(qty_c != ''){
          c0 = c;
          qty2 = qty_c;
        }else{
          error = 'QTY Coupon Belum Diisi';
        }
      }else{
        error = 'Coupon Belum Dipilih';
      }
    }
  }

  let point = $('#point').val();
  if(point == ''){
    error = 'Point Belum Diisi';
  }

  if(error == ''){
    $.ajax({
      type: 'POST',
      url: current_url + "/" + link,
      data: {
        tipe: checkbox_,
        id_product: s0,
        id_product_paket: p0,
        id_coupon: c0,
        is_active: $('#is_active').val(),
        point: point,
        customer_merchant: $('#customer_merchant').val(),
        nama: nama,
        desc: desc,
        qty_reward: qty_reward,
        id: id,
        base64_image: base64_image,
        qty0: qty0,
        qty1: qty1,
        qty2: qty2
      },
      dataType: "json",
      success: function (data) {
        if (data.success == '1') {
          window.location.replace(current_url);
        }else if(data.success == '0'){
          swal({
            title: "Error !",
            text: 'Data Sudah Ada',
            type: 'error'
          }).then((result) => {
            
          });
        }
      }
    });
  }else{
    swal({
      title: head_error,
      text: error,
      type: 'error'
    }).then((result) => {
      
    });
  }
}

function chose_image() {
  $('#gambar_input').click();
}

function change_image(val) {
  if (val.files && val.files[0]) {
    
    var FR= new FileReader();
    
    FR.addEventListener("load", function(e) {
      $('#img').show();
      document.getElementById("img").src       = e.target.result;
      $('#base64_image').val(e.target.result);
    }); 
    
    FR.readAsDataURL( val.files[0] );
  }
}

function change_tahun_awal(val) {
    if(val != ''){
        $('#bulan_mulai').prop('disabled', false);
    }else{
        $('.tahun_mulai').prop('disabled', true);
    }
}

function change_bulan_awal(val) {
    if(val != ''){
        $('#tanggal_mulai').prop('disabled', false);
        var total_tanggal = get_tanggal($('#tahun_mulai').val(), $('#bulan_mulai').val());
        var tgl = '<option value="">Pilih Tanggal</option>';
        for(var a=1; a<=total_tanggal; a++){
            tgl += '<option value="'+a+'">'+a+'</option>';
        }
        $('#tanggal_mulai').html(tgl);
    }else{
        $('.bulan_mulai').prop('disabled', true);
    }
}

function change_tanggal_awal(val) {
    if(val != ''){
        $('#tahun_selesai').prop('disabled', false);
        var thn = $('#tahun_mulai').val();
        var thn_ = '<option value="">Pilih Tahun</option>';
        for(var a=0; a<5; a++){
            thn_ += '<option value="'+(thn-(a * -1))+'">'+(thn-(a * -1))+'</option>';
        }
        $('#tahun_selesai').html(thn_);
    }else{
        $('.tanggal_mulai').prop('disabled', true);
    }
}

function change_tahun_akhir(val) {
    if(val != ''){
        $('#bulan_selesai').prop('disabled', false);
    }else{
        $('.tahun_selesai').prop('disabled', true);
    }
}

function change_bulan_akhir(val) {
    if(val != ''){
        $('#tanggal_selesai').prop('disabled', false);
        var total_tanggal = get_tanggal($('#tahun_selesai').val(), $('#bulan_selesai').val());
        var tgl = '<option value="">Pilih Tanggal</option>';
        for(var a=1; a<=total_tanggal; a++){
            tgl += '<option value="'+a+'">'+a+'</option>';
        }
        $('#tanggal_selesai').html(tgl);
    }else{
        $('.bulan_selesai').prop('disabled', true);
    }
}

function get_tanggal(tahun, bulan) {
    if(bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10 || bulan == 12){
        return 31;
    }else if(bulan == 4 || bulan == 6 || bulan == 9 || bulan == 11){
        return 30;
    }else{
        if(bulan % 4 == 0){
            return 29;
        }else{
            return 28;
        }
    }
}

