var lang = {
    button: {
        delete: 'Hapus',
        cancel: 'Batal',
        save: 'Simpan',
        yes: 'Ya',
    },
    message: {
        confirm: {
            delete: 'Yakin data akan dihapus?',
            activate: 'Yakin data akan diaktifkan?',
            paid: 'Yakin pembayaran sudah diterima?',
            delete_image: 'Yakin gambar akan dihapus?',
            delete_image_success: 'Gambar berhasil dihapus.',
            delete_combination: 'Yakin kombinasi akan dihapus?',
            delete_combination_success: 'Kombinasi berhasil dihapus.',
        },
    },
    table: {
        processing: "Sedang memproses...",
        search: 'Cari',
        search_placeholder: 'Ketik kata kunci...',
        length_menu: 'Tampilkan',
        zero_records: "Tidak ditemukan data yang sesuai",
        info: "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        info_empty: "Menampilkan 0 sampai 0 dari 0 entri",
        info_filtered: "(disaring dari _MAX_ entri keseluruhan)",
        first: 'Awal',
        last: 'Akhir',
    }
};
