$(function () {
    tinymce.init({
        selector: ".tinymce",
        plugins: "align textcolor colorpicker link image filemanager table media placeholder advlist code table autoresize lists",
        browser_spellcheck: true,
        toolbar: [
            'fontselect,fontsizeselect,bold,italic,underline,forecolor,backcolor,blockquote,align,formatselect',
            'link,bullist,numlist,table,image,media,code'
        ],
        external_filemanager_path: base_url + "filemanager/",
        filemanager_title: "File manager",
        external_plugins: {"filemanager": base_url + "filemanager/plugin.js"},
        file_browser_callback: function (field_name, url, type, win) {
            console.log(field_name);
            console.log(url);
            console.log(type);
            console.log(win);
//            win.document.getElementById(field_name).value = 'my browser value';
        },
        cleanup_on_startup: true,
        extended_valid_elements: 'img[class=imgContent|!src|border:0|alt|title|width|height|style],span[*]',
        fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt 72pt",
        menubar: false,
        statusbar: false,
        relative_urls: false,
        convert_urls: false,
        branding: false,
        setup: function (editor) {
            editor.on('change', function () {
                tinymce.triggerSave();
            });
        }
    });
    $(".styled").uniform({
        radioClass: 'choice'
    });
    $('.bootstrap-select').selectpicker();
    $('.select2').select2();
    $('.tags-input').tagsinput();
    $('.number').number(true, decimal_digit, decimal_separator, thousand_separator);
    $('.number').attr('autocomplete', 'off');
    $(".switch").bootstrapSwitch();
    $('.date').datepicker({
        format: 'yyyy-mm-dd',
        zIndex: 10001
    });
    $('body').on('click', '#edit-image-btn', function () {
        $('#image-preview').remove();
    });
    $('body').on('click', '#delete-image-btn', function () {
        $('#image').val('');
        $('#image-preview').remove();
        $('#add-image-btn').show();
        $('#edit-image-btn').hide();
        $('#delete-image-btn').hide();
    });
    $("#form").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        // Different components require proper error label placement
        errorPlacement: function (error, element) {
            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                } else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.remove();
            label.closest('.form-group').removeClass('has-error');
        },
//        rules: rules_form,
//        messages: messages_form,
        submitHandler: function (form) {
            $(form).ajaxSubmit({
                success: function (data) {
                    responseForm(data);
                }
            });
            return false;
        }
    });
});

function convertToSlug(string) {
    $('#seo_url').val(string.toLowerCase().replace(/[^\w ]+/g, '').replace(/ +/g, '-'));
}
function responsive_filemanager_callback(field_id) {
    if (field_id) {
        var url = $('#' + field_id).val();
        var html = '<div class="col-sm-3" id="image-preview">';
        html += '<div class="thumbnail"><div class="thumb">';
        html += '<img src="' + base_url + '../files/images/' + url + '">';
        html += '</div></div></div>';
        $(html).insertBefore('#add-image');
        $('#add-image-btn').hide();
        $('#edit-image-btn').show();
        $('#delete-image-btn').show();
    }
}