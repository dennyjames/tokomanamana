var canvas = document.getElementById('signature-pad');

// Adjust canvas coordinate space taking into account pixel ratio,
// to make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas() {
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
}

window.onresize = resizeCanvas;
resizeCanvas();

var signaturePad = new SignaturePad(canvas, {
  backgroundColor: '#ccc' // necessary for saving image as JPEG; can be removed is only saving as PNG or SVG
});
document.getElementById('clear-canvas').addEventListener('click', function () {
    signaturePad.clear();
  });
jQuery.validator.addMethod("letters_spaceonly", function(value, element) 
{
return this.optional(element) || /^[a-z ]+$/i.test(value);
}, "Harap masukan huruf dan spasi saja.");
jQuery.validator.addMethod("exactlength", function(value, element, param) {
    return this.optional(element) || value.length == param;
   }, $.validator.format("Harap masukkan {0} karakter."));
function changeName(name) {
    $('#bank_account_name').val(name);
}
$(document).on("click", ".openOTPModal", function () {
    var mobileNumber = $('#owner_phone').val();
    $(".modal-body #inputNoHP").val( mobileNumber );
});
function requestOTPButton(){
    var phone_num = $('#inputNoHP').val();
    if(phone_num == '') {
        swal({
            title: "Oops...",
            text: 'No Hp tidak boleh kosong',
            confirmButtonColor: "#EF5350",
            type: "error",
            html: true
        });
        return false;
    }
    $.post(
        site_url + 'auth/check_phone/',{
            'phone': phone_num
        },function(res) {
            if(res == 'false'){
                swal({
                    title: "Oops...",
                    text: 'No Hp ini sudah pernah diverifikasi',
                    confirmButtonColor: "#EF5350",
                    type: "error",
                    html: true
                });
            } else {
                $.post(
                    site_url + 'auth/request_token_merchant/',{
                        'phone': phone_num
                    },function(data) {
                        data = JSON.parse(data);
                        if(data.status == 'success'){
                            swal({
                                title: "Good job!",
                                text: data.message,
                                confirmButtonColor: "#66BB6A",
                                type: "success",
                                html: true
                            });
                            $('#btn-token').attr('style', 'pointer-events: none;');
                            $('#btn-token').text('Kirim Ulang (120s)');
                            countdown(120);
                        } else {
                            swal({
                                title: "Oops...",
                                text: data.message,
                                confirmButtonColor: "#EF5350",
                                type: "error",
                                html: true
                            });
                        }
                    }
                )
            }
        });
 }

 function submitOTP(){
    $.post(
        site_url + 'auth/check_token_merchant/',{
            'phone': $('#inputNoHP').val(),
            'code': $('#otpCode').val()
        },function(data) {
            data = JSON.parse(data);
            if(data.status == 'success'){
                $('#verif_icon').hide();
                $('#verif_icon_ok').show();
                $('#verified_phone').val('1');
                $('#requestOTPModal').modal('hide');
            } else {
                swal({
                    title: "Oops...",
                    text: data.message,
                    confirmButtonColor: "#EF5350",
                    type: "error",
                    html: true
                });
            }
        }
    )
 }
 function countdown(duration) {
    var countdown = setInterval(function () {
        if (--duration) {
            $('#btn-token').text('Kirim Ulang (' + duration + 's)');
        } else {
            clearInterval(countdown);
            $('#btn-token').attr('style', '');
            $('#btn-token').text('Request OTP');
        }
    }, 1000);
}

function agreeSnk() {
    $("input[name=agree]").prop("checked", true);
    $('#terms').trigger('click.dismiss.bs.modal');
}
function openSnKmodal() {
    $('#terms').modal('show');;
}
function closeSnKmodal() {
    $('#terms').trigger('click.dismiss.bs.modal');
}
function changeKnowFrom() {
    var val = $("select[name=know_from]").val();
    if(val == 'sales') {
        $('#sales_name').show();
    } else {
        $('#sales_name').hide();
    }
}
$(function () {
    //var currentDate =  new Date(new Date().setFullYear(new Date().getFullYear() - 20));
    $.fn.datepicker.defaults.startView = 2;
    $.fn.datepicker.defaults.defaultViewDate= {year:1980, month:0, day:1};
    $('.datepicker').datepicker();
    $('#province').on('change', function () {
        $.get(site_url+'auth/cities/'+$(this).val(),function(res){
            $('#city').html(res);
            $('#district').html('<option></option>');
        });
    });
    $('#city').on('change', function () {
        $.get(site_url+'auth/districts/'+$(this).val(),function(res){
            $('#district').html(res);
        });
    });
    $.fn.stepy.defaults.legend = false;
    $.fn.stepy.defaults.backLabel = '<span><i class="icon-arrow-left13 position-left"></i> Kembali</span>';
    $.fn.stepy.defaults.nextLabel = '<span>Lanjutkan <i class="icon-arrow-right14 position-right"></i></span>';
    $(".stepy").stepy({
        validate: true,
        block: true,
        next: function (index) {
        //    if (!$(".stepy").validate(validate)) {
        //        return false
        //    }
        }
    });
    $('.stepy-step').find('.button-next').addClass('btn btn-primary');
    $('.stepy-step').find('.button-back').addClass('btn btn-default');

    $("#register").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        rules: {
            email: {
                required: true,
                email: true,
                remote: {
                    url: site_url+'auth/check_email_ajax/',
                    type: "post"
                }
            },
            password: {
                required: true,
                minlength: 6
            },
            confirm_password: {
                required: true,
                equalTo: '#password'
            },
            owner_name: {
                required: true,
                letters_spaceonly: true
            },
            owner_phone: {
                required: true,
                digits: true
            },
            verified_phone: {
                required: true
            },
            owner_id: {
                required: true,
                digits: true,
                exactlength: 16,
                remote: {
                    url: site_url+'auth/check_ktp_ajax/',
                    type: "post"
                }
            },
            store_name: {
                required: true,
                letters_spaceonly: true
            },
            store_telephone: {
                required: true,
                digits: true
            },
            store_postcode: {
                required: true,
                digits: true
            },
            bank_name: {
                required: true,
                letters_spaceonly: true
            },
            bank_branch: {
                required: true,
                letters_spaceonly: true
            },
            bank_account_number: {
                required: true,
                digits: true
            },
            bank_account_name: {
                required: true,
                letters_spaceonly: true
            },
        },
        messages: {
            email: {
                remote: "Maaf, email ini sudah terdaftar!" 
            },
            owner_id: {
                remote: "Maaf, No KTP ini sudah terdaftar" 
            }
        }, 
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        // Different components require proper error label placement
        errorPlacement: function (error, element) {
            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                } else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }


            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.remove();
            label.closest('.form-group').removeClass('has-error');
        },
        submitHandler: function (form) {
            $('#base64_img').val(encodeURIComponent(signaturePad.toDataURL().replace(/^data:image\/(png|jpg);base64,/, '')));
            $(form).ajaxSubmit({
                success: function (data) {
                    responseForm(data);
                }
            });
            grecaptcha.reset()
            return false;
        }
    });
});
function setLocation(lat, lng) {
    $('#lat').val(lat);
    $('#lng').val(lng);
}
$(".toggle-password").click(function() {

    //$(this).toggleClass("icon-eye fa-eye-slash");
    var input = $('#password');
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
  });