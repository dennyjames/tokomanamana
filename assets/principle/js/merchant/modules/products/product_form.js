$(function () {
    $.fn.stepy.defaults.legend = false;
    $.fn.stepy.defaults.backLabel = '<i class="icon-arrow-left13 position-left"></i> Kembali';
    $.fn.stepy.defaults.nextLabel = 'Lanjutkan <i class="icon-arrow-right14 position-right"></i>';
    $(".stepy").stepy({
        validate: true,
        block: true,
//        next: function (index) {
//            if (!$(".stepy").validate(validate)) {
//                return false
//            }
//        }
    });
    $('.stepy-step').find('.button-next').addClass('btn btn-primary');
    $('.stepy-step').find('.button-back').addClass('btn btn-default');

    Dropzone.autoDiscover = false
    var myDropzone = new Dropzone('div#myId', {
        url: base_url + "products/image_upload",
        autoProcessQueue: false,
        uploadMultiple: true,
        parallelUploads: 8,
        maxFiles: 8,
//        previewsContainer: "#showhere",
//        clickable: "#showhere",
        dictDefaultMessage: '',
        maxFilesize: 0.5, // MB
        acceptedFiles: '.jpg,.png',
        addRemoveLinks: true,
        init: function () {
            var myDropzone = this;
//            var submitButton =
//            document.querySelector("button[type=submit]").addEventListener("click", function (e) {
//                e.preventDefault();
//                e.stopPropagation();
//                myDropzone.processQueue();
//            });
            document.querySelector('#submit').addEventListener("click", function (e) {
                if (myDropzone.files.length == 0) {
                    error_message('Gambar produk harus ada.')
                } else {
                    myDropzone.processQueue();
                }
            });
            // First change the button to actually tell Dropzone to process the queue.
//            this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {
//            $('button[type=submit]').on('click', function (e) {
//                // Make sure that the form isn't actually being sent.
//                e.preventDefault();
//                e.stopPropagation();
//                myDropzone.processQueue();
//            });
            this.on("addedfile", function (file) {
                console.log(file);
                var unique_field_id = new Date().getTime();
                file.id = unique_field_id;
                file.previewElement.appendChild(Dropzone.createElement('<div class="radio"><label><input type="radio" value="' + file.name + '" ' + ((myDropzone.files.length == 1) ? 'checked' : '') + ' name="image_primary" id="image_primary' + file.id + '">Gambar Utama</label></div>'));
            });
            this.on("removedfile", function (file) {
                if (!$("input:radio[name='image_primary']").is(":checked")) {
                    $("input:radio[name='image_primary']").prop('checked', true);
                }
            });
            this.on("thumbnail", function (file, done) {
                // Do the dimension checks you want to do
                if (file.width < 600 || file.height < 600) {
//                    file.rejectDimensions();
//                    done("Dimensi gambar minimum 600px.");
                    error_message('Dimensi gambar minimum 600px.');
                    this.removeFile(file);
                }
            });
            this.on("maxfilesexceeded", function (file) {
                this.removeFile(file);
            });
            this.on("error", function (file, done) {
                error_message(done);
                if (!file.accepted)
                    this.removeFile(file);
            });
            this.on('sendingmultiple', function (e) {
                $('body').block({
                    message: '<i class="icon-spinner spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
            });
            this.on('sending', function (file, xhr, formData) {
                $('body').block({
                    message: '<i class="icon-spinner spinner"></i>',
                    overlayCSS: {
                        backgroundColor: '#fff',
                        opacity: 0.8,
                        cursor: 'wait'
                    },
                    css: {
                        border: 0,
                        padding: 0,
                        backgroundColor: 'none'
                    }
                });
//                myDropzone.options.renameFilename = function (file) {
//                    //keeping the file extension.
//                    var id = file.id;
//                    console.log(file);
//                    var ext = file.split('.').pop();
//                    return file.name = id + '.' + ext;
//                };

//                console.log(myDropzone.getAcceptedFiles());
//                for (var key of myDropzone.getAcceptedFiles().keys()) {
//                    console.log(key);
//                }
//                console.log(myDropzone.getAcceptedFiles().length);
//                data.name = data.id;
//                console.log(data.name);
//                console.log(formData.getAll('file'));
//                console.log(xhr);
//                console.log(file_id);
//                console.log($(this).attr('name'));
//                if (data.name == $("input[name='image_primary']:checked").val()) {
//                    console.log(data.name);
//                }
                formData.append("primary", $("input[name='image_primary']:checked").val());
                formData.append("name", $("#name").val());
                formData.append("id_temp", $("#id_temp").val());
            });
        },
//        accept: function (file, done) {
//            var myDropzone = this;
//            file.rejectDimensions = function () {
//                done("Dimensi gambar minimum 600px.");
//            };
//            if (myDropzone.files.length == 9) {
//                done("Jumlah gambar maksimum 8.");
//            }
////            console.log(myDropzone.files.length);
//        }
    });
    $("#form-product").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        // Different components require proper error label placement
        errorPlacement: function (error, element) {
            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                } else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }


            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            } else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            } else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {
            label.remove();
            label.closest('.form-group').removeClass('has-error');
        },
        rules: rules_form,
//        messages: messages_form,
        submitHandler: function (form) {
//            myDropzone.processQueue();
            myDropzone.on('successmultiple', function (e) {
                $(form).ajaxSubmit({
                    success: function (data) {
                        responseForm(data);
                    }
                });
                return false;
            });
        }
    });


//    Dropzone.options.form = {// The camelized version of the ID of the form element
//        // The configuration we've talked about above
//        autoProcessQueue: false,
//        uploadMultiple: true,
//        parallelUploads: 8,
//        maxFiles: 8,
//        previewsContainer: "#showhere",
//        clickable: "#showhere",
//        dictDefaultMessage: '',
//        maxFilesize: 0.2, // MB
//        acceptedFiles: '.jpg,.png',
//        addRemoveLinks: true,
//        // The setting up of the dropzone
//        init: function () {
//            var myDropzone = this;
//            // First change the button to actually tell Dropzone to process the queue.
//            this.element.querySelector("button[type=submit]").addEventListener("click", function (e) {
//                // Make sure that the form isn't actually being sent.
//                e.preventDefault();
//                e.stopPropagation();
//                myDropzone.processQueue();
//            });
//            this.on("addedfile", function (file) {
//                var unique_field_id = new Date().getTime();
//                file.id = unique_field_id;
//                file.previewElement.appendChild(Dropzone.createElement('<div class="radio"><label><input type="radio" value="' + file.name + '" ' + ((myDropzone.files.length == 1) ? 'checked' : '') + ' name="image_primary" id="image_primary' + file.id + '">Gambar Utama</label></div>'));
//            });
//            this.on("removedfile", function (file) {
//                if (!$("input:radio[name='image_primary']").is(":checked")) {
//                    $("input:radio[name='image_primary']").prop('checked', true);
//                }
//            });
//            this.on("thumbnail", function (file) {
//                // Do the dimension checks you want to do
//                if (file.width < 600 || file.height < 600) {
//                    file.rejectDimensions();
//                }
//            });
//            this.on("error", function (file, done) {
//                error_message(done);
//                if (!file.accepted)
//                    this.removeFile(file);
//            });
//        },
//        accept: function (file, done) {
//            var myDropzone = this;
//            file.rejectDimensions = function () {
//                done("Dimensi gambar minimum 600px.");
//            };
//            if (myDropzone.files.length == 9) {
//                done("Jumlah gambar maksimum 8.");
//            }
////            console.log(myDropzone.files.length);
//        }
//
//    }

//    $("#dropzone").dropzone({
//        paramName: "file", // The name that will be used to transfer the file
//        dictDefaultMessage: 'Drop files to upload <span>or CLICK</span>',
//        maxFilesize: 0.1 // MB
//    });

    $('#type').on('change', function () {
        var type = $(this).val();
        if (type == 'c') {
            $('#combination_help').show();
        } else {
            $('#combination_help').hide();
            if (type == 'p') {
                $('.pack-product').show();
            } else {
                $('.pack-product').hide();
            }
        }
    });
    $('#discount').on('change', function () {
        get_price_discount()
    });
    $('#price').on('change', function () {
        get_price_discount()
    });
    $('.get-child-category').on('change', function () {
        var target = $(this).data('target');
        $('#category' + target).html('<option value="">Pilih Kategori ' + target + '</option>');
        if (target == 2) {
            $('#category3').attr('disabled', 'disabled');
            $('#category3').html('<option value="">Pilih Kategori 3</option>');
        }
        $.ajax({
            type: "POST",
            url: base_url + 'products/get_child_category',
            data: {parent: $(this).val()},
            dataType: 'json',
            success: function (data) {
                $('#category' + target).val(null);
                if (data.status = 'success') {
                    $('#category' + target).removeAttr('disabled');
                    $('#category' + target).append(data.list);
                }
            }
        });
    });
    $('#category3').on('change', function () {
        $.ajax({
            type: "POST",
            url: base_url + 'products/get_feature_form',
            data: {category: $(this).val(), product: $('#id').val()},
            success: function (data) {
                $('#feature-form').html(data);
            }
        });
    });
});

function delete_image(id) {
    swal({
        title: lang.message.confirm.delete_image,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: lang.button.delete,
        cancelButtonText: lang.button.cancel,
        closeOnConfirm: false,
        closeOnCancel: false
    }, function () {
        $('#' + id).remove();
        if (!$("input:radio[name='image_primary']").is(":checked")) {
            $("input:radio[name='image_primary']").prop('checked', true);
        }
        swal("Deleted!", lang.message.confirm.delete_image_success, "success");
    });
}

function add_feature() {
    var id = Math.random().toString(36).slice(2);
    var feature_row_html = feature_add_html.replace(/idf/g, id);
    $('#feature table tbody').append(feature_row_html);
    $('.bootstrap-select').selectpicker();
    $('#feature-select-' + id).on('change', function () {
        var feature = $(this).val();
        value_feature(feature, id);
    });
}

function delete_feature(id) {
    $('#row-' + id).remove();
}

function value_feature(feature, id) {
    $.ajax(site_url + 'catalog/products/get_feature_values/' + feature + '/' + id).done(function (response) {
        $('#feature-value-' + id).html(response);
    });
}

function get_price_discount() {
    var price = $('#price').val();
    var discount = $('#discount').val();
    var price_discount = price * discount / 100;
    $('#price-discount').val(price - price_discount);
}