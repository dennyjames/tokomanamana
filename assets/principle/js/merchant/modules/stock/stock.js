function change_confirm(id, stock) {
    id = id;
    let table = $('#table').DataTable();
    swal({
        title: "Apakah Data Sesuai?",
        type: "info",
        showCancelButton: true,
        confirmButtonText: "Ya, Sesuai!",
        cancelButtonText: "Tidak Sesuai!",
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function(isConfirm) {
        if (isConfirm) {
          $.ajax({
              url: site_url + 'stock/confirm',
              data: {id: id},
              type: 'post',
              success: function(data) {
                data = JSON.parse(data);
                table.draw();
                swal({
                  title: data.message,
                  type: data.status,
                  timer: 2000,
                  showConfirmButton: false
                });
              }
          });
        } else {
            $('#not_corresponding').modal('show');
            $('#stock_pricipal').val(stock);
            $('#new_stock').val('');
            $('#id').val(id);
            
            $('#form_not_corresponding').submit(function(e) {
                e.preventDefault();
                let form = $(this);
                let url = form.attr('action');
                let type = form.attr('method');

                $.ajax({
                    url: url,
                    type: type,
                    data: form.serialize(),
                    success: function(data) {
                        $('#not_corresponding').modal('hide');
                        swal({
                            title: data.message,
                            type: data.status,
                            timer: 2000,
                            showConfirmButton: false
                        });
                        table.draw();
                    }
                });
            });
        }
      });
}