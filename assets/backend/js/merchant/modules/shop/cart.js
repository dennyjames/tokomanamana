$(document).ready(function () {
    $('input[type=number]').keyup(function () {
        console.log('qty');
    var qty = parseInt($(this).val());
    if (isNaN(qty) || qty < 1) {
        $(this).val(1);
    } else {
        $(this).val(qty);
    }
    })
});
function cartUpdateQty(qty, product) {
    $.post(base_url + 'shop/update_cart/', {qty: qty, product: product}, function (data) {
        location.reload();
    });
}
function checkStock(){
    $.post(site_url + 'shop/checkout/check_stock/', {}, function (data) {
        data = JSON.parse(data);
        $('.cart-item').attr("style", '');
        $('.alertEmptyStock').html('');
        if(data.status == 'failed') {
            $.each(data.data  , function(i, item) {
                $('#cart-'+item.rowid).attr("style", "border-color:red;border-bottom:1px solid red;'");
                $('#alert-'+item.rowid).html('Maaf, Produk kosong di sekitar anda!');
            });
        } else {
            window.location.href = site_url + 'shop/checkout';
        }
    });
}