$(document).ready(function () {
    $('#province').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        getCities(id);
    });
    $('#city').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        getDistricts(id);
    });
    $('.choose-shipping').change(function (e) {
        var tag = $(this).prop('tagName');
        if (tag == 'SELECT') {
            $('#shipping-cost-' + $(this).find(':selected').data('id')).val($(this).find(':selected').data('value'));
        } else {
            $('#shipping-cost-' + $(this).data('id')).val($(this).data('value'));
        }
        totalShippingCost();
    });
    $('#form-shipping_payment').submit(function (e) {
        if (jQuery(':radio[name=shipping]', '#select-shipping').length) {
            return true;
        } else {
            alert('Jasa pengiriman tidak mencakupi wilayah kota Anda. Hubungi kami untuk bantuan.');
            return false;
        }
    });
    $('#add-address').click(function () {
        $('#modal-address').modal('show');
        $('#message').html('');
        $('#modal-address form')[0].reset();
        $('#city').html('<option value="">Pilih kota</option>');
        $('#district').html('<option value="">Pilih kecamatan</option>');
    });
    $('.info-store').popover({
        html: true,
        content: function () {
            var e = $(this);
            var rowid = e.data('id');
            var id = $('#shipping-' + rowid).val();
            return $.ajax({url: site_url + 'shop/checkout/info_store/' + id,
                dataType: 'html',
                async: false
            }).responseText;
        }
    })
});
function submitCoupon(){
    $.ajax({
        url: site_url + 'shop/checkout/add_coupon',
        type: 'post',
        data: {'couponCode' : $('.couponCode').val()},
        success: function (response) {
            $('#showDisc').attr('style','display:none;');
            response = JSON.parse(response);
            if (response.status == 'success') {
                var html = '<div class="alert alert-success  alert-dismissable">'+
                    '<a href="javascript:void(0)" onclick="resetCoupon()" class="close" aria-label="close">×</a>'+
                    '<strong>Kupon "'+response.data.code+'" sedang digunakan</strong>'+
                '</div>';
                $('#showDisc').attr('style','display:block;');
                $('#coupon_disc').html(formatRupiah(response.data.total_disc, 'Rp. '));
                $('#totalShopping').html(formatRupiah(response.data.total_shopping - response.data.total_disc, 'Rp. '));
            } else {
                var html = '<input type="text" class="couponCode" name="couponCode" style="float:left;height:40px;width:60%;" value=""><a href="javascript:void(0)" onclick="submitCoupon()" class="btn btn-default btn-sm">Submit</a>';
                $('#coupon-alert').html(response.message);
            }
            $('#coupon-input').html(html);
        }
    })
}
function formatRupiah(angka, prefix){
    var number_string = angka.toString(),
    split   		= number_string.split(','),
    sisa     		= split[0].length % 3,
    rupiah     		= split[0].substr(0, sisa),
    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}
function resetCoupon() {
    var r = confirm("Hapus kode kupon??");
    if (r == true) {
        $.ajax({
            url: site_url + 'shop/checkout/reset_coupon',
            type: 'post',
            data: {'reset' : true},
            success: function (response) {
                $('#showDisc').attr('style','display:none;');
                response = JSON.parse(response);
                if (response.status == 'success') {
                    var html = '<input type="text" class="couponCode" name="couponCode" style="float:left;height:40px;width:60%;" value=""><a href="javascript:void(0)" onclick="submitCoupon()" class="btn btn-default btn-sm">Submit</a>';
                    $('#totalShopping').html(formatRupiah(response.totalShopping, 'Rp. '));
                    $('#coupon-input').html(html);
                }
            }
        })
    } 
}
function submitPaymentForm(){
    var dataForm = $('.form').serializeArray();
    var payment = null;
    jQuery.each( dataForm, function( i, field ) {
        if(field.name == 'payment') {
            payment = field.value;
        }
    });
    if (payment == 'kredivo') {
       $('#KredivoModal').modal('show');
    } else {
        $('.form').submit();
    }
}
function agreeKredivo(){
    $('.form').submit();
}
function submit_address() {
    $.ajax({
        url: site_url + 'shop/checkout/add_address',
        type: 'post',
        data: $('#modal-address input, select'),
        success: function (response) {
            response = JSON.parse(response);
            if (response.status == 'success') {
                $('#address-list').append(response.html);
                $('#modal-address').modal('hide');
            } else {
                $('#message').html(response.message);
            }
        }
    })
}
function totalShippingCost() {
    var shipping_cost = 0;
    $('input[name^="shipping_cost"]').each(function () {
        var cost = parseFloat($(this).val());
        if (!isNaN(cost)) {
            shipping_cost += cost;
        }
    });
    $('#shipping-cost-text').text(rp(shipping_cost));
    $('#total-text').text(rp(parseFloat($('#total').val()) + shipping_cost));
}
function getCities(province) {
    $('#district option').remove();
    $("#district").append('<option value="">Pilih kecamatan</option>');
    if (province == '' || province == 0) {
        $('#city option').remove();
        $("#city").append('<option value="">Pilih kota</option>');
        $('#select-shipping').children().remove();
        $('#select-shipping').html('<p>Pilih provinsi terlebih dahulu.</p>');
    } else {
        $.get(site_url + 'shop/checkout/checkout/getCities/' + province, function (data) {
            $('#city').html(data);
            $('#select-shipping').children().remove();
            $('#select-shipping').html('<p>Pilih kota terlebih dahulu.</p>');
        });
    }
}
function getDistricts(city) {
    if (city == '' || city == 0) {
        $('#district option').remove();
        $("#district").append('<option value="">Pilih kecamatan</option>');
        $('#select-shipping').children().remove();
        $('#select-shipping').html('<p>Pilih kota terlebih dahulu.</p>');
    } else {
        $.get(site_url + 'shop/checkout/checkout/getDistricts/' + city, function (data) {
            $('#district').html(data);
            $('#select-shipping').children().remove();
            $('#select-shipping').html('<p>Pilih kecamatan terlebih dahulu.</p>');
        });
    }
}
function getShippings(el) {
//    console.log(el);
    var id = $(el).find(':selected').data('id');
    var merchant = $(el).find(':selected').val();
    var district_from = $(el).find(':selected').data('district-from');
    var district_to = $(el).find(':selected').data('district-to');
    var weight = $(el).find(':selected').data('weight');
    var courier = $(el).find(':selected').data('courier');
//    console.log(merchant);
//    console.log(district);
//    console.log(weight);
//    console.log(courier);
//    $('#shipping-cost').html(rp(0));
//    $('#shipping-cost-promo').html(rp(0));
//    if (district == '' || district == 0) {
//        $('#select-shipping').children().remove();
//        $('#select-shipping').html('<p>Pilih kecamatan terlebih dahulu.</p>');
//    } else {
    $('#shipping-courier-' + id).html('<p>Loading...</p>');
    totalShippingCost();
    $.ajax({
        type: 'post',
        url: site_url + 'shop/checkout/getShippings/',
        data: {
            from: district_from,
            to: district_to,
            weight: weight,
            courier: courier,
            id: id
        },
        success: function (data) {
            $('#shipping-courier-' + id).html(data);
            $('.shipping').change(function (e) {
                $('#shipping-cost-' + $(this).data('id')).val($(this).data('value'));
                totalShippingCost();
            });
        }
    })

}
function selectPayment(el) {
    $('.payment-method-block').removeClass('payment-method-block-active');
    $(el).parents('.payment-method-block').addClass('payment-method-block-active');
}
function rp(number) {
    return "Rp " + parseFloat(number).toFixed(0).replace(/./g, function (c, i, a) {
        return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "." + c : c;
    });
}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

    return true;
}