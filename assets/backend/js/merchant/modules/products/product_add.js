var page = 1;
var text = '';
$('#search').on('keyup', function (e) {
    e.preventDefault();
    if (e.keyCode == 13) {
        var search = $(this).val();
        if (search != '') {
            $('#list-product').html('');
            text = search;
            page = 1;
            load();
//                text = search;
//                $('#list-product').load(site_url + 'products/search_products/' + text + '/' + page_search);
        }
    }
})
//    if ($(window).scrollTop() >= ($(document).height() - $(window).height()) * 0.7) {
//        page_search = page_search + 1;
//        if (text != '') {
//            $.get(site_url + 'products/search_products/' + text + '/' + page_search, function (data) {
//                $('#list-product').append(data);
//            });
//        }
//    }

//$(window).scroll(bindScroll);

function load() {
    if (text != '') {
        $.get(site_url + 'products/search_products/' + text + '/' + page, function (data) {
            data = JSON.parse(data);
            if (data.status == 'success') {
                $('#list-product').append(data.content);
                page = data.page;
                if (data.page == data.page_max) {
                    $('#load-more').hide();
                }else{
                    $('#load-more').show();
                }
            }
        });
    }
}

function loadMore() {
    console.log("More loaded");
    load();
//    $("body").append("<div>");

    $(window).bind('scroll', bindScroll);
}

function bindScroll() {
    if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
        $(window).unbind('scroll');
        loadMore();
    }
}
