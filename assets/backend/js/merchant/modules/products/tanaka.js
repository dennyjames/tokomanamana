$(function () {
    $('body').on('change', '.quantity', function (e) {
        e.preventDefault();
//    console.log($(this).val());
        var el = $(this);
        el.attr('disabled', 'disabled');
        $.ajax({
            type: "POST",
            url: current_url + '/update_quantity',
            data: {product: el.data('product'), quantity: el.val(), id: el.data('id')},
            success: function (data) {
                el.removeAttr('disabled');
                el.data('id', data);
            }
        });
//    $.post(site_url + 'products/search_products/' + text + '/' + page, function (data) {
//        data = JSON.parse(data);
//        if (data.status == 'success') {
//            $('#list-product').append(data.content);
//            page = data.page;
//            if (data.page == data.page_max) {
//                $('#load-more').hide();
//            } else {
//                $('#load-more').show();
//            }
//        }
//    });
    });
    $('#table').DataTable().on('draw', function (e) {
        e.preventDefault();
        $('[data-popup=popover]').popover();
    });
    $('body').on('change', '.pricing_level', function (e) {
        e.preventDefault();
        var checked = 0;
        if($(this).is(':checked')){
            checked = 1;
        }
        $.ajax({
            type: "POST",
            url: current_url + '/update_quantity',
            data: {id: $(this).data('id'), pricing_level: checked},
            success: function (data) {
                
            }
        });
    });
    $('#btn-export').on('click', function () {
        window.open(site_url + 'products/tanaka/export', '_blank');
    });
});