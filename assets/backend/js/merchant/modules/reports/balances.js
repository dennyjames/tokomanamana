$(function () {
    $('#add-request').on('click', function () {
        $('#form-request')[0].reset();
        $('#modal-request').modal('show');
    })
});
function submit_request() {
    $.ajax({
        url: site_url + 'reports/balances/request',
        type: 'post',
        data: $('#form-request').serialize(),
        success: function (res) {
            res = JSON.parse(res);
            if (res.status == 'success') {
                swal({
                    title: "Good job!",
                    text: res.message,
                    confirmButtonColor: "#66BB6A",
                    type: "success",
                    html: true
                }, function () {
                    $('#form-request')[0].reset();
                    $('#modal-request').modal('hide');
                });
            } else {
                error_message(res.message);
            }
        }
    });
}