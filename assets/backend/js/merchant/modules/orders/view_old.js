function change_airway(e, id) {
    $(e).parent().parent().parent().attr('method', 'post');
    $(e).parent().parent().parent().attr('action', site_url + 'orders/sent/' + id);
    $(e).parent().html('<div class="input-group"><input type="text" class="form-control" name="airway" required value="' + $(e).parent().text().trim() + '"><span class="input-group-btn"><button type="submit" class="btn btn-primary"><i class="icon-checkmark"></i></button></span></div>');
}
$(document).on("click", ".openOTPModal", function () {
    var mobileNumber = $(this).data('mobilenumber');
    $(".modal-body #inputNoHP").val( mobileNumber );
});

function requestOTPButton(){
    $.post(
        site_url + 'orders/request_token/',{
            'order_id': $('#otpOrderID').val(),
            'phone': $('#inputNoHP').val()
        },function(data) {
            data = JSON.parse(data);
            if(data.status == 'success'){
               Swal.fire({
                    title: "Good job!",
                    text: 'Kode Verifikasi telah dikirim.',
                    confirmButtonColor: "#66BB6A",
                    type: "success",
                });
                $('#btn-token').attr('disabled', 'disabled');
                $('#btn-token').text('Kirim Ulang (120s)');
                countdown(120);
            } else {
                Swal.fire({
                    title: "Oops...",
                    text: 'Kode Verifikasi gagal dikirim.',
                    confirmButtonColor: "#EF5350",
                    type: "error",

                });
            }
        }
    )
 }

 function submitOTP(){
    var url_success = $('.openOTPModal').data('urlsuccess');
    $.post(
        site_url + 'orders/check_token/',{
            'order_id': $('#otpOrderID').val(),
            'code': $('#otpCode').val()
        },function(data) {
            data = JSON.parse(data);
            if(data.status == 'success'){
                window.location.href = url_success;
            } else {
                swal({
                    title: "Oops...",
                    text: data.message,
                    confirmButtonColor: "#EF5350",
                    type: "error",
                    html: true
                });
            }
        }
    )
 }
 function countdown(duration) {
    var countdown = setInterval(function () {
        if (--duration) {
            $('#btn-token').text('Kirim Ulang (' + duration + 's)');
        } else {
            clearInterval(countdown);
            $('#btn-token').removeAttr('disabled');
            $('#btn-token').text('Request OTP');
        }
    }, 1000);
}





$(function() {
    $('#tolak-btn').on('click',function(e){
        e.preventDefault();
        Swal.fire({
            title: 'Konfirmasi Penolakan',
            text: 'Apakah anda yakin akan menolak pesanan ini?',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonText:'Iya',
            cancelButtonText:'Tidak',


        }).then((result) => {
          if (result.value) {
            window.location = $(this).attr('href');
          }
        })
    })

    $('.batal-btn').on('click',function(e){
        e.preventDefault();
        Swal.fire({
            title: 'Konfirmasi Pembatalan',
            text: 'Apakah anda yakin akan membatalkan pesanan ini?',
            type: 'warning',
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonText:'Iya',
            cancelButtonText:'Tidak',


        }).then((result) => {
          if (result.value) {
             window.location = $(this).attr('href');
          }
        })
    })


})