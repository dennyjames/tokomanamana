$(function () {
    am4core.useTheme(am4themes_animated);
    var chart = am4core.create("chartdiv", am4charts.PieChart);
    chart.legend = new am4charts.Legend();
     // Add data
     chart.data = [];
      $.ajax({
        url: site_url + 'dashboard/chart_order_stats',
        success: function (data) {
            data = JSON.parse(data);
            chart.data = [ {
                "name": "Selesai",
                "total": data.finish,
            }, {
                "name": "Belum Selesai",
                "total": data.progress
            },{
                "name": "Ditolak",
                "total": data.reject
            }];
        }
    });
    // Add and configure Series
    var pieSeries = chart.series.push(new am4charts.PieSeries());
    var colorSet = new am4core.ColorSet();
    colorSet.list = ["#388E3C", "#0288d1", "#F44336"].map(function(color) {
    return new am4core.color(color);
    });
    pieSeries.colors = colorSet;
    pieSeries.dataFields.value = "total";
    pieSeries.dataFields.category = "name";
    pieSeries.slices.template.stroke = am4core.color("#fff");
    pieSeries.slices.template.strokeWidth = 2;
    pieSeries.slices.template.strokeOpacity = 1;
    pieSeries.labels.template.disabled = true;
    pieSeries.ticks.template.disabled = true;
    
    // This creates initial animation
    pieSeries.hiddenState.properties.opacity = 1;
    pieSeries.hiddenState.properties.endAngle = -90;
    pieSeries.hiddenState.properties.startAngle = -90;

});
function copyLink() {
    var copyText = document.getElementById("myInput");
    copyText.select();
    document.execCommand("copy");

    swal('Copy link berhasil', '', 'success');
}