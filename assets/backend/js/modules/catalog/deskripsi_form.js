$(function () {
    $('body').on('click', '#edit-image-btn', function () {
        $('#image-preview').remove();
    });
    $('body').on('click', '#delete-image-btn', function () {
        $('#image').val('');
        $('#image-preview').remove();
        $('#add-image-btn').show();
        $('#edit-image-btn').hide();
        $('#delete-image-btn').hide();
    });
    $('body').on('click', '#edit-banner-btn', function () {
        $('#banner-preview').remove();
    });
    $('body').on('click', '#delete-banner-btn', function () {
        $('#banner').val('');
        $('#banner-preview').remove();
        $('#add-banner-btn').show();
        $('#edit-banner-btn').hide();
        $('#delete-banner-btn').hide();
    });
    $('.listbox').bootstrapDualListbox({
        nonSelectedListLabel: 'Tidak dipilih',
        selectedListLabel: 'Dipilih',
        infoText: 'Total {0}',
        infoTextEmpty: '',
        filterPlaceHolder: 'Cari...',
    });
});
function responsive_filemanager_callback(field_id) {
    if (field_id) {
        var url = $('#' + field_id).val();
        var html = '<div class="col-sm-1" id="'+field_id+'-preview">';
        html += '<div class="thumbnail"><div class="thumb">';
        html += '<img src="' + site_url + '../files/images/' + url + '">';
        html += '</div></div></div>';
        $(html).insertBefore('#add-'+field_id);
        $('#add-' + field_id + '-btn').hide();
        $('#edit-' + field_id + '-btn').show();
        $('#delete-' + field_id + '-btn').show();
    }
}