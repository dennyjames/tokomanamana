$('body').on('click', '#btn-export', function (e) {
    e.preventDefault();
    var table = $('#table').DataTable();
    var page = table.page.info();
    var search = table.search();
    var order = table.order();
    let category = $('#table_category').val();
    let merchant = $('#table_merchant').val();

    window.open(site_url + 'catalog/products/export?category=' + category + '&merchant=' + merchant + '&start=' + page.start + '&length=' + page.length + '&search=' + search + '&order=' + order[0], '_blank');
});
$(function() {

	$('<div class="datatable_content" style="display: inline-block; float: right;"></div>').appendTo('.datatable-header');

	$('.dataTables_length').appendTo('.datatable_content');
	$('.dataTables_length').css('float', 'none');

	load_filter();

});
function load_filter() {
	$.ajax({
		url: site_url + 'catalog/products/get_category',
		method: 'post',
		data: {},
		dataType: 'html',
		success: function(data) {
			$('.datatable-header .datatable_content').append(data);
		}
	});

	$.ajax({
		url: site_url + 'catalog/products/get_merchant',
		method: 'post',
		data: {},
		dataType: 'html',
		success: function(data) {
			$('.datatable-header .datatable_content').append(data);
		}
	});
}
function set_datatable() {
	var table = $('#table').DataTable();
	table.draw();
}
// $('#btn-import').on('click', function () {
//     $('#modal-import').modal('show');
//     $('#modal-import form')[0].reset();
// });
// $('#submit-import').on('click', function () {
//     var data = new FormData();
//     var file = $('#file')[0].files[0];
//     data.append('file', file);
//     $.ajax({
//         url: site_url+'catalog/products/import',
//         data: data,
//         cache: false,
//         contentType: false,
//         processData: false,
//         method: 'POST',
//         type: 'POST',
//         success: function (res) {
//             responseForm(res);
//         }
//     });
// });