$(function () {
    $('body').on('click', '#edit-image-btn', function () {
        $('#image-preview').remove();
    });
    $('body').on('click', '#delete-image-btn', function () {
        $('#image').val('');
        $('#image-preview').remove();
        $('#add-image-btn').show();
        $('#edit-image-btn').hide();
        $('#delete-image-btn').hide();
    });
    $('#approve').on('click', function () {
        swal({
                title: "Apakah ingin disetujui?",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Iya",
                cancelButtonText: "Batal",
                closeOnConfirm: true
            },
            function () {
                let id = $('#approve').data('id');
                $.ajax({
                    url: site_url + 'principle/request/approved',
                    data: {
                        id: id
                    },
                    method: 'post',
                    success: function (data) {
                        data = JSON.parse(data);
                        console.log(data);
                        swal({
                                title: data.message,
                                type: data.status
                            },
                            function () {
                                document.location.href = site_url + 'principle/request';
                            });
                    }
                })
            });
    });
    $("#reject").on('click', function () {
        swal({
            title: "Apakah tidak setuju?",
            type: "error",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Iya",
            cancelButtonText: "Batal",
            closeOnConfirm: true
        },
        function() {
            $('#modal_rejected').modal('show');
        })
    });
    $('input[name=documents_not_complete]').on('click', function() {
        if($(this).prop('checked') == true) {
            $('#documents_not_complete').attr('style', 'display: block;margin-top: 10px;');
        } else {
            $('#documents_not_complete').attr('style', 'display: none;margin-top: 10px;');
        }
    });
    $('input[name=documents_not_valid]').on('click', function() {
        if($(this).prop('checked') == true) {
            $('#documents_not_valid').attr('style', 'display: block;margin-top: 10px;');
        } else {
            $('#documents_not_valid').attr('style', 'display: none;margin-top: 10px;');
        }
    })
});
function responsive_filemanager_callback(field_id) {
    if (field_id) {
        var url = $('#' + field_id).val();
        var html = '<div class="col-md-3" id="image-preview">';
        html += '<div class="thumbnail"><div class="thumb">';
        html += '<img src="' + site_url + '../files/images/' + url + '">';
        html += '</div></div></div>';
        $(html).insertBefore('#add-image');
        $('#add-image-btn').hide();
        $('#edit-image-btn').show();
        $('#delete-image-btn').show();
    }
}