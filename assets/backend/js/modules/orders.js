$(function () {
    var table = $('#table').DataTable();
    table.on('click', '.paid', function (e) {
        var url = $(this).attr('href');
        e.preventDefault();
        swal({
            title: lang.message.confirm.paid,
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: lang.button.yes,
            cancelButtonText: lang.button.cancel,
            closeOnConfirm: false
        }, function () {
            $.ajax({
                url: url,
                success: function (data) {
                    data = JSON.parse(data);
                    table.ajax.reload();
                    swal(data.message, '', data.status);
                }
            });
        });
    })

    $('<div class="datatable_content" style="display: inline-block; float: right;"></div>').appendTo('.datatable-header');

    $('.dataTables_length').appendTo('.datatable_content');
    $('.dataTables_length').css('float', 'none');

    // load_filter();

})

function load_filter() {
    $.ajax({
        url: site_url + 'orders/orders/get_all_merchant',
        data: {},
        success: function (data) {
            $('.datatable-header .datatable_content').append(data);
        }
    });
}

function set_datatable() {
    var table = $('#table').DataTable();
    table.draw();
}