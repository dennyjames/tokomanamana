$(function () {
    var table = $('#table').DataTable();
    table.on('click', '.cancel', function (e) {
        var id = $(this).data('id');
        var bank = $(this).data('bank');
        var amount = $(this).data('amount');
        var account_number = $(this).data('accountnumber');
        swal({
            title: 'Permintaan Tarik Dana akan dicancel Yakin?',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: lang.button.yes,
            cancelButtonText: lang.button.cancel,
            closeOnConfirm: false
        }, function () {
            $.ajax({
                url: site_url+'merchants/withdrawals/pay_cancel/',
                method: 'post',
                data : {id : id, bank: bank, amount: amount, account_number: account_number},
                success: function (data) {
                    data = JSON.parse(data);
                    table.ajax.reload();
                    swal(data.message, '', data.status);
                }
            });
        });
    });
    table.on('click', '.pay', function (e) {
        var id = $(this).data('id');
        var bank = $(this).data('bank');
        var amount = $(this).data('amount');
        var account_number = $(this).data('accountnumber');
        e.preventDefault();
        if(bank == 'BCA') {
            swal({
                title: 'Permintaan Tarik Dana akan diproses otomatis, Yakin?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#EF5350",
                confirmButtonText: lang.button.yes,
                cancelButtonText: lang.button.cancel,
                closeOnConfirm: false
            }, function () {
                $.ajax({
                    url: site_url+'merchants/withdrawals/pay_bca/',
                    method: 'post',
                    data : {id : id, bank: bank, amount: amount, account_number: account_number},
                    success: function (data) {
                        data = JSON.parse(data);
                        table.ajax.reload();
                        swal(data.message, '', data.status);
                    }
                });
            });
        } else {
            swal({
                title: 'Permintaan Tarik Dana sudah Dibayar?',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#EF5350",
                confirmButtonText: lang.button.yes,
                cancelButtonText: lang.button.cancel,
                closeOnConfirm: false
            }, function () {
                $.ajax({
                    url: site_url+'merchants/withdrawals/pay/'+id,
                    success: function (data) {
                        data = JSON.parse(data);
                        table.ajax.reload();
                        swal(data.message, '', data.status);
                    }
                });
            });
        }
    })
})