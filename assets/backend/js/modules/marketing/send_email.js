$(document).ready(function() {

    var max_fields  = 20; //maximum input boxes allowed
    var wrapper     = $("#input_gbr_form"); //Fields wrapper
    var add_button  = $(".add_field_button"); //Add button ID
    //teks form
    var add_teks  = $(".add_field_teks");
    var wrapper_teks = $("#input_teks_form"); 

    var x = 0; //initlal text box count
    var y = 0;
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div id="imgTest'+x+'"></div><div class="col-md-12" id="divv'+x+'" style="margin-bottom:20px"><table><tr><td><label class="control-label"><b>Image '+x+'</b></label></td><td><input style="width:120px" id="inputFileToLoad'+x+'" class="form-control" type="file" name="mytext[]" onchange="encodeImageFileAsURL('+x+');"/></td><td><input style="width:150px" type="teks" id="linkgbr'+x+'" placeholder="Masukkan Link jika diperlukan" class="form-control" name="" ></td><td><button onclick="remove_gbr('+x+')" style="" class="remove_field btn btn-danger"><i class="fa fa-trash"></i></button><input type="hidden" id="namagbr'+x+'"></td></tr></table></div>'); //add input box
            $('#countgbr').val(x);
        }
    });

    $(add_teks).click(function(f){ //on add input button click
        f.preventDefault();
        if(y < max_fields){ //max input box allowed
            y++; //text box increment
            $(wrapper_teks).append('<div id="divteks'+y+'"></div><div id="divvv'+y+'"><table><tr><td><label class="control-label"><b>Teks '+y+'</b></label></td><td><textarea id="teks'+y+'" placeholder="isi teks '+y+'" style="height:100px;width:350px" class="form-control" ></textarea></td><td><button onclick="remove_teks('+y+')" style="" class="remove_teks btn btn-danger"><i class="fa fa-trash"></i></button></td></tr></table></div>'); //add input box
            $('#countteks').val(y);
        }
    });

    /*$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        //$('#imgTest'+x).remove();
        //$('#divv'+x).remove();
        //e.preventDefault(); $(this).parent('div').remove(); 
        x--;
        $('#countgbr').val(x);
    })*/

    /*$(wrapper_teks).on("click",".remove_teks", function(f){ //user click on remove text
        console.log(f);
        $('#divteks'+y).remove();
        $('#divvv'+y).remove();
        //f.preventDefault(); $(this).parent('div').remove(); 
        y--;
        $('#countteks').val(y);
    })*/
});

function remove_teks(id){
    $('#divteks'+id).remove();
    $('#divvv'+id).remove();
    var y = id-1;
    $('#countteks').val(y);
}

function remove_gbr(id){
    $('#imgTest'+id).remove();
    $('#divv'+id).remove();
    var y = id-1;
    $('#countteks').val(y);
}

function encodeImageFileAsURL(id) {

    //for(x=1; x <= jml; x++){
        var filesSelected = document.getElementById("inputFileToLoad"+id).files;
        if (filesSelected.length > 0) {
          var fileToLoad = filesSelected[0];

          var fileReader = new FileReader();

          fileReader.onload = function(fileLoadedEvent) {
            var srcData = fileLoadedEvent.target.result; // <--- data: base64

            var newImage = document.createElement('img');
            newImage.src = srcData;
            document.getElementById("imgTest"+id).innerHTML = newImage.outerHTML;
            var filename = document.getElementById("inputFileToLoad"+id).value.replace(/C:\\fakepath\\/i, ''); //get filename gambar
            $('#namagbr'+id).val(filename);

            //console.log(); 
            var tes = document.getElementById("imgTest"+id).innerHTML;
            var imgurl = $('#imgTest'+id+' img').attr('src');
            $('#imgTest'+id+' img').attr('style', 'width:250px');

          }
          fileReader.readAsDataURL(fileToLoad);
        }
    //}
    
}

function kirim(){
    var customer = $('#customer').prop("checked");
    var merchant = $('#merchant').prop("checked");
    var principal = $('#principal').prop("checked");
    var subscriber = $('#subscriber').prop("checked");

    if(customer == true || merchant == true || principal == true || subscriber == true){
        var jmlgbr = $('#countgbr').val();
        var jmlteks = $('#countteks').val();
        var template = $("input[name='template']:checked").val();
        var image1 = $('#imgTest1 img').attr('src');
        var namasatu = $('#namagbr1').val();
        var data = {};
        //untuk gambar
        for (i=1; i <= jmlgbr; i++){
            var loopimg = 'image'+i;
            var loopnmimg = 'namagbr'+i;
            var looplinkimg = 'linkgbr'+i;
            data[loopimg] = $('#imgTest'+i+' img').attr('src');
            data[loopnmimg] = $('#namagbr'+i).val();
            data[looplinkimg] = $('#linkgbr'+i).val();
        }
        // untuk teks
        for (i=1; i <= jmlteks; i++){
            var loopteks = 'teks'+i;
            data[loopteks] = $('#teks'+i).val();
        }

        data['template'] = template;
        data['jmlgbr'] = jmlgbr;
        data['jmlteks'] = jmlteks;

        console.log(customer);
        console.log(merchant);
        if(customer == true){
            data['customer'] = 1;
        } else {
            data['customer'] = 0;
        }
        if(merchant == true){
            data['merchant'] = 2;
        } else{
            data['merchant'] = 0;
        }
        if(principal == true){
            data['principal'] = 3;
        } else {
            data['principal'] = 0;
        }
        if(subscriber == true){
            data['subscriber'] = 4;
        } else{
            data['subscriber'] = 0;
        }
        console.log(data['customer']);

        $.ajax({
          type: 'POST',
          url: current_url + "/kirim_email",
          data: data,
          dataType: "json",
          success: function (data) {
            if (data.success == 1) {
              window.location.replace(current_url);
              swal({
                  title: "Success",
                    text: 'Submit Berhasil',
                    type: 'success'
                });
            }else if(data.success == 0){
              swal({
                  title: "Error !",
                    text: 'Oops! Something wrong',
                    type: 'error'
                }).then((result) => {
                
              });
            }
          }
        });
        /*$('#customer').focus();
        $('#err_penerima').attr('style', 'display:block; color:green');*/
    }
    else {
        $('#customer').focus();
        $('#err_penerima').attr('style', 'display:block; color:red');
    }
}