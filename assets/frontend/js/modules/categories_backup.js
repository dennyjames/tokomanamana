function addtoCart(id, store_id) {
    $.ajax({
        url: site_url + 'catalog/products/add_to_cart_ajax_new',
        type: 'post',
        data: {
            product: id,
            store: store_id,
            qty: 1
        },
        beforeSend: function() {
            $("#tags-load").css('display', 'block');
        },
        complete: function() {
            $("#tags-load").css('display', 'none');
        },
        success: function(data) {
            data = JSON.parse(data);
            if (data.status == 'error') {
                Swal.fire({
                    type: 'error',
                    title: 'Terjadi Kesalahan',
                    text: data.message
                });
            } else {
                var length = Object.keys(data.items).length;
                var total = 0;
                var total_qty = 0;
                if(length > 0) {
                    var html = '<div style="max-height: 200px; overflow: auto">';
                    html += '<div class="carts-items">';
                    $.each(data.items, function(index, val) {
                        if(val.option) {
                            let cart_item_option = JSON.parse(val.option);
                            $.each(cart_item_option, function(index, value) {
                                total += (Number(value.price * value.quantity));
                                total_qty += Number(value.quantity);
                                html += '<div class="row" id="cart-header-option-' + val.id_encrypt + '" style="padding: 10px 10px;border-bottom:1px solid #EEE;">';
                                html += '<div class="cart-left col-md-4" style="display: inline-block;">';
                                html += '<a class="cart-image" href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                                html += '<img style="width: 50px;" src="' + site_url + 'files/images/' + value.image + '" alt="" title="">';
                                html += '</a>';
                                html += '</div>';
                                html += '<div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">';
                                html += '<div class="">';
                                html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '" style="color:#000;">';
                                html += val.name;
                                html += '</a>';
                                html += '</div>';
                                html += '<div class="cart-price">';
                                html += '<span class="money" style="color: #97C23C;">' + formatRupiah(value.price, '.') + '</span>';
                                html += '<span class="x" style="color: #97C23C;">x ' + value.quantity + '</span>'
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                            })
                        } else {
                            total += (Number(val.price * val.quantity));
                            total_qty += Number(val.quantity);
                            html += '<div class="row" id="cart-header-' + val.id_encrypt + '" style="padding: 10px 10px;border-bottom:1px solid #EEE;">';
                            html += '<div class="cart-left col-md-4" style="display: inline-block;">';
                            html += '<a class="cart-image" href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                            html += '<img style="width: 50px;" src="' + site_url + 'files/images/' + val.image + '" alt="" title="">';
                            html += '</a>';
                            html += '</div>';
                            html += '<div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">';
                            html += '<div class="">';
                            html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '" style="color:#000;">';
                            html += val.name;
                            html += '</a>';
                            html += '</div>';
                            html += '<div class="cart-price">';
                            html += '<span class="money" style="color: #97C23C;">' + formatRupiah(val.price, '.') + '</span>';
                            html += '<span class="x" style="color: #97C23C;">x ' + val.quantity + '</span>'
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                    });
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="subtotal" style="color: black;padding: 15px 20px;font-size:14px;border-top:1px solid #EEE;">';
                    html += '<span>Subtotal:</span><span class="cart-total-right money" style="float: right;">' + formatRupiah(total, '.') + '</span>'
                    html += '</div>';
                    html += '<div class="action" style="padding: 15px 20px;margin-top:-10px;">';
                    html += '<a href="' + site_url + 'cart" class="btn btn-show-all" style="font-size:10px; width: 100%;border-radius:3px;">Lihat Semua</a>'
                    html += '</div>';
                } else {
                    var html = '<p style="font-size: 13px; font-weight: bold; padding: 15px;text-align:center;">Keranjang belanja Anda kosong. Yuk, belanja sekarang!</p>'
                }

                $('#cart-target .badge').html(total_qty);
                $('#cart-button .badge').html(total_qty);
                $('.icon-cart .cart_text .number').html(total_qty);
                $('#cart-target-mobile .number').html(total_qty);
                $('#cart-target .cart-info .cart-content').html(html);
                $('#cart-target-mobile .cart-info .cart-content').html(html);

                // new
                $('.cart_icon #icon_cart .badge').html(total_qty);
                $('.cart_icon .cart-dropdown').html(html);
                Swal.fire({
                    type: 'success',
                    title: 'Produk Berhasil Ditambah!',
                    timer: 1000,
                    showConfirmButton: false,
                    customClass: 'swal-class'
                });
            }
        }
    })
}

function formatRupiah(t, a) {
    var e = (t = t.toString()).replace(/[^,\d]/g, "").toString().split(","),
        s = e[0].length % 3,
        r = e[0].substr(0, s),
        i = e[0].substr(s).match(/\d{3}/gi);
    return i && (separator = s ? "." : "", r += separator + i.join(".")), r = null != e[1] ? r + "," + e[1] : r, null == a ? r : r ? "Rp " + r : ""
}
$(document).ready(function () {
    $("#form-filter select").change(function () {
        $("#form-filter").submit()
    })
});

function add_wishlist(e) {
    let product = $(e).data('product');
    let store = $(e).data('store');
    let customer = $(e).data('customer');

    $.ajax({
        url: site_url + 'catalog/products/add_wishlist_new',
        data: {
            product: product,
            store: store,
            customer: customer
        },
        type: 'POST',
        success: function(data) {
            data = JSON.parse(data);
            if (data.status == 'add') {
                $(e).attr('style', 'color:#d9534f;');
                Swal.fire({
                    type: 'success',
                    title: 'Wishlist Berhasil Ditambah',
                    timer: 1000,
                    showConfirmButton: false
                });
                $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').html(data.total);
                $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').attr('style', 'position: absolute;right:25px;');
            } else {
                $(e).attr('style', 'color:#BBB;');
                Swal.fire({
                    type: 'success',
                    title: 'Wishlist Berhasil Dihapus',
                    timer: 1000,
                    showConfirmButton: false
                });
                $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').html(data.total);
                $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').attr('style', 'position: absolute;right:25px;');
            }
        }
    })
}

function change_store(id, type) {
    if (type == 'over') {
        $('#text-ellipsis-city-' + id).attr('style', 'margin-top:-18px;display:block;transition:.3s;');
    } else {
        $('#text-ellipsis-city-' + id).attr('style', 'margin-top:0px;display:block;transition:.3s;')
    }
}