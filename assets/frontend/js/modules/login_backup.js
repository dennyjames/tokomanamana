$(function () {
    $('form').submit(function () {
        var type = $(this).children('form input[name=login_type]').val();
//       return false;
        $('#login-' + type + ' form').ajaxSubmit({
            success: function (data) {
                data = JSON.parse(data);
                if (data.status == 'success') {
                    window.setTimeout(function () {
                        window.location = data.redirect;
                    }, 1000);
                } else {
                    $('#login-' + type + ' .message').html(data.message);
                }
            }
        });
        return false;
    });
    $('#btn-token').on('click', function () {
        var phone = $('form input[name=phone]').val();
        if (!validationPhone(phone)) {
            $('#login-phone .message').html('<div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Format No. Handphone salah!</div></div>');
        } else {
            $.ajax({
                url: site_url + 'auth/request_token',
                type: 'post',
                data: {phone: phone},
                success: function (res) {
                    res = JSON.parse(res);
                    if (res.status == 'error') {
                        $('#login-phone .message').html('<div class="alert alert-danger fade in alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>Tidak ada akun terkait dengan No. Handphone Anda!</div></div>');
                    } else {
                        $('#btn-token').attr('disabled', 'disabled');
                        $('#btn-token').text('Kirim Ulang (120s)');
                        countdown(120);
                    }
                }
            })
        }
    });
    $('#login_form input').on('blur keyup', function() {
        let name = $(this).attr('name');
        $(this).css('border', '1px solid #CCC');
        $('#login_form #error_' + name).html('');
    });
    $('#form_login_merchant input').on('blur keyup', function() {
        let name = $(this).attr('name');
        $(this).css('border', '1px solid #CCC');
        $('#form_login_merchant #error_' + name).html('');
    });
    $('#login_form').submit(function(e) {
        e.preventDefault();
        $('#login_form .error_validation').html('');
        $('#login_form input').css('border', '1px solid #CCC');
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: $(this).serialize(),
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == 'error') {
                    $.each(data.message, function(index, value) {
                        $('#login_form #error_' + value.type).html(value.message);
                        $('#login_form input[name=' + value.type + ']').css('border', '1px solid #D9534F');
                    })
                } else if(data.status == 'error_validation') {
                    $('#login_form .error_validation_login').html(data.message);
                } else if(data.status == 'success_phone') {
                    $('#modal_login').modal('hide');
                    $('#modal_verification').modal('show');
                    $('#modal_verification #modal_verification_body').html('');
                    let content = `<form action="${site_url + 'verification_phone_login'}" method="POST">
                            <div class="modal-body">
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="error_verification_login col-md-12">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-7 form-group">
                                                <label for="verification_phone_login">Kode Verifikasi Handphone</label>
                                                <input type="text" class="form-control" id="verification_phone_login" name="verification_phone_login" placeholder="Kode Verifikasi Handphone">
                                            </div>
                                            <div class="col-md-5">
                                                <button type="button" id="button_login_verification" class="button_login_verification" data-otp="true" onclick="send_verification('${data.id_customer}', this)">Minta Kode Verifikasi</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn_cancel" data-dismiss="modal" onclick="$('#modal_verification #modal_verification_body').html('');$('#modal_verification').modal('hide');">Tutup</button>
                                <button type="button" class="btn btn_next" onclick="form_verification_login('${data.id_customer}')">Masuk</button>
                            </div>
                            <input type="hidden" name="back" value="${current_url}">
                        </form>`;
                    $('#modal_verification #modal_verification_body').html(content);
                    set_interval_login();
                } else if(data.status == 'error_phone') {
                    $('#modal_login').modal('hide');
                    $('#modal_verification').modal('show');
                    $('#modal_verification #modal_verification_body').html('');
                    let content = `<form action="${site_url + 'verification_phone_login'}" method="POST">
                            <div class="modal-body">
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="error_verification_login col-md-12">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-7 form-group">
                                                <label for="verification_phone_login">Kode Verifikasi Handphone</label>
                                                <input type="text" class="form-control" id="verification_phone_login" name="verification_phone_login" placeholder="Kode Verifikasi Handphone">
                                            </div>
                                            <div class="col-md-5">
                                                <button type="button" id="button_login_verification" class="button_login_verification" data-otp="true" onclick="send_verification('${data.id_customer}', this)">Minta Kode Verifikasi</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn_cancel" data-dismiss="modal" onclick="$('#modal_verification #modal_verification_body').html('');$('#modal_verification').modal('hide');">Tutup</button>
                                <button type="button" class="btn btn_next" onclick="form_verification_login('${data.id_customer}')">Masuk</button>
                            </div>
                            <input type="hidden" name="back" value="${current_url}">
                        </form>`;
                    $('#modal_verification #modal_verification_body').html(content);
                    $('#modal_verification #modal_verification_body .error_verification_login').html(data.message);
                } else {
                    window.location.href = data.message;
                }
            }
        });
    });
    $('#form_login_merchant').submit(function(e) {
        e.preventDefault();
        let link = $(this).attr('action');
        $('#modal_login .error_validation_login_merchant').html('');
        $.ajax({
            url: link,
            type: 'POST',
            data: $(this).serialize(),
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == 'error') {
                    $.each(data.message, function(i, val) {
                        $('#form_login_merchant #error_' + val.type).html(val.message);
                        $('#form_login_merchant input[name=' + val.type + ']').css('border', '1px solid #D9534F');
                    });
                } else if(data.status == 'error_validation') {
                    $('#form_login_merchant .error_validation_login_merchant').html(data.message);
                } else if(data.status == 'success_phone') {
                    $('#modal_verification').modal('show');
                    $('#modal_verification #modal_verification_body').html('');
                    clear_interval();
                    let content = `<form action="${site_url + 'verification_phone_login'}" method="POST">
                            <div class="modal-body">
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="error_verification_login col-md-12">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-7 form-group">
                                                <label for="verification_phone_login">Kode Verifikasi Handphone</label>
                                                <input type="text" class="form-control" id="verification_phone_login" name="verification_phone_login" placeholder="Kode Verifikasi Handphone">
                                            </div>
                                            <div class="col-md-5">
                                                <label for="" style="color: white;">Y</label>
                                                <button type="button" id="button_login_verification" class="button_login_verification" data-otp="true" onclick="send_verification('${data.id_customer}', this)">Minta Kode Verifikasi</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn_cancel" data-dismiss="modal" onclick="$('#modal_login').modal('show');$('#modal_verification #modal_verification_body').html('');$('#modal_verification').modal('hide');">Kembali</button>
                                <button type="button" class="btn btn_next" onclick="form_verification_login('${data.id_customer}')">Masuk</button>
                            </div>
                            <input type="hidden" name="back" value="${current_url}">
                        </form>`;
                    $('#modal_verification #modal_verification_body').html(content);
                    $('#modal_verification #modal_verification_body .error_verification_login').html(data.message);
                    set_interval_login();
                } else if(data.status == 'error_phone') {
                    $('#modal_login').modal('hide');
                    $('#modal_verification').modal('show');
                    $('#modal_verification #modal_verification_body').html('');
                    clear_interval();
                    let content = `<form action="${site_url + 'verification_phone_login'}" method="POST">
                            <div class="modal-body">
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="error_verification_login col-md-12">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-7 form-group">
                                                <label for="verification_phone_login">Kode Verifikasi Handphone</label>
                                                <input type="text" class="form-control" id="verification_phone_login" name="verification_phone_login" placeholder="Kode Verifikasi Handphone">
                                            </div>
                                            <div class="col-md-5">
                                                <label for="" style="color: white;">Y</label>
                                                <button type="button" id="button_login_verification" class="button_login_verification" data-otp="true" onclick="send_verification('${data.id_customer}', this)">Minta Kode Verifikasi</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn_cancel" data-dismiss="modal" onclick="$('#modal_login').modal('show');$('#modal_verification #modal_verification_body').html('');$('#modal_verification').modal('hide');">Kembali</button>
                                <button type="button" class="btn btn_next" onclick="form_verification_login('${data.id_customer}')">Masuk</button>
                            </div>
                            <input type="hidden" name="back" value="${current_url}">
                        </form>`;
                    $('#modal_verification #modal_verification_body').html(content);
                    $('#modal_verification #modal_verification_body .error_verification_login').html(data.message);
                    set_interval_login();
                } else {
                    window.location.href = data.message;
                }
            }
        });
    })
});

function validationPhone(phone) {
    if (phone == null || phone == '') {
        return false;
    }
    var number = /^[0-9]+$/;
    if (!phone.match(number)) {
        return false;
    }
    if (phone.length < 6) {
        return false;
    }
    return true;
}
function countdown(duration) {
    var countdown = setInterval(function () {
        if (--duration) {
            $('#btn-token').text('Kirim Ulang (' + duration + 's)');
        } else {
            clearInterval(countdown);
            $('#btn-token').removeAttr('disabled');
            $('#btn-token').text('Kirim Kode');
        }
    }, 1000);
}
function see_password(e) {
    $(e).toggleClass("fa-eye fa-eye-slash");
    let input = $('#login_form #password_login');
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
}
function password_field_login_merchant(e) {
    $(e).toggleClass("fa-eye fa-eye-slash");
    let input = $('#login_form #password_login_merchant');
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
}