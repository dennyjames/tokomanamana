$(document).ready(function () {
    $('.tracking').on('click',function(e){
        $.ajax({
            url: site_url+'auth/member/tracking/'+$(this).data('id'),
            dataType:'html',
            success: function(res){
                $('#modal-tracking .modal-body').html(res);
                $('#modal-tracking').modal('show');
            }
        })
    });
});