$(document).ready(function () {
    $('.tracking').on('click', function (e) {
        $.ajax({
            url: site_url + 'auth/member/tracking/' + $(this).data('id'),
            dataType: 'html',
            success: function (res) {
                $('#modal-tracking .modal-body').html(res);
                $('#modal-tracking').modal('show');
            }
        })
    });
    $('#payment-confirmation').click(function () {
        $('#modal-payment').modal('show');
        $('#message').html('');
    });
    $('#confirm').click(function () {
        $('#modal-confirm').modal('show');
        $('#modal-confirm .modal-title small').html($(this).data('code'));
        $('#modal-confirm input[name=id]').val($(this).data('id'));
    });
    $('#modal-payment input[type=file]').on('change', function(e) {
        let file = $('#modal-payment input[type=file]');
        // console.log(file[0].value);
        let allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
        let size = file[0].files[0].size;
        if(size >= 300000) {
            $('#modal-payment').modal('hide');
            Swal.fire({
                title: 'Ukuran file terlalu besar!',
                type: 'error'
            });
            $(this).val('');
        }
        if(!allowedExtensions.exec(file[0].value)) {
            $('#modal-payment').modal('hide');
            Swal.fire({
                title: 'Jenis file tidak didukung!',
                type: 'error'
            });
            $(this).val('');
        }
    })
});
function submit_payment() {
    $.ajax({
        url: site_url + 'auth/member/payment_confirmation',
        type: 'post',
        data: $('#modal-payment input, select, textarea'),
        success: function (response) {
            response = JSON.parse(response);
            if (response.status == 'success') {
                $('#message').html(response.message);
                setTimeout(function () {
                    location.reload();
                }, 2000);
            } else {
                $('#message').html(response.message);
            }
        }
    })
}
function submit_confirm() {
    $('#modal-confirm').modal('hide');
    $.ajax({
        url: site_url + 'auth/member/receipt_confirmation',
        type: 'post',
        data: $('#modal-confirm input'),
        beforeSend: function() {
            $("#tags-load").css('display', 'block');
        },
        complete: function() {
            $("#tags-load").css('display', 'none');
        },
        success: function (response) {
            // setTimeout(function () {
            //     location.reload();
            // }, 1000);
            document.location.href = site_url + 'member/review'
        }
    })
}