$(document).ready(function () {
    $('#payment-confirmation').click(function () {
        $('#modal-payment').modal('show');
        $('#message').html('');
    });
    $('[data-toggle="tooltip"]').tooltip();   
    $('#modal-payment input[type=file]').on('change', function(e) {
        let file = $('#modal-payment input[type=file]');
        // console.log(file[0].value);
        let allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
        let size = file[0].files[0].size;
        if(size >= 300000) {
            $('#modal-payment').modal('hide');
            Swal.fire({
                title: 'Ukuran file terlalu besar!',
                type: 'error'
            });
            $(this).val('');
        }
        if(!allowedExtensions.exec(file[0].value)) {
            $('#modal-payment').modal('hide');
            Swal.fire({
                title: 'Jenis file tidak didukung!',
                type: 'error'
            });
            $(this).val('');
        }
    });
    $('#add-image').on('click', function () {
        $('#filemanager').modal('show');
    });
    $('#payment-instruction').on('click', function () {
        $('#payment-instruction-modal').modal('show');
    });
});
function submit_payment() {
    $.ajax({
        url: site_url + 'auth/member/payment_confirmation',
        type: 'post',
        data: $('#modal-payment input, select, textarea'),
        success: function (response) {
            response = JSON.parse(response);
            if (response.status == 'success') {
                window.location.href = site_url+'member/order_detail/'+response.id;
            } else {
                $('#message').html(response.message);
            }
        }
    })
}
function copy_text(val) {
    var tempInput = document.createElement("input");
    tempInput.style = "position: absolute; left: -10000px; top: -10000px";
    tempInput.value = val;
    document.body.appendChild(tempInput);
    tempInput.select();
    document.execCommand("copy");
    document.body.removeChild(tempInput);
}