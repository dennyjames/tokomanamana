$(window).load(function () {
    $(document).off('mouseenter').on('mouseenter', '.pos-slideshow', function (e) {
        $('.ma-banner7-container .timethai').addClass('pos_hover');
    });
    $(document).off('mouseleave').on('mouseleave', '.pos-slideshow', function (e) {
        $('.ma-banner7-container .timethai').removeClass('pos_hover');
    });
});
$(window).load(function () {
    // $('#ma-inivoslider-banner7').nivoSlider({
    //     effect: 'fade',
    //     slices: 15,
    //     boxCols: 8,
    //     boxRows: 4,
    //     animSpeed: 1,
    //     pauseTime: 6000,
    //     startSlide: 0,
    //     controlNav: false,
    //     controlNavThumbs: false,
    //     pauseOnHover: true,
    //     manualAdvance: false,
    //     prevText: 'Prev',
    //     nextText: 'Next',
    //     afterLoad: function () {
    //     },
    //     beforeChange: function () {
    //     },
    //     afterChange: function () {
    //     }
    // });
});
$(document).ready(function () {
    /* timely-owl */
    $("#timely-owl .owl").owlCarousel({
        autoPlay: false,
        items: 1,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [991, 1],
        itemsTablet: [767, 2],
        itemsMobile: [480, 1],
        slideSpeed: 1000,
        paginationSpeed: 1000,
        rewindSpeed: 1000,
        navigation: true,
        stopOnHover: true,
        pagination: false,
        scrollPerPage: true,
    });
    /* special-offer slider */
    $("#special-offer .owl").owlCarousel({
        autoPlay: false,
        items: 1,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [991, 1],
        itemsTablet: [767, 2],
        itemsMobile: [480, 1],
        slideSpeed: 3000,
        paginationSpeed: 3000,
        rewindSpeed: 3000,
        navigation: true,
        stopOnHover: true,
        pagination: false,
        scrollPerPage: true,
    });
    /* latest-news slider */
    $("#latest-news .owl").owlCarousel({
        autoPlay: false,
        items: 1,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [991, 1],
        itemsTablet: [767, 2],
        itemsMobile: [480, 1],
        slideSpeed: 1000,
        paginationSpeed: 1000,
        rewindSpeed: 1000,
        navigation: true,
        stopOnHover: true,
        pagination: false,
        scrollPerPage: true,
    });
    /* clients-say slider */
    $("#clients-say .owl").owlCarousel({
        autoPlay: false,
        items: 1,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [991, 1],
        itemsTablet: [767, 2],
        itemsMobile: [480, 1],
        slideSpeed: 3000,
        paginationSpeed: 3000,
        rewindSpeed: 3000,
        navigation: true,
        stopOnHover: true,
        pagination: false,
        scrollPerPage: true,
    });
    /* featured-products slider */
    $("#featured-products .owl").owlCarousel({
        autoPlay: false,
        items: 4,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [767, 2],
        itemsMobile: [480, 1],
        slideSpeed: 3000,
        paginationSpeed: 3000,
        rewindSpeed: 3000,
        navigation: true,
        stopOnHover: true,
        pagination: false,
        scrollPerPage: true,
    });
    /* new-products slider */
    $("#new-products .owl").owlCarousel({
        autoPlay: false,
        items: 4,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [991, 2],
        itemsTablet: [767, 2],
        itemsMobile: [480, 1],
        slideSpeed: 3000,
        paginationSpeed: 3000,
        rewindSpeed: 3000,
        navigation: true,
        stopOnHover: true,
        pagination: false,
        scrollPerPage: true,
    });
});

$(window).on('resize orientationchange', function() {
    $('.responsive').slick('resize');
});

$(document).ready(function() {
    $('#home_main-banner').slick({
        autoplay: true,
        adaptiveHeight: false,
        autoplaySpeed: 3000,
        arrows: false,
        centerMode: false,
        slidesToShow: 1,
        focusOnSelect: true,
        variableWidth: false,
        dots: false,
        fade: true,
        cssEase: 'linear'
    });

    $('.responsive').slick({
        autoplay: true,
        adaptiveHeight: false,
        autoplaySpeed: 3000,
        arrows: false,
        centerMode: false,
        slidesToShow: 7,
        slideToScroll: 4,
        focusOnSelect: true,
        variableWidth: false,
        dots: false,
        responsive: [{
                breakpoint: 768,
                settings: {
                    centerMode: false,
                    // centerPadding: '10px',
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 480,
                settings: {
                    centerMode: false,
                    // centerPadding: '10px',
                    slidesToShow: 3
                }
            }
        ]
    });
    $('.btn-cart-not-login').on('click', function(e) {
        e.preventDefault();
        back_data = $(this).data('back');
        $('#back-modal').val(back_data);
    });
    $('.btn-add-to-wishlist').on('click', (e) => {
        e.preventDefault();
    })
});

function formatNumber(number) {
    return number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
}

function lastseen(id, ip) {
    $("#btn-last-seen").addClass('active');
    $("#btn-wishlist").removeClass('active');
    var content_ = '';

    content_ += '<div class="spinner">';
    content_ += '  <div class="bounce1"></div>';
    content_ += '  <div class="bounce2"></div>';
    content_ += '  <div class="bounce3"></div>';
    content_ += '</div>';

    $('#list-product').html(content_);

    $.ajax({
        type: 'POST',
        url: site_url + "home/get_last_seen",
        cache: false,
        data: {
            id: id,
            ip: ip
        },
        success: function(result) {
            var obj = JSON.parse(result);
            $('#list-product').html('');
            var content = '';

            for (x = 0; x < obj.length; x++) {
                content += '<div class="col-md-2 col-sm-2 col-xs-6 home_proban_product_card">';
                content += '    <div class="row-container product list-unstyled clearfix">';
                if (obj[x].label_disc) {
                    content += '        <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;">' + obj[x].label_disc + '</span>';
                } else if (obj[x].preorder == 1) {
                    content += '        <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;">Pre Order</span>';

                }
                content += '        <div class="row-left">';
                content += '            <a href="' + obj[x].url + '" class="hoverBorder container_item">';
                content += '                <div class="hoverBorderWrapper">';
                content += '                    <img src="' + obj[x].image + '" class="img-responsive front" alt="' + obj[x].name + '">';
                content += '                </div>';
                content += '            </a>';
                content += '        </div>';
                content += '        <div class="row-right animMix">';
                content += '            <div class="product-title">';
                content += '                <a class="title-5" href="' + obj[x].url + '">' + obj[x].name + '</a>';
                content += '            </div>';
                content += '            <div class="product-price">';
                content += '                <span class="price_sale">';
                if (obj[x].end_price) {
                    content += '                    <span class="money" style="text-decoration: line-through;font-size:9px;color:#8b8f8b">Rp. ' + formatNumber(obj[x].price) + '</span>';
                    content += '                    <span class="money">Rp. ' + formatNumber(obj[x].end_price) + '</span>';
                } else {
                    content += '                    <span class="money">Rp. ' + formatNumber(obj[x].price) + '</span>';
                }
                content += '                </span>';
                content += '            </div>';
                content += '            <div>';
                content += '                <span class="text-ellipsis">' + obj[x].merchant_name + '</span>';
                content += '            </div>';
                content += '            <div class="rating-star">';
                content += '                <span class="spr-badge" data-rating="0.0">';
                content += '                    <span class="spr-starrating spr-badge-starrating">';
                for (let i = 1; i <= obj[x].rating; i++) {
                    content += '                        <i class="spr-icon spr-icon-star"></i>';
                }
                for (i = obj[x].rating + 1; i <= 5; i++) {
                    content += '                        <i class="spr-icon spr-icon-star-empty"></i>';
                }
                content += '                    </span>';
                content += '                </span>';
                content += '            </div>';
                content += '        </div>';
                content += '    </div>';
                content += '</div>';

                $('#list-product').html(content);
            }
            $("#last-seen-content .home-content-dinamic-link").attr('href', site_url + 'last_seen');
        }
    });
    $("#list-product").focus();
}

function wishlist(id) {
    $("#btn-last-seen").removeClass('active');
    $("#btn-wishlist").addClass('active');
    var content_ = '';

    content_ += '<div class="spinner">';
    content_ += '  <div class="bounce1"></div>';
    content_ += '  <div class="bounce2"></div>';
    content_ += '  <div class="bounce3"></div>';
    content_ += '</div>';

    $('#list-product').html(content_);

    $.ajax({
        type: 'POST',
        url: site_url + "home/get_wishlist",
        cache: false,
        data: {
            id: id
        },
        success: function(result) {
            var obj = JSON.parse(result);
            $('#list-product').html('');
            var content = '';

            for (x = 0; x < obj.length; x++) {
                content += '<div class="col-md-2 col-sm-2 col-xs-6 home_proban_product_card">';
                content += '    <div class="row-container product list-unstyled clearfix">';
                if (obj[x].label_disc) {
                    content += '        <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;">' + obj[x].label_disc + '</span>';
                } else if (obj[x].preorder == 1) {
                    content += '        <span class="product-discount-label" style="position:absolute;top:5px;right:10px;z-index:1;background-color:red;color:white;padding:5px 10px;">Pre Order</span>';

                }
                content += '        <div class="row-left">';
                content += '            <a href="' + obj[x].url + '" class="hoverBorder container_item">';
                content += '                <div class="hoverBorderWrapper">';
                content += '                    <img src="' + obj[x].image + '" class="img-responsive front" alt="' + obj[x].name + '">';
                content += '                </div>';
                content += '            </a>';
                content += '        </div>';
                content += '        <div class="row-right animMix">';
                content += '            <div class="product-title">';
                content += '                <a class="title-5" href="' + obj[x].url + '">' + obj[x].name + '</a>';
                content += '            </div>';
                content += '            <div class="product-price">';
                content += '                <span class="price_sale">';
                if (obj[x].end_price) {
                    content += '                    <span class="money" style="text-decoration: line-through;font-size:9px;color:#8b8f8b">Rp. ' + formatNumber(obj[x].price) + '</span>';
                    content += '                    <span class="money">Rp. ' + formatNumber(obj[x].end_price) + '</span>';
                } else {
                    content += '                    <span class="money">Rp. ' + formatNumber(obj[x].price) + '</span>';
                }
                content += '                </span>';
                content += '            </div>';
                content += '            <div>';
                content += '                <span class="text-ellipsis">' + obj[x].merchant_name + '</span>';
                content += '            </div>';
                content += '            <div class="rating-star">';
                content += '                <span class="spr-badge" data-rating="0.0">';
                content += '                    <span class="spr-starrating spr-badge-starrating">';
                for (let i = 1; i <= obj[x].rating; i++) {
                    content += '                        <i class="spr-icon spr-icon-star"></i>';
                }
                for (i = obj[x].rating + 1; i <= 5; i++) {
                    content += '                        <i class="spr-icon spr-icon-star-empty"></i>';
                }
                content += '                    </span>';
                content += '                </span>';
                content += '            </div>';
                content += '        </div>';
                content += '    </div>';
                content += '</div>';

                $('#list-product').html(content);
            }
            $("#last-seen-content .home-content-dinamic-link").attr('href', site_url + 'member/wishlist');
        }
    });
    $("#list-product").focus();
}

function terbaru(id, home_banner) {
    $("#btn-terbaru-" + id).attr('style', 'color:#a6dd69; pointer-events: none;');
    $("#btn-terlaris-" + id).attr('style', 'color:#000; cursor:pointer;');
    var content_ = '';
    /*content_ += '<div class="col-sm-12" style="height:80px; " align="center">';
    content_ += '<div id="loader'+id+'" style="display: inline-block;" class="loader"></div>';
    content_ += '</div>';*/
    content_ += '<div class="spinner">';
    content_ += '  <div class="bounce1"></div>';
    content_ += '  <div class="bounce2"></div>';
    content_ += '  <div class="bounce3"></div>';
    content_ += '</div>';
    $('#list-product-' + id).html(content_);
    var div = document.getElementById('div_ajax' + id);

    var myForm = document.getElementById('form_product');
    var sa = new FormData(myForm);

    $.ajax({
        type: 'POST',
        url: site_url + "home/get_terbaru",
        cache: false,
        data: {
            id: id,
            banner: home_banner
        },
        success: function(result) {
            var obj = JSON.parse(result);
            $('#list-product-' + id).html('');
            console.log(obj);
            var content = '';
            content += '<div class="col-sm-6 proban_banner">';
            content += '                                <a href="#">';
            content += '                                    <img src="' + obj[0].banner + '" alt="">';
            content += '                                </a>';
            content += '                            </div>';

            for (x = 0; x < obj.length; x++) {
                content += '<div class="col-sm-2 proban_product" id="proban_product">';
                content += '    <div class="row-container product list-unstyled clearfix">';
                content += '        <div class="row-left">';
                content += '            <a href="' + obj[x].url + '" class="hoverBorder container_item">';
                content += '                <div class="hoverBorderWrapper">';
                content += '                    <img src="' + obj[x].image + '" class="img-responsive front" alt="' + obj[x].name + '">';
                content += '                </div>';
                content += '            </a>';
                content += '        </div>';

                content += '        <div class="row-right animMix">';
                content += '            <div class="product-title"><a class="title-5" href="' + obj[x].url + '">' + obj[x].name + '</a></div>';
                content += '            <div class="rating-star">';
                content += '                <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>';
                content += '                </span>';
                content += '            </div>';
                content += '            <div class="product-price">';
                content += '                <span class="price_sale"><span class="money">' + obj[x].rupiah + '</span></span>';
                content += '            </div>';
                content += '        </div>';
                content += '    </div>';
                content += '</div>';
                $('#list-product-' + id).html(content);
            }
        }
    });
    $("#list-product-" + id).focus();
}

function terlaris(id, home_banner) {
    $("#btn-terlaris-" + id).attr('style', 'color:#a6dd69; pointer-events: none;');
    $("#btn-terbaru-" + id).attr('style', 'color:#000; cursor:pointer;');

    var content_ = '';
    content_ += '<div class="spinner">';
    content_ += '  <div class="bounce1"></div>';
    content_ += '  <div class="bounce2"></div>';
    content_ += '  <div class="bounce3"></div>';
    content_ += '</div>';
    $('#list-product-' + id).html(content_);
    var div = document.getElementById('div_ajax' + id);

    var myForm = document.getElementById('form_product');
    var sa = new FormData(myForm);
    $.ajax({
        type: 'POST',
        url: site_url + "home/get_terlaris",
        cache: false,
        data: {
            id: id,
            banner: home_banner
        },
        success: function(result) {
            var obj = JSON.parse(result);
            $("#list-product-" + id).html('');
            console.log(obj);
            var content = '';
            content += '<div class="col-sm-6 proban_banner">';
            content += '                                <a href="#">';
            content += '                                    <img src="' + obj[0].banner + '" alt="">';
            content += '                                </a>';
            content += '                            </div>';

            for (x = 0; x < obj.length; x++) {
                content += '<div class="col-sm-2 proban_product" id="proban_product">';
                content += '    <div class="row-container product list-unstyled clearfix">';
                content += '        <div class="row-left">';
                content += '            <a href="' + obj[x].url + '" class="hoverBorder container_item">';
                content += '                <div class="hoverBorderWrapper">';
                content += '                    <img src="' + obj[x].image + '" class="img-responsive front" alt="' + obj[x].name + '">';
                content += '                </div>';
                content += '            </a>';
                content += '        </div>';

                content += '        <div class="row-right animMix">';
                content += '            <div class="product-title"><a class="title-5" href="' + obj[x].url + '">' + obj[x].name + '</a></div>';
                content += '            <div class="rating-star">';
                content += '                <span class="spr-badge" data-rating="0.0"><span class="spr-starrating spr-badge-starrating"><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i><i class="spr-icon spr-icon-star-empty" style=""></i></span><span class="spr-badge-caption">No reviews</span>';
                content += '                </span>';
                content += '            </div>';
                content += '            <div class="product-price">';
                content += '                <span class="price_sale"><span class="money">' + obj[x].rupiah + '</span></span>';
                content += '            </div>';
                content += '        </div>';
                content += '    </div>';
                content += '</div>';
                $('#list-product-' + id).html(content);
            }
        }
    });
    $("#list-product-" + id).focus();
}

$(window).on('resize orientationchange', function() {
    $('.responsive-brand').slick('resize');
    $('responsive-desktop').slick('resize');
    $('responsive-mobile').slick('resize');
});

// function runSwalSetLocation() {
//     Swal.fire({
//         title: 'Silahkan set "LOKASI ANDA" terlebih dahulu!',
//         type: 'info',
//         showCancelButton: false,
//         confirmButtonColor: '#a7c22a',
//         showCloseButton: true,
//         iconColor: '#a7c22a',
//         confirmButtonText: 'Set Lokasi Saya!'

//     }).then((result) => {
//         if (result.value) {
//             window.location.href = 'javascript:my_location();';
//         }
//     })

// }
$(document).ready(function() {
    $('.responsive-desktop').slick({
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000
    });
    $('.responsive-mobile').slick({
        arrows: false,
        dots: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000
    });

    $('.responsive-brand').slick({
        autoplay: true,
        autoplaySpeed: 3000,
        slidesToShow: 5,
        responsive: [{
                breakpoint: 1000,
                settings: {

                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {

                    slidesToShow: 1
                }
            }
        ]

    });

    // if (!localStorage.getItem("visited")) {
    //     runSwalSetLocation();
    //     localStorage.setItem("visited", true);
    // }
});

function addtoCart(id, store_id) {
    $.ajax({
        url: site_url + 'catalog/products/add_to_cart_ajax_new',
        type: 'post',
        data: {
            product: id,
            store: store_id,
            qty: 1
        },
        beforeSend: function() {
            $("#tags-load").css('display', 'block');
        },
        complete: function() {
            $("#tags-load").css('display', 'none');
        },
        success: function(data) {
            data = JSON.parse(data);
            if (data.status == 'error') {
                Swal.fire({
                    type: 'error',
                    title: 'Terjadi Kesalahan',
                    text: data.message
                });
            } else {
                var length = Object.keys(data.items).length;
                var total = 0;
                var total_qty = 0;
                if(length > 0) {
                    var html = '<div style="max-height: 200px; overflow: auto">';
                    html += '<div class="carts-items">';
                    $.each(data.items, function(index, val) {
                        if(val.option) {
                            let cart_item_option = JSON.parse(val.option);
                            $.each(cart_item_option, function(index, value) {
                                total += (Number(value.price * value.quantity));
                                total_qty += Number(value.quantity);
                                html += '<div class="row" id="cart-header-option-' + val.id_encrypt + '" style="padding: 10px 10px;border-bottom:1px solid #EEE;">';
                                html += '<div class="cart-left col-md-4" style="display: inline-block;">';
                                html += '<a class="cart-image" href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                                html += '<img style="width: 50px;" src="' + site_url + 'files/images/' + value.image + '" alt="" title="">';
                                html += '</a>';
                                html += '</div>';
                                html += '<div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">';
                                html += '<div class="">';
                                html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '" style="color:#000;">';
                                html += val.name;
                                html += '</a>';
                                html += '</div>';
                                html += '<div class="cart-price">';
                                html += '<span class="money" style="color: #97C23C;">' + formatRupiah(value.price, '.') + '</span>';
                                html += '<span class="x" style="color: #97C23C;">x ' + value.quantity + '</span>'
                                html += '</div>';
                                html += '</div>';
                                html += '</div>';
                            })
                        } else {
                            total += (Number(val.price * val.quantity));
                            total_qty += Number(val.quantity);
                            html += '<div class="row" id="cart-header-' + val.id_encrypt + '" style="padding: 10px 10px;border-bottom:1px solid #EEE;">';
                            html += '<div class="cart-left col-md-4" style="display: inline-block;">';
                            html += '<a class="cart-image" href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '">';
                            html += '<img style="width: 50px;" src="' + site_url + 'files/images/' + val.image + '" alt="" title="">';
                            html += '</a>';
                            html += '</div>';
                            html += '<div class="cart-right col-md-8" style="display: inline-block;margin-left:-25px;">';
                            html += '<div class="">';
                            html += '<a href="' + site_url + 'catalog/products/view/' + val.product_id + '/' + val.merchant_id + '" style="color:#000;">';
                            html += val.name;
                            html += '</a>';
                            html += '</div>';
                            html += '<div class="cart-price">';
                            html += '<span class="money" style="color: #97C23C;">' + formatRupiah(val.price, '.') + '</span>';
                            html += '<span class="x" style="color: #97C23C;">x ' + val.quantity + '</span>'
                            html += '</div>';
                            html += '</div>';
                            html += '</div>';
                        }
                    });
                    html += '</div>';
                    html += '</div>';
                    html += '<div class="subtotal" style="color: black;padding: 15px 20px;font-size:14px;border-top:1px solid #EEE;">';
                    html += '<span>Subtotal:</span><span class="cart-total-right money" style="float: right;">' + formatRupiah(total, '.') + '</span>'
                    html += '</div>';
                    html += '<div class="action" style="padding: 15px 20px;margin-top:-10px;">';
                    html += '<a href="' + site_url + 'cart" class="btn btn-show-all" style="font-size:10px; width: 100%;border-radius:3px;">Lihat Semua</a>'
                    html += '</div>';
                } else {
                    var html = '<p style="font-size: 13px; font-weight: bold; padding: 15px;text-align:center;">Keranjang belanja Anda kosong. Yuk, belanja sekarang!</p>'
                }

                $('#cart-target .badge').html(total_qty);
                $('#cart-button .badge').html(total_qty);
                $('.icon-cart .cart_text .number').html(total_qty);
                $('#cart-target-mobile .number').html(total_qty);
                $('#cart-target .cart-info .cart-content').html(html);
                $('#cart-target-mobile .cart-info .cart-content').html(html);

                // new
                $('.cart_icon #icon_cart .badge').html(total_qty);
                $('.cart_icon .cart-dropdown').html(html);
                Swal.fire({
                    type: 'success',
                    title: 'Produk Berhasil Ditambah!',
                    timer: 1000,
                    showConfirmButton: false,
                    customClass: 'swal-class'
                });
            }
        }
    })
}

function formatRupiah(angka, prefix) {
    angka = angka.toString();
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split = number_string.split(','),
        sisa = split[0].length % 3,
        rupiah = split[0].substr(0, sisa),
        ribuan = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
}

function change_store(id, type, type_product) {
    if (type_product == 'promotion') {
        if (type == 'over') {
            $('#text-ellipsis-city-promotion-' + id).attr('style', 'margin-top:-18px;display:block;transition:.3s;');
        } else {
            $('#text-ellipsis-city-promotion-' + id).attr('style', 'margin-top:0px;display:block;transition:.3s;')
        }
    } else if (type_product == 'recomend') {
        if (type == 'over') {
            $('#text-ellipsis-city-recomendation-' + id).attr('style', 'margin-top:-18px;display:block;transition:.3s;');
        } else {
            $('#text-ellipsis-city-recomendation-' + id).attr('style', 'margin-top:0px;display:block;transition:.3s;');
        }
    } else if (type_product == 'best_selling') {
        if (type == 'over') {
            $('#text-ellipsis-city-best_selling-' + id).attr('style', 'margin-top:-18px;display:block;transition:.3s;');
        } else {
            $('#text-ellipsis-city-best_selling-' + id).attr('style', 'margin-top:0px;display:block;transition:.3s;');
        }
    }
}

function add_wishlist(e) {
    let product = $(e).data('product');
    let store = $(e).data('store');
    let customer = $(e).data('customer');

    $.ajax({
        url: site_url + 'catalog/products/add_wishlist_new',
        data: {
            product: product,
            store: store,
            customer: customer
        },
        type: 'POST',
        success: function(data) {
            data = JSON.parse(data);
            if (data.status == 'add') {
                $(e).attr('style', 'color:#d9534f;');
                Swal.fire({
                    type: 'success',
                    title: 'Wishlist Berhasil Ditambah',
                    timer: 1000,
                    showConfirmButton: false
                });
                $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').html(data.total);
                $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').attr('style', 'position: absolute;right:25px;');
            } else {
                $(e).attr('style', 'color:#BBB;');
                Swal.fire({
                    type: 'success',
                    title: 'Wishlist Berhasil Dihapus',
                    timer: 1000,
                    showConfirmButton: false
                });
                $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').html(data.total);
                $('.profile-list .profile-dropdown .profile-body .wishlist-link .badge').attr('style', 'position: absolute;right:25px;');
            }
        }
    })
}

function btn_load_more(last_view, last_key) {
    $.ajax({
        url: site_url + 'home/load_more_products',
        data: {
            last_view: last_view,
            last_key: last_key
        },
        type: 'POST',
        beforeSend: function() {
            $('#btn-load-more').html('Memuat...');
        },
        success: function(data) {
            $('#btn-load-more').remove();
            $('#list-product-recomendations').append(data);
            // console.log(data);
        }
    });
}