$(function() {
	$('#register_form #change_captcha_register').on('click', function(e) {
		e.preventDefault();
        $.ajax({
            url: site_url + 'auth/get_captcha',
            data: '',
            success: function(data) {
                data = JSON.parse(data);
                $('#register_form .captcha_register_image').html(data.image);
            }
        });
	});
	$('#register_form').submit(function(e) {
        e.preventDefault();
        $('#register_form .error_validation').html('');
        $('#register_form input').css('border', '1px solid #CCC');
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: $(this).serialize(),
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == 'error') {
                    $.each(data.message, function(index, value) {
                        $('#register_form #error_' + value.type).html(value.message);
                        $('#register_form input[name=' + value.type + ']').css('border', '1px solid #D9534F');
                    })
                } else if(data == 'error_login') {
                    $('#register_form .error_register_validation').html(data.message);
                } else {
                    window.location.href = data.message;
                }
            }
        });
    });
    $('#register_form input[name=phone_register]').on("keypress keyup blur", function (event) {    
       $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $('#register_form input').on('blur keyup', function() {
        let name = $(this).attr('name');
        $(this).css('border', '1px solid #CCC');
        $('#error_' + name).html('');
    });
    $('#register_form #button_phone_verification').on('click', function() {
        $('#register_form #error_phone_register').html('');
        $('#register_form input[name=phone_register]').css('border', '1px solid #CCC');
        let otp = $(this).data('otp');
        let phone_register = $('#register_form #phone_register').val();
        if(otp == true) {
            if(phone_register != '') {
                $.ajax({
                    url: site_url + 'auth/check_phone_register',
                    type: 'POST',
                    data: {
                        phone: phone_register
                    },
                    success: function(data) {
                        if(data == 'not_available') {
                            $('#register_form #error_phone_register').html('<span class="help-block">No Handphone sudah digunakan!</span>');
                            $('#register_form input[name=phone_register]').css('border', '1px solid #D9534F');
                        } else {
                            $.ajax({
                                url: site_url + 'auth/send_otp_register',
                                type: 'post',
                                data: {
                                    phone: phone_register
                                },
                                beforeSend: function() {
                                    let loader = '<div class="lds-ring" style="padding: 5px 0; margin-left: -10px;"><div></div><div></div><div></div><div></div></div>';
                                    $('#register_form #button_phone_verification').html(loader);
                                },
                                success: function(data) {
                                    data = JSON.parse(data);
                                    if(data.status == 'success') {
                                        let duration = 120;
                                        let x = setInterval(function() {
                                            if(--duration) {
                                                $('#register_form #button_phone_verification').text('Kirim Ulang (' + duration + ' s)');
                                                $('#register_form #button_phone_verification').data('otp', false);
                                            } else {
                                                clearInterval(x);
                                                $('#register_form #button_phone_verification').text('Kirim OTP');
                                                $('#register_form #button_phone_verification').data('otp', true);
                                            }
                                        }, 1000);
                                        $('#register_form .send_otp_success').html(data.message);
                                    } else {
                                        $('#register_form .send_otp_success').html(data.message);
                                    }
                                    
                                }
                            });
                        }
                    }
                });
            } else {
                $('#register_form #error_phone_register').html('<span class="help-block">No Handphone harus diisi!</span>');
                $('#register_form input[name=phone_register]').css('border', '1px solid #D9534F');
            }
        } else {
            return false;
        }
    });
});

function password_register_show(e) {
	$(e).toggleClass("fa-eye fa-eye-slash");
    let input = $('#register_form input[name=password_register]');
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
}