$(function() {
    $('#reset_password_form input').on('blur keyup', function() {
        let name = $(this).attr('name');
        $(this).css('border', '1px solid #CCC');
        $('#error_' + name).html('');
    });
	$('#reset_password_form').submit(function(e) {
		e.preventDefault();
		let link = $(this).attr('action');
		$.ajax({
			url: link,
			data: $(this).serialize(),
			type: 'post',
			success: function(data) {
				data = JSON.parse(data);
				if(data.status == 'error_validation') {
					$('#reset_password_form #error_phone_reset_password').html(data.message);
					$('#reset_password_form input[name=phone_reset_password]').css('border', '1px solid #D9534F');
				} else if(data.status == 'error') {
					$('#reset_password_form .error_validation_reset').html(data.message);
				} else {
					$('#choose_validate').modal('show');
					$('#choose_validate #choose_validate_body').html('');
					let html = '<div class="row">';
					html += `<div class="col-md-12 error_validation_reset"></div>`;
					let type = '';
					let val = '';
					$.each(data.message, function(index, value) {
						if(value.type2 == 'email') {
							type = 'Email';
							val = value.message;
						} else {
							type = 'SMS';
							val = value.message;
						}
						html += `<div class="col-md-12">
			                       <button class="btn_choose_validate" type="button" onclick="send_validate('${value.type}', '${value.id_customer}')">Melalui <b>${type}</b> ${val}</button>
			                   </div>`;
					});
					html += '</div>';
					$('#choose_validate #choose_validate_body').html(html);
				}
			}
		})
	})
});

function send_validate(type, id) {
	$.ajax({
		url: site_url + 'auth/send_verification_reset_password',
		data: {
			id: id,
			type: type
		},
		type: 'post',
		success: function(data) {
			data = JSON.parse(data);
			if(data.status == 'error') {
				$('#choose_validate .error_validation_reset').html(data.message);
				return false;
			} else if(data.status == 'phone_success') {
				$('#choose_validate').modal('hide');
				$('#validate_phone').modal('show');
				$('#validate_phone #validate_phone_body').html('');
				let content = `<form action="#">
                    <div class="modal-body">
                    	<div class="row">
		                	<div class="col-md-12 message_error_validate_phone">
		                    </div>
                    	</div>
                        <div class="row">
                            <div class="col-md-8 form-group">
                                <input type="text" name="verification_code" class="form-control" placeholder="Masukkan kode verifikasi">
                            </div>
                            <div class="col-md-4 form-group">
                                <button class="verification_code_phone" type="button" data-otp="true" onclick="send_sms('${data.phone}', this)">Kirim Ulang</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn_cancel" data-dismiss="modal" aria-label="Close" onclick="$('#choose_validate').modal('show');$('#validate_phone #validate_phone_body').html('');$('#validate_phone').modal('hide');">Kembali</button>
                        <button type="button" class="btn btn_next" onclick="submit_phone_verification('${data.phone}')">Lanjutkan</button>
                    </div>
                </form>`;
				$('#validate_phone #validate_phone_body').html(content);
				clear_interval();
                set_interval();
			} else if (data.status == 'error_phone') {
				$('#choose_validate').modal('hide');
				$('#validate_phone').modal('show');
				$('#validate_phone #validate_phone_body').html('');
				let content = `<form action="#">
                    <div class="modal-body">
                    	<div class="row">
		                	<div class="col-md-12 message_error_validate_phone">
		                    </div>
                    	</div>
                        <div class="row">
                            <div class="col-md-8 form-group">
                                <input type="text" name="verification_code" class="form-control" placeholder="Masukkan kode verifikasi">
                            </div>
                            <div class="col-md-4 form-group">
                                <button class="verification_code_phone" type="button" data-otp="true" onclick="send_sms('${data.phone}', this)">Kirim Ulang</button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn_cancel" data-dismiss="modal" aria-label="Close" onclick="$('#choose_validate').modal('show');$('#validate_phone #validate_phone_body').html('');$('#validate_phone').modal('hide');">Kembali</button>
                        <button type="button" class="btn btn_next" onclick="submit_phone_verification('${data.phone}')">Lanjutkan</button>
                    </div>
                </form>`;
				$('#validate_phone #validate_phone_body').html(content);
				$('#validate_phone_body .message_error_validate_phone').html(data.message);
			} else {
				$('#choose_validate').modal('hide');
				Swal.fire({
                    title: data.message,
                    type: 'success',
                    timer: 1000,
                    showConfirmButton: false
                }).then((result) => {
                	document.location.href = site_url;
                });
			}
		}
	});
}
function send_sms(phone, e) {
	let otp = $(e).data('otp');
	let type = 'forgot_password';
	if(otp == true) {
		$.ajax({
			url: site_url + 'auth/send_otp_login',
			data: {phone: phone, type: type},
			type: 'post',
			success: function(data) {
				data = JSON.parse(data);
                if(data.status == 'error') {
                    $('#validate_phone_body .message_error_validate_phone').html(data.message);
                } else {
                	clear_interval();
                	set_interval();
                    $('#validate_phone_body .message_error_validate_phone').html(data.message);
                }
			}
		})
	} else {
		return false;
	}
}
function submit_phone_verification(phone) {
	$('#validate_phone_body .message_error_validate_phone').html('');
	let verification_code = $('#validate_phone #validate_phone_body input[name=verification_code]').val();
	let link = site_url + 'auth/submit_phone_verification';
	$.ajax({
		url: link,
		data: {
			phone: phone,
			verification_code: verification_code
		},
		type: 'post',
		success: function(data) {
			data = JSON.parse(data);
			if(data.status == 'error') {
				$('#validate_phone_body .message_error_validate_phone').html(data.message);
				return false;
			} else {
				$('#change_password_by_phone').modal('show');
				$('#validate_phone').modal('hide');
				let content = `<form action="#">
                    <div class="modal-body">
                    	<div class="row">
		                	<div class="col-md-12 message_change_password_by_phone">
		                    </div>
                    	</div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <input type="password" name="new_password" class="form-control" placeholder="Masukkan password baru">
                                <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"
                                style="float: right;
                                margin-top:-23px;
                                margin-right:10px;
                                position: relative;
                                z-index: 2;
                                cursor: pointer;" onclick="password_field_login_reset(this, 'password')"></span>
                            </div>
                            <div class="col-md-12 form-group">
                                <input type="password" name="confirm_new_password" class="form-control" placeholder="Konfirmasi password baru">
                                <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"
                                style="float: right;
                                margin-top:-23px;
                                margin-right:10px;
                                position: relative;
                                z-index: 2;
                                cursor: pointer;" onclick="password_field_login_reset(this, 'confirm_password')"></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn_cancel" data-dismiss="modal" aria-label="Close" onclick="$('#validate_phone').modal('show');$('#change_password_by_phone #change_password_by_phone_body').html('');$('#change_password_by_phone').modal('hide');">Kembali</button>
                        <button type="button" class="btn btn_next" onclick="change_password_by_phone('${data.id_customer}')">Ubah</button>
                    </div>
                </form>`;
                $('#change_password_by_phone #change_password_by_phone_body').html(content);
			}
		}
	});
}
function change_password_by_phone(id_customer) {
	$('#change_password_by_phone .message_change_password_by_phone').html('');
	let new_password = $('#change_password_by_phone #change_password_by_phone_body input[name=new_password]').val();
	let confirm_new_password = $('#change_password_by_phone #change_password_by_phone_body input[name=confirm_new_password]').val();
	$.ajax({
		url: site_url + 'auth/change_password_by_phone',
		data: {
			new_password: new_password,
			confirm_new_password: confirm_new_password,
			id_customer: id_customer
		},
		type: 'post',
		success: function(data) {
			data = JSON.parse(data);
			if(data.status == 'error') {
				$('#change_password_by_phone .message_change_password_by_phone').html(data.message);
				return false;
			} else {
				$('#change_password_by_phone').modal('hide');
				Swal.fire({
                    title: data.message,
                    type: data.status,
                    timer: 1000,
                    showConfirmButton: false
                }).then((result) => {
                	document.location.href = site_url + 'member/login';
                });
			}
		}
	});
}
function password_field_login_reset(e, type) {
	if(type == 'password') {
		let input = $('#change_password_by_phone #change_password_by_phone_body input[name=new_password]');
		if (input.attr("type") == "password") {
	      input.attr("type", "text");
	    } else {
	      input.attr("type", "password");
	    }
	} else {
		let input = $('#change_password_by_phone #change_password_by_phone_body input[name=confirm_new_password]');
		if (input.attr("type") == "password") {
	      input.attr("type", "text");
	    } else {
	      input.attr("type", "password");
	    }
	}
}
function close_choose_validate() {
    $('#choose_validate #choose_validate_body').html('');$('#choose_validate').modal('hide');
}
function close_validate_phone() {
    $('#validate_phone #validate_phone_body').html('');$('#validate_phone').modal('hide');
}
function close_change_password_by_phone() {
    $('#change_password_by_phone #change_password_by_phone_body').html('');$('#change_password_by_phone').modal('hide');
}
let x1;
function set_interval() {
    let duration = 120;
    x1 = setInterval(function() {
        if(--duration) {
            $('#validate_phone_body .verification_code_phone').text('Kirim Ulang (' + duration + ' s)');
            $('#validate_phone_body .verification_code_phone').data('otp', false);
        } else {
            clearInterval(x1);
            $('#validate_phone_body .verification_code_phone').text('Kirim Ulang');
            $('#validate_phone_body .verification_code_phone').data('otp', true);
        }
    }, 1000);
}
function clear_interval() {
    clearInterval(x1);
    $('#validate_phone_body .verification_code_phone').text('Kirim Ulang');
    $('#validate_phone_body .verification_code_phone').data('otp', true);
}