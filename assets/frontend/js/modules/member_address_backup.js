$(document).ready(function () {
    $('#province').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        get_cities(id);
    });
    $('#city').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        get_districts(id);
    });
    $('#form_address').submit(function(e) {
        e.preventDefault();
        $('#modal-address .error_validation').html('');
        $('#modal-address input, #modal-address select').css('border', '1px solid #ccc');
        let link = $(this).attr('action');
        const loader = '<div class="lds-ring" style="margin-top:50px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
        $.ajax({
            url: link,
            method: 'POST',
            data: $(this).serialize(),
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == 'error') {
                    $.each(data.message, function(i, val) {
                        $('#modal-address #error_' + val.type).html(val.message);
                        $('#modal-address input[name=' + val.type + '], #modal-address select[name=' + val.type + ']').css('border', '1px solid #D9534F');
                    });
                } else if(data.status == 'error_edit') {
                    $('#modal-address #message').html(data.message);
                } else {
                    $('#modal-address').modal('hide');
                    Swal.fire({
                        title: 'Alamat berhasil disimpan!',
                        type: 'success',
                        timer: 1000,
                        showConfirmButton: false
                    });
                    $('#address_list').html(loader);
                    $('#address_list').load(window.location.href + ' #customer_address_list');
                    $('.notification_profile_container').load(window.location.href + ' #notification_profile');
                }
            }
        });
    });
    $('#modal-address input[name=phone]').on('keypress keyup blur', function(event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $('#modal-address input, #modal-address select').on('blur keyup', function() {
        let name = $(this).attr('name');
        $(this).css('border', '1px solid #CCC');
        $('#error_' + name).html('');
    });
});
function add() {
    $('#modal-address').modal('show');
    $('#modal-address form')[0].reset();
    $('#city').html('<option>Pilih kota</option>');
    $('#district').html('<option>Pilih kecamatan</option>');
    $('#modal-address input, #modal-address select').css('border', '1px solid #CCC');
    $('#modal-address .error_validation').html('');
    $('#modal-address input[name=id]').val('');
    $('#modal-address #message').html('');
    $('#modal-address input, #modal-address select').attr('disabled', false);
    $('#modal-address .btn_save').attr('type', 'submit');
}
function add_pin() {
    //alert('jjjj');
    $('#modal-location-pin').modal('show');

    $('#modal-address').modal('hide');

    $('#modal-location-pin').find('.modal-body').load(site_url + 'member/modal_pin_address');
}
function edit(id) {
    $('#modal-address').modal('show');
    $('#modal-address #message').html('');
    $('#modal-address input, #modal-address select').css('border', '1px solid #CCC');
    $('#modal-address .error_validation').html('');
    $.ajax({
        url: site_url + 'auth/member/get_address',
        type: 'post',
        data: {id: id},
        success: function (data) {
            data = JSON.parse(data);
            console.log(data);
            console.log(id);
            if(data.status === 'success') {
                $('#modal-address input, #modal-address select').attr('disabled', false);
                get_cities(data.address.province, data.address.city);
                get_districts(data.address.city, data.address.district);
                $('#modal-address input[name=id]').val(data.address.id);
                $('#modal-address input[name=title]').val(data.address.title);
                $('#modal-address input[name=name]').val(data.address.name);
                $('#modal-address input[name=phone]').val(data.address.phone);
                $('#modal-address input[name=postcode]').val(data.address.postcode);
                $('#modal-address input[name=address]').val(data.address.address);
                $('#province').val(data.address.province);
                $('#temp-lat').val(data.address.latitude);
                $('#temp-lng').val(data.address.longitude);
                $('#modal-address .btn_save').attr('type', 'submit');
                $('#modal-address #message').html('');
            } else if(data.status == 'error') {
                $('#modal-address form')[0].reset();
                get_cities(0, 0);
                get_districts(0, 0);
                $('#modal-address input, #modal-address select').val();
                $('#city').val('');
                $('#district').val('');
                $('#modal-address #message').html(data.message);
                $('#modal-address input, #modal-address select').attr('disabled', true);
                $('#modal-address .btn_save').attr('type', 'button');
                return false;
            }
            
        }
    })
}
function remove(id) {
    const loader = '<div class="lds-ring" style="margin-top:50px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
    Swal.fire({
        title: 'Apakah ingin menghapus alamat ?',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, hapus!',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if (result.value) {
            // $.ajax(site_url + 'auth/member/remove_address/' + id);
            // window.location.reload();
            $.ajax({
                url: site_url + 'auth/member/remove_address',
                data: {id: id},
                type: 'POST',
                success: function(data) {
                    data = JSON.parse(data);
                    if(data.status == 'success') {
                        Swal.fire({
                            title: 'Alamat berhasil dihapus!',
                            type: 'success',
                            timer: 1000,
                            showConfirmButton: false
                        });
                        $('#address_list').html(loader);
                        $('#address_list').load(window.location.href + ' #customer_address_list');
                        $('.notification_profile_container').load(window.location.href + ' #notification_profile');
                    } else {
                        Swal.fire({
                            title: 'Alamat gagal dihapus',
                            type: 'error'
                        });
                    }
                }
            });
        }
    });
}
function get_cities(province, city) {
    $('#district option').remove();
    $("#district").append('<option value="">Pilih kecamatan</option>');
    if (province == '' || province == 0) {
        $('#city option').remove();
        $("#city").append('<option value="">Pilih kota</option>');
    } else {
        $.get(site_url + 'auth/member/get_cities/' + province + (city ? '/' + city : ''), function (data) {
            $('#city').html(data);
        });
    }
}
function get_districts(city, district) {
    if (city == '' || city == 0) {
        $('#district option').remove();
        $("#district").append('<option value="">Pilih kecamatan</option>');
    } else {
        $.get(site_url + 'auth/member/get_districts/' + city + (district ? '/' + district : ''), function (data) {
            $('#district').html(data);
        });
    }
}
function submit_address() {
    //window.alert('Sukses Menambahkan Alamat');
    window.location.reload();

    $.ajax({
        url: site_url + 'auth/member/save_address',
        type: 'post',
        data: $('#modal-address input, select'),
        success: function (response) {
            response = JSON.parse(response);
            if (response.status == 'success') {
                if (response.type == 'add') {
                    $('.collection-mainarea').append(response.html);
                } else {
                    $('#address-' + response.id).replaceWith(response.html);
                }
                $('#modal-address').modal('hide');
            } else {
                $('#message').html(response.message);
            }
        }
    })
}

function test(){
    console.log($('#postcode').val());
    var postcode = $('#postcode').val();

    $.ajax({
        url: site_url + 'auth/member/test',
        type: 'post',
        data: {postcode : postcode},
        success: function (response) {
            response = JSON.parse(response);
            console.log(response);
        }
    })
}

function primary_address(id) {
    const loader = '<div class="lds-ring" style="margin-top:50px; margin-bottom:20px;"><div></div><div></div><div></div><div></div></div>';
    Swal.fire({
        type: 'info',
        title: 'Apakah ingin dijadikan alamat utama ?',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya!',
        cancelButtonText: 'Batal'
    }).then((result) => {
        if(result.value) {
            $.ajax({
                url: site_url + 'auth/member/change_primary_address',
                type: 'post',
                data: {id: id},
                success: function(data) {
                    data = JSON.parse(data);
                    if(data.status == 'success') {
                        Swal.fire({
                            type: data.status,
                            title: data.message,
                            timer: 1000,
                            showConfirmButton: false
                        });
                        $('#address_list').html(loader);
                        $('#address_list').load(window.location.href + ' #customer_address_list');
                        $('.notification_profile_container').load(window.location.href + ' #notification_profile');
                    } else {
                        Swal.fire({
                            type: data.status,
                            title: data.message
                        });
                    }
                }
            });
        }
    });
}