$(document).ready(function () {
    $('#province').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        getCities(id);
    });
    $('#city').change(function (e) {
        e.preventDefault();
        var id = $(this).val();
        getDistricts(id);
    });
});
function getCities(province) {
    $('#district option').remove();
    $("#district").append('<option value="">Pilih kecamatan</option>');
    if (province == '' || province == 0) {
        $('#city option').remove();
        $("#city").append('<option value="">Pilih kota</option>');
        $('#select-shipping').children().remove();
        $('#select-shipping').html('<p>Pilih provinsi terlebih dahulu.</p>');
    } else {
        console.log(site_url);
        $.get(site_url + 'auth/getCities/' + province, function (data) {
            $('#city').html(data);
            $('#select-shipping').children().remove();
            $('#select-shipping').html('<p>Pilih kota terlebih dahulu.</p>');
        });
    }
}
function getDistricts(city) {
    if (city == '' || city == 0) {
        $('#district option').remove();
        $("#district").append('<option value="">Pilih kecamatan</option>');
        $('#select-shipping').children().remove();
        $('#select-shipping').html('<p>Pilih kota terlebih dahulu.</p>');
    } else {
        $.get(site_url + 'auth/getDistricts/' + city, function (data) {
            $('#district').html(data);
            $('#select-shipping').children().remove();
            $('#select-shipping').html('<p>Pilih kecamatan terlebih dahulu.</p>');
        });
    }
}
function rp(number) {
    return "Rp " + parseFloat(number).toFixed(0).replace(/./g, function (c, i, a) {
        return i > 0 && c !== "," && (a.length - i) % 3 === 0 ? "." + c : c;
    });
}
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

    return true;
}