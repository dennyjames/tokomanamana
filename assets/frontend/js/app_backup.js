var slideIndex = 0;
$(function () {
    // if (!location_session) {
    //     getLocation();
    //     // $('#modal-location').modal({
    //     //     backdrop: 'static',
    //     //     keyboard: false
    //     // });
    //     // $('#modal-location').find('.modal-body').load(site_url + 'location/modal_location');
    // }
    showDivs(slideIndex);

    $('.search_box').keyup(function (e) {
        var input = $(this).val();
        $.ajax({
            type: "GET",
            url: site_url + "catalog/search/suggest",
            data: {keyword: input},
            async: true,
            success: function (data) {
                // console.log(data);
                var outWords = $.parseJSON(data);
                $('#search_suggest').html('');
                $('#search_suggest').show();

                for (x = 0; x < outWords.length; x++) {
                    $('#search_suggest').prepend('<a href="' + outWords[x].url + '">' + outWords[x].name + '</a>');
                }

            }
        });
    });
    $('.search_box').focusout(function () {
        setTimeout(function () {
            $('#search_suggest').hide();
        }, 1000);
    });
//    var is_mobile = window.matchMedia("(max-width: 767px)");
//    if (is_mobile) {
//        $('#top').addClass('sticky');
//    }
    $('.navigation_mobile .arrow').on('click', function () {
        if ($(this).attr('class') == 'arrow class_test') {
            $('.navigation_mobile .arrow').removeClass('class_test');
            $('.navigation_mobile').removeClass('active');
            $('.navigation_mobile').find('.menu-mobile-container').hide("slow");
        } else {
            $('.navigation_mobile .arrow').removeClass('class_test');
            $(this).addClass('class_test');
            $('.navigation_mobile').each(function () {
                if ($(this).find('.arrow').attr('class') == 'arrow class_test') {
                    $(this).find('.menu-mobile-container').show("slow");
                    $(this).addClass('active');
                } else {
                    $(this).find('.menu-mobile-container').hide("slow");
                    $(this).removeClass('active');
                }
            });
        }
    });
    $('#btn-subscribe').on('click', function () {
        var email = $('#email-subscribe').val();
        if (is_email(email)) {
            $.ajax({
                type: "post",
                url: site_url + "auth/subscription",
                data: {email: email},
                success: function (data) {
                    $('.footer_newsoc_inner').parent().prepend(data);
                }
            });
        }
        return false;
    });
    $('#button_register').on('click', function(e) {
        e.preventDefault();
        $('#modal_register').modal('show');
        $.ajax({
            url: site_url + 'auth/get_captcha',
            data: '',
            success: function(data) {
                data = JSON.parse(data);
                $('#modal_register .captcha_register_image').html(data.image);
                // $('#modal_register input[type=hidden]').val(data.word);
                $('#modal_register input').css('border', '1px solid #CCC');
                $('#modal_register .error_validation').html('');
                $('#modal_register .error_register_validation').html('');
                $('#modal_register .send_otp_success').html('');
            }
        });
    });
    $('#change_captcha').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            url: site_url + 'auth/get_captcha',
            data: '',
            success: function(data) {
                data = JSON.parse(data);
                $('#modal_register .captcha_register_image').html(data.image);
                // $('#modal_register input[type=hidden]').val(data.word);
            }
        });
    });
    $('#form_register').submit(function(e) {
        e.preventDefault();
        $('#modal_register .error_validation').html('');
        $('#modal_register input').css('border', '1px solid #CCC');
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: $(this).serialize(),
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == 'error') {
                    $.each(data.message, function(index, value) {
                        $('#modal_register #error_' + value.type).html(value.message);
                        $('#modal_register input[name=' + value.type + ']').css('border', '1px solid #D9534F');
                    })
                } else if(data == 'error_login') {
                    $('#modal_register .error_register_validation').html(data.message);
                } else {
                    if($('#modal_register #lat_register').val() && $('#modal_register #lng_register').val()) {
                        var location = {
                            lat: $('#modal_register #lat_register').val(),
                            lng: $('#modal_register #lng_register').val(),
                        }
                        localStorage.setItem('tmmLoc', JSON.stringify(location));
                    }
                    window.location.href = data.message;
                }
            }
        });
    });
    $('#modal_register input[name=phone_register]').on("keypress keyup blur", function (event) {    
       $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });
    $('#modal_register input').on('blur keyup', function() {
        let name = $(this).attr('name');
        $(this).css('border', '1px solid #CCC');
        $('#error_' + name).html('');
    });
    $('#modal_register #button_phone_verification').on('click', function() {
        $('#modal_register #error_phone_register').html('');
        $('#modal_register input[name=phone_register]').css('border', '1px solid #CCC');
        let otp = $(this).data('otp');
        let phone_register = $('#modal_register #phone_register').val();
        let loader = '<div class="lds-ring" style="padding: 5px 2px;margin-left: -20px;"><div></div><div></div><div></div><div></div></div>';
        if(otp == true) {
            if(phone_register != '') {
                $.ajax({
                    url: site_url + 'auth/check_phone_register',
                    type: 'POST',
                    data: {
                        phone: phone_register
                    },
                    success: function(data) {
                        if(data == 'not_available') {
                            $('#modal_register #error_phone_register').html('<span class="help-block">No Handphone sudah digunakan!</span>');
                            $('#modal_register input[name=phone_register]').css('border', '1px solid #D9534F');
                        } else if(data == 'error') {
                            $('#modal_register #error_phone_register').html('<span class="help-block">No Handphone tidak valid!</span>');
                            $('#modal_register input[name=phone_register]').css('border', '1px solid #D9534F');
                        } else {
                            $.ajax({
                                url: site_url + 'auth/send_otp_register',
                                type: 'post',
                                data: {
                                    phone: phone_register
                                },
                                success: function(data) {
                                    data = JSON.parse(data);
                                    if(data.status == 'success') {
                                        clear_interval();
                                        $('#modal_register #button_phone_verification').html(loader);
                                        $('#modal_register .send_otp_success').html(data.message);
                                        set_interval_register();
                                    } else {
                                        $('#modal_register #button_phone_verification').html(loader);
                                        $('#modal_register .send_otp_success').html(data.message);
                                        set_interval_register();
                                    }
                                    
                                }
                            });
                        }
                    }
                });
            } else {
                $('#modal_register #error_phone_register').html('<span class="help-block">No Handphone harus diisi!</span>');
                $('#modal_register input[name=phone_register]').css('border', '1px solid #D9534F');
            }
        } else {
            return false;
        }
    });

    $('#button_login').on('click', function(e) {
        e.preventDefault();
        $('#modal_login').modal('show');
        $('#modal_login input').css('border', '1px solid #CCC');
        $('#modal_login .error_validation').html('');
        $('#modal_login .error_validation_login').html('');
    });
    $('#modal_login input').on('blur keyup', function() {
        let name = $(this).attr('name');
        $(this).css('border', '1px solid #CCC');
        $('#modal_login #error_' + name).html('');
    });
    $('#modal_login_merchant input').on('blur keyup', function() {
        let name = $(this).attr('name');
        $(this).css('border', '1px solid #CCC');
        $('#modal_login_merchant #error_' + name).html('');
    });
    $('#form_login').submit(function(e) {
        e.preventDefault();
        let link = $(this).attr('action');
        $('#modal_login .error_validation_login').html('');
        $.ajax({
            url: link,
            type: 'POST',
            data: $(this).serialize(),
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == 'error') {
                    $.each(data.message, function(i, val) {
                        $('#modal_login #error_' + val.type).html(val.message);
                        $('#modal_login input[name=' + val.type + ']').css('border', '1px solid #D9534F');
                    })
                } else if(data.status == 'error_validation') {
                    $('#modal_login .error_validation_login').html(data.message);
                } else if(data.status == 'success_phone') {
                    $('#modal_login').modal('hide');
                    $('#modal_verification').modal('show');
                    $('#modal_verification #modal_verification_body').html('');
                    clear_interval();
                    let content = `<form action="${site_url + 'verification_phone_login'}" method="POST" onsubmit="form_verification_login('${data.id_customer}');return false;">
                            <div class="modal-body">
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="error_verification_login col-md-12">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-7 form-group">
                                                <label for="verification_phone_login">Kode Verifikasi Handphone</label>
                                                <input type="text" class="form-control" id="verification_phone_login" name="verification_phone_login" placeholder="Kode Verifikasi Handphone">
                                            </div>
                                            <div class="col-md-5">
                                                <label for="" style="color: white;">Y</label>
                                                <button type="button" id="button_login_verification" class="button_login_verification" data-otp="true" onclick="send_verification('${data.id_customer}', this)">Minta Kode Verifikasi</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn_cancel" data-dismiss="modal" onclick="$('#modal_login').modal('show');$('#modal_verification #modal_verification_body').html('');$('#modal_verification').modal('hide');">Kembali</button>
                                <button type="submit" class="btn btn_next">Masuk</button>
                            </div>
                            <input type="hidden" name="back" value="${current_url}">
                        </form>`;
                    $('#modal_verification #modal_verification_body').html(content);
                    $('#modal_verification #modal_verification_body .error_verification_login').html(data.message);
                    set_interval_login();
                } else if(data.status == 'error_phone') {
                    $('#modal_login').modal('hide');
                    $('#modal_verification').modal('show');
                    $('#modal_verification #modal_verification_body').html('');
                    clear_interval();
                    let content = `<form action="#${site_url + 'verification_phone_login'}" method="POST" onsubmit="form_verification_login('${data.id_customer}');return false;">
                            <div class="modal-body">
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="error_verification_login col-md-12">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-7 form-group">
                                                <label for="verification_phone_login">Kode Verifikasi Handphone</label>
                                                <input type="text" class="form-control" id="verification_phone_login" name="verification_phone_login" placeholder="Kode Verifikasi Handphone">
                                            </div>
                                            <div class="col-md-5">
                                                <label for="" style="color: white;">Y</label>
                                                <button type="button" id="button_login_verification" class="button_login_verification" data-otp="true" onclick="send_verification('${data.id_customer}', this)">Minta Kode Verifikasi</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn_cancel" data-dismiss="modal" onclick="$('#modal_login').modal('show');$('#modal_verification #modal_verification_body').html('');$('#modal_verification').modal('hide');">Kembali</button>
                                <button type="submit" class="btn btn_next">Masuk</button>
                            </div>
                            <input type="hidden" name="back" value="${current_url}">
                        </form>`;
                    $('#modal_verification #modal_verification_body').html(content);
                    $('#modal_verification #modal_verification_body .error_verification_login').html(data.message);
                    set_interval_login();
                } else {
                    window.location.href = data.message;
                }
            }
        });
    });
    $('#modal_login .text_register a').on('click', function() {
        $('#modal_login').modal('hide');
        $('#modal_register').modal('show');
        $.ajax({
            url: site_url + 'auth/get_captcha',
            data: '',
            success: function(data) {
                data = JSON.parse(data);
                $('#modal_register .captcha_register_image').html(data.image);
                // $('#modal_register input[type=hidden]').val(data.word);
                $('#modal_register input').css('border', '1px solid #CCC');
                $('#modal_register .error_validation').html('');
                $('#modal_register .error_register_validation').html('');
                $('#modal_register .send_otp_success').html('');
            }
        });
    });
    $('#modal_register .text_login a').on('click', function() {
        $('#modal_register').modal('hide');
        $('#modal_login').modal('show');
        $('#modal_login input').css('border', '1px solid #CCC');
        $('#modal_login .error_validation').html('');
        $('#modal_login .error_validation_login').html('');
    });
    $('#modal_login #form_login_merchant').submit(function(e) {
        e.preventDefault();
        let link = $(this).attr('action');
        $('#modal_login .error_validation_login_merchant').html('');
        $.ajax({
            url: link,
            type: 'POST',
            data: $(this).serialize(),
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == 'error') {
                    $.each(data.message, function(i, val) {
                        $('#modal_login #error_' + val.type).html(val.message);
                        $('#modal_login input[name=' + val.type + ']').css('border', '1px solid #D9534F');
                    });
                } else if(data.status == 'error_validation') {
                    $('#modal_login .error_validation_login_merchant').html(data.message);
                } else if(data.status == 'success_phone') {
                    $('#modal_login').modal('hide');
                    $('#modal_verification').modal('show');
                    $('#modal_verification #modal_verification_body').html('');
                    clear_interval();
                    let content = `<form action="${site_url + 'verification_phone_login'}" method="POST" onsubmit="form_verification_login('${data.id_customer}');return false;">
                            <div class="modal-body">
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="error_verification_login col-md-12">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-7 form-group">
                                                <label for="verification_phone_login">Kode Verifikasi Handphone</label>
                                                <input type="text" class="form-control" id="verification_phone_login" name="verification_phone_login" placeholder="Kode Verifikasi Handphone">
                                            </div>
                                            <div class="col-md-5">
                                                <label for="" style="color: white;">Y</label>
                                                <button type="submit" id="button_login_verification" class="button_login_verification" data-otp="true">Minta Kode Verifikasi</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn_cancel" data-dismiss="modal" onclick="$('#modal_login').modal('show');$('#modal_verification #modal_verification_body').html('');$('#modal_verification').modal('hide');">Kembali</button>
                                <button type="button" class="btn btn_next" onclick="form_verification_login('${data.id_customer}')">Masuk</button>
                            </div>
                            <input type="hidden" name="back" value="${current_url}">
                            <input type="hidden" name="id_merchant" value="${data.id_merchant}" />
                        </form>`;
                    $('#modal_verification #modal_verification_body').html(content);
                    $('#modal_verification #modal_verification_body .error_verification_login').html(data.message);
                    set_interval_login();
                } else if(data.status == 'error_phone') {
                    $('#modal_login').modal('hide');
                    $('#modal_verification').modal('show');
                    $('#modal_verification #modal_verification_body').html('');
                    clear_interval();
                    console.log(data.id_merchant);
                    let content = `<form action="${site_url + 'verification_phone_login'}" method="POST" onsubmit="form_verification_login('${data.id_customer}');return false;">
                            <div class="modal-body">
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="error_verification_login col-md-12">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-7 form-group">
                                                <label for="verification_phone_login">Kode Verifikasi Handphone</label>
                                                <input type="text" class="form-control" id="verification_phone_login" name="verification_phone_login" placeholder="Kode Verifikasi Handphone">
                                            </div>
                                            <div class="col-md-5">
                                                <label for="" style="color: white;">Y</label>
                                                <button type="button" id="button_login_verification" class="button_login_verification" data-otp="true" onclick="send_verification('${data.id_customer}', this)">Minta Kode Verifikasi</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn_cancel" data-dismiss="modal" onclick="$('#modal_login').modal('show');$('#modal_verification #modal_verification_body').html('');$('#modal_verification').modal('hide');">Kembali</button>
                                <button type="submit" class="btn btn_next">Masuk</button>
                            </div>
                            <input type="hidden" name="back" value="${current_url}">
                            <input type="hidden" name="id_merchant" value="${data.id_merchant}" />
                        </form>`;
                    $('#modal_verification #modal_verification_body').html(content);
                    $('#modal_verification #modal_verification_body .error_verification_login').html(data.message);
                    set_interval_login();
                } else {
                    $.ajax({
                        url: site_url + 'ma/auth/login_with_users',
                        data: {
                            id_merchant: data.id_merchant,
                            back_merchant: data.message
                        },
                        type: 'POST',
                        success: function(response) {
                            response = JSON.parse(response);
                            window.location.href = response.back;
                        }
                    })
                }
            }
        });
    });
    $('#logout_merchant').on('click', function(e) {
        e.preventDefault();
        $.ajax({
        url: site_url + 'ma/auth/logout_with_user',
        data: {back: current_url},
        type: 'post',
        success: function(data) {
            data = JSON.parse(data);
            $.ajax({
                url: site_url + 'auth/logout_merchant',
                data: {back: data.back},
                type: 'post',
                success: function(data) {
                    window.location.href = data;
                }
            });
        }
    });
    });

    $('#modal_login_merchant #form_login_merchant').submit(function(e) {
        e.preventDefault();
        let link = $(this).attr('action');
        $('#modal_login_merchant .error_validation_login_merchant').html('');
        $.ajax({
            url: link,
            type: 'POST',
            data: $(this).serialize(),
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == 'error') {
                    $.each(data.message, function(i, val) {
                        $('#modal_login_merchant #error_' + val.type).html(val.message);
                        $('#modal_login_merchant input[name=' + val.type + ']').css('border', '1px solid #D9534F');
                    });
                } else if(data.status == 'error_validation') {
                    $('#modal_login_merchant .error_validation_login_merchant').html(data.message);
                } else if(data.status == 'success_phone') {
                    $('#modal_login_merchant').modal('hide');
                    $('#modal_verification').modal('show');
                    $('#modal_verification #modal_verification_body').html('');
                    clear_interval();
                    let content = `<form action="${site_url + 'verification_phone_login'}" method="POST" onsubmit="form_verification_login('${data.id_customer}');return false;">
                            <div class="modal-body">
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="error_verification_login col-md-12">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-7 form-group">
                                                <label for="verification_phone_login">Kode Verifikasi Handphone</label>
                                                <input type="text" class="form-control" id="verification_phone_login" name="verification_phone_login" placeholder="Kode Verifikasi Handphone">
                                            </div>
                                            <div class="col-md-5">
                                                <label for="" style="color: white;">Y</label>
                                                <button type="button" id="button_login_verification" class="button_login_verification" data-otp="true" onclick="send_verification('${data.id_customer}', this)">Minta Kode Verifikasi</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn_cancel" data-dismiss="modal" onclick="$('#modal_login_merchant').modal('show');$('#modal_verification #modal_verification_body').html('');$('#modal_verification').modal('hide');">Kembali</button>
                                <button type="submit" class="btn btn_next">Masuk</button>
                            </div>
                            <input type="hidden" name="back" value="${current_url}">
                            <input type="hidden" name="id_merchant" value="${data.id_merchant}" />
                        </form>`;
                    $('#modal_verification #modal_verification_body').html(content);
                    $('#modal_verification #modal_verification_body .error_verification_login').html(data.message);
                    set_interval_login();
                } else if(data.status == 'error_phone') {
                    $('#modal_login_merchant').modal('hide');
                    $('#modal_verification').modal('show');
                    $('#modal_verification #modal_verification_body').html('');
                    clear_interval();
                    let content = `<form action="${site_url + 'verification_phone_login'}" method="POST" onsubmit="form_verification_login('${data.id_customer}');return false;">
                            <div class="modal-body">
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="error_verification_login col-md-12">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-7 form-group">
                                                <label for="verification_phone_login">Kode Verifikasi Handphone</label>
                                                <input type="text" class="form-control" id="verification_phone_login" name="verification_phone_login" placeholder="Kode Verifikasi Handphone">
                                            </div>
                                            <div class="col-md-5">
                                                <label for="" style="color: white;">Y</label>
                                                <button type="button" id="button_login_verification" class="button_login_verification" data-otp="true" onclick="send_verification('${data.id_customer}', this)">Minta Kode Verifikasi</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn_cancel" data-dismiss="modal" onclick="$('#modal_login_merchant').modal('show');$('#modal_verification #modal_verification_body').html('');$('#modal_verification').modal('hide');">Kembali</button>
                                <button type="submit" class="btn btn_next">Masuk</button>
                            </div>
                            <input type="hidden" name="back" value="${current_url}">
                            <input type="hidden" name="id_merchant" value="${data.id_merchant}" />
                        </form>`;
                    $('#modal_verification #modal_verification_body').html(content);
                    $('#modal_verification #modal_verification_body .error_verification_login').html(data.message);
                    set_interval_login();
                } else {
                    $.ajax({
                        url: site_url + 'ma/auth/login_with_users',
                        data: {
                            id_merchant: data.id_merchant,
                            back_merchant: data.message
                        },
                        type: 'POST',
                        success: function(response) {
                            response = JSON.parse(response);
                            window.location.href = response.back;
                        }
                    })
                }
            }
        });
    });
    $('#see_merchant_').on('click', function(e) {
        e.preventDefault();
        $.ajax({
            url: site_url + 'ma/auth/check_user_merchant',
            data: {},
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == 'not_login') {
                    $('#modal_login_merchant').modal('show');
                } else {
                    window.location.href = site_url + 'ma';
                }
            }
        })
    })
});
function form_verification_login(id) {
    let verification_code = $('#modal_verification #modal_verification_body input[name=verification_phone_login]').val();
    let back = $('#modal_verification #modal_verification_body input[name=back]').val();
    let id_merchant = $('#modal_verification #modal_verification_body input[name=id_merchant]').val();
    if(id_merchant) {
        id_merchant = id_merchant;
    } else {
        id_merchant = false;
    }
    $.ajax({
        url: site_url + 'auth/login_verification_phone',
        data: {
            id: id,
            verification_code: verification_code,
            back: back,
            id_merchant: id_merchant
        },
        type: 'post',
        success: function(data) {
            data = JSON.parse(data);
            if(data.status == 'error') {
                $('#modal_verification_body .error_verification_login').html(data.message);
            } else if (data.status == 'success_merchant') {
                $.ajax({
                    url: site_url + 'ma/auth/login_with_users',
                    data: {
                        id_merchant: data.id_merchant,
                        back_merchant: data.message
                    },
                    type: 'POST',
                    success: function(response) {
                        response = JSON.parse(response);
                        window.location.href = response.back;
                    }
                })
            } else {
                window.location.href = data.message;
            }
        }
    });
}
function form_verification_login_merchant(id) {
    let verification_code = $('#modal_verification #modal_verification_body input[name=verification_phone_login]').val();
    let back = $('#modal_verification #modal_verification_body input[name=back]').val();
    let id_merchant = $('#modal_verification #modal_verification_body input[name=id_merchant]').val();
    if(id_merchant) {
        id_merchant = id_merchant;
    } else {
        id_merchant = false;
    }
    $.ajax({
        url: site_url + 'auth/login_verification_phone_merchant',
        data: {
            id: id,
            verification_code: verification_code,
            back: back,
            id_merchant: id_merchant
        },
        type: 'post',
        success: function(data) {
            data = JSON.parse(data);
            if(data.status == 'error') {
                $('#modal_verification_body .error_verification_login').html(data.message);
            } else {
                $.ajax({
                    url: site_url + 'ma/auth/login_with_users',
                    data: {
                        id_merchant: data.id_merchant,
                        back_merchant: data.message
                    },
                    type: 'POST',
                    success: function(response) {
                        response = JSON.parse(response);
                        window.location.href = response.back;
                    }
                })
            }
        }
    });
}
function send_verification(id, e) {
    let otp = $(e).data('otp');
    if(otp == true) {
        $.ajax({
            url: site_url + 'auth/send_otp_login',
            data: {
                id: id
            },
            type: 'post',
            success: function(data) {
                data = JSON.parse(data);
                if(data.status == 'error') {
                    clear_interval();
                    set_interval_login();
                    $('#modal_verification_body .error_verification_login').html(data.message);
                } else {
                    clear_interval();
                    set_interval_login();
                    $('#modal_verification_body .error_verification_login').html(data.message);
                }
            }
        });
    } else {
        return false;
    }
}
let x_login;
let x_register;
function set_interval_register(duration = null) {
    if(duration == null) {
        duration = 120;
    }
    x_register = setInterval(function() {
        if(--duration) {
            $('#modal_register #button_phone_verification').text('Kirim Ulang (' + duration + ' s)');
            $('#modal_register #button_phone_verification').data('otp', false);
        } else {
            clearInterval(x_register);
            $('#modal_register #button_phone_verification').text('Kirim OTP');
            $('#modal_register #button_phone_verification').data('otp', true);
        }
    }, 1000);
}
function set_interval_login(duration = null) {
    if(duration == null) {
        duration = 120;
    }
    x_login = setInterval(function() {
        if(--duration) {
            $('#modal_verification_body #button_login_verification').text('Kirim Ulang (' + duration + ' s)');
            $('#modal_verification_body #button_login_verification').data('otp', false);
        } else {
            clearInterval(x_login);
            $('#modal_verification_body #button_login_verification').text('Kirim Ulang');
            $('#modal_verification_body #button_login_verification').data('otp', true);
        }
    }, 1000);
}
function clear_interval() {
    clearInterval(x_login);
    clearInterval(x_register);
    $('#modal_verification_body #button_login_verification').text('Kirim Ulang');
    $('#modal_verification_body #button_login_verification').data('otp', true);
    $('#modal_register #button_phone_verification').text('Kirim OTP');
    $('#modal_register #button_phone_verification').data('otp', true);
}
function getLocation() {
    if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition,showError);
    } else {
    var lat = '-6.186486399999999';
    var lng = '106.83409110000002';
    auto_set(lat,lng);
    alert("Geolocation is not supported by this browser.");
    }
  }
  
  function showPosition(position) {
    auto_set(position.coords.latitude, position.coords.longitude);
    //alert("Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude);    
  }
  
  function showError(error) {
    var lat = '-6.186486399999999';
    var lng = '106.83409110000002';
    switch(error.code) {
      case error.PERMISSION_DENIED:
        alert("User denied the request for Geolocation.");
        break;
      case error.POSITION_UNAVAILABLE:
        alert("Location information is unavailable.");
        break;
      case error.TIMEOUT:
        alert("The request to get user location timed out.");
        break;
      case error.UNKNOWN_ERROR:
        alert("An unknown error occurred.");
        break;
    }
    auto_set(lat,lng);
  }

function auto_set(lat,lng) {
    $.ajax({
        type: 'post',
        url: site_url + 'location/set',
        data: {lat: lat, lng: lng},
        success: function (data) {
            var location = {
                lat: lat,
                lng: lng,
            }
            localStorage.setItem('tmmLoc', JSON.stringify(location));
            window.location.reload();
        }
    });
}

function is_email(email) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(email);
}

function my_location() {
    $('#modal-location').find('.modal-body').load(site_url + 'location/modal_location');
    $('#modal-location').modal('show');
}

function plusDivs(n) {
    showDivs(slideIndex += n);
}

function currentDiv(n) {
    showDivs(slideIndex = n);
}

function showDivs(n) {
    var i;
    var x = $('.hssEach');
    if (n == x.length) {
        slideIndex = 0;
    }
    if (n < 0) {
        slideIndex = (x.length - 1)
    }
    $('#slide-id').text(slideIndex + 1);
    x.each(function () {
        $(this).hide();
        if ($(this).index() == slideIndex) {
            $(this).show();
        }
    });
}
//function set_location(latitude, longitude) {
//    $.ajax({
//        type: 'post',
//        url: site_url + 'location/set',
//        data: {lat: latitude, lng: longitude},
//        success: function (data) {
//            var location = {
//                lat: latitude,
//                lng: longitude,
//            }
//            localStorage.setItem('tmmLoc', location);
//        }
//    });
//}
function password_field_register(e) {
    $(e).toggleClass("fa-eye fa-eye-slash");
    let input = $('#modal_register input[name=password_register]');
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
}
function password_field_login(e) {
    $(e).toggleClass("fa-eye fa-eye-slash");
    let input = $('#modal_login #password_login');
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
}
function password_field_login_merchant(e) {
    $(e).toggleClass("fa-eye fa-eye-slash");
    let input = $('#modal_login #password_login_merchant');
    if (input.attr("type") == "password") {
      input.attr("type", "text");
    } else {
      input.attr("type", "password");
    }
}
function add_other_address() {
    $('#modal_register').modal('hide');
    $('#modal_set_address').modal('show');
    $('#modal_set_address').attr('style', 'padding-right:7px;display:block;');
    $('#modal_set_address').find('#modal_set_address_body').load(site_url + 'location/modal_location');
}