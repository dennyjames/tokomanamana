$(document).ready(function () {
    $('.thumbnail-container .bxslider').bxSlider({
        slideWidth: 94,
        slideMargin: 5,
        minSlides: 4,
        maxSlides: 4,
        pager: false,
        speed: 500,
        pause: 3000
    });
    $('#review-rating').rating({
        step: 1,
        size: 'lg',
        showClear: false,
        showCaption: false,
        animate: false
    });
});
function submitReview(id) {
    $.ajax({
        url: base_url + 'catalog/products/submitReview',
        method: 'post',
        data: {
            title: $('#review-title').val(),
            description: $('#review-description').val(),
            rating: $('#review-rating').val(),
            product: id
        },
        success: function (data) {
            data = JSON.parse(data);
            if (data.status == 1) {
                $('.review-notif').addClass('error');
            } else if (data.status == 2) {
                $('#form-review')[0].reset();
                $('.review-notif').addClass('success');
            }
            $('.review-notif').html(data.message);
            $('#review-' + data.id).focus();
        }
    });
}

function likeDislike(type, id) {
    var likes = $('#likedislike' + id).find('.likes');
    var dislikes = $('#likedislike' + id).find('.dislikes');
    if (type == 1) {
        likes.text(parseInt(likes.text()) + 1);
        $('#likedislike' + id).find('.like').addClass('current');
    } else if (type == 2) {
        dislikes.text(parseInt(dislikes.text()) + 1);
        $('#likedislike' + id).find('.dislike').addClass('current');
    }
    $('#likedislike' + id).find('div').removeAttr('onclick');
    $.ajax({
        method: 'post',
        url: base_url + 'catalog/products/submitLikeDislike',
        data: {
            id: id,
            type: type
        },
        success: function (data) {
            $('#likedislike' + id).find('.notif').html(data);
        }
    });
}