<?php

date_default_timezone_set('Asia/Jakarta');

$system_path = '../protected/system';
$application_folder = '../protected/merchant/base';
$view_folder = '../protected/merchant/views';

require_once '../protected/db.php';
require_once '../protected/system.php';
